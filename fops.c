/* SPDX-License-Identifier: MIT */
/*
 * Matrox kernel stub
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/vmalloc.h>
#include <asm/fb.h>

static DEFINE_MUTEX(mga_mutex);

static LIST_HEAD(mga_devs);

struct mga_dev_entry {
	struct list_head list;
	struct miscdevice *misc;
	struct pci_dev *pdev;
};

static struct pci_dev *mga_get_pci_dev(int minor)
{
	struct mga_dev_entry *ent;

	list_for_each_entry(ent, &mga_devs, list) {
		if (ent->misc && ent->misc->minor == minor)
			return ent->pdev;
	}

	return NULL;
}

static bool mga_has_dev(struct pci_dev *pdev)
{
	struct mga_dev_entry *ent;

	list_for_each_entry(ent, &mga_devs, list) {
		if (ent->pdev == pdev)
			return true;
	}

	return false;
}

static int mga_add_dev(struct pci_dev *pdev,
		       struct miscdevice *misc)
{
	struct mga_dev_entry *ent;

	if (!pdev)
		return -EINVAL;

	ent = kzalloc(sizeof *ent, GFP_KERNEL);
	if (!ent)
		return -ENOMEM;

	INIT_LIST_HEAD(&ent->list);
	ent->pdev = pdev;
	ent->misc = misc;

	list_add(&ent->list, &mga_devs);

	return 0;
}

static int mga_remove_dev(struct pci_dev *pdev,
			  struct miscdevice **ret_misc)
{
	struct mga_dev_entry *ent;

	if (!pdev)
		return -EINVAL;

	list_for_each_entry(ent, &mga_devs, list) {
		if (ent->pdev == pdev) {
			if (ret_misc)
				*ret_misc = ent->misc;
			list_del(&ent->list);
			kfree(ent);
			return 0;
		}
	}

	return -ENODEV;
}

static int mga_get_rom(struct mga_dev *mdev,
		       u8 **ret_rom, size_t *ret_rom_size)
{
	if (!mdev)
		return -EINVAL;

	if (!mdev->rom)
		return -ENODEV;

	*ret_rom = mdev->rom;
	*ret_rom_size = mdev->rom_size;

	return 0;
}

static bool mga_has_rom(struct mga_dev *mdev)
{
	u8 *rom;
	size_t size;

	return !mga_get_rom(mdev, &rom, &size);
}

static int mga_add_rom(struct mga_dev *mdev, u8 *rom, size_t rom_size)
{
	if (!mdev || !rom || !rom_size)
		return -EINVAL;

	mdev->rom = rom;
	mdev->rom_size = rom_size;

	return 0;
}

static int mga_remove_rom(struct mga_dev *mdev,
			  u8 **ret_rom,
			  size_t *ret_rom_size)
{
	if (!mdev)
		return -EINVAL;

	if (!mdev->rom)
		return -ENODEV;

	*ret_rom = mdev->rom;
	*ret_rom_size = mdev->rom_size;

	return 0;
}

static int mga_pci_suspend(struct pci_dev *pdev, pm_message_t state);
static int mga_pci_resume(struct pci_dev *pdev);

static int mga_pins_parse(struct mga_dev *mdev,
			  void __iomem *rom,
			  size_t rom_size)
{
	int ret;
	u8 *ptr;
	size_t i;

	mutex_lock(&mga_mutex);

	if (mga_has_rom(mdev)) {
		pr_err("BUG! SECOND ROM\n");
		mutex_unlock(&mga_mutex);
		return -EBUSY;
	}

	ptr = vmalloc(rom_size);
	if (!ptr) {
		mutex_unlock(&mga_mutex);
		return -ENOMEM;
	}

	for (i = 0; i < rom_size; i++)
		ptr[i] = readb(rom + i);

	ret = mga_add_rom(mdev, ptr, rom_size);
	if (ret)
		vfree(ptr);

	mutex_unlock(&mga_mutex);

	return ret;
}

static void mga_pins_free(struct mga_dev *mdev)
{
	u8 *rom;
	size_t rom_size;

	if (!mga_remove_rom(mdev, &rom, &rom_size))
		vfree(rom);
}

static int mga_open(struct inode *inode, struct file *file)
{
	struct pci_dev *pdev;
	int ret = 0;

	pr_debug("%s\n", __func__);

	mutex_lock(&mga_mutex);

	pdev = mga_get_pci_dev(iminor(inode));
	if (!pdev) {
		ret = -ENODEV;
		file->private_data = NULL;
		goto out;
	}

	if (file->private_data == pdev) {
		ret = -EBUSY;
		goto out;
	}

	file->private_data = pdev;

 out:
	mutex_unlock(&mga_mutex);

	return ret;
}

static int mga_release(struct inode *inode, struct file *file)
{
	pr_debug("%s\n", __func__);

	mutex_lock(&mga_mutex);

	file->private_data = NULL;

	mutex_unlock(&mga_mutex);

	return 0;
}

static ssize_t mga_read(struct file *file, char __user *buf,
			size_t count, loff_t *pos)
{
	struct pci_dev *pdev;
	struct mga_dev *mdev;
	u8 *rom = NULL;
	size_t rom_size = 0;
	ssize_t ret;

	pr_debug("%s\n", __func__);

	mutex_lock(&mga_mutex);

	pdev = file->private_data;
	if (!pdev) {
		ret = -ENODEV;
		goto out;
	}

	mdev = pci_get_drvdata(pdev);

	ret = mga_get_rom(mdev, &rom, &rom_size);
	if (ret)
		goto out;

	if (*pos >= rom_size) {
		ret = 0;
		goto out;
	}

	if (count > rom_size - *pos)
		count = rom_size - *pos;

	if (copy_to_user(buf, rom + *pos, count)) {
		ret = -EFAULT;
		goto out;
	}

	ret = count;
	*pos += count;

 out:
	mutex_unlock(&mga_mutex);

	return ret;
}

enum {
	MGA_OFFSET_MMIO		= 0,
	MGA_OFFSET_MEM		= 32 << 20,
	MGA_OFFSET_ILOAD	= 64 << 20,
	MGA_OFFSET_DMA		= 128 << 20,
};

struct mga_resource {
	u32 index;
	u32 len;
	u64 off;
};

struct mga_chip {
	u32 chip;
	u32 pci_domain_nr;
	u32 pci_bus_nr;
	u32 pci_devfn;
	u16 pci_vendor_id;
	u16 pci_device_id;
	u16 pci_sub_vendor_id;
	u16 pci_sub_device_id;
	u32 pci_class;
};

struct mga_pci_cfg {
	u32 offset;
	u32 value;
};

enum {
	MGA_IOC_PCI_CFG_READL   = _IOWR('M', 1, struct mga_pci_cfg),
	MGA_IOC_PCI_CFG_WRITEL  = _IOW ('M', 2, struct mga_pci_cfg),
	MGA_IOC_SUSPEND		= _IO  ('M', 3),
	MGA_IOC_RESUME		= _IO  ('M', 4),
	MGA_IOC_CHIP		= _IOR ('M', 5, struct mga_chip),
	MGA_IOC_MMIO_RESOURCE	= _IOR ('M', 6, struct mga_resource),
	MGA_IOC_MEM_RESOURCE	= _IOR ('M', 7, struct mga_resource),
	MGA_IOC_ILOAD_RESOURCE	= _IOR ('M', 8, struct mga_resource),
	MGA_IOC_PCI_CFG_READB   = _IOWR('M', 9, struct mga_pci_cfg),
	MGA_IOC_PCI_CFG_WRITEB  = _IOW ('M', 10, struct mga_pci_cfg),
	MGA_IOC_DMA_RESOURCE	= _IOR ('M', 11, struct mga_resource),
	MGA_IOC_CACHE_FLUSH	= _IO  ('M', 12),
	MGA_IOC_IRQ_RESOURCE	= _IOR ('M', 13, u32),
	MGA_IOC_IEN             = _IOW ('M', 14, u32),
	MGA_IOC_ICLEAR          = _IOW ('M', 15, u32),
	MGA_IOC_WAIT_IRQ        = _IO  ('M', 16),
};

static bool mga_irq_pending(struct mga_dev *mdev)
{
	bool ret;

	spin_lock_irq(&mdev->irq.lock);
	ret = mdev->irq.pending;
	spin_unlock_irq(&mdev->irq.lock);

	return ret;
}

static long mga_ioctl(struct file *file,
		      unsigned int cmd, unsigned long arg)
{
	void __user *argp = (void __user *)arg;
	struct pci_dev *pdev;
	struct mga_dev *mdev;
	int ret;

	pr_debug("%s\n", __func__);

	mutex_lock(&mga_mutex);

	pdev = file->private_data;
	if (!pdev) {
		mutex_unlock(&mga_mutex);
		return -ENODEV;
	}

	mdev = pci_get_drvdata(pdev);

	switch (cmd) {
		struct mga_resource res;
		struct mga_chip chip;
		struct mga_pci_cfg pcfg;
		u8 tmp8;
		u32 tmp32;

	case MGA_IOC_SUSPEND:
		ret = mga_pci_suspend(pdev, PMSG_SUSPEND);

		mutex_unlock(&mga_mutex);

		return ret;

	case MGA_IOC_RESUME:
		ret = mga_pci_resume(pdev);

		mutex_unlock(&mga_mutex);

		return ret;

	case MGA_IOC_CHIP:
		chip.chip = mdev->chip;
		chip.pci_domain_nr = pci_domain_nr(pdev->bus);
		chip.pci_bus_nr = pdev->bus->number;
		chip.pci_devfn = pdev->devfn;
		chip.pci_vendor_id = pdev->vendor;
		chip.pci_device_id = pdev->device;
		chip.pci_sub_vendor_id = pdev->subsystem_vendor;
		chip.pci_sub_device_id = pdev->subsystem_device;
		chip.pci_class = (pdev->class << 8) | pdev->revision;

		mutex_unlock(&mga_mutex);

		if (copy_to_user(argp, &chip, sizeof chip))
			return -EFAULT;

		return 0;

	case MGA_IOC_MMIO_RESOURCE:
		res.index = mdev->mmio_bar;
		res.len = pci_resource_len(pdev, res.index);
		res.off = pci_resource_start(pdev, res.index);

		mutex_unlock(&mga_mutex);

		if (copy_to_user(argp, &res, sizeof res))
			return -EFAULT;

		return 0;

	case MGA_IOC_MEM_RESOURCE:
		res.index = mdev->mem_bar;
		res.len = pci_resource_len(pdev, res.index);
		res.off = pci_resource_start(pdev, res.index);

		mutex_unlock(&mga_mutex);

		if (copy_to_user(argp, &res, sizeof res))
			return -EFAULT;

		return 0;

	case MGA_IOC_ILOAD_RESOURCE:
		if (mdev->iload_bar < 0) {
			mutex_unlock(&mga_mutex);
			return -ENXIO;
		}

		res.index = mdev->iload_bar;
		res.len = pci_resource_len(pdev, res.index);
		res.off = pci_resource_start(pdev, res.index);

		mutex_unlock(&mga_mutex);

		if (copy_to_user(argp, &res, sizeof res))
			return -EFAULT;

		return 0;

	case MGA_IOC_DMA_RESOURCE:
		if (!mdev->dma.pages) {
			mutex_unlock(&mga_mutex);
			return -ENXIO;
		}

		res.index = 0xffffffff;
		res.len = mdev->dma.num_pages << PAGE_SHIFT;
		res.off = page_to_phys(mdev->dma.pages);

		mutex_unlock(&mga_mutex);

		if (copy_to_user(argp, &res, sizeof res))
			return -EFAULT;

		return 0;

	case MGA_IOC_PCI_CFG_READL:
		mutex_unlock(&mga_mutex);

		if (copy_from_user(&pcfg, argp, sizeof pcfg)) {
			mutex_unlock(&mga_mutex);
			return -EFAULT;
		}

		ret = pci_read_config_dword(pdev, pcfg.offset, &pcfg.value);
		if (ret) {
			mutex_unlock(&mga_mutex);
			return ret;
		}

		if (copy_to_user(argp, &pcfg, sizeof pcfg))
			return -EFAULT;

		return 0;

	case MGA_IOC_PCI_CFG_WRITEL:
		mutex_unlock(&mga_mutex);

		if (copy_from_user(&pcfg, argp, sizeof pcfg)) {
			mutex_unlock(&mga_mutex);
			return -EFAULT;
		}

		ret = pci_write_config_dword(pdev, pcfg.offset, pcfg.value);

		return ret;

	case MGA_IOC_PCI_CFG_READB:
		mutex_unlock(&mga_mutex);

		if (copy_from_user(&pcfg, argp, sizeof pcfg)) {
			mutex_unlock(&mga_mutex);
			return -EFAULT;
		}

		ret = pci_read_config_byte(pdev, pcfg.offset, &tmp8);
		if (ret) {
			mutex_unlock(&mga_mutex);
			return ret;
		}
		pcfg.value = tmp8;

		if (copy_to_user(argp, &pcfg, sizeof pcfg))
			return -EFAULT;

		return 0;

	case MGA_IOC_PCI_CFG_WRITEB:
		mutex_unlock(&mga_mutex);

		if (copy_from_user(&pcfg, argp, sizeof pcfg)) {
			mutex_unlock(&mga_mutex);
			return -EFAULT;
		}

		ret = pci_write_config_byte(pdev, pcfg.offset, pcfg.value);

		return ret;

	case MGA_IOC_CACHE_FLUSH:
		mutex_unlock(&mga_mutex);

		wbinvd();

		return 0;

	case MGA_IOC_IRQ_RESOURCE:
		tmp32 = mdev->irq.irq;

		mutex_unlock(&mga_mutex);

		if (put_user(tmp32, (u32 __user *)argp))
			return -EFAULT;

		return 0;

	case MGA_IOC_IEN:
		mutex_unlock(&mga_mutex);

		if (get_user(tmp32, (u32 __user *)argp))
			return -EFAULT;

		spin_lock_irq(&mdev->irq.lock);
		if (!mdev->irq.pending)
			mga_write32(mdev, MGA_IEN, tmp32);
		mdev->irq.ien = tmp32;
		spin_unlock_irq(&mdev->irq.lock);

		return 0;

	case MGA_IOC_ICLEAR:
		mutex_unlock(&mga_mutex);

		if (get_user(tmp32, (u32 __user *)argp))
			return -EFAULT;

		spin_lock_irq(&mdev->irq.lock);
		mga_write32(mdev, MGA_ICLEAR, tmp32);
		if (mdev->irq.pending) {
			mga_write32(mdev, MGA_IEN, mdev->irq.ien);
			mdev->irq.pending = false;
		}
		spin_unlock_irq(&mdev->irq.lock);

		return 0;

	case MGA_IOC_WAIT_IRQ:
		mutex_unlock(&mga_mutex);

		return wait_event_interruptible(mdev->irq.wait, mga_irq_pending(mdev));

	default:
		mutex_unlock(&mga_mutex);

		pr_err("Invalid ioctl %x\n", cmd);

		return -EINVAL;
	}
}

static vm_fault_t mga_dma_vm_fault(struct vm_fault *vmf)
{
	struct vm_area_struct *vma = vmf->vma;
	struct mga_dev *mdev;
	unsigned long offset;
	int ret = 0;

	mutex_lock(&mga_mutex);

	mdev = vma->vm_private_data;

	offset = (vmf->address - vma->vm_start) >> PAGE_SHIFT;

	printk("faulting page %lu\n", offset);

	if (offset >= mdev->dma.num_pages) {
		ret = VM_FAULT_SIGBUS;
		goto out;
	}

	vmf->page = &mdev->dma.pages[offset];
	get_page(vmf->page);

 out:
	mutex_unlock(&mga_mutex);

	return ret;
}

static const struct vm_operations_struct mga_dma_vm_ops = {
	.fault = mga_dma_vm_fault,
};

static int mga_mmap(struct file *file, struct vm_area_struct *vma)
{
	struct pci_dev *pdev;
	struct mga_dev *mdev;
	unsigned long off;
	unsigned long len;
	int ret = 0;
	s8 bar = -1;

	pr_debug("%s\n", __func__);

	pdev = file->private_data;
	if (!pdev) {
		ret = -ENODEV;
		goto out;
	}

	mdev = pci_get_drvdata(pdev);

	off = vma->vm_pgoff << PAGE_SHIFT;

	switch (off) {
	case MGA_OFFSET_MMIO:
		bar = mdev->mmio_bar;
		break;
	case MGA_OFFSET_MEM:
		bar = mdev->mem_bar;
		break;
	case MGA_OFFSET_ILOAD:
		if (mdev->iload_bar < 0) {
			ret = -ENXIO;
			goto out;
		}
		bar = mdev->iload_bar;
		break;
	case MGA_OFFSET_DMA:
		if (!mdev->dma.pages) {
			ret = -ENXIO;
			goto out;
		}
		break;
	default:
		ret = -EINVAL;
		goto out;
	}

	if (bar >= 0) {
		off = pci_resource_start(pdev, bar);
		len = pci_resource_len(pdev, bar);

		if (vma->vm_end - vma->vm_start != len) {
			ret = -EINVAL;
			goto out;
		}

		vma->vm_pgoff = off >> PAGE_SHIFT;
		vma->vm_flags |= VM_IO | VM_DONTDUMP | VM_DONTEXPAND;
		vma->vm_page_prot = vm_get_page_prot(vma->vm_flags);

		if (bar == mdev->mem_bar)
			vma->vm_page_prot =
				pgprot_writecombine(vma->vm_page_prot);
		else
			vma->vm_page_prot =
				pgprot_noncached(vma->vm_page_prot);

		if (io_remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,
				       vma->vm_end - vma->vm_start,
				       vma->vm_page_prot)) {
			ret = -EAGAIN;
			goto out;
		}
	} else {
		off = page_to_pfn(mdev->dma.pages);
		len = mdev->dma.num_pages << PAGE_SHIFT;

		if (vma->vm_end - vma->vm_start != len) {
			ret = -EINVAL;
			goto out;
		}

		vma->vm_pgoff = off;
		vma->vm_flags |= VM_DONTDUMP | VM_DONTEXPAND;
		vma->vm_page_prot = vm_get_page_prot(vma->vm_flags);

		vma->vm_ops = &mga_dma_vm_ops;
		vma->vm_private_data = mdev;
	}

 out:
	return ret;
}

static struct file_operations mga_fops = {
	.owner = THIS_MODULE,
	.open = mga_open,
	.release = mga_release,
	.read = mga_read,
	.unlocked_ioctl = mga_ioctl,
	.mmap = mga_mmap,
};

static int mga_fops_register(struct pci_dev *pdev)
{
	struct miscdevice *misc;
	char name[64];
	int ret = -ENOMEM;

	pr_debug("%s\n", __func__);

	if (!pdev)
		return -EINVAL;

	snprintf(name, sizeof name, "mga%04x:%02x:%02x.%d",
		 pci_domain_nr(pdev->bus), pdev->bus->number,
		 PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn));
	name[sizeof name - 1] = '\0';

	misc = kzalloc(sizeof *misc, GFP_KERNEL);
	if (!misc)
		return -ENOMEM;

	misc->minor = MISC_DYNAMIC_MINOR;
	misc->fops = &mga_fops;
	misc->name = kstrdup(name, GFP_KERNEL);
	if (!misc->name) {
		kfree(misc);
		return -ENOMEM;
	}

	mutex_lock(&mga_mutex);

	if (mga_has_dev(pdev)) {
		pr_err("BUG! SECOND DEV\n");
		ret = -EBUSY;
		goto out;
	}

	ret = mga_add_dev(pdev, misc);
	if (ret)
		goto out;

	mutex_unlock(&mga_mutex);

	ret = misc_register(misc);
	if (ret) {
		mutex_lock(&mga_mutex);
		mga_remove_dev(pdev, NULL);
		goto out;
	}

	return 0;

 out:
	mutex_unlock(&mga_mutex);

	kfree(misc->name);

	kfree(misc);

	return ret;
}

static void mga_fops_deregister(struct pci_dev *pdev)
{
	int ret;
	struct miscdevice *misc;

	pr_debug("%s\n", __func__);

	if (!pdev)
		return;

	mutex_lock(&mga_mutex);

	ret = mga_remove_dev(pdev, &misc);
	if (ret)
		goto out;

	mutex_unlock(&mga_mutex);

	misc_deregister(misc);

	kfree(misc->name);
	kfree(misc);

	return;

 out:
	mutex_unlock(&mga_mutex);
}
