/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "mga_regs.h"
#include "mga_kms.h"
#include "mga_g450_dac.h"
#include "tvo_g450.h"

struct mga_crtc_state1 {
	struct drm_framebuffer *fb;
	struct drm_display_mode mode;
	unsigned int pll;
	unsigned int outputs;
	unsigned int pixclk;
	int hscale;
	int vscale;
	int x;
	int y;
};

struct mga_mode_state1 {
	struct mga_crtc_state1 crtc1;
	struct mga_crtc_state1 crtc2;
};

static struct mga_mode_state1 mdev_crtcs;

static void dac1_enable(struct mga_dev *mdev, unsigned int crtc)
{
	mga_crtc2_dac_source(mdev, crtc);
	mga_g450_dac_dac1_source(mdev, crtc);

	mga_dac_dac_power(mdev, true);
}

static void dac1_disable(struct mga_dev *mdev)
{
	mga_dac_dac_power(mdev, false);

	mga_g450_dac_dac1_source(mdev, MGA_CRTC_NONE);
	mga_crtc2_dac_source(mdev, MGA_CRTC_NONE);
}

static void dac2_enable(struct mga_dev *mdev, unsigned int crtc)
{
	mga_g450_dac_dac2_source(mdev, crtc, false);

	mga_g450_dac_dac2_power(mdev, true);
}

static void dac2_disable(struct mga_dev *mdev)
{
	mga_g450_dac_dac2_power(mdev, false);

	mga_g450_dac_dac2_source(mdev, MGA_CRTC_NONE, false);
}

static void panel_enable(struct mga_dev *mdev, unsigned int crtc,
			 unsigned int pll, bool bt656)
{
	mga_g450_dac_panel_source(mdev, crtc, bt656);
	mga_g450_dac_panel_power(mdev, true);
	mga_g450_dac_dvi_clock(mdev, crtc, pll, false);
	mga_g450_dac_dvi_pipe(mdev, pll, 0x00);
}

static void panel_disable(struct mga_dev *mdev)
{
	mga_g450_dac_dvi_clock(mdev, MGA_CRTC_NONE, MGA_PLL_NONE, false);
	mga_g450_dac_panel_power(mdev, false);
	mga_g450_dac_panel_source(mdev, MGA_CRTC_NONE, false);
}

static void tvo_enable(struct mga_dev *mdev, unsigned int crtc)
{
	mga_g450_dac_rset(mdev, MGA_RSET_1_0_V);
	mga_g450_dac_crtc2_power(mdev, true);
	mga_g450_dac_dac2_power(mdev, true);
	mga_g450_dac_dac2_source(mdev, crtc, true);
	// scart
	if (0) {
		mga_g450_dac_dac2_set_sync(mdev,
					   DRM_MODE_FLAG_PHSYNC | DRM_MODE_FLAG_PVSYNC);
		mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_ON);
	}
}

static void tvo_disable(struct mga_dev *mdev)
{
	mga_g450_tvo_disable(&mdev->tvodev_g450);
	mga_g450_dac_dac2_source(mdev, MGA_CRTC_NONE, false);
	mga_g450_dac_rset(mdev, MGA_RSET_0_7_V);
}

static void crtc1_enable(struct mga_dev *mdev,
			 const struct mga_mode_config *mc,
			 unsigned int pll,
			 unsigned int pixclk)
{
	const struct mga_plane_config *pc = &mc->crtc1.plane_config;
	const struct mga_crtc_config *cc = &mc->crtc1.crtc_config;
	const struct mga_crtc1_regs *regs = &mc->crtc1.regs;
	const struct drm_framebuffer *fb = pc->fb;
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	bool overlay = cc->overlay;
	unsigned int outputs = cc->outputs;
	unsigned int hzoom = 0x10000 / pc->hscale;
	/* FIXME sync from mode */
	unsigned int sync = DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_NVSYNC;

	mga_crtc1_restore(mdev, regs);
	mga_dac_crtc1_program(mdev, &mc->dac_config);
	mga_dac_crtc1_pixclk_select(mdev, pll);

	/*
	 * Leave CRTC sync sync polarity settings alone as all
	 * outputs have independent sync polarity settings.
	 */
	mga_misc_set_sync(mdev, DRM_MODE_FLAG_PHSYNC | DRM_MODE_FLAG_PVSYNC);

	if (outputs & MGA_OUTPUT_DAC1) {
		/* FIXME check which outputs/CRTCs are affected */
		mga_dac_dac_set_sync(mdev, sync);
		mga_g450_dac_dac1_set_sync(mdev, sync);
		dac1_enable(mdev, MGA_CRTC_CRTC1);
	}

	if (outputs & MGA_OUTPUT_DAC2) {
		mga_g450_dac_dac2_set_sync(mdev, sync);
		dac2_enable(mdev, MGA_CRTC_CRTC1);
	}

	if (outputs & MGA_OUTPUT_TMDS1) {
		mga_dac_panel_set_sync(mdev, sync);
		panel_enable(mdev, MGA_CRTC_CRTC1, pll, false);
	}

	mga_dac_crtc1_power(mdev, true);
	mga_dac_crtc1_set_palette(mdev);
	mga_dac_crtc1_pixclk_enable(mdev, true);
	if (outputs & MGA_OUTPUT_TMDS1) {
		udelay(10);
		mga_g450_dac_dvi_clock(mdev, MGA_CRTC_CRTC1, pll, true);//bb
	}

	mga_crtc1_dpms(mdev, DRM_MODE_DPMS_ON);

	if (outputs & MGA_OUTPUT_TMDS1) {
		mga_g450_dac_panel_mode(mdev, pixclk);
		mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_ON);
	}

	if (outputs & MGA_OUTPUT_DAC2)
		mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_ON);

	if (outputs & MGA_OUTPUT_DAC1)
		mga_g450_dac_dac1_dpms(mdev, DRM_MODE_DPMS_ON);

	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, true);
}

static void crtc1_disable(struct mga_dev *mdev)
{
	unsigned int outputs = mdev_crtcs.crtc1.outputs;

	if (!outputs)
		return;

	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);

	if (outputs & MGA_OUTPUT_DAC1)
		mga_g450_dac_dac1_dpms(mdev, DRM_MODE_DPMS_OFF);

	if (outputs & MGA_OUTPUT_DAC2)
		mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_OFF);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_OFF);

	mga_crtc1_dpms(mdev, DRM_MODE_DPMS_OFF);

	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	mga_dac_crtc1_power(mdev, false);

	if (outputs & MGA_OUTPUT_DAC1)
		dac1_disable(mdev);

	if (outputs & MGA_OUTPUT_DAC2)
		dac2_disable(mdev);

	if (outputs & MGA_OUTPUT_TMDS1)
		panel_disable(mdev);

	/* FIXME reset sync settings? */

	mdev_crtcs.crtc1.pixclk = 0;
	mdev_crtcs.crtc1.outputs = 0;
	mdev_crtcs.crtc1.pll = 0;
	memset(&mdev_crtcs.crtc1.mode, 0, sizeof mdev_crtcs.crtc1.mode);
	mdev_crtcs.crtc1.fb = NULL;
}

static void crtc2_enable(struct mga_dev *mdev,
			 const struct mga_mode_config *mc,
			 unsigned int pll,
			 unsigned int pixclk)
{
	const struct mga_plane_config *pc = &mc->crtc2.plane_config;
	const struct mga_crtc_config *cc = &mc->crtc2.crtc_config;
	const struct mga_crtc2_regs *regs = &mc->crtc2.regs;
	const struct drm_framebuffer *fb = pc->fb;
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	bool bt656 = cc->bt656;
	unsigned int outputs = cc->outputs;
	/* FIXME sync from mode */
	unsigned int sync = DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_NVSYNC;

	mga_crtc2_restore(mdev, regs);
	mga_crtc2_pixclk_select(mdev, pll);

	/*
	 * Leave CRTC sync sync polarity settings alone as all
	 * outputs have independent sync polarity settings.
	 */
	mga_crtc2_set_sync(mdev, DRM_MODE_FLAG_PHSYNC | DRM_MODE_FLAG_PVSYNC);

	if (outputs & MGA_OUTPUT_DAC1) {
		mga_g450_dac_dac1_set_sync(mdev, sync);
		dac1_enable(mdev, MGA_CRTC_CRTC2);
	}

	if (outputs & MGA_OUTPUT_DAC2) {
		mga_g450_dac_dac2_set_sync(mdev, sync);
		dac2_enable(mdev, MGA_CRTC_CRTC2);
	}

	if (outputs & MGA_OUTPUT_TMDS1) {
		mga_dac_panel_set_sync(mdev, sync);
		panel_enable(mdev, MGA_CRTC_CRTC2, pll, bt656);
	}

	if (outputs & MGA_OUTPUT_TVOUT) {
		tvo_enable(mdev, MGA_CRTC_CRTC2);
		mga_g450_tvo_disable(&mdev->tvodev_g450);
	}

	mga_g450_dac_crtc2_power(mdev, true);
	mga_crtc2_video(mdev, true);
	mga_crtc2_pixclk_enable(mdev, true);
	if (outputs & MGA_OUTPUT_TMDS1) {
		udelay(10);
		mga_g450_dac_dvi_clock(mdev, MGA_CRTC_CRTC2, pll, true);//bb
	} else if (outputs & MGA_OUTPUT_TMDS2) {
		udelay(10);
		mga_g450_dac_dvi_clock(mdev, MGA_CRTC_CRTC1 | MGA_CRTC_CRTC2, pll, true);//bb
	}

	if (outputs & MGA_OUTPUT_TMDS1) {
		mga_g450_dac_panel_mode(mdev, pixclk);
		mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_ON);
	}

	if (outputs & MGA_OUTPUT_DAC2)
		mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_ON);

	if (outputs & MGA_OUTPUT_DAC1)
		mga_g450_dac_dac1_dpms(mdev, DRM_MODE_DPMS_ON);

	if (outputs & MGA_OUTPUT_TVOUT) {
		mga_g450_tvo_program_bt656(&mdev->tvodev_g450,
					   &mc->tvo_config,
					   mode, fb->pixel_format);
		mga_crtc2_restore_interlace(mdev, regs);
		mga_g450_tvo_enable_bt656(&mdev->tvodev_g450);
	} else if (bt656)
		mga_crtc2_restore_interlace(mdev, regs);
}

static void crtc2_disable(struct mga_dev *mdev)
{
	unsigned int outputs = mdev_crtcs.crtc2.outputs;

	if (!outputs)
		return;

	mga_crtc2_wait_vblank(mdev);
	mga_crtc2_video(mdev, false);

	if (outputs & MGA_OUTPUT_DAC1)
		mga_g450_dac_dac1_dpms(mdev, DRM_MODE_DPMS_OFF);

	if (outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
		mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_OFF);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_OFF);

	mga_crtc2_dpms(mdev, DRM_MODE_DPMS_OFF);

	mga_crtc2_pixclk_enable(mdev, false);
	mga_crtc2_pixclk_select(mdev, MGA_PLL_NONE);

	mga_g450_dac_crtc2_power(mdev, false);

	if (outputs & MGA_OUTPUT_DAC1)
		dac1_disable(mdev);

	if (outputs & MGA_OUTPUT_DAC2)
		dac2_disable(mdev);

	if (outputs & MGA_OUTPUT_TMDS1)
		panel_disable(mdev);

	if (outputs & MGA_OUTPUT_TVOUT)
		tvo_disable(mdev);

	mdev_crtcs.crtc2.pixclk = 0;
	mdev_crtcs.crtc2.outputs = 0;
	mdev_crtcs.crtc2.pll = 0;
	memset(&mdev_crtcs.crtc2.mode, 0, sizeof mdev_crtcs.crtc2.mode);
	mdev_crtcs.crtc2.fb = NULL;
}

static unsigned int pick_first_pll(unsigned int plls)
{
	int i;

	if (!plls)
		return 0;

	for (i = 0; i < 31; i++) {
		unsigned int pll = plls & (1 << i);
		if (pll)
			return pll;
	}

	return 0;
}

int mga_g450_set_mode(struct mga_dev *mdev,
		      struct mga_mode_config *mc)
{
	struct mga_dac_pll_settings pll_settings[2][MGA_PLL_SETTINGS_MAX];
	unsigned int pll_count[2];
	unsigned int pll[2] = { MGA_PLL_NONE, MGA_PLL_NONE };
	unsigned int pixclk[2] = { 0, 0 };
	unsigned int plls;
	int ret;
	unsigned int cpp[2] = { 0, 0 };
	struct mga_crtc1_regs *c1regs = &mc->crtc1.regs;
	struct mga_crtc2_regs *c2regs = &mc->crtc2.regs;
	bool changed[2] = {};
	struct mga_plane_config *pc[2] = {
		[0] = &mc->crtc1.plane_config,
		[1] = &mc->crtc2.plane_config,
	};
	struct mga_crtc_config *cc[2] = {
		[0] = &mc->crtc1.crtc_config,
		[1] = &mc->crtc2.crtc_config,
	};

	/* all or nothing! */
	if (cc[0]->mode_valid) {
		if (!cc[0]->outputs || !pc[0]->fb)
			return -EINVAL;
		cpp[0] = drm_format_plane_cpp(pc[0]->fb->pixel_format, 0);
	} else {
		if (cc[0]->outputs || pc[0]->fb)
			return -EINVAL;
	}
	if (cc[1]->mode_valid) {
		if (!cc[1]->outputs || !pc[1]->fb)
			return -EINVAL;
		cpp[1] = drm_format_plane_cpp(pc[1]->fb->pixel_format, 0);
	} else {
		if (cc[1]->outputs || pc[1]->fb)
			return -EINVAL;
	}

	/* CRTC1 can drive anything except TMDS2 and TV-out */
	if (cc[0]->outputs & ~(MGA_OUTPUT_DAC1 | MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS1))
		return -EINVAL;
	/* CRTC2 can drive anything */
	if (cc[1]->outputs & ~(MGA_OUTPUT_DAC1 | MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS1 |
			   MGA_OUTPUT_TMDS2 | MGA_OUTPUT_TVOUT))
		return -EINVAL;

	/* the same output can't be fed from multiple CRTCs */
	if (cc[0]->outputs & cc[1]->outputs)
		return -EINVAL;

	/* FIXME simplify to make sure only one output per connector is active. */
	/* VGA1 and DVI1 mutually exclusive */
	if ((cc[0]->outputs | cc[1]->outputs) & MGA_OUTPUT_DAC1 &&
	    (cc[0]->outputs | cc[1]->outputs) & MGA_OUTPUT_TMDS1)
		return -EINVAL;
	/* VGA2 and DVI2 mutually exclusive */
	if ((cc[0]->outputs | cc[1]->outputs) & MGA_OUTPUT_DAC2 &&
	    (cc[0]->outputs | cc[1]->outputs) & MGA_OUTPUT_TMDS2)
		return -EINVAL;
	/* DAC2 and TV-out mutually exclusive */
	if ((cc[0]->outputs | cc[1]->outputs) & MGA_OUTPUT_DAC2 &&
	    (cc[0]->outputs | cc[1]->outputs) & MGA_OUTPUT_TVOUT)
		return -EINVAL;
	/* TMDS2 and TV-out mutually exclusive */
	if ((cc[0]->outputs | cc[1]->outputs) & MGA_OUTPUT_TMDS2 &&
	    (cc[0]->outputs | cc[1]->outputs) & MGA_OUTPUT_TVOUT)
		return -EINVAL;

	/* check for board features */
	if ((cc[0]->outputs | cc[1]->outputs) & ~mdev->outputs)
		return -ENODEV;

	// FIXME calc modes before determining pixclk
	if (cc[0]->mode_valid) {
		ret = mga_crtc1_calc_mode(mdev, pc[0], cc[0], c1regs);
		if (ret)
			return ret;

		pixclk[0] = mga_calc_pixel_clock(&cc[0]->adjusted_mode, 1);
		if (!pixclk[0])
			return -EINVAL;
	}
	if (cc[1]->mode_valid) {
		/*
		 * FIXME figure out BT.656 panel mode.
		 * Initial testing hasn't been positive.
		 */
		if (cc[1]->outputs == MGA_OUTPUT_TVOUT)
			cc[1]->bt656 = true;

		ret = mga_crtc2_calc_mode(mdev, pc[1], cc[1], c2regs);
		if (ret)
			return ret;

		pixclk[1] = mga_calc_pixel_clock(&cc[1]->adjusted_mode, 1);
		if (!pixclk[1])
			return -EINVAL;
	}

	ret = mga_g400_hipri(mdev, mdev->mclk, pixclk[0], pixclk[1],
			     cpp[0] * 8, cpp[1] * 8, cc[1]->bt656, c1regs, c2regs);
	if (ret)
		return ret;

	/* DVI2 can only be enabled when DVI1 is enabled */
	/*
	 * FIXME probably not really the case since we could
	 * enabled CRTC2 DVI1 and DVI2 output at the same time
	 * and let CRTC1 drive DAC1 at the same time. need to try it.
	 */
	/* a bit of a hack, make CRTC2 drive both TMDS outputs,
	 * even though only TMDS2 will be used */
	if (cc[1]->outputs & MGA_OUTPUT_TMDS2 && !(cc[0]->outputs & MGA_OUTPUT_TMDS1))
		cc[1]->outputs |= MGA_OUTPUT_TMDS1;

	dev_dbg(mdev->dev, "FOOaaaaa\n");
	if (cc[1]->outputs & MGA_OUTPUT_TMDS2 && cc[0]->outputs & MGA_OUTPUT_TMDS1) {
		/* must have identical modes */
		/* This check must be done after the modes have been adjusted! */
		/* FIXME do we need identical modes, or just the same pixclk? */
		if (!drm_mode_equal(&cc[0]->adjusted_mode, &cc[1]->adjusted_mode))
			return -EINVAL;
	}
	dev_dbg(mdev->dev, "FOOaaaaa2\n");

	/*
	 * FIXME write some sane PLL selection algo,
	 * it should make lists of possible PLLs for each
	 * CRTC, and pick from those PLLs for each CRTC.
	 * It should also try to keep the already made  PLL
	 * selections, to avoid disturbing other heads while
	 * modesetting on some other one.
	 * If the clock is identical and DVI is not used it
	 * could even favor to share the same PLL for both heads.
	 */

	/* possible PLLs for each CRTC */
	pll[0] = MGA_PLL_PIXPLL | MGA_PLL_VIDPLL;
	pll[1] = MGA_PLL_PIXPLL | MGA_PLL_VIDPLL | MGA_PLL_SYSPLL;

	/* don't touch PLLs used for system clocks */
	pll[0] &= ~mdev->sysclk_plls;
	pll[1] &= ~mdev->sysclk_plls;

	/* FIXME can we use cristal in other cases? */
	if (cc[1]->bt656 && cc[1]->outputs != MGA_OUTPUT_TVOUT && pixclk[1] == 13500)
		pll[1] = MGA_PLL_CRISTAL;

	/* no need for a PLL if no mode is set */
	if (!cc[0]->outputs)
		pll[0] = 0;
	if (!cc[1]->outputs)
		pll[1] = 0;

	/* CRTC2 doesn't need a PLL if one PLL drives both outputs (dual-DVI mode) */
	if (cc[0]->outputs == MGA_OUTPUT_TMDS1 && cc[1]->outputs == MGA_OUTPUT_TMDS2)
		pll[1] = 0;

	if (cc[0]->outputs != mdev_crtcs.crtc1.outputs ||
	    pixclk[0] != mdev_crtcs.crtc1.pixclk ||
	    pc[0]->fb != mdev_crtcs.crtc1.fb ||
	    (cc[0]->mode_valid && !drm_mode_equal(&cc[0]->adjusted_mode, &mdev_crtcs.crtc1.mode)) ||
	    pc[0]->hscale != mdev_crtcs.crtc1.hscale ||
	    pc[0]->vscale != mdev_crtcs.crtc1.vscale ||
	    pc[0]->src.x1 != mdev_crtcs.crtc1.x ||
	    pc[0]->src.y1 != mdev_crtcs.crtc1.y)
		changed[0] = true; /* full modeset when outputs/pixclk/fb/mode change */
	if (cc[1]->outputs != mdev_crtcs.crtc2.outputs ||
	    pixclk[1] != mdev_crtcs.crtc2.pixclk ||
	    pc[1]->fb != mdev_crtcs.crtc2.fb ||
	    (cc[1]->mode_valid && !drm_mode_equal(&cc[1]->adjusted_mode, &mdev_crtcs.crtc2.mode)) ||
	    pc[1]->src.x1 != mdev_crtcs.crtc1.x ||
	    pc[1]->src.y1 != mdev_crtcs.crtc1.y)
		changed[1] = true; /* full modeset when outputs/pixclk/fb/mode change */

	/* need to change CRTC1 PLL? */
	if ((pll[0] || mdev_crtcs.crtc1.pll) && !(pll[0] & mdev_crtcs.crtc1.pll))
		changed[0] = true;
	/* need to change CRTC2 PLL? */
	if ((pll[1] || mdev_crtcs.crtc2.pll) && !(pll[1] & mdev_crtcs.crtc2.pll))
		changed[1] = true;

	if (!!mdev_crtcs.crtc1.pll != !!pll[0])
		changed[0] = true;
	if (!!mdev_crtcs.crtc2.pll != !!pll[1])
		changed[1] = true;

	if (!changed[0] && !changed[1]) {
		pll[0] = mdev_crtcs.crtc1.pll;
		pll[1] = mdev_crtcs.crtc2.pll;
	} else if (!changed[0]) {
		unsigned int plls = pll[1] & ~mdev_crtcs.crtc1.pll;
		if (plls)
			pll[1] = pick_first_pll(plls);
		else
			changed[0] = true;
	} else if (!changed[1]) {
		unsigned int plls = pll[0] & ~mdev_crtcs.crtc2.pll;
		if (plls)
			pll[0] = pick_first_pll(plls);
		else
			changed[1] = true;
	}

	dev_dbg(mdev->dev, "FFF\n");

	if (changed[0] || changed[1]) {
		if (!pll[0])
			pll[1] = pick_first_pll(pll[1]);
		else if (!pll[1])
			pll[0] = pick_first_pll(pll[0]);
		else {
			int i, j;

			for (i = 0; i < 31; i++) {
				unsigned int c1pll = pll[0] & (1 << i);
				if (!c1pll)
					continue;
				for (j = 0; j < 31; j++) {
					unsigned int c2pll;
					if (i == j)
						continue;

					c2pll = pll[1] & (1 << j);
					if (!c2pll)
						continue;

					pll[0] = c1pll;
					pll[1] = c2pll;
					goto plls_selected;
				}
			}
		}
	}
 plls_selected:

	if (pll[0] && pll[0] == pll[1])
		return -EINVAL;

	if (pll[0] != mdev_crtcs.crtc1.pll)
		changed[0] = true;
	if (pll[1] != mdev_crtcs.crtc2.pll)
		changed[1] = true;

	/* FIXME CRTC/DAC max clock */
	switch (pll[0]) {
	case MGA_PLL_PIXPLL:
		ret = mga_g450_dac_pixpllc_calc(mdev, pixclk[0], 0, pll_settings[0], &pll_count[0]);
		break;
	case MGA_PLL_VIDPLL:
		ret = mga_g450_dac_vidpll_calc(mdev, pixclk[0], 0, pll_settings[0], &pll_count[0]);
		break;
	case MGA_PLL_NONE:
		if (cc[0]->outputs & MGA_OUTPUT_TMDS2)
			break;
		/* fall through */
	default:
		BUG_ON(cc[0]->outputs);
		ret = 0;
	}
	if (ret)
		return ret;

	/* FIXME CRTC/DAC max clock */
	switch (pll[1]) {
	case MGA_PLL_PIXPLL:
		ret = mga_g450_dac_pixpllc_calc(mdev, pixclk[1], 0, pll_settings[1], &pll_count[1]);
		break;
	case MGA_PLL_SYSPLL:
		ret = mga_g450_dac_syspll_calc(mdev, pixclk[1], 0, pll_settings[1], &pll_count[1]);
		break;
	case MGA_PLL_VIDPLL:
		ret = mga_g450_dac_vidpll_calc(mdev, pixclk[1], 0, pll_settings[1], &pll_count[1]);
		break;
	case MGA_PLL_CRISTAL:
		dev_dbg(mdev->dev, "CRTC2 using cristal frequency\n");
		ret = 0;
		break;
	case MGA_PLL_NONE:
		if (cc[1]->outputs & MGA_OUTPUT_TMDS2)
			break;
		/* fall through */
	default:
		BUG_ON(cc[1]->outputs);
		ret = 0;
	}
	if (ret)
		return ret;

	if (!changed[0] && !changed[1])
		goto exit;

	if (changed[0])
		crtc1_disable(mdev);

	if (changed[1])
		crtc2_disable(mdev);

	if (changed[0] && pll[0]) {
		switch (pll[0]) {
		case MGA_PLL_PIXPLL:
			mga_dac_pixpll_power(mdev, true);
			ret = mga_g450_dac_pixpllc_program(mdev, pll_settings[0], pll_count[0]);
			break;
		case MGA_PLL_VIDPLL:
			mga_g450_dac_vidpll_power(mdev, true);
			ret = mga_g450_dac_vidpll_program(mdev, pll_settings[0], pll_count[0]);
			break;
		default:
			BUG_ON(cc[0]->outputs);
		}
		if (ret) {
			// FIXME restore previous mode?
			goto exit;
		}
	}

	if (changed[1] && pll[1]) {
		switch (pll[1]) {
		case MGA_PLL_PIXPLL:
			mga_dac_pixpll_power(mdev, true);
			ret = mga_g450_dac_pixpllc_program(mdev, pll_settings[1], pll_count[1]);
			break;
		case MGA_PLL_SYSPLL:
			mga_dac_syspll_power(mdev, true);
			ret = mga_g450_dac_syspll_program(mdev, pll_settings[1], pll_count[1]);
			break;
		case MGA_PLL_VIDPLL:
			mga_g450_dac_vidpll_power(mdev, true);
			ret = mga_g450_dac_vidpll_program(mdev, pll_settings[1], pll_count[1]);
			break;
		case MGA_PLL_CRISTAL:
			ret = 0;
			break;
		default:
			BUG_ON(cc[1]->outputs);
		}
		if (ret) {
			// FIXME restore previous mode?
			goto exit;
		}
	}

	// FIXME sysclks?

	if (changed[0] && cc[0]->mode_valid)
		crtc1_enable(mdev, mc, pll[0], pixclk[0]);

	if (changed[1] && cc[1]->mode_valid)
		crtc2_enable(mdev, mc, pll[1], pixclk[1]);

	if ((changed[0] || changed[1]) &&
	    cc[0]->outputs & MGA_OUTPUT_TMDS1 &&
	    cc[1]->outputs & MGA_OUTPUT_TMDS2) {
		u8 val = 0x30;

		if (cpp[1] == 4)
			val |= 0xd;
		else
			val |= 0xa;

		mga_crtc1_enable_vidrst(mdev, true);
		mga_crtc2_enable_vidrst(mdev, true);

		mga_crtc1_wait_vblank(mdev);

		mga_g450_dac_dvi_pipe(mdev, pll[0] | pll[1], val);

		mga_wait(mdev);

		val &= ~0x10;
		mga_g450_dac_dvi_pipe(mdev, pll[0] | pll[1], val);
	} else if (changed[1] && cc[1]->outputs & MGA_OUTPUT_TMDS2) {
		u8 val = 0x20;

		if (cpp[1] == 4)
			val |= 0xd;
		else
			val |= 0xa;

		/* FIXME test the combo w/ CRTC1->DAC2 CRTC2->TMDS2 */
		/* FIXME need to set up DVI stuff in crtc2_enable instead... */
		/* FIXME need to set up DVICLK stuff only for CRTC2 */
		mga_g450_dac_dvi_pipe(mdev, pll[0] | pll[1], val);
	}

	if (cc[1]->outputs & MGA_OUTPUT_TVOUT) {
		//mga_g450_tvo_program_bt656(&mdev->tvodev, cc[1]->adjusted_mode, pc[1]->fb);
		//mga_g450_tvo_enable_bt656(&mdev->tvodev);
		//mga_crtc2_interlace(mdev);
	}

	// disp start
	// zoom

	mdev_crtcs.crtc1.fb = pc[0]->fb;
	if (cc[0]->mode_valid)
		mdev_crtcs.crtc1.mode = cc[0]->adjusted_mode;
	else
		memset(&mdev_crtcs.crtc1.mode, 0, sizeof mdev_crtcs.crtc1.mode);
	mdev_crtcs.crtc1.pll = pll[0];
	mdev_crtcs.crtc1.outputs = cc[0]->outputs;
	mdev_crtcs.crtc1.pixclk = pixclk[0];
	mdev_crtcs.crtc1.hscale = pc[0]->hscale;
	mdev_crtcs.crtc1.vscale = pc[0]->vscale;
	mdev_crtcs.crtc1.x = pc[0]->src.x1;
	mdev_crtcs.crtc1.y = pc[0]->src.y1;

	mdev_crtcs.crtc2.fb = pc[1]->fb;
	if (cc[1]->mode_valid)
		mdev_crtcs.crtc2.mode = cc[1]->adjusted_mode;
	else
		memset(&mdev_crtcs.crtc2.mode, 0, sizeof mdev_crtcs.crtc2.mode);
	mdev_crtcs.crtc2.pll = pll[1];
	mdev_crtcs.crtc2.outputs = cc[1]->outputs;
	mdev_crtcs.crtc2.pixclk = pixclk[1];
	mdev_crtcs.crtc2.hscale = pc[1]->hscale;
	mdev_crtcs.crtc2.vscale = pc[1]->vscale;
	mdev_crtcs.crtc2.x = pc[1]->src.x1;
	mdev_crtcs.crtc2.y = pc[1]->src.y1;

 exit:
	// FIXME hipri
	// FIXME patch pll?

	/* power down unused PLLs */
	plls = mdev->sysclk_plls | mdev_crtcs.crtc1.pll | mdev_crtcs.crtc2.pll;

	if (mdev->enabled_plls & MGA_PLL_PIXPLL && !(plls & MGA_PLL_PIXPLL))
		mga_dac_pixpll_power(mdev, false);
	if (mdev->enabled_plls & MGA_PLL_SYSPLL && !(plls & MGA_PLL_SYSPLL))
		mga_dac_syspll_power(mdev, false);
	if (mdev->enabled_plls & MGA_PLL_VIDPLL && !(plls & MGA_PLL_VIDPLL))
		mga_g450_dac_vidpll_power(mdev, false);

	return 0;
}
