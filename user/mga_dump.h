/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_DUMP_H
#define MGA_DUMP_H

#include "mga_kernel_api.h"
#include "kernel_emul.h"
#include "i2c.h"
#include "device.h"
#include "spinlock.h"
#include "wait.h"

#include "tvp3026.h"
#include "pcf8574.h"
#include "av9110.h"

struct mga_dev;

#include "mga_dac_common.h"
#include "mga_dac.h"
#include "tvo.h"
#include "tvo_g450.h"
#include "drm_kms.h"

#include "mga_pins.h"
#include "mga_kms.h"

#define to_mga_device(dev) container_of((dev), struct mga_dev, base)
#define to_mga_framebuffer(fb) container_of((fb), struct mga_framebuffer, base)

enum {
	MGA_FEAT_TVO_BUILTIN      = 0x0001,
	MGA_FEAT_TVO_ADDON        = 0x0002,
	MGA_FEAT_TMDS_BUILTIN     = 0x0004,
	MGA_FEAT_TMDS_ADDON       = 0x0008,
	MGA_FEAT_DUALHEAD_BUILTIN = 0x0010,
	MGA_FEAT_DUALHEAD_ADDON   = 0x0020,
	MGA_FEAT_DVD_BUILTIN      = 0x0040,
	MGA_FEAT_DVD_ADDON        = 0x0080,
	MGA_FEAT_MJPEG_BUILTIN    = 0x0100,
	MGA_FEAT_MJPEG_ADDON      = 0x0200,
	MGA_FEAT_VIN_BUILTIN      = 0x0400,
	MGA_FEAT_VIN_ADDON        = 0x0800,
	MGA_FEAT_TUNER_BUILTIN    = 0x1000,
	MGA_FEAT_TUNER_ADDON      = 0x2000,
	MGA_FEAT_AUDIO_BUILTIN    = 0x4000,
	MGA_FEAT_AUDIO_ADDON      = 0x8000,

	MGA_FEAT_ADDON            = 0x10000,
};

enum {
	MGA_CRTC_NONE  = 0x0,
	MGA_CRTC_CRTC1 = 0x1,
	MGA_CRTC_CRTC2 = 0x2,
};

enum {
	MGA_PLANE_NONE   = 0x0,
	MGA_PLANE_CRTC1  = 0x1,
	MGA_PLANE_CURSOR = 0x2,
	MGA_PLANE_BES    = 0x4,
	MGA_PLANE_CRTC2  = 0x8,
	MGA_PLANE_SPIC   = 0x10,
};

enum mga_pll {
	MGA_PLL_NONE    = 0x00,
	MGA_PLL_PIXPLL  = 0x01,
	MGA_PLL_SYSPLL  = 0x02,
	MGA_PLL_VIDPLL  = 0x04,
	MGA_PLL_CRISTAL = 0x08,
	MGA_PLL_TVO     = 0x10,
	MGA_PLL_AV9110  = 0x20,
};

enum {
	MGA_OUTPUT_NONE  = 0x00,
	MGA_OUTPUT_DAC1  = 0x01,
	MGA_OUTPUT_DAC2  = 0x02,
	MGA_OUTPUT_TMDS1 = 0x04,
	MGA_OUTPUT_TMDS2 = 0x08,
	MGA_OUTPUT_TVOUT = 0x10,
};

struct mga_i2c_channel {
	struct i2c_adapter adap;
	struct i2c_algo_bit_data algo;
};

struct mga_dev;
struct drm_display_mode;
struct drm_framebuffer;

struct mga_framebuffer {
	struct drm_framebuffer base;
	unsigned int size;
	u32 bus;
	void __iomem *virt;
	struct list_head head;
	unsigned int possible_planes;
	bool cursor;
};

struct mga_crtc_state {
	bool mode_valid;
	struct drm_framebuffer *fb;
	struct drm_display_mode user_mode;
	struct drm_display_mode mode;
	unsigned int outputs;
	unsigned int hscale;
	unsigned int vscale;
	unsigned int x, y;
};

struct mga_mode_state {
	struct mga_crtc_state crtc1;
	struct mga_crtc_state crtc2;
	struct {
		struct drm_framebuffer *fb;
	} cursor;
	struct {
		struct drm_framebuffer *fb;
	} bes;
	struct {
		struct drm_framebuffer *fb;
	} spic;
};

struct mga_chip_funcs {
	void (*rfhcnt)(struct mga_dev *mdev, int mclk);
	void (*monitor_sense_dac1)(struct mga_dev *mdev);
	void (*monitor_sense_dac2)(struct mga_dev *mdev);
	int (*suspend)(struct mga_dev *mdev);
	int (*resume)(struct mga_dev *mdev);
	void (*init)(struct mga_dev *mdev);
	void (*test)(struct mga_dev *mdev);
	int (*set_crtc1_mode)(struct mga_dev *mdev, unsigned int pixel_format, struct drm_display_mode *m, unsigned int outputs);
	int (*set_crtc2_mode)(struct mga_dev *mdev, unsigned int pixel_format, struct drm_display_mode *m, unsigned int outputs);
	int (*set_mode)(struct mga_dev *mdev, struct mga_mode_config *mc);
	void (*softreset)(struct mga_dev *mdev);
};

struct mga_irq {
	/* our interrupt line */
	unsigned int irq;
	/* protection */
	spinlock_t lock;
	/* for waiting */
	wait_queue_head_t wait;
	/* the interrupts someone is interested in */
	u32 ien_requested;
	/*
	 * Same as ien_requested, except possibly
	 * EXTINT while we're in the process of handling it.
	 * This is what we always write to MGA_IEN.
	 */
	u32 ien;
	/* reference count for each interrupt event */
	u8 ref[32];
	/* count interrupt events */
	u8 count[32];
};

/* for emulating irqs via polling */
struct mga_irqthread {
	bool local_irq_disabled;
	u32 ien;
	pthread_t thread;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	irq_handler_t isr;
	void *isr_dev;
	bool irq_pending;
	pthread_cond_t irq_pending_cond;
	bool active;
};

struct mga_dma;
struct mga_mem_alloc;

struct mga_dev {
	struct drm_device base;
	int fd;

	struct mga_resource mmio_res;
	struct mga_resource mem_res;
	struct mga_resource iload_res;
	struct mga_resource dma_res;

	void __iomem *mmio_virt;
	void __iomem *mem_virt;
	void __iomem *iload_virt;
	void __iomem *dma_virt;

	unsigned int features;
	unsigned int outputs;
	unsigned int chip;
	unsigned int fifo_size;
	unsigned int mclk;

	u32 maccess;
	u8 vrefctrl;

	unsigned int pixel_format;

	struct mga_mode_config mode_config;
	struct drm_color palette[256];

	bool splitmode;
	bool interleave;
	bool nogscale;

	bool suspended;

	struct mga_irq irq;
	struct mga_irqthread irqthread;
	struct mga_dma *dma;
	struct mga_mem_alloc *dma_alloc;
	struct mga_mem_alloc *fb_alloc;

	union mga_pins pins;

	struct mga_dac_pll_info pixplla_info;
	struct mga_dac_pll_info pixpllb_info;
	struct mga_dac_pll_info pixpllc_info;
	struct mga_dac_pll_info syspll_info;
	struct mga_dac_pll_info vidpll_info;

	struct {
		u32 bus;
		void __iomem *virt;
	} cursor;

	struct mga_framebuffer empty_fb;
	unsigned int hzoom, vzoom;

	struct pci_dev pdev;
	struct tvp_dev tdev;
	struct pcf8574_dev edev;
	struct av9110_dev adev;
	struct mga_tvo_dev tvodev;
	struct mga_g450_tvo_dev tvodev_g450;

	struct device *dev;

	struct mga_i2c_channel ddc1;
	struct mga_i2c_channel ddc2;
	struct mga_i2c_channel ddcd;
	struct mga_i2c_channel misc;

	const struct mga_chip_funcs *funcs;

	enum mga_pll sysclk_plls;
	enum mga_pll enabled_plls;

	unsigned int mem_size;

	unsigned int rom_size;
	unsigned int pins_ptr;
	u8 rom_data[65536];
};

struct mga_pll_settings {
	enum mga_pll type;
	union {
		unsigned int fcristal;
		struct mga_dac_pll_settings dac;
		struct av9110_pll_settings av9110;
	};
};

struct mga_dac_regs {
	u8 indirect[0xFF];
	u8 palwtadd;
	u8 paldata;
	u8 pixrdmsk;
	u8 palrdadd;
	u8 curposxl;
	u8 curposxh;
	u8 curposyl;
	u8 curposyh;
};

u8  rom_read8(struct mga_dev *mdev, unsigned int off);
u16 rom_read16(struct mga_dev *mdev, unsigned int off);
u32 rom_read32(struct mga_dev *mdev, unsigned int off);

u8  mga_read8(struct mga_dev *mdev, unsigned int reg);
u16 mga_read16(struct mga_dev *mdev, unsigned int reg);
u32 mga_read32(struct mga_dev *mdev, unsigned int reg);

void mga_write8(struct mga_dev *mdev, unsigned int reg, u8 val);
void mga_write16(struct mga_dev *mdev, unsigned int reg, u16 val);
void mga_write32(struct mga_dev *mdev, unsigned int reg, u32 val);

int mga_open_device(const char *dev, struct mga_dev *mdev);
void mga_close_device(struct mga_dev *mdev);
int mga_probe_mem_size(struct mga_dev *mdev);
const char *pretty_size(unsigned int size);
void mga_print_chip_info(struct mga_dev *mdev);
void mga_print_features(struct mga_dev *mdev);

void dump_expdev(struct mga_dev *mdev);
void dump_pci_cfg(struct mga_dev *mdev);
void read_rom(struct mga_dev *mdev);
void dump_rom(struct mga_dev *mdev);
void dump_pins(struct mga_dev *mdev);
void dump_g450_dac(struct mga_dev *mdev);
void dump_dac(struct mga_dev *mdev);
void dump_tvp3026(struct mga_dev *mdev);
void dump_vga(struct mga_dev *mdev);
void dump_generic(struct mga_dev *mdev);

void dump_pins1(struct mga_dev *mdev);
void dump_pins2(struct mga_dev *mdev);
void dump_pins3(struct mga_dev *mdev);
void dump_pins4(struct mga_dev *mdev);
void dump_pins5(struct mga_dev *mdev);

void mga_determine_fifo_size(struct mga_dev *mdev);

void mga_wait_dwgeng_idle(struct mga_dev *mdev);
void mga_wait_dma_idle(struct mga_dev *mdev);

void mga_wait(struct mga_dev *mdev);

void mga_mclk_change_pre(struct mga_dev *mdev, unsigned int mclk);
void mga_mclk_change_post(struct mga_dev *mdev, unsigned int mclk);

void mga_misc_powerup(struct mga_dev *mdev);
void mga_misc_disable_vga(struct mga_dev *mdev);
void mga_misc_set_sync(struct mga_dev *mdev, unsigned int sync);

void mga_crtc1_restore(struct mga_dev *mdev, const struct mga_crtc1_regs *regs);
void mga_crtc1_restore_framebuffer(struct mga_dev *mdev, const struct mga_crtc1_regs *regs);
void mga_crtc1_enable_vidrst(struct mga_dev *mdev, bool enable);
void mga_crtc1_wait_vblank(struct mga_dev *mdev);
void mga_crtc1_wait_active_video(struct mga_dev *mdev);
void mga_crtc1_video(struct mga_dev *mdev, bool enable);
void mga_crtc1_dpms(struct mga_dev *mdev, u8 mode);
void mga_crtc1_set_sync(struct mga_dev *mdev, unsigned int sync);
unsigned int mga_crtc1_align_pitch(struct mga_dev *mdev,
				   unsigned int pitch,
				   unsigned int bpp);

void mga_crtc2_restore(struct mga_dev *mdev, const struct mga_crtc2_regs *regs);
void mga_crtc2_restore_framebuffer(struct mga_dev *mdev, const struct mga_crtc2_regs *regs);
void mga_crtc2_enable_vidrst(struct mga_dev *mdev, bool enable);
void mga_crtc2_wait_vblank(struct mga_dev *mdev);
void mga_crtc2_wait_active_video(struct mga_dev *mdev);
void mga_crtc2_video(struct mga_dev *mdev, bool enable);
void mga_crtc2_dpms(struct mga_dev *mdev, u8 mode);
void mga_crtc2_set_sync(struct mga_dev *mdev, unsigned int sync);
void mga_crtc2_restore_interlace(struct mga_dev *mdev, const struct mga_crtc2_regs *regs);
void mga_crtc2_pixclk_enable(struct mga_dev *mdev, bool enable);
void mga_crtc2_pixclk_select(struct mga_dev *mdev, unsigned int pll);
void mga_crtc2_dac_source(struct mga_dev *mdev, unsigned int crtc);

void mga_crtc1_align_address(const struct mga_dev *mdev,
			     struct mga_framebuffer *fb);
void mga_crtc1_powerup(struct mga_dev *mdev);
struct mga_plane_config;
struct mga_crtc_config;
int mga_crtc1_calc_mode(struct mga_dev *mdev,
			const struct mga_plane_config *pc,
			struct mga_crtc_config *cc,
			struct mga_crtc1_regs *regs);
int mga_crtc2_calc_mode(struct mga_dev *mdev,
			const struct mga_plane_config *pc,
			struct mga_crtc_config *cc,
			struct mga_crtc2_regs *regs);

unsigned int mga_2064w_alignment(unsigned int bpp, bool interleave);
unsigned int mga_1064sg_alignment(unsigned int bpp);

void mga_g100_rfhcnt(struct mga_dev *mdev, int mclk);

void mga_1064sg_softreset(struct mga_dev *mdev);

void mga_2064w_monitor_sense(struct mga_dev *mdev);
void mga_1064sg_monitor_sense(struct mga_dev *mdev);

int mga_init(struct mga_dev *mdev, bool force);
void mga_fini(struct mga_dev *mdev);
int mga_hardreset(struct mga_dev *mdev, bool force);

void mga_tests(struct mga_dev *mdev);

int mga_suspend(struct mga_dev *mdev);
int mga_resume(struct mga_dev *mdev);

void mga_reg_test(struct mga_dev *mdev);

void mga_solid_fill(struct mga_dev *mdev);

void mga_fill(struct mga_dev *mdev);

int mga_set_mode_crtc(struct mga_dev *mdev,
		      unsigned int crtc,
		      struct drm_framebuffer *fb,
		      unsigned int x, unsigned int y,
		      struct drm_display_mode *mode,
		      unsigned int outputs);

int mga_g450_set_mode(struct mga_dev *mdev,
		      struct mga_mode_config *mc);

int mga_g400_set_mode(struct mga_dev *mdev,
		      struct mga_mode_config *mc);

int mga_1064sg_set_mode(struct mga_dev *mdev,
			struct mga_mode_config *mc);

int mga_2064w_set_mode(struct mga_dev *mdev,
		       struct mga_mode_config *mc);

void mga_2064w_prepare(struct mga_dev *mdev);
void mga_2164w_prepare(struct mga_dev *mdev);
void mga_1064sg_prepare(struct mga_dev *mdev);
void mga_g100_prepare(struct mga_dev *mdev);
void mga_g200_prepare(struct mga_dev *mdev);
void mga_g400_prepare(struct mga_dev *mdev);
void mga_g450_prepare(struct mga_dev *mdev);
void mga_prepare(struct mga_dev *mdev);

int mga_g400_detect_tvout_adapter(struct mga_dev *mdev);

int mga_pll_calc(struct mga_dev *mdev, unsigned int freq, struct mga_pll_settings *pll);
int mga_pll_program(struct mga_dev *mdev, const struct mga_pll_settings *pll);

enum {
	DRM_REG_RW_CRTC,
	DRM_REG_RW_CRTCEXT,
	DRM_REG_RW_DAC,
	DRM_REG_RW_TVO,
	DRM_REG_RW_PCI,
	DRM_REG_RW_REG,
	DRM_REG_RW_GCTL,
	DRM_REG_RW_SEQ,
	DRM_REG_RW_ATTR,
};

enum {
	DRM_REG_RW_READ,
	DRM_REG_RW_WRITE_READ,
};

enum {
	DRM_REG_RW_BYTE,
	DRM_REG_RW_WORD,
	DRM_REG_RW_LONG,
};

int mga_reg_rw(struct mga_dev *mdev, u32 dev, u32 dir, u32 size, u32 reg, u32 *val);

int mga_g200_hipri(struct mga_dev *mdev,
		   int mclk, int pixclk,
		   struct mga_crtc1_regs *regs);

int mga_g400_hipri(struct mga_dev *mdev,
		   int mclk, int c1pixclk, int c2pixclk,
		   int c1bpp, int c2bpp, bool bt656,
		   struct mga_crtc1_regs *c1regs,
		   struct mga_crtc2_regs *c2regs);

void mga_enable_outputs(struct mga_dev *mdev);
void mga_disable_outputs(struct mga_dev *mdev);
void mga_set_initial_crtc1_mode(struct mga_dev *mdev,
				unsigned int pixel_format,
				const struct drm_display_mode *mode,
				unsigned int outputs);
void mga_set_initial_crtc2_mode(struct mga_dev *mdev,
				unsigned int pixel_format,
				const struct drm_display_mode *mode,
				unsigned int outputs);

int mga_get_crtc1_mode(struct mga_dev *mdev, const struct drm_display_mode **m);
int mga_get_crtc1_fb(struct mga_dev *mdev, struct drm_framebuffer **fb);
int mga_get_crtc2_mode(struct mga_dev *mdev, const struct drm_display_mode **m);
int mga_get_crtc2_fb(struct mga_dev *mdev, struct drm_framebuffer **fb);

int mga_mem_test(struct mga_dev *mdev,
		 u32 *read_bytes,
		 u32 *write_bytes,
		 u32 *read_usec,
		 u32 *write_usec,
		 u32 *clock);

void wbinvd(struct mga_dev *mdev);

void mga_irq_init(struct mga_dev *mdev);
void mga_irq_fini(struct mga_dev *mdev);
void mga_irq_enable(struct mga_dev *mdev, u32 irq);
void mga_irq_disable(struct mga_dev *mdev, u32 irq);
void mga_irq_ref(struct mga_dev *mdev, u32 irq);
void mga_irq_unref(struct mga_dev *mdev, u32 irq);
bool mga_irq_enabled(struct mga_dev *mdev, u32 irq);
int mga_irq_wait(struct mga_dev *mdev, u32 irq, unsigned int timeout);

struct mga_mem_alloc *mga_mem_alloc_init(u32 off, u32 len, void *virt, u32 alignment);
void mga_mem_alloc_fini(struct mga_mem_alloc *mem_alloc);
int mga_mem_alloc(struct mga_mem_alloc *mem_alloc, u32 *bus, void **virt, size_t size, bool reverse);
void mga_mem_free(struct mga_mem_alloc *mem_alloc, u32 bus, size_t size);

unsigned int crtc1_address_alignment(const struct mga_dev *mdev);
unsigned int crtc1_pitch_alignment(const struct mga_dev *mdev);
unsigned int crtc1_max_pitch(const struct mga_dev *mdev);

struct drm_framebuffer *mga_framebuffer_create(struct drm_device *dev,
					       unsigned int width,
					       unsigned int height,
					       unsigned int pixel_format,
					       unsigned int flags,
					       unsigned int offsets[4],
					       unsigned int pitches[4]);
void mga_framebuffer_destroy(struct drm_device *dev,
			     struct drm_framebuffer *fb);
void mga_framebuffer_pre_destroy(struct drm_device *dev,
				 struct drm_framebuffer *fb);
size_t mga_framebuffer_calc_size(const struct drm_framebuffer *fb);

int mga_load_lut(struct mga_dev *mdev,
		 unsigned int crtc, struct drm_color pal[256]);

irqreturn_t mga_irq_handler(int irq, void *dev);

int mga_crtc1_check_framebuffer(const struct mga_dev *mdev,
				unsigned int pixel_format,
				unsigned int flags,
				const unsigned int offsets[4],
				const unsigned int pitches[4]);
int mga_crtc2_check_framebuffer(const struct mga_dev *mdev,
				unsigned int pixel_format,
				unsigned int flags,
				const unsigned int offsets[4],
				const unsigned int pitches[4]);
int mga_cursor_check_framebuffer(const struct mga_dev *mdev,
				 unsigned int pixel_format,
				 unsigned int flags,
				 const unsigned int offsets[4],
				 const unsigned int pitches[4]);
int mga_bes_check_framebuffer(const struct mga_dev *mdev,
			      unsigned int pixel_format,
			      unsigned int flags,
			      const unsigned int offsets[4],
			      const unsigned int pitches[4]);
int mga_spic_check_framebuffer(const struct mga_dev *mdev,
			       unsigned int pixel_format,
			       unsigned int flags,
			       const unsigned int offsets[4],
			       const unsigned int pitches[4]);

u32 mga_framebuffer_calc_bus(const struct mga_framebuffer *mfb,
			     unsigned int x, unsigned int y,
			     unsigned int plane, bool field);

void __iomem *mga_framebuffer_calc_virt(const struct mga_framebuffer *mfb,
					unsigned int x, unsigned int y,
					unsigned int plane, bool field);

int _mga_crtc1_wait_vblank(struct mga_dev *mdev);
int _mga_crtc2_wait_vblank(struct mga_dev *mdev);

const char *mga_chip_name(unsigned int chip);

unsigned int mga_crtc1_vidrst_delay(const struct mga_dev *mdev, u32 pixel_format);
unsigned int mga_crtc2_vidrst_delay(const struct mga_dev *mdev, bool bt656);

#endif
