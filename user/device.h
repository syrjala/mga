/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef DEVICE_H
#define DEVICE_H

#include <stdbool.h>
#include <stdint.h>
#include <time.h>

struct device {
	struct device *parent;
	void *drvdata;
	char drv_name[64];
	char dev_name[64];
};

static inline void dev_set_drvdata(struct device *dev, void *data)
{
	dev->drvdata = data;
}

static inline void *dev_get_drvdata(struct device *dev)
{
	return dev->drvdata;
}

static inline char *dev_driver_string(struct device *dev)
{
	return dev->drv_name;
}

static inline char *dev_name(struct device *dev)
{
	return dev->dev_name;
}

#define dev_printk(level, dev, format, arg...) \
	printk(level "%s %s: " format , dev_driver_string(dev) , \
	       dev_name(dev) , ## arg)

#define dev_emerg(dev, format, arg...) \
	dev_printk(KERN_EMERG , dev , format , ## arg)
#define dev_alert(dev, format, arg...) \
	dev_printk(KERN_ALERT , dev , format , ## arg)
#define dev_crit(dev, format, arg...) \
	dev_printk(KERN_CRIT , dev , format , ## arg)
#define dev_err(dev, format, arg...) \
	dev_printk(KERN_ERR , dev , format , ## arg)
#define dev_warn(dev, format, arg...) \
	dev_printk(KERN_WARNING , dev , format , ## arg)
#define dev_notice(dev, format, arg...) \
	dev_printk(KERN_NOTICE , dev , format , ## arg)
#define dev_info(dev, format, arg...) \
	dev_printk(KERN_INFO , dev , format , ## arg)
#define dev_dbg(dev, format, arg...) \
	dev_printk(KERN_DEBUG , dev , format , ## arg)
#define dev_vdbg dev_dbg

#endif
