/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>
#include <fcntl.h>
#include <assert.h>
#include <stdarg.h>
#include <errno.h>

#include <readline/readline.h>
#include <readline/history.h>

#include "server.h"
#include "modes.h"
#include "drm_kms.h"

struct client {
	int fd;
	u32 serial;

	struct list_head modelist;
	struct list_head fblist;
};

static u32 client_next_serial(struct client *client)
{
	return client->serial++;
}

static int do_connect(const char *path)
{
	int fd;
	int ret;
	struct sockaddr_un addr;

	fd = open_socket(path, &addr);
	if (fd < 0)
		return fd;

	ret = connect(fd, (struct sockaddr *) &addr, sizeof addr);
	if (ret) {
		close(fd);
		return ret;
	}

	return fd;
}

static ssize_t do_cmd(struct client *client,
		      unsigned int cmd,
		      unsigned int target,
		      unsigned int len,
		      const void *data,
		      struct mga_res *res)
{
	struct mga_cmd c;
	ssize_t r;

	c.cmd    = cmd;
	c.serial = client_next_serial(client);
	c.len    = len;
	c.target = target;

	r = wrap_write(client->fd, &c, sizeof c);
	if (r < 0)
		return r;
	if ((size_t)r != sizeof c) {
		errno = ENXIO;
		return -1;
	}

	if (c.len && data) {
		r = wrap_write(client->fd, data, c.len);
		if (r < 0)
			return r;
		if ((size_t)r != c.len) {
			errno = ENXIO;
			return -1;
		}
	}

	r = wrap_read(client->fd, res, sizeof *res);
	if (r < 0)
		return r;
	if ((size_t)r != sizeof *res) {
		errno = ENXIO;
		return -1;
	}

	if (res->cmd != c.cmd ||
	    res->serial != c.serial) {
		errno = ENXIO;
		return -1;
	}

	if (cmd != MGA_CMD_OPEN && res->target != c.target) {
		errno = ENXIO;
		return -1;
	}

	if (res->_errno) {
		errno = res->_errno;
		return -1;
	}

	return res->code;
}

static int client_open(struct client *client,
		       const char *pathname,
		       int flags)
{
	struct mga_res res;

	if (flags & ~(O_RDONLY | O_WRONLY | O_RDWR)) {
		errno = EINVAL;
		return -1;
	}
	if ((flags & (O_RDONLY | O_WRONLY | O_RDWR)) != O_RDONLY &&
	    (flags & (O_RDONLY | O_WRONLY | O_RDWR)) != O_WRONLY &&
	    (flags & (O_RDONLY | O_WRONLY | O_RDWR)) != O_RDWR) {
		errno = EINVAL;
		return -1;
	}

	if (!pathname) {
		errno = EINVAL;
		return -1;
	}

	return do_cmd(client, MGA_CMD_OPEN, 0, strlen(pathname) + 1, pathname, &res);
}

static int client_close(struct client *client,
			int fd)
{
	struct mga_res res;

	if (fd < 0) {
		errno = EINVAL;
		return -1;
	}

	return do_cmd(client, MGA_CMD_CLOSE, fd, 0, NULL, &res);
}

static ssize_t client_write(struct client *client,
			    int fd,
			    const void *data,
			    size_t len)
{
	struct mga_res res;
	ssize_t r;

	if (fd < 0) {
		errno = EINVAL;
		return -1;
	}

	r = do_cmd(client, MGA_CMD_WRITE, fd, len, data, &res);
	if (r < 0)
		return r;

	if (res.code > len) {
		errno = ENXIO;
		return -1;
	}

	return res.code;
}

static ssize_t client_read(struct client *client,
			   int fd,
			   void *data,
			   size_t len)
{
	struct mga_res res;
	ssize_t r;

	if (fd < 0) {
		errno = EINVAL;
		return -1;
	}

	r = do_cmd(client, MGA_CMD_READ, fd, len, NULL, &res);
	if (r < 0)
		return r;

	if (res.len > len) {
		errno = ENXIO;
		return -1;
	}

	if (res.code > res.len) {
		errno = ENXIO;
		return -1;
	}

	r = wrap_read(client->fd, data, res.code);
	if (r < 0)
		return r;
	if ((size_t)r != res.code) {
		errno = ENXIO;
		return -1;
	}

	return res.code;
}

/*
 * phase 1:
 * cmd + request nr
 * res + len of request data
 *
 * phase 2:
 * cmd + request data
 * res + return request data
 */
static int client_ioctl(struct client *client,
			int fd, int request, ...)
{
	u8 buf[20];
	struct mga_cmd cmd;
	struct mga_res res;
	ssize_t r;
	size_t len;
	va_list ap;
	void *argp;
	struct server_ioctl *ioc;

	if (fd < 0) {
		errno = EINVAL;
		return -1;
	}

	va_start(ap, request);
	argp = va_arg(ap, void *);
	switch (request) {
	case DRM_IOC_DMA_GENERAL_PURPOSE:
	case DRM_IOC_ATOMIC:
	case DRM_IOC_GETRESOURCES:
		len = va_arg(ap, int);
		break;
	default:
		len = 0;
		break;
	}
	va_end(ap);

	len += _IOC_SIZE(request);
	len += sizeof(*ioc);

	ioc = malloc(len);
	if (!ioc) {
		errno = ENOMEM;
		return -1;
	}

	cmd.cmd    = MGA_CMD_IOCTL;
	cmd.serial = client_next_serial(client);
	cmd.len    = len;
	cmd.target = fd;

	r = wrap_write(client->fd, &cmd, sizeof cmd);
	if (r < 0)
		return r;
	if ((size_t)r != sizeof cmd) {
		errno = ENXIO;
		return r;
	}

	ioc->cmd = request;
	ioc->pad = 0;

	r = wrap_write(client->fd, ioc, sizeof *ioc);
	if (r < 0)
		return r;
	if ((size_t)r != sizeof *ioc) {
		errno = ENXIO;
		return r;
	}

	if (len > sizeof *ioc && (_IOC_DIR(ioc->cmd) & _IOC_WRITE)) {
		r = wrap_write(client->fd, argp, len - sizeof *ioc);
		if (r < 0)
			return r;
		if ((size_t)r != len - sizeof *ioc) {
			errno = ENXIO;
			return r;
		}
	}

	r = wrap_read(client->fd, &res, sizeof res);
	if (r < 0)
		return r;
	if ((size_t)r != sizeof res) {
		errno = ENXIO;
		return -1;
	}

	if (res.cmd != cmd.cmd ||
	    res.serial != cmd.serial ||
	    res.target != cmd.target) {
		errno = ENXIO;
		return -1;
	}

	printf("%d\n", res._errno);

	if (res._errno) {
		errno = res._errno;
		return -1;
	}

	if (res.len > cmd.len) {
		errno = ENXIO;
		return -1;
	}

	if (res.len) {
		assert(res.len >= sizeof *ioc);

		r = wrap_read(client->fd, ioc, sizeof *ioc);
		if (r < 0)
			return r;
		if ((size_t)r != sizeof *ioc) {
			errno = ENXIO;
			return -1;
		}

		r = wrap_read(client->fd, argp, res.len - sizeof *ioc);
		if (r < 0)
			return r;
		if ((size_t)r != res.len - sizeof *ioc) {
			errno = ENXIO;
			return -1;
		}
	}

	return res.code;
}

static void print_mode(const struct drm_display_mode *mode)
{
	printf(" vrefresh     = %f Hz\n"
	       " dblscan      = %d\n"
	       " interlace    = %d\n"
	       " htotal       = %u\n"
	       " hdisplay     = %u\n"
	       " hblank_start = %u\n"
	       " hblank_end   = %u\n"
	       " hsync_start  = %u\n"
	       " hsync_end    = %u\n"
	       " vtotal       = %u\n"
	       " vdisplay     = %u\n"
	       " vblank_start = %u\n"
	       " vblank_end   = %u\n"
	       " vsync_start  = %u\n"
	       " vsync_end    = %u\n",
	       mode->vrefresh / 1000.0,
	       !!(mode->flags & DRM_MODE_FLAG_DBLSCAN),
	       !!(mode->flags & DRM_MODE_FLAG_INTERLACE),
	       mode->htotal,
	       mode->hdisplay,
	       mode->hblank_start,
	       mode->hblank_start + mode->hblank_width,
	       mode->hsync_start,
	       mode->hsync_start + mode->hsync_width,
	       mode->vtotal,
	       mode->vdisplay,
	       mode->vblank_start,
	       mode->vblank_start + mode->vblank_width,
	       mode->vsync_start,
	       mode->vsync_start + mode->vsync_width);
}

struct modelist_entry {
	char name[64];
	struct drm_display_mode mode;
	struct list_head head;
};

struct fblist_entry {
	struct drm_mk_fb f;
	struct list_head head;
};

static int modelist_add(struct client *client, const char *name, const struct drm_display_mode *mode)
{
	struct modelist_entry *e;

	list_for_each_entry(e, &client->modelist, head) {
		/* overwrite? */
		if (!strcmp(name, e->name)) {
			e->mode = *mode;
			return 0;
		}
	}

	e = calloc(1, sizeof *e);
	if (!e)
		return -1;

	strncpy(e->name, name, 64);
	e->name[63] = 0;
	e->mode = *mode;

	list_add(&e->head, &client->modelist);

	return 0;
}

static int modelist_del(struct client *client, const char *name)
{
	struct modelist_entry *e;

	list_for_each_entry(e, &client->modelist, head) {
		/* found it? */
		if (!strcmp(name, e->name)) {
			list_del(&e->head);
			free(e);
			return 0;
		}
	}

	return - 1;
}

static const struct drm_display_mode *modelist_lookup(struct client *client, const char *name)
{
	struct modelist_entry *e;

	list_for_each_entry(e, &client->modelist, head) {
		/* found it? */
		if (!strcmp(name, e->name))
			return &e->mode;
	}

	return NULL;
}

static int fblist_add(struct client *client, const struct drm_mk_fb *f)
{
	struct fblist_entry *e;

	list_for_each_entry(e, &client->fblist, head) {
		/* overwrite? */
		if (f->fb_id == e->f.fb_id) {
			e->f = *f;
			return 0;
		}
	}

	e = calloc(1, sizeof *e);
	if (!e)
		return -1;

	e->f = *f;

	list_add(&e->head, &client->fblist);

	return 0;
}

static int fblist_del(struct client *client, unsigned int fb_id)
{
	struct fblist_entry *e;

	list_for_each_entry(e, &client->fblist, head) {
		if (fb_id == e->f.fb_id) {
			list_del(&e->head);
			free(e);
			return 0;
		}
	}

	return - 1;
}

static const struct drm_mk_fb *fblist_lookup(struct client *client, unsigned int fb_id)
{
	struct fblist_entry *e;

	list_for_each_entry(e, &client->fblist, head) {
		if (fb_id == e->f.fb_id)
			return &e->f;
	}

	return NULL;
}

static void print_mk_fb(const struct drm_mk_fb *f)
{
	printf(" fb_id        = %u\n"
	       " width        = %u\n"
	       " height       = %u\n"
	       " pixel_format = %u\n"
	       " flags        = %x\n"
	       " offsets {\n"
	       "  [0] = %u\n"
	       "  [1] = %u\n"
	       "  [2] = %u\n"
	       "  [3] = %u\n"
	       " }\n"
	       " pitches {\n"
	       "  [0] = %u\n"
	       "  [1] = %u\n"
	       "  [2] = %u\n"
	       "  [3] = %u\n"
	       " }\n",
	       f->fb_id,
	       f->width,
	       f->height,
	       f->pixel_format,
	       f->flags,
	       f->offsets[0],
	       f->offsets[1],
	       f->offsets[2],
	       f->offsets[3],
	       f->pitches[0],
	       f->pitches[1],
	       f->pitches[2],
	       f->pitches[3]);
}

static void print_fb(struct client *client, unsigned int fb_id)
{
	const struct drm_mk_fb *f = fblist_lookup(client, fb_id);
	if (!f)
		return;

	print_mk_fb(f);
}

static struct drm_display_mode curr_mode[2];

#define FORMAT_ENTRY(f) [DRM_FORMAT_##f] = #f

static const char *formats[] = {
	FORMAT_ENTRY(UNKNOWN),
	FORMAT_ENTRY(C1),
	FORMAT_ENTRY(C2),
	FORMAT_ENTRY(C4),
	FORMAT_ENTRY(C6),
	FORMAT_ENTRY(C8),
	FORMAT_ENTRY(AC44),
	FORMAT_ENTRY(XRGB4444),
	FORMAT_ENTRY(ARGB4444),
	FORMAT_ENTRY(RGBX4444),
	FORMAT_ENTRY(RGBA4444),
	FORMAT_ENTRY(XBGR4444),
	FORMAT_ENTRY(ABGR4444),
	FORMAT_ENTRY(BGRX4444),
	FORMAT_ENTRY(BGRA4444),
	FORMAT_ENTRY(XRGB1555),
	FORMAT_ENTRY(ARGB1555),
	FORMAT_ENTRY(RGBX5551),
	FORMAT_ENTRY(RGBA5551),
	FORMAT_ENTRY(XBGR1555),
	FORMAT_ENTRY(ABGR1555),
	FORMAT_ENTRY(BGRX5551),
	FORMAT_ENTRY(BGRA5551),
	FORMAT_ENTRY(RGB565),
	FORMAT_ENTRY(BGR565),
	FORMAT_ENTRY(RGB888),
	FORMAT_ENTRY(BGR888),
	FORMAT_ENTRY(XRGB8888),
	FORMAT_ENTRY(ARGB8888),
	FORMAT_ENTRY(RGBX8888),
	FORMAT_ENTRY(RGBA8888),
	FORMAT_ENTRY(XBGR8888),
	FORMAT_ENTRY(ABGR8888),
	FORMAT_ENTRY(BGRX8888),
	FORMAT_ENTRY(BGRA8888),
	FORMAT_ENTRY(YUYV),
	FORMAT_ENTRY(UYVY),
	FORMAT_ENTRY(VYUY),
	FORMAT_ENTRY(YVYU),
	FORMAT_ENTRY(YUV420),
	FORMAT_ENTRY(YVU420),
	FORMAT_ENTRY(NV12),
	FORMAT_ENTRY(NV21),
};

static unsigned int parse_pixel_format(const char *str)
{
	int i;

	for (i = 0; i < (int)ARRAY_SIZE(formats); i++)
		if (!strcasecmp(str, formats[i]))
			return i;

	return DRM_FORMAT_UNKNOWN;
}

static int parse_crtc(char *str, unsigned int *ret_crtc)
{
	if (!strcasecmp(str, "crtc1"))
		*ret_crtc = MGA_CRTC_CRTC1;
	else if (!strcasecmp(str, "crtc2"))
		*ret_crtc = MGA_CRTC_CRTC2;
	else
		return -EINVAL;

	return 0;
}

static int parse_outputs(char *str, unsigned int *ret_outputs)
{
	char *saveptr = NULL;
	char *t = strtok_r(str, ":", &saveptr);

	printf("po: %s\n", t);

	while (t) {
		if (!strcasecmp(t, "off"))
			;
		else if (!strcasecmp(t, "dac1"))
			*ret_outputs |= MGA_OUTPUT_DAC1;
		else if (!strcasecmp(t, "dac2"))
			*ret_outputs |= MGA_OUTPUT_DAC2;
		else if (!strcasecmp(t, "tmds1"))
			*ret_outputs |= MGA_OUTPUT_TMDS1;
		else if (!strcasecmp(t, "tmds2"))
			*ret_outputs |= MGA_OUTPUT_TMDS2;
		else if (!strcasecmp(t, "tvout"))
			*ret_outputs |= MGA_OUTPUT_TVOUT;
		else
			return -EINVAL;

		t = strtok_r(NULL, ":", &saveptr);
	}

	return 0;
}

static int parse_mode(struct client *client, char *str, struct drm_display_mode *ret_mode)
{
	unsigned int x = 0, y = 0, r = 0;
	char c = '\0';
	const struct drm_display_mode *mode;

	if (!strcmp(str, "off")) {
		memset(ret_mode, 0, sizeof *ret_mode);
		return 0;
	} else {
		printf("looking for mode %s\n", str);
		mode = modelist_lookup(client, str);
		if (mode) {
			*ret_mode = *mode;
			return 0;
		}
	}

	if (sscanf(str, "%ux%u-%u%c", &x, &y, &r, &c) < 3)
		return -EINVAL;

	ret_mode->hdisplay = x;
	ret_mode->vdisplay = y;
	ret_mode->vrefresh = r * 1000;
	if (c == 'i')
		ret_mode->flags = DRM_MODE_FLAG_INTERLACE;
	if (c == 'd')
		ret_mode->flags = DRM_MODE_FLAG_DBLSCAN;

	return 0;
}

enum {
	CRTC_MODE       = 0x1,
	CRTC_CONNECTORS = 0x2,
};

struct client_crtc_cmd {
	uint32_t flags;
	uint32_t crtc;
	struct drm_umode mode;
	uint32_t connectors[5];
	uint32_t num_connectors;
};

enum {
	PLANE_FB      = 0x1,
	PLANE_CRTC    = 0x2,
	PLANE_SRC     = 0x4,
	PLANE_DST     = 0x8,
	PLANE_ROTATE  = 0x10,
	PLANE_OVERLAY = 0x20,
	PLANE_OVERLAY_CKEY = 0x40,
	PLANE_LUT     = 0x80,
};

struct client_plane_cmd {
	uint32_t flags;
	uint32_t plane, fb, crtc;
	uint32_t sx, sy, sw, sh;
	int32_t dx, dy;
	uint32_t dw, dh;
	uint8_t rotate;
	bool overlay;
	uint8_t overlay_ckey;
	struct drm_color lut[256];
};

enum {
	CONNECTOR_TV_STD           = 0x1,
	CONNECTOR_CABLE_TYPE       = 0x2,
	CONNECTOR_TEXT_FILTER      = 0x4,
	CONNECTOR_DEFLICKER        = 0x8,
	CONNECTOR_COLOR_BARS       = 0x10,
	CONNECTOR_DOT_CRAWL_FREEZE = 0x20,
	CONNECTOR_GAMMA            = 0x40,
};

struct client_connector_cmd {
	uint32_t flags;
	uint32_t connector;
	uint32_t tv_std;
	uint32_t cable_type;
	uint32_t text_filter;
	uint32_t deflicker;
	uint32_t color_bars;
	uint32_t dot_crawl_freeze;
	uint32_t gamma;
};

static int parse_connectors(char *str, uint32_t *connectors,
			    uint32_t num_connectors, uint32_t *ret_num_connectors)
{
	char *saveptr = NULL;
	char *t = strtok_r(str, ":", &saveptr);
	unsigned int i = 0;

	printf("po: %s\n", t);

	while (t) {
		unsigned int connector;

		if (sscanf(t, "%u", &connector) != 1)
			return -EINVAL;

		if (i >= num_connectors)
			return -ENOSPC;

		connectors[i++] = connector;

		t = strtok_r(NULL, ":", &saveptr);
	}

	*ret_num_connectors = i;

	return 0;
}

static int parse_crtc_cmd(struct client *client, char *str,
			  struct client_crtc_cmd *cmd)
{
	char *saveptr = NULL;
	char *t = strtok_r(str, ",", &saveptr);
	int r;

	while (t) {
		if (!strncmp(t, "crtc=", 5)) {
			if (sscanf(t + 5, "%u", &cmd->crtc) != 1)
				return -EINVAL;
		} else if (!strncmp(t, "mode=", 5)) {
			r = parse_mode(client, t + 5, &cmd->mode.mode);
			if (r < 0)
				return r;
			cmd->flags |= CRTC_MODE;
		} else if (!strncmp(t, "connectors=", 11)) {
			r = parse_connectors(t + 11, cmd->connectors,
					     ARRAY_SIZE(cmd->connectors),
					     &cmd->num_connectors);
			if (r < 0)
				return r;
			cmd->flags |= CRTC_CONNECTORS;
		} else
			return -EINVAL;

		t = strtok_r(NULL, ",", &saveptr);
	}

	return 0;
}

static int parse_rotate(const char *str, uint8_t *r)
{
	unsigned int angle = 0;
	char a = '\0', b = '\0';

	*r = 0;

	if (sscanf(str, "%u:%c%c", &angle, &a, &b) == 3)
		goto check;
	if (sscanf(str, "%u:%c", &angle, &a) == 2)
		goto check;
	if (sscanf(str, "%u:", &angle) == 1)
		goto check;

	return -EINVAL;

 check:
	switch (a) {
	case 'x':
		*r |= DRM_REFLECT_X;
		break;
	case 'y':
		*r |= DRM_REFLECT_Y;
		break;
	case '\0':
		break;
	default:
		return -EINVAL;
	}

	switch (b) {
	case 'x':
		*r |= DRM_REFLECT_X;
		break;
	case 'y':
		*r |= DRM_REFLECT_Y;
		break;
	case '\0':
		break;
	default:
		return -EINVAL;
	}

	switch (angle) {
	case 0:
		*r |= DRM_ROTATE_0;
		break;
	case 90:
		*r |= DRM_ROTATE_90;
		break;
	case 180:
		*r |= DRM_ROTATE_180;
		break;
	case 270:
		*r |= DRM_ROTATE_270;
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static int parse_dst_region(const char *str, uint32_t *w, uint32_t *h, int32_t *x, int32_t *y)
{
	if (sscanf(str, "%ux%u+%u+%u", w, h, x, y) == 4) {
		return 0;
	}

	if (sscanf(str, "%ux%u-%d+%d", w, h, x, y) == 4) {
		*x = -*x;
		return 0;
	}

	if (sscanf(str, "%ux%u+%d-%d", w, h, x, y) == 4) {
		*y = -*y;
		return 0;
	}

	if (sscanf(str, "%ux%u-%d-%d", w, h, x, y) == 4) {
		*x = -*x;
		*y = -*y;
		return 0;
	}

	return -EINVAL;
}

static int parse_src_region(const char *str, uint32_t *w, uint32_t *h, uint32_t *x, uint32_t *y)
{
	if (sscanf(str, "%ux%u+%u+%u", w, h, x, y) != 4)
		return -EINVAL;

	*x <<= 16;
	*y <<= 16;
	*w <<= 16;
	*h <<= 16;

	return 0;
}

static int parse_plane_cmd(struct client *client, char *str,
			   struct client_plane_cmd *cmd)
{
	char *saveptr = NULL;
	char *t = strtok_r(str, ",", &saveptr);
	int r;

	while (t) {
		if (!strncmp(t, "src=", 4)) {
			r = parse_src_region(t + 4, &cmd->sw, &cmd->sh, &cmd->sx, &cmd->sy);
			if (r)
				return r;
			cmd->flags |= PLANE_SRC;
		} else if (!strncmp(t, "dst=", 4)) {
			r = parse_dst_region(t + 4, &cmd->dw, &cmd->dh, &cmd->dx, &cmd->dy);
			if (r)
				return r;
			cmd->flags |= PLANE_DST;
		} else if (!strncmp(t, "plane=", 6)) {
			if (sscanf(t + 6, "%u", &cmd->plane) != 1)
				return -EINVAL;
		} else if (!strncmp(t, "fb=", 3)) {
			if (sscanf(t + 3, "%u", &cmd->fb) != 1)
				return -EINVAL;
			cmd->flags |= PLANE_FB;
		} else if (!strncmp(t, "crtc=", 5)) {
			if (sscanf(t + 5, "%u", &cmd->crtc) != 1)
				return -EINVAL;
			cmd->flags |= PLANE_CRTC;
		} else if (!strncmp(t, "rotate=", 7)) {
			r = parse_rotate(t + 7, &cmd->rotate);
			if (r)
				return r;
			cmd->flags |= PLANE_ROTATE;
		} else if (!strncmp(t, "overlay=", 8)) {
			int tmp;
			if (sscanf(t + 8, "%u", &tmp) != 1)
				return -EINVAL;
			if (tmp != 0 && tmp != 1)
				return -EINVAL;
			cmd->overlay = tmp;
			cmd->flags |= PLANE_OVERLAY;
		} else if (!strncmp(t, "overlay_ckey=", 13)) {
			int tmp;
			if (sscanf(t + 13, "%u", &tmp) != 1)
				return -EINVAL;
			if (tmp < 0 || tmp > 0xff)
				return -EINVAL;
			cmd->overlay_ckey = tmp;
			cmd->flags |= PLANE_OVERLAY_CKEY;
		} else if (!strncmp(t, "lut=", 4)) {
			unsigned int pixel_format = parse_pixel_format(t + 4);
			drm_generate_palette(pixel_format, cmd->lut);
			cmd->flags |= PLANE_LUT;
		} else
			return -EINVAL;

		t = strtok_r(NULL, ",", &saveptr);
	}

	return 0;
}

static int parse_tv_std(const char *str, uint32_t *std)
{
	if (!strncasecmp(str, "pal", 3))
		*std = DRM_TV_STD_PAL;
	else if (!strncasecmp(str, "ntsc", 4))
		*std = DRM_TV_STD_NTSC;
	else
		return -EINVAL;

	return 0;
}

static int parse_cable_type(const char *str, uint32_t *cable_type)
{
	if (!strncasecmp(str, "composite", 9) ||
	    !strncasecmp(str, "s-video", 7))
		*cable_type = MGA_TVO_CABLE_COMPOSITE_SVIDEO;
	else if (!strncasecmp(str, "scart-rgb", 9))
		*cable_type = MGA_TVO_CABLE_SCART_RGB;
	else if (!strncasecmp(str, "scart-type2", 11))
		*cable_type = MGA_TVO_CABLE_SCART_TYPE2;
	else
		return -EINVAL;

	return 0;
}

static int parse_connector_cmd(struct client *client, char *str,
			       struct client_connector_cmd *cmd)
{
	char *saveptr = NULL;
	char *t = strtok_r(str, ",", &saveptr);
	int r;

	while (t) {
		if (!strncmp(t, "connector=", 10)) {
			if (sscanf(t + 10, "%u", &cmd->connector) != 1)
				return -EINVAL;
		} else if (!strncmp(t, "tv_std=", 7)) {
			r = parse_tv_std(str + 7, &cmd->tv_std);
			if (r)
				return r;
			cmd->flags |= CONNECTOR_TV_STD;
		} else if (!strncmp(t, "cable_type=", 11)) {
			r = parse_cable_type(str + 11, &cmd->cable_type);
			if (r)
				return r;
			cmd->flags |= CONNECTOR_CABLE_TYPE;
		} else if (!strncmp(t, "text_filter=", 12)) {
			int tmp;
			if (sscanf(t + 12, "%u", &tmp) != 1)
				return -EINVAL;
			if (tmp != 0 && tmp != 1)
				return -EINVAL;
			cmd->text_filter = tmp;
			cmd->flags |= CONNECTOR_TEXT_FILTER;
		} else if (!strncmp(t, "deflicker=", 10)) {
			int tmp;
			if (sscanf(t + 10, "%u", &tmp) != 1)
				return -EINVAL;
			if (tmp < 0 || tmp > 2)
				return -EINVAL;
			cmd->deflicker = tmp;
			cmd->flags |= CONNECTOR_DEFLICKER;
		} else if (!strncmp(t, "color_bars=", 11)) {
			int tmp;
			if (sscanf(t + 11, "%u", &tmp) != 1)
				return -EINVAL;
			if (tmp != 0 && tmp != 1)
				return -EINVAL;
			cmd->color_bars = tmp;
			cmd->flags |= CONNECTOR_COLOR_BARS;
		} else if (!strncmp(t, "dot_crawl_freeze=", 17)) {
			int tmp;
			if (sscanf(t + 17, "%u", &tmp) != 1)
				return -EINVAL;
			if (tmp != 0 && tmp != 1)
				return -EINVAL;
			cmd->dot_crawl_freeze = tmp;
			cmd->flags |= CONNECTOR_DOT_CRAWL_FREEZE;
		} else if (!strncmp(t, "gamma=", 6)) {
			int tmp;
			if (sscanf(t + 6, "%u", &tmp) != 1)
				return -EINVAL;
			if (tmp < 4 || tmp > 40)
				return -EINVAL;
			cmd->gamma = tmp;
			cmd->flags |= CONNECTOR_GAMMA;
		} else
			return -EINVAL;

		t = strtok_r(NULL, ",", &saveptr);
	}

	return 0;
}

static int parse_atomic(struct client *client, char *str,
			struct client_crtc_cmd *crtc,
			unsigned int num_crtc, unsigned int *ret_num_crtc,
			struct client_plane_cmd *plane,
			unsigned int num_plane, unsigned int *ret_num_plane,
			struct client_connector_cmd *connector,
			unsigned int num_connector, unsigned int *ret_num_connector)
{
	char *saveptr = NULL;
	char *t = strtok_r(str, "/", &saveptr);
	unsigned int ci = 0;
	unsigned int pi = 0;
	unsigned int ri = 0;

	while (t) {
		if (!strncmp(t, "crtc", 4)) {
			int r;

			if (ci >= num_crtc)
				return -ENOSPC;
			r = parse_crtc_cmd(client, t, &crtc[ci]);
			if (r)
				return r;
			ci++;
		} else if (!strncmp(t, "plane", 5)) {
			int r;

			if (pi >= num_plane)
				return -ENOSPC;
			r = parse_plane_cmd(client, t, &plane[pi]);
			if (r)
				return r;
			pi++;
		} else if (!strncmp(t, "connector", 9)) {
			int r;

			if (ri >= num_connector)
				return -ENOSPC;
			r = parse_connector_cmd(client, t, &connector[ri]);
			if (r)
				return r;
			ri++;
		} else
			return -EINVAL;

		t = strtok_r(NULL, "/", &saveptr);
	}

	*ret_num_crtc = ci;
	*ret_num_plane = pi;
	*ret_num_connector = ri;

	return 0;
}

static void atomic_add_blob(struct drm_mode_atomic *ato, uint32_t prop_id,
			    unsigned long _blob_ptr, const void *blob, size_t len,
			    unsigned int prop_idx, unsigned int blob_idx)
{
	uint32_t *props_ptr = (void*)ato + ato->props_ptr;
	uint64_t *prop_values_ptr = (void*)ato + ato->prop_values_ptr;
	uint64_t *blob_values_ptr = (void*)ato + ato->blob_values_ptr;
	void *blob_ptr = (void*)ato + _blob_ptr;

	props_ptr[prop_idx] = prop_id;
	prop_values_ptr[prop_idx] = len;
	blob_values_ptr[blob_idx] = _blob_ptr;

	if (len)
		memcpy(blob_ptr, blob, len);
}

static void atomic_add_prop(struct drm_mode_atomic *ato,
			    uint32_t prop_id, uint64_t value,
			    unsigned int prop_idx)
{
	uint32_t *props_ptr = (void*)ato + ato->props_ptr;
	uint64_t *prop_values_ptr = (void*)ato + ato->prop_values_ptr;

	props_ptr[prop_idx] = prop_id;
	prop_values_ptr[prop_idx] = value;
}

static void atomic_add_obj(struct drm_mode_atomic *ato,
			   uint32_t obj_id, uint32_t count_props,
			   unsigned int obj_idx)
{
	uint32_t *objs_ptr = (void*)ato + ato->objs_ptr;
	uint32_t *count_props_ptr = (void*)ato + ato->count_props_ptr;

	objs_ptr[obj_idx] = obj_id;
	count_props_ptr[obj_idx] = count_props;
}

static unsigned int count_crtc_props_one(const struct client_crtc_cmd *crtc)
{
	unsigned int count = 0;

	if (crtc->flags & CRTC_MODE)
		count++;
	if (crtc->flags & CRTC_CONNECTORS)
		count++;

	return count;
}

static unsigned int count_crtc_blobs_one(const struct client_crtc_cmd *crtc)
{
	unsigned int count = 0;

	if (crtc->flags & CRTC_MODE)
		count++;
	if (crtc->flags & CRTC_CONNECTORS)
		count++;

	return count;
}

static unsigned int count_crtc_props(const struct client_crtc_cmd *crtc, int num_crtc)
{
	unsigned int count = 0;
	int i;

	for (i = 0; i < num_crtc; i++)
		count += count_crtc_props_one(&crtc[i]);

	return count;
}

static unsigned int count_crtc_blobs(const struct client_crtc_cmd *crtc, int num_crtc)
{
	unsigned int count = 0;
	int i;

	for (i = 0; i < num_crtc; i++)
		count += count_crtc_blobs_one(&crtc[i]);

	return count;
}

static unsigned int count_plane_props_one(const struct client_plane_cmd *plane)
{
	unsigned int count = 0;

	if (plane->flags & PLANE_FB)
		count++;
	if (plane->flags & PLANE_CRTC)
		count++;
	if (plane->flags & PLANE_SRC)
		count += 4;
	if (plane->flags & PLANE_DST)
		count += 4;
	if (plane->flags & PLANE_ROTATE)
		count++;
	if (plane->flags & PLANE_OVERLAY)
		count++;
	if (plane->flags & PLANE_OVERLAY_CKEY)
		count++;
	if (plane->flags & PLANE_LUT)
		count++;

	return count;
}

static unsigned int count_plane_blobs_one(const struct client_plane_cmd *plane)
{
	unsigned int count = 0;

	if (plane->flags & PLANE_LUT)
		count++;

	return count;
}

static unsigned int count_plane_props(const struct client_plane_cmd *plane, int num_plane)
{
	unsigned int count = 0;
	int i;

	for (i = 0; i < num_plane; i++)
		count += count_plane_props_one(&plane[i]);

	return count;
}

static unsigned int count_plane_blobs(const struct client_plane_cmd *plane, int num_plane)
{
	unsigned int count = 0;
	int i;

	for (i = 0; i < num_plane; i++)
		count += count_plane_blobs_one(&plane[i]);

	return count;
}

static unsigned int count_connector_props_one(const struct client_connector_cmd *connector)
{
	unsigned int count = 0;

	if (connector->flags & CONNECTOR_TV_STD)
		count++;
	if (connector->flags & CONNECTOR_CABLE_TYPE)
		count++;
	if (connector->flags & CONNECTOR_TEXT_FILTER)
		count++;
	if (connector->flags & CONNECTOR_DEFLICKER)
		count++;
	if (connector->flags & CONNECTOR_COLOR_BARS)
		count++;
	if (connector->flags & CONNECTOR_DOT_CRAWL_FREEZE)
		count++;
	if (connector->flags & CONNECTOR_GAMMA)
		count++;

	return count;
}

static unsigned int count_connector_props(const struct client_connector_cmd *connector, int num_connector)
{
	unsigned int count = 0;
	int i;

	for (i = 0; i < num_connector; i++)
		count += count_connector_props_one(&connector[i]);

	return count;
}

static int parse_console_cmd(struct client *client, char *line)
{
	char buf[4096];

	strncpy(buf, line, sizeof buf);

	do {
		char *c;
		char *ptr = buf;
		if (!ptr)
			break;

		c = strchr(buf, '\n');
		if (c)
			*c = '\0';

		if (!strncmp(buf, "open,", 5)) {
			int fd;
			char dev_name[256] = "";

			if (sscanf(buf, "open,%255s", dev_name) != 1)
				continue;

			fd = client_open(client, dev_name, O_RDWR);
			if (fd < 0) {
				printf("open error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("open ok (fd = %d)\n", fd);
			return 1;
		} else if (!strncmp(buf, "close,", 6)) {
			int fd, ret;

			if (sscanf(buf, "close,%d", &fd) != 1)
				continue;

			ret = client_close(client, fd);
			if (ret < 0) {
				printf("close error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("close ok\n");
			return 2;
		} else if (!strncmp(buf, "init,", 5)) {
			int fd, ret;

			if (sscanf(buf, "init,%d", &fd) != 1)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_INIT);
			if (ret < 0) {
				printf("init error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("init ok\n");
			return 3;
		} else if (!strncmp(buf, "fini,", 5)) {
			int fd, ret;

			if (sscanf(buf, "fini,%d", &fd) != 1)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_FINI);
			if (ret < 0) {
				printf("fini error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("fini ok\n");
			return 4;
		} else if (!strncmp(buf, "hardreset,", 10)) {
			int fd, ret;

			if (sscanf(buf, "hardreset,%d", &fd) != 1)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_HARDRESET);
			if (ret < 0) {
				printf("hardreset error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("hardreset ok\n");
			return 5;
		} else if (!strncmp(buf, "setmode,", 8)) {
			int fd, ret;
			struct drm_setmode setmode = { .crtc = 0 };
			const struct drm_display_mode *mode = NULL;
			char crtc_str[32] = "";
			char outputs_str[32] = "";
			char mode_str[32] = "";
			bool enabled;

			if (sscanf(buf, "setmode,%d,%u,%31[^,],%31[^,],%31[^,]",
				   &fd, &setmode.fb_id, crtc_str, outputs_str, mode_str) != 5)
				continue;

			ret = parse_crtc(crtc_str, &setmode.crtc);
			if (ret < 0)
				continue;

			ret = parse_outputs(outputs_str, &setmode.outputs);
			if (ret < 0)
				continue;

			ret = parse_mode(client, mode_str, &setmode.mode);
			if (ret < 0)
				continue;

			printf("setting mode:\n");
			printf("fd = %d\n", fd);
			printf("fb_id = %u\n", setmode.fb_id);
			if (setmode.fb_id)
				print_fb(client, setmode.fb_id);
			printf("crtc = %d\n", setmode.crtc);
			printf("outputs = %d\n", setmode.outputs);
			printf("setting mode:\n");
			print_mode(&setmode.mode);

			ret = client_ioctl(client, fd, DRM_IOC_SET_MODE, &setmode);
			if (ret < 0) {
				printf("setmode error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("setmode ok, modified mode:\n");
			print_mode(&setmode.mode);
			curr_mode[setmode.crtc] = setmode.mode;
			return 6;
		} else if (!strncmp(buf, "addmode,", 8)) {
			char name[64] = "";
			char flags[64] = "";
			unsigned int hd, hss, hse, ht;
			unsigned int vd, vss, vse, vt;
			unsigned int vrefresh;
			struct drm_display_mode mode = { .htotal = 0 };
			const char *p;

			if (sscanf(buf, "addmode,%u,%u,%u,%u,%u,%u,%u,%u,%u,%63[^,],%63[^,]",
				   &hd, &hss, &hse, &ht, &vd, &vss, &vse, &vt, &vrefresh, flags, name) != 11)
				continue;

			name[sizeof name - 1] = '\0';

			mode.hdisplay = hd;
			mode.hblank_start = hd;
			mode.hblank_width = ht - hd;
			mode.hsync_start = hss;
			mode.hsync_width = hse - hss;
			mode.htotal = ht;

			mode.vdisplay = vd;
			mode.vblank_start = vd;
			mode.vblank_width = vt - vd;
			mode.vsync_start = vss;
			mode.vsync_width = vse - vss;
			mode.vtotal = vt;

			mode.vrefresh = vrefresh;

			p = flags;
			while (p) {
				if (*p == 'i')
					mode.flags |= DRM_MODE_FLAG_INTERLACE;
				else if (*p == 'd')
					mode.flags |= DRM_MODE_FLAG_DBLSCAN;
				p++;
			}

			printf("adding mode: %s\n", name);
			print_mode(&mode);

			if (!modelist_add(client, name, &mode))
				return 7;
		} else if (!strncmp(buf, "delmode,", 8)) {
			char name[64];

			if (sscanf(buf, "delmode,%64s", name) != 2)
				continue;

			name[sizeof name - 1] = '\0';

			printf("deleting mode %s\n", name);

			if (!modelist_del(client, name))
				return 8;
		} else if (!strncmp(buf, "suspend,", 8)) {
			int fd, ret;

			if (sscanf(buf, "suspend,%d", &fd) != 1)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_SUSPEND);
			if (ret < 0) {
				printf("suspend error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("suspend ok\n");
			return 9;
		} else if (!strncmp(buf, "resume,", 7)) {
			int fd, ret;

			if (sscanf(buf, "resume,%d", &fd) != 1)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_RESUME);
			if (ret < 0) {
				printf("resume error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("resume ok\n");
			return 10;
		} else if (!strncmp(buf, "fill,", 5)) {
			int fd, ret;

			if (sscanf(buf, "fill,%d", &fd) != 1)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_FILL);
			if (ret < 0) {
				printf("fill error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("fill ok\n");
			return 11;
		} else if (!strncmp(buf, "exit", 4) ||
			   !strncmp(buf, "quit", 4) ||
			   !strncmp(buf, "q", 1)) {
			return -1;
		} else if (!strncmp(buf, "rb,", 3) ||
			   !strncmp(buf, "rw,", 3) ||
			   !strncmp(buf, "rl,", 3)) {
			char size;
			struct drm_reg_rw reg_rw = { .dir = 0 };
			unsigned int reg, val = 0;
			char dev_str[64] = "";
			char eq_chr = '\0';
			int ret;
			int fd;

			ret = sscanf(buf, "r%c,%d,%63[^,],%x%c%x", &size, &fd,
				     dev_str, &reg, &eq_chr, &val);
			if (ret != 4 && ret != 6)
				continue;
			if (ret == 6 && eq_chr != '=')
				continue;

			if (!strncasecmp(dev_str, "dac", 4))
				reg_rw.dev = DRM_REG_RW_DAC;
			else if (!strncasecmp(dev_str, "pci", 4))
				reg_rw.dev = DRM_REG_RW_PCI;
			else if (!strncasecmp(dev_str, "crtc", 5))
				reg_rw.dev = DRM_REG_RW_CRTC;
			else if (!strncasecmp(dev_str, "crtcext", 8))
				reg_rw.dev = DRM_REG_RW_CRTCEXT;
			else if (!strncasecmp(dev_str, "tvo", 4))
				reg_rw.dev = DRM_REG_RW_TVO;
			else if (!strncasecmp(dev_str, "reg", 4))
				reg_rw.dev = DRM_REG_RW_REG;
			else if (!strncasecmp(dev_str, "gctl", 3))
				reg_rw.dev = DRM_REG_RW_GCTL;
			else if (!strncasecmp(dev_str, "seq", 3))
				reg_rw.dev = DRM_REG_RW_SEQ;
			else if (!strncasecmp(dev_str, "attr", 3))
				reg_rw.dev = DRM_REG_RW_ATTR;
			else
				continue;

			reg_rw.dir = eq_chr == '=' ? DRM_REG_RW_WRITE_READ : DRM_REG_RW_READ;
			reg_rw.size = size == 'b' ? DRM_REG_RW_BYTE : size == 'w' ? DRM_REG_RW_WORD : DRM_REG_RW_LONG;
			reg_rw.reg = reg;
			reg_rw.val = val;

			ret = client_ioctl(client, fd, DRM_IOC_REG_RW, &reg_rw);
			if (ret < 0) {
				printf("reg r/w error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("reg r/w ok, %s %x=%x\n", dev_str, reg_rw.reg, reg_rw.val);
			return 12;
		} else if (!strncmp(buf, "memtest,", 4)) {
			struct drm_mem_test mt = { .read_bytes = 0 };
			int ret;
			int fd;

			if (sscanf(buf, "memtest,%d", &fd) != 1)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_MEM_TEST, &mt);
			if (ret < 0) {
				printf("memtest error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			float read = (float)mt.read_bytes;
			float write = (float)mt.write_bytes;
			float clock = (float)mt.clock;
			char *read_unit = "B";
			char *write_unit = "B";
			char *clock_unit = "kHz";

			if (read >= 1024.0f) {
				read /= 1024.0f;
				read_unit = "KiB";
			}
			if (read >= 1024.0f) {
				read /= 1024.0f;
				read_unit = "MiB";
			}
			if (read >= 1024.0f) {
				read /= 1024.0f;
				read_unit = "GiB";
			}
			if (write >= 1024.0f) {
				write /= 1024.0f;
				write_unit = "KiB";
			}
			if (write >= 1024.0f) {
				write /= 1024.0f;
				write_unit = "MiB";
			}
			if (write >= 1024.0f) {
				write /= 1024.0f;
				write_unit = "GiB";
			}
			if (clock >= 1000.0f) {
				clock /= 1000.0f;
				clock_unit = "MHz";
			}

			printf("memtest read = %f %s/sec, write = %f %s/sec, clock = %f %s\n",
			       1000000.0f * read / mt.read_usec, read_unit,
			       1000000.0f * write / mt.write_usec, write_unit,
			       clock, clock_unit);
			return 13;
		} else if (!strncmp(buf, "mkfb,", 5)) {
			int fd, ret;
			struct drm_mk_fb f = { .width = 0, };
			char format[32] = "";

			ret = sscanf(buf, "mkfb,%d,%u,%u,%31[^,],%u,%u,%u", &fd,
				     &f.width, &f.height, format, &f.flags, &f.offsets[0], &f.pitches[0]);
			if (ret != 7)
				continue;

			f.pixel_format = parse_pixel_format(format);

			switch (f.pixel_format) {
			case DRM_FORMAT_NV12:
			case DRM_FORMAT_NV21:
				f.pitches[1] = f.pitches[0];
				f.offsets[1] = f.offsets[0] + f.height * f.pitches[0];
				break;
			case DRM_FORMAT_YUV420:
			case DRM_FORMAT_YVU420:
				f.pitches[1] = f.pitches[2] = f.pitches[0] / 2;
				f.offsets[1] = f.offsets[0] + f.height * f.pitches[0];
				f.offsets[2] = f.offsets[1] + f.height/2 * f.pitches[1];
				break;
			}

			print_mk_fb(&f);

			ret = client_ioctl(client, fd, DRM_IOC_MK_FB, &f);
			if (ret < 0) {
				printf("mkfb error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("mkfb ok\n");
			fblist_add(client, &f);
			print_fb(client, f.fb_id);
			return 14;
		} else if (!strncmp(buf, "rmfb,", 5)) {
			int fd, ret;
			struct drm_rm_fb f = { .fb_id = 0, };

			if (sscanf(buf, "rmfb,%d,%u", &fd, &f.fb_id) != 2)
				continue;

			printf("removing fb:\n");
			print_fb(client, f.fb_id);

			ret = client_ioctl(client, fd, DRM_IOC_RM_FB, &f);
			if (ret < 0) {
				printf("rmfb error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("rmfb ok\n");
			fblist_del(client, f.fb_id);
			return 15;
		} else if (!strncmp(buf, "loadlut,", 8)) {
			int fd, ret;
			struct drm_lut l = { .crtc = 0 };
			char crtc_str[32] = "";
			char format_str[32] = "";
			unsigned int pixel_format = 0;

			if (sscanf(buf, "loadlut,%d,%[^,],%[^,]", &fd, crtc_str, format_str) != 3)
				continue;

			ret = parse_crtc(crtc_str, &l.crtc);
			if (ret < 0)
				continue;

			pixel_format = parse_pixel_format(format_str);

			drm_generate_palette(pixel_format, l.lut);

			printf("loading lut:\n");
			printf("fd = %d\n", fd);
			printf("crtc = %d\n", l.crtc);
			printf("pixel_format = %d\n", pixel_format);

			ret = client_ioctl(client, fd, DRM_IOC_LOAD_LUT, &l);
			if (ret < 0) {
				printf("loadlut error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("loadlut ok\n");
			return 16;
		} else if (!strncmp(buf, "dma,", 4)) {
			int fd, ret;
			u32 dma_buf[7] = {
				0,
				20,
				0x15151515,
				0x00000000,
				0x00000000,
				0x00000000,
				0x00000000,
			};

			if (sscanf(buf, "dma,%d", &fd) != 1)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_DMA_GENERAL_PURPOSE, dma_buf, 20);
			if (ret < 0) {
				printf("dma error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("dma ok (handle = %u)\n", dma_buf[0]);
			return 17;
		} else if (!strncmp(buf, "getres,", 7)) {
			int fd, ret;
			size_t len;
			struct drm_mode_resources *res, *new;
			unsigned int i;

			if (sscanf(buf, "getres,%d", &fd) != 1)
				continue;

			res = calloc(1, sizeof *res);
			if (!res)
				continue;

			ret = client_ioctl(client, fd, DRM_IOC_GETRESOURCES, res, 0);
			if (ret < 0) {
				free(res);
				printf("getres error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("getres phase 1 ok:\n"
			       " count_fb = %u\n"
			       " count_plane = %u\n"
			       " count_crtc = %u\n"
			       " count_encoder = %u\n"
			       " count_connector = %u\n",
			       res->count_fb, res->count_plane,
			       res->count_crtc, res->count_encoder,
			       res->count_connector);

			len = (res->count_fb + res->count_plane + res->count_crtc +
			       res->count_encoder + res->count_connector) * sizeof(uint32_t);

			new = realloc(res, sizeof *res + len);
			if (!new) {
				free(res);
				continue;
			}
			res = new;

			res->fb_id = sizeof *res;
			res->plane_id = res->fb_id + res->count_fb * sizeof(uint32_t);
			res->crtc_id = res->plane_id + res->count_plane * sizeof(uint32_t);
			res->encoder_id = res->crtc_id + res->count_crtc * sizeof(uint32_t);
			res->connector_id = res->encoder_id + res->count_encoder * sizeof(uint32_t);

			ret = client_ioctl(client, fd, DRM_IOC_GETRESOURCES, res, len);
			if (ret < 0) {
				printf("dma error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			res->fb_id = (unsigned long)res + res->fb_id;
			res->plane_id = (unsigned long)res + res->plane_id;
			res->crtc_id = (unsigned long)res + res->crtc_id;
			res->encoder_id = (unsigned long)res + res->encoder_id;
			res->connector_id = (unsigned long)res + res->connector_id;

			printf("getres phase 2 ok:\n"
			       " count_fb = %u\n"
			       " count_plane = %u\n"
			       " count_crtc = %u\n"
			       " count_encoder = %u\n"
			       " count_connector = %u\n",
			       res->count_fb, res->count_plane,
			       res->count_crtc, res->count_encoder,
			       res->count_connector);

			printf(" fb_id:");
			for (i = 0; i < res->count_fb; i++)
				printf(" %u", *((uint32_t *)(unsigned long)res->fb_id + i));
			printf("\n");

			printf(" plane_id:");
			for (i = 0; i < res->count_plane; i++)
				printf(" %u", *((uint32_t *)(unsigned long)res->plane_id + i));
			printf("\n");

			printf(" crtc_id:");
			for (i = 0; i < res->count_crtc; i++)
				printf(" %u", *((uint32_t *)(unsigned long)res->crtc_id + i));
			printf("\n");

			printf(" encoder_id:");
			for (i = 0; i < res->count_encoder; i++)
				printf(" %u", *((uint32_t *)(unsigned long)res->encoder_id + i));
			printf("\n");

			printf(" connector_id:");
			for (i = 0; i < res->count_connector; i++)
				printf(" %u", *((uint32_t *)(unsigned long)res->connector_id + i));
			printf("\n");

			return 18;
		} else if (!strncmp(buf, "atomic,", 7)) {
			int fd, ret;
			size_t len;
			char atomic_str[256] = "";
			struct drm_mode_atomic *ato;
			unsigned int i;
			struct client_crtc_cmd crtc[2] = {};
			unsigned int num_crtc;
			struct client_plane_cmd plane[5] = {};
			unsigned int num_plane;
			struct client_connector_cmd connector[5] = {};
			unsigned int num_connector;
			unsigned int obj_idx = 0;
			unsigned int prop_idx = 0;
			unsigned int blob_idx = 0;
			unsigned long blob_ptr;
			unsigned int num_objs;
			unsigned int num_props;
			unsigned int num_blobs;

			if (sscanf(buf, "atomic,%d,%s", &fd, atomic_str) != 2)
				continue;

			ret = parse_atomic(client, atomic_str,
					   crtc, ARRAY_SIZE(crtc), &num_crtc,
					   plane, ARRAY_SIZE(plane), &num_plane,
					   connector, ARRAY_SIZE(connector), &num_connector);
			if (ret < 0)
				continue;

			num_objs = num_crtc + num_plane;
			/* crtc(mode+connectors) plane(src+dst+fb+crtc+...) connector(tv_std) */
			num_props = count_crtc_props(crtc, num_crtc) +
				count_plane_props(plane, num_plane) +
				count_connector_props(connector, num_connector);
			/* crtc(mode+connectors) plane(lut) */
			num_blobs = count_crtc_blobs(crtc, num_crtc) +
				count_plane_blobs(plane, num_plane);

			/* obj ids, prop counts, prop ids, prop values, blob value ptrs */
			len = num_objs * sizeof(uint32_t) +
				num_objs * sizeof(uint32_t) +
				num_props * sizeof(uint32_t) +
				num_props * sizeof(uint64_t) +
				num_blobs * sizeof(uint64_t);

			/* size of blobs */
			for (i = 0; i < num_crtc; i++) {
				/* connectors data */
				if (crtc[i].flags & CRTC_CONNECTORS)
					len += crtc[i].num_connectors * sizeof(uint32_t);
				/* mode data */
				if (crtc[i].flags & CRTC_MODE &&
				    memchr_inv(&crtc[i].mode, 0, sizeof crtc[i].mode))
					len += sizeof(crtc[i].mode);
			}
			for (i = 0; i < num_plane; i++) {
				/* LUT data */
				if (plane[i].flags & PLANE_LUT)
					len += sizeof plane[i].lut;
			}

			ato = malloc(sizeof *ato + len);
			if (!ato)
				continue;

			ato->flags = 0;
			ato->count_objs = num_objs;
			ato->objs_ptr = sizeof *ato;
			ato->count_props_ptr = ato->objs_ptr + num_objs * sizeof(uint32_t);
			ato->props_ptr = ato->count_props_ptr + num_objs * sizeof(uint32_t);
			ato->prop_values_ptr = ato->props_ptr + num_props * sizeof(uint32_t);
			ato->blob_values_ptr = ato->prop_values_ptr + num_props * sizeof(uint64_t);
			blob_ptr = ato->blob_values_ptr + num_blobs * sizeof(uint64_t);

			for (i = 0; i < num_crtc; i++) {
				atomic_add_obj(ato, crtc[i].crtc, count_crtc_props_one(&crtc[i]), obj_idx++);

				if (crtc[i].flags & CRTC_MODE) {
					size_t blob_len = sizeof crtc[i].mode;
					if (!memchr_inv(&crtc[i].mode, 0, blob_len))
						blob_len = 0;
					atomic_add_blob(ato, DRM_MODE_PROP_MODE, blob_ptr, &crtc[i].mode, blob_len,
							prop_idx++, blob_idx++);
					blob_ptr += blob_len;
				}

				if (crtc[i].flags & CRTC_CONNECTORS) {
					size_t blob_len = crtc[i].num_connectors * sizeof(uint32_t);
					atomic_add_blob(ato, DRM_MODE_PROP_CONNECTOR_IDS, blob_ptr, crtc[i].connectors, blob_len,
							prop_idx++, blob_idx++);
					blob_ptr += blob_len;
				}
			}
			for (i = 0; i < num_plane; i++) {
				atomic_add_obj(ato, plane[i].plane, count_plane_props_one(&plane[i]), obj_idx++);

				if (plane[i].flags & PLANE_CRTC)
					atomic_add_prop(ato, DRM_MODE_PROP_CRTC_ID, plane[i].crtc, prop_idx++);
				if (plane[i].flags & PLANE_FB)
					atomic_add_prop(ato, DRM_MODE_PROP_FB_ID, plane[i].fb, prop_idx++);
				if (plane[i].flags & PLANE_SRC) {
					atomic_add_prop(ato, DRM_MODE_PROP_SRC_X, plane[i].sx, prop_idx++);
					atomic_add_prop(ato, DRM_MODE_PROP_SRC_Y, plane[i].sy, prop_idx++);
					atomic_add_prop(ato, DRM_MODE_PROP_SRC_W, plane[i].sw, prop_idx++);
					atomic_add_prop(ato, DRM_MODE_PROP_SRC_H, plane[i].sh, prop_idx++);
				}
				if (plane[i].flags & PLANE_DST) {
					atomic_add_prop(ato, DRM_MODE_PROP_CRTC_X, plane[i].dx, prop_idx++);
					atomic_add_prop(ato, DRM_MODE_PROP_CRTC_Y, plane[i].dy, prop_idx++);
					atomic_add_prop(ato, DRM_MODE_PROP_CRTC_W, plane[i].dw, prop_idx++);
					atomic_add_prop(ato, DRM_MODE_PROP_CRTC_H, plane[i].dh, prop_idx++);
				}
				if (plane[i].flags & PLANE_ROTATE)
					atomic_add_prop(ato, DRM_MODE_PROP_ROTATE, plane[i].rotate, prop_idx++);
				if (plane[i].flags & PLANE_OVERLAY)
					atomic_add_prop(ato, DRM_MODE_PROP_OVERLAY, plane[i].overlay, prop_idx++);
				if (plane[i].flags & PLANE_OVERLAY_CKEY)
					atomic_add_prop(ato, DRM_MODE_PROP_OVERLAY_COLOR_KEY, plane[i].overlay_ckey, prop_idx++);

				if (plane[i].flags & PLANE_LUT) {
					size_t blob_len = sizeof plane[i].lut;
					atomic_add_blob(ato, DRM_MODE_PROP_LUT, blob_ptr, plane[i].lut, blob_len,
							prop_idx++, blob_idx++);
					blob_ptr += blob_len;
				}
			}
			for (i = 0; i < num_connector; i++) {
				atomic_add_obj(ato, connector[i].connector, count_connector_props_one(&connector[i]), obj_idx++);

				if (connector[i].flags & CONNECTOR_TV_STD)
					atomic_add_prop(ato, DRM_MODE_PROP_TV_STD, connector[i].tv_std, prop_idx++);
				if (connector[i].flags & CONNECTOR_CABLE_TYPE)
					atomic_add_prop(ato, DRM_MODE_PROP_CABLE_TYPE, connector[i].cable_type, prop_idx++);
				if (connector[i].flags & CONNECTOR_TEXT_FILTER)
					atomic_add_prop(ato, DRM_MODE_PROP_TEXT_FILTER, connector[i].text_filter, prop_idx++);
				if (connector[i].flags & CONNECTOR_DEFLICKER)
					atomic_add_prop(ato, DRM_MODE_PROP_DEFLICKER, connector[i].deflicker, prop_idx++);
				if (connector[i].flags & CONNECTOR_COLOR_BARS)
					atomic_add_prop(ato, DRM_MODE_PROP_COLOR_BARS, connector[i].color_bars, prop_idx++);
				if (connector[i].flags & CONNECTOR_DOT_CRAWL_FREEZE)
					atomic_add_prop(ato, DRM_MODE_PROP_DOT_CRAWL_FREEZE, connector[i].dot_crawl_freeze, prop_idx++);
				if (connector[i].flags & CONNECTOR_GAMMA)
					atomic_add_prop(ato, DRM_MODE_PROP_GAMMA, connector[i].gamma, prop_idx++);
			}

			assert(blob_ptr == sizeof *ato + len);

			ret = client_ioctl(client, fd, DRM_IOC_ATOMIC, ato, len);
			if (ret < 0) {
				free(ato);
				printf("atomic error = %d : %s\n", errno, strerror(errno));
				continue;
			}

			printf("atomic ok\n");
			return 19;
		} else if (!strncmp(buf, "help", 4)) {
			printf("supported commands:\n"
			       " open,<device>\n"
			       "  device = eg. /dev/mga01:00.0\n"
			       " close,<fd>\n"
			       " init,<fd>\n"
			       " hardreset,<fd>\n"
			       " setmode,<fd>,<crtc>,<outputs>,<mode>\n"
			       "  crtc = 'crtc1' or 'crtc2'\n"
			       "  outputs = <output>[[:<output>]...]\n"
			       "  output = 'dac1', 'dac2', 'tmds1', 'tmds2' or 'tvout'\n"
			       "  mode = 'off' or <hdisplay>x<vdisplay>-<vrefresh>[r][i][d]\n"
			       "  r = reduced blanking, i = interlaced, d = doublescan\n"
			       "  eg. 640x480-60 or 1920x1200-60r\n"
			       " mkfb,<fd>,<width>,<height>,<pixel_format>,<flags>,<offset>,<pitch>\n"
			       " rmfb,<fd>,<fb_id>\n"
			       " suspend,<fd>\n"
			       " resume,<fd>\n"
			       " addmode,<hd>,<hss>,<hse>,<ht>,<vd>,<vss>,<vse>,<vt>,<vrefresh>,<flags>,<name>\n"
			       "  hd,hss,hse,ht = horizontal display,sync start,sync end,total\n"
			       "  vd,vss,vse,vt = vertical display,sync start,sync end,total\n"
			       "  vrefresh = vertical refresh rate\n"
			       "  flags = 'd' dblscan, 'i' interlace\n"
			       "  name = name of mode\n"
			       " delmode,<name>\n"
			       "  name = name of mode\n"
			       " rb,<fd>,<dev>,<reg>[=<value>]\n"
			       " rw,<fd>,<dev>,<reg>[=<value>]\n"
			       " rl,<fd>,<dev>,<reg>[=<value>]\n"
			       "  read/write registers (register size b,w,l for byte,word,lomg)\n"
			       "  dev = dac|pci|crtc|crtcext|tvo|reg|gctl|seq|attr\n"
			       "  reg = register offset to be read/written\n"
			       "  value = value to be written to register\n"
			       " memtest,<fd>\n"
			       " dma,<fd>\n"
			       " getres,<fd>\n"
			       " atomic,<fd>,(<c>|<p>)[[/(<c>|<p>)]...]\n"
			       "  c = crtc=<crtc id>,mode=<mode>,connectors=<connector id>[[<connector id>]...]\n"
			       "  p = plane=<plane id>,fb=<fb id>,crtc=<crtc id>,src=<coord>,dst=<coord>\n"
			       "  coord = <w>x<h>+<x>+<y>\n"
			       " loadlut,<fd>,<crtc>,<pixel_format>\n"
			       " exit\n");
		} else {
			printf("unknown command (say 'help' to get assistance)\n");
		}
	} while (0);

	printf("foo\n");
	return 0;
}

#define max(a,b) ((a) > (b) ? (a) : (b))

static struct client *rl_client;

static bool quit;
static int last_cmd_class = -1;

static void stdin_handler(char *input)
{
	int r;

	if (!input)
		return;

	fprintf(stderr, "console cmd\n");

	r = parse_console_cmd(rl_client, input);

	if (r < 0) {
		quit = true;
		return;
	}

	if (r != 0) {
		if (r != last_cmd_class) {
			add_history(input);
			last_cmd_class = r;
		} else {
			HIST_ENTRY *ent = replace_history_entry(history_length-1, input, NULL);
			if (ent) {
				printf("replaced base element (%s)\n", ent->line);
				free_history_entry(ent);
			} else
				printf("replace faild\n");
		}
	}
}

static void sighand(int num)
{
}

static int client_loop(struct client *client)
{
	int ret = 0;
	int fd = client->fd;
	void (*old)(int);

	modelist_add(client, "640x480-60", &mode_640_480_60);
	modelist_add(client, "800x600-60", &mode_800_600_60);
	modelist_add(client, "1024x768-60", &mode_1024_768_60);
	modelist_add(client, "1280x1024-60", &mode_1280_1024_60);
	modelist_add(client, "1400x1050-60r", &mode_1400_1050_60_r);
	modelist_add(client, "1400x1050-60", &mode_1400_1050_60);
	modelist_add(client, "1600x1200-60r", &mode_1600_1200_60_r);
	modelist_add(client, "1600x1200-60", &mode_1600_1200_60);
	modelist_add(client, "1680x1050-60r", &mode_1680_1050_60_r);
	modelist_add(client, "1920x1200-60r", &mode_1920_1200_60_r);
	modelist_add(client, "1920x1200-60", &mode_1920_1200_60);

	modelist_add(client, "640x480-tvo", &mode_640_480_tvo);
	modelist_add(client, "800x600-tvo", &mode_800_600_tvo);
	modelist_add(client, "1024x768-tvo", &mode_1024_768_tvo);

	modelist_add(client, "1280x720-60-cea", &mode_1280_720_60_cea);
	modelist_add(client, "1280x720-50-cea", &mode_1280_720_50_cea);

	modelist_add(client, "1920x1080-60-cea", &mode_1920_1080_60_cea);
	modelist_add(client, "1920x1080-50-cea", &mode_1920_1080_50_cea);
	modelist_add(client, "1920x1080-30-cea", &mode_1920_1080_30_cea);
	modelist_add(client, "1920x1080-25-cea", &mode_1920_1080_25_cea);
	modelist_add(client, "1920x1080-24-cea", &mode_1920_1080_24_cea);

	modelist_add(client, "1920x1080i-60-cea", &mode_1920_1080i_60_cea);
	modelist_add(client, "1920x1080i-50-cea", &mode_1920_1080i_50_cea);

	read_history("./.mga_client_history");

	rl_callback_handler_install("> ", stdin_handler);

	old = signal(SIGINT, sighand);

	while (!quit) {
		fd_set rfds, efds;
		int maxfd;

		FD_ZERO(&rfds);
		FD_ZERO(&efds);

		maxfd = max(fd, STDIN_FILENO);

		//FD_SET(fd, &rfds);
		FD_SET(STDIN_FILENO, &rfds);

		FD_SET(fd, &efds);

		ret = select(maxfd + 1, &rfds, NULL, &efds, NULL);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0) {
			fprintf(stderr, "select() failed %d:%s\n",
				errno, strerror(errno));
			break;
		}

		if (FD_ISSET(fd, &efds)) {
			fprintf(stderr, "socket error\n");
			break;
		}
		if (FD_ISSET(fd, &rfds)) {
			fprintf(stderr, "socket closed\n");
			break;
		}
		if (FD_ISSET(STDIN_FILENO, &rfds)) {
			rl_client = client;
			rl_callback_read_char();
			signal(SIGINT, sighand);
			rl_client = NULL;
		}
	}

	rl_callback_handler_remove();

	write_history("./.mga_client_history");
	history_truncate_file("./.mga_client_history", 100);

	close(fd);

	signal(SIGINT, old);

	return ret;
}

static void usage(const char *name)
{
	fprintf(stderr, "Usage: %s <socket eg. /tmp/mga>\n", name);
}

int main(int argc, char *argv[])
{
	struct client client = { .fd = -1 };

	if (argc < 2) {
		usage(argv[0]);
		return 1;
	}

	client.fd = do_connect(argv[1]);
	if (client.fd < 0) {
		usage(argv[0]);
		return 2;
	}

	INIT_LIST_HEAD(&client.modelist);
	INIT_LIST_HEAD(&client.fblist);

	return client_loop(&client);
}
