/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef SPINLOCK_H
#define SPINLOCK_H

#include <pthread.h>

typedef struct spinlock spinlock_t;
struct spinlock {
	pthread_mutex_t mutex;
};

#include "mga_irq_thread.h"

static inline void spin_lock_init(spinlock_t *lock)
{
	pthread_mutex_init(&lock->mutex, NULL);
}

static inline void spin_lock(spinlock_t *lock)
{
	pthread_mutex_lock(&lock->mutex);
}

static inline void spin_unlock(spinlock_t *lock)
{
	pthread_mutex_unlock(&lock->mutex);
}


#define spin_lock_irq(lock) \
do { \
	local_irq_disable(); \
	pthread_mutex_lock(&(lock)->mutex); \
} while (0)

#define spin_unlock_irq(lock) \
do { \
	pthread_mutex_unlock(&(lock)->mutex); \
	local_irq_enable(); \
} while(0)

#define spin_lock_irqsave(lock, flags) \
do { \
	(flags) = 0;			\
	if (!local_irq_is_disabled()) { \
		(flags) = 1; \
		local_irq_disable(); \
	} \
	pthread_mutex_lock(&(lock)->mutex); \
} while (0)

#define spin_unlock_irqrestore(lock, flags) \
do { \
	pthread_mutex_unlock(&(lock)->mutex); \
	if (flags) \
		local_irq_enable(); \
} while (0)

#endif
