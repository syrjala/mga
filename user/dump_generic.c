/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "mga_dump.h"

static void dump_reg(struct mga_dev *mdev, const char *name, u32 addr)
{
	dev_dbg(mdev->dev, "%s = 0x%08x\n", name, mga_read32(mdev, addr));
}

enum {
	MASK_2064W = 1 << MGA_CHIP_2064W,
	MASK_2164W = 1 << MGA_CHIP_2164W,
	MASK_1064SG = 1 << MGA_CHIP_1064SG,
	MASK_1164SG = 1 << MGA_CHIP_1164SG,

	MASK_G100 = 1 << MGA_CHIP_G100,
	MASK_G200 = 1 << MGA_CHIP_G200,
	MASK_G400 = 1 << MGA_CHIP_G400,
	MASK_G450 = 1 << MGA_CHIP_G450,
	MASK_G550 = 1 << MGA_CHIP_G550,

	MASK_G = MASK_G100 | MASK_G200 | MASK_G400 | MASK_G450 | MASK_G550,
	MASK_OLD = MASK_2064W | MASK_1064SG,
	MASK_NEW = MASK_2164W | MASK_1164SG | MASK_G,
	MASK_DMA = MASK_1064SG | MASK_NEW,
	MASK_DAC = MASK_1064SG | MASK_1164SG | MASK_G,
	MASK_G400_UP = MASK_G400 | MASK_G450 | MASK_G550,
	MASK_G200_UP = MASK_G200 | MASK_G400_UP,
	MASK_G100_UP = MASK_G100 | MASK_G200_UP,
	MASK_ALL = MASK_OLD | MASK_NEW,
};

static const struct {
	u32 addr;
	const char *name;
	unsigned int chip_mask;
} registers[] = {
	{ 0x1E00, "WIADDRNB2   ", MASK_G400_UP },
	{ 0x1E04, "WIADDRNB1   ", MASK_G400_UP },
	{ 0x1E08, "WFLAGNB1    ", MASK_G400_UP  },
	{ 0x1E0C, "TEST1       ", MASK_G400_UP },
	{ 0x1E10, "FIFOSTATUS  ", MASK_ALL },
	{ 0x1E14, "STATUS      ", MASK_ALL },
	{ 0x1E1C, "IEN         ", MASK_ALL },
	{ 0x1E20, "VCOUNT      ", MASK_ALL },
	{ 0x1E30, "DMAMAP30    ", MASK_DMA },
	{ 0x1E34, "DMAMAP74    ", MASK_DMA },
	{ 0x1E38, "DMAMAPB8    ", MASK_DMA },
	{ 0x1E3C, "DMAMAPFC    ", MASK_DMA },
	{ 0x1E40, "RST         ", MASK_ALL },
	{ 0x1E44, "MEMRDBK     ", MASK_G100_UP },//hal sets this for G100 too
	{ 0x1E48, "TEST0       ", MASK_G200_UP },
	{ 0x1E4C, "AGP_PLL     ", MASK_G200 },
	{ 0x1E4C, "CFG_OR      ", MASK_G400 },//? G450/G550
	{ 0x1E50, "PRIMPTR     ", MASK_G200_UP },
	{ 0x1E54, "OPMODE      ", MASK_ALL },
	{ 0x1E58, "PRIMADDRESS ", MASK_DMA }, // FIXME mill2/myst?
	{ 0x1E5C, "PRIMEND     ", MASK_DMA }, // FIXME mill2/myst?
	{ 0x1E60, "WIADDRNB    ", MASK_G200_UP },
	{ 0x1E64, "WFLAGNB     ", MASK_G200_UP },
	{ 0x1E6C, "WCODEADDR   ", MASK_G200_UP },
	{ 0x1E70, "WMISC       ", MASK_G200_UP },

	{ 0x2C40, "SECADDRESS  ", MASK_DMA }, // FIXME mill2/myst?
	{ 0x2C44, "SECEND      ", MASK_DMA }, // FIXME mill2/myst?
	{ 0x2C48, "SOFTRAP     ", MASK_DMA }, // FIXME mill2/myst?
	{ 0x2C4C, "DWGSYNC     ", MASK_G200_UP },

	{ 0x2CD0, "SETUPADDRESS", MASK_G200_UP },
	{ 0x2CD4, "SETUPEND    ", MASK_G200_UP },

	{ 0x3C10, "C2CTL       ", MASK_G400_UP },
	{ 0x3C44, "C2MISC      ", MASK_G400_UP },
	{ 0x3C48, "C2VCOUNT    ", MASK_G400_UP },
	{ 0x3C4C, "C2DATACTL   ", MASK_G400_UP },

	{ 0x3D20, "BESCTL      ", MASK_G200_UP },
	{ 0x3DC0, "BESGLOBCTL  ", MASK_G200_UP },
	{ 0x3DC4, "BESSTATUS   ", MASK_G200_UP },

	{ 0x3E30, "VSTATUS     ", MASK_G100_UP },
	{ 0x3E38, "VIEN        ", MASK_G100_UP }, //FIXME WO for G100?

	{ 0x3E4C, "CODECHARDPTR", MASK_G100_UP },
	{ 0x3E50, "CODECLCODE  ", MASK_G100_UP },
};

void dump_generic(struct mga_dev *mdev)
{
	unsigned int i;

	dev_dbg(mdev->dev, "Dump of generic registers:\n");

	for (i = 0; i < ARRAY_SIZE(registers); i++)
		if (registers[i].chip_mask & (1 << mdev->chip))
			dump_reg(mdev,
				 registers[i].name,
				 registers[i].addr);
}
