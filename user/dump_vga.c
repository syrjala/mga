/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "mga_regs.h"
#include "mga_dump.h"

enum {
	MGA_NUM_ATTRX = 0x20,
	MGA_NUM_CRTCX = 0x40,
	MGA_NUM_CRTCEXTX = 0x08,
	MGA_NUM_CRTCEXTX_G400 = 0x09,
	MGA_NUM_GCTLX = 0x10,
	MGA_NUM_SEQX = 0x08,
};

static bool mda_emulation(struct mga_dev *mdev)
{
	u8 val;

	val = mga_read8(mdev, MGA_MISC_R);
	return !(val & MGA_MISC_IOADDSEL);
}

static u32 pick_insts1(struct mga_dev *mdev)
{
	return (mdev->chip == MGA_CHIP_2064W && mda_emulation(mdev)) ?
		MGA_INSTS1_R_MDA : MGA_INSTS1_R;
}

static u32 pick_crtcx(struct mga_dev *mdev)
{
	return (mdev->chip == MGA_CHIP_2064W && mda_emulation(mdev)) ?
		MGA_CRTCX_MDA : MGA_CRTCX;
}

static u32 pick_crtcd(struct mga_dev *mdev)
{
	return (mdev->chip == MGA_CHIP_2064W && mda_emulation(mdev)) ?
		MGA_CRTCD_MDA : MGA_CRTCD;
}

static int pick_num_crtcextx(struct mga_dev *mdev)
{
	return (mdev->chip >= MGA_CHIP_G400) ?
		MGA_NUM_CRTCEXTX_G400 : MGA_NUM_CRTCEXTX;
}

static void dump_attr(struct mga_dev *mdev)
{
	u32 insts1;
	u8 val;
	int i;

	insts1 = pick_insts1(mdev);

	for (i = 0x00; i < MGA_NUM_ATTRX; i++) {
		mga_read8(mdev, insts1);
		mga_write8(mdev, MGA_ATTRX, i);
		val = mga_read8(mdev, MGA_ATTRD_R);

		dev_dbg(mdev->dev, "ATTR%X = 0x%02x\n", i, val);
	}

	mga_read8(mdev, insts1);
	mga_write8(mdev, MGA_ATTRX, MGA_ATTRX_PAS);
}

static void dump_crtc(struct mga_dev *mdev)
{
	u32 crtcx, crtcd;
	u8 val;
	int i;

	crtcx = pick_crtcx(mdev);
	crtcd = pick_crtcd(mdev);

	for (i = 0x00; i < MGA_NUM_CRTCX; i++) {
		mga_write8(mdev, crtcx, i);
		val = mga_read8(mdev, crtcd);

		dev_dbg(mdev->dev, "CRTC%X = 0x%02x\n", i, val);
	}
}

static void dump_crtcext(struct mga_dev *mdev)
{
	int i, num_crtcextx = pick_num_crtcextx(mdev);

	for (i = 0x00; i < num_crtcextx; i++) {
		u8 val;

		mga_write8(mdev, MGA_CRTCEXTX, i);
		val = mga_read8(mdev, MGA_CRTCEXTD);

		dev_dbg(mdev->dev, "CRTCEXT%X = 0x%02x\n", i, val);
	}
}

static void dump_gctl(struct mga_dev *mdev)
{
	int i;

	for (i = 0x00; i < MGA_NUM_GCTLX; i++) {
		u8 val;

		mga_write8(mdev, MGA_GCTLX, i);
		val = mga_read8(mdev, MGA_GCTLD);

		dev_dbg(mdev->dev, "GCTL%X = 0x%02x\n", i, val);
	}
}

static void dump_seq(struct mga_dev *mdev)
{
	int i;

	for (i = 0x00; i < MGA_NUM_SEQX; i++) {
		u8 val;

		mga_write8(mdev, MGA_SEQX, i);
		val = mga_read8(mdev, MGA_SEQD);

		dev_dbg(mdev->dev, "SEQ%X = 0x%02x\n", i, val);
	}
}

static void dump_misc(struct mga_dev *mdev)
{
	u8 val;

	val = mga_read8(mdev, MGA_MISC_R);

	dev_dbg(mdev->dev, "MISC = 0x%02x\n", val);
}

static void dump_feat(struct mga_dev *mdev)
{
	u8 val;

	val = mga_read8(mdev, MGA_FEAT_R);

	dev_dbg(mdev->dev, "FEAT = 0x%02x\n", val);
}

static void dump_insts0(struct mga_dev *mdev)
{
	u8 val;

	val = mga_read8(mdev, MGA_INSTS0_R);

	dev_dbg(mdev->dev, "INSTS0 = 0x%02x\n", val);
}

static void dump_insts1(struct mga_dev *mdev)
{
	u32 insts1;
	u8 val;

	insts1 = pick_insts1(mdev);

	val = mga_read8(mdev, insts1);

	dev_dbg(mdev->dev, "INSTS1 = 0x%02x\n", val);
}

static void dump_dacstat(struct mga_dev *mdev)
{
	u8 val;

	val = mga_read8(mdev, MGA_DACSTAT_R);

	dev_dbg(mdev->dev, "DACSTAT = 0x%02x\n", val);
}

static void dump_cacheflush(struct mga_dev *mdev)
{
	u8 val;

	val = mga_read8(mdev, MGA_CACHEFLUSH);

	dev_dbg(mdev->dev, "CACHEFLUSH = 0x%02x\n", val);
}

void dump_vga(struct mga_dev *mdev)
{
	dev_dbg(mdev->dev, "Dump of VGA registers:\n");

	dump_attr(mdev);
	dump_crtc(mdev);
	dump_crtcext(mdev);
	dump_gctl(mdev);
	dump_seq(mdev);
	dump_misc(mdev);
	dump_feat(mdev);
	dump_insts0(mdev);
	dump_insts1(mdev);
	dump_dacstat(mdev);
	dump_cacheflush(mdev);

	{
	int vtotal = 0;
	u8 val;

	mga_write8(mdev, MGA_CRTCX, 0x06);
	val = mga_read8(mdev, MGA_CRTCD);
	vtotal += val;

	mga_write8(mdev, MGA_CRTCX, 0x07);
	val = mga_read8(mdev, MGA_CRTCD);
	vtotal += (val & 0x01) ? 0x100 : 0;
	vtotal += (val & 0x20) ? 0x200 : 0;

	mga_write8(mdev, MGA_CRTCEXTX, 0x02);
	val = mga_read8(mdev, MGA_CRTCEXTD);
	vtotal += (val & 0x01) ? 0x400 : 0;
	vtotal += (val & 0x02) ? 0x800 : 0;

	dev_dbg(mdev->dev, "VTOTAL = %d\n", vtotal);
	}
}
