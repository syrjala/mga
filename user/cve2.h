/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef CVE2_H
#define CVE2_H

#include "kernel_emul.h"

enum {
	CVE2_ADJ_MODE_DESKTOP,
	CVE2_ADJ_MODE_VIDEO,
};

struct cve2_adjustments {
	u8 brightness;
	u8 contrast;
	u8 saturation;
	u8 hue;
};

struct cve2_bwlevel {
	u16 wlmax;
	u16 blmin;
};

struct cve2_regs {
	u8 regs[0x40];
};

struct cve2_dev {
	struct device *dev;

	void (*write8)(void *data, u8 reg, u8 val);
	void (*write16)(void *data, u8 reg, u16 val);
	u8 (*read8)(void *data, u8 reg);
	u16 (*read16)(void *data, u8 reg);
	void *data;

	/* adjustment defaults */
	const struct cve2_adjustments *desktop_adj_defaults[2];
	const struct cve2_adjustments *video_adj_defaults[2];

	/* bwlevel constants */
	const struct cve2_bwlevel *bwlevel[2];

	/* default register values */
	const struct cve2_regs *regs[2];

	/* current adjustments */
	struct cve2_adjustments desktop_adj[2];
	struct cve2_adjustments video_adj[2];

	bool enabled;
	bool dot_crawl_freeze;
	u8 tv_std;
	u8 adj_mode;
};

void cve2_program_bwlevel(struct cve2_dev *cdev, u8 brightness, u8 contrast);
void cve2_program_hue(struct cve2_dev *cdev, u8 hue);
void cve2_program_saturation(struct cve2_dev *cdev, u8 saturation);
void cve2_program_regs(struct cve2_dev *cdev);
void cve2_program_adjustments(struct cve2_dev *cdev);

void cve2_power(struct cve2_dev *cdev, bool enable);

void cve2_init(struct cve2_dev *cdev,
	       struct device *dev,
	       const struct cve2_adjustments desktop_adj[2],
	       const struct cve2_adjustments video_adj[2],
	       const struct cve2_bwlevel bwlevel[2],
	       const struct cve2_regs regs[2],
	       void (*write8)(void *data, u8 reg, u8 val),
	       void (*write16)(void *data, u8 reg, u16 val),
	       u8 (*read8)(void *data, u8 reg),
	       u16 (*read16)(void *data, u8 reg),
	       void *data);

void cve2_test(struct cve2_dev *cdev,
	       char len, u8 reg, unsigned int val, bool write);

int cve2_set_tv_standard(struct cve2_dev *cdev, unsigned int tv_std);

int cve2_set_dot_crawl_freeze(struct cve2_dev *cdev, bool enable);

unsigned int cve2_get_tv_standard(struct cve2_dev *cdev);

#endif
