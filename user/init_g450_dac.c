/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <limits.h>
#include <errno.h>

#include "mga_dump.h"
#include "mga_g450_dac.h"
#include "mga_regs.h"
#include "mga_dac.h"
#include "mga_dac_regs.h"

static void mga_dac_write8(struct mga_dev *mdev, u8 reg, u8 val)
{
	mga_write8(mdev, MGA_X_INDEXREG, reg);
	mga_write8(mdev, MGA_X_DATAREG, val);
}

static u8 mga_dac_read8(struct mga_dev *mdev, u8 reg)
{
	mga_write8(mdev, MGA_X_INDEXREG, reg);
	return mga_read8(mdev, MGA_X_DATAREG);
}

enum {
	/* FIXME ? */
	MGA_PLL_TIMEOUT = 10, /* 1 ms typical for G100-G400 */
	MGA_PLL_LOCK_COUNT = 100,
	MGA_PLL_LOCK_RELIABILITY = 90,

	MGA_PANEL_LOCK_COUNT = 4,
	MGA_PANEL_LOCK_RELIABILITY = 50,
};

enum {
	/* Rset */
	MGA_GPIO_RSET = 0x40,
};

static const struct mga_dac_pll_info pixplla_info = {
	.name = "PIXPLLA",
	.pllm = MGA_XPIXPLLAM,
	.plln = MGA_XPIXPLLAN,
	.pllp = MGA_XPIXPLLAP,
	.pllstat = MGA_XPIXPLLSTAT,
	.pllstat_lock = MGA_XPIXPLLSTAT_PIXLOCK,
};

static const struct mga_dac_pll_info pixpllb_info = {
	.name = "PIXPLLB",
	.pllm = MGA_XPIXPLLBM,
	.plln = MGA_XPIXPLLBN,
	.pllp = MGA_XPIXPLLBP,
	.pllstat = MGA_XPIXPLLSTAT,
	.pllstat_lock = MGA_XPIXPLLSTAT_PIXLOCK,
};

static const struct mga_dac_pll_info pixpllc_info = {
	.name = "PIXPLLC",
	.pllm = MGA_XPIXPLLCM,
	.plln = MGA_XPIXPLLCN,
	.pllp = MGA_XPIXPLLCP,
	.pllstat = MGA_XPIXPLLSTAT,
	.pllstat_lock = MGA_XPIXPLLSTAT_PIXLOCK,
};

static const struct mga_dac_pll_info syspll_info = {
	.name = "SYSPLL",
	.pllm = MGA_XSYSPLLM,
	.plln = MGA_XSYSPLLN,
	.pllp = MGA_XSYSPLLP,
	.pllstat = MGA_XSYSPLLSTAT,
	.pllstat_lock = MGA_XSYSPLLSTAT_SYSLOCK,
};

static const struct mga_dac_pll_info vidpll_info = {
	.name = "VIDPLL",
	.pllm = MGA_XVIDPLLM,
	.plln = MGA_XVIDPLLN,
	.pllp = MGA_XVIDPLLP,
	.pllstat = MGA_XVIDPLLSTAT,
	.pllstat_lock = MGA_XVIDPLLSTAT_VIDLOCK,
};

static unsigned int pll_calc_fvco(unsigned int fref, int n, int m)
{
	return div_round(2 * fref * (n + 2), m + 1);
}

static unsigned int pll_calc_fo(unsigned int fref, int n, int m, int p)
{
	p = (p & 0x40) ? 0 : ((p & 0x03) + 1);
	return div_round(2 * fref * (n + 2), (m + 1) << p);
}

static unsigned int calc_diff(unsigned int a, unsigned int b)
{
	if (a > b)
		return a - b;
	else
		return b - a;
}

static void get_pixpll(struct mga_dev *mdev,
		       unsigned int *fvco,
		       unsigned int *fo)
{
	struct mga_dac_pll_settings pll;
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);

	if (!(val & MGA_XPIXCLKCTRL_PIXPLLPDN)) {
		*fvco = 0;
		*fo = 0;
		return;
	}

	mga_g450_dac_pixpllc_save(mdev, &pll);
	*fvco = pll.fvco;
	*fo = pll.fo;
}

static void get_vidpll(struct mga_dev *mdev,
		       unsigned int *fvco,
		       unsigned int *fo)
{
	struct mga_dac_pll_settings pll;
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPWRCTRL);

	if (!(val & MGA_XPWRCTRL_VIDPLLPDN)) {
		*fvco = 0;
		*fo = 0;
		return;
	}

	mga_g450_dac_vidpll_save(mdev, &pll);
	*fvco = pll.fvco;
	*fo = pll.fo;
}

static int check_pixpll_vidpll_fvco(unsigned int fvco1, unsigned int fvco2)
{
	unsigned int big, small;

	return 0;

	big = max(fvco1, fvco2);
	small = min(fvco1, fvco2);

	while (big > small)
		big >>= 1;

	if (big == small)
		return -EINVAL;

	return 0;
}

static int check_pixpll_vidpll_fo(unsigned int fo1, unsigned int fo2)
{
	return 0;

	if (fo1 == fo2)
		return -EINVAL;

	return 0;
}

static int
pll_select_best(struct device *dev,
		const struct mga_dac_pll_info *info,
		unsigned int freq, unsigned int fo,
		unsigned int fvco, u8 n, u8 m, u8 p,
		struct mga_dac_pll_settings *pll,
		unsigned int *pll_count)
{
	int i = *pll_count;
	unsigned int diff = calc_diff(fo, freq);

	/* Table full? */
	if (i == MGA_PLL_SETTINGS_MAX) {
		/* Fo is worse */
		if (diff > calc_diff(pll[i-1].fo, freq))
			return -EINVAL;
		i--;
	} else
		(*pll_count)++;

	for (; i > 0; i--) {
		/* Fo is worse */
		if (diff > calc_diff(pll[i-1].fo, freq))
			break;

		pll[i] = pll[i-1];
	}

	/* FIXME check that diff is within some tolerance? */

#if 0
	dev_dbg(dev, "%s: found new %d. best %u kHz (N=%02x M=%02x P=%02x)\n",
		info->name, i+1, fo, n, m, p);
#endif

	pll[i].fvco = fvco;
	pll[i].fo = fo;
	pll[i].n = n;
	pll[i].m = m;
	pll[i].p = p;

	return 0;
}

static int pll_calc(struct mga_dev *mdev,
		    const struct mga_dac_pll_info *info,
		    unsigned int freq,
		    unsigned int fmax,
		    struct mga_dac_pll_settings *pll,
		    unsigned int *pll_count)
{
	unsigned int other_fvco = 0, other_fo = 0;
	int r = -EINVAL;
	int m, n, p;

	if (fmax == 0 || fmax > info->fo_max)
		fmax = info->fo_max;

	dev_dbg(mdev->dev, "%s M: %d - %d\n", info->name, info->m_min, info->m_max);
	dev_dbg(mdev->dev, "%s N: %d - %d\n", info->name, info->n_min, info->n_max);
	dev_dbg(mdev->dev, "%s P: %d - %d\n", info->name, 0, 4);

	*pll_count = 0;

	/*
	 * FIXME if we're doing a modeset involving both
	 * PIXPLL and VIDPLL, we can't simply read out the
	 * registers to know what the state will be.
	 * Neither can we just pass in a single PLL settings
	 * struct for the other PLL, since we don't actually
	 * know which one of the multiple settings we'll end
	 * up using. Ugh. Really nasty. Is there some way to
	 * solve this cleanly?
	 */
	if (info->pllstat == MGA_XPIXPLLSTAT)
		get_vidpll(mdev, &other_fvco, &other_fo);
	else if (info->pllstat == MGA_XVIDPLLSTAT)
		get_pixpll(mdev, &other_fvco, &other_fo);

	for (m = info->m_min; m <= info->m_max; m++) {
	for (n = info->n_min; n <= info->n_max; n++) {
	for (p = 0; p <= 4; p++) {
		unsigned int fvco, fo;
		u8 pll_p = (p == 0) ? 0x40 : (p - 1);

		fvco = pll_calc_fvco(info->fref, n, m);
		if (fvco < info->fvco_min || fvco > info->fvco_max)
			continue;

		if ((info->pllstat == MGA_XPIXPLLSTAT ||
		     info->pllstat == MGA_XVIDPLLSTAT) &&
		    check_pixpll_vidpll_fvco(fvco, other_fvco))
			continue;

		fo = pll_calc_fo(info->fref, n, m, pll_p);
		if (fo > fmax)
			continue;

		if ((info->pllstat == MGA_XPIXPLLSTAT ||
		     info->pllstat == MGA_XVIDPLLSTAT) &&
		    check_pixpll_vidpll_fo(fo, other_fo))
			continue;

		if (pll_select_best(mdev->dev, info, freq, fo, fvco,
				    n, m, pll_p, pll, pll_count))
			continue;

		/* Found at least one valid setting */
		r = 0;
	}
	}
	}

	return r;
}

static void pll_program(struct mga_dev *mdev,
			const struct mga_dac_pll_info *info,
			u8 m, u8 n, u8 p)
{
	dev_dbg(mdev->dev, "Programming %s: N=%02x M=%02x P=%02x\n",
		info->name, n, m, p);

	mga_dac_write8(mdev, info->pllm, m);
	mga_dac_write8(mdev, info->plln, n);
	mga_dac_write8(mdev, info->pllp, p);
}

static void pll_save(struct mga_dev *mdev,
		     const struct mga_dac_pll_info *info,
		     struct mga_dac_pll_settings *pll)
{
	pll->m = mga_dac_read8(mdev, info->pllm);
	pll->n = mga_dac_read8(mdev, info->plln);
	pll->p = mga_dac_read8(mdev, info->pllp);

	pll->fvco = pll_calc_fvco(info->fref, pll->n, pll->m);
	pll->fo = pll_calc_fo(info->fref, pll->n, pll->m, pll->p);

	dev_dbg(mdev->dev, "Saved %s: N=%02x M=%02x P=%02x\n",
		info->name, pll->n, pll->m, pll->p);
}

static int pll_wait(struct mga_dev *mdev,
		    const struct mga_dac_pll_info *info)
{
	unsigned long timeout;
	unsigned int i = 0;
	unsigned int count = 0, locked = 0;
	u8 val;
	u8 lock = info->pllstat_lock;

	val = mga_dac_read8(mdev, info->pllstat);

	/* Wait for the PLL to lock. */
	timeout = jiffies + msecs_to_jiffies(MGA_PLL_TIMEOUT);
	while (!(val & lock) && time_before(jiffies, timeout)) {
		i++;
		//msleep(1);
		val = mga_read8(mdev, MGA_X_DATAREG);
	}

	timeout = jiffies_to_msecs(jiffies - timeout +
				   msecs_to_jiffies(MGA_PLL_TIMEOUT));

	if (!(val & lock)) {
		dev_err(mdev->dev,
			"%s not locked after %lu ms (%u iterations)\n",
			info->name, timeout, i);
		return -ETIMEDOUT;
	}

	dev_dbg(mdev->dev, "%s locked after %lu ms (%u iterations)\n",
		info->name, timeout, i);

	/* Make sure the lock holds some of the time. */
	while (count < MGA_PLL_LOCK_COUNT) {
		count++;
		locked += !!(mga_read8(mdev, MGA_X_DATAREG) & lock);
	}

	if (locked * 100 < count * MGA_PLL_LOCK_RELIABILITY) {
		dev_err(mdev->dev, "%s lock unreliable (%u%%)\n",
			info->name, div_round(100 * locked, count));
		return -ENXIO; // FIXME?
	}

	dev_dbg(mdev->dev, "%s locked reliably (%u%%)\n",
		info->name, div_round(100 * locked, count));

	return 0;
}

static int pll_test(struct mga_dev *mdev,
		    const struct mga_dac_pll_info *info,
		    u8 m, u8 n, u8 p)
{
	pll_program(mdev, info, m, n, p);

	return pll_wait(mdev, info);
}

static int mga_g450_dac_pll_program(struct mga_dev *mdev,
				    const struct mga_dac_pll_info *info,
				    const struct mga_dac_pll_settings *pll,
				    unsigned int pll_count)
{
	unsigned int i;

	for (i = 0; i < pll_count; i++) {
		int j;
		u8 m, n, p;
		u8 s[3] = { 0xff, 0xff, 0xff };

		m = pll[i].m;
		n = pll[i].n;
		p = pll[i].p;

		/* Try 0, -1, +1 loop filter settings. */
		s[0] = p & 0x38;
		if (s[0] > 0x00)
			s[1] = s[0] - 0x8;
		if (s[0] < 0x38)
			s[2] = s[0] + 0x8;

		for (j = 0; j < 3; j++) {
			if (s[j] == 0xff)
				continue;

			p = (p & ~0x38) | s[j];

			/* Must get a lock for -3 <= n <= +3 values. */
			if (!pll_test(mdev, info, m, n - 3, p) &&
			    !pll_test(mdev, info, m, n + 3, p) &&
			    !pll_test(mdev, info, m, n - 2, p) &&
			    !pll_test(mdev, info, m, n + 2, p) &&
			    !pll_test(mdev, info, m, n - 1, p) &&
			    !pll_test(mdev, info, m, n + 1, p) &&
			    !pll_test(mdev, info, m, n    , p))
				return 0;
		}
	}

	return -ENXIO;
}

static int mga_g450_dac_pll_calc(struct mga_dev *mdev,
				 const struct mga_dac_pll_info *info,
				 unsigned int freq,
				 unsigned int fmax,
				 struct mga_dac_pll_settings *pll,
				 unsigned int *pll_count)
{
	struct device *dev = mdev->dev;
	int ret;
	int i;

	dev_dbg(dev, "%s target Fo = %u kHZ\n", info->name, freq);
	dev_dbg(dev, "%s min Fvco = %u kHZ\n", info->name, info->fvco_min);
	dev_dbg(dev, "%s max Fvco = %u kHZ\n", info->name, info->fvco_max);
	dev_dbg(dev, "%s max Fo = %u kHZ\n", info->name, info->fo_max);

	ret = pll_calc(mdev, info, freq, fmax, pll, pll_count);
	if (ret)
		return ret;

	for (i = 0; i < (int) *pll_count; i++) {
		u8 s;

		BUG_ON(pll[i].n < info->n_min || pll[i].n > info->n_max ||
		       pll[i].m < info->m_min || pll[i].m > info->m_max ||
		       (pll[i].p != 0x40 && pll[i].p > 3) ||
		       pll[i].fvco < info->fvco_min ||
		       pll[i].fvco > info->fvco_max ||
		       pll[i].fo > info->fo_max || (fmax && pll[i].fo > fmax));

		/* Loop filter bandwith. */
		for (s = 0; s < 4; s++) {
			if (pll[i].fvco < info->s_limits[s])
				break;
		}
		pll[i].p |= s << 3;

		dev_dbg(dev, "%s NREG=%02x M=%02x P=%02x\n",
			info->name, pll[i].n, pll[i].m, pll[i].p);
		dev_dbg(dev, "%s Fvco = %u kHz, Fo = %u kHz\n",
			info->name, pll[i].fvco, pll[i].fo);
	}

	return 0;
}

int mga_g450_dac_pixplla_calc(struct mga_dev *mdev,
			      unsigned int freq,
			      unsigned int fmax,
			      struct mga_dac_pll_settings *pll,
			      unsigned int *pll_count)
{
	return mga_g450_dac_pll_calc(mdev, &mdev->pixplla_info,
				     freq, fmax, pll, pll_count);
}

int mga_g450_dac_pixpllb_calc(struct mga_dev *mdev,
			      unsigned int freq,
			      unsigned int fmax,
			      struct mga_dac_pll_settings *pll,
			      unsigned int *pll_count)
{
	return mga_g450_dac_pll_calc(mdev, &mdev->pixpllb_info,
				     freq, fmax, pll, pll_count);
}

int mga_g450_dac_pixpllc_calc(struct mga_dev *mdev,
			      unsigned int freq,
			      unsigned int fmax,
			      struct mga_dac_pll_settings *pll,
			      unsigned int *pll_count)
{
	return mga_g450_dac_pll_calc(mdev, &mdev->pixpllc_info,
				     freq, fmax, pll, pll_count);
}

int mga_g450_dac_syspll_calc(struct mga_dev *mdev,
			     unsigned int freq,
			     unsigned int fmax,
			     struct mga_dac_pll_settings *pll,
			     unsigned int *pll_count)
{
	return mga_g450_dac_pll_calc(mdev, &mdev->syspll_info,
				     freq, fmax, pll, pll_count);
}

int mga_g450_dac_vidpll_calc(struct mga_dev *mdev,
			     unsigned int freq,
			     unsigned int fmax,
			     struct mga_dac_pll_settings *pll,
			     unsigned int *pll_count)
{
	return mga_g450_dac_pll_calc(mdev, &mdev->vidpll_info,
				     freq, fmax, pll, pll_count);
}

int mga_g450_dac_pixplla_program(struct mga_dev *mdev,
				 const struct mga_dac_pll_settings *pll,
				 unsigned int pll_count)
{
	return mga_g450_dac_pll_program(mdev, &mdev->pixplla_info, pll, pll_count);
}

int mga_g450_dac_pixpllb_program(struct mga_dev *mdev,
				 const struct mga_dac_pll_settings *pll,
				 unsigned int pll_count)
{
	return mga_g450_dac_pll_program(mdev, &mdev->pixpllb_info, pll, pll_count);
}

int mga_g450_dac_pixpllc_program(struct mga_dev *mdev,
				 const struct mga_dac_pll_settings *pll,
				 unsigned int pll_count)
{
	return mga_g450_dac_pll_program(mdev, &mdev->pixpllc_info, pll, pll_count);
}

int mga_g450_dac_syspll_program(struct mga_dev *mdev,
				const struct mga_dac_pll_settings *pll,
				unsigned int pll_count)
{
	return mga_g450_dac_pll_program(mdev, &mdev->syspll_info, pll, pll_count);
}

int mga_g450_dac_vidpll_program(struct mga_dev *mdev,
				const struct mga_dac_pll_settings *pll,
				unsigned int pll_count)
{
	return mga_g450_dac_pll_program(mdev, &mdev->vidpll_info, pll, pll_count);
}

void mga_g450_dac_pixplla_save(struct mga_dev *mdev,
			       struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->pixplla_info, pll);
}

void mga_g450_dac_pixpllb_save(struct mga_dev *mdev,
			       struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->pixpllb_info, pll);
}

void mga_g450_dac_pixpllc_save(struct mga_dev *mdev,
			       struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->pixpllc_info, pll);
}

void mga_g450_dac_syspll_save(struct mga_dev *mdev,
			      struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->syspll_info, pll);
}

void mga_g450_dac_vidpll_save(struct mga_dev *mdev,
			      struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->vidpll_info, pll);
}

void mga_g450_dac_vidpll_power(struct mga_dev *mdev, bool on)
{
	static const u8 vidpllpdn_vals[] = {
		[false] = 0,
		[true]  = MGA_XPWRCTRL_VIDPLLPDN,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPWRCTRL);
	val &= ~MGA_XPWRCTRL_VIDPLLPDN;
	val |= vidpllpdn_vals[on];
	mga_write8(mdev, MGA_X_DATAREG, val);

	if (on)
		mdev->enabled_plls |= MGA_PLL_VIDPLL;
	else
		mdev->enabled_plls &= ~MGA_PLL_VIDPLL;
}

void mga_g450_dac_panel_power(struct mga_dev *mdev, bool on)
{
	static const u8 panpdn_vals[] = {
		[false] = 0,
		[true]  = MGA_XPWRCTRL_PANPDN,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPWRCTRL);
	val &= ~MGA_XPWRCTRL_PANPDN;
	val |= panpdn_vals[on];
	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_g450_dac_crtc2_power(struct mga_dev *mdev, bool on)
{
	static const u8 cfifopdn_vals[] = {
		[false] = 0,
		[true]  = MGA_XPWRCTRL_CFIFOPDN,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPWRCTRL);
	val &= ~MGA_XPWRCTRL_CFIFOPDN;
	val |= cfifopdn_vals[on];
	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_g450_dac_dac2_power(struct mga_dev *mdev, bool on)
{
	static const u8 dac2pdn_vals[] = {
		[false] = 0,
		[true]  = MGA_XPWRCTRL_DAC2PDN,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPWRCTRL);
	val &= ~MGA_XPWRCTRL_DAC2PDN;
	val |= dac2pdn_vals[on];
	mga_write8(mdev, MGA_X_DATAREG, val);
}

/*
 * CRTC1/CRTC2 -> DAC1
 */
void mga_g450_dac_dac1_dpms(struct mga_dev *mdev,
			    unsigned int mode)
{
	static const u8 dac1soff_vals[] = {
		[DRM_MODE_DPMS_ON]      = 0,
		[DRM_MODE_DPMS_STANDBY] = MGA_XSYNCCTRL_DAC1HSOFF,
		[DRM_MODE_DPMS_SUSPEND] = MGA_XSYNCCTRL_DAC1VSOFF,
		[DRM_MODE_DPMS_OFF]     = MGA_XSYNCCTRL_DAC1HSOFF | MGA_XSYNCCTRL_DAC1VSOFF,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XSYNCCTRL);
	val &= ~(MGA_XSYNCCTRL_DAC1HSOFF | MGA_XSYNCCTRL_DAC1VSOFF);
	val |= dac1soff_vals[mode];
	mga_write8(mdev, MGA_X_DATAREG, val);
}

/*
 * CRTC1/CRTC2 -> DAC2
 */
void mga_g450_dac_dac2_dpms(struct mga_dev *mdev,
			    unsigned int mode)
{
	static const u8 dac2soff_vals[] = {
		[DRM_MODE_DPMS_ON]      = 0,
		[DRM_MODE_DPMS_STANDBY] = MGA_XSYNCCTRL_DAC2HSOFF,
		[DRM_MODE_DPMS_SUSPEND] = MGA_XSYNCCTRL_DAC2VSOFF,
		[DRM_MODE_DPMS_OFF]     = MGA_XSYNCCTRL_DAC2HSOFF | MGA_XSYNCCTRL_DAC2VSOFF,
	};
	u8 val;

	val= mga_dac_read8(mdev, MGA_XSYNCCTRL);
	val &= ~(MGA_XSYNCCTRL_DAC2HSOFF | MGA_XSYNCCTRL_DAC2VSOFF);
	val |= dac2soff_vals[mode];
	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_g450_dac_dac1_source(struct mga_dev *mdev,
			      unsigned int crtc)
{
	static const u8 dac1outsel_vals[] = {
		[MGA_CRTC_NONE]  = MGA_XDISPCTRL_DAC1OUTSEL_DIS,
		[MGA_CRTC_CRTC1] = MGA_XDISPCTRL_DAC1OUTSEL_EN,
		[MGA_CRTC_CRTC2] = MGA_XDISPCTRL_DAC1OUTSEL_EN,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XDISPCTRL);
	val &= ~MGA_XDISPCTRL_DAC1OUTSEL;
	val |= dac1outsel_vals[crtc];
	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_g450_dac_dac2_source(struct mga_dev *mdev, unsigned int crtc, bool tve)
{
	static const u8 dac2outsel_vals[] = {
		[MGA_CRTC_NONE]  = MGA_XDISPCTRL_DAC2OUTSEL_DIS,
		[MGA_CRTC_CRTC1] = MGA_XDISPCTRL_DAC2OUTSEL_CRTC1,
		[MGA_CRTC_CRTC2] = MGA_XDISPCTRL_DAC2OUTSEL_CRTC2,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XDISPCTRL);
	val &= ~MGA_XDISPCTRL_DAC2OUTSEL;
	if (tve) {
		BUG_ON(crtc != MGA_CRTC_CRTC2);
		val |= MGA_XDISPCTRL_DAC2OUTSEL_TVE;
	} else
		val |= dac2outsel_vals[crtc];
	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_g450_dac_panel_source(struct mga_dev *mdev,
			       unsigned int crtc,
			       bool bt656)
{
	static const u8 panoutsel_vals[] = {
		[MGA_CRTC_NONE]  = MGA_XDISPCTRL_PANOUTSEL_DIS,
		[MGA_CRTC_CRTC1] = MGA_XDISPCTRL_PANOUTSEL_CRTC1,
		[MGA_CRTC_CRTC2] = MGA_XDISPCTRL_PANOUTSEL_CRTC2RGB,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XDISPCTRL);

	val &= ~MGA_XDISPCTRL_PANOUTSEL;

	if (bt656) {
		BUG_ON(crtc != MGA_CRTC_CRTC2);
		val |= MGA_XDISPCTRL_PANOUTSEL_CRTC2656;
	} else
		val |= panoutsel_vals[crtc];

	mga_write8(mdev, MGA_X_DATAREG, val);
}

static unsigned int panel_crtc(struct mga_dev *mdev)
{
	u8 val;
	unsigned int crtc;

	val = mga_dac_read8(mdev, MGA_XDISPCTRL);

	switch (val & MGA_XDISPCTRL_PANOUTSEL) {
	case MGA_XDISPCTRL_PANOUTSEL_CRTC1:
		return MGA_CRTC_CRTC1;
	case MGA_XDISPCTRL_PANOUTSEL_CRTC2RGB:
	case MGA_XDISPCTRL_PANOUTSEL_CRTC2656:
		return MGA_CRTC_CRTC2;
	default:
		BUG();
	}
}

static const struct mga_dac_pll_info *panel_pll_info(struct mga_dev *mdev)
{
	unsigned int crtc = panel_crtc(mdev);

	switch (crtc) {
		u8 pixclkctrl;
		u32 c2ctl;

	case MGA_CRTC_CRTC1:
		pixclkctrl = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);

		switch (pixclkctrl & MGA_XPIXCLKCTRL_PIXCLKSL) {
		case MGA_XPIXCLKCTRL_PIXCLKSL_PIXPLL:
			return &mdev->pixpllc_info;
		case MGA_XPIXCLKCTRL_PIXCLKSL_VIDPLL:
			return &mdev->vidpll_info;
		default:
			BUG();
		}
		break;
	case MGA_CRTC_CRTC2:
		c2ctl = mga_read32(mdev, MGA_C2CTL);

		switch (c2ctl & MGA_C2CTL_C2PIXCLKSEL) {
		case MGA_C2CTL_C2PIXCLKSEL_PIXPLL:
			return &mdev->pixpllc_info;
		case MGA_C2CTL_C2PIXCLKSEL_SYSPLL_G450:
			return &mdev->syspll_info;
		case MGA_C2CTL_C2PIXCLKSEL_VIDPLL_G450:
			return &mdev->vidpll_info;
		default:
			BUG();
		}
		break;
	default:
		BUG();
	}
}

static enum mga_pll panel_pll(struct mga_dev *mdev)
{
	unsigned int crtc = panel_crtc(mdev);

	switch (crtc) {
		u8 pixclkctrl;
		u32 c2ctl;

	case MGA_CRTC_CRTC1:
		pixclkctrl = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);

		switch (pixclkctrl & MGA_XPIXCLKCTRL_PIXCLKSL) {
		case MGA_XPIXCLKCTRL_PIXCLKSL_PIXPLL:
			return MGA_PLL_PIXPLL;
		case MGA_XPIXCLKCTRL_PIXCLKSL_VIDPLL:
			return MGA_PLL_VIDPLL;
		default:
			BUG();
		}
		break;
	case MGA_CRTC_CRTC2:
		c2ctl = mga_read32(mdev, MGA_C2CTL);

		switch (c2ctl & MGA_C2CTL_C2PIXCLKSEL) {
		case MGA_C2CTL_C2PIXCLKSEL_PIXPLL:
			return MGA_PLL_PIXPLL;
		case MGA_C2CTL_C2PIXCLKSEL_SYSPLL_G450:
			return MGA_PLL_SYSPLL;
		case MGA_C2CTL_C2PIXCLKSEL_VIDPLL_G450:
			return MGA_PLL_VIDPLL;
		default:
			BUG();
		}
		break;
	default:
		BUG();
	}
}

static int panel_wait(struct mga_dev *mdev)
{
	unsigned int count = 0, locked = 0;
	u8 val;

	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPANCTRL);
	val = mga_read8(mdev, MGA_X_DATAREG);//hack
	mga_write8(mdev, MGA_X_DATAREG, val);//hack

	/* Make sure the lock holds some of the time. */
	while (count < MGA_PANEL_LOCK_COUNT) {
		count++;
		locked += !!(mga_read8(mdev, MGA_X_DATAREG) & MGA_XPANCTRL_PANLOCK); // FIXME
	}

	if (locked * 100 < count * MGA_PANEL_LOCK_RELIABILITY) {
		dev_err(mdev->dev, "Panel lock unreliable (%u%%)\n",
			div_round(100 * locked, count));
		return -ENXIO; // FIXME?
	}

	dev_dbg(mdev->dev, "Panel locked reliably (%u%%)\n",
		div_round(100 * locked, count));

	return 0;
}

static enum mga_pll mga_get_pixclk_select(struct mga_dev *mdev, unsigned int crtc)
{
	switch (crtc) {
		u8 pixclkctrl;
		u32 c2ctl;

	case MGA_CRTC_CRTC1:
		pixclkctrl = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);
		switch (pixclkctrl & MGA_XPIXCLKCTRL_PIXCLKSL) {
		}
		break;
	case MGA_CRTC_CRTC2:
		c2ctl = mga_read32(mdev, MGA_C2CTL);
		switch (c2ctl & MGA_C2CTL_C2PIXCLKSEL) {
		}
		break;
	default:
		BUG();
	}
	return 0;
}

int mga_g450_dac_panel_mode(struct mga_dev *mdev,
			    unsigned int pixclk)
{
	static const unsigned int pan_limits[7] = {
		[0] = 50000,
		[1] = 65000,
		[2] = 85000,
		[3] = 95000,
		[4] = 105000,
		[5] = 120000,
		[6] = 135000,
	};
	unsigned long timeout;
	u8 val;
	u8 p;
	const struct mga_dac_pll_info *info = panel_pll_info(mdev);
	struct mga_dac_pll_settings pll_saved;
	struct mga_dac_pll_settings pll[MGA_PLL_SETTINGS_MAX];
	unsigned int pll_count = 0;
	int r;
	unsigned int crtc = panel_crtc(mdev);
	enum mga_pll pixclk_saved;

	/* Also need to check if we get a lock at pixel clock + 4000. */
	pll_save(mdev, info, &pll_saved);
	r = mga_g450_dac_pll_calc(mdev, info, pll_saved.fo + 4000,
				  0, pll, &pll_count);
	if (r)
		return r;

	val = mga_dac_read8(mdev, MGA_XPANCTRL);
	val |= MGA_XPANCTRL_PANIEDGE; // FIXME
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* Pick the initial setting based on the pixel clock. */
	for (p = 0; p < 7; p++)
		if (pixclk < pan_limits[p])
			break;

	/* Keep increasing the setting until a good one is found. */
	for (; p < 8; p++) {
		val = mga_dac_read8(mdev, MGA_XPANCTRL);
		val &= ~MGA_XPANCTRL_PANMODE; // FIXME
		val |= p << 3;
		mga_write8(mdev, MGA_X_DATAREG, val);

		udelay(250);

		//dev_dbg(mdev->dev, "Testing panel lock wt/ original PLL settings\n");
		/* No good? Try the next setting */
		if (panel_wait(mdev))
			continue;

		/* Highest possible setting? */
		if (p == 7)
			break;

		/* Also check if we get a lock at pixel clock + 4000. */

		switch (crtc) {
		case MGA_CRTC_CRTC1:
			mga_dac_crtc1_pixclk_enable(mdev, false);
			break;
		case MGA_CRTC_CRTC2:
			mga_crtc2_pixclk_enable(mdev, false);
			break;
		}

		r = mga_g450_dac_pll_program(mdev, info, pll, pll_count);
		if (r)
			/* Restore original PLL settings */
			pll_test(mdev, info, pll_saved.m, pll_saved.n, pll_saved.p);

		switch (crtc) {
		case MGA_CRTC_CRTC1:
			mga_dac_crtc1_pixclk_enable(mdev, true);
			break;
		case MGA_CRTC_CRTC2:
			mga_crtc2_pixclk_enable(mdev, true);
			break;
		}

		if (r)
			continue;

		/*
		 * G550: PIXPLL needs delays after enabling pixel clock
		 * G550: VIDPLL seems to do w/o delays
		 */
		udelay(250);

		//dev_dbg(mdev->dev, "Testing panel lock +4000 PLL settings\n");
		r = panel_wait(mdev);
		if (r && p < 7) {
			/* If it doesn't lock just assume the next setting is better. */
			val = mga_read8(mdev, MGA_X_DATAREG);
			val &= ~MGA_XPANCTRL_PANMODE; // FIXME
			val |= (p + 1) << 3;
			mga_write8(mdev, MGA_X_DATAREG, val);

			udelay(250);

			/* just a check for now */
			//dev_dbg(mdev->dev, "Testing panel lock next higher setting\n");
			r = panel_wait(mdev);
		}

		switch (crtc) {
		case MGA_CRTC_CRTC1:
			mga_dac_crtc1_pixclk_enable(mdev, false);
			break;
		case MGA_CRTC_CRTC2:
			mga_crtc2_pixclk_enable(mdev, false);
			break;
		}

		/* Restore original PLL settings */
		pll_test(mdev, info, pll_saved.m, pll_saved.n, pll_saved.p);

		switch (crtc) {
		case MGA_CRTC_CRTC1:
			mga_dac_crtc1_pixclk_enable(mdev, true);
			break;
		case MGA_CRTC_CRTC2:
			mga_crtc2_pixclk_enable(mdev, true);
			break;
		}
		udelay(250);

		/* another test just a check for now */
		//dev_dbg(mdev->dev, "Re-testing panel lock w/ original PLL settings\n");
		panel_wait(mdev);
		r = 0;//assume everyting is kosher
		return r;
	}

	return -ENXIO; // FIXME ?
}

void  mga_g450_dac_dvi_clock(struct mga_dev *mdev,
			     unsigned int crtc,
			     unsigned int pll,
			     bool phase)
{
	u8 val = 0;

#if 0
	val = 0xb9;
	if (phase) {
		val |= 0x2;
		//val |= 0x44;
	}
	val = 0;
#else
	/*
	 * DVI clock enabling needs to happen in phases.
	 * First the DVICLKEN must be enabled for the CRTC,
	 * then the pixel clock for the CRTC must be enabled,
	 * and then the DVICLKSEL bit can be enabled.
	 *
	 * Perhaps that means that DVICLKEN actually redirects
	 * the CRTC's pixel clock input to some DVI circuitry,
	 * enabling the CRTC pixel clock then causes the DVI clock
	 * to tick, and then DVICLKSEL overrides the CRTC's pixel
	 * clock input to come from the DVI clock.
	 *
	 * pixpll-\         0-\                      /-dviclk-\
	 * vidpll--[pixclksl]--[pixclkdis]-[dviclken]--pixclk--[dviclksl]--crtc
	 *  ...
	 */
	/* FIXME figure out if this makes sense */
	if (crtc != MGA_CRTC_NONE) {
		val |= MGA_XDVICLKCTRL_DVIDATAPATHSEL |
			MGA_XDVICLKCTRL_DVILOOPCTL |
			MGA_XDVICLKCTRL_P1LOOPBWDTCTL;
		if (crtc & MGA_CRTC_CRTC1)
			val |= MGA_XDVICLKCTRL_C1DVICLKEN;
		if (crtc & MGA_CRTC_CRTC2)
			val |= MGA_XDVICLKCTRL_C2DVICLKEN;
		if (phase && crtc & MGA_CRTC_CRTC1)
			val |= MGA_XDVICLKCTRL_C1DVICLKSEL;
		if (phase && crtc & MGA_CRTC_CRTC2)
			val |= MGA_XDVICLKCTRL_C2DVICLKSEL;
	}
#endif

	mga_dac_write8(mdev, MGA_XDVICLKCTRL, val);
}

void  mga_g450_dac_dvi_pipe(struct mga_dev *mdev, unsigned int pll, u8 val)
{
	/*
	 * XDVIPIPECTRL bits:
	 * 0x80 not used?
	 * 0x40 PLL selection bit, see code below
	 * 0x20 CRTC2 output copied to TMDS2 output
	 * 0x10 VIDRST signal enable bit (enabled while syncing both CRTCs)
	 * 0x0f shifts TMDS2 image 0x0=leftmost, 0xf=rightmost
	 *      set to 0xd for 32bpp, 0xa otherwise
	 */

	/* 100% sure. Otherwise no picture appears */
	if ((mdev->chip == MGA_CHIP_G550 && pll == MGA_PLL_VIDPLL) ||
	    (mdev->chip == MGA_CHIP_G450 && pll == MGA_PLL_PIXPLL))
		val |= 0x40;

	mga_dac_write8(mdev, MGA_XDVIPIPECTRL, val);
}

void mga_g450_dac_rset(struct mga_dev *mdev,
		       enum mga_rset rset)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XGENIOCTRL);

	switch (rset) {
	case MGA_RSET_0_7_V:
		val &= ~MGA_GPIO_RSET;
		break;
	case MGA_RSET_1_0_V:
		val |= MGA_GPIO_RSET;
		break;
	default:
		BUG();
	}

	mga_write8(mdev, MGA_X_DATAREG, val);
}

static void mga_g450_dac_pll_info_init(struct mga_dac_pll_info *info,
				       unsigned int fref,
				       unsigned int fvco_min,
				       unsigned int fvco_max)
{
	info->fref = fref;
	info->fvco_min = fvco_min;
	info->fvco_max = fvco_max;
	// FIXME
	info->fo_max = fvco_max;

	info->m_min = 0;
	info->m_max = 9;
	info->n_min = 3;
	info->n_max = 122;

	info->s_limits[0] = 550000;
	info->s_limits[1] = 700000;
	info->s_limits[2] = 1000000;
	info->s_limits[3] = 1150000;
}

void mga_g450_dac_init(struct mga_dev *mdev,
		       unsigned int fref,
		       unsigned int pixpll_fvco_min,
		       unsigned int pixpll_fvco_max,
		       unsigned int syspll_fvco_min,
		       unsigned int syspll_fvco_max,
		       unsigned int vidpll_fvco_min,
		       unsigned int vidpll_fvco_max)
{
	mdev->pixplla_info = pixplla_info;
	mdev->pixpllb_info = pixpllb_info;
	mdev->pixpllc_info = pixpllc_info;
	mdev->syspll_info = syspll_info;
	mdev->vidpll_info = vidpll_info;

	mga_g450_dac_pll_info_init(&mdev->pixplla_info,
				   fref, pixpll_fvco_min, pixpll_fvco_max);
	mga_g450_dac_pll_info_init(&mdev->pixpllb_info,
				   fref, pixpll_fvco_min, pixpll_fvco_max);
	mga_g450_dac_pll_info_init(&mdev->pixpllc_info,
				   fref, pixpll_fvco_min, pixpll_fvco_max);
	mga_g450_dac_pll_info_init(&mdev->syspll_info,
				   fref, syspll_fvco_min, syspll_fvco_max);
	mga_g450_dac_pll_info_init(&mdev->vidpll_info,
				   fref, vidpll_fvco_min, vidpll_fvco_max);

	mga_dac_pll_info_print(mdev, &mdev->pixplla_info);
	mga_dac_pll_info_print(mdev, &mdev->pixpllb_info);
	mga_dac_pll_info_print(mdev, &mdev->pixpllc_info);
	mga_dac_pll_info_print(mdev, &mdev->syspll_info);
	mga_dac_pll_info_print(mdev, &mdev->vidpll_info);
}

void mga_g450_dac_powerup(struct mga_dev *mdev)
{
	u8 val;

	/* Disable sync signals */
	mga_g450_dac_dac1_dpms(mdev, DRM_MODE_DPMS_OFF);
	mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_OFF);
	mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_OFF);

	/* Disable outputs */
	mga_g450_dac_dac1_source(mdev, MGA_CRTC_NONE);
	mga_g450_dac_dac2_source(mdev, MGA_CRTC_NONE, false);
	mga_g450_dac_panel_source(mdev, MGA_CRTC_NONE, false);

	mga_dac_powerup(mdev);

	/* Power down eveything except VIDPLL and RFIFO */
	/* FIXME what is RFIFO? */
	val = mga_dac_read8(mdev, MGA_XPWRCTRL);
	val &= MGA_XPWRCTRL_VIDPLLPDN;
	val |= MGA_XPWRCTRL_RFIFOPDN;
	mga_write8(mdev, MGA_X_DATAREG, val);

	mga_g450_dac_rset(mdev, MGA_RSET_0_7_V);

	mga_dac_write8(mdev, MGA_XPANCTRL, 0x00);
	mga_dac_write8(mdev, MGA_XDVIPIPECTRL, 0x00);
	mga_dac_write8(mdev, MGA_XDVICLKCTRL, 0x00);
}

static unsigned int mga_g450_dac_dac1_crtc_sync(struct mga_dev *mdev,
						unsigned int sync)
{
	(void)mdev;
	//return sync & ~(SYNC_ON_GREEN | SYNC_HSYNCPOL | SYNC_VSYNCPOL);
	return sync;
}

static unsigned int mga_g450_dac_dac2_crtc_sync(struct mga_dev *mdev,
						unsigned int sync)
{
	(void)mdev;
	//return sync & ~(SYNC_HSYNCPOL | SYNC_VSYNCPOL);
	return sync;
}

/*
 * CRTC1/CRTC2 -> DAC1
 */
void mga_g450_dac_dac1_set_sync(struct mga_dev *mdev,
				unsigned int sync)
{
	u8 val = mga_dac_read8(mdev, MGA_XSYNCCTRL);

	if (sync & DRM_MODE_FLAG_NHSYNC)
		val |= MGA_XSYNCCTRL_DAC1HSPOL;
	else
		val &= ~MGA_XSYNCCTRL_DAC1HSPOL;

	if (sync & DRM_MODE_FLAG_NVSYNC)
		val |= MGA_XSYNCCTRL_DAC1VSPOL;
	else
		val &= ~MGA_XSYNCCTRL_DAC1VSPOL;

	mga_write8(mdev, MGA_X_DATAREG, val);
}

/*
 * CRTC1/CRTC2 -> DAC2
 */
void mga_g450_dac_dac2_set_sync(struct mga_dev *mdev,
				unsigned int sync)
{
	u8 val = mga_dac_read8(mdev, MGA_XSYNCCTRL);

	if (sync & DRM_MODE_FLAG_NHSYNC)
		val |= MGA_XSYNCCTRL_DAC2HSPOL;
	else
		val &= ~MGA_XSYNCCTRL_DAC2HSPOL;

	if (sync & DRM_MODE_FLAG_NVSYNC)
		val |= MGA_XSYNCCTRL_DAC2VSPOL;
	else
		val &= ~MGA_XSYNCCTRL_DAC2VSPOL;

	mga_write8(mdev, MGA_X_DATAREG, val);
}

int mga_g450_dac_power_down(struct mga_dev *mdev)
{
	mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_OFF);
	mga_g450_dac_dac1_dpms(mdev, DRM_MODE_DPMS_OFF);

	mga_dac_crtc1_cursor_mode(mdev, 0);

	mga_g450_dac_dac2_source(mdev, MGA_CRTC_NONE, false);
	mga_g450_dac_dac1_source(mdev, MGA_CRTC_NONE);

	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	mga_g450_dac_dac2_power(mdev, false);
	mga_dac_dac_power(mdev, false);

	mga_g450_dac_crtc2_power(mdev, false);
	mga_dac_crtc1_power(mdev, false);

	mga_dac_pixpll_power(mdev, false);

	u8 val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);
	dev_dbg(mdev->dev, "CRTC1 PIXCLK is %s\n",
		(val & MGA_XPIXCLKCTRL_PIXCLKDIS) ? "DISABLED":"ENABLED");

	return 0;
}

int mga_g450_dac_power_up(struct mga_dev *mdev, unsigned int clock, bool crtc2)
{
	struct mga_dac_pll_settings pixpll[MGA_PLL_SETTINGS_MAX];
	unsigned int pixpll_count;
	int ret;
	struct mga_dac_regs regs;

	u8 val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);
	dev_dbg(mdev->dev, "pre CRTC1 PIXCLK is %s\n",
		(val & MGA_XPIXCLKCTRL_PIXCLKDIS) ? "DISABLED":"ENABLED");

	ret = mga_g450_dac_pixpllc_calc(mdev, clock, 0, pixpll, &pixpll_count);
	if (ret)
		return ret;

	mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_OFF);
	mga_g450_dac_dac1_dpms(mdev, DRM_MODE_DPMS_OFF);

	mga_dac_crtc1_cursor_mode(mdev, 0);

	mga_g450_dac_dac2_source(mdev, MGA_CRTC_NONE, false);
	mga_g450_dac_dac1_source(mdev, MGA_CRTC_NONE);

	mga_dac_crtc1_pixclk_enable(mdev, false);

	mga_dac_pixpll_power(mdev, true);

	mga_dac_crtc1_power(mdev, true);
	mga_g450_dac_crtc2_power(mdev, true);

	mga_dac_dac_power(mdev, true);
	mga_g450_dac_dac2_power(mdev, true);

	ret = mga_g450_dac_pixpllc_program(mdev, pixpll, pixpll_count);
	if (ret)
		goto error;

	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_PIXPLL);

	mga_dac_crtc1_pixclk_enable(mdev, true);

	//mga_cursor_mode(mdev, 1); // FIXME

	// Restore palette
	if (0 && (mdev->pixel_format == DRM_FORMAT_ARGB8888 ||
		  mdev->pixel_format == DRM_FORMAT_XRGB8888))
		drm_generate_palette(DRM_FORMAT_C8, mdev->palette);
	else
		drm_generate_palette(mdev->pixel_format, mdev->palette);
	mga_dac_crtc1_set_palette(mdev);

	mga_g450_dac_dac1_source(mdev, crtc2 ?
				 MGA_CRTC_CRTC2 : MGA_CRTC_CRTC1);
	mga_g450_dac_dac2_source(mdev, crtc2 ?
				 MGA_CRTC_CRTC2 : MGA_CRTC_CRTC1, false);

	mga_g450_dac_dac1_dpms(mdev, DRM_MODE_DPMS_ON);
	mga_g450_dac_dac2_dpms(mdev, DRM_MODE_DPMS_ON);

	return 0;
 error:
	mga_g450_dac_dac2_power(mdev, false);
	mga_dac_dac_power(mdev, false);
	mga_g450_dac_crtc2_power(mdev, false);
	mga_dac_crtc1_power(mdev, false);
	mga_dac_pixpll_power(mdev, false);

	return ret;
}

static u8 xsynctrl;

/* CRTC1 */
void mga_g450_dac1_monitor_sense_start(struct mga_dev *mdev)
{
	int i;

	/* DPMS off */
	xsynctrl = mga_dac_read8(mdev, MGA_XSYNCCTRL);
	mga_write8(mdev, MGA_X_DATAREG, xsynctrl | MGA_XSYNCCTRL_DAC1HSOFF | MGA_XSYNCCTRL_DAC1VSOFF);

	/* FIXME: Make sure a paletted mode is used... */
	/* FIXME: Make sure video is enabled... */
	/*
	 * Program a uniform color for the entire LUT.
	 * Voltage is 0 mV - 700 mV, detection threshold is 350 mV
	 * (1/2 of the maximum). Since the voltage should double
	 * when the monitor termination is missing aim for 1/3 of the
	 * maximum.
	 */
	mga_write8(mdev, MGA_PALWTADD, 0x00);
	for (i = 0; i < 256 * 3; i++)
		mga_write8(mdev, MGA_PALDATA, 0xFF / 3);

#if 0
	/* FIXME not needed on G450/G550? */
	/* Power up the sense comparator. */
	mga_dac_write8(mdev, MGA_XSENSETEST, MGA_XSENSETEST_SENSEPDN);
#endif
}

/* CRTC1 */
void mga_g450_dac2_monitor_sense_start(struct mga_dev *mdev)
{
	int i;

	/* DPMS off */
	xsynctrl = mga_dac_read8(mdev, MGA_XSYNCCTRL);
	mga_write8(mdev, MGA_X_DATAREG, xsynctrl | MGA_XSYNCCTRL_DAC2HSOFF | MGA_XSYNCCTRL_DAC2VSOFF);

	/* FIXME: Make sure a paletted mode is used... */
	/* FIXME: Make sure video is enabled... */
	/*
	 * Program a uniform color for the entire LUT.
	 * Voltage is 0 mV - 700 mV, detection threshold is 350 mV
	 * (1/2 of the maximum). Since the voltage should double
	 * when the monitor termination is missing aim for 1/3 of the
	 * maximum.
	 */
	mga_write8(mdev, MGA_PALWTADD, 0x00);
	for (i = 0; i < 256 * 3; i++)
		mga_write8(mdev, MGA_PALDATA, 0xFF / 3);

#if 0
	/* FIXME not needed on G450/G550? */
	/* Power up the sense comparator. */
	mga_dac_write8(mdev, MGA_XSENSETEST, MGA_XSENSETEST_SENSEPDN);
#endif
}

/* CRTC1 */
void mga_g450_dac1_monitor_sense_stop(struct mga_dev *mdev,
				      bool *r, bool *g, bool *b)
{
	int i;
	u8 val;

	/* Get the results. */
	val = mga_dac_read8(mdev, MGA_XSENSETEST);
	*r = !(val & MGA_XSENSETEST_RCOMP);
	*g = !(val & MGA_XSENSETEST_GCOMP);
	*b = !(val & MGA_XSENSETEST_BCOMP);

#if 0
	/* FIXME not needed on G450/G550? */
	/* Power down the sense comparator. */
	mga_dac_write8(mdev, MGA_XSENSETEST, 0);
#endif

	/* Restore the LUT. */
	mga_dac_crtc1_set_palette(mdev);

	/* Restore DPMS */
	/* FIXME */
	xsynctrl = mga_dac_read8(mdev, MGA_XSYNCCTRL);
	mga_write8(mdev, MGA_X_DATAREG, xsynctrl);
}

/* CRTC1 */
void mga_g450_dac2_monitor_sense_stop(struct mga_dev *mdev,
				      bool *r, bool *g, bool *b)
{
	int i;
	u8 val;

	/* Get the results. */
	val = mga_dac_read8(mdev, MGA_XSENSETEST);
	*r = !(val & MGA_XSENSETEST_DAC2RCOMP);
	*g = !(val & MGA_XSENSETEST_DAC2GCOMP);
	*b = !(val & MGA_XSENSETEST_DAC2BCOMP);

#if 0
	/* FIXME not needed on G450/G550? */
	/* Power down the sense comparator. */
	mga_dac_write8(mdev, MGA_XSENSETEST, 0);
#endif

	/* Restore the LUT. */
	mga_dac_crtc1_set_palette(mdev);

	/* Restore DPMS */
	/* FIXME */
	xsynctrl = mga_dac_read8(mdev, MGA_XSYNCCTRL);
	mga_write8(mdev, MGA_X_DATAREG, xsynctrl);
}
