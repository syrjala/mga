/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "list.h"

struct mga_mem_node
{
	u32 start, end;
	bool free;
	struct list_head head;
};

struct mga_mem_alloc
{
	struct mutex mutex;
	struct list_head list;
	u32 off;
	u32 len;
	void *virt;
	u32 alignment;
};

static struct mga_mem_node *split_node(struct mga_mem_node *node, size_t new_size)
{
	struct mga_mem_node *new;

	BUG_ON(new_size >= node->end - node->start);
	BUG_ON(!node->free);

	new = kmalloc(sizeof *new, GFP_KERNEL);
	if (!new)
		return ERR_PTR(-ENOMEM);

	new->start = node->start;
	new->end = node->start + new_size;
	new->free = false;
	node->start = new->end;

	list_add_tail(&new->head, &node->head);

	pr_debug("allocated start=%8u end=%8u free=%u\n", new->start, new->end, new->free);

	return new;
}

static struct mga_mem_node *split_node_reverse(struct mga_mem_node *node, size_t new_size)
{
	struct mga_mem_node *new;

	BUG_ON(new_size >= node->end - node->start);
	BUG_ON(!node->free);

	new = kmalloc(sizeof *new, GFP_KERNEL);
	if (!new)
		return ERR_PTR(-ENOMEM);

	new->start = node->end - new_size;
	new->end = node->end;
	new->free = false;
	node->end = new->start;

	list_add(&new->head, &node->head);

	pr_debug("allocated start=%8u end=%8u free=%u\n", new->start, new->end, new->free);

	return new;
}

static struct mga_mem_node *prev_node(struct mga_mem_node *node)
{
	struct mga_mem_node *prev;

	prev = list_entry(node->head.prev, struct mga_mem_node, head);

	if (prev->start >= node->start)
		return NULL;

	BUG_ON(node->start != prev->end);

	return prev;
}

static struct mga_mem_node *next_node(struct mga_mem_node *node)
{
	struct mga_mem_node *next;

	next = list_entry(node->head.next, struct mga_mem_node, head);

	if (next->start <= node->start)
		return NULL;

	BUG_ON(next->start != node->end);

	return next;
}

static void free_node(struct mga_mem_node *node)
{
	struct mga_mem_node *prev = prev_node(node);
	struct mga_mem_node *next = next_node(node);

	BUG_ON(node->free);

	node->free = true;

	/* join w/ prev is prev is free */
	if (prev && prev->free) {
		node->start = prev->start;
		list_del(&prev->head);
		kfree(prev);
	}

	/* join w/ next is next is free */
	if (next && next->free) {
		node->end = next->end;
		list_del(&next->head);
		kfree(next);
	}
}

static void mga_mem_alloc_debug(const char *msg, struct mga_mem_alloc *mem_alloc)
{
	struct mga_mem_node *node;

	pr_debug("%s:\n", msg);

	list_for_each_entry(node, &mem_alloc->list, head)
		pr_debug(" start=%8u end=%8u free=%u\n", node->start, node->end, node->free);
}


struct mga_mem_alloc *mga_mem_alloc_init(u32 off, u32 len, void *virt, u32 alignment)
{
	struct mga_mem_alloc *mem_alloc;
	struct mga_mem_node *node;

	if (!len)
		return ERR_PTR(-EINVAL);
	if (!alignment)
		return ERR_PTR(-EINVAL);

	mem_alloc = kzalloc(sizeof *mem_alloc, GFP_KERNEL);
	if (!mem_alloc)
		return ERR_PTR(-ENOMEM);

	mem_alloc->off = off;
	mem_alloc->len = len;
	mem_alloc->virt = virt;
	mem_alloc->alignment = alignment;

	mutex_init(&mem_alloc->mutex);
	INIT_LIST_HEAD(&mem_alloc->list);

	node = kzalloc(sizeof *node, GFP_KERNEL);
	if (!node) {
		kfree(mem_alloc);
		return ERR_PTR(-ENOMEM);
	}

	node->start = 0;
	node->end = len & ~(alignment - 1);
	node->free = true;

	list_add(&node->head, &mem_alloc->list);

	pr_debug("memory allocator: off=%u len=%u virt=%p alignment=%u\n",
		 off, len, virt, alignment);

	mga_mem_alloc_debug("initial state", mem_alloc);

	return mem_alloc;
}

void mga_mem_alloc_fini(struct mga_mem_alloc *mem_alloc)
{
	struct mga_mem_node *node;

	if (!mem_alloc)
		return;

	mga_mem_alloc_debug("final state", mem_alloc);

	node = list_first_entry(&mem_alloc->list, struct mga_mem_node, head);

	BUG_ON(!node->free);
	BUG_ON(node->start != 0);
	BUG_ON(node->end != mem_alloc->len);

	list_del(&node->head);
	kfree(node);

	BUG_ON(!list_empty(&mem_alloc->list));

	kfree(mem_alloc);
}

int mga_mem_alloc(struct mga_mem_alloc *mem_alloc, u32 *bus, void **virt, size_t size, bool reverse)
{
	struct mga_mem_node *node;
	int ret = -ENOSPC;

	if (!mem_alloc)
		return -ENXIO;

	if (!size || !bus || !virt)
		return -EINVAL;

	size = ALIGN(size, mem_alloc->alignment);

	pr_debug("allocating size=%8u\n", size);

	mutex_lock(&mem_alloc->mutex);

	mga_mem_alloc_debug("before alloc", mem_alloc);

	if (reverse) {
		/* simple first fit, searching from bottom to up */
		list_for_each_entry_reverse(node, &mem_alloc->list, head) {
			struct mga_mem_node *new;

			if (!node->free)
				continue;
			if (node->end - node->start < size)
				continue;

			new = split_node_reverse(node, size);
			if (IS_ERR(new)) {
				ret = PTR_ERR(new);
				break;
			}

			*bus = mem_alloc->off + new->start;
			*virt = mem_alloc->virt + new->start;

			ret = 0;
			break;
		}
	} else {
		/* simple first fit, searching from bottom to up */
		list_for_each_entry(node, &mem_alloc->list, head) {
			struct mga_mem_node *new;

			if (!node->free)
				continue;
			if (node->end - node->start < size)
				continue;

			new = split_node(node, size);
			if (IS_ERR(new)) {
				ret = PTR_ERR(new);
				break;
			}

			*bus = mem_alloc->off + new->start;
			*virt = mem_alloc->virt + new->start;

			ret = 0;
			break;
		}
	}

	mga_mem_alloc_debug("after alloc", mem_alloc);

	mutex_unlock(&mem_alloc->mutex);

	return ret;
}

void mga_mem_free(struct mga_mem_alloc *mem_alloc, u32 bus, size_t size)
{
	struct mga_mem_node *node;
	u32 start, end;
	bool freed = false;

	if (!mem_alloc)
		return;

	size = ALIGN(size, mem_alloc->alignment);

	start = bus - mem_alloc->off;
	end = start + size;

	pr_debug("freeing start=%8u end=%u\n", start, end);

	mutex_unlock(&mem_alloc->mutex);

	mga_mem_alloc_debug("before free", mem_alloc);

	list_for_each_entry(node, &mem_alloc->list, head) {
		if (node->start != start || node->end != end)
			continue;

		free_node(node);
		freed = true;
		break;
	}

	mga_mem_alloc_debug("after free", mem_alloc);

	mutex_unlock(&mem_alloc->mutex);

	BUG_ON(!freed);
}
