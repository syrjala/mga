/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include "wait.h"
#include "spinlock.h"
#include "mga_dump.h"
#include "mga_dma.h"

#include "mga_regs.h"

void mga_irq_init(struct mga_dev *mdev)
{
	spin_lock_init(&mdev->irq.lock);
	init_waitqueue_head(&mdev->irq.wait);

	mdev->irq.ien_requested = 0;
	mdev->irq.ien = 0;
	/*
	 * the kernel driver doesn't clear irq pending condition even
	 * when all file descriptors are closed. So we should do this
	 * here to make sure the irq thread won't go into a loop
	 * when it gets started.
	 */
	mga_write32(mdev, MGA_IEN, 0);
	mga_write32(mdev, MGA_ICLEAR, 0);
}

void mga_irq_fini(struct mga_dev *mdev)
{
	BUG_ON(mdev->irq.ien_requested != 0);
	BUG_ON(mdev->irq.ien != 0);
}

/*
 * Enable the specified IRQ.
 * caller must hold mdev->irq.lock.
 */
void mga_irq_enable(struct mga_dev *mdev, u32 irq)
{
	mdev->irq.ien_requested |= irq;
	mdev->irq.ien |= irq;
	mga_write32(mdev, MGA_ICLEAR, irq);
	mga_write32(mdev, MGA_IEN, mdev->irq.ien);
}

/*
 * Disable the specified IRQ.
 * caller must hold mdev->irq.lock.
 */
void mga_irq_disable(struct mga_dev *mdev, u32 irq)
{
	if (!irq)
		return;

	mdev->irq.ien_requested &= ~irq;
	mdev->irq.ien &= ~irq;
	mga_write32(mdev, MGA_IEN, mdev->irq.ien);
}

/*
 * Decrease the usage count of the specified IRQ.
 * Disable the IRQ if the usage count gots to zero.
 */
void mga_irq_unref(struct mga_dev *mdev, u32 irq)
{
	unsigned int index = fls(irq) - 1;
	unsigned long flags;

	spin_lock_irqsave(&mdev->irq.lock, flags);

	BUG_ON(mdev->irq.ref[index] == 0);

	if (--mdev->irq.ref[index] == 0)
		mga_irq_disable(mdev, irq);

	spin_unlock_irqrestore(&mdev->irq.lock, flags);
}

/*
 * Increase the usage count of the specified IRQ.
 * Enabled the IRQ if it was previous disabled.
 */
void mga_irq_ref(struct mga_dev *mdev,
		 u32 irq)
{
	unsigned int index = fls(irq) - 1;
	unsigned long flags;

	spin_lock_irqsave(&mdev->irq.lock, flags);

	BUG_ON(mdev->irq.ref[index] == 0xff);

	if (mdev->irq.ref[index]++ == 0)
		mga_irq_enable(mdev, irq);

	spin_unlock_irqrestore(&mdev->irq.lock, flags);
}

bool mga_irq_enabled(struct mga_dev *mdev, u32 irq)
{
	unsigned int index = fls(irq) - 1;

	return mdev->irq.ref[index] != 0;
}

static void mga_update_irq_count(struct mga_dev *mdev, u32 status)
{
	u32 index = 0;

	if (!status)
		return;

	while (status) {
		mdev->irq.count[index] += status & 1;
		index++;
		status >>= 1;
	}

	wake_up_all(&mdev->irq.wait);
}

#if 0
static void mga_extint_work(struct work *work)
{
	struct mga_dev *mdev = container_of(work, struct mga_dev, extint_work);

	/* ack the interrupt */
#if 0
	mga_dualhead_check_hpd();
#endif

	/* Re-enable external interrupts */
	spin_lock_irq(&mdev->irq.lock);
	mdev->irq.ien |= mdev->irq.ien_requested & MGA_IEN_EXTEN;
	mga_write32(mdev, MGA_IEN, mdev->irq.ien);
	spin_unlock_irq(&mdev->irq.lock);
}
#endif

/*
 * Interrupt handler
 */
irqreturn_t mga_irq_handler(int irq, void *dev)
{
	struct mga_dev *mdev = dev;
	u32 status;

	spin_lock_irq(&mdev->irq.lock);

	status = mga_read32(mdev, MGA_STATUS) & mdev->irq.ien;
	mga_write32(mdev, MGA_ICLEAR, status);

	dev_dbg(mdev->dev, "%s: STATUS=0x%08x\n", __func__, status);

	/*
	 * External interrupts need special handling.
	 * They must be acked at the source which
	 * can involve slow things like i2c. So disable
	 * external interrupts, and schedule a work to take
	 * care of it later.
	 */
	if (status & MGA_STATUS_EXTPEN) {
		mdev->irq.ien &= ~MGA_IEN_EXTEN;
		mga_write32(mdev, MGA_IEN, mdev->irq.ien);
	}

	mga_update_irq_count(mdev, status);

	spin_unlock_irq(&mdev->irq.lock);

	if (status & MGA_STATUS_SOFTRAPEN)
		mga_dma_handle_softrap(mdev);

#if 0
	if (status & MGA_STATUS_EXTPEN)
		schedule_work(mdev->extint_work);
#endif

	return status ? IRQ_HANDLED : IRQ_NONE;
}

int mga_irq_wait(struct mga_dev *mdev, u32 irq, unsigned int timeout)
{
	u32 index = fls(irq) - 1;
	u8 count = mdev->irq.count[index];
	int ret;

	dev_dbg(mdev->dev, "pre jiffies %lu\n", jiffies);
	dev_dbg(mdev->dev, "waiting for irq 0x%08x, bit=%u\n", irq, index);
	dev_dbg(mdev->dev, "pre count = %u\n", count);

	mga_irq_ref(mdev, irq);

	ret = wait_event_timeout(&mdev->irq.wait, count != mdev->irq.count[index], timeout);

	mga_irq_unref(mdev, irq);

	dev_dbg(mdev->dev, "post count = %u\n", mdev->irq.count[index]);
	dev_dbg(mdev->dev, "waited for irq 0x%08x, bit=%u, result=%d\n", irq, index, ret);
	dev_dbg(mdev->dev, "post jiffies %lu\n", jiffies);

	return ret;
}

int _mga_crtc1_wait_vblank(struct mga_dev *mdev)
{
	unsigned int timeout = HZ;//mga_crtc1_frame_time(mdev);

	return mga_irq_wait(mdev, MGA_IEN_VLINEIEN, timeout);
}

int _mga_crtc2_wait_vblank(struct mga_dev *mdev)
{
	unsigned int timeout = HZ;//mga_crtc2_frame_time(mdev);

	return mga_irq_wait(mdev, MGA_IEN_C2VLINEIEN, timeout);
}
