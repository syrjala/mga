/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <stdint.h>

#include "mga_dump.h"

static u8 pins_get_8(struct mga_dev *mdev)
{
	u8 ret = rom_read8(mdev, mdev->pins_ptr);
	mdev->pins_ptr++;
	return ret;
}

static u16 pins_get_16(struct mga_dev *mdev)
{
	u16 ret = rom_read16(mdev, mdev->pins_ptr);
	mdev->pins_ptr += 2;
	return ret;
}

static u32 pins_get_32(struct mga_dev *mdev)
{
	u32 ret = rom_read32(mdev, mdev->pins_ptr);
	mdev->pins_ptr += 4;
	return ret;
}

static unsigned int pins1_clk_speed(u16 clk)
{
	return clk * 10;
}

static unsigned int pins2_clk_speed(u8 clk, u8 offset)
{
	if (clk == 0xFF)
		return 0;
	return (clk + offset) * 1000;
}

static unsigned int pins4_clk_speed(u8 clk, u8 shift)
{
	if (clk == 0xFF)
		return 0;
	return (clk << shift) * 1000;
}

static unsigned int pins5_clk_speed(u8 clk, u8 mul)
{
	if (clk == 0xFF)
		return 0;
	return (clk * mul) * 1000;
}

static void pins2_dump_vidctrl(struct mga_dev *mdev,
			       const char *name, u8 vidctrl)
{
	static const char *pedestal[] = {
		"0 IRE",
		"7.5 IRE",
	};
	static const char *compsyncen[] = {
		"separate",
		"composite",
	};
	static const char *syncongreen[] = {
		"enable",
		"disable",
	};

	dev_dbg(mdev->dev, "%s = %02x\n", name, vidctrl);
	dev_dbg(mdev->dev, "* Pedestal    = %s\n", pedestal[!!(vidctrl & 0x01)]);
	dev_dbg(mdev->dev, "* CompSyncEn  = %s\n", compsyncen[!!(vidctrl & 0x20)]);
	dev_dbg(mdev->dev, "* SyncOnGreen = %s\n", syncongreen[!!(vidctrl & 0x40)]);
}

static void pins1_dump_vidctrl(struct mga_dev *mdev,
			       const char *name, u8 vidctrl)
{
	pins2_dump_vidctrl(mdev, name, vidctrl ^ 0x40);
}

static void pins3_dump_memtype(struct mga_dev *mdev, const char *name, u8 memtype)
{
	dev_dbg(mdev->dev, "%s = %02x\n", name, memtype);
	dev_dbg(mdev->dev, "* Memory type = %s\n", (memtype & 0x03) ? "SSTL" : "LVTTL");
	dev_dbg(mdev->dev, "* Memory size = %s\n", (memtype & 0x0C) ? "16Mbit" : "8Mbit");
	dev_dbg(mdev->dev, "* Memory read clock delay = %x\n", memtype >> 4);
	dev_dbg(mdev->dev, "* Memconfig = %x\n", memtype & 0x07);
}

static void pins1_dump_clk(struct mga_dev *mdev,
			   const char *name,
			   unsigned int *ret,
			   unsigned int fallback_clock)
{
	unsigned int val = pins_get_16(mdev);
	unsigned int clock = pins1_clk_speed(val);
	if (!clock)
		clock = fallback_clock;
	dev_dbg(mdev->dev, "%s = %u kHz (%04x)\n", name, clock, val);
	if (ret)
		*ret = clock;
}

static void pins2_dump_clk(struct mga_dev *mdev,
			   const char *name,
			   unsigned int *ret,
			   unsigned int fallback_clock)
{
	unsigned int val = pins_get_8(mdev);
	unsigned int clock = pins2_clk_speed(val, 100);
	if (!clock)
		clock = fallback_clock;
	dev_dbg(mdev->dev, "%s = %u kHz (%02x)\n", name, clock, val);
	if (ret)
		*ret = clock;
}

static void pins2_dump_clk2(struct mga_dev *mdev,
			    const char *name,
			    unsigned int *ret,
			    unsigned int fallback_clock)
{
	unsigned int val = pins_get_8(mdev);
	unsigned int clock = pins2_clk_speed(val, 0);
	if (!clock)
		clock = fallback_clock;
	dev_dbg(mdev->dev, "%s = %u kHz (%02x)\n", name, clock, val);
	if (ret)
		*ret = clock;
}

static void pins4_dump_clk(struct mga_dev *mdev,
			   const char *name,
			   unsigned int *ret,
			   unsigned int fallback_clock)
{
	unsigned int val = pins_get_8(mdev);
	unsigned int clock = pins4_clk_speed(val, 2);
	if (!clock)
		clock = fallback_clock;
	dev_dbg(mdev->dev, "%s = %u kHz (%02x)\n", name, clock, val);
	if (ret)
		*ret = clock;
}

static void pins5_dump_clk(struct mga_dev *mdev,
			   const char *name,
			   unsigned int *ret,
			   unsigned int fallback_clock,
			   u8 mul)
{
	unsigned int val = pins_get_8(mdev);
	unsigned int clock = pins5_clk_speed(val, mul);
	if (!clock)
		clock = fallback_clock;
	dev_dbg(mdev->dev, "%s = %u kHz (%02x)\n", name, clock, val);
	if (ret)
		*ret = clock;
}

static void pins3_dump_clkdiv(struct mga_dev *mdev, u8 clkdiv)
{
	dev_dbg(mdev->dev, "ClkDiv Frq  = %02x\n", clkdiv);
	dev_dbg(mdev->dev, "\tGClkDiv   = %u\n", !!(clkdiv & 0x01));
	dev_dbg(mdev->dev, "\tMClkDiv   = %u\n", !!(clkdiv & 0x02));
	dev_dbg(mdev->dev, "\tFMClkDiv  = %u\n", !!(clkdiv & 0x04));
	dev_dbg(mdev->dev, "\tPLLSwap   = %s\n", (clkdiv & 0x08) ? "yes" : "no");
	dev_dbg(mdev->dev, "\tSyncMType = %s\n", (clkdiv & 0x10) ? "SDRAM" : "SGRAM");
	dev_dbg(mdev->dev, "\tfRef      = %u kHz\n", (clkdiv & 0x20) ? 14318 : 27000);
}

static void pins_dump_8(struct mga_dev *mdev, const char *name)
{
	dev_dbg(mdev->dev, "%s = %02x\n", name, pins_get_8(mdev));
}
static void pins_dump_16(struct mga_dev *mdev, const char *name)
{
	dev_dbg(mdev->dev, "%s = %04x\n", name, pins_get_16(mdev));
}
static void pins_dump_32(struct mga_dev *mdev, const char *name)
{
	dev_dbg(mdev->dev, "%s = %08x\n", name, pins_get_32(mdev));
}

static void pins_dump_hex(struct mga_dev *mdev, const char *name, unsigned int len)
{
	dev_dbg(mdev->dev, "%s = ", name);
	while (len--)
		printk(KERN_CONT "%02x", pins_get_8(mdev));
	printk(KERN_CONT "\n");
}

enum {
	PIXCLK_VGA1_DEFAULT = 25175,
	PIXCLK_VGA2_DEFAULT = 28322,
};

void dump_pins1(struct mga_dev *mdev)
{
	static const char *ramdac_type[] = {
		"TVP3026",
		"TVP3027",
	};
	static const unsigned int ramdac_speed[] = {
		175000,
		220000,
		240000,
	};

	unsigned int dac_speed, pclk_max, lclk_max,
		gclk_base, gclk_4mb, gclk_8mb, gclk_module;
	unsigned int i;

	dev_dbg(mdev->dev, "Dump of PInS 1.0:\n");

	pins_dump_16  (mdev, "      PInNS length");
	pins_dump_16  (mdev, "        Product ID");
	pins_dump_hex (mdev, "        Serial No.", 10);
	pins_dump_16  (mdev, "Manufacturing date");
	pins_dump_16  (mdev, "  Manufacturing ID");
	pins_dump_16  (mdev, "          PCB info");
	pins_dump_16  (mdev, "          PMB info");
	dac_speed = ramdac_speed[pins_get_8(mdev)];
	dev_dbg(mdev->dev, "RAMDAC speed = %u kHz\n", dac_speed);
	dev_dbg(mdev->dev, "RAMDAC type = %s\n", ramdac_type[pins_get_8(mdev)]);
	pins1_dump_clk(mdev, "          PCLK max", &pclk_max, dac_speed);
	// FIXME is it really LCLK?
	// FIXME fallback
	pins1_dump_clk(mdev, "          LCLK max", &lclk_max, 85000);
	// FIXME fallbak
	pins1_dump_clk(mdev, "         GCLK base", &gclk_base, 50000);
	pins1_dump_clk(mdev, "         GCLK 4 MB", &gclk_4mb, gclk_base);
	pins1_dump_clk(mdev, "         GCLK 8 MB", &gclk_8mb, gclk_4mb);
	pins1_dump_clk(mdev, "       GCLK module", &gclk_module, gclk_8mb);
	pins1_dump_clk(mdev, "        Test Clock", NULL, 0);
	pins1_dump_clk(mdev, "        PCLK VGA 1", NULL, PIXCLK_VGA1_DEFAULT);
	pins1_dump_clk(mdev, "        PCLK VGA 2", NULL, PIXCLK_VGA2_DEFAULT);
	pins_dump_16  (mdev, "      Program date");
	pins_dump_16  (mdev, "     Program count");
	pins_dump_32  (mdev, "     Ser. No. Ext.");
	pins_dump_32  (mdev, "          Features");
	pins1_dump_clk(mdev, "          GCLK VGA", NULL, 50000);//FIXME?
	pins_dump_16  (mdev, "      PInS version");
	pins1_dump_vidctrl(mdev, "           VidCtrl", pins_get_8(mdev));
	for (i = 0; i < 5; i++)
		pins_dump_8   (mdev, "          Reserved");
}


static void dump_common(struct mga_dev *mdev)
{
	pins_dump_16 (mdev, "PInS signature");
	pins_dump_8  (mdev, "   PInS length");
	pins_dump_8  (mdev, "             ?");
	pins_dump_16 (mdev, "  PInS version");
	pins_dump_16 (mdev, "  Program date");
	pins_dump_16 (mdev, " Program count");
	pins_dump_16 (mdev, "    Product ID");
	pins_dump_hex(mdev, "    Serial No.", 16);
	pins_dump_hex(mdev, "       PL info", 6);
	pins_dump_16 (mdev, "      PCB info");
}

void dump_pins2(struct mga_dev *mdev)
{
	static const char *ramdac_type[] = {
		[0x26] = "TVP3026",
		[0x27] = "TVP3027",
	};

	unsigned int i;
	unsigned int dac_speed, pclk_max, gclk_base,
		mclk_base, gclk_4mb, gclk_8mb, gclk_12mb, gclk_16mb, gclk_module;

	dev_dbg(mdev->dev, "Dump of PInS 2.0:\n");

	dump_common(mdev);

	pins_dump_32   (mdev, "  Feat. flag");
	dev_dbg(mdev->dev,    " RAMDAC type = %s\n", ramdac_type[pins_get_8(mdev)]);
	pins2_dump_clk (mdev, "RAMDAC speed", &dac_speed, 170000);//FIXME?
	pins2_dump_clk (mdev, "    PCLK max", &pclk_max, dac_speed);
	// FIXME is it really LCLK? what about Mystique?
	pins2_dump_clk2(mdev, "   GCLK base", &gclk_base, 50000);//FIXME?//
	pins2_dump_clk2(mdev, "   MCLK base", &mclk_base, gclk_base);//FIXME?
	pins2_dump_clk2(mdev, "   GCLK 4 MB", &gclk_4mb, gclk_base);
	pins2_dump_clk2(mdev, "   GCLK 8 MB", &gclk_8mb, gclk_4mb);
	pins2_dump_clk2(mdev, " GCLK module", &gclk_module, gclk_8mb); //FIXME?
	pins2_dump_clk (mdev, "  Test clock", NULL, 0);
	pins2_dump_clk (mdev, "  PCLK VGA 1", NULL, PIXCLK_VGA1_DEFAULT);
	pins2_dump_clk (mdev, "  PCLK VGA 2", NULL, PIXCLK_VGA2_DEFAULT);
	pins_dump_8    (mdev, "    MCTLWTST");
	pins2_dump_vidctrl(mdev, "     VidCtrl", pins_get_8(mdev));
	pins2_dump_clk2(mdev, "  GCLK 12 MB", &gclk_12mb, gclk_8mb);//FIXME?
	pins2_dump_clk2(mdev, "  GCLK 16 MB", &gclk_16mb, gclk_12mb);
	for (i = 0; i < 8; i++)
		pins_dump_8   (mdev, "    Reserved");
	pins_dump_8    (mdev, "    Checksum");
}

void dump_pins3(struct mga_dev *mdev)
{
	dev_dbg(mdev->dev, "Dump of PInS 3.0:\n");

	dump_common(mdev);

	pins2_dump_clk (mdev, "     VCO max", NULL , 0);
	pins2_dump_clk (mdev, " PCLK max  8", NULL , 0);
	pins2_dump_clk (mdev, " PCLK max 16", NULL , 0);
	pins2_dump_clk (mdev, " PCLK max 24", NULL , 0);
	pins2_dump_clk (mdev, " PCLK max 32", NULL , 0);
	pins2_dump_clk (mdev, "  PCLK VGA 1", NULL, PIXCLK_VGA1_DEFAULT);
	pins2_dump_clk (mdev, "  PCLK VGA 2", NULL, PIXCLK_VGA2_DEFAULT);
	pins2_dump_clk2(mdev, "    GCLK VGA", NULL , 0);
	pins2_dump_clk2(mdev, "GCLK 2D base", NULL , 0);
	pins2_dump_clk2(mdev, "GCLK 2D  2MB", NULL , 0);
	pins2_dump_clk2(mdev, "GCLK 2D  4MB", NULL , 0);
	pins2_dump_clk2(mdev, "GCLK 2D  8MB", NULL , 0);
	pins_dump_32   (mdev, "    MCTLWTST");
	pins3_dump_clkdiv(mdev, pins_get_8(mdev));
	pins2_dump_vidctrl(mdev, "     VidCtrl", pins_get_8(mdev));
	pins3_dump_memtype(mdev, "    Mem Type", pins_get_8(mdev));
	pins_dump_8    (mdev, "   Mem Param");
	pins_dump_16   (mdev, "     MEMRDBK");
	pins_dump_8    (mdev, "     OPTION2");
	pins_dump_8    (mdev, "Factory opts");
	pins2_dump_clk2(mdev, "GCLK 3D     ", NULL , 0);
	pins_dump_8    (mdev, "GCLK 3D  div");
	pins_dump_8    (mdev, "   Derate 3D");
	pins_dump_8    (mdev, "    Checksum");
}

void dump_pins4(struct mga_dev *mdev)
{
	unsigned int i;

	dev_dbg(mdev->dev, "Dump of PInS 4.0:\n");

	dump_common(mdev);

	pins_dump_16   (mdev, "           CFG_OR");
	pins4_dump_clk (mdev, "   SYSPLL VCO max", NULL , 0);
	pins4_dump_clk (mdev, "   PIXPLL VCO max", NULL , 0);
	pins4_dump_clk (mdev, "CRTC1 PCLK max  8", NULL , 0);
	pins4_dump_clk (mdev, "CRTC1 PCLK max 16", NULL , 0);
	pins4_dump_clk (mdev, "CRTC1 PCLK max 24", NULL , 0);
	pins4_dump_clk (mdev, "CRTC1 PCLK max 32", NULL , 0);
	pins4_dump_clk (mdev, "CRTC2 PCLK max 16", NULL , 0);
	pins4_dump_clk (mdev, "CRTC2 PCLK max 32", NULL , 0);
	pins_dump_32   (mdev, "            C2CTL");
	pins_dump_8    (mdev, "           C2MISC");
	pins4_dump_clk (mdev, "       PCLK VGA 1", NULL, PIXCLK_VGA1_DEFAULT);
	pins4_dump_clk (mdev, "       PCLK VGA 2", NULL, PIXCLK_VGA2_DEFAULT);
	pins_dump_8    (mdev, "           OPTION");
	pins2_dump_clk2(mdev, "  Max codec clock", NULL, 0);
	pins2_dump_clk2(mdev, "         GCLK VGA", NULL, 0);
	pins_dump_8    (mdev, "                ?");
	pins_dump_32   (mdev, "      OPTION3 VGA");
	pins_dump_32   (mdev, "     MCTLWTST VGA");
	pins2_dump_clk2(mdev, "          GCLK 2D", NULL, 0);
	pins_dump_8    (mdev, "                ?");
	pins_dump_32   (mdev, "       OPTION3 2D");
	pins_dump_32   (mdev, "      MCTLWTST 2D");
	pins2_dump_clk2(mdev, "          GCLK 3D", NULL, 0);
	pins_dump_8    (mdev, "                ?");
	pins_dump_32   (mdev, "       OPTION3 3D");
	pins_dump_32   (mdev, "      MCTLWTST 3D");
	pins_dump_8    (mdev, "      GCLK derate");
	pins_dump_16   (mdev, "          MEMRDBK");
	pins_dump_16   (mdev, "   MEMRDBK module");
	pins2_dump_vidctrl(mdev, "          VidCtrl", pins_get_8(mdev));
	pins_dump_16   (mdev, "     Factory opts");
	for (i = 0; i < 34; i++)
		pins_dump_8   (mdev, "         Reserved");
	pins_dump_8   (mdev, "         Checksum");
}

void dump_pins5(struct mga_dev *mdev)
{
	u8 mul = rom_read8(mdev, mdev->pins_ptr + 4);
	switch (mul) {
	case 0x00:
		mul = 6;
		break;
	case 0x01:
		mul = 8;
		break;
	default:
		mul = 10;
		break;
	}

	dev_dbg(mdev->dev, "Dump of PInS 5.0:\n");

	dump_common(mdev);

	pins5_dump_clk(mdev, "      SYSPLL VCO max", NULL, 0, mul);
	pins5_dump_clk(mdev, "      VIDPLL VCO max", NULL, 0, mul);
	pins5_dump_clk(mdev, "      PIXPLL VCO max", NULL, 0, mul);
	pins4_dump_clk(mdev, "   CRTC1 PCLK max  8", NULL, 0);
	pins4_dump_clk(mdev, "   CRTC1 PCLK max 16", NULL, 0);
	pins4_dump_clk(mdev, "   CRTC1 PCLK max 24", NULL, 0);
	pins4_dump_clk(mdev, "   CRTC1 PCLK max 32", NULL, 0);
	pins4_dump_clk(mdev, "   CRTC2 PCLK max 16", NULL, 0);
	pins4_dump_clk(mdev, "   CRTC2 PCLK max 32", NULL, 0);
	pins4_dump_clk(mdev, "          PCLK VGA 1", NULL, PIXCLK_VGA1_DEFAULT);
	pins4_dump_clk(mdev, "          PCLK VGA 2", NULL, PIXCLK_VGA2_DEFAULT);
	pins4_dump_clk(mdev, "      PCLK PanelLink", NULL, 0);
	pins_dump_32  (mdev, "              OPTION");
	pins_dump_32  (mdev, "             OPTION2");

	pins4_dump_clk(mdev, "          SYSPLL VGA", NULL, 0);
	pins4_dump_clk(mdev, "          VIDPLL VGA", NULL, 0);
	pins_dump_32  (mdev, "         OPTION3 VGA");
	pins_dump_32  (mdev, "        MCTLWTST VGA");
	pins_dump_32  (mdev, "         MEMMISC VGA");
	pins_dump_32  (mdev, "         MEMRDBK VGA");

	pins4_dump_clk(mdev, "           SYSPLL SH", NULL, 0);
	pins4_dump_clk(mdev, "           VIDPLL SH", NULL, 0);
	pins_dump_32  (mdev, "          OPTION3 SH");
	pins_dump_32  (mdev, "         MCTLWTST SH");
	pins_dump_32  (mdev, "          MEMMISC SH");
	pins_dump_32  (mdev, "          MEMRDBK SH");

	pins4_dump_clk(mdev, "           SYSPLL DH", NULL, 0);
	pins4_dump_clk(mdev, "           VIDPLL DH", NULL, 0);
	pins_dump_32  (mdev, "          OPTION3 DH");
	pins_dump_32  (mdev, "         MCTLWTST DH");
	pins_dump_32  (mdev, "          MEMMISC DH");
	pins_dump_32  (mdev, "          MEMRDBK DH");

	pins_dump_32  (mdev, "        Factory opts");
	pins_dump_16  (mdev, "            Mem info");
	pins_dump_16  (mdev, "           Disp info");

	pins4_dump_clk(mdev, "      DAC1 max clock", NULL, 0);
	pins4_dump_clk(mdev, "      DAC2 max clock", NULL, 0);

	pins_dump_8   (mdev, "                Rset");

	pins5_dump_clk(mdev, "      SYSPLL VCO min", NULL, 0, mul);
	pins5_dump_clk(mdev, "      VIDPLL VCO min", NULL, 0, mul);
	pins5_dump_clk(mdev, "      PIXPLL VCO min", NULL, 0, mul);

	pins4_dump_clk(mdev, "CRTC1 PCLK max 32 DH", NULL, 0);
	pins4_dump_clk(mdev, "CRTC2 PCLK max 32 DH", NULL, 0);

	pins_dump_8   (mdev, "            Reserved");
	pins_dump_8   (mdev, "            Checksum");
}
