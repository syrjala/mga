/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <stdlib.h>

#include "mga_dump.h"
#include "mga_regs.h"
#include "mga_i2c.h"
#include "mga_kms.h"
#include "modes.h"

#if 0
static unsigned int mga_2064w_align(unsigned int bpp)
{
	if (interleave) {
		static const u8 videopitch_multiple_of[] = { 128, 64, 128, 32 };
		offset = videopitch * bpp / 128;
		static const u8 startadd_factor[] = { 24, 12, 8, 6 };
		startadd = 3 * address / factor;
	} else {
		static const u8 videopitch_multiple_of[] = { 64, 32, 64, 16 };
		offset = videopitch * bpp / 64;
		static const u8 startadd_factor[] = { 12, 6, 4, 3 };
		startadd = 3 * address / factor;
	}
}
#endif

static unsigned int bpp_to_index(unsigned int bpp)
{
	return bpp / 8 - 1;
}

unsigned int mga_2064w_alignment(unsigned int bpp, bool interleave)
{
	/* FIXME isn't 96 betkter for 24bpp? */
	static const u8 values[] = { 64, 32, 64, 16 };
	return values[bpp_to_index(bpp)] << interleave;
}

static void mga_2064w_softreset(struct mga_dev *mdev)
{
	dev_dbg(mdev->dev, "Performing soft reset\n");
	mga_write32(mdev, MGA_RST, MGA_RST_SOFTRESET);
	udelay(200);
	mga_write32(mdev, MGA_RST, 0);
	udelay(200);

	dev_dbg(mdev->dev, "Waiting for VBL\n");
	mga_crtc1_wait_vblank(mdev);

	dev_dbg(mdev->dev, "Enabling video\n");
	mga_crtc1_video(mdev, true);

	dev_dbg(mdev->dev, "Waiting for VBL\n");
	mga_crtc1_wait_vblank(mdev);

	dev_dbg(mdev->dev, "Performing mem reset\n");
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_MEMRESET);
	udelay(1);

	dev_dbg(mdev->dev, "Initializing drawing engine\n");
	mga_determine_fifo_size(mdev);
	mga_solid_fill(mdev);

	dev_dbg(mdev->dev, "Waiting for drawing engine idle\n");
	mga_wait_dwgeng_idle(mdev);
}

static void mga_2064w_rfhcnt(struct mga_dev *mdev, int gclk)
{
	int rfhcnt = 0;
	u32 option;

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);

	if (gclk > 0) {
		int gscaling_factor = (option & MGA_OPTION_NOGSCALE) ? 1 : 4;

		gclk *= 1000;

		/*
		 * Program (round the fraction to the nearest integer):
		 * rfhcnt = (ram refresh period us * gclk_frequency Mhz / gscaling_factor / 128) - 1
		 * The gscaling_factor is tied to the value of nogscale, as shown below:
		 * nogscale   gscaling_factor
		 * 0          4
		 * 1          1
		 */
		rfhcnt = (gclk - 64 * 30120 * gscaling_factor) / (128 * 30120 * gscaling_factor);
		rfhcnt = clamp(rfhcnt, 0x0, 0xF);
	}

	dev_dbg(mdev->dev, "Setting rfhcnt to 0x%01x\n", rfhcnt);

	option &= ~MGA_OPTION_RFHCNT_OLD;
	option |= rfhcnt << 16;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static void framebuffer_init(struct mga_dev *mdev)
{
	(void)mdev;
#if 0
	mga_write32(mdev, MGA_OPMODE,
	       MGA_DMAMOD_GENERAL_PURPOSE |
	       MGA_DMADATASIZ_8 |
	       MGA_DIRDATASIZ_8);
	mga_write32(mdev, MGA_PLNWT, 0xFFFFFFFF);
	mga_write32(mdev, MGA_MACCESS, 0);
	mga_write32(mdev, MGA_YDSTORG, 0);
	mga_write32(mdev, MGA_ZORG, 0);
#endif
}

/* scanout pitch alignment */
static unsigned int calc_offset(struct mga_dev *mdev,
				unsigned int pitch, unsigned int bpp, bool interleave)
{
	unsigned int fsplit = 1;

	if (mdev->chip >= MGA_CHIP_1064SG)
		return DIV_ROUND_UP(pitch * bpp * fsplit, 128);
	else
		return DIV_ROUND_UP(pitch * bpp, 128 >> !interleave);
}

static void mga_2064w_init(struct mga_dev *mdev)
{
	struct drm_display_mode mode = { .vrefresh = 0 };
	unsigned int outputs = 0;

	mode = mode_640_480_60;

	outputs |= MGA_OUTPUT_DAC1;

	mga_set_initial_crtc1_mode(mdev,
				   mdev->pixel_format,
				   &mode,
				   outputs);

	static const u32 interleave_vals[] = {
		0,
		MGA_OPTION_INTERLEAVE,
	};
	const struct mga_pins1 *pins = &mdev->pins.pins1;
	unsigned int gclk = pins->clk_2d.gclk[0 >> 2];
	u32 option;
	struct mga_crtc1_regs regs = { .crtc[0] = 0 };
	const struct drm_display_mode *pmode;
	struct drm_framebuffer *pfb;

	/* FIXME use interleave from the beginning in PInS reports base memory >= 4MB */
	mdev->interleave = false;
	mdev->nogscale = !!(pins->option & MGA_OPTION_NOGSCALE);

	mga_misc_disable_vga(mdev);

	dev_dbg(mdev->dev, "Disabling CRTC1\n");
	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);
	mga_crtc1_dpms(mdev, DRM_MODE_DPMS_OFF);

	dev_dbg(mdev->dev, "Setting interleave to %d\n", mdev->interleave);
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_INTERLEAVE;
	option |= interleave_vals[mdev->interleave];
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	dev_dbg(mdev->dev, "Initializing TVP3026\n");
	tvp_init(&mdev->tdev, mdev->dev, mdev->mmio_virt + 0x3C00,
		 pins->fref, 0, max(220000, pins->fvco_max),
		 pins->ldclk_max, pins->fvco_max, 0);

	mga_misc_powerup(mdev);

	dev_dbg(mdev->dev, "Powering up TVP3026\n");
	tvp_powerup(&mdev->tdev, gclk);

	dev_dbg(mdev->dev, "Programming nogscale\n");
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_NOGSCALE;
	option |= pins->option & MGA_OPTION_NOGSCALE;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	dev_dbg(mdev->dev, "Powering up CRTC1\n");
	mga_crtc1_powerup(mdev);

	mga_get_crtc1_mode(mdev, &pmode);
	mga_get_crtc1_fb(mdev, &pfb);
	mode = *pmode;
	const struct mga_plane_config pc = {
		.fb = pfb,
		.hscale = 0x10000,
		.vscale = 0x10000,
	};
	struct mga_crtc_config cc = {
		.adjusted_mode = mode,
	};
	mga_crtc1_calc_mode(mdev, &pc, &cc, &regs);
	mga_crtc1_restore(mdev, &regs);
	tvp_start(&mdev->tdev, pfb->pixel_format,
		  mga_calc_pixel_clock(&mode, 1),
		  mdev->interleave ? TVP_BUS_WIDTH_64 : TVP_BUS_WIDTH_32, 1);
	framebuffer_init(mdev);

	dev_dbg(mdev->dev, "Going to enable hsync/vsync\n");
	mga_crtc1_dpms(mdev, DRM_MODE_DPMS_ON);

	dev_dbg(mdev->dev, "Going to calculate rhfcnt\n");
	mga_2064w_rfhcnt(mdev, gclk);

	dev_dbg(mdev->dev, "Going to soft reset\n");
	mga_2064w_softreset(mdev);

	mga_i2c_tvp_ddc_init(mdev);

	mdev->outputs |= MGA_OUTPUT_DAC1;

	mga_disable_outputs(mdev);

	mga_probe_mem_size(mdev);

	/*
	 * 4 MB boards could be used in non-interleave mode. It would relax
	 * some alignment constrainst but it would also set the bank boundary to
	 * 2 MB so placement of the scanout buffer would be harder and it reduces
	 * the RAMDAC serial port bandwith so it reduces the maximum resolution
	 * or refresh rate. So use interleave mode if possible.
	 */
	if (!mdev->interleave && mdev->mem_size >= 0x400000) {
		mdev->interleave = true;

		dev_dbg(mdev->dev, "Setting interleave to %d\n", mdev->interleave);
		option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
		option &= ~MGA_OPTION_INTERLEAVE;
		option |= interleave_vals[mdev->interleave];
		pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
	}

	// FIXME gclk is the value we asked from the PLL, not the actual freq we got
	mdev->mclk = gclk;

	/* increase memory clock when possible */
	gclk = pins->clk_2d.gclk[mdev->mem_size >> 22];
	if (gclk != mdev->mclk) {
		struct tvp_pll_settings pll;

		if (tvp_mclkpll_calc(&mdev->tdev, gclk, 0, &pll) == 0 &&
		    tvp_mclkpll_program_safe(&mdev->tdev, &pll) == 0)
			mdev->mclk = pll.fpll;
	}

	//
	mga_write32(mdev, MGA_PLNWT, 0xFFFFFFFF);
}

static void mga_2064w_test(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");

	mga_enable_outputs(mdev);

	// fixme
	//	if (mdev_outputs & MGA_OUTPUT_DAC1)
	//	mdev->funcs->monitor_sense_dac1(mdev);

	if (strstr(test, "reg"))
		mga_reg_test(mdev);

	mga_disable_outputs(mdev);
}

static int mga_2064w_suspend(struct mga_dev *mdev)
{
	/* FIXME implement */
	return -ENOSYS;
}

static int mga_2064w_resume(struct mga_dev *mdev)
{
	/* FIXME implement */
	return -ENOSYS;
}

void mga_2064w_monitor_sense(struct mga_dev *mdev)
{
	bool r, g, b;

	mga_crtc1_wait_vblank(mdev);
	tvp_monitor_sense_start(&mdev->tdev);
	mga_crtc1_wait_vblank(mdev);
	tvp_monitor_sense_stop(&mdev->tdev, &r, &g, &b);

	dev_dbg(mdev->dev, "Monitor sense = %s%s%s\n",
		r ? "R" : ".",
		g ? "G" : ".",
		b ? "B" : ".");
}

static const struct mga_chip_funcs mga_2064w_funcs = {
	.rfhcnt = mga_2064w_rfhcnt,
	.monitor_sense_dac1 = mga_2064w_monitor_sense,
	.suspend = mga_2064w_suspend,
	.resume = mga_2064w_resume,
	.init = mga_2064w_init,
	.set_mode = mga_2064w_set_mode,
	.test = mga_2064w_test,
	.softreset = mga_2064w_softreset,
};

void mga_2064w_prepare(struct mga_dev *mdev)
{
	mdev->funcs = &mga_2064w_funcs;
}
