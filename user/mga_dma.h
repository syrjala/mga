/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_DMA_H
#define MGA_DMA_H

#include "kernel_emul.h"

struct mga_dev;

enum mga_dma_mode {
	MGA_DMA_GENERAL_PURPOSE_WRITE,
	MGA_DMA_BLIT_WRITE,
	MGA_DMA_VECTOR_WRITE,
	MGA_DMA_VERTEX_WRITE,
	MGA_DMA_VERTEX_FIXED_LENGTH_SETUP_LIST,
};

static u8 mga_reg_to_index(u32 reg)
{
	return ((reg & 0x2000) >> 6) | ((reg & 0x01fc) >> 2);
}

static u32 mga_index_to_reg(u8 index)
{
	return 0x1C00 | (((u32)index & 0x80) << 6) | (((u32)index & 0x7f) << 2);
}

int mga_dma_init(struct mga_dev *mdev);
void mga_dma_fini(struct mga_dev *mdev);

int mga_submit_dma(struct mga_dev *mdev,
		   const void *data, size_t len,
		   enum mga_dma_mode mode,
		   u32 *ret_handle);

void mga_dma_handle_softrap(struct mga_dev *mdev);

#endif
