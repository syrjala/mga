/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "pcf8574.h"
#include "av9110.h"
#include "mga_i2c.h"
#include "mga_regs.h"
#include "mga_dualhead.h"

/* PCF7574 IO pin mapping. */
enum {
	MGA_DH_DDC_SCL  = 0x01,
	MGA_DH_DDC_SDA  = 0x02,
	MGA_DH_HSYNCPOL = 0x08,
	MGA_DH_VSYNCPOL = 0x10,
	MGA_DH_VIDRST   = 0x80,
};

static void ddc_setscl(void *data, int state)
{
	struct mga_dev *mdev = data;

	pcf8574_gpio_set(&mdev->edev, MGA_DH_DDC_SCL,
			 state ? MGA_DH_DDC_SCL : 0);
}

static void ddc_setsda(void *data, int state)
{
	struct mga_dev *mdev = data;

	pcf8574_gpio_set(&mdev->edev, MGA_DH_DDC_SDA,
			 state ? MGA_DH_DDC_SDA : 0);
}

static int ddc_getscl(void *data)
{
	struct mga_dev *mdev = data;

	return !!(pcf8574_gpio_get(&mdev->edev) & MGA_DH_DDC_SCL);
}

static int ddc_getsda(void *data)
{
	struct mga_dev *mdev = data;

	return !!(pcf8574_gpio_get(&mdev->edev) & MGA_DH_DDC_SDA);
}

void mga_dualhead_addon_set_sync(struct mga_dev *mdev, unsigned int sync)
{
	u8 val = 0;

	BUG_ON(!(mdev->features & MGA_FEAT_DUALHEAD_ADDON));

	/* MGA-TVO hsync and vsync are positive. */

	if (sync & DRM_MODE_FLAG_NHSYNC)
		val |= MGA_DH_HSYNCPOL;

	if (sync & DRM_MODE_FLAG_NVSYNC)
		val |= MGA_DH_VSYNCPOL;

	pcf8574_gpio_set(&mdev->edev, MGA_DH_HSYNCPOL | MGA_DH_VSYNCPOL, val);
}

void mga_dualhead_addon_vidrst(struct mga_dev *mdev, bool enable)
{
	u8 val = 0;

	BUG_ON(!(mdev->features & MGA_FEAT_DUALHEAD_ADDON));

	if (!enable)
		val |= MGA_DH_VIDRST;

	pcf8574_gpio_set(&mdev->edev, MGA_DH_VIDRST, val);
}

int mga_dualhead_addon_probe(struct mga_dev *mdev)
{
	int ret;

	if (mdev->chip != MGA_CHIP_G400)
		return -ENODEV;

	ret = pcf8574_init(&mdev->edev, &mdev->misc.adap, 0x20);
	if (ret) {
		dev_err(mdev->dev, "Failed to init PCF8574\n");
		return ret;
	}

	pcf8574_reset(&mdev->edev);

	/* Invert HSYNC and VSYNC, disable VIDRST. */
	pcf8574_gpio_set(&mdev->edev,
			 MGA_DH_HSYNCPOL | MGA_DH_VSYNCPOL | MGA_DH_VIDRST,
			 MGA_DH_HSYNCPOL | MGA_DH_VSYNCPOL);

	ret = mga_i2c_init(mdev,
			   ddc_setsda, ddc_setscl,
			   ddc_getsda, ddc_getscl,
			   &mdev->ddc2);
	if (ret) {
		dev_err(mdev->dev, "Failed to init dual head addon DDC\n");
		return ret;
	}

	ret = mga_tvo_init(&mdev->tvodev, mdev, 0x1a);
	if (ret) {
		dev_err(mdev->dev, "Failed to init dual head addon TVO\n");
		return ret;
	}

	/*
	 * External reset is not needed with addon since
	 * sync polarity is not handled by MGA-TVO pin.
	 */

	mga_tvo_disable(&mdev->tvodev);

	mdev->features |= MGA_FEAT_TVO_ADDON | MGA_FEAT_DUALHEAD_ADDON;
	mdev->outputs |= MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT;

	return 0;
}

void mga_dualhead_builtin_set_sync(struct mga_dev *mdev, unsigned int sync)
{
	BUG_ON(!(mdev->features & MGA_FEAT_DUALHEAD_BUILTIN));

	mga_tvo_set_sync(&mdev->tvodev, sync);
}

int mga_dualhead_builtin_probe(struct mga_dev *mdev)
{
	int ret;

	if (mdev->chip != MGA_CHIP_G400)
		return -ENODEV;

	ret = mga_i2c_dac_ddc2_init(mdev);
	if (ret) {
		dev_err(mdev->dev, "Failed to init builtin dual head DDC2\n");
		return ret;
	}

	ret = mga_tvo_init(&mdev->tvodev, mdev, 0x1b);
	if (ret) {
		dev_err(mdev->dev, "Failed to init builtin dual head TVO\n");
		return ret;
	}

	/* Reset sync polarity to 0 position. */
	mga_write32(mdev, MGA_RST, MGA_RST_SOFTEXTRST);
	msleep(100);
	mga_write32(mdev, MGA_RST, 0);
	msleep(100);

	mga_tvo_disable(&mdev->tvodev);

	mdev->features |= MGA_FEAT_TVO_BUILTIN | MGA_FEAT_DUALHEAD_BUILTIN;
	mdev->outputs |= MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT;

	return 0;
}
