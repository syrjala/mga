/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "mga_regs.h"
#include "mga_kms.h"
#include "tvp3026.h"

struct crtc_settings {
	struct drm_framebuffer *fb;
	struct drm_display_mode mode;
	unsigned int outputs;
	unsigned int pixclk;
	int hscale;
	int vscale;
	int x;
	int y;
};

static struct crtc_settings mdev_crtc;

static void crtc1_disable(struct mga_dev *mdev)
{
	//if (!mdev_crtc.outputs)
	//return;

	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);

	mga_crtc1_dpms(mdev, DRM_MODE_DPMS_OFF);

	tvp_dac_power(&mdev->tdev, false);

	tvp_lclkpll_disable(&mdev->tdev);

	tvp_dotclock_enable(&mdev->tdev, false);

	tvp_pclkpll_disable(&mdev->tdev);

	mdev_crtc.pixclk = 0;
	mdev_crtc.outputs = 0;
	memset(&mdev_crtc.mode, 0, sizeof mdev_crtc.mode);
	mdev_crtc.fb = NULL;
}

static void crtc1_enable(struct mga_dev *mdev,
			 struct mga_mode_config *mc,
			 unsigned int pixclk)
{
	const struct mga_plane_config *pc = &mc->crtc1.plane_config;
	const struct mga_crtc_config *cc = &mc->crtc1.crtc_config;
	const struct mga_crtc1_regs *regs = &mc->crtc1.regs;
	const struct drm_framebuffer *fb = pc->fb;
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	/* FIXME sync from mode */
	unsigned int sync = DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_NVSYNC;

	mga_crtc1_restore(mdev, regs);

	/* must be active low */
	mga_crtc1_set_sync(mdev, DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_NVSYNC);

	tvp_dac_power(&mdev->tdev, true);

	tvp_program(&mdev->tdev, fb->pixel_format,
		    mdev->interleave ? TVP_BUS_WIDTH_64 : TVP_BUS_WIDTH_32);

	tvp_set_palette(&mdev->tdev);

	tvp_set_sync(&mdev->tdev, sync);

	mga_crtc1_dpms(mdev, DRM_MODE_DPMS_ON);

	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, true);
}

int mga_2064w_set_mode(struct mga_dev *mdev,
		       struct mga_mode_config *mc)
{
	unsigned int pixclk = 0;
	struct tvp_pll_settings pclk, lclk;
	bool changed = false;
	int ret;
	struct mga_plane_config *pc = &mc->crtc1.plane_config;
	struct mga_crtc_config *cc = &mc->crtc1.crtc_config;
	unsigned int hzoom = 0x10000 / pc->hscale;

	if (mc->crtc2.crtc_config.mode_valid)
		return -ENODEV;

	/* all or nothing */
	if (cc->mode_valid) {
		if (!cc->outputs || !pc->fb)
			return -EINVAL;
	} else {
		if (cc->outputs || pc->fb)
			return -EINVAL;
	}

	/* all possible outputs */
	if (cc->outputs & ~MGA_OUTPUT_DAC1)
		return -EINVAL;

	if (cc->mode_valid) {
		int ret;

		ret = mga_crtc1_calc_mode(mdev, pc, cc, &mc->crtc1.regs);
		if (ret)
			return ret;

		pixclk = mga_calc_pixel_clock(&cc->adjusted_mode, hzoom);
		if (!pixclk)
			return -EINVAL;

		/* FIXME CRTC/DAC max clock */
		ret = tvp_pclkpll_calc(&mdev->tdev, pixclk, 0, &pclk);
		if (ret)
			return ret;

		pixclk = pclk.fpll;

		ret = tvp_lclkpll_calc(&mdev->tdev, pixclk,
				       drm_format_bpp(pc->fb->pixel_format),
				       mdev->interleave ? TVP_BUS_WIDTH_64 : TVP_BUS_WIDTH_32,
				       &lclk);
		if (ret)
			return ret;
	}

	if (cc->outputs != mdev_crtc.outputs ||
	    pixclk != mdev_crtc.pixclk ||
	    pc->fb != mdev_crtc.fb ||
	    (cc->mode_valid && !drm_mode_equal(&cc->adjusted_mode, &mdev_crtc.mode)) ||
	    pc->hscale != mdev_crtc.hscale ||
	    pc->vscale != mdev_crtc.vscale ||
	    pc->src.x1 != mdev_crtc.x ||
	    pc->src.y1 != mdev_crtc.y)
		changed = true; /* full modeset when outputs/fb/mode/pixclk change */

	if (!changed)
		goto exit;

	if (changed)
		crtc1_disable(mdev);

	if (changed && cc->mode_valid) {
		ret = tvp_pclkpll_program(&mdev->tdev, &pclk);
		if (ret)
			// FIXME restore prev config
			return ret;

		tvp_dotclock_enable(&mdev->tdev, true);

		ret = tvp_lclkpll_program(&mdev->tdev, &lclk);
		if (ret) {
			tvp_dotclock_enable(&mdev->tdev, false);
			tvp_pclkpll_disable(&mdev->tdev);
			// FIXME restore prev config
			return ret;
		}

		crtc1_enable(mdev, mc, pixclk);
	}

	// disp start
	// zoom

	if (cc->mode_valid)
		drm_mode_copy(&mdev_crtc.mode, &cc->adjusted_mode);
	else
		memset(&mdev_crtc.mode, 0, sizeof mdev_crtc.mode);
	mdev_crtc.fb = pc->fb;
	mdev_crtc.outputs = cc->outputs;
	mdev_crtc.pixclk = pixclk;
	mdev_crtc.hscale = pc->hscale;
	mdev_crtc.vscale = pc->vscale;
	mdev_crtc.x = pc->src.x1;
	mdev_crtc.y = pc->src.y1;

 exit:
	return 0;
}
