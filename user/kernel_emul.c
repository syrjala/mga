/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <pthread.h>

#include "kernel_emul.h"
#include "wait.h"

unsigned long get_jiffies(void)
{
	struct timespec tv;
	clock_gettime(CLOCK_MONOTONIC, &tv);
	return tv.tv_sec * 1000 + tv.tv_nsec / 1000000;
}

unsigned long usecs_to_jiffies(const unsigned int u)
{
	return DIV_ROUND_UP(u, 1000);
}

unsigned long msecs_to_jiffies(const unsigned int m)
{
	return m;
}

unsigned int jiffies_to_msecs(const unsigned long j)
{
	return j;
}

void timespecadd(const struct timespec *a,
		 const struct timespec *b,
		 struct timespec *result)
{
	result->tv_sec = a->tv_sec + b->tv_sec;
	result->tv_nsec = a->tv_nsec + b->tv_nsec;
	if (result->tv_nsec >= 1000000000) {
		++result->tv_sec;
		result->tv_nsec -= 1000000000;
	}
}

void timespecsub(const struct timespec *a,
		 const struct timespec *b,
		 struct timespec *result)
{
	result->tv_sec = a->tv_sec - b->tv_sec;
	result->tv_nsec = a->tv_nsec - b->tv_nsec;
	if (result->tv_nsec < 0) {
		--result->tv_sec;
		result->tv_nsec += 1000000000;
	}
}

void msleep(unsigned int msecs)
{
	struct timespec tv1, tv2;
	struct timespec interval = {
		.tv_sec = msecs / 1000,
		.tv_nsec = (msecs % 1000) * 1000000,
	};

	clock_gettime(CLOCK_MONOTONIC, &tv1);
	timespecadd(&tv1, &interval, &tv2);
	while (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &tv2, NULL) && errno == EINTR)
		;
}

void udelay(unsigned int usecs)
{
	struct timespec tv1, tv2;
	struct timespec interval = {
		.tv_sec = usecs / 1000000,
		.tv_nsec = (usecs % 1000000) * 1000,
	};

	clock_gettime(CLOCK_MONOTONIC, &tv1);
	timespecadd(&tv1, &interval, &tv2);
	while (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &tv2, NULL) && errno == EINTR)
		;
}

void ndelay(unsigned int nsecs)
{
	struct timespec tv1, tv2;
	struct timespec interval = {
		.tv_sec = nsecs / 1000000000,
		.tv_nsec = nsecs % 1000000000,
	};

	clock_gettime(CLOCK_MONOTONIC, &tv1);
	timespecadd(&tv1, &interval, &tv2);
	while (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &tv2, NULL) && errno == EINTR)
		;
}

unsigned int div_round(unsigned int n, unsigned int d)
{
	return (2 * n + d) / (2 * d);
}

static unsigned int div_floor(unsigned int n, unsigned int d)
{
	return n / d;
}

static unsigned int div_ceil(unsigned int n, unsigned int d)
{
	return (n + d - 1) / d;
}

void printk(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	fflush(stdout);
}

void *kmalloc(size_t size, gfp_t flags)
{
	(void)flags;
	return malloc(size);
}

void *kcalloc(size_t n, size_t size, gfp_t flags)
{
	(void)flags;
	return calloc(n, size);
}

void *kzalloc(size_t size, gfp_t flags)
{
	(void)flags;
	return calloc(1, size);
}

void *kmemdup(void *ptr, size_t size, gfp_t flags)
{
	void *dst;
	(void)flags;
	dst = malloc(size);
	if (dst)
		memcpy(dst, ptr, size);
	return dst;
}

void kfree(void *ptr)
{
	free(ptr);
}

int fls(int x)
{
	int r = 32;
	if (!x)
		return 0;

	if (!(x & 0xffff0000u)) {
		x <<= 16;
		r -= 16;
	}
	if (!(x & 0xff000000u)) {
		x <<= 8;
		r -= 8;
	}
	if (!(x & 0xf0000000u)) {
		x <<= 4;
		r -= 4;
	}
	if (!(x & 0xc0000000u)) {
		x <<= 2;
		r -= 2;
	}
	if (!(x & 0x80000000u)) {
		x <<= 1;
		r -= 1;
	}
	return r;
}

int ilog2(u32 n)
{
	return fls(n) - 1;
}

u8 readb(void __iomem *addr)
{
	return *(volatile u8*)addr;
}

u16 readw(void __iomem *addr)
{
	return *(volatile u16*)addr;
}

u32 readl(void __iomem *addr)
{
	return *(volatile u32*)addr;
}

void writeb(u8 val, void __iomem *addr)
{
	*(volatile u8*)addr = val;
}

void writew(u16 val, void __iomem *addr)
{
	*(volatile u16*)addr = val;
}

void writel(u32 val, void __iomem *addr)
{
	*(volatile u32*)addr = val;
}

void mutex_init(struct mutex *mutex)
{
	pthread_mutex_init(&mutex->mutex, NULL);
}

void mutex_lock(struct mutex *mutex)
{
	pthread_mutex_lock(&mutex->mutex);
}

void mutex_unlock(struct mutex *mutex)
{
	pthread_mutex_unlock(&mutex->mutex);
}

void init_completion(struct completion *completion)
{
	if (!completion->inited) {
		init_waitqueue_head(&completion->wait);
		completion->inited = true;
	}
	completion->complete = false;
}

void wait_for_completion(struct completion *completion)
{
	wait_event(&completion->wait, completion->complete);
}

int wait_for_completion_timeout(struct completion *completion,
				unsigned int timeout)
{
	return wait_event_timeout(&completion->wait, completion->complete, timeout) ;
}

void complete(struct completion *completion)
{
	completion->complete = true;
	wake_up_all(&completion->wait);
}

void *memchr_inv(const void *start, int c, size_t n)
{
	const unsigned char *b = start;

	while (n > 0) {
		if (*b != (unsigned char)c)
			break;
		b++;
		n--;
	}

	return n > 0 ? (void *)b : NULL;
}

unsigned int hweight32(unsigned int w)
{
	unsigned int res = 0;

	while (w) {
		res += w & 1;
		w >>= 1;
	}

	return res;
}

/* FIXME should be thread local */
static struct {
	void __user *data;
	void __user *start;
	void __user *end;
} __ud;

void _ioctl_user_data(void __user *data, size_t start, size_t end)
{
	__ud.data = data;
	__ud.start = data + start;
	__ud.end = data + end;
}

int get_user_4(u32 *x, const u32 __user *ptr)
{
	const u32 __user *start = (const u32 __user *)__ud.start;
	const u32 __user *end = (const u32 __user *)__ud.end;

	ptr = __ud.data + (unsigned long)ptr;

	*x = 0;

	if (ptr < start)
		return -EFAULT;
	if (ptr + 1 > end)
		return -EFAULT;
	//if ((unsigned long)ptr & 3)
	//return -EFAULT;

	*x = *ptr;

	return 0;
}

int get_user_8(u64 *x, const u64 __user *ptr)
{
	const u64 __user *start = (const u64 __user *)__ud.start;
	const u64 __user *end = (const u64 __user *)__ud.end;

	ptr = __ud.data + (unsigned long)ptr;

	*x = 0;

	if (ptr < start)
		return -EFAULT;
	if (ptr + 1 > end)
		return -EFAULT;
	//if ((unsigned long)ptr & 7)
	//return -EFAULT;

	*x = *ptr;

	return 0;
}

int put_user_4(u32 x, u32 __user *ptr)
{
	u32 __user *start = (u32 __user *)__ud.start;
	u32 __user *end = (u32 __user *)__ud.end;

	ptr = __ud.data + (unsigned long)ptr;

	if (ptr < start)
		return -EFAULT;
	if (ptr + 1 > end)
		return -EFAULT;
	//if ((unsigned long)ptr & 3)
	//return -EFAULT;

	*ptr = x;

	return 0;
}

int put_user_8(u64 x, u64 __user *ptr)
{
	u64 __user *start = (u64 __user *)__ud.start;
	u64 __user *end = (u64 __user *)__ud.end;

	ptr = __ud.data + (unsigned long)ptr;

	if (ptr < start)
		return -EFAULT;
	if (ptr + 1 > end)
		return -EFAULT;
	//if ((unsigned long)ptr & 7)
	//return -EFAULT;

	*ptr = x;

	return 0;
}

int copy_from_user(void *dst, const void __user *src, size_t len)
{
	const void __user *start = (const void __user *)__ud.start;
	const void __user *end = (const void __user *)__ud.end;

	src = __ud.data + (unsigned long)src;

	if (src < start)
		return -EFAULT;
	if (src + len > end)
		return -EFAULT;
	memcpy(dst, src, len);
	return 0;
}

int copy_to_user(void __user *dst, const void *src, size_t len)
{
	void __user *start = (void __user *)__ud.data;
	void __user *end = (void __user *)__ud.end;

	dst = __ud.data + (unsigned long)dst;

	if (dst < start)
		return -EFAULT;
	if (dst + len > end)
		return -EFAULT;
	memcpy(dst, src, len);
	return 0;
}
