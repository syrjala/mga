/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include "mga_dump.h"
#include "mga_kms.h"
#include "mga_regs.h"

static u8 mga_seq_read8(struct mga_dev *mdev, u8 reg)
{
	mga_write8(mdev, MGA_SEQX, reg);
	return mga_read8(mdev, MGA_SEQD);
}

static void mga_seq_write8(struct mga_dev *mdev, u8 reg, u8 val)
{
	mga_write8(mdev, MGA_SEQX, reg);
	mga_write8(mdev, MGA_SEQD, val);
}

static u8 mga_gctl_read8(struct mga_dev *mdev, u8 reg)
{
	mga_write8(mdev, MGA_GCTLX, reg);
	return mga_read8(mdev, MGA_GCTLD);
}

static void mga_gctl_write8(struct mga_dev *mdev, u8 reg, u8 val)
{
	mga_write8(mdev, MGA_GCTLX, reg);
	mga_write8(mdev, MGA_GCTLD, val);
}

static void mga_attr_reset(struct mga_dev *mdev)
{
	mga_read8(mdev, MGA_INSTS1_R);
}

/* remember to reset before each read */
static u8 mga_attr_read8(struct mga_dev *mdev, u8 reg)
{
	mga_write8(mdev, MGA_ATTRX, reg);
	return mga_read8(mdev, MGA_ATTRD_R);
}

/* remember to reset before first write */
static void mga_attr_write8(struct mga_dev *mdev, u8 reg, u8 val)
{
	mga_write8(mdev, MGA_ATTRX, reg);
	mga_write8(mdev, MGA_ATTRD_W, val);
}

static u8 mga_crtc_read8(struct mga_dev *mdev, u8 reg)
{
	mga_write8(mdev, MGA_CRTCX, reg);
	return mga_read8(mdev, MGA_CRTCD);
}

static void mga_crtc_write8(struct mga_dev *mdev, u8 reg, u8 val)
{
	mga_write8(mdev, MGA_CRTCX, reg);
	mga_write8(mdev, MGA_CRTCD, val);
}

static u8 mga_crtcext_read8(struct mga_dev *mdev, u8 reg)
{
	mga_write8(mdev, MGA_CRTCEXTX, reg);
	return mga_read8(mdev, MGA_CRTCEXTD);
}

static void mga_crtcext_write8(struct mga_dev *mdev, u8 reg, u8 val)
{
	mga_write8(mdev, MGA_CRTCEXTX, reg);
	mga_write8(mdev, MGA_CRTCEXTD, val);
}

void mga_crtc1_video(struct mga_dev *mdev, bool enable)
{
	u8 val = mga_seq_read8(mdev, 0x01);

	if (enable)
		val &= ~MGA_SEQ1_SCROFF;
	else
		val |= MGA_SEQ1_SCROFF;

	mga_write8(mdev, MGA_SEQD, val);
}

/*
 * G450/G550 CRTC1 -> DAC1/DAC2/TMDS
 * Others CRTC1 -> DAC1
 */
void mga_crtc1_dpms(struct mga_dev *mdev, u8 mode)
{
	u8 val = mga_crtcext_read8(mdev, 0x01);

	val &= ~(MGA_CRTCEXT1_HSYNCOFF | MGA_CRTCEXT1_VSYNCOFF);
	switch (mode) {
	case DRM_MODE_DPMS_ON:
		break;
	case DRM_MODE_DPMS_STANDBY:
		val |= MGA_CRTCEXT1_HSYNCOFF;
		break;
	case DRM_MODE_DPMS_SUSPEND:
		val |= MGA_CRTCEXT1_VSYNCOFF;
		break;
	case DRM_MODE_DPMS_OFF:
		val |= MGA_CRTCEXT1_HSYNCOFF | MGA_CRTCEXT1_VSYNCOFF;
		break;
	}

	mga_write8(mdev, MGA_CRTCEXTD, val);
}

/*
 * G450/G550 CRTC1 -> DAC1/DAC2/TMDS
 * Others CRTC1 -> DAC1
 *
 * FIXME verify this for others:
 * G400 csync always negative
 */
void mga_crtc1_set_sync(struct mga_dev *mdev, unsigned int sync)
{
	u8 val = mga_crtcext_read8(mdev, 0x03);

	if (sync & DRM_MODE_FLAG_CSYNC)
		val |= MGA_CRTCEXT3_CSYNCEN;
	else
		val &= ~MGA_CRTCEXT3_CSYNCEN;

	mga_write8(mdev, MGA_CRTCEXTD, val);
}

void mga_misc_powerup(struct mga_dev *mdev)
{
	mga_write8(mdev, MGA_MISC_W,
		   MGA_MISC_CLKSEL_PIXPLLC |
		   MGA_MISC_IOADDSEL);
}

/*
 * G450/G550: CRTC1 -> DAC1/DAC2/TMDS
 * Others: CRTC1 -> DAC1
 */
void mga_misc_set_sync(struct mga_dev *mdev, unsigned int sync)
{
	u8 val = mga_read8(mdev, MGA_MISC_R);

	if (sync & DRM_MODE_FLAG_NHSYNC)
		val |= MGA_MISC_HSYNCPOL;
	else
		val &= ~MGA_MISC_HSYNCPOL;

	if (sync & DRM_MODE_FLAG_NVSYNC)
		val |= MGA_MISC_VSYNCPOL;
	else
		val &= ~MGA_MISC_HSYNCPOL;

	mga_write8(mdev, MGA_MISC_W, val);
}

void mga_misc_disable_vga(struct mga_dev *mdev)
{
	u32 option, devctrl;
	u8 val;

	dev_dbg(mdev->dev, "Disabling VGA IO ports\n");
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_VGAIOEN;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	dev_dbg(mdev->dev, "Disabling IO port decode\n");
	devctrl = pci_cfg_read32(&mdev->pdev, MGA_DEVCTRL);
	devctrl &= ~MGA_DEVCTRL_IOSPACE;
	pci_cfg_write32(&mdev->pdev, MGA_DEVCTRL, devctrl);

	dev_dbg(mdev->dev, "Enabling CGA emulation\n");
	dev_dbg(mdev->dev, "Disabling VGA framebuffer\n");
	val = mga_read8(mdev, MGA_MISC_R);
	val |= MGA_MISC_IOADDSEL;
	val &= ~MGA_MISC_RAMMAPENABLE;
	mga_write8(mdev, MGA_MISC_W, val);
}

static void mga_crtc1_init_vga(struct mga_crtc1_regs *regs)
{
	int i;

	regs->seq[0x00] = MGA_SEQ0_SYNCRST | MGA_SEQ0_ASYNCRTS;
	regs->seq[0x01] = MGA_SEQ1_DOTMODE;
	regs->seq[0x02] = 0x0F;
	regs->seq[0x03] = 0x00;
	regs->seq[0x04] = MGA_SEQ4_CHAIN4 | MGA_SEQ4_SEQODDEVMD | MGA_SEQ4_MEMSZ256;

	regs->gctl[0x00] = 0x00;
	regs->gctl[0x01] = 0x00;
	regs->gctl[0x02] = 0x00;
	regs->gctl[0x03] = 0x00;
	regs->gctl[0x04] = 0x00;
	regs->gctl[0x05] = MGA_GCTL5_MODE256;
	regs->gctl[0x06] = MGA_GCTL6_MEMMAPSEL_A0000_AFFFF | MGA_GCTL6_GCGRMODE;
	regs->gctl[0x07] = 0x0F;
	regs->gctl[0x08] = 0xFF;
	regs->gctl[0x09] = 0x00;

	for (i = 0; i < 0x10; i++)
		regs->attr[i] = i;
	regs->attr[0x10] = MGA_ATTR10_PELWIDTH | MGA_ATTR10_ATCGRMODE | (1<<5);
	regs->attr[0x11] = 0xFF;
	regs->attr[0x12] = 0x0F;
	regs->attr[0x13] = 0x00;
	regs->attr[0x14] = 0x00;

	regs->attrx = MGA_ATTRX_PAS;
}

static void mga_crtc1_init_crtc(struct mga_crtc1_regs *regs)
{
	regs->crtc[0x08] = 0x00;
	regs->crtc[0x0A] = 0x00;
	regs->crtc[0x0B] = 0x00;
	regs->crtc[0x0E] = 0x00;
	regs->crtc[0x0F] = 0x00;
	regs->crtc[0x14] = 0x00;
	regs->crtc[0x17] = MGA_CRTC17_CRTCRSTN | MGA_CRTC17_WBMODE |
		MGA_CRTC17_SELROWSCAN | MGA_CRTC17_CMS;

	regs->crtcext[0x04] = 0x00;
	/*
	 * hiprilvl: >= G100
	 * maxhipri: >= G200
	 * hiprilvl = 0
	 * maxhipri = 0
	 */
	regs->crtcext[0x06] = 0x00; /* maxhipri and hiprilvl */
	/* FIXME: What are winsize and winfreq? */
	regs->crtcext[0x07] = 0x00;

	regs->misc = MGA_MISC_CLKSEL_PIXPLLC | MGA_MISC_IOADDSEL;
}

static void mga_crtc1_restore_vga(struct mga_dev *mdev,
				  const struct mga_crtc1_regs *regs)
{
	int i;

	for (i = 0; i < (int)ARRAY_SIZE(regs->seq); i++)
		mga_seq_write8(mdev, i, regs->seq[i]);

	for (i = 0; i < (int)ARRAY_SIZE(regs->gctl); i++)
		mga_gctl_write8(mdev, i, regs->gctl[i]);

	mga_attr_reset(mdev);
	for (i = 0; i < (int)ARRAY_SIZE(regs->attr); i++)
		mga_attr_write8(mdev, i, regs->attr[i]);
	mga_write8(mdev, MGA_ATTRX, regs->attrx);
	mga_attr_reset(mdev);
}

static void mga_crtc1_restore_crtc(struct mga_dev *mdev,
				   const struct mga_crtc1_regs *regs)
{
	int i;

	mga_write8(mdev, MGA_MISC_W, regs->misc);

	/* unlock the CRTC registers */
	mga_crtc_write8(mdev, 0x11, regs->crtc[0x11] & ~MGA_CRTC11_CRTCPROTECT);

	for (i = 0; i < (int)ARRAY_SIZE(regs->crtc); i++) {
		if (i == 0x11)
			continue;

		mga_crtc_write8(mdev, i, regs->crtc[i]);
	}

	for (i = 0; i < (int)ARRAY_SIZE(regs->crtcext); i++) {
		if (i == 0x00)
			continue;

		mga_crtcext_write8(mdev, i, regs->crtcext[i]);
	}

	/* trigger startadd update */
	mga_crtcext_write8(mdev, 0x00, regs->crtcext[0x00]);

	/* possibly lock the CRTC registers */
	mga_crtc_write8(mdev, 0x11, regs->crtc[0x11]);
}

static void mga_crtc1_save_vga(struct mga_dev *mdev,
			       struct mga_crtc1_regs *regs)
{
	int i;

	for (i = 0; i < (int)ARRAY_SIZE(regs->seq); i++)
		regs->seq[i] = mga_seq_read8(mdev, i);

	for (i = 0; i < (int)ARRAY_SIZE(regs->gctl); i++)
		regs->gctl[i] = mga_gctl_read8(mdev, i);

	mga_attr_reset(mdev);
	regs->attrx = mga_read8(mdev, MGA_ATTRX);
	for (i = 0; i < (int)ARRAY_SIZE(regs->attr); i++) {
		mga_attr_reset(mdev);
		regs->attr[i] = mga_attr_read8(mdev, i);
	}
	mga_attr_reset(mdev);
	mga_write8(mdev, MGA_ATTRX, regs->attrx);
	mga_attr_reset(mdev);
}

static void mga_crtc1_save_crtc(struct mga_dev *mdev,
				struct mga_crtc1_regs *regs)
{
	int i;

	for (i = 0; i < (int)ARRAY_SIZE(regs->crtc); i++)
		regs->crtc[i] = mga_crtc_read8(mdev, i);

	for (i = 0; i < (int)ARRAY_SIZE(regs->crtcext); i++)
		regs->crtcext[i] = mga_crtcext_read8(mdev, i);

	regs->misc = mga_read8(mdev, MGA_MISC_R);
}

void mga_crtc1_restore(struct mga_dev *mdev,
		       const struct mga_crtc1_regs *regs)
{
	mga_crtc1_restore_crtc(mdev, regs);
}

void mga_crtc1_powerup(struct mga_dev *mdev)
{
	struct mga_crtc1_regs regs;
	u8 val;

	mga_crtc1_init_vga(&regs);
	mga_crtc1_init_crtc(&regs);

	mga_crtc1_restore_vga(mdev, &regs);

	/* FIXME */
	/* unlock/relock the CRTC registers */
	val = mga_crtc_read8(mdev, 0x11);
	val &= ~MGA_CRTC11_CRTCPROTECT;
	mga_write8(mdev, MGA_CRTCD, val);

	/* FIXME */
	/* Switch to MGA mode */
	val = mga_crtcext_read8(mdev, 0x03);
	val |= MGA_CRTCEXT3_MGAMODE;
	mga_write8(mdev, MGA_CRTCEXTD, val);
}

static u8 calc_viddelay(struct mga_dev *mdev)
{
	u8 viddelay;

	if (mdev->chip >= MGA_CHIP_1064SG)
		return 0;

	/*
	 * FIXME: Memory size not known initially.
	 * Is it OK if mem_size == 0 and so viddelay = 1?
	 */
	if (mdev->mem_size >= 8)
		viddelay = 2;
	else if (mdev->mem_size >= 4)
		viddelay = 0;
	else
		viddelay = 1;

	return viddelay;
}

unsigned int crtc1_pitch_alignment(const struct mga_dev *mdev)
{
	if (mdev->chip >= MGA_CHIP_1064SG)
		return 16 >> mdev->splitmode;
	else
		return 64 << mdev->interleave;
}

unsigned int crtc1_address_alignment(const struct mga_dev *mdev)
{
	bool shift;

	if (mdev->chip >= MGA_CHIP_1064SG)
		shift = mdev->splitmode;
	else
		shift = !mdev->interleave;

	return 8 >> shift;
}

static unsigned int calc_startadd(const struct mga_dev *mdev,
				  const struct mga_framebuffer *mfb,
				  unsigned int x,
				  unsigned int y)
{
	unsigned int address = mga_framebuffer_calc_bus(mfb, x, y, 0, 0);

	dev_dbg(mdev->dev, "cpp = %u, pitch = %u, x = %u, y = %u\n",
		drm_format_plane_cpp(mfb->base.pixel_format, 0),
		mfb->base.pitches[0], x, y);

	BUG_ON(address % crtc1_address_alignment(mdev));

	return address / crtc1_address_alignment(mdev);
}

static unsigned int calc_offset(const struct mga_dev *mdev,
				const struct mga_framebuffer *mfb)
{
	bool shift;

	if (mdev->chip >= MGA_CHIP_1064SG)
		shift = mdev->splitmode;
	else
		shift = !mdev->interleave;

	return mfb->base.pitches[0] / (16 >> shift);
}

unsigned int crtc1_max_pitch(const struct mga_dev *mdev)
{
	bool shift;

	if (mdev->chip >= MGA_CHIP_1064SG)
		shift = mdev->splitmode;
	else
		shift = !mdev->interleave;

	return 0x3ff * (16 >> shift);
}

static unsigned int crtc1_max_address(const struct mga_dev *mdev)
{
	bool shift;
	unsigned int max = 0xfffff;

	if (mdev->chip >= MGA_CHIP_G100)
		max = 0x1fffff;

	if (mdev->chip >= MGA_CHIP_1064SG)
		shift = mdev->splitmode;
	else
		shift = !mdev->interleave;

	return max * (8 >> shift);
}

static u8 calc_scale(const struct mga_dev *mdev,
		     unsigned int pixel_format)
{
	unsigned int cpp = drm_format_plane_cpp(pixel_format, 0);
	unsigned int scale = (cpp << 1) - 1;

	if (mdev->chip >= MGA_CHIP_1064SG || mdev->interleave)
		scale >>= 1;

	return scale;
}

static unsigned int mga_alignment(const struct mga_dev *mdev,
				  unsigned int bpp)
{
	switch (mdev->chip) {
	case MGA_CHIP_2064W:
	case MGA_CHIP_2164W:
		return mga_2064w_alignment(bpp, mdev->interleave);
	case MGA_CHIP_1064SG:
	case MGA_CHIP_1164SG:
	case MGA_CHIP_G100:
	case MGA_CHIP_G200:
	case MGA_CHIP_G200SE:
	case MGA_CHIP_G200EV:
	case MGA_CHIP_G200WB:
	case MGA_CHIP_G400:
	case MGA_CHIP_G450:
	case MGA_CHIP_G550:
		return mga_1064sg_alignment(bpp);
	default:
		return 0;
	}
}

void mga_crtc1_align_address(const struct mga_dev *mdev,
			     struct mga_framebuffer *mfb)
{
	unsigned int address = mga_framebuffer_calc_bus(mfb, 0, 0, 0, 0);
	unsigned int alignment = crtc1_address_alignment(mdev);
	unsigned int boundary = 0x200000 << mdev->interleave;
	unsigned int size = mfb->base.pitches[0] * mfb->base.height;

	BUG_ON(mdev->chip > MGA_CHIP_2164W);
	BUG_ON(mfb->base.pitches[0] % crtc1_pitch_alignment(mdev));

	/* bring it into alignment */
	address -= address % alignment;

	/* Make sure no line crosses the memory bank boundary, */
	if (address < boundary && address + size > boundary) {
		dev_dbg(mdev->dev, "crtc1: adjusting address %08x -> %08x\n",
			address,  address + (boundary - address) % mfb->base.pitches[0]);
		address += (boundary - address) % mfb->base.pitches[0];
	}

	mfb->bus = address - mfb->base.offsets[0];

	BUG_ON((mfb->bus + mfb->base.offsets[0]) % alignment);
}

static int crtc1_check_mode(const struct mga_dev *mdev,
			    const struct drm_display_mode *mode,
			    unsigned int hzoom, unsigned int vzoom)
{

	if (mode->flags & ~DRM_MODE_FLAG_INTERLACE) {
		dev_dbg(mdev->dev, "crtc1: unsupported mode flags\n");
		return -EINVAL;
	}

	BUG_ON(mode->private_flags);

	switch (hzoom) {
	case 1:
	case 2:
	case 4:
		break;
	default:
		dev_dbg(mdev->dev, "crtc1: unsupported horizontal zoom factor %u\n", hzoom);
		return -EINVAL;
	}

	if (vzoom < 1 || vzoom > 0x20) {
		dev_dbg(mdev->dev, "crtc1: unsupported vertical zoom factor %u\n", vzoom);
		return -EINVAL;
	}

	return 0;
}

static int crtc1_check_framebuffer(const struct mga_dev *mdev,
				   const struct mga_framebuffer *mfb,
				   const struct drm_display_mode *mode,
				   unsigned int x, unsigned int y)
{
	unsigned int max_pitch;
	unsigned int address;

	switch (mfb->base.pixel_format) {
	case DRM_FORMAT_C8:
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_RGB888:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		break;
	default:
		dev_dbg(mdev->dev, "crtc1: unsupported pixel format 0x%08x\n", mfb->base.pixel_format);
		return -EINVAL;
	}

	address = mga_framebuffer_calc_bus(mfb, x, y, 0, 0);

	if (address % crtc1_address_alignment(mdev) ||
	    address > crtc1_max_address(mdev)) {
		dev_dbg(mdev->dev, "crtc1: unsupported offset "
			"%u (align=%u, max=%u)\n", mfb->bus + mfb->base.offsets[0],
			crtc1_address_alignment(mdev),
			crtc1_max_address(mdev));
		return -EINVAL;
	}

	max_pitch = crtc1_max_pitch(mdev);
	if (mode->flags & DRM_MODE_FLAG_INTERLACE &&
	    !(mode->flags & DRM_MODE_FLAG_DBLSCAN))
		max_pitch >>= 1;

	if (mfb->base.pitches[0] % crtc1_pitch_alignment(mdev) ||
	    mfb->base.pitches[0] > max_pitch) {
		dev_dbg(mdev->dev, "crtc1: unsupported pitch "
			"%u (align=%u, max=%u)\n", mfb->base.pitches[0],
			crtc1_pitch_alignment(mdev), max_pitch);
		return -EINVAL;
	}

	return 0;
}

static void print_mode(struct mga_dev *mdev,
		       const struct drm_display_mode *mode)
{
	dev_dbg(mdev->dev,
		" vrefresh     = %f Hz\n"
		" dblscan      = %d\n"
		" interlace    = %d\n"
		" htotal       = %lu\n"
		" hdisplay     = %lu\n"
		" hblank_start = %lu\n"
		" hblank_width = %lu\n"
		" hsync_start  = %lu\n"
		" hsync_width  = %lu\n"
		" vtotal       = %lu\n"
		" vdisplay     = %lu\n"
		" vblank_start = %lu\n"
		" vblank_width = %lu\n"
		" vsync_start  = %lu\n"
		" vsync_width  = %lu\n",
		mode->vrefresh / 1000.0,
		!!(mode->flags & DRM_MODE_FLAG_DBLSCAN),
		!!(mode->flags & DRM_MODE_FLAG_INTERLACE),
		mode->htotal,
		mode->hdisplay,
		mode->hblank_start,
		mode->hblank_width,
		mode->hsync_start,
		mode->hsync_width,
		mode->vtotal,
		mode->vdisplay,
		mode->vblank_start,
		mode->vblank_width,
		mode->vsync_start,
		mode->vsync_width);
}

static int crtc1_mode_to_timings(struct mga_dev *mdev,
				 const struct mga_framebuffer *mfb,
				 const struct drm_display_mode *mode,
				 int hzoom, int vzoom,
				 struct mga_crtc1_regs *regs)
{
	int htotal, hdispend, hblkstr, hblkend, hsyncstr, hsyncend;
	int vtotal, vdispend, vblkstr, vblkend, vsyncstr, vsyncend;
	int hdispskew, hsyncdel, linecomp, hvidmid;
	u8 maxscan = vzoom - 1;
	u8 viddelay = calc_viddelay(mdev);
	u8 scale = calc_scale(mdev, mfb->base.pixel_format);

	hdispend = mode->hdisplay;
	hblkstr  = mode->hblank_start;
	hblkend  = mode->hblank_start + mode->hblank_width;
	hsyncstr = mode->hsync_start;
	hsyncend = mode->hsync_start + mode->hsync_width;
	htotal   = mode->htotal;

	hdispskew = 0;
	hsyncdel = 0;
	hvidmid = 0;

	if (hzoom > 1 && mdev->chip <= MGA_CHIP_2164W) {
		BUG_ON(hdispend & (hzoom - 1));

		hdispend = hdispend / hzoom;
		hblkstr  = DIV_ROUND_UP(hblkstr, hzoom);
		hblkend  = DIV_ROUND_UP(hblkend, hzoom);
		hsyncstr = DIV_ROUND_UP(hsyncstr, hzoom);
		hsyncend = DIV_ROUND_UP(hsyncend, hzoom);
		htotal   = DIV_ROUND_UP(htotal, hzoom);
		hzoom = 1;
	}

	/* Horizontal */

	BUG_ON(hdispend & 7);

	hdispend = hdispend / 8 - 1;
	hsyncstr = DIV_ROUND_UP(hsyncstr, 8) - 1;
	hsyncend = DIV_ROUND_UP(hsyncend, 8) - 1;/* FIXME check me */
	htotal   = DIV_ROUND_UP(htotal, 8) - 5;

	if (hdispend < 0)
		return -EINVAL;
	if (hsyncstr - hdispend < 3) {
		dev_dbg(mdev->dev, "crtc1: adjusting hsyncstr %d -> %d\n", hsyncstr, hdispend + 3);
		hsyncstr = hdispend + 3;
	}
	if (hsyncend - hsyncstr < 1) {
		dev_dbg(mdev->dev, "crtc1: adjusting hsyncend %d -> %d\n", hsyncend, hsyncstr + 1);
		hsyncend = hsyncstr + 1;
	}
	if (htotal < 1) {
		dev_dbg(mdev->dev, "crtc1: adjusting htotal %d -> %d\n", htotal, 1);
		htotal = 1;
	}
	if (htotal - hdispend < 1) {
		dev_dbg(mdev->dev, "crtc1: adjusting htotal %d -> %d\n", htotal, hdispend + 1);
		htotal = hdispend + 1;
	}
	if (htotal - hsyncstr < 2) {
		dev_dbg(mdev->dev, "crtc1: adjusting htotal %d -> %d\n", htotal, hsyncstr + 2);
		htotal = hsyncstr + 2;
	}
	// spec only mentions interlace but tvo code gets into troubl
	//if (mode->flags & DRM_MODE_FLAG_INTERLACE && htotal - hsyncend < 1) {
	if (htotal - hsyncend < 1) {
		dev_dbg(mdev->dev, "crtc1: adjusting htotal %d -> %d\n", htotal, hsyncend + 1);
		htotal = hsyncend + 1;
	}

	switch (mdev->chip) {
		int modulo;
		int limit;

	case MGA_CHIP_2164W:
		/* hdisplay below certain threshold is always fine. */
		if (hdispend <= 15)
			break;
		/* fall through */
	case MGA_CHIP_2064W:
		/* certain htotal values are no good */
		if (((htotal + 5) * (scale + 1) & 15) == 15) {
			dev_dbg(mdev->dev, "crtc1: adjusting htotal %d -> %d\n", htotal, htotal + 1);
			htotal++;
		}
		break;

	case MGA_CHIP_G200:
		/* FIXME other G200 variants? */
	case MGA_CHIP_G400:
	case MGA_CHIP_G450:
	case MGA_CHIP_G550:
		modulo = 8;
		limit = 128;
		if (mdev->chip >= MGA_CHIP_G400) {
			modulo <<= 1;
			limit <<= 1;
		}

		/*
		 * hdisplay below certain threshold is always fine.
		 * This formula might not be 100% exact, but it
		 * seems to be quite close, dropping all bad cases,
		 * while not dropping too many good cases.
		 */
		if (hdispend <= (limit * hzoom - 3 * (scale + 1)) / (2 * (scale + 1)))
			break;

		/* certain htotal values are no good */
		modulo = modulo * hzoom - 1;
		if (((htotal + 5) * (scale + 1) & modulo) == modulo) {
			dev_dbg(mdev->dev, "crtc1: adjusting htotal %d -> %d\n", htotal, htotal + 1);
			htotal++;
		}
		break;
	default:
		/* no modulo issues with other chips */
		break;
	}

	dev_dbg(mdev->dev,
		"HTOTAL = %u, SCALE = %u\n",
		htotal, scale);

	/* No overscan in power graphic mode */
	hblkstr = hdispend;
	hblkend = htotal + 4;

	dev_dbg(mdev->dev,
		"CRTC1:\n"
		" htotal   = %u\n"
		" hdispend = %u\n"
		" hblkstr  = %u\n"
		" hblkend  = %u\n"
		" hsyncstr = %u\n"
		" hsyncend = %u\n",
		htotal, hdispend, hblkstr, hblkend, hsyncstr, hsyncend);

	/*
	 * FIXME check 2064w/2164w
	 * hblkend  == hblkstr  & 0x7f -> 0x80
	 * hsyncend == hsyncstr & 0x1f -> 0x20
	 */
	if (htotal > 0x1ff || hdispend > 0xff ||
	    hblkstr > 0x1ff || hblkend - hblkstr > 0x80 ||
	    hsyncstr > 0x1ff || hsyncend - hsyncstr > 0x20)
		return -EINVAL;

	/* Vertical */

	vdispend = mode->vdisplay;
	vsyncstr = mode->vsync_start;
	vsyncend = mode->vsync_start + mode->vsync_width;
	vtotal   = mode->vtotal;

	if (mode->flags & DRM_MODE_FLAG_DBLSCAN) {
		vdispend <<= 1;
		vsyncstr <<= 1;
		vsyncend <<= 1;
		vtotal   <<= 1;
	}

	if (mode->flags & DRM_MODE_FLAG_INTERLACE) {
		vdispend >>= 1;
		vsyncstr >>= 1;
		vsyncend >>= 1;
		vtotal   >>= 1;
	}

	vtotal   -= 2;
	vdispend -= 1;
	vsyncstr -= 1;
	vsyncend -= 1;/* FIXME check me */

	if (vdispend < 0)
		return -EINVAL;
	if (vsyncstr < 0) {
		dev_dbg(mdev->dev, "crtc1: adjusting vsyncstr %d -> %d\n", vsyncstr, 0);
		vsyncstr = 0;
	}
	if (vsyncend - vsyncstr < 1) {
		dev_dbg(mdev->dev, "crtc1: adjusting vsyncend %d -> %d\n", vsyncend, vsyncstr + 1);
		vsyncend = vsyncstr + 1;
	}
	if (vtotal < 1) {
		dev_dbg(mdev->dev, "crtc1: adjusting vtotal %d -> %d\n", vtotal, 1);
		vtotal = 1;
	}
	if (vtotal - vdispend < 0) {
		dev_dbg(mdev->dev, "crtc1: adjusting vtotal %d -> %d\n", vtotal, vdispend);
		vtotal = vdispend;
	}
	if (mode->flags & DRM_MODE_FLAG_INTERLACE && (vtotal & 1)) {
		dev_dbg(mdev->dev, "crtc1: adjusting vtotal %d -> %d\n", vtotal, vtotal + 1);
		vtotal++;
	}
	/* FIXME vsync within vtotal? */

	/* No overscan in power graphic mode */
	vblkstr = vdispend;
	vblkend = vtotal + 1;

	dev_dbg(mdev->dev,
		"CRTC1:\n"
		" vtotal   = %u\n"
		" vdispend = %u\n"
		" vblkstr  = %u\n"
		" vblkend  = %u\n"
		" vsyncstr = %u\n"
		" vsyncend = %u\n",
		vtotal, vdispend, vblkstr, vblkend, vsyncstr, vsyncend);

	/*
	 * FIXME check 2064w/2164w
	 * vblkend  == vblkstr  & 0xff -> 0x100
	 * vsyncend == vsyncstr & 0xf  -> 0x10
	 */
	if (vtotal > 0xfff || vdispend > 0x7ff ||
	    vblkstr > 0xfff || vblkend - vblkstr > 0x100 ||
	    vsyncstr > 0xfff || vsyncend - vsyncstr > 0x10)
		return -EINVAL;

	linecomp = vdispend + 1;/* FIXME check me */

	if (linecomp > 0x7ff)
		return -EINVAL;

	if (mode->flags & DRM_MODE_FLAG_INTERLACE) {
		/* The midway point between hsync end and hsync start. */
		hvidmid = (hsyncstr + 1) + (hsyncend + 1) - (htotal + 5);
		/* Oops, the midway point is in backporch. What now? */
		if (hvidmid < 1)
			hvidmid += (htotal + 5) << 1;
		hvidmid = hvidmid / 2 - 1;
	}

	regs->crtc[0x00] = htotal & 0xff;
	regs->crtc[0x01] = hdispend & 0xff;
	regs->crtc[0x02] = hblkstr & 0xff;
	regs->crtc[0x03] =
		((hdispskew & 0x03) << 5) |
		(hblkend & 0x1f);
	regs->crtc[0x04] = hsyncstr & 0xff;
	regs->crtc[0x05] =
		((hblkend  & 0x20) << 2) |
		((hsyncdel & 0x03) << 5) |
		(hsyncend & 0x1f);
	regs->crtc[0x06] = vtotal & 0xff;
	regs->crtc[0x07] =
		((vsyncstr & 0x200) >> 2) |
		((vdispend & 0x200) >> 3) |
		((vtotal   & 0x200) >> 4) |
		((linecomp & 0x100) >> 4) |
		((vblkstr  & 0x100) >> 5) |
		((vsyncstr & 0x100) >> 6) |
		((vdispend & 0x100) >> 7) |
		((vtotal   & 0x100) >> 8);
	regs->crtc[0x08] = 0x00;
	regs->crtc[0x09] =
		((linecomp & 0x200) >> 3) |
		((vblkstr  & 0x200) >> 4) |
		(maxscan & 0x1f);

	regs->crtc[0x10] = vsyncstr & 0xff;
	regs->crtc[0x11] = MGA_CRTC11_VINTEN | (vsyncend & 0xf);
	regs->crtc[0x12] = vdispend & 0xff;

	regs->crtc[0x15] = vblkstr & 0xff;
	regs->crtc[0x16] = vblkend & 0xff;
	regs->crtc[0x18] = linecomp & 0xff;

	regs->crtcext[0x01] =
		(hblkend   & 0x40) |
		MGA_CRTCEXT1_VSYNCOFF |
		MGA_CRTCEXT1_HSYNCOFF |
		((hsyncstr & 0x100) >> 6) |
		((hblkstr  & 0x100) >> 7) |
		((htotal   & 0x100) >> 8);
	regs->crtcext[0x02] =
		((linecomp & 0x400) >>  3) |
		((vsyncstr & 0xC00) >>  5) |
		((vblkstr  & 0xC00) >>  7) |
		((vdispend & 0x400) >>  8) |
		((vtotal   & 0xC00) >> 10);
	regs->crtcext[0x03] = MGA_CRTCEXT3_MGAMODE |
		((viddelay & 0x03) << 3) |
		(scale & 0x07);
	regs->crtcext[0x04] = 0x00;
	regs->crtcext[0x05] = hvidmid;

	/* FIXME use vzoom instead? */
	if (mode->flags & DRM_MODE_FLAG_DBLSCAN &&
	    !(mode->flags & DRM_MODE_FLAG_INTERLACE))
		regs->crtc[0x09] |= MGA_CRTC9_CONV2T4;

	if (mode->flags & DRM_MODE_FLAG_INTERLACE)
		regs->crtcext[0x00] |= MGA_CRTCEXT0_INTERLACE;

	return 0;
}

static unsigned int mga_crtc1_detect_vzoom(const struct mga_crtc1_regs *regs)
{
	return (regs->crtc[0x09] & 0x1f) + 1;
}

static void crtc1_timings_to_mode(const struct mga_dev *mdev,
				  const struct mga_crtc1_regs *regs,
				  unsigned int hzoom,
				  struct drm_display_mode *mode)
{
	int htotal, hdispend, hblkstr, hblkend, hsyncstr, hsyncend;
	int vtotal, vdispend, vblkstr, vblkend, vsyncstr, vsyncend;

	hdispend = regs->crtc[0x01];
	htotal = ((regs->crtcext[0x01] & 0x01) << 8) |
		regs->crtc[0x00];
	hblkstr = ((regs->crtcext[0x01] & 0x02) << 7) |
		regs->crtc[0x02];
	hblkend = (regs->crtcext[0x01]  & 0x40) |
		((regs->crtc[0x05] & 0x80) >> 2) |
		(regs->crtc[0x03] & 0x1f);
	hsyncstr = ((regs->crtcext[0x01] & 0x04) << 6) |
		regs->crtc[0x04];
	hsyncend = regs->crtc[0x05] & 0x1f;

	vdispend = ((regs->crtcext[0x02] & 0x04) << 8) |
		((regs->crtc[0x07] & 0x02) << 7) |
		((regs->crtc[0x07] & 0x40) << 3) |
		regs->crtc[0x12];
	vtotal = ((regs->crtcext[0x02] & 0x03) << 10) |
		((regs->crtc[0x07] & 0x20) << 4) |
		((regs->crtc[0x07] & 0x01) << 8) |
		regs->crtc[0x06];
	vblkstr = ((regs->crtcext[0x02] & 0x18) << 7) |
		((regs->crtc[0x09] & 0x20) << 4) |
		((regs->crtc[0x07] & 0x08) << 5) |
		regs->crtc[0x15];
	vblkend = regs->crtc[0x16];
	vsyncstr = ((regs->crtcext[0x02] & 0x60) << 5) |
		((regs->crtc[0x07] & 0x80) << 2) |
		((regs->crtc[0x07] & 0x04) << 6) |
		regs->crtc[0x10];
	vsyncend = regs->crtc[0x11] & 0xf;

	hblkend |= hblkstr & ~0x7f;
	if (hblkend <= hblkstr)
		hblkend += 0x80;

	hsyncend |= hsyncstr & ~0x1f;
	if (hsyncend <= hsyncstr)
		hsyncend += 0x20;

	vblkend |= vblkstr & ~0xff;
	if (vblkend <= vblkstr)
		vblkend += 0x100;

	vsyncend |= vsyncstr & ~0xf;
	if (vsyncend <= vsyncstr)
		vsyncend += 0x10;

	mode->flags = 0;

	mode->htotal = (htotal + 5) << 3;
	mode->hdisplay = (hdispend + 1) << 3;
	mode->hblank_start = (hblkstr + 1) << 3;
	mode->hblank_width = (hblkend - hblkstr) << 3;
	mode->hsync_start = (hsyncstr + 1) << 3;
	mode->hsync_width = (hsyncend - hsyncstr) << 3;

	mode->vtotal = vtotal + 2;
	mode->vdisplay = vdispend + 1;
	mode->vblank_start = vblkstr + 1;
	mode->vblank_width = vblkend - vblkstr;
	mode->vsync_start = vsyncstr + 1;
	mode->vsync_width = vsyncend - vsyncstr;

	/* FIXME detect hzoom somehow */
	if (mdev->chip <= MGA_CHIP_2164W) {
		mode->htotal *= hzoom;
		mode->hdisplay *= hzoom;
		mode->hblank_start *= hzoom;
		mode->hblank_width *= hzoom;
		mode->hsync_start *= hzoom;
		mode->hsync_width *= hzoom;
	}

	if (regs->crtcext[0x00] & MGA_CRTCEXT0_INTERLACE) {
		mode->flags |= DRM_MODE_FLAG_INTERLACE;

		mode->vtotal = (mode->vtotal << 1) | 1;
		mode->vdisplay <<= 1;
		mode->vblank_start <<= 1;
		mode->vblank_width <<= 1;
		mode->vsync_start <<= 1;
		mode->vsync_width <<= 1;
	}

	/* FIXME use vzoom instead? */
	/* FIXME interlace+dblscan doesn't use conv2t4 */
	if (regs->crtc[0x09] & MGA_CRTC9_CONV2T4) {
		mode->flags |= DRM_MODE_FLAG_DBLSCAN;

		mode->vtotal >>= 1;
		mode->vdisplay >>= 1;
		mode->vblank_start >>= 1;
		mode->vblank_width >>= 1;
		mode->vsync_start >>= 1;
		mode->vsync_width >>= 1;
	}
}

static void crtc1_calc_framebuffer(const struct mga_dev *mdev,
				   const struct mga_framebuffer *mfb,
				   const struct drm_display_mode *mode,
				   unsigned int x, unsigned int y,
				   struct mga_crtc1_regs *regs)
{
	unsigned int startadd = calc_startadd(mdev, mfb, x, y);
	unsigned int offset = calc_offset(mdev, mfb);

	if (mode->flags & DRM_MODE_FLAG_INTERLACE) {
		if (!(mode->flags & DRM_MODE_FLAG_DBLSCAN))
			offset <<= 1;
	}

	dev_dbg(mdev->dev, "CRTC1:\n");
	dev_dbg(mdev->dev, " bus        = %u\n", mfb->bus);
	dev_dbg(mdev->dev, " offsets[0] = %u\n", mfb->base.offsets[0]);
	dev_dbg(mdev->dev, " pitches[0] = %u\n", mfb->base.pitches[0]);
	dev_dbg(mdev->dev, " startadd = %x\n", startadd);
	dev_dbg(mdev->dev, " offset   = %u\n", offset);

	regs->crtc[0x0C] = (startadd & 0xff00) >> 8;
	regs->crtc[0x0D] = startadd & 0xff;
	regs->crtc[0x13] = offset & 0xff;

	regs->crtcext[0x00] =
		((startadd & 0x100000) >> 14) |
		((offset   &    0x300) >>  4) |
		((startadd &  0xf0000) >> 16);
	regs->crtcext[0x08] = (startadd & 0x200000) >> 21;
}

void mga_crtc1_restore_framebuffer(struct mga_dev *mdev,
				   const struct mga_crtc1_regs *regs)
{
	mga_crtc_write8(mdev, 0x0C, regs->crtc[0x0C]);
	mga_crtc_write8(mdev, 0x0D, regs->crtc[0x0D]);
	mga_crtc_write8(mdev, 0x13, regs->crtc[0x13]);

	mga_crtcext_write8(mdev, 0x08, regs->crtcext[0x08]);

	/* write CRTC0 last to trigger startadd update */
	mga_crtcext_write8(mdev, 0x00, regs->crtcext[0x00]);
}

unsigned int mga_crtc1_vidrst_delay(const struct mga_dev *mdev, u32 pixel_format)
{
	u8 vidrst_delay = 0;

	switch (drm_format_plane_cpp(pixel_format, 0)) {
	case 1:
		vidrst_delay = 31;
		break;
	case 2:
		vidrst_delay = 21;
		break;
	case 3:
		vidrst_delay = 17;
		break;
	case 4:
		vidrst_delay = 16;
		break;
	default:
		BUG();
	}

	if (mdev->chip < MGA_CHIP_G400)
		vidrst_delay--;

	/* FIXME for some reason the documented delay isn't quite right */
	vidrst_delay += 8;

	return vidrst_delay;
}

int mga_crtc1_calc_mode(struct mga_dev *mdev,
			const struct mga_plane_config *pc,
			struct mga_crtc_config *cc,
			struct mga_crtc1_regs *regs)
{
	const struct drm_framebuffer *fb = pc->fb;
	const struct mga_framebuffer *mfb = to_mga_framebuffer(fb);
	struct drm_display_mode *mode = &cc->adjusted_mode;
	bool vidrst = cc->vidrst;
	unsigned int hzoom = 0x10000 / pc->hscale;
	unsigned int vzoom = 0x10000 / pc->vscale;
	unsigned int x = pc->src.x1 >> 16;
	unsigned int y = pc->src.y1 >> 16;
	int ret;

	ret = crtc1_check_mode(mdev, mode, hzoom, vzoom);
	if (ret)
		return ret;

	ret = crtc1_check_framebuffer(mdev, mfb, mode, x, y);
	if (ret)
		return ret;

	mga_crtc1_init_crtc(regs);

	ret = crtc1_mode_to_timings(mdev, mfb, mode, hzoom, vzoom, regs);
	if (ret)
		return ret;

	crtc1_calc_framebuffer(mdev, mfb, mode, x, y, regs);

	dev_dbg(mdev->dev, "Original CRTC1 mode:\n");
	print_mode(mdev, mode);

	if (vidrst)
		regs->crtcext[0x01] |= MGA_CRTCEXT1_VRSTEN | MGA_CRTCEXT1_HRSTEN;

	/* FIXME detect hzoom somehow? */
	vzoom = mga_crtc1_detect_vzoom(regs);

	crtc1_timings_to_mode(mdev, regs, hzoom, mode);

	dev_dbg(mdev->dev, "Adjusted CRTC1 mode:\n");
	print_mode(mdev, mode);

	return 0;
}

void mga_crtc1_enable_vidrst(struct mga_dev *mdev, bool enable)
{
	u8 val;

	val = mga_crtcext_read8(mdev, 0x01);

	if (enable)
		val |= MGA_CRTCEXT1_VRSTEN | MGA_CRTCEXT1_HRSTEN;
	else
		val &= ~(MGA_CRTCEXT1_VRSTEN | MGA_CRTCEXT1_HRSTEN);

	mga_write8(mdev, MGA_CRTCEXTD, val);
}

enum {
	MGA_FRAME_TIMEOUT = 100,
};

void mga_crtc1_wait_vblank(struct mga_dev *mdev)
{
#if 1
	if (_mga_crtc1_wait_vblank(mdev) == 0)
		dev_err(mdev->dev, "CRTC1 VBLANK timeout\n");
#else
	unsigned long timeout = jiffies + msecs_to_jiffies(MGA_FRAME_TIMEOUT);

	mga_write32(mdev, MGA_ICLEAR, MGA_ICLEAR_VLINEICLR);

	dev_dbg(mdev->dev, "CRTC1 VBLANK [");
	while (!(mga_read32(mdev, MGA_STATUS) & MGA_STATUS_VLINEPEN) &&
	       time_before(jiffies, timeout))
		printk(KERN_CONT ".");
	printk(KERN_CONT "]\n");

	if (time_after_eq(jiffies, timeout))
		dev_err(mdev->dev, "CRTC1 VBLANK timeout\n");
#endif
}

void mga_crtc1_wait_active_video(struct mga_dev *mdev)
{
	unsigned long timeout = jiffies + msecs_to_jiffies(MGA_FRAME_TIMEOUT);

	dev_dbg(mdev->dev, "CRTC1 ACT [");
	while (mga_read32(mdev, MGA_VCOUNT) && time_before(jiffies, timeout))
		printk(KERN_CONT ".");
	printk(KERN_CONT "]\n");

	if (time_after_eq(jiffies, timeout))
		dev_err(mdev->dev, "CRTC1 ACT timeout\n");
}
