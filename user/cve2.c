/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <string.h>
#include <errno.h>

#include "drm_kms.h"
#include "cve2.h"

static void cve2_write8(struct cve2_dev *cdev, u8 reg, u8 val)
{
	dev_dbg(cdev->dev, "CVE2 %02x <- %02x   (%d)\n", reg, val, val);
	cdev->write8(cdev->data, reg, val);
}

static void cve2_write16(struct cve2_dev *cdev, u8 reg, u16 val)
{
	dev_dbg(cdev->dev, "CVE2 %02x <- %04x (%d)\n", reg, val, val);
	cdev->write16(cdev->data, reg, val);
}

static u8 cve2_read8(struct cve2_dev *cdev, u8 reg)
{
	return cdev->read8(cdev->data, reg);
}

static u16 cve2_read16(struct cve2_dev *cdev, u8 reg)
{
	return cdev->read16(cdev->data, reg);
}

static void cve2_dump8(struct cve2_dev *cdev, u8 reg)
{
	u16 val = cve2_read8(cdev, reg);
	dev_dbg(cdev->dev, "CVE2 %02x = %02x   (%d)\n", reg, val, val);
}

static void cve2_dump10(struct cve2_dev *cdev, u8 reg)
{
	u16 val = cve2_read16(cdev, reg);
	dev_dbg(cdev->dev, "CVE2 %02x = %04x (%d)\n", reg, val,
		((val & 0x0300) >> 8) | ((val & 0x00ff) << 2));
}

static void cve2_dump11(struct cve2_dev *cdev, u8 reg)
{
	u16 val = cve2_read16(cdev, reg);
	dev_dbg(cdev->dev, "CVE2 %02x = %04x (%d)\n", reg, val,
		((val & 0x0700) >> 8) | ((val & 0x00ff) << 3));
}

static void cve2_dump15(struct cve2_dev *cdev, u8 reg)
{
	u16 val = cve2_read16(cdev, reg);
	dev_dbg(cdev->dev, "CVE2 %02x = %04x (%d)\n", reg, val,
		((val & 0x7f00) >> 8) | ((val & 0x00ff) << 7));
}

void cve2_test(struct cve2_dev *cdev, char len, u8 reg, unsigned int val, bool write)
{
	if (len != 'b' && len != 'w')
		return;
	if (reg > 0x3f)
		return;

	if (write) {
		if (len == 'w') {
			if (val > 0xffff)
				return;
			cve2_write16(cdev, reg, val);
		} else {
			if (val > 0xff)
				return;
			cve2_write8(cdev, reg, val);
		}
	} else {
		if (len == 'w') {
			val = cve2_read16(cdev, reg);
			dev_dbg(cdev->dev, "%02x = %04x (%d)\n",
				reg, val, val);
		} else {
			val = cve2_read8(cdev, reg);
			dev_dbg(cdev->dev, "%02x = %02x   (%d)\n",
				reg, val, val);
		}
	}
}

#if 0
static const struct cve2_adjustments g100_desktop_adj[] = {
	[DRM_TV_STD_PAL] = {
		.brightness = 0x57,
		.contrast   = 0x79,
		.saturation = 0x67,
		.hue        = 0x00,
	},
	[DRM_TV_STD_NTSC] = {
		.brightness = 0x64,
		.contrast   = 0x64,
		.saturation = 0x5a,
		.hue        = 0x00,
	},
};

static const struct cve2_adjustments g100_video_adj[] = {
	[DRM_TV_STD_PAL] = {
		.brightness = 0xa7,
		.contrast   = 0xff,
		.saturation = 0x8a,
		.hue        = 0x00,
	},
	[DRM_TV_STD_NTSC] = {
		.brightness = 0xb4,
		.contrast   = 0xea,
		.saturation = 0x89,
		.hue        = 0x00,
	},
};

static const struct cve2_adjustments g400_desktop_adj[] = {
	[DRM_TV_STD_PAL] = {
		.brightness = 0x57,
		.contrast   = 0x79,
		.saturation = 0x72,
		.hue        = 0x00,
	},
	[DRM_TV_STD_NTSC] = {
		.brightness = 0x64,
		.contrast   = 0x64,
		.saturation = 0x5f,
		.hue        = 0x00,
	},
};

static const struct cve2_adjustments g400_video_adj[] = {
	[DRM_TV_STD_PAL] = {
		.brightness = 0xa8,
		.contrast   = 0xff,
		.saturation = 0x95,
		.hue        = 0x00,
	},
	[DRM_TV_STD_NTSC] = {
		.brightness = 0xb5,
		.contrast   = 0xea,
		.saturation = 0x8e,
		.hue        = 0x00,
	},
};

static const struct cve2_adjustments g450_desktop_adj[] = {
	[DRM_TV_STD_PAL] = {
		.brightness = 0x50,
		.contrast   = 0x78,
		.saturation = 0x96,
		.hue        = 0x00,
	},
	[DRM_TV_STD_NTSC] = {
		.brightness = 0x5a,
		.contrast   = 0x64,
		.saturation = 0x82,
		.hue        = 0x00,
	},
};

static const struct cve2_adjustments g450_video_adj[] = {
	[DRM_TV_STD_PAL] = {
		.brightness = 0x9e,
		.contrast   = 0xff,
		.saturation = 0xbb,
		.hue        = 0x00,
	},
	[DRM_TV_STD_NTSC] = {
		.brightness = 0xaa,
		.contrast   = 0xea,
		.saturation = 0xae,
		.hue        = 0x00,
	},
};
#endif

static void reset_desktop_adjustments(struct cve2_dev *cdev, u8 std)
{
	memcpy(&cdev->desktop_adj[std],
	       cdev->desktop_adj_defaults[std],
	       sizeof cdev->desktop_adj[std]);
}

static void reset_video_adjustments(struct cve2_dev *cdev, u8 std)
{
	memcpy(&cdev->video_adj[std],
	       cdev->video_adj_defaults[std],
	       sizeof cdev->video_adj[std]);
}

#if 0
static const struct cve2_bwlevel g100_bwlevel[] = {
	[DRM_TV_STD_PAL]  = { .wlmax = 0x031f, .blmin = 0x00ff, },
	[DRM_TV_STD_NTSC] = { .wlmax = 0x0322, .blmin = 0x00f0, },
};

static const struct cve2_bwlevel g400_bwlevel[] = {
	[DRM_TV_STD_PAL]  = { .wlmax = 0x0312, .blmin = 0x00ff, },
	[DRM_TV_STD_NTSC] = { .wlmax = 0x0312, .blmin = 0x00f2, },
};

static const struct cve2_bwlevel g450_bwlevel[] = {
	[DRM_TV_STD_PAL]  = { .wlmax = 0x03aa, .blmin = 0x0119, },
	[DRM_TV_STD_NTSC] = { .wlmax = 0x03a8, .blmin = 0x010b, },
};
#endif

void cve2_program_bwlevel(struct cve2_dev *cdev, u8 brightness, u8 contrast)
{
	int b, c, bl, wl, wlmax, blmin, range;
	const struct cve2_bwlevel *bwlevel = cdev->bwlevel[cdev->tv_std];

	wlmax = bwlevel->wlmax;
	blmin = bwlevel->blmin;
	range = wlmax - blmin - 128;

	b = brightness * range / 0xff + blmin;
	c = contrast * (range / 2) / 0xff + 64;

	bl = max(b - c, blmin);
	wl = min(b + c, wlmax);

	blmin = ((blmin << 8) & 0x0300) | ((blmin >> 2) & 0x00ff);
	bl    = ((bl    << 8) & 0x0300) | ((bl    >> 2) & 0x00ff);
	wl    = ((wl    << 8) & 0x0300) | ((wl    >> 2) & 0x00ff);

	cve2_write16(cdev, 0x10, blmin);
	cve2_write16(cdev, 0x0e, bl);
	cve2_write16(cdev, 0x1e, wl);
}

void cve2_program_hue(struct cve2_dev *cdev, u8 hue)
{
	cve2_write8(cdev, 0x25, hue);
}

void cve2_program_saturation(struct cve2_dev *cdev, u8 saturation)
{
	cve2_write8(cdev, 0x20, saturation);
	cve2_write8(cdev, 0x22, saturation);
}

void cve2_program_adjustments(struct cve2_dev *cdev)
{
	const struct cve2_adjustments *adj =
		cdev->adj_mode == CVE2_ADJ_MODE_DESKTOP ?
		&cdev->desktop_adj[cdev->tv_std] : &cdev->video_adj[cdev->tv_std];

	cve2_program_saturation(cdev, adj->saturation);
	cve2_program_hue(cdev, adj->hue);
	cve2_program_bwlevel(cdev, adj->brightness, adj->contrast);
}

static void program_chroma_sub_carrier(struct cve2_dev *cdev)
{
	const u8 *regs = cdev->regs[cdev->tv_std]->regs;

	if (cdev->tv_std == DRM_TV_STD_NTSC && cdev->dot_crawl_freeze) {
		cve2_write8(cdev, 0x00, 0x21);
		cve2_write8(cdev, 0x01, 0xf0);
		cve2_write8(cdev, 0x02, 0x72);
		cve2_write8(cdev, 0x03, 0xcc);
	} else {
		cve2_write8(cdev, 0x00, regs[0x00]);
		cve2_write8(cdev, 0x01, regs[0x01]);
		cve2_write8(cdev, 0x02, regs[0x02]);
		cve2_write8(cdev, 0x03, regs[0x03]);
	}
}

void cve2_program_regs(struct cve2_dev *cdev)
{
	const u8 *regs = cdev->regs[cdev->tv_std]->regs;

	program_chroma_sub_carrier(cdev);
	cve2_write8(cdev, 0x04, regs[0x04]);
	cve2_write8(cdev, 0x2c, regs[0x2c]);
	cve2_write8(cdev, 0x08, regs[0x08]);
	cve2_write8(cdev, 0x0a, regs[0x0a]);
	cve2_write8(cdev, 0x09, regs[0x09]);
	cve2_write8(cdev, 0x29, regs[0x29]);
	cve2_write16(cdev, 0x31, regs[0x31] | (regs[0x32] << 8));
	cve2_write16(cdev, 0x17, regs[0x17] | (regs[0x18] << 8));
	cve2_write8(cdev, 0x0b, regs[0x0b]);
	cve2_write8(cdev, 0x0c, regs[0x0c]);
	cve2_write8(cdev, 0x35, regs[0x35]);
	cve2_write16(cdev, 0x10, regs[0x10] | (regs[0x11] << 8));
	cve2_write16(cdev, 0x0e, regs[0x0e] | (regs[0x0f] << 8));
	cve2_write16(cdev, 0x1e, regs[0x1e] | (regs[0x1f] << 8));
	cve2_write8(cdev, 0x20, regs[0x20]);
	cve2_write8(cdev, 0x22, regs[0x22]);
	cve2_write8(cdev, 0x25, regs[0x25]);
	cve2_write8(cdev, 0x34, regs[0x34]);

	cve2_write8(cdev, 0x33, regs[0x33]);
	cve2_write8(cdev, 0x19, regs[0x19]);
	cve2_write8(cdev, 0x12, regs[0x12]);
	cve2_write8(cdev, 0x3b, regs[0x3b]);
	cve2_write8(cdev, 0x13, regs[0x13]);
	cve2_write8(cdev, 0x39, regs[0x39]);
	cve2_write8(cdev, 0x1d, regs[0x1d]);
	cve2_write8(cdev, 0x3a, regs[0x3a]);
	cve2_write8(cdev, 0x24, regs[0x24]);
	cve2_write8(cdev, 0x14, regs[0x14]);
	cve2_write8(cdev, 0x15, regs[0x15]);
	cve2_write8(cdev, 0x16, regs[0x16]);
	cve2_write16(cdev, 0x2d, regs[0x2d] | (regs[0x2e] << 8));
	cve2_write16(cdev, 0x2f, regs[0x2f] | (regs[0x30] << 8));
	cve2_write8(cdev, 0x1a, regs[0x1a]);
	cve2_write8(cdev, 0x1b, regs[0x1b]);
	cve2_write8(cdev, 0x1c, regs[0x1c]);
	cve2_write8(cdev, 0x23, regs[0x23]);
	cve2_write8(cdev, 0x26, regs[0x26]);
	cve2_write8(cdev, 0x28, regs[0x28]);
	cve2_write8(cdev, 0x27, regs[0x27]);
	cve2_write8(cdev, 0x21, regs[0x21]);
	cve2_write16(cdev, 0x2a, regs[0x2a] | (regs[0x2b] << 8));
	cve2_write8(cdev, 0x35, regs[0x35]);
	cve2_write16(cdev, 0x3c, regs[0x3c] | (regs[0x3d] << 8));
	cve2_write8(cdev, 0x37, regs[0x37]);
	cve2_write8(cdev, 0x38, regs[0x38]);
}

#if 0
static void cve2_init_regs(struct cve2_dev *cdev)
{
	/* FIXME */
	static const u8 matroxfb_regs[2][0x40] = {
		[DRM_TV_STD_PAL] = {
			0x2a, 0x09, 0x8a, 0xcb, /* 00-03 */
			0x00, /* 04 */
			0x00,
			0x00,
			0x00,
			0x7e, /* 08 */
			0x3c, /* 09 */ //44
			0x82, /* 0a */ //9c
			0x2e, /* 0b */
			0x21, /* 0c */
			0x00,
			0x3f, 0x03, /* 0e-0f */
			0x3f, 0x03, /* 10-11 */
			0x1a, /* 12 */
			0x2a, /* 13 */
			0x1c, /* 14 */
			0x3d, /* 15 */
			0x14, /* 16 */
			0x9c, 0x01, /* 17-18 */
			0x00, /* 19 */
			0xfe, /* 1a */
			0x7e, /* 1b */
			0x60, /* 1c */
			0x05, /* 1d */
			0xc4, 0x01, /* 1e-1f */ //89 03
			0x95, /* 20 */ //72
			0x07, /* 21 */
			0x95, /* 22 */ //72
			0x00, /* 23 */
			0x00, /* 24 */
			0x00, /* 25 */
			0x08, /* 26 */
			0x04, /* 27 */
			0x00, /* 28 */
			0x1a, /* 29 */
			0x55, 0x01, /* 2a-2b */
			0x20, /* 2c */ //26
			0x07, 0x7e, /* 2d-2e */
			0x02, 0x54, /* 2f-30 */
			0xb4, 0x00, /* 31-32 */ //b0 00
			0x14, /* 33 */
			0x49, /* 34 */
			0x1d, /* 35 */
			0x00,
			0xa3, /* 37 */
			0xc8, /* 38 */
			0x22, /* 39 */
			0x02, /* 3a */
			0x22, /* 3b */
			0x3f, 0x03, /* 3c-3d */
			0x00, /* 3e */
			0x00,
		},
		[DRM_TV_STD_NTSC] = {
			0x21, 0xf0, 0x7c, 0x1f, /* 00-03 */
			0x00, /* 04 */
			0x00,
			0x00,
			0x00,
			0x7e, /* 08 */
			0x43, /* 09 */
			0x7e, /* 0a */
			0x3d, /* 0b */
			0x00, /* 0c */
			0x00,
			0x46, 0x03, /* 0e-0f */ //41 00
			0x3c, 0x02, /* 10-11 */ //3c 00
			0x17, /* 12 */
			0x21, /* 13 */
			0x1b, /* 14 */
			0x1b, /* 15 */
			0x24, /* 16 */
			0x83, 0x01, /* 17-18 */
			0x00, /* 19 */
			0x0f, /* 1a */
			0x0f, /* 1b */
			0x60, /* 1c */
			0x05, /* 1d */
			0xc4, 0x02, /* 1e-1f */ //89 02
			0x8e, /* 20 */ //5f
			0x04, /* 21 */
			0x8e, /* 22 */ //5f
			0x01, /* 23 */
			0x02, /* 24 */
			0x00, /* 25 */
			0x0a, /* 26 */
			0x05, /* 27 */
			0x00, /* 28 */
			0x10, /* 29 */
			0xff, 0x03, /* 2a-2b */
			0x18, /* 2c */ //24
			0x0f, 0x78, /* 2d-2e */
			0x00, 0x00, /* 2f-30 */
			0xb4, 0x00, /* 31-32 */ //b2 04
			0x14, /* 33 */
			0x02, /* 34 */
			0x1c, /* 35 */
			0x00,
			0xa3, /* 37 */
			0xc8, /* 38 */
			0x15, /* 39 */
			0x05, /* 3a */
			0x15, /* 3b */ //3b
			0x3c, 0x00, /* 3c-3d */
			0x00, /* 3e */
			0x00,
		},
	};
	static const u8 g100_regs[2][0x40] = {
		[DRM_TV_STD_PAL] = {
		},
		[DRM_TV_STD_NTSC] = {
		},
	};
	static const u8 g400_regs[2][0x40] = {
		[DRM_TV_STD_PAL] = {
			0x2a, 0x09, 0x8a, 0xcb, /* 00-03 */
			0x00, /* 04 */
			0x00,
			0x00,
			0x00,
			0x7e, /* 08 */
			0x3c, /* 09 */
			0x82, /* 0a */
			0x2e, /* 0b */
			0x21, /* 0c */
			0x00,
			0x3f, 0x03, /* 0e-0f */
			0x3f, 0x03, /* 10-11 */
			0x1a, /* 12 */
			0x2a, /* 13 */
			0x1c, /* 14 */
			0x3d, /* 15 */
			0x14, /* 16 */
			0x9c, 0x01, /* 17-18 */
			0x00, /* 19 */
			0xfe, /* 1a */
			0x7e, /* 1b */
			0x60, /* 1c */
			0x05, /* 1d */
			0xc4, 0x01, /* 1e-1f */
			0x95, /* 20 */
			0x07, /* 21 */
			0x95, /* 22 */
			0x00, /* 23 */
			0x00, /* 24 */
			0x00, /* 25 */
			0x08, /* 26 */
			0x04, /* 27 */
			0x00, /* 28 */
			0x1a, /* 29 */
			0x55, 0x01, /* 2a-2b */
			0x20, /* 2c */
			0x07, 0x7e, /* 2d-2e */
			0x02, 0x54, /* 2f-30 */
			0xb4, 0x00, /* 31-32 */
			0x14, /* 33 */
			0x49, /* 34 */
			0x1d, /* 35 */
			0x00,
			0xa3, /* 37 */
			0xc8, /* 38 */
			0x22, /* 39 */
			0x02, /* 3a */
			0x22, /* 3b */
			0x3f, 0x03, /* 3c-3d */
			0x00, /* 3e */
			0x00,
		},
		[DRM_TV_STD_NTSC] = {
			0x21, 0xf0, 0x7c, 0x1f, /* 00-03 */
			0x00, /* 04 */
			0x00,
			0x00,
			0x00,
			0x7e, /* 08 */
			0x43, /* 09 */
			0x7e, /* 0a */
			0x3d, /* 0b */
			0x00, /* 0c */
			0x00,
			0x46, 0x03, /* 0e-0f */
			0x3c, 0x02, /* 10-11 */
			0x17, /* 12 */
			0x21, /* 13 */
			0x1b, /* 14 */
			0x1b, /* 15 */
			0x24, /* 16 */
			0x83, 0x01, /* 17-18 */
			0x00, /* 19 */
			0x0f, /* 1a */
			0x0f, /* 1b */
			0x60, /* 1c */
			0x05, /* 1d */
			0xc4, 0x02, /* 1e-1f */
			0x8e, /* 20 */
			0x04, /* 21 */
			0x8e, /* 22 */
			0x01, /* 23 */
			0x02, /* 24 */
			0x00, /* 25 */
			0x0a, /* 26 */
			0x05, /* 27 */
			0x00, /* 28 */
			0x10, /* 29 */
			0xff, 0x03, /* 2a-2b */
			0x18, /* 2c */
			0x0f, 0x78, /* 2d-2e */
			0x00, 0x00, /* 2f-30 */
			0xb4, 0x00, /* 31-32 */
			0x14, /* 33 */
			0x02, /* 34 */
			0x1c, /* 35 */
			0x00,
			0xa3, /* 37 */
			0xc8, /* 38 */
			0x15, /* 39 */
			0x05, /* 3a */
			0x15, /* 3b */
			0x3c, 0x00, /* 3c-3d */
			0x00, /* 3e */
			0x00,
		},
	};
	static const u8 g450_regs[2][0x40] = {
		[DRM_TV_STD_PAL] = {
			0x2a, 0x09, 0x8a, 0xcb, /* 00-03 */
			0x00, /* 04 */
			0x00,
			0x00,
			0x00,
			0x7e, /* 08 */
			0x3a, /* 09 */
			0x8a, /* 0a */
			0x38, /* 0b */
			0x28, /* 0c */
			0x00,
			0x46, 0x01, /* 0e-0f */
			0x46, 0x01, /* 10-11 */
			0x1a, /* 12 */
			0x2a, /* 13 */
			0x1c, /* 14 */
			0x3d, /* 15 */
			0x14, /* 16 */
			0x9c, 0x01, /* 17-18 */
			0x00, /* 19 */
			0xfe, /* 1a */
			0x7e, /* 1b */
			0x60, /* 1c */
			0x05, /* 1d */
			0xea, 0x00, /* 1e-1f */
			0xbb, /* 20 */
			0x07, /* 21 */
			0xbb, /* 22 */
			0x00, /* 23 */
			0x00, /* 24 */
			0x00, /* 25 */
			0x08, /* 26 */
			0x04, /* 27 */
			0x00, /* 28 */
			0x1a, /* 29 */
			0x55, 0x01, /* 2a-2b */
			0x18, /* 2c */
			0x07, 0x7e, /* 2d-2e */
			0x02, 0x54, /* 2f-30 */
			0xb4, 0x00, /* 31-32 */
			0x16, /* 33 */
			0x49, /* 34 */
			0x00, /* 35 */
			0x00,
			0xb9, /* 37 */
			0xdd, /* 38 */
			0x22, /* 39 */
			0x02, /* 3a */
			0x22, /* 3b */
			0x46, 0x00, /* 3c-3d */
			0x00, /* 3e */
			0x00,
		},
		[DRM_TV_STD_NTSC] = {
			0x21, 0xf0, 0x7c, 0x1f, /* 00-03 */
			0x00, /* 04 */
			0x00,
			0x00,
			0x00,
			0x7e, /* 08 */
			0x44, /* 09 */
			0x76, /* 0a */
			0x49, /* 0b */
			0x00, /* 0c */
			0x00,
			0x4e, 0x03, /* 0e-0f */
			0x42, 0x03, /* 10-11 */
			0x17, /* 12 */
			0x21, /* 13 */
			0x1b, /* 14 */
			0x1b, /* 15 */
			0x24, /* 16 */
			0x83, 0x01, /* 17-18 */
			0x00, /* 19 */
			0x0f, /* 1a */
			0x0f, /* 1b */
			0x60, /* 1c */
			0x05, /* 1d */
			0xea, 0x00, /* 1e-1f */
			0xae, /* 20 */
			0x04, /* 21 */
			0xae, /* 22 */
			0x01, /* 23 */
			0x02, /* 24 */
			0x00, /* 25 */
			0x0a, /* 26 */
			0x05, /* 27 */
			0x00, /* 28 */
			0x11, /* 29 */
			0xff, 0x03, /* 2a-2b */
			0x20, /* 2c */
			0x0f, 0x78, /* 2d-2e */
			0x00, 0x00, /* 2f-30 */
			0xb4, 0x00, /* 31-32 */
			0x14, /* 33 */
			0x02, /* 34 */
			0x00, /* 35 */
			0x00,
			0xbd, /* 37 */
			0xda, /* 38 */
			0x15, /* 39 */
			0x05, /* 3a */
			0x15, /* 3b */
			0x42, 0x03, /* 3c-3d */
			0x00, /* 3e */
			0x00,
		},
	};

	//cdev->regs[DRM_TV_STD_PAL]  = g400_regs[DRM_TV_STD_PAL];
	//cdev->regs[DRM_TV_STD_NTSC] = g400_regs[DRM_TV_STD_NTSC];
}
#endif

int cve2_set_tv_standard(struct cve2_dev *cdev,
			 unsigned int tv_std)
{
	switch (tv_std) {
	case DRM_TV_STD_PAL:
	case DRM_TV_STD_NTSC:
		break;
	default:
		return -EINVAL;
	}

	if (cdev->tv_std == tv_std)
		return 0;

	cdev->tv_std = tv_std;

	return 0;
}

int cve2_set_dot_crawl_freeze(struct cve2_dev *cdev, bool enable)
{
	if (cdev->dot_crawl_freeze == enable)
		return 0;

	cdev->dot_crawl_freeze = enable;

	if (!cdev->enabled || cdev->tv_std != DRM_TV_STD_NTSC)
		return 0;

	program_chroma_sub_carrier(cdev);

	return 0;
}

static int cve2_set_adjustment_mode(struct cve2_dev *cdev, u8 adj_mode)
{
	switch (adj_mode) {
	case CVE2_ADJ_MODE_DESKTOP:
	case CVE2_ADJ_MODE_VIDEO:
		break;
	default:
		return -EINVAL;
	}

	if (cdev->adj_mode == adj_mode)
		return 0;

	cdev->adj_mode = adj_mode;

	if (!cdev->enabled)
		return 0;

	cve2_program_adjustments(cdev);

	return 0;
}

static int cve2_reset_adjustments(struct cve2_dev *cdev, u8 std, u8 adj_mode)
{
	switch (std) {
	case DRM_TV_STD_PAL:
	case DRM_TV_STD_NTSC:
		break;
	default:
		return -EINVAL;
	}

	switch (adj_mode) {
	case CVE2_ADJ_MODE_DESKTOP:
		reset_desktop_adjustments(cdev, std);
		break;
	case CVE2_ADJ_MODE_VIDEO:
		reset_video_adjustments(cdev, std);
		break;
	default:
		return -EINVAL;
	}

	if (!cdev->enabled || cdev->adj_mode != adj_mode || cdev->tv_std != std)
		return 0;

	cve2_program_adjustments(cdev);

	return 0;
}

void cve2_power(struct cve2_dev *cdev, bool enable)
{
	cve2_write8(cdev, 0x3e, enable ? 0x00 : 0x01);

	cdev->enabled = enable;
}

static void cve2_dump(struct cve2_dev *cdev)
{
	dev_dbg(cdev->dev, "CVE2 registers:\n");
	cve2_dump8(cdev, 0x19);//
	cve2_dump8(cdev, 0x12);//
	cve2_dump8(cdev, 0x3b);//
	cve2_dump8(cdev, 0x13);//
	cve2_dump8(cdev, 0x39);//
	cve2_dump8(cdev, 0x1d);//
	cve2_dump8(cdev, 0x3a);//
	cve2_dump8(cdev, 0x24);//
	cve2_dump8(cdev, 0x14);//
	cve2_dump8(cdev, 0x15);//
	cve2_dump8(cdev, 0x16);//
	cve2_dump15(cdev, 0x2d);//15
	cve2_dump15(cdev, 0x2f);//15
	cve2_dump8(cdev, 0x1a);//
	cve2_dump8(cdev, 0x1b);//
	cve2_dump8(cdev, 0x1c);//
	cve2_dump8(cdev, 0x23);//
	cve2_dump8(cdev, 0x26);//
	cve2_dump8(cdev, 0x28);//
	cve2_dump8(cdev, 0x27);//
	cve2_dump8(cdev, 0x21);//
	cve2_dump10(cdev, 0x2a);//10
	cve2_dump8(cdev, 0x35);//
	cve2_dump10(cdev, 0x3c);//10
	cve2_dump8(cdev, 0x37);//
	cve2_dump8(cdev, 0x38);//
	cve2_dump8(cdev, 0x36);//not used?

	cve2_dump8(cdev, 0x00);//32
	cve2_dump8(cdev, 0x01);//32
	cve2_dump8(cdev, 0x02);//32
	cve2_dump8(cdev, 0x03);//32
	cve2_dump8(cdev, 0x04);
	cve2_dump8(cdev, 0x05);
	cve2_dump8(cdev, 0x06);
	cve2_dump8(cdev, 0x07);
	cve2_dump8(cdev, 0x08);
	cve2_dump8(cdev, 0x09);
	cve2_dump8(cdev, 0x0a);
	cve2_dump8(cdev, 0x0b);
	cve2_dump8(cdev, 0x0c);
	cve2_dump8(cdev, 0x0d);
	cve2_dump10(cdev, 0x0e);//10
	cve2_dump10(cdev, 0x10);//10
	cve2_dump10(cdev, 0x17);//10
	cve2_dump10(cdev, 0x1e);//10
	cve2_dump8(cdev, 0x20);
	cve2_dump8(cdev, 0x22);
	cve2_dump8(cdev, 0x25);
	cve2_dump8(cdev, 0x29);
	cve2_dump8(cdev, 0x2c);
	cve2_dump11(cdev, 0x31);//11
	cve2_dump8(cdev, 0x33);
	cve2_dump8(cdev, 0x34);
	cve2_dump8(cdev, 0x3e);
	cve2_dump8(cdev, 0x3f);
}

void cve2_init(struct cve2_dev *cdev,
	       struct device *dev,
	       const struct cve2_adjustments desktop_adj[2],
	       const struct cve2_adjustments video_adj[2],
	       const struct cve2_bwlevel bwlevel[2],
	       const struct cve2_regs regs[2],
	       void (*write8)(void *data, u8 reg, u8 val),
	       void (*write16)(void *data, u8 reg, u16 val),
	       u8 (*read8)(void *data, u8 reg),
	       u16 (*read16)(void *data, u8 reg),
	       void *data)
{
	cdev->dev = dev;
	cdev->write8 = write8;
	cdev->write16 = write16;
	cdev->read8 = read8;
	cdev->read16 = read16;
	cdev->data = data;

	cdev->desktop_adj_defaults[DRM_TV_STD_PAL]  = &desktop_adj[DRM_TV_STD_PAL];
	cdev->desktop_adj_defaults[DRM_TV_STD_NTSC] = &desktop_adj[DRM_TV_STD_NTSC];

	cdev->video_adj_defaults[DRM_TV_STD_PAL]  = &video_adj[DRM_TV_STD_PAL];
	cdev->video_adj_defaults[DRM_TV_STD_NTSC] = &video_adj[DRM_TV_STD_NTSC];

	cdev->bwlevel[DRM_TV_STD_PAL]  = &bwlevel[DRM_TV_STD_PAL];
	cdev->bwlevel[DRM_TV_STD_NTSC] = &bwlevel[DRM_TV_STD_NTSC];

	cdev->regs[DRM_TV_STD_PAL]  = &regs[DRM_TV_STD_PAL];
	cdev->regs[DRM_TV_STD_NTSC] = &regs[DRM_TV_STD_NTSC];

	reset_desktop_adjustments(cdev, DRM_TV_STD_PAL);
	reset_desktop_adjustments(cdev, DRM_TV_STD_NTSC);
	reset_video_adjustments(cdev, DRM_TV_STD_PAL);
	reset_video_adjustments(cdev, DRM_TV_STD_NTSC);

	cdev->enabled = false;
	cdev->dot_crawl_freeze = false;
	cdev->tv_std = DRM_TV_STD_PAL;
	cdev->adj_mode = CVE2_ADJ_MODE_DESKTOP;
}

unsigned int cve2_get_tv_standard(struct cve2_dev *cdev)
{
	return cdev->tv_std;
}
