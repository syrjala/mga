/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef DRM_KMS_H
#define DRM_KMS_H

#include "list.h"

enum {
	DRM_MODE_FLAG_PHSYNC    = 1<<0,
	DRM_MODE_FLAG_NHSYNC    = 1<<1,
	DRM_MODE_FLAG_PVSYNC    = 1<<2,
	DRM_MODE_FLAG_NVSYNC    = 1<<3,
	DRM_MODE_FLAG_INTERLACE = 1<<4,
	DRM_MODE_FLAG_DBLSCAN   = 1<<5,
	DRM_MODE_FLAG_CSYNC     = 1<<6,
	DRM_MODE_FLAG_PCSYNC    = 1<<7,
	DRM_MODE_FLAG_NCSYNC    = 1<<8,
	DRM_MODE_FLAG_IOGSYNC   = 1<<14,
};

enum {
	DRM_MODE_DPMS_ON      = 0,
	DRM_MODE_DPMS_STANDBY = 1,
	DRM_MODE_DPMS_SUSPEND = 2,
	DRM_MODE_DPMS_OFF     = 3,
};

struct drm_mode_object {
	unsigned int id;
	unsigned int type;
	struct list_head head;
};

struct drm_display_mode {
	struct drm_mode_object base;

	u32 mode_id;

	u32 vrefresh;

	u32 clock;

	u32 flags;

	u32 vtotal;
	u32 vdisplay;
	u32 vblank_start;
	u32 vblank_width;
	u32 vsync_start;
	u32 vsync_width;

	u32 htotal;
	u32 hdisplay;
	u32 hblank_start;
	u32 hblank_width;
	u32 hsync_start;
	u32 hsync_width;

	u32 hskew;
	u32 vscan;

	u32 private_flags;
};

enum {
	DRM_FB_INTERLACED = 0x1,
};

enum {
	DRM_FORMAT_UNKNOWN = 0,
	DRM_FORMAT_C1,
	DRM_FORMAT_C2,
	DRM_FORMAT_C4,
	DRM_FORMAT_C6,
	DRM_FORMAT_C8,

	DRM_FORMAT_AC44,

	DRM_FORMAT_XRGB4444,
	DRM_FORMAT_ARGB4444,
	DRM_FORMAT_RGBX4444,
	DRM_FORMAT_RGBA4444,

	DRM_FORMAT_XBGR4444,
	DRM_FORMAT_ABGR4444,
	DRM_FORMAT_BGRX4444,
	DRM_FORMAT_BGRA4444,

	DRM_FORMAT_XRGB1555,
	DRM_FORMAT_ARGB1555,
	DRM_FORMAT_RGBX5551,
	DRM_FORMAT_RGBA5551,

	DRM_FORMAT_XBGR1555,
	DRM_FORMAT_ABGR1555,
	DRM_FORMAT_BGRX5551,
	DRM_FORMAT_BGRA5551,

	DRM_FORMAT_RGB565,
	DRM_FORMAT_BGR565,

	DRM_FORMAT_RGB888,
	DRM_FORMAT_BGR888,

	DRM_FORMAT_XRGB8888,
	DRM_FORMAT_ARGB8888,
	DRM_FORMAT_RGBX8888,
	DRM_FORMAT_RGBA8888,

	DRM_FORMAT_XBGR8888,
	DRM_FORMAT_ABGR8888,
	DRM_FORMAT_BGRX8888,
	DRM_FORMAT_BGRA8888,

	DRM_FORMAT_YUYV,
	DRM_FORMAT_UYVY,
	DRM_FORMAT_VYUY,
	DRM_FORMAT_YVYU,

	DRM_FORMAT_YUV420,
	DRM_FORMAT_YVU420,

	DRM_FORMAT_NV12,
	DRM_FORMAT_NV21,
};

struct drm_device;

typedef struct drm_framebuffer *(*drm_fb_create_t)(struct drm_device *dev,
						   unsigned int width,
						   unsigned int height,
						   unsigned int pixel_format,
						   unsigned int flags,
						   unsigned int offsets[4],
						   unsigned int pitches[4]);
typedef void (*drm_fb_destroy_t)(struct drm_device *dev, struct drm_framebuffer *fb);

enum {
	DRM_MODE_OBJECT_CRTC      = 0xcccccccc,
	DRM_MODE_OBJECT_ENCODER   = 0xe0e0e0e0,
	DRM_MODE_OBJECT_CONNECTOR = 0xc0c0c0c0,
	DRM_MODE_OBJECT_PLANE     = 0xeeeeeeee,
	DRM_MODE_OBJECT_FB        = 0xfbfbfbfb,
	DRM_MODE_OBJECT_PROPERTY  = 0xb0b0b0b0,
	DRM_MODE_OBJECT_BLOB      = 0xbbbbbbbb,
	DRM_MODE_OBJECT_MODE      = 0xdededede,
	DRM_MODE_OBJECT_ANY       = 0,
};

#define obj_to_framebuffer(o) container_of((o), struct drm_framebuffer, base)
#define obj_to_plane(o) container_of((o), struct drm_plane, base)
#define obj_to_crtc(o) container_of((o), struct drm_crtc, base)
#define obj_to_encoder(o) container_of((o), struct drm_encoder, base)
#define obj_to_connector(o) container_of((o), struct drm_connector, base)
#define obj_to_property(o) container_of((o), struct drm_property, base)

enum {
	DRM_MODE_PROP_BLOB = 1,
};

struct drm_property {
	struct drm_mode_object base;
	unsigned int flags;
};

struct drm_mode_config {
	struct mutex mutex;
	struct list_head plane_list;
	struct list_head crtc_list;
	struct list_head encoder_list;
	struct list_head connector_list;
	struct list_head fb_list;

	unsigned int num_plane;
	unsigned int num_crtc;
	unsigned int num_encoder;
	unsigned int num_connector;
	unsigned int num_fb;

	/* all objects */
	struct list_head obj_list;

	struct drm_property prop_src_x;
	struct drm_property prop_src_y;
	struct drm_property prop_src_w;
	struct drm_property prop_src_h;

	struct drm_property prop_crtc_x;
	struct drm_property prop_crtc_y;
	struct drm_property prop_crtc_w;
	struct drm_property prop_crtc_h;

	struct drm_property prop_fb_id;
	struct drm_property prop_crtc_id;

	struct drm_property prop_mode;
	struct drm_property prop_connector_ids;

	struct drm_property prop_rotate;

	struct drm_property prop_lut;
	struct drm_property prop_overlay;
	struct drm_property prop_overlay_color_key;

	struct drm_property prop_tv_std;
	struct drm_property prop_cable_type;
	struct drm_property prop_text_filter;
	struct drm_property prop_deflicker;
	struct drm_property prop_color_bars;
	struct drm_property prop_dot_crawl_freeze;
	struct drm_property prop_gamma;
};

struct drm_device {
	struct drm_mode_config mode_config;
	struct {
		drm_fb_create_t create;
		drm_fb_destroy_t destroy;
		drm_fb_destroy_t pre_destroy;
	} fb;
};

struct drm_framebuffer {
	struct drm_mode_object base;
	struct drm_device *dev;
	unsigned int flags;
	unsigned int pixel_format;
	unsigned int width, height;
	unsigned int pitches[4];
	unsigned int offsets[4];
	struct list_head head;
};

struct drm_plane {
	struct drm_mode_object base;
	struct drm_device *dev;
	unsigned int possible_crtcs;
	struct list_head head;
	void (*destroy)(struct drm_plane *plane);
};

struct drm_crtc {
	struct drm_mode_object base;
	struct drm_device *dev;
	struct list_head head;
	void (*destroy)(struct drm_crtc *crtc);
};

struct drm_encoder {
	struct drm_mode_object base;
	struct drm_device *dev;
	unsigned int possible_crtcs;
	unsigned int possible_clones;
	struct list_head head;
	void (*destroy)(struct drm_encoder *encoder);
};

struct drm_connector {
	struct drm_mode_object base;
	struct drm_device *dev;
	struct list_head head;
	void (*destroy)(struct drm_connector *connector);
};

static inline unsigned int div_round64(u64 num,
				       unsigned int den)
{
	return (2 * num + den) / (2 * den);
}

struct drm_framebuffer *drm_framebuffer_create(struct drm_device *dev,
					       unsigned int width,
					       unsigned int height,
					       unsigned int pixel_format,
					       unsigned int flags,
					       unsigned int offsets[4],
					       unsigned int pitches[4]);
void drm_framebuffer_init(struct drm_framebuffer *fb,
			  unsigned int width,
			  unsigned int height,
			  unsigned int pixel_format,
			  unsigned int flags,
			  unsigned int offsets[4],
			  unsigned int pitches[4]);
void drm_framebuffer_destroy(struct drm_device *dev,
			     struct drm_framebuffer *fb);
void drm_modeset_init(struct drm_device *ddev,
			  drm_fb_create_t create,
			  drm_fb_destroy_t destroy,
			  drm_fb_destroy_t pre_destroy);
void drm_modeset_fini(struct drm_device *ddev);

int drm_format_bits_per_pixel(unsigned int pixel_format);
int drm_format_plane_cpp(unsigned int pixel_format, int plane);
int drm_format_depth(unsigned int pixel_format);
int drm_format_bpp(unsigned int pixel_format);
int drm_format_num_planes(unsigned int pixel_format);
int drm_format_horz_chroma_subsampling(unsigned int pixel_format);
int drm_format_vert_chroma_subsampling(unsigned int pixel_format);

bool drm_framebuffer_equal(const struct drm_framebuffer *fb1,
			   const struct drm_framebuffer *fb2);
bool drm_mode_equal(const struct drm_display_mode *mode1,
		    const struct drm_display_mode *mode2);

struct drm_color {
	u8 r;
	u8 g;
	u8 b;
};

void drm_generate_palette(unsigned int pixel_format, struct drm_color pal[256]);

int drm_mode_object_init(struct drm_device *dev,
			 struct drm_mode_object *obj,
			 unsigned int type);

void drm_mode_object_fini(struct drm_mode_object *obj);

struct drm_mode_object *drm_mode_object_find(struct drm_device *dev,
					     unsigned int id,
					     unsigned int type);

void drm_mode_copy(struct drm_display_mode *dst, const struct drm_display_mode *src);
struct drm_display_mode *drm_mode_create(struct drm_device *dev);
void drm_mode_destroy(struct drm_device *dev, struct drm_display_mode *mode);
struct drm_display_mode *drm_mode_duplicate(struct drm_device *dev,
					    const struct drm_display_mode *mode);


struct drm_framebuffer *drm_framebuffer_find(struct drm_device *dev, unsigned int id);
struct drm_plane *drm_plane_find(struct drm_device *dev, unsigned int id);
struct drm_crtc *drm_crtc_find(struct drm_device *dev, unsigned int id);
struct drm_encoder *drm_encoder_find(struct drm_device *dev, unsigned int id);
struct drm_connector *drm_connector_find(struct drm_device *dev, unsigned int id);

struct drm_region {
	int x1, y1, x2, y2;
};

int drm_region_width(const struct drm_region *r);
int drm_region_height(const struct drm_region *r);
void drm_region_init(struct drm_region *r, int x, int y, int w, int h);
void drm_region_empty(struct drm_region *r);
bool drm_region_clip(struct drm_region *r,
		     const struct drm_region *clip);
bool drm_region_clip_both(struct drm_region *dst,
			  struct drm_region *src,
			  const struct drm_region *clip);
bool drm_region_clip_scaled(struct drm_region *dst,
			    struct drm_region *src,
			    const struct drm_region *clip,
			    int hscale, int vscale);
void drm_region_adjust_size(struct drm_region *r, int x, int y);
bool drm_region_equal(const struct drm_region *a,
		      const struct drm_region *b);

int drm_calc_hscale(const struct drm_region *src,
		    const struct drm_region *dst,
		    int min_hscale, int max_hscale);
int drm_calc_vscale(const struct drm_region *src,
		    const struct drm_region *dst,
		    int min_vscale, int max_vscale);
bool drm_region_visible(const struct drm_region *r);
void drm_region_translate(struct drm_region *r, int x, int y);

struct drm_umode {
	struct drm_display_mode mode;
};

int drm_convert_umode(struct drm_display_mode *mode, const struct drm_umode *umode);

enum {
	DRM_ATOMIC_TEST_ONLY = 1,
};

struct drm_mode_atomic {
	uint32_t flags;
	uint32_t count_objs;
	uint64_t objs_ptr;
	uint64_t count_props_ptr;
	uint64_t props_ptr;
	uint64_t prop_values_ptr;
	uint64_t blob_values_ptr;
};

struct drm_mode_resources {
	uint32_t count_fb;
	uint32_t count_plane;
	uint32_t count_crtc;
	uint32_t count_encoder;
	uint32_t count_connector;
	uint64_t fb_id;
	uint64_t plane_id;
	uint64_t crtc_id;
	uint64_t encoder_id;
	uint64_t connector_id;
};

enum {
	_DRM_IOC_INIT,
	_DRM_IOC_HARDRESET,
	_DRM_IOC_SET_MODE,
	_DRM_IOC_SUSPEND,
	_DRM_IOC_RESUME,
	_DRM_IOC_FILL,
	_DRM_IOC_REG_RW,
	_DRM_IOC_MEM_TEST,
	_DRM_IOC_FINI,
	_DRM_IOC_MK_FB,
	_DRM_IOC_RM_FB,
	_DRM_IOC_LOAD_LUT,
	_DRM_IOC_DMA_GENERAL_PURPOSE,
	_DRM_IOC_ATOMIC,
	_DRM_IOC_GETRESOURCES,
};

struct drm_setmode {
	u32 crtc;
	u32 outputs;
	struct drm_display_mode mode;
	u32 fb_id;
};

struct drm_reg_rw {
	u32 dev;
	u32 dir;
	u32 size;
	u32 reg;
	u32 val;
};

struct drm_mem_test {
	u32 read_bytes;
	u32 write_bytes;
	u32 read_usec;
	u32 write_usec;
	u32 clock;
};

struct drm_mk_fb {
	u32 width;
	u32 height;
	u32 pixel_format;
	u32 flags;
	u32 pitches[4];
	u32 offsets[4];
	u32 fb_id;
};

struct drm_rm_fb {
	u32 fb_id;
};

struct drm_lut {
	u32 crtc;
	struct drm_color lut[256];
};

struct mga_dma_ioc {
	u32 handle;
	u32 len;
	u32 data[0]; /* must be multiples of 5 u32 */
};

enum {
	DRM_IOC_INIT = _IO('m', _DRM_IOC_INIT),
	DRM_IOC_HARDRESET = _IO('m', _DRM_IOC_HARDRESET),
	DRM_IOC_SET_MODE = _IOWR('m', _DRM_IOC_SET_MODE, struct drm_setmode),
	DRM_IOC_SUSPEND = _IO('m', _DRM_IOC_SUSPEND),
	DRM_IOC_RESUME = _IO('m', _DRM_IOC_RESUME),
	DRM_IOC_FILL = _IO('m', _DRM_IOC_FILL),
	DRM_IOC_REG_RW = _IOWR('m', _DRM_IOC_REG_RW, struct drm_reg_rw),
	DRM_IOC_MEM_TEST = _IOWR('m', _DRM_IOC_MEM_TEST, struct drm_mem_test),
	DRM_IOC_FINI = _IO('m', _DRM_IOC_FINI),
	DRM_IOC_MK_FB = _IOWR('m', _DRM_IOC_MK_FB, struct drm_mk_fb),
	DRM_IOC_RM_FB = _IOW('m', _DRM_IOC_RM_FB, struct drm_rm_fb),
	DRM_IOC_LOAD_LUT = _IOW('m', _DRM_IOC_LOAD_LUT, struct drm_lut),
	DRM_IOC_DMA_GENERAL_PURPOSE = _IOWR('m', _DRM_IOC_DMA_GENERAL_PURPOSE, struct mga_dma_ioc),
	DRM_IOC_ATOMIC = _IOW('m', _DRM_IOC_ATOMIC, struct drm_mode_atomic),
	DRM_IOC_GETRESOURCES = _IOWR('m', _DRM_IOC_GETRESOURCES, struct drm_mode_resources),
};

int drm_ioctl(struct drm_device *dev, unsigned int cmd, void *data);

enum drm_rotation {
	DRM_ROTATE_0   = 0x1,
	DRM_ROTATE_90  = 0x2,
	DRM_ROTATE_180 = 0x4,
	DRM_ROTATE_270 = 0x8,
	DRM_REFLECT_X  = 0x10,
	DRM_REFLECT_Y  = 0x20,
};

enum drm_tv_standard {
	DRM_TV_STD_PAL   = 0x0,
	DRM_TV_STD_NTSC  = 0x1,
};

enum {
	DRM_MODE_PROP_CRTC_ID = 0x1000,
	DRM_MODE_PROP_FB_ID,
	DRM_MODE_PROP_SRC_X,
	DRM_MODE_PROP_SRC_Y,
	DRM_MODE_PROP_SRC_W,
	DRM_MODE_PROP_SRC_H,
	DRM_MODE_PROP_CRTC_X,
	DRM_MODE_PROP_CRTC_Y,
	DRM_MODE_PROP_CRTC_W,
	DRM_MODE_PROP_CRTC_H,
	DRM_MODE_PROP_MODE,
	DRM_MODE_PROP_CONNECTOR_IDS,
	DRM_MODE_PROP_ROTATE,
	DRM_MODE_PROP_LUT,
	DRM_MODE_PROP_OVERLAY,
	DRM_MODE_PROP_OVERLAY_COLOR_KEY,

	DRM_MODE_PROP_TV_STD,
	DRM_MODE_PROP_CABLE_TYPE,
	DRM_MODE_PROP_TEXT_FILTER,
	DRM_MODE_PROP_DEFLICKER,
	DRM_MODE_PROP_COLOR_BARS,
	DRM_MODE_PROP_DOT_CRAWL_FREEZE,
	DRM_MODE_PROP_GAMMA,
};

int drm_format_horz_subsampling(unsigned int pixel_format);
int drm_format_vert_subsampling(unsigned int pixel_format);
unsigned int drm_format_plane_width(unsigned int pixel_format, unsigned int width, int plane);
unsigned int drm_format_plane_height(unsigned int pixel_format, unsigned int height, int plane);

void drm_mode_print(struct device *dev, const struct drm_display_mode *mode);

void drm_region_print(const struct drm_region *r,
		      const char *name, bool fixed_point);

/*
 * apply rotation to region
 * width,height are the dimensions of the total area
 */
void drm_region_rotate(struct drm_region *r,
		       int width, int height,
		       unsigned int rotation);
/*
 * undo rotation to region
 * width,height are the dimensions of the total area
 * (in the original orientation, ie. the same exact values
 * you have passed to drm_region_rotate() some time before).
 */
void drm_region_rotate_rev(struct drm_region *r,
			   int width, int height,
			   unsigned int rotation);


int drm_framebuffer_check_viewport(const struct drm_framebuffer *fb,
				   uint32_t x, uint32_t y,
				   uint32_t w, uint32_t h);

#endif
