/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_KMS_H
#define MGA_KMS_H

#include "drm_kms.h"

#define to_mga_plane(p) container_of((p), struct mga_plane, base)
#define to_mga_crtc(c) container_of((c), struct mga_crtc, base)
#define to_mga_encoder(e) container_of((e), struct mga_encoder, base)
#define to_mga_connector(c) container_of((c), struct mga_connector, base)

struct mga_plane {
	struct drm_plane base;
	unsigned int plane;
};

struct mga_crtc {
	struct drm_crtc base;
	unsigned int crtc;
};

struct mga_encoder {
	struct drm_encoder base;
	unsigned int output;
};

struct mga_connector {
	struct drm_connector base;
	unsigned int output;
};

int mga_modeset_init(struct mga_dev *mdev);
void mga_modeset_fini(struct mga_dev *mdev);

unsigned int mga_calc_pixel_clock(const struct drm_display_mode *mode,
				  unsigned int hzoom);

struct mga_crtc_config {
	/* current mode */
	bool mode_valid;
	struct drm_display_mode user_mode;
	struct drm_display_mode adjusted_mode;

	/* current outputs (MGA_OUTPUT_*) */
	unsigned int outputs;

	/* clip region, computed from mode */
	struct drm_region clip;

	bool vidrst;
	bool bt656;
	bool overlay;
};

struct mga_plane_config {
	/* original values */
	uint32_t src_x, src_y, src_w, src_h;
	int32_t crtc_x, crtc_y;
	uint32_t crtc_w, crtc_h;

	/* clipped values */
	struct drm_region src, dst;

	/* current framebuffer */
	struct drm_framebuffer *fb;

	/* current CRTC (MGA_CRTC_*) */
	unsigned int crtc;

	unsigned int rotation;
	bool deinterlace;
	bool overlay;
	uint8_t overlay_ckey;
	int hscale, vscale;
};

enum {
	MGA_DIRTY_CRTC1_MODE  = 0x1,
	MGA_DIRTY_CRTC1_FB    = 0x2,
	MGA_DIRTY_CRTC1_LUT   = 0x4,

	MGA_DIRTY_CRTC1       = 0x3,

	MGA_DIRTY_CRTC2_MODE  = 0x8,
	MGA_DIRTY_CRTC2_FB    = 0x10,

	MGA_DIRTY_CRTC2       = 0x18,

	MGA_DIRTY_CURSOR      = 0x20,
	MGA_DIRTY_BES         = 0x40,
	MGA_DIRTY_SPIC        = 0x80,
};

struct mga_crtc1_regs {
	u8 seq[0x05];
	u8 gctl[0x0A];
	u8 attr[0x15];
	u8 attrx;
	u8 misc;
	u8 crtc[0x19];
	u8 crtcext[0x09];
};

struct mga_crtc2_regs {
	u32 c2ctl;
	u32 c2hparam;
	u32 c2hsync;
	u32 c2vparam;
	u32 c2vsync;
	u32 c2preload;
	u32 c2startadd0;
	u32 c2startadd1;
	u32 c2pl2startadd0;
	u32 c2pl2startadd1;
	u32 c2pl3startadd0;
	u32 c2pl3startadd1;
	u32 c2offset;
	u32 c2misc;
	u32 c2datactl;
	u32 c2spicstartadd0;
	u32 c2spicstartadd1;
	u32 c2subpiclut[16];
};

struct mga_bes_regs {
	u32 besa1org;
	u32 besa2org;
	u32 besa1corg;
	u32 besa2corg;
	u32 besa1c3org;
	u32 besa2c3org;
	u32 besctl;
	u32 bespitch;
	u32 beshcoord;
	u32 besvcoord;
	u32 beshiscal;
	u32 besviscal;
	u32 beshsrcst;
	u32 beshsrcend;
	u32 beslumactl;
	u32 besv1wght;
	u32 besv2wght;
	u32 beshsrclst;
	u32 besv1srclst;
	u32 besv2srclst;
	u32 besglobctl;
};

struct mga_mode_config {
	unsigned int dirty;
	struct mga_tvo_config tvo_config;
	struct mga_dac_config dac_config;
	struct {
		struct mga_crtc_config crtc_config;
		struct mga_plane_config plane_config;
		struct mga_crtc1_regs regs;
		struct drm_color lut[256];
	} crtc1;
	struct {
		struct mga_crtc_config crtc_config;
		struct mga_plane_config plane_config;
		struct mga_crtc2_regs regs;
	} crtc2;
	struct {
		struct mga_plane_config plane_config;
		struct drm_color lut[16];
	} cursor;
	struct {
		struct mga_plane_config plane_config;
		struct mga_bes_regs regs;
	} bes;
	struct {
		struct mga_plane_config plane_config;
		struct drm_color lut[16];
	} spic;
};

#endif
