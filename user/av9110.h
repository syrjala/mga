/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef AV9110_H
#define AV9110_H

#include "kernel_emul.h"
#include "device.h"

struct av9110_dev
{
	struct device *dev;
	unsigned int fref;
	void (*setcen)(void *data, int state);
	void (*setsclk)(void *data, int state);
	void (*setdata)(void *data, int state);
	void *data;
};

struct av9110_pll_settings
{
	unsigned int fclk;
	unsigned int fclkx;
	u32 val;
};

int av9110_init(struct av9110_dev *adev,
		struct device *dev,
		unsigned int fref,
		void (*setcen)(void *data, int state),
		void (*setsclk)(void *data, int state),
		void (*setdata)(void *data, int state),
		void *data);

void av9110_power_down(struct av9110_dev *adev);

int av9110_pll_calc(struct av9110_dev *adev,
		    unsigned int fclk, unsigned int fclkx,
		    struct av9110_pll_settings *pll);

void av9110_pll_program(struct av9110_dev *adev,
			const struct av9110_pll_settings *pll);

void av9110_pll_wait(struct av9110_dev *adev);

#endif
