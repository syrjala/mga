/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include "kernel_emul.h"
#include "mga_dump.h"
#include "mga_regs.h"
#include <errno.h>
#include <limits.h>

int mga_bes_check_framebuffer(const struct mga_dev *mdev,
			      unsigned int pixel_format,
			      unsigned int flags,
			      const unsigned int offsets[4],
			      const unsigned int pitches[4])
{
	unsigned int offset_alignment;
	unsigned int pitch_alignment;
	unsigned int cpp;

	if (mdev->chip < MGA_CHIP_G200)
		return -ENODEV;

	switch (pixel_format) {
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
		break;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		if (mdev->chip < MGA_CHIP_G400)
			return -EINVAL;
		break;
	default:
		return -EINVAL;
	}

	cpp = drm_format_plane_cpp(pixel_format, 0);

	if (pitches[0] / cpp > MGA_BESPITCH_BESPITCH)
		return -EINVAL;

	/* FIXME try these out on real hardware */
	if (mdev->chip >= MGA_CHIP_G400)
		offset_alignment = 15;
	else
		offset_alignment = 7;

	/* FIXME try these out on real hardware */
	switch (pixel_format) {
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
		if (mdev->chip >= MGA_CHIP_G400)
			pitch_alignment = 15;
		else
			pitch_alignment = 7;
		break;
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		if (mdev->chip >= MGA_CHIP_G400)
			pitch_alignment = 31;
		else
			pitch_alignment = 7;
		break;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
		pitch_alignment = 15;
		break;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		pitch_alignment = 31;
		break;
	default:
		BUG();
	}

	switch (drm_format_num_planes(pixel_format)) {
	case 3:
		if (offsets[2] & offset_alignment)
			return -EINVAL;
		if (pitches[2] != pitches[1])
			return -EINVAL;
		/* fall through */
	case 2:
		if (offsets[1] & offset_alignment)
			return -EINVAL;

		if (drm_format_num_planes(pixel_format) == 3) {
			if (pitches[1] != pitches[0] / 2)
				return -EINVAL;
		} else {
			if (pitches[1] != pitches[0])
				return -EINVAL;
		}
		/* fall through */
	case 1:
		if (offsets[0] & offset_alignment)
			return -EINVAL;
		if (pitches[0] & pitch_alignment)
			return -EINVAL;
		break;
	default:
		BUG();
	}

	return 0;
}

int mga_crtc2_check_framebuffer(const struct mga_dev *mdev,
				unsigned int pixel_format,
				unsigned int flags,
				const unsigned int offsets[4],
				const unsigned int pitches[4])
{
	unsigned int pitch_alignment;

	if (mdev->chip < MGA_CHIP_G400)
		return -ENODEV;

	switch (pixel_format) {
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		break;
	default:
		return -EINVAL;
	}

	if (pitches[0] > MGA_C2OFFSET_C2OFFSET)
		return -EINVAL;

	/* FIXME try these out on real hardware */
	switch (pixel_format) {
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		pitch_alignment = 63;
		break;
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		/* c2vcbcrsingle */
		if (flags & DRM_FB_INTERLACED)
			pitch_alignment = 127;
		else
			pitch_alignment = 255;
		break;
	default:
		BUG();
	}

	switch (drm_format_num_planes(pixel_format)) {
	case 3:
		if (offsets[2] & 63)
			return -EINVAL;
		if (pitches[2] != pitches[1])
			return -EINVAL;

		if (offsets[1] & 63)
			return -EINVAL;
		if (pitches[1] != pitches[0] / 2)
			return -EINVAL;
		/* fall through */
	case 1:
		if (offsets[0] & 63)
			return -EINVAL;
		if (pitches[0] & pitch_alignment)
			return -EINVAL;
		break;
	default:
		BUG();
	}

	return 0;
}

int mga_spic_check_framebuffer(const struct mga_dev *mdev,
			       unsigned int pixel_format,
			       unsigned int flags,
			       const unsigned int offsets[4],
			       const unsigned int pitches[4])
{
	unsigned int pitch_alignment;

	if (mdev->chip < MGA_CHIP_G400)
		return -ENODEV;

	switch (pixel_format) {
	case DRM_FORMAT_AC44:
		break;
	default:
		return -EINVAL;
	}

	if (pitches[0] > 0x7fff)
		return -EINVAL;

	/* c2vcbcrsingle */
	if (flags & DRM_FB_INTERLACED)
		pitch_alignment = 127;
	else
		pitch_alignment = 255;

	if (offsets[0] & 63)
		return -EINVAL;
	if (pitches[0] & pitch_alignment)
		return -EINVAL;

	return 0;
}

int mga_crtc1_check_framebuffer(const struct mga_dev *mdev,
				unsigned int pixel_format,
				unsigned int flags,
				const unsigned int offsets[4],
				const unsigned int pitches[4])
{
	switch (pixel_format) {
	case DRM_FORMAT_C8:
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_RGB888:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		break;
	default:
		return -EINVAL;
	}

	if (pitches[0] > crtc1_max_pitch(mdev))
		return -EINVAL;

	if (offsets[0] & (crtc1_address_alignment(mdev) - 1))
		return -EINVAL;
	if (pitches[0] & (crtc1_pitch_alignment(mdev) - 1))
		return -EINVAL;

	return 0;
}

int mga_cursor_check_framebuffer(const struct mga_dev *mdev,
				 unsigned int pixel_format,
				 unsigned int flags,
				 const unsigned int offsets[4],
				 const unsigned int pitches[4])
{
	switch (pixel_format) {
	case DRM_FORMAT_C2:
		break;
	case DRM_FORMAT_C6:
		if (mdev->chip < MGA_CHIP_G200)
			return -EINVAL;
		break;
	default:
		return -EINVAL;
	}

	if (pitches[0] != 64U * drm_format_bits_per_pixel(pixel_format) / 8)
		return -EINVAL;

	return 0;
}

static int mga_framebuffer_check(const struct mga_dev *mdev,
				 const struct drm_framebuffer *fb)
{
	struct mga_framebuffer *mfb = to_mga_framebuffer(fb);
	int r;

	/* we don't support other flags */
	if (fb->flags & ~DRM_FB_INTERLACED)
		return -EINVAL;

	/*
	 * If the requested configuration is not supported
	 * by any scanout engine, reject it immediately.
	 */
	r = mga_crtc1_check_framebuffer(mdev, fb->pixel_format, fb->flags, fb->offsets, fb->pitches);
	if (!r)
		mfb->possible_planes |= MGA_PLANE_CRTC1;

	r = mga_crtc2_check_framebuffer(mdev, fb->pixel_format, fb->flags, fb->offsets, fb->pitches);
	if (!r)
		mfb->possible_planes |= MGA_PLANE_CRTC2;

	r = mga_cursor_check_framebuffer(mdev, fb->pixel_format, fb->flags, fb->offsets, fb->pitches);
	if (!r)
		mfb->possible_planes |= MGA_PLANE_CURSOR;

	r = mga_bes_check_framebuffer(mdev, fb->pixel_format, fb->flags, fb->offsets, fb->pitches);
	if (!r)
		mfb->possible_planes |= MGA_PLANE_BES;

	r = mga_spic_check_framebuffer(mdev, fb->pixel_format, fb->flags, fb->offsets, fb->pitches);
	if (!r)
		mfb->possible_planes |= MGA_PLANE_SPIC;

	if (!mfb->possible_planes)
		return -EINVAL;

	return 0;
}

size_t mga_framebuffer_calc_size(const struct drm_framebuffer *fb)
{
	unsigned int max = 0;
	int i;

	for (i = 0; i < drm_format_num_planes(fb->pixel_format); i++) {
		unsigned int height = drm_format_plane_height(fb->pixel_format, fb->height, i);
		unsigned int size = fb->offsets[i] + height * fb->pitches[i];

		if (size > max)
			max = size;
	}

	return max;
}

struct drm_framebuffer *mga_framebuffer_create(struct drm_device *dev,
					       unsigned int width,
					       unsigned int height,
					       unsigned int pixel_format,
					       unsigned int flags,
					       unsigned int offsets[4],
					       unsigned int pitches[4])
{
	struct mga_dev *mdev = to_mga_device(dev);
	struct mga_framebuffer *mfb;
	int r;

	mfb = kzalloc(sizeof *mfb, GFP_KERNEL);
	if (!mfb)
		return ERR_PTR(-ENOMEM);

	drm_framebuffer_init(&mfb->base, width, height, pixel_format, flags, offsets, pitches);

	r = mga_framebuffer_check(mdev, &mfb->base);
	if (r) {
		kfree(mfb);
		return ERR_PTR(r);
	}

	mfb->size = mga_framebuffer_calc_size(&mfb->base);

	switch (pixel_format) {
	case DRM_FORMAT_C2:
	case DRM_FORMAT_C6:
		mfb->cursor = true;
		mfb->virt = kzalloc(mfb->size, GFP_KERNEL);
		if (!mfb->virt) {
			kfree(mfb);
			return ERR_PTR(-ENOMEM);
		}
		break;
	default:
		r = mga_mem_alloc(mdev->fb_alloc, &mfb->bus, &mfb->virt, mfb->size, false);
		if (r) {
			kfree(mfb);
			return ERR_PTR(r);
		}
		break;
	}

	mutex_lock(&dev->mode_config.mutex);

	r = drm_mode_object_init(dev, &mfb->base.base, DRM_MODE_OBJECT_FB);
	if (r) {
		mutex_unlock(&dev->mode_config.mutex);
		if (mfb->cursor)
			kfree(mfb->virt);
		else
			mga_mem_free(mdev->fb_alloc, mfb->bus, mfb->size);
		kfree(mfb);
		return ERR_PTR(r);
	}

	list_add_tail(&mfb->base.head, &dev->mode_config.fb_list);
	dev->mode_config.num_fb++;

	mutex_unlock(&dev->mode_config.mutex);

	return &mfb->base;
}

void mga_framebuffer_destroy(struct drm_device *dev,
			     struct drm_framebuffer *fb)
{
	struct mga_dev *mdev = to_mga_device(dev);
	struct mga_framebuffer *mfb;

	if (!fb)
		return;

	mfb = to_mga_framebuffer(fb);

	mutex_lock(&dev->mode_config.mutex);
	dev->mode_config.num_fb--;
	list_del(&fb->head);
	drm_mode_object_fini(&fb->base);
	mutex_unlock(&dev->mode_config.mutex);

	if (mfb->cursor)
		kfree(mfb->virt);
	else
		mga_mem_free(mdev->fb_alloc, mfb->bus, mfb->size);
	kfree(mfb);
}

static u32 mga_framebuffer_calc_offset(const struct mga_framebuffer *mfb,
				       unsigned int x, unsigned int y,
				       unsigned int plane, bool field)
{
	return mfb->base.offsets[plane] +
		field * mfb->base.pitches[plane] +
		y * mfb->base.pitches[plane] +
		x * drm_format_plane_cpp(mfb->base.pixel_format, plane);
}

u32 mga_framebuffer_calc_bus(const struct mga_framebuffer *mfb,
			     unsigned int x, unsigned int y,
			     unsigned int plane, bool field)
{
	return mfb->bus + mga_framebuffer_calc_offset(mfb, x, y, plane, field);
}

void __iomem *mga_framebuffer_calc_virt(const struct mga_framebuffer *mfb,
					unsigned int x, unsigned int y,
					unsigned int plane, bool field)
{
	return mfb->virt + mga_framebuffer_calc_offset(mfb, x, y, plane, field);
}
