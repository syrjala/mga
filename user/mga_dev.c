/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <limits.h>

#include "mga_regs.h"
#include "mga_dump.h"

#include "mga_irq_thread.h"

int mga_open_device(const char *dev, struct mga_dev *mdev)
{
	struct pci_dev *pdev;
	struct mga_chip chip;
	int ret;
	u32 irq;

	memset(mdev, 0, sizeof *mdev);

	pdev = &mdev->pdev;

	mdev->fd = open(dev, O_RDWR | O_SYNC);
	if (mdev->fd < 0) {
		pr_err("0\n");
		goto out;
	}

	ret = ioctl(mdev->fd, MGA_IOC_CHIP, &chip);
	if (ret) {
		pr_err("1\n");
		goto close;
	}

	mdev->chip = chip.chip;
	mga_prepare(mdev);

	pdev->fd = -1;
	pdev->bus = &pdev->pci_bus;
	pdev->bus->domain = chip.pci_domain_nr;
	pdev->bus->number = chip.pci_bus_nr;
	pdev->devfn = chip.pci_devfn;
	pdev->vendor = chip.pci_vendor_id;
	pdev->device = chip.pci_device_id;
	pdev->sub_vendor = chip.pci_sub_vendor_id;
	pdev->sub_device = chip.pci_sub_device_id;
	pdev->class = chip.pci_class >> 8;
	pdev->revision = chip.pci_class & 0xff;

	sprintf(pdev->dev.drv_name, "mga");
	sprintf(pdev->dev.dev_name, "%04x:%02x:%02x.%d",
		pci_domain_nr(pdev->bus), pdev->bus->number,
		PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn));

	mdev->dev = &pdev->dev;

	ret = ioctl(mdev->fd, MGA_IOC_MMIO_RESOURCE, &mdev->mmio_res);
	if (ret) {
		ret = -errno;
		pr_err("2\n");
		goto close;
	}

	ret = ioctl(mdev->fd, MGA_IOC_MEM_RESOURCE, &mdev->mem_res);
	if (ret) {
		ret = -errno;
		pr_err("3\n");
		goto close;
	}

	ret = ioctl(mdev->fd, MGA_IOC_ILOAD_RESOURCE, &mdev->iload_res);
	if (ret) {
		if (errno != ENXIO) {
			ret = -errno;
			pr_err("4\n");
			goto close;
		}
		/* ignore missing ILOAD */
		mdev->iload_res.index = 0;
	}

	ret = ioctl(mdev->fd, MGA_IOC_IRQ_RESOURCE, &irq);
	if (ret) {
		ret = -errno;
		pr_err("irq\n");
		goto close;
	}
	mdev->irq.irq = irq;

	ret = ioctl(mdev->fd, MGA_IOC_DMA_RESOURCE, &mdev->dma_res);
	if (ret) {
		if (errno != ENXIO) {
			ret = -errno;
			pr_err("5\n");
			goto close;
		}
		mdev->dma_res.index = 0;
	}

	mdev->mmio_virt = mmap(NULL, mdev->mmio_res.len, PROT_READ | PROT_WRITE, MAP_SHARED, mdev->fd, MGA_OFFSET_MMIO);
	if (mdev->mmio_virt == MAP_FAILED) {
		ret = -errno;
		pr_err("6\n");
		goto close;
	}

	mdev->mem_virt = mmap(NULL, mdev->mem_res.len, PROT_READ | PROT_WRITE, MAP_SHARED, mdev->fd, MGA_OFFSET_MEM);
	if (mdev->mem_virt == MAP_FAILED) {
		ret = -errno;
		pr_err("7\n");
		goto unmap_mmio;
	}

	if (mdev->iload_res.index) {
		mdev->iload_virt = mmap(NULL, mdev->iload_res.len, PROT_READ | PROT_WRITE, MAP_SHARED, mdev->fd, MGA_OFFSET_ILOAD);
		if (mdev->iload_virt == MAP_FAILED) {
			ret = -errno;
			pr_err("8\n");
			goto unmap_mem;
		}
	}

	if (mdev->dma_res.index) {
		mdev->dma_virt = mmap(NULL, mdev->dma_res.len, PROT_READ | PROT_WRITE, MAP_SHARED, mdev->fd, MGA_OFFSET_DMA);
		if (mdev->dma_virt == MAP_FAILED) {
			ret = -errno;
			pr_err("9\n");
			goto unmap_iload;
		}
	}

	ret = pci_cfg_open(&mdev->pdev, mdev->fd);
	if (ret)
	{
		pr_err("10\n");
		goto unmap_dma;
	}

	read_rom(mdev);

	ret = mga_parse_pins(mdev->chip, mdev->rom_data,
			     mdev->rom_size, &mdev->pins);
	if (ret)
	{
		pr_err("11\n");
		goto close_pci;
	}

	/*
	 * Interleave must be initially disabled since it will not
	 * work properly with a 1 bank board (2MB). It will in fact
	 * cause the memory size probe to erroneosly detect 4MB.
	 */
	mdev->interleave = false;

	/* some defaults */
	mdev->pixel_format = DRM_FORMAT_C8;

	dev_dbg(mdev->dev, "mapped MMIO @ 0x%08x (size=%u)\n",
		mdev->mmio_virt, mdev->mmio_res.len);
	dev_dbg(mdev->dev, "mapped MEM @ 0x%08x (size=%u)\n",
		mdev->mem_virt, mdev->mem_res.len);
	dev_dbg(mdev->dev, "mapped ILOAD @ 0x%08x (size=%u)\n",
		mdev->iload_virt, mdev->iload_res.len);
	dev_dbg(mdev->dev, "mapped DMA @ 0x%08x (size=%u)\n",
		mdev->dma_virt, mdev->dma_res.len);

#ifdef USE_IRQ_THREAD
	mga_irq_thread_init(mdev);
#endif

	mga_irq_init(mdev);

	return 0;

 close_pci:
	pci_cfg_close(&mdev->pdev);
 unmap_dma:
	if (mdev->dma_res.index)
		munmap(mdev->dma_virt, mdev->dma_res.len);
 unmap_iload:
	if (mdev->iload_res.index)
		munmap(mdev->iload_virt, mdev->iload_res.len);
 unmap_mem:
	munmap(mdev->mem_virt, mdev->mem_res.len);
 unmap_mmio:
	munmap(mdev->mmio_virt, mdev->mmio_res.len);
 close:
	close(mdev->fd);
 out:
	memset(mdev, 0, sizeof *mdev);
	mdev->fd = -1;
	return -1;
}

void mga_close_device(struct mga_dev *mdev)
{
	mga_irq_fini(mdev);

#ifdef USE_IRQ_THREAD
	mga_irq_thread_fini(mdev);
#endif

	pci_cfg_close(&mdev->pdev);
	if (mdev->dma_res.index)
		munmap(mdev->dma_virt, mdev->dma_res.len);
	if (mdev->iload_res.index)
		munmap(mdev->iload_virt, mdev->iload_res.len);
	munmap(mdev->mem_virt, mdev->mem_res.len);
	munmap(mdev->mmio_virt, mdev->mmio_res.len);
	close(mdev->fd);
}

/*
 * Probes the memory in 2 MB strides.
 */
int mga_probe_mem_size(struct mga_dev *mdev)
{
	u8 tmp[16];
	int i;
	unsigned int memsize1, memsize2;
	void __iomem *mem = mdev->mem_virt;
	const int max = mdev->mem_res.len >> 21;
	u8 crtcext3;

	mga_write8(mdev, MGA_CRTCEXTX, 0x03);
	crtcext3 = mga_read8(mdev, MGA_CRTCEXTD);
	mga_write8(mdev, MGA_CRTCEXTD, crtcext3 | MGA_CRTCEXT3_MGAMODE);
	dev_dbg(mdev->dev, "Chip was in %s mode\n", (crtcext3 & MGA_CRTCEXT3_MGAMODE) ? "MGA" : "VGA");

	/* Save */
	for (i = 0; i < max; i++) {
		tmp[i] = readb(mem + (i << 21));
		dev_dbg(mdev->dev, "Save @ %d MB = %02x\n", i << 1, tmp[i]);
	}

	/* Check once */
	for (i = max - 1; i >= 0; i--) {
		u8 val = i + 1;
		writeb(val, mem + (i << 21));
		dev_dbg(mdev->dev, "Write @ %d MB = %02x\n", i << 1, val);
	}
	mga_write8(mdev, MGA_CACHEFLUSH, 0x00);
	for (i = 0; i < max; i++) {
		u8 expected = i + 1;
		u8 val = readb(mem + (i << 21));
		dev_dbg(mdev->dev, "Read @ %d MB = %02x\n", i << 1, val);
		if (val != expected)
			break;
	}
	memsize1 = i << 21;
	dev_dbg(mdev->dev, "First mem size probe result: %u MB\n", memsize1 >> 20);

	/* Check twice */
	for (i = max - 1; i >= 0; i--) {
		u8 val = (i + 1) ^ 0xff;
		writeb(val, mem + (i << 21));
		dev_dbg(mdev->dev, "Write @ %d MB = %02x\n", i << 1, val);
	}
	mga_write8(mdev, MGA_CACHEFLUSH, 0x00);
	for (i = 0; i < max; i++) {
		u8 expected = (i + 1) ^ 0xff;
		u8 val = readb(mem + (i << 21));
		dev_dbg(mdev->dev, "Read @ %d MB = %02x\n", i << 1, val);
		if (val != expected)
			break;
	}
	memsize2 = i << 21;
	dev_dbg(mdev->dev, "Second memory size probe result: %u MB\n", memsize2 >> 20);

	/* Restore */
	for (i = max - 1; i >= 0; i--) {
		writeb(tmp[i], mem + (i << 21));
		dev_dbg(mdev->dev, "Restore @ %d MB = %02x\n", i << 1, tmp[i]);
	}

	/* FIXME return to vgamode? */
	mga_write8(mdev, MGA_CRTCEXTX, 3);
	mga_write8(mdev, MGA_CRTCEXTD, crtcext3);
	dev_dbg(mdev->dev, "Chip restored to %s mode\n", (crtcext3 & MGA_CRTCEXT3_MGAMODE) ? "MGA" : "VGA");

	memsize1 = min(memsize1, memsize2);
	dev_dbg(mdev->dev, "Final memory size probe result: %u MB\n", memsize1 >> 20);

	if (!memsize1)
		return -ENOMEM;

	mdev->mem_size = memsize1;
	return 0;
}

const char *pretty_size(unsigned int size)
{
	static char tmp[64];
	if (size >= (1 << 20))
		snprintf(tmp, sizeof tmp, "%u MB", size >> 20);
	else if (size >= (1 << 10))
		snprintf(tmp, sizeof tmp, "%u kB", size >> 10);
	else
		snprintf(tmp, sizeof tmp, "%u B", size);
	return tmp;
}

const char *mga_chip_name(unsigned int chip)
{
	static const char *chip_names[] = {
		[MGA_CHIP_2064W]  = "MGA-2064W",
		[MGA_CHIP_2164W]  = "MGA-2164W",
		[MGA_CHIP_1064SG] = "MGA-1064SG",
		[MGA_CHIP_1164SG] = "MGA-1164SG",
		[MGA_CHIP_G100]   = "MGA-G100",
		[MGA_CHIP_G200]   = "MGA-G200",
		[MGA_CHIP_G200SE] = "MGA-G200SE",
		[MGA_CHIP_G200EV] = "MGA-G200EV",
		[MGA_CHIP_G200WB] = "MGA-G200WB",
		[MGA_CHIP_G400]   = "MGA-G400",
		[MGA_CHIP_G450]   = "MGA-G450",
		[MGA_CHIP_G550]   = "MGA-G550",
	};
	BUG_ON(chip >= ARRAY_SIZE(chip_names));

	return chip_names[chip];
}

void mga_print_chip_info(struct mga_dev *mdev)
{
	struct pci_dev *pdev = &mdev->pdev;

	dev_dbg(mdev->dev, "chip       = %s\n", mga_chip_name(mdev->chip));
	dev_dbg(mdev->dev, "bus        = %04x:%02x:%02x.%d\n",
		pci_domain_nr(pdev->bus), pdev->bus->number,
		PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn));
	dev_dbg(mdev->dev, "vendor     = %04x\n", pdev->vendor);
	dev_dbg(mdev->dev, "device     = %04x\n", pdev->device);
	if (pdev->sub_vendor)
		dev_dbg(mdev->dev, "sub vendor = %04x\n", pdev->sub_vendor);
	if (pdev->sub_device)
		dev_dbg(mdev->dev, "sub device = %04x\n", pdev->sub_device);
	dev_dbg(mdev->dev, "mmio  = %08llx (%s)\n",
		(unsigned long long) mdev->mmio_res.off,
		pretty_size(mdev->mmio_res.len));
	dev_dbg(mdev->dev, "mem   = %08llx (%s)\n",
		(unsigned long long) mdev->mem_res.off,
		pretty_size(mdev->mem_res.len));
	if (mdev->iload_res.index)
		dev_dbg(mdev->dev, "iload = %08llx (%s)\n",
			(unsigned long long) mdev->iload_res.off,
			pretty_size(mdev->iload_res.len));
}

void mga_print_features(struct mga_dev *mdev)
{
	static const char *feat_names[] = {
		[MGA_FEAT_TVO_BUILTIN]      = "TVO builtin",
		[MGA_FEAT_TVO_ADDON]        = "TVO addon",
		[MGA_FEAT_TMDS_BUILTIN]     = "TMDS builtin",
		[MGA_FEAT_TMDS_ADDON]       = "TMDS addon",
		[MGA_FEAT_DUALHEAD_BUILTIN] = "DUALHEAD builtin",
		[MGA_FEAT_DUALHEAD_ADDON]   = "DUALHEAD addon",
		[MGA_FEAT_DVD_BUILTIN]      = "DVD builtin",
		[MGA_FEAT_DVD_ADDON]        = "DVD addon",
		[MGA_FEAT_MJPEG_BUILTIN]    = "MJPEG builtin",
		[MGA_FEAT_MJPEG_ADDON]      = "MJPEG addon",
		[MGA_FEAT_VIN_BUILTIN]      = "VIN builtin",
		[MGA_FEAT_VIN_ADDON]        = "VIN addon",
		[MGA_FEAT_TUNER_BUILTIN]    = "TUNER builtin",
		[MGA_FEAT_TUNER_ADDON]      = "TUNER addon",
		[MGA_FEAT_AUDIO_BUILTIN]    = "AUDIO builtin",
		[MGA_FEAT_AUDIO_ADDON]      = "AUDIO addon",
	};
	static const char *output_names[] = {
		[MGA_OUTPUT_DAC1]  = "DAC1",
		[MGA_OUTPUT_DAC2]  = "DAC2",
		[MGA_OUTPUT_TMDS1] = "TMDS1",
		[MGA_OUTPUT_TMDS2] = "TMDS2",
		[MGA_OUTPUT_TVOUT] = "TVOUT",
	};
	unsigned int i;

	dev_dbg(mdev->dev, "Features:");
	for (i = 1; i < ARRAY_SIZE(feat_names); i <<= 1)
		if (mdev->features & i)
			printk(KERN_CONT " %s", feat_names[i]);
	printk(KERN_CONT "\n");

	dev_dbg(mdev->dev, "Outputs:");
	for (i = 1; i < ARRAY_SIZE(output_names); i <<= 1)
		if (mdev->outputs & i)
			printk(KERN_CONT " %s", output_names[i]);
	printk(KERN_CONT "\n");
}

void mga_write8(struct mga_dev *mdev, unsigned int reg, u8 val)
{
	writeb(val, mdev->mmio_virt + reg);
}

void mga_write16(struct mga_dev *mdev, unsigned int reg, u16 val)
{
	writew(val, mdev->mmio_virt + reg);
}

void mga_write32(struct mga_dev *mdev, unsigned int reg, u32 val)
{
#ifdef USE_IRQ_THREAD
	if (reg == MGA_IEN) {
		mga_write32_ien(mdev, reg, val);
		return;
	} else if (reg == MGA_ICLEAR) {
		mga_write32_iclear(mdev, reg, val);
		return;
	}
#endif
	dev_dbg(mdev->dev, "%s: 0x%x = 0x%08x\n", __func__, reg, val);
	writel(val, mdev->mmio_virt + reg);
}

u8 mga_read8(struct mga_dev *mdev, unsigned int reg)
{
	return readb(mdev->mmio_virt + reg);
}

u16 mga_read16(struct mga_dev *mdev, unsigned int reg)
{
	return readw(mdev->mmio_virt + reg);
}

u32 mga_read32(struct mga_dev *mdev, unsigned int reg)
{
#ifdef USE_IRQ_THREAD
	if (reg == MGA_IEN)
		return mga_read32_ien(mdev, reg);
#endif
	return readl(mdev->mmio_virt + reg);
}

void wbinvd(struct mga_dev *mdev)
{
	if (ioctl(mdev->fd, MGA_IOC_CACHE_FLUSH))
		dev_err(mdev->dev, "wvinvd() failed\n");
}
