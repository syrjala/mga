/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_PINS_H
#define MGA_PINS_H

#include "kernel_emul.h"

#if 0
enum mga_clock_preset {
	MGA_CLK_VGA,
	MGA_CLK_2D,
	MGA_CLK_3D,

	NUM_MGA_CLK,

	/* G450/G550 */
	MGA_CLK_SH = MGA_CLK_2D,
	MGA_CLK_DH = MGA_CLK_3D,
};

struct {
	/* 0 MiB, 2 MiB, 4 MiB, ... 16 MiB */
	unsigned int gclk[9];

	/* G450/G550 */
	unsigned int syspll;
	unsigned int vidpll;

	/* Not all of these are used for all chips. */
	u32 option;
	u32 option2;
	u32 option3;
	u32 memmisc;
	u32 mctlwtst;
	u32 mctlwtst_core;
	u32 memrdbk;
	u8 crtcext6;
} clk[NUM_MGA_CLK];

int mga_select_gclk(struct mga_dev *mdev,
		    enum mga_clock_preset preset)
{
	unsigned int idx = UINT_MAX;

	if (mdev->mem_size >= mdev->p.mem.base_size)
		idx = mdev->mem_size - mdev->p.mem.base_size >> 21;

	idx = min(idx, ARRAY_SIZE(mdev->p.clk[preset].gclk) - 1);

	return mdev->p.clk[preset].gclk[idx];
}

void mga_duplicate_gclks(struct mga_dev *mdev,
			 enum mga_clock_preset preset)
{
	unsigned int gclk = mdev->p.clk[preset].gclk[0];

	for (i = 0; i < ARRAY_SIZE(mdev->p.clk[preset].gclk); i++) {
		if (mdev->p.clk[preset].gclk[i]) {
			gclk = mdev->p.clk[preset].gclk[i];
			continue;
		}

		mdev->p.clk[preset].gclk[i] gclk;
	}
}
#endif

struct mga_pins1 {
	unsigned int fref;
	unsigned int fvco_max;

	struct {
		unsigned int pixclk_max;
	} crtc1;

	unsigned int ldclk_max;

	struct {
		unsigned int gclk[1];
	} clk_vga;

	struct {
		unsigned int gclk[(8 >> 2) + 1];
		unsigned int gclk_mod;
	} clk_2d;

	u32 option;
};

struct mga_pins2 {
	unsigned int fref;
	unsigned int fvco_max;

	struct {
		unsigned int pixclk_max;
	} crtc1;

	unsigned int ldclk_max; /* 2164W only */

	struct {
		unsigned int gclk[(16 >> 2) + 1];
		unsigned int gclk_mod;
	} clk_2d;

	u32 mctlwtst; /* 1064SG/1164SG only */
	u32 option;
	u8 vrefctrl;
};

enum {
	_MGA_FEAT_TVO_BUILTIN   = 0x01,
	_MGA_FEAT_DVD_BUILTIN   = 0x02,
	_MGA_FEAT_MJPEG_BUILTIN = 0x04,
	_MGA_FEAT_VIN_BUILTIN   = 0x08,
	_MGA_FEAT_TUNER_BUILTIN = 0x10,
	_MGA_FEAT_AUDIO_BUILTIN = 0x20,
	_MGA_FEAT_TMDS_BUILTIN  = 0x40,
};

enum {
	_MGA_OUTPUT_NONE  = 0x00,
	_MGA_OUTPUT_DAC1  = 0x01,
	_MGA_OUTPUT_DAC2  = 0x02,
	_MGA_OUTPUT_TMDS1 = 0x04,
	_MGA_OUTPUT_TMDS2 = 0x08,
	_MGA_OUTPUT_TVOUT = 0x10,
};

struct mga_pins3 {
	unsigned int features;

	struct {
		unsigned int base_size;
		bool sdram;
	} mem;

	unsigned int fref;
	unsigned int fvco_max;

	struct {
		unsigned int pixclk_8_max;
		unsigned int pixclk_16_max;
		unsigned int pixclk_24_max;
		unsigned int pixclk_32_max;
	} crtc1;

	struct {
		unsigned int gclk[1];
		u32 option;
		u32 option2;
		u8 crtcext6;
	} clk_vga;

	struct {
		unsigned int gclk[(8 >> 1) + 1];
		u32 option;
		u32 option2;
		u8 crtcext6;
	} clk_2d;

	struct {
		unsigned int gclk[(8 >> 1) + 1];
		u32 option;
		u32 option2;
	} clk_3d;

	u32 mctlwtst;
	u32 memrdbk;
	u8 vrefctrl;
};

struct mga_pins4 {
	unsigned int features;

	struct {
		unsigned int base_size;
		bool sdram;
	} mem;

	unsigned int fref;

	struct {
		unsigned int fvco_max;
	} syspll;

	struct {
		unsigned int fvco_max;
	} pixpll;

	struct {
		unsigned int pixclk_8_max;
		unsigned int pixclk_16_max;
		unsigned int pixclk_24_max;
		unsigned int pixclk_32_max;
	} crtc1;

	struct {
		unsigned int pixclk_16_max;
		unsigned int pixclk_32_max;
	} crtc2;

	u32 option;
	u32 memrdbk;
	u32 memrdbk_mod;
	u8 vrefctrl;

	struct {
		unsigned int gclk;
		u32 option3;
		u32 mctlwtst;
	} clk_vga;

	struct {
		unsigned int gclk;
		u32 option3;
		u32 mctlwtst;
	} clk_2d;

	struct {
		unsigned int gclk;
		u32 option3;
		u32 mctlwtst;
	} clk_3d;
};

struct mga_pins5 {
	unsigned int outputs;
	unsigned int features;

	struct {
		unsigned int base_size;
		bool sdram;
		bool emrswen;
		bool dll;
		bool ddr;
	} mem;

	unsigned int fref;

	struct {
		unsigned int fvco_min;
		unsigned int fvco_max;
	} syspll;

	struct {
		unsigned int fvco_min;
		unsigned int fvco_max;
	} pixpll;

	struct {
		unsigned int fvco_min;
		unsigned int fvco_max;
	} vidpll;

	struct {
		unsigned int pixclk_8_max;
		unsigned int pixclk_16_max;
		unsigned int pixclk_24_max;
		unsigned int pixclk_32_max;
		unsigned int pixclk_32_max_dh;
	} crtc1;

	struct {
		unsigned int pixclk_16_max;
		unsigned int pixclk_32_max;
		unsigned int pixclk_32_max_dh;
	} crtc2;

	struct {
		unsigned int pixclk_max;
	} dac1;

	struct {
		unsigned int pixclk_max;
	} dac2;

	struct {
		unsigned int pixclk_max;
	} tmds;

	u32 maccess;
	u32 option;
	u32 option2;
	u8 vrefctrl;

	struct {
		unsigned int syspll;
		unsigned int vidpll;
		u32 option3;
		u32 mctlwtst;
		u32 mctlwtst_core;
		u32 memmisc;
		u32 memrdbk;
	} clk_vga;

	struct {
		unsigned int syspll;
		unsigned int vidpll;
		u32 option3;
		u32 mctlwtst;
		u32 mctlwtst_core;
		u32 memmisc;
		u32 memrdbk;
	} clk_sh;

	struct {
		unsigned int syspll;
		unsigned int vidpll;
		u32 option3;
		u32 mctlwtst;
		u32 mctlwtst_core;
		u32 memmisc;
		u32 memrdbk;
	} clk_dh;
};

union mga_pins {
	struct mga_pins1 pins1;
	struct mga_pins2 pins2;
	struct mga_pins3 pins3;
	struct mga_pins4 pins4;
	struct mga_pins5 pins5;
};

int mga_parse_pins(unsigned int chip, const u8 *rom_data,
		   size_t rom_size, union mga_pins *pins);

#endif
