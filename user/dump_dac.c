/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include "mga_dump.h"
#include "mga_dac_regs.h"

static const char *direct_register_names[] = {
	//[MGA_PALWTADD ] = "PALWTADD",
	//[MGA_PALDATA  ] = "PALDATA",
	  [MGA_PIXRDMSK ] = "PIXRDMSK",
	//[MGA_PALRDADD ] = "PALRDADD",
	//[MGA_X_DATAREG] = "X_DATAREG",
	  [MGA_CURPOSXL ] = "CURPOSXL",
	  [MGA_CURPOSXH ] = "CURPOSXH",
	  [MGA_CURPOSYL ] = "CURPOSYL",
	  [MGA_CURPOSYH ] = "CURPOSYH",
};

static const char *indirect_register_names_old[] = {
	[MGA_XCURADDL       ] = "XCURADDL",
	[MGA_XCURADDH       ] = "XCURADDH",
	[MGA_XCURCTRL       ] = "XCURCTRL",
	[MGA_XCURCOL0RED    ] = "XCURCOL0RED",
	[MGA_XCURCOL0GREEN  ] = "XCURCOL0GREEN",
	[MGA_XCURCOL0BLUE   ] = "XCURCOL0BLUE",
	[MGA_XCURCOL1RED    ] = "XCURCOL1RED",
	[MGA_XCURCOL1GREEN  ] = "XCURCOL1GREEN",
	[MGA_XCURCOL1BLUE   ] = "XCURCOL1BLUE",
	[MGA_XCURCOL2RED    ] = "XCURCOL2RED",
	[MGA_XCURCOL2GREEN  ] = "XCURCOL2GREEN",
	[MGA_XCURCOL2BLUE   ] = "XCURCOL2BLUE",
	[MGA_XVREFCTRL      ] = "XVREFCTRL",
	[MGA_XMULCTRL       ] = "XMULCTRL",
	[MGA_XPIXCLKCTRL    ] = "XPIXCLKCTRL",
	[MGA_XGENCTRL       ] = "XGENCTRL",
	[MGA_XMISCCTRL      ] = "XMISCCTRL",
	[MGA_XGENIOCTRL     ] = "XGENIOCTRL",
	[MGA_XGENIODATA     ] = "XGENIODATA",
	[MGA_XSYSPLLM       ] = "XSYSPLLM",
	[MGA_XSYSPLLN       ] = "XSYSPLLN",
	[MGA_XSYSPLLP       ] = "XSYSPLLP",
	[MGA_XSYSPLLSTAT    ] = "XSYSPLLSTAT",
	[MGA_XZOOMCTRL      ] = "XZOOMCTRL",
	[MGA_XSENSETEST     ] = "XSENSETEST",
	[MGA_XCRCREML       ] = "XCRCREML",
	[MGA_XCRCREMH       ] = "XCRCREMH",
	[MGA_XCRCBITSEL     ] = "XCRCBITSEL",
	[MGA_XCOLKEYMSKL    ] = "XCOLKEYMSKL",  /* <= G100 */
	[MGA_XCOLKEYMSKH    ] = "XCOLKEYMSKH",  /* <= G100 */
	[MGA_XCOLKEYL       ] = "XCOLKEYL",  /* <= G100 */
	[MGA_XCOLKEYH       ] = "XCOLKEYH",  /* <= G100 */
	[MGA_XPIXPLLAM      ] = "XPIXPLLAM",
	[MGA_XPIXPLLAN      ] = "XPIXPLLAN",
	[MGA_XPIXPLLAP      ] = "XPIXPLLAP",
	[MGA_XPIXPLLBM      ] = "XPIXPLLBM",
	[MGA_XPIXPLLBN      ] = "XPIXPLLBN",
	[MGA_XPIXPLLBP      ] = "XPIXPLLBP",
	[MGA_XPIXPLLCM      ] = "XPIXPLLCM",
	[MGA_XPIXPLLCN      ] = "XPIXPLLCN",
	[MGA_XPIXPLLCP      ] = "XPIXPLLCP",
	[MGA_XPIXPLLSTAT    ] = "XPIXPLLSTAT",
};

static const char *indirect_register_names_new[] = {
	[MGA_XCURADDL       ] = "XCURADDL",
	[MGA_XCURADDH       ] = "XCURADDH",
	[MGA_XCURCTRL       ] = "XCURCTRL",
	[MGA_XCURCOL0RED    ] = "XCURCOL0RED",
	[MGA_XCURCOL0GREEN  ] = "XCURCOL0GREEN",
	[MGA_XCURCOL0BLUE   ] = "XCURCOL0BLUE",
	[MGA_XCURCOL1RED    ] = "XCURCOL1RED",
	[MGA_XCURCOL1GREEN  ] = "XCURCOL1GREEN",
	[MGA_XCURCOL1BLUE   ] = "XCURCOL1BLUE",
	[MGA_XCURCOL2RED    ] = "XCURCOL2RED",
	[MGA_XCURCOL2GREEN  ] = "XCURCOL2GREEN",
	[MGA_XCURCOL2BLUE   ] = "XCURCOL2BLUE",
	[MGA_XVREFCTRL      ] = "XVREFCTRL",
	[MGA_XMULCTRL       ] = "XMULCTRL",
	[MGA_XPIXCLKCTRL    ] = "XPIXCLKCTRL",
	[MGA_XGENCTRL       ] = "XGENCTRL",
	[MGA_XMISCCTRL      ] = "XMISCCTRL",
	[MGA_XGENIOCTRL     ] = "XGENIOCTRL",
	[MGA_XGENIODATA     ] = "XGENIODATA",
	[MGA_XSYSPLLM       ] = "XSYSPLLM",
	[MGA_XSYSPLLN       ] = "XSYSPLLN",
	[MGA_XSYSPLLP       ] = "XSYSPLLP",
	[MGA_XSYSPLLSTAT    ] = "XSYSPLLSTAT",
	[MGA_XZOOMCTRL      ] = "XZOOMCTRL",
	[MGA_XSENSETEST     ] = "XSENSETEST",
	[MGA_XCRCREML       ] = "XCRCREML",
	[MGA_XCRCREMH       ] = "XCRCREMH",
	[MGA_XCRCBITSEL     ] = "XCRCBITSEL",
	[MGA_XCOLMSK        ] = "XCOLMSK", /* >= G200 */
	[MGA_XCOLKEY        ] = "XCOLKEY", /* >= G200 */
	[MGA_XPIXPLLAM      ] = "XPIXPLLAM",
	[MGA_XPIXPLLAN      ] = "XPIXPLLAN",
	[MGA_XPIXPLLAP      ] = "XPIXPLLAP",
	[MGA_XPIXPLLBM      ] = "XPIXPLLBM",
	[MGA_XPIXPLLBN      ] = "XPIXPLLBN",
	[MGA_XPIXPLLBP      ] = "XPIXPLLBP",
	[MGA_XPIXPLLCM      ] = "XPIXPLLCM",
	[MGA_XPIXPLLCN      ] = "XPIXPLLCN",
	[MGA_XPIXPLLCP      ] = "XPIXPLLCP",
	[MGA_XPIXPLLSTAT    ] = "XPIXPLLSTAT",
	[MGA_XKEYOPMODE     ] = "XKEYOPMODE", /* >= G200 */
	[MGA_XCOLMSK0RED    ] = "XCOLMSK0RED", /* >= G200 */
	[MGA_XCOLMSK0GREEN  ] = "XCOLMSK0GREEN", /* >= G200 */
	[MGA_XCOLMSK0BLUE   ] = "XCOLMSK0BLUE", /* >= G200 */
	[MGA_XCOLKEY0RED    ] = "XCOLKEY0RED", /* >= G200 */
	[MGA_XCOLKEY0GREEN  ] = "XCOLKEY0GREEN", /* >= G200 */
	[MGA_XCOLKEY0BLUE   ] = "XCOLKEY0BLUE", /* >= G200 */
	[MGA_XCURCOL3RED    ] = "XCURCOL3RED", /* >= G200 */
	[MGA_XCURCOL3GREEN  ] = "XCURCOL3GREEN", /* >= G200 */
	[MGA_XCURCOL3BLUE   ] = "XCURCOL3BLUE", /* >= G200 */
	[MGA_XCURCOL4RED    ] = "XCURCOL4RED", /* >= G200 */
	[MGA_XCURCOL4GREEN  ] = "XCURCOL4GREEN", /* >= G200 */
	[MGA_XCURCOL4BLUE   ] = "XCURCOL4BLUE", /* >= G200 */
	[MGA_XCURCOL5RED    ] = "XCURCOL5RED", /* >= G200 */
	[MGA_XCURCOL5GREEN  ] = "XCURCOL5GREEN", /* >= G200 */
	[MGA_XCURCOL5BLUE   ] = "XCURCOL5BLUE", /* >= G200 */
	[MGA_XCURCOL6RED    ] = "XCURCOL6RED", /* >= G200 */
	[MGA_XCURCOL6GREEN  ] = "XCURCOL6GREEN", /* >= G200 */
	[MGA_XCURCOL6BLUE   ] = "XCURCOL6BLUE", /* >= G200 */
	[MGA_XCURCOL7RED    ] = "XCURCOL7RED", /* >= G200 */
	[MGA_XCURCOL7GREEN  ] = "XCURCOL7GREEN", /* >= G200 */
	[MGA_XCURCOL7BLUE   ] = "XCURCOL7BLUE", /* >= G200 */
	[MGA_XCURCOL8RED    ] = "XCURCOL8RED", /* >= G200 */
	[MGA_XCURCOL8GREEN  ] = "XCURCOL8GREEN", /* >= G200 */
	[MGA_XCURCOL8BLUE   ] = "XCURCOL8BLUE", /* >= G200 */
	[MGA_XCURCOL9RED    ] = "XCURCOL9RED", /* >= G200 */
	[MGA_XCURCOL9GREEN  ] = "XCURCOL9GREEN", /* >= G200 */
	[MGA_XCURCOL9BLUE   ] = "XCURCOL9BLUE", /* >= G200 */
	[MGA_XCURCOL10RED   ] = "XCURCOL10RED", /* >= G200 */
	[MGA_XCURCOL10GREEN ] = "XCURCOL10GREEN", /* >= G200 */
	[MGA_XCURCOL10BLUE  ] = "XCURCOL10BLUE", /* >= G200 */
	[MGA_XCURCOL11RED   ] = "XCURCOL11RED", /* >= G200 */
	[MGA_XCURCOL11GREEN ] = "XCURCOL11GREEN", /* >= G200 */
	[MGA_XCURCOL11BLUE  ] = "XCURCOL11BLUE", /* >= G200 */
	[MGA_XCURCOL12RED   ] = "XCURCOL12RED", /* >= G200 */
	[MGA_XCURCOL12GREEN ] = "XCURCOL12GREEN", /* >= G200 */
	[MGA_XCURCOL12BLUE  ] = "XCURCOL12BLUE", /* >= G200 */
	[MGA_XCURCOL13RED   ] = "XCURCOL13RED", /* >= G200 */
	[MGA_XCURCOL13GREEN ] = "XCURCOL13GREEN", /* >= G200 */
	[MGA_XCURCOL13BLUE  ] = "XCURCOL13BLUE", /* >= G200 */
	[MGA_XCURCOL14RED   ] = "XCURCOL14RED", /* >= G200 */
	[MGA_XCURCOL14GREEN ] = "XCURCOL14GREEN", /* >= G200 */
	[MGA_XCURCOL14BLUE  ] = "XCURCOL14BLUE", /* >= G200 */
	[MGA_XCURCOL15RED   ] = "XCURCOL15RED", /* >= G200 */
	[MGA_XCURCOL15GREEN ] = "XCURCOL15GREEN", /* >= G200 */
	[MGA_XCURCOL15BLUE  ] = "XCURCOL15BLUE", /* >= G200 */
};

static const char *indirect_register_names_g400[] = {
	[MGA_XPANELMODE     ] = "XPANELMODE", /* >= G400 */
	[MGA_XMAFCDEL       ] = "XMAFCDEL", /* >= G400 */
};

static const char *indirect_register_names_g200ev[] = {
	[MGA_XPIXPLLCM_EV] = "XPIXPLLCM_EV", /* G200EV */
	[MGA_XPIXPLLCN_EV] = "XPIXPLLCN_EV", /* G200EV */
	[MGA_XPIXPLLCP_EV] = "XPIXPLLCP_EV", /* G200EV */
};

static const char *indirect_register_names_g200wb[] = {
	[MGA_XSPAREREG      ] = "XSPAREREG", /* G200WB */
	[MGA_XREMHEADCTRL   ] = "XREMHEADCTRL", /* G200WB */
	[MGA_XREMHEADCTRL2  ] = "XREMHEADCRTL2", /* G200WB */
	[MGA_XPIXPLLCM_WB   ] = "XPIXPLLCM_WB", /* G200WB */
	[MGA_XPIXPLLCN_WB   ] = "XPIXPLLCM_WB", /* G200WB */
	[MGA_XPIXPLLCP_WB   ] = "XPIXPLLCP_WB", /* G200WB */
};

static void dump_direct(struct mga_dev *mdev)
{
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(direct_register_names); i++) {
		u8 val;

		if (!direct_register_names[i])
			continue;

		val = mga_read8(mdev, 0x3C00 + i);

		dev_dbg(mdev->dev, "%s = %02x\n", direct_register_names[i], val);
	}
}

static void dump_indirect(struct mga_dev *mdev,
			  const char *indirect_register_names[],
			  int indirect_register_names_size)
{
	int i;

	for (i = 0; i < indirect_register_names_size; i++) {
		u8 val;

		if (!indirect_register_names[i])
			continue;

		mga_write8(mdev, MGA_X_INDEXREG, i);
		val = mga_read8(mdev, MGA_X_DATAREG);

		dev_dbg(mdev->dev, "%s = %02x\n", indirect_register_names[i], val);
	}
}

static void dump_indirect_old(struct mga_dev *mdev)
{
	dump_indirect(mdev, indirect_register_names_old,
		      ARRAY_SIZE(indirect_register_names_old));
}

static void dump_indirect_new(struct mga_dev *mdev)
{
	dump_indirect(mdev, indirect_register_names_new,
		      ARRAY_SIZE(indirect_register_names_new));
}

static void dump_indirect_g400(struct mga_dev *mdev)
{
	dump_indirect(mdev, indirect_register_names_g400,
		      ARRAY_SIZE(indirect_register_names_g400));
}

static void dump_indirect_g200ev(struct mga_dev *mdev)
{
	dump_indirect(mdev, indirect_register_names_g200ev,
		      ARRAY_SIZE(indirect_register_names_g200ev));
}

static void dump_indirect_g200wb(struct mga_dev *mdev)
{
	dump_indirect(mdev, indirect_register_names_g200wb,
		      ARRAY_SIZE(indirect_register_names_g200wb));
}

static unsigned int calc_fvco(unsigned int fref,
			      unsigned int m,
			      unsigned int n)
{
	return div_round(fref * (n + 1), m + 1);
}

static unsigned int calc_fo(unsigned int fref,
			    unsigned int m,
			    unsigned int n,
			    unsigned int p)
{
	return div_round(fref * (n + 1), (m + 1) * (p + 1));
}

static void dump_filter(struct mga_dev *mdev, unsigned int s)
{
	static const unsigned int s_limits[][5] = {
		[MGA_CHIP_1064SG] = {
			50000,
			100000,
			140000,
			180000,
			220000,
		},
		[MGA_CHIP_G100]   = {
			50000,
			100000,
			140000,
			180000,
			220000,
		},
		[MGA_CHIP_G200]   = {
			50000,
			100000,
			140000,
			180000,
			250000,
		},
		[MGA_CHIP_G400]   = {
			50000,
			110000,
			170000,
			240000,
			310000,
		},
	};

	dev_dbg(mdev->dev, " Loop filter bandwith: %u kHz <= Fvco < %u kHz\n",
		s_limits[mdev->chip][s],
		s_limits[mdev->chip][s+1]);
}

static void dump_syspll(struct mga_dev *mdev)
{
	unsigned int fref = mdev->chip < MGA_CHIP_G100 ? 14318 : 27000;
	unsigned int fo, fvco;
	u8 m, n, p;

	mga_write8(mdev, MGA_X_INDEXREG, MGA_XSYSPLLM);
	m = mga_read8(mdev, MGA_X_DATAREG);
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XSYSPLLN);
	n = mga_read8(mdev, MGA_X_DATAREG);
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XSYSPLLP);
	p = mga_read8(mdev, MGA_X_DATAREG);

	fvco = calc_fvco(fref, m, n);
	fo = calc_fo(fref, m, n, p & 0x7);

	dev_dbg(mdev->dev, "SYSPLL:\n");
	dev_dbg(mdev->dev, " Fref = %u kHz\n", fref);
	dev_dbg(mdev->dev, " Fvco = %u kHz\n", fvco);
	dev_dbg(mdev->dev, " Fo   = %u kHz\n", fo);
	dump_filter(mdev, p >> 3);
}

static void dump_pixplla(struct mga_dev *mdev)
{
	unsigned int fref = mdev->chip < MGA_CHIP_G100 ? 14318 : 27000;
	unsigned int fo, fvco;
	u8 m, n, p;

	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLAM);
	m = mga_read8(mdev, MGA_X_DATAREG);
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLAN);
	n = mga_read8(mdev, MGA_X_DATAREG);
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLAP);
	p = mga_read8(mdev, MGA_X_DATAREG);

	fvco = calc_fvco(fref, m, n);
	fo = calc_fo(fref, m, n, p & 0x7);

	dev_dbg(mdev->dev, "PIXPLLA:\n");
	dev_dbg(mdev->dev, " Fref = %u kHz\n", fref);
	dev_dbg(mdev->dev, " Fvco = %u kHz\n", fvco);
	dev_dbg(mdev->dev, " Fo   = %u kHz\n", fo);
	dump_filter(mdev, p >> 3);
}

static void dump_pixpllb(struct mga_dev *mdev)
{
	unsigned int fref = mdev->chip < MGA_CHIP_G100 ? 14318 : 27000;
	unsigned int fo, fvco;
	u8 m, n, p;

	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLBM);
	m = mga_read8(mdev, MGA_X_DATAREG);
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLBN);
	n = mga_read8(mdev, MGA_X_DATAREG);
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLBP);
	p = mga_read8(mdev, MGA_X_DATAREG);

	fvco = calc_fvco(fref, m, n);
	fo = calc_fo(fref, m, n, p & 0x7);

	dev_dbg(mdev->dev, "PIXPLLB:\n");
	dev_dbg(mdev->dev, " Fref = %u kHz\n", fref);
	dev_dbg(mdev->dev, " Fvco = %u kHz\n", fvco);
	dev_dbg(mdev->dev, " Fo   = %u kHz\n", fo);
	dump_filter(mdev, p >> 3);
}

static void dump_pixpllc(struct mga_dev *mdev)
{
	unsigned int fref = mdev->chip < MGA_CHIP_G100 ? 14318 : 27000;
	unsigned int fo, fvco;
	u8 m, n, p;

	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLCM);
	m = mga_read8(mdev, MGA_X_DATAREG);
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLCN);
	n = mga_read8(mdev, MGA_X_DATAREG);
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLCP);
	p = mga_read8(mdev, MGA_X_DATAREG);

	fvco = calc_fvco(fref, m, n);
	fo = calc_fo(fref, m, n, p & 0x7);

	dev_dbg(mdev->dev, "PIXPLLC:\n");
	dev_dbg(mdev->dev, " Fref = %u kHz\n", fref);
	dev_dbg(mdev->dev, " Fvco = %u kHz\n", fvco);
	dev_dbg(mdev->dev, " Fo   = %u kHz\n", fo);
	dump_filter(mdev, p >> 3);
}

void dump_dac(struct mga_dev *mdev)
{
	if (mdev->chip < MGA_CHIP_1064SG)
		return;
	if (mdev->chip > MGA_CHIP_G400)
		return;

	dev_dbg(mdev->dev, "Dump of DAC registers:\n");

	dump_direct(mdev);

	switch (mdev->chip) {
	case MGA_CHIP_1064SG:
	case MGA_CHIP_1164SG:
	case MGA_CHIP_G100:
		dump_indirect_old(mdev);
		break;
	default:
		dump_indirect_new(mdev);
		break;
	}

	switch (mdev->chip) {
	case MGA_CHIP_G200EV:
		dump_indirect_g200ev(mdev);
		break;
	case MGA_CHIP_G200WB:
		dump_indirect_g200wb(mdev);
		break;
	case MGA_CHIP_G400:
		dump_indirect_g400(mdev);
		break;
	};

	dump_syspll(mdev);
	dump_pixplla(mdev);
	dump_pixpllb(mdev);
	dump_pixpllc(mdev);
}
