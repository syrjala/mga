/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_TVO_H
#define MGA_TVO_H

#include "cve2.h"
#include "mga_tvo_common.h"

struct device;
struct i2c_adapter;
struct drm_display_mode;
struct mga_dev;
struct mga_dac_pll_settings;

enum {
	MGA_TVO_C = 0x14,
};

struct mga_tvo_dev {
	struct cve2_dev cdev;

	struct device *dev;

	struct i2c_adapter *adap;
	u8 addr;

	u8 version;

	bool enabled;
	struct mga_tvo_config config;
	u8 gamma_vals[9];
	u8 mode;

	u8 sync_pol_wanted;
	u8 sync_pol_actual;
};

struct mga_tvo_crtc_regs {
	u8 hscale;//90
	u16 vscale;//91

	u16 hsyncend;//9a
	u16 hblkend;//9c
	u16 hblkstr;//9e
	u16 htotal;//a0

	u16 vsyncend;//a2
	u16 vblkend;//a4
	u16 vblkstr;//a6
	u16 vtotal;//a8

	u16 reg_98;// ?
	u16 reg_ae;// ?

	u16 htotal_last;//96

	u16 hvidrst;//aa
	u16 vvidrst;//ac

	u8 vdisp;//be ?
	u8 hdisp;//c2 ?
};

int mga_tvo_init(struct mga_tvo_dev *tdev,
		 struct mga_dev *mdev,
		 u8 addr);

void mga_tvo_program_panel(struct mga_tvo_dev *tdev,
			   const struct mga_tvo_config *config,
			   const struct drm_display_mode *mode,
			   const struct mga_dac_pll_settings *pll);

void mga_tvo_program_crt(struct mga_tvo_dev *tdev,
			 const struct mga_tvo_config *config,
			 const struct drm_display_mode *mode,
			 const struct mga_dac_pll_settings *pll);

int mga_tvo_calc_tvout(struct mga_tvo_dev *tdev,
		       const struct mga_tvo_config *config,
		       const struct drm_display_mode *mode,
		       const struct mga_dac_pll_settings *pll);
void mga_tvo_program_tvout(struct mga_tvo_dev *tdev,
			   const struct mga_tvo_config *config,
			   const struct drm_display_mode *mode,
			   const struct mga_dac_pll_settings *pll);

void mga_tvo_program_bt656(struct mga_tvo_dev *tvodev,
			   const struct mga_tvo_config *config,
			   const struct drm_display_mode *mode,
			   const struct mga_dac_pll_settings *pll);
void mga_tvo_enable_bt656(struct mga_tvo_dev *tvodev);

void mga_tvo_reset_mafc(struct mga_tvo_dev *tvodev);

void mga_tvo_disable(struct mga_tvo_dev *tvodev);

int mga_tvo_test(struct mga_tvo_dev *tvodev);

int mga_tvo_pll_calc(struct mga_tvo_dev *tvodev,
		     unsigned int freq,
		     unsigned int fmax,
		     struct mga_dac_pll_settings *pll);

void mga_tvo_set_sync(struct mga_tvo_dev *tdev, unsigned int sync);
int mga_tvo_set_dot_crawl_freeze(struct mga_tvo_dev *tdev, bool enable);
int mga_tvo_set_color_bars(struct mga_tvo_dev *tdev, bool enable);
int mga_tvo_set_text_filter(struct mga_tvo_dev *tdev, bool enable);
int mga_tvo_set_deflicker(struct mga_tvo_dev *tdev, u8 deflicker);
int mga_tvo_set_gamma(struct mga_tvo_dev *tdev, u8 gamma);

int calc_tvout(struct mga_tvo_dev *tdev,
	       const struct mga_tvo_config *config,
	       const struct drm_display_mode *mode,
	       struct mga_tvo_crtc_regs *regs,
	       struct mga_dac_pll_settings *pll);

u8 mga_tvo_read8(struct mga_tvo_dev *tdev, u8 reg);
u16 mga_tvo_read16(struct mga_tvo_dev *tdev, u8 reg);
int mga_tvo_write8(struct mga_tvo_dev *tdev, u8 reg, u8 val);
int mga_tvo_write16(struct mga_tvo_dev *tdev, u8 reg, u16 val);

int mga_tvo_set_tv_standard(struct mga_tvo_dev *tdev,
			    unsigned int tv_std);
int mga_tvo_set_cable_type(struct mga_tvo_dev *tdev,
			   enum mga_tvo_cable_type cable_type);

void mga_tvo_monitor_sense(struct mga_tvo_dev *tdev,
			   bool *r, bool *g, bool *b);

int mga_tvo_check_config(const struct mga_tvo_dev *tdev,
			 const struct mga_tvo_config *config);

void mga_tvo_program(struct mga_tvo_dev *tdev,
		     const struct mga_tvo_config *config,
		     const struct drm_display_mode *mode,
		     const struct mga_dac_pll_settings *pll);

#endif
