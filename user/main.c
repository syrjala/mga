/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "mga_regs.h"
#include "mga_dump.h"

static void do_dump(struct mga_dev *mdev)
{
	dump_pci_cfg(mdev);
	dump_vga(mdev);
	dump_tvp3026(mdev);
	dump_dac(mdev);
	dump_g450_dac(mdev);
	//dump_rom(mdev);
	dump_pins(mdev);
	dump_generic(mdev);
	//dump_expdev(mdev);
}

static void usage(const char *name)
{
	pr_err("Usage: %s <device eg. /dev/mga0000\\:01\\:00.0> [init|noinit|coldinit|forceinit|forcecoldinit] [BIOS dump file]\n", name);
}

int main(int argc, char *argv[])
{
	struct mga_dev mdev;

	if (argc != 2 && argc != 3 && argc != 4) {
		usage(argv[0]);
		return 1;
	}
	if (argc >= 3 &&
	    strcmp(argv[2], "init") &&
	    strcmp(argv[2], "noinit") &&
	    strcmp(argv[2], "coldinit") &&
	    strcmp(argv[2], "forceinit") &&
	    strcmp(argv[2], "forcecoldinit")) {
		usage(argv[0]);
		return 1;
	}

	if (mga_open_device(argv[1], &mdev)) {
		pr_err("Failed to open device '%s'\n", argv[1]);
		return 2;
	}

	mga_print_chip_info(&mdev);

	if (argc >= 4) {
		int fd;
		ssize_t r;
		const char *fname = argv[3];

		read_rom(&mdev);
		if (!mdev.rom_size){
			dev_err(mdev.dev, "No ROM on board\n");
			goto skip;
		}

		fname = argv[3];
		fd = open(fname, O_CREAT | O_EXCL | O_WRONLY, 0644);
		if (fd < 0) {
			dev_err(mdev.dev, "Failed to open raw BIOS dump file '%s'\n", fname);
			goto skip;
		}

		r = write(fd, mdev.rom_data, mdev.rom_size);
		if (r != (ssize_t) mdev.rom_size) {
			dev_err(mdev.dev, "Failed to write raw BIOS dump file '%s': %s\n",
				fname, strerror(errno));
			unlink(fname);
		}
		close(fd);
	}
 skip:

	mdev.pixel_format = DRM_FORMAT_XRGB8888;//

	if (argc >= 3 && (!strcmp(argv[2], "coldinit") ||
			  !strcmp(argv[2], "forcecoldinit"))) {
		mga_hardreset(&mdev, !strcmp(argv[2], "forcecoldinit"));
	}

	dev_dbg(mdev.dev, "Pre-init dump:\n");
	do_dump(&mdev);

	if (argc >= 3 && (!strcmp(argv[2], "init") ||
			  !strcmp(argv[2], "coldinit") ||
			  !strcmp(argv[2], "forceinit") ||
			  !strcmp(argv[2], "forcecoldinit"))) {
		if (mga_init(&mdev,
			     !strcmp(argv[2], "forceinit") ||
			     !strcmp(argv[2], "forcecoldinit"))) {
			dev_dbg(mdev.dev, "VGA device, skipping init\n");
		} else {
			mga_tests(&mdev);
			dev_dbg(mdev.dev, "Post-init dump:\n");
			do_dump(&mdev);
			mga_fini(&mdev);
		}
	}
	mga_probe_mem_size(&mdev);

	mga_close_device(&mdev);

	return 0;
}
