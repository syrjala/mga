/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <pthread.h>
#include <assert.h>
#include <fcntl.h>
#include <errno.h>

#include "server.h"

#include "mga_dump.h"
#include "mga_dma.h"

#include "drm_kms.h"

struct server;

struct client {
	int fd;
	int thread_pipe[2];

	pthread_t thread;

	struct list_head head;

	struct server *server;

	pthread_mutex_t open_lock;
	unsigned int open_count;
	struct mga_dev *mdev_for_fd[256];

	u32 last_serial;
};

struct server {
	int fd;

	pthread_mutex_t clients_lock;
	struct list_head clients;

	pthread_mutex_t devs_lock;
	char *devs[8];
	struct mga_dev mdevs[8];
	unsigned int refs[8];
};

static struct mga_dev *server_ref_dev(struct server *srv, struct mga_dev *mdev)
{
	int ret;
	unsigned int fd;

	BUG_ON(!mdev);

	pthread_mutex_lock(&srv->devs_lock);

	/* Look for the same device in a slot */
	for (fd = 0; fd < ARRAY_SIZE(srv->devs); fd++) {
		if (&srv->mdevs[fd] == mdev)
			break;
	}
	/* Found it? */
	if (fd != ARRAY_SIZE(srv->devs)) {
		assert(srv->refs[fd]);

		srv->refs[fd]++;
		goto out;
	}

	BUG();

	mdev = NULL;

 out:
	pthread_mutex_unlock(&srv->devs_lock);

	return mdev;
}

static void server_unref_dev(struct server *srv, struct mga_dev *mdev)
{
	int ret;
	unsigned int fd;

	BUG_ON(!mdev);

	pthread_mutex_lock(&srv->devs_lock);

	/* Look for the same device in a slot */
	for (fd = 0; fd < ARRAY_SIZE(srv->devs); fd++) {
		if (&srv->mdevs[fd] == mdev)
			break;
	}
	/* Found it? */
	if (fd != ARRAY_SIZE(srv->devs)) {
		assert(srv->refs[fd] > 1);

		srv->refs[fd]--;
		goto out;
	}

	BUG();

 out:
	pthread_mutex_unlock(&srv->devs_lock);
}


static struct mga_dev *server_get_dev(struct server *srv, const char *dev)
{
	struct mga_dev *mdev = NULL;
	int ret;
	unsigned int fd;

	pthread_mutex_lock(&srv->devs_lock);

	/* Look for the same device in a slot */
	for (fd = 0; fd < ARRAY_SIZE(srv->devs); fd++) {
		if (srv->devs[fd] && !strcmp(srv->devs[fd], dev))
			break;
	}
	/* Found it? */
	if (fd != ARRAY_SIZE(srv->devs)) {
		assert(srv->refs[fd]);

		srv->refs[fd]++;
		mdev = &srv->mdevs[fd];
		goto out;
	}

	/* Look for a free slot */
	for (fd = 0; fd < ARRAY_SIZE(srv->devs); fd++) {
		if (!srv->devs[fd])
			break;
	}
	/* Didn't find one? */
	if (fd == ARRAY_SIZE(srv->devs)) {
		errno = ENOMEM;
		goto out;
	}

	assert(!srv->refs[fd]);

	srv->devs[fd] = strdup(dev);
	if (!srv->devs[fd])
		goto out;

	ret = mga_open_device(dev, &srv->mdevs[fd]);
	if (ret) {
		free(srv->devs[fd]);
		srv->devs[fd] = NULL;
		goto out;
	}

	mdev = &srv->mdevs[fd];
	srv->refs[fd]++;

	assert(srv->refs[fd] == 1);

 out:
	pthread_mutex_unlock(&srv->devs_lock);

	return mdev;
}

static int server_put_dev(struct server *srv,
			  struct mga_dev *mdev)
{
	int ret;
	unsigned int fd;

	pthread_mutex_lock(&srv->devs_lock);

	/* Look for our device */
	for (fd = 0; fd < ARRAY_SIZE(srv->devs); fd++) {
		if (srv->devs[fd] && &srv->mdevs[fd] == mdev)
			break;
	}
	/* Didn't find it? */
	if (fd == ARRAY_SIZE(srv->devs)) {
		ret = -EINVAL;
		goto out;
	}

	assert(srv->refs[fd]);

	/* Not the last open for this device? */
	if (srv->refs[fd] != 1) {
		srv->refs[fd]--;
		ret = 0;
		goto out;
	}

	mga_close_device(mdev);

	memset(&srv->mdevs[fd], 0, sizeof srv->mdevs[fd]);
	free(srv->devs[fd]);
	srv->devs[fd] = NULL;
	srv->refs[fd]--;
	ret = 0;

	assert(!srv->refs[fd]);

 out:
	pthread_mutex_unlock(&srv->devs_lock);

	return ret;
}

static struct mga_dev *client_get_dev(struct client *client, unsigned int fd)
{
	struct mga_dev *mdev = NULL;

	pthread_mutex_lock(&client->open_lock);

	if (fd >= ARRAY_SIZE(client->mdev_for_fd)) {
		errno = EINVAL;
		goto out;
	}

	if (!client->mdev_for_fd[fd]) {
		errno = EBADFD;
		goto out;
	}

	mdev = server_ref_dev(client->server, client->mdev_for_fd[fd]);

 out:
	pthread_mutex_unlock(&client->open_lock);

	return mdev;
}

static void client_put_dev(struct client *client, struct mga_dev *mdev)
{
	unsigned int fd;

	BUG_ON(!mdev);

	pthread_mutex_lock(&client->open_lock);

	/* Look for our device */
	for (fd = 0; fd < ARRAY_SIZE(client->mdev_for_fd); fd++) {
		if (client->mdev_for_fd[fd] == mdev)
			break;
	}
	/* Didn't find it? */
	if (fd == ARRAY_SIZE(client->mdev_for_fd)) {
		BUG();
		goto out;
	}

	server_unref_dev(client->server, mdev);

 out:
	pthread_mutex_unlock(&client->open_lock);
}

static int client_op_open(struct client *client,
			  const char *pathname, int flags)
{
	struct server *srv = client->server;
	struct mga_dev *mdev;
	int ret;
	unsigned int fd;

	printf("%s: %s %d\n", __func__, pathname, flags);

	pthread_mutex_lock(&client->open_lock);

	if (client->open_count == ARRAY_SIZE(client->mdev_for_fd)) {
		errno = ENOMEM;
		ret = -1;
		goto out;
	}

	mdev = server_get_dev(client->server, pathname);
	if (!mdev) {
		ret = -1;
		printf("ls\n");
		goto out;
	}

	for (fd = 0; fd < ARRAY_SIZE(client->mdev_for_fd); fd++) {
		if (!client->mdev_for_fd[fd])
			break;
	}
	assert(fd != ARRAY_SIZE(client->mdev_for_fd));

	client->mdev_for_fd[fd] = mdev;
	client->open_count++;
	ret = fd;

 out:
	pthread_mutex_unlock(&client->open_lock);

	printf("%s -> %d\n", __func__, ret);

	return ret;
}

static int client_op_close(struct client *client,
			   unsigned int fd)
{
	int ret;

	printf("%s: %u\n", __func__, fd);

	pthread_mutex_lock(&client->open_lock);

	if (!client->open_count) {
		errno = EINVAL;
		ret = -1;
		goto out;
	}

	if (fd >= ARRAY_SIZE(client->mdev_for_fd)) {
		errno = EINVAL;
		ret = -1;
		goto out;
	}

	if (!client->mdev_for_fd[fd]) {
		errno = EINVAL;
		ret = -1;
		goto out;
	}

	ret = server_put_dev(client->server,
			     client->mdev_for_fd[fd]);
	if (ret)
		goto out;

	client->mdev_for_fd[fd] = NULL;
	client->open_count--;

 out:
	pthread_mutex_unlock(&client->open_lock);

	printf("%s -> %d\n", __func__, ret);

	return ret;
}

static ssize_t client_op_read(struct client *client,
			      int fd, void *data, size_t len)
{
	struct mga_dev *mdev;
	ssize_t ret = len;

	printf("%s: %d %p %zu\n", __func__, fd, data, len);

	mdev = client_get_dev(client, fd);
	if (!fd) {
		ret = -1;
		goto out;
	}

	// FIXME

	client_put_dev(client, mdev);

 out:
	printf("%s -> %zd\n", __func__, ret);

	return ret;
}

static ssize_t client_op_write(struct client *client,
			       int fd, void *data, size_t len)
{
	struct mga_dev *mdev;
	ssize_t ret = len;

	printf("%s: %d %p %zu\n", __func__, fd, data, len);

	mdev = client_get_dev(client, fd);
	if (!fd) {
		ret = -EINVAL;
		goto out;
	}

	// FIXME

	client_put_dev(client, mdev);

 out:
	printf("%s -> %zd\n", __func__, ret);

	return ret;
}

static int client_open(struct client *client,
		       struct mga_cmd *cmd)
{
	char *pathname;

	printf("%s\n", __func__);

	if (!cmd->len) {
		errno = ENXIO;
		return -1;
	}

	pathname = (char *)cmd->data;
	pathname[cmd->len - 1] = '\0';

	// FIXME flags?
	return client_op_open(client, pathname, O_RDWR);
}

static int client_close(struct client *client,
			const struct mga_cmd *cmd)
{
	int fd = cmd->target;

	printf("%s\n", __func__);

	if (cmd->len != 0) {
		errno = ENXIO;
		return -1;
	}

	if (fd < 0) {
		errno = EBADFD;
		return -1;
	}

	return client_op_close(client, fd);
}

static ssize_t client_read(struct client *client,
			   struct mga_cmd *cmd)
{
	int fd = cmd->target;

	printf("%s\n", __func__);

	if (fd < 0) {
		errno = EBADFD;
		return -1;
	}

	return client_op_read(client, fd, cmd->data, cmd->len);
}

static ssize_t client_write(struct client *client,
			    const struct mga_cmd *c)
{
	ssize_t r;
	ssize_t ret;
	u8 *b = NULL;
	int fd = c->target;

	printf("%s\n", __func__);

	if (fd < 0) {
		errno = EBADFD;
		return -1;
	}

	if (c->len) {
		b = malloc(c->len);
		if (!b)
			return -1;

		r = wrap_read(client->fd, b, c->len);
		if (r < 0) {
			free(b);
			return -1;
		}

		if ((unsigned)r != c->len) {
			free(b);
			errno = ENXIO;
			return -1;
		}
	}

	ret = client_op_write(client, fd, b, c->len);

	free(b);

	return ret;
}

static int client_ioctl(struct client *client,
			struct mga_cmd *cmd)
{
	struct server_ioctl *ioc = (struct server_ioctl *)cmd->data;
	struct mga_dev *mdev;
	int ret = 0;
	size_t len;
	int fd = cmd->target;

	printf("%s\n", __func__);

	if (cmd->len < sizeof(*ioc)) {
		errno = ENXIO;
		return -1;
	}

	if (fd < 0) {
		errno = EBADFD;
		return -1;
	}

	len = cmd->len - sizeof(*ioc);

	switch (ioc->cmd) {
	case DRM_IOC_SET_MODE:
	case DRM_IOC_MK_FB:
	case DRM_IOC_RM_FB:
	case DRM_IOC_LOAD_LUT:
	case DRM_IOC_REG_RW:
	case DRM_IOC_MEM_TEST:
	case DRM_IOC_INIT:
	case DRM_IOC_FINI:
	case DRM_IOC_HARDRESET:
	case DRM_IOC_SUSPEND:
	case DRM_IOC_RESUME:
	case DRM_IOC_FILL:
		if (len != _IOC_SIZE(ioc->cmd)) {
			errno = EINVAL;
			return -1;
		}
		break;
	case DRM_IOC_ATOMIC:
	case DRM_IOC_GETRESOURCES:
	case DRM_IOC_DMA_GENERAL_PURPOSE:
		if (len < _IOC_SIZE(ioc->cmd)) {
			errno = EINVAL;
			return -1;
		}
		break;
	default:
		errno = EINVAL;
		return -1;
	}

	mdev = client_get_dev(client, fd);
	if (!mdev) {
		ret = -EINVAL;
		goto err;
	}

	_ioctl_user_data(ioc->data, _IOC_SIZE(ioc->cmd), len);

	ret = drm_ioctl(&mdev->base, ioc->cmd, ioc->data);

	client_put_dev(client, mdev);

 err:
	if (ret < 0) {
		errno = -ret;
		return -1;
	}

	return ret;
}

static bool cmd_writes(const struct mga_cmd *cmd)
{
	switch (cmd->cmd) {
		const struct server_ioctl *ioc;
	case MGA_CMD_OPEN:
		return true; /* filename */
	case MGA_CMD_WRITE:
		return true; /* data to be written */
	case MGA_CMD_IOCTL:
		return true; /* ioctl cmd nr at the very least */
	default:
		return false;
	}
}

static bool cmd_reads(const struct mga_cmd *cmd)
{
	switch (cmd->cmd) {
		const struct server_ioctl *ioc;
	case MGA_CMD_READ:
		return true;
	case MGA_CMD_IOCTL:
		ioc = (const struct server_ioctl *)cmd->data;

		if (_IOC_DIR(ioc->cmd) & _IOC_READ)
			return true;
		return false;
	default:
		return false;
	}
}

static int client_handle_cmd(struct client *client)
{
	struct mga_cmd *cmd;
	struct mga_res res;
	size_t len;
	ssize_t r;
	u32 last_serial = client->last_serial;
	u8 buf[24];
	int ret;

	r = wrap_read(client->fd, buf, 16);
	if (r < 0)
		return -1;
	if (r != 16) {
		errno = ENXIO;
		return -1;
	}

	res.cmd    = LE32_TO_CPU(buf + 0);
	res.serial = LE32_TO_CPU(buf + 4);
	res.len    = LE32_TO_CPU(buf + 8);
	res.target = LE32_TO_CPU(buf + 12);
	res.code   = 0;
	res._errno  = 0;

	printf("command cmd=%x, serial=%u, len=%u, target=%u\n",
	       res.cmd, res.serial, res.len, res.target);

	cmd = kmalloc(sizeof *cmd + res.len, GFP_KERNEL);
	if (!cmd) {
		errno = ENOMEM;
		ret = -1;
		goto send_error;
	}

	cmd->cmd    = res.cmd;
	cmd->serial = res.serial;
	cmd->len    = res.len;
	cmd->target = res.target;

	client->last_serial = cmd->serial;

	if (last_serial == cmd->serial) {
		errno = ENXIO;
		ret = -1;
		goto send_error;
	}

	if (cmd->len && cmd_writes(cmd)) {
		r = wrap_read(client->fd, cmd->data, cmd->len);
		if (r < 0)
			return -1;
		if ((size_t)r != cmd->len) {
			errno = ENXIO;
			return -1;
		}
	}

	if (res.len && !cmd_reads(cmd))
		res.len = 0;

	switch (cmd->cmd) {
	case MGA_CMD_OPEN:
		ret = client_open(client, cmd);
		break;
	case MGA_CMD_CLOSE:
		ret = client_close(client, cmd);
		break;
	case MGA_CMD_READ:
		ret = client_read(client, cmd);
		break;
	case MGA_CMD_WRITE:
		ret = client_write(client, cmd);
		break;
	case MGA_CMD_IOCTL:
		ret = client_ioctl(client, cmd);
		break;
	default:
		errno = EINVAL;
		return -1;
	}

 send_error:
	if (ret < 0) {
		res._errno = errno;

		CPU_TO_LE32(buf +  0, res.cmd);
		CPU_TO_LE32(buf +  4, res.serial);
		CPU_TO_LE32(buf +  8, res.len);
		CPU_TO_LE32(buf + 12, res.target);
		CPU_TO_LE32(buf + 16, res.code);
		CPU_TO_LE32(buf + 20, res._errno);

		printf("response(error) cmd=%x, serial=%u, len=%u, target=%u, code=%u, errno=%u\n",
		       res.cmd, res.serial, res.len, res.target, res.code, res._errno);

		r = wrap_write(client->fd, buf, 24);
		if (r < 0)
			return -1;
		if (r != 24) {
			errno = ENXIO;
			return -1;
		}

		return 0;
	}

	res.code = ret;

	CPU_TO_LE32(buf +  0, res.cmd);
	CPU_TO_LE32(buf +  4, res.serial);
	CPU_TO_LE32(buf +  8, res.len);
	CPU_TO_LE32(buf + 12, res.target);
	CPU_TO_LE32(buf + 16, res.code);
	CPU_TO_LE32(buf + 20, res._errno);

	printf("response(success) cmd=%x, serial=%u, len=%u, code=%u errno=%u\n",
	       res.cmd, res.serial, res.len, res.code, res._errno);

	r = wrap_write(client->fd, buf, 24);
	if (r < 0)
		return -1;
	if (r != 24) {
		errno = ENXIO;
		return -1;
	}

	if (res.len) {
		r = wrap_write(client->fd, cmd->data, res.len);
		if (r < 0)
			return -1;
		if ((size_t)r != res.len) {
			errno = ENXIO;
			return -1;
		}
	}

	kfree(cmd);

	return 0;
}

static void *client_loop(void *data)
{
	struct client *client = data;
	int ret;
	int fd = client->fd;

	printf("client thread\n");

	for (;;) {
		fd_set rfds, efds;

		FD_ZERO(&rfds);
		FD_ZERO(&efds);

		FD_SET(fd, &rfds);
		FD_SET(fd, &efds);

		ret = select(fd + 1, &rfds, NULL, &efds, NULL);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0) {
			fprintf(stderr, "select() failed %d:%s\n",
				errno, strerror(errno));
			break;
		}
		if (ret == 0)
			continue;

		if (FD_ISSET(fd, &efds)) {
			ret = -1;
			printf("err\n");
			break;
		}
		if (FD_ISSET(fd, &rfds)) {
			//printf("data in\n");
			if (client_handle_cmd(client))
				break;
		}
	}

	printf("client disconnecting\n");

	close(client->thread_pipe[1]);

	return NULL;
}

static int do_listen(const char *path)
{
	int fd;
	int ret;
	struct sockaddr_un addr;

	fd = open_socket(path, &addr);
	if (fd < 0)
		return fd;

	ret = bind(fd, (struct sockaddr *) &addr, sizeof addr);
	if (ret) {
		close(fd);
		return ret;
	}

	ret = listen(fd, 128);
	if (ret) {
		close(fd);
		return ret;
	}

	return fd;
}

static bool server_has_client(const struct server *srv,
			      const struct client *client)
{
	struct client *other;

	assert(srv);
	assert(client);

	list_for_each_entry(other, &srv->clients, head)
		if (client == other)
			return true;
	return false;
}

static void server_add_client(struct server *srv,
			      struct client *client)
{
	assert(srv);
	assert(client);

	assert(!server_has_client(srv, client));

	list_add(&client->head, &srv->clients);
}

static void server_rm_client(struct server *srv,
			     struct client *client)
{
	assert(srv);
	assert(client);

	assert(server_has_client(srv, client));

	list_del(&client->head);
}

static int client_connect(struct client *client)
{
	struct server *srv = client->server;
	int ret;

	printf("%s %p\n", __func__, client);

	pthread_mutex_lock(&srv->clients_lock);
	server_add_client(srv, client);
	pthread_mutex_unlock(&srv->clients_lock);

	printf("starting client thread\n");

	ret = pthread_create(&client->thread, NULL, client_loop, client);
	if (ret) {
		pthread_mutex_lock(&srv->clients_lock);
		server_rm_client(srv, client);
		pthread_mutex_lock(&srv->clients_lock);
		return ret;
	}

	return 0;
}

static void client_disconnect(struct client *client)
{
	struct server *srv = client->server;

	printf("%s %p\n", __func__, client);

	pthread_cancel(client->thread);
	pthread_join(client->thread, NULL);

	server_rm_client(srv, client);

	close(client->thread_pipe[0]);
	close(client->thread_pipe[1]);

	// FIXME do close

	free(client);
}

static int do_accept(struct server *srv)
{
	struct client *client;
	int fd;
	int ret;
	struct sockaddr_un addr = { .sun_family = 0 };
	socklen_t len = sizeof addr;
	int _errno;

	fd = accept(srv->fd, (struct sockaddr *) &addr, &len);
	if (fd < 0)
		return fd;

	printf("client connected from %s\n", addr.sun_path);

	client = calloc(1, sizeof *client);
	if (!client) {
		_errno = errno;
		close(fd);
		errno = _errno;
		return -1;
	}
	client->fd = fd;
	client->server = srv;
	client->last_serial = -1;
	if (pipe(client->thread_pipe)) {
		close(fd);
		free(client);
		return -1;
	}
	pthread_mutex_init(&client->open_lock, NULL);

	ret = client_connect(client);
	if (ret) {
		close(fd);
		free(client);
		return ret;
	}

	return 0;
}

#define max(a,b) ((a) > (b) ? (a) : (b))

static int server_loop(struct server *srv)
{
	int ret;
	int fd = srv->fd;

	for (;;) {
		struct client *client, *next;
		fd_set rfds, wfds, efds;
		int maxfd = fd;

		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
		FD_ZERO(&efds);

		FD_SET(fd, &rfds);
		FD_SET(fd, &efds);

		pthread_mutex_lock(&srv->clients_lock);
		list_for_each_entry(client, &srv->clients, head) {
			FD_SET(client->thread_pipe[0], &rfds);
			maxfd = max(client->thread_pipe[0], maxfd);
		}
		pthread_mutex_unlock(&srv->clients_lock);

		ret = select(maxfd + 1, &rfds, &wfds, &efds, NULL);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0) {
			fprintf(stderr, "select() failed %d:%s\n",
				errno, strerror(errno));
			break;
		}

		/* Collect dead client threads */
		pthread_mutex_lock(&srv->clients_lock);
		list_for_each_entry_safe(client, next, &srv->clients, head) {
			if (!FD_ISSET(client->thread_pipe[0], &rfds))
				continue;
			client_disconnect(client);
		}
		pthread_mutex_unlock(&srv->clients_lock);

		/* Disconnect all clients on error */
		if (FD_ISSET(fd, &efds)) {
			pthread_mutex_lock(&srv->clients_lock);
			list_for_each_entry_safe(client, next, &srv->clients, head)
				client_disconnect(client);
			pthread_mutex_unlock(&srv->clients_lock);
		}
		/* Accept new client connections */
		if (FD_ISSET(fd, &rfds))
			do_accept(srv);
	}

	close(fd);

	return ret;
}

static void usage(const char *name)
{
	fprintf(stderr, "Usage: %s <socket eg. /tmp/mga>\n", name);
}

int main(int argc, char *argv[])
{
	struct server srv = { .fd = -1 };
	int ret;

	if (argc < 2) {
		usage(argv[0]);
		return 1;
	}

	unlink(argv[1]);

	srv.fd = do_listen(argv[1]);
	if (srv.fd < 0) {
		usage(argv[0]);
		return 2;
	}

	INIT_LIST_HEAD(&srv.clients);
	pthread_mutex_init(&srv.clients_lock, NULL);
	pthread_mutex_init(&srv.devs_lock, NULL);

	ret = server_loop(&srv);

	unlink(argv[1]);

	if (ret)
		return 3;

	return 0;
}


