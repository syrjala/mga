/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef I2C_H
#define I2C_H

#include "kernel_emul.h"
#include "device.h"

#define THIS_MODULE NULL

struct i2c_adapter {
	void *owner;
	unsigned int id;
	unsigned int class;
	void *algo_data;
	int timeout;
	int retries;
	struct device dev;
};

struct i2c_algo_bit_data {
	void *data;
	void (*setsda)(void *data, int state);
	void (*setscl)(void *data, int state);
	int (*getsda)(void *data);
	int (*getscl)(void *data);
	int udelay;
	int timeout;
};

enum {
	I2C_M_RD           = 0x0001,
	I2C_M_NOSTART      = 0x4000,
	I2C_M_REV_DIR_ADDR = 0x2000,
};

struct i2c_msg {
	u16 addr;
	u16 flags;
	u16 len;
	u8 *buf;
};

void i2c_set_adapdata(struct i2c_adapter *adap, void *data);
void *i2c_get_adapdata(struct i2c_adapter *adap);

int i2c_bit_add_bus(struct i2c_adapter *adap);

int i2c_transfer(struct i2c_adapter * adap, struct i2c_msg *msgs, int num);

int i2c_smbus_write_byte_data(struct i2c_adapter *adap,
			      u8 addr, u8 command, u8 data);

int i2c_smbus_write_word_data(struct i2c_adapter *adap,
			      u8 addr, u8 command, u16 data);

int i2c_smbus_write_byte(struct i2c_adapter *adap, u8 addr, u8 data);
int i2c_smbus_read_byte(struct i2c_adapter *adap, u8 addr);

int i2c_smbus_read_buf(struct i2c_adapter *adap,
		       u8 addr, u8 command,
		       u8 buf[], size_t len);

int i2c_probe(struct i2c_adapter *adap, u8 addr);

#endif
