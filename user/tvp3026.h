/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef TVP_H
#define TVP_H

#include "kernel_emul.h"
#include "device.h"
#include "drm_kms.h"

/* FIXME make types opaque */

struct tvp_pll_info {
	const char *name;

	/* TVP_XPCLKPLLDATA, TVP_XMCLKPLLDATA or TVP_XLCLKPLLDATA */
	u8 xplldata;

	unsigned int fref;
	unsigned int fpll_max;
	unsigned int fvco_min, fvco_max;
};

struct tvp_pll_settings {
	unsigned int fvco;
	unsigned int fpll;
	u8 n, m, p, q;
};

enum tvp_bus_width {
	TVP_BUS_WIDTH_32,
	TVP_BUS_WIDTH_64,
};

struct tvp_dev {
	struct device *dev;

	void __iomem *mmio_virt;

	struct tvp_pll_info lclkpll_info;
	struct tvp_pll_info pclkpll_info;
	struct tvp_pll_info mclkpll_info;

	bool palette;
	bool ovl_src_ckey;
	bool ovl_dst_ckey;
	bool pedestal;
	bool interlace;

	u8 rev;
	u8 k;

	struct drm_color pal[256];
};

int tvp_powerup(struct tvp_dev *tdev, unsigned int mclk);

int tvp_init(struct tvp_dev *tdev, struct device *dev, void __iomem *mmio_virt,
	     unsigned int fref, unsigned int fvco_min, unsigned int fvco_max,
	     unsigned int rclk_max, unsigned int pclk_max, unsigned int mclk_max);

int tvp_start(struct tvp_dev *tdev, unsigned int pixel_format,
	      unsigned int pixel_clock, enum tvp_bus_width bus_width,
	      unsigned int rclk_to_lclk_ratio);

void tvp_stop(struct tvp_dev *tdev);

void tvp_monitor_sense_start(struct tvp_dev *tdev);
void tvp_monitor_sense_stop(struct tvp_dev *tdev, bool *r, bool *g, bool *b);

void tvp_set_sync(struct tvp_dev *tdev, unsigned int sync);
void tvp_set_pedestal(struct tvp_dev *tdev, bool enable);

void tvp_i2c_set(struct tvp_dev *tdev, u8 pin, bool state);
bool tvp_i2c_get(struct tvp_dev *tdev, u8 pin);

int tvp_mclkpll_calc(struct tvp_dev *tdev, unsigned int freq, unsigned int fmax,
		     struct tvp_pll_settings *pll);
int tvp_pclkpll_calc(struct tvp_dev *tdev, unsigned int freq, unsigned int fmax,
		     struct tvp_pll_settings *pll);
int tvp_lclkpll_calc(struct tvp_dev *tdev,
		     unsigned int fdot,
		     unsigned int bpp,
		     enum tvp_bus_width bus_width,
		     struct tvp_pll_settings *pll);

int tvp_mclkpll_program(struct tvp_dev *tdev,
			const struct tvp_pll_settings *pll);
int tvp_pclkpll_program(struct tvp_dev *tdev,
			const struct tvp_pll_settings *pll);
int tvp_lclkpll_program(struct tvp_dev *tdev,
			const struct tvp_pll_settings *pll);

void tvp_mclkpll_disable(struct tvp_dev *tdev);
void tvp_pclkpll_disable(struct tvp_dev *tdev);
void tvp_lclkpll_disable(struct tvp_dev *tdev);

void tvp_dac_power(struct tvp_dev *tdev, bool enable);

void tvp_dotclock_enable(struct tvp_dev *tdev, bool enable);

void tvp_program_format(struct tvp_dev *tdev,
			unsigned int pixel_format);

void tvp_program(struct tvp_dev *tdev,
		 unsigned int pixel_format,
		 enum tvp_bus_width bus_width);

int tvp_mclkpll_program_safe(struct tvp_dev *tdev,
			     const struct tvp_pll_settings *pll);

void tvp_load_palette(struct tvp_dev *tdev,
		      const struct drm_color pal[256]);
void tvp_set_palette(struct tvp_dev *tdev);

void tvp_cursor_image(struct tvp_dev *tdev, const u8 *data);
void tvp_cursor_position(struct tvp_dev *tdev,  int x, int y);

#endif
