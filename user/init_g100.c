/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <stdlib.h>

#include "mga_dump.h"
#include "mga_dac.h"
#include "mga_dac_regs.h"
#include "mga_regs.h"
#include "mga_i2c.h"
#include "modes.h"
#include "mga_flat.h"
#include "mga_tvout.h"

/*
 * Valid for G100 - G400
 * FIXME: check G450, G550
 */
void mga_g100_rfhcnt(struct mga_dev *mdev, int mclk)
{
	int rfhcnt = 0;
	u32 option;

	if (mclk <= 0)
		goto done;

	mclk *= 1000;

	/*
	 * ram refresh period >= (rfhcnt<5:0> * 64 + 1) * MCLK period
	 */
	rfhcnt = (mclk - 66667) / (64 * 66667);
	rfhcnt = clamp(rfhcnt, 0x01, 0x3F);

 done:
	dev_dbg(mdev->dev, "Setting rfhcnt to 0x%02x\n", rfhcnt);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_RFHCNT_NEW;
	option |= rfhcnt << 15;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static unsigned int calc_mclk(unsigned int syspll, u32 option)
{
	unsigned int mclk;

	switch (option & MGA_OPTION_SYSCLKSL) {
	case MGA_OPTION_SYSCLKSL_PCI:
		/* FIXME? */
		return 33333;
	case MGA_OPTION_SYSCLKSL_SYSPLL:
		mclk = syspll;
		break;
	case MGA_OPTION_SYSCLKSL_MCLK:
		/* FIXME? */
		return 0;
	default:
		BUG();
	}

	if (option & MGA_OPTION_MCLKDIV)
		return mclk;
	else
		return div_round(mclk, 2);
}

#if 0
static void mga_g100_clock_powerup(struct mga_dev *mdev)
{
	// fIXME
	u32 option;
	u8 val;

	/* 1. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 2. */
	option |= MGA_OPTION_SYSCLKSL_SYSPLL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 3. */
	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 4. */
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXCLKCTRL);
	val = mga_read8(mdev, MGA_X_DATAREG);
	val |= MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 5. */
	val |= MGA_XPIXCLKCTRL_PIXCLKSEL_PIXPLL;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 6. */
	val &= ~MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);
}
#endif

static void sysclk_select(struct mga_dev *mdev, u32 sysclksl)
{
	u32 option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);

	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option &= ~MGA_OPTION_SYSCLKSL;
	option |= sysclksl & MGA_OPTION_SYSCLKSL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static int sysclk_program(struct mga_dev *mdev,
			  unsigned int syspllfo,
			  u32 option_clk)
{
	struct mga_dac_pll_settings syspll;
	unsigned int mclk;
	u32 option;
	int ret;

	if (syspllfo) {
		ret = mga_dac_syspll_calc(mdev, syspllfo, 0, &syspll);
		if (ret)
			return ret;
	}

	mclk = calc_mclk(0, MGA_OPTION_SYSCLKSL_PCI);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, MGA_OPTION_SYSCLKSL_PCI);
	mga_mclk_change_post(mdev, mclk);

	mga_dac_syspll_power(mdev, false);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_PLLSEL;
	option |= option_clk & MGA_OPTION_PLLSEL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	if (syspllfo) {
		mga_dac_syspll_power(mdev, true);
		ret = mga_dac_syspll_program(mdev, &syspll);
		if (ret) {
			mga_dac_syspll_power(mdev, false);
			return ret;
		}
	}

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~(MGA_OPTION_GCLKDIV | MGA_OPTION_MCLKDIV | MGA_OPTION_FMCLKDIV);
	option |= option_clk & (MGA_OPTION_GCLKDIV | MGA_OPTION_MCLKDIV | MGA_OPTION_FMCLKDIV);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	mclk = calc_mclk(syspll.fo, option_clk);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, option_clk);
	mga_mclk_change_post(mdev, mclk);

	if (syspllfo)
		mdev->sysclk_plls |= MGA_PLL_SYSPLL;
	else
		mdev->sysclk_plls &= ~MGA_PLL_SYSPLL;

	return 0;
}

static void mga_g100_mem_reset(struct mga_dev *mdev);

static void mga_g100_powerup(struct mga_dev *mdev, bool mem_reset)
{
	const struct mga_pins3 *pins = &mdev->pins.pins3;
#if 0
	/* productiva */
	u32 pins_option = 0x00001089;
	unsigned int pins_gclk = 72000;
#else
	u32 pins_option = pins->clk_2d.option;
	unsigned int pins_gclk = pins->clk_2d.gclk[0 >> 1];
#endif
	unsigned int gclk, mclk, fmclk, syspll = 0;
	int ret;

	switch (pins_option & MGA_OPTION_SYSCLKSL) {
	case MGA_OPTION_SYSCLKSL_PCI:
		gclk = mclk = fmclk = 33333;
		break;
	case MGA_OPTION_SYSCLKSL_SYSPLL:
		gclk = pins_gclk;
		/* mclk and fmclk calculated later */
		break;
	case MGA_OPTION_SYSCLKSL_MCLK:
		gclk = mclk = fmclk = 0;
		break;
	default:
		BUG();
	}

	if ((pins_option & MGA_OPTION_SYSCLKSL) == MGA_OPTION_SYSCLKSL_SYSPLL) {
		if (pins_option & MGA_OPTION_GCLKDIV)
			syspll = pins_gclk * 3;
		else
			syspll = pins_gclk * 2;

		if (pins_option & MGA_OPTION_MCLKDIV)
			mclk = syspll;
		else
			mclk = div_round(syspll, 2);

		if (pins_option & MGA_OPTION_FMCLKDIV)
			fmclk = div_round(syspll, 2);
		else
			fmclk = syspll;
	}

	dev_dbg(mdev->dev, "SYSPLL = %u kHz\n", syspll);
	dev_dbg(mdev->dev, "  GCLK = %u kHz\n", gclk);
	dev_dbg(mdev->dev, "  MCLK = %u kHz\n", mclk);
	dev_dbg(mdev->dev, " FMCLK = %u kHz\n", fmclk);

	mga_dac_init(mdev, pins->fref,
		     pins->fvco_max, pins->fvco_max);

	ret = sysclk_program(mdev, syspll, pins_option);
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		return;
	}

	if (mem_reset) {
		mga_g100_rfhcnt(mdev, 0);//
		udelay(200);//
		mga_g100_mem_reset(mdev);
	}

	mdev->maccess = MGA_MACCESS_JEDECRST;
}

enum {
	MGA_GPIO_SDRAM = 0x01,
};

static void mga_g100_mem_reset(struct mga_dev *mdev)
{
	const struct mga_pins3 *pins = &mdev->pins.pins3;
#if 0
	/* productiva */
	int pins_option = 0x00001089;
	int pins_option2 = 0x00000007;
	int pins_mctlwtst = 0x01232521;
#else
	u32 pins_option = pins->clk_2d.option;
	u32 pins_option2 = pins->clk_2d.option2;
	u32 pins_mctlwtst = pins->mctlwtst;
#endif
	u32 option, option2;

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_MRMOPTION;
	option |= pins_option & MGA_OPTION_MRMOPTION;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	// GPIO magic for plane write mask
	if (pins->mem.sdram) {
		u8 val;

		mga_write8(mdev, MGA_X_INDEXREG, MGA_XGENIOCTRL);
		val = mga_read8(mdev, MGA_X_DATAREG);
		val |= MGA_GPIO_SDRAM;
		mga_write8(mdev, MGA_X_DATAREG, val);
	}

	mga_write32(mdev, MGA_PLNWT, 0x00000000);
	mga_write32(mdev, MGA_PLNWT, 0xffffffff);

	/* Step 2. */
	mga_write32(mdev, MGA_MCTLWTST, pins_mctlwtst);

	/* Step 3. */
	/* Set memconfig */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_MEMCONFIG;
	option |= pins_option & MGA_OPTION_MEMCONFIG;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* Set mbuftype */
	option2 = pci_cfg_read32(&mdev->pdev, MGA_OPTION2);
	option2 &= ~MGA_OPTION2_MBUFTYPE;
	option2 |= pins_option2 & MGA_OPTION2_MBUFTYPE;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION2, option2);

	/* Set memrclkd */
	option2 = pci_cfg_read32(&mdev->pdev, MGA_OPTION2);
	option2 &= ~MGA_OPTION2_MEMRCLKD;
	option2 |= pins_option2 & MGA_OPTION2_MEMRCLKD;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION2, option2);

	/* Step 4. */
	udelay(200);

	/* Step 7. */
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_MEMRESET | MGA_MACCESS_JEDECRST);

	/* Step 8. */
	mga_g100_rfhcnt(mdev, mdev->mclk);
}

static void probe_addons(struct mga_dev *mdev)
{
	const struct mga_pins3 *pins = &mdev->pins.pins3;
	int ret;

	mdev->outputs |= MGA_OUTPUT_DAC1;

	if (pins->features & _MGA_FEAT_TVO_BUILTIN)
		mdev->features |= MGA_FEAT_TVO_BUILTIN;
	if (pins->features & _MGA_FEAT_DVD_BUILTIN)
		mdev->features |= MGA_FEAT_DVD_BUILTIN;
	if (pins->features & _MGA_FEAT_MJPEG_BUILTIN)
		mdev->features |= MGA_FEAT_MJPEG_BUILTIN;
	if (pins->features & _MGA_FEAT_VIN_BUILTIN)
		mdev->features |= MGA_FEAT_VIN_BUILTIN;
	if (pins->features & _MGA_FEAT_TUNER_BUILTIN)
		mdev->features |= MGA_FEAT_TUNER_BUILTIN;
	if (pins->features & _MGA_FEAT_AUDIO_BUILTIN)
		mdev->features |= MGA_FEAT_AUDIO_BUILTIN;
	if (pins->features & _MGA_FEAT_TMDS_BUILTIN)
		mdev->features |= MGA_FEAT_TMDS_BUILTIN;

	dev_dbg(mdev->dev, "Before probe:\n");
	mga_print_features(mdev);

	if (!(mdev->features & MGA_FEAT_VIN_BUILTIN)) {
		/*
		 * No TV-out addon for MMS.
		 * Can't detect MMS w/o TV tuner from features.
		 */
		ret = mga_tvout_addon_probe(mdev);
		if (ret)
			dev_dbg(mdev->dev, "Failed to locate TV-out addon\n");
	}

	if (!(mdev->features & (MGA_FEAT_VIN_BUILTIN | MGA_FEAT_TVO_ADDON))) {
		/*
		 * No flat panel addon for MMS.
		 * Can't detect MMS w/o TV tuner from features.
		 * Only one addon can be present.
		 */
		ret = mga_tmds_addon_probe(mdev);
		if (ret)
			dev_dbg(mdev->dev, "Failed to locate flat panel addon\n");
	}

	dev_dbg(mdev->dev, "After probe:\n");
	mga_print_features(mdev);
}

static void mga_g100_init(struct mga_dev *mdev)
{
	struct drm_display_mode mode = { .vrefresh = 0 };
	unsigned int outputs = 0;
	u32 option;

	mode = mode_640_480_60;

	outputs |= MGA_OUTPUT_DAC1;
	outputs |= MGA_OUTPUT_TMDS1;
	//outputs |= MGA_OUTPUT_TVOUT;

	mga_set_initial_crtc1_mode(mdev,
				   mdev->pixel_format,
				   &mode,
				   outputs);

	mga_misc_disable_vga(mdev);

	dev_dbg(mdev->dev, "Disabling CRTC1\n");
	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);
	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	//mga_dac_init(mdev);

	mga_misc_powerup(mdev);

	mga_dac_powerup(mdev);

	mga_g100_powerup(mdev, true);

	mga_crtc1_powerup(mdev);

	mga_i2c_dac_misc_init(mdev);
	mga_i2c_dac_ddc1_init(mdev);

	probe_addons(mdev);

	mga_probe_mem_size(mdev);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	if (mdev->splitmode)
		option |= MGA_OPTION_SPLITMODE;
	else
		option &= ~MGA_OPTION_SPLITMODE;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static void mga_g100_test(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");

	mga_enable_outputs(mdev);

	//fixme
	//if (mdev_outputs & MGA_OUTPUT_DAC1)
	//	mdev->funcs->monitor_sense_dac1(mdev);

	if (strstr(test, "reg"))
		mga_reg_test(mdev);
	if (strstr(test, "tvo") &&
	    mdev->features & (MGA_FEAT_TVO_BUILTIN | MGA_FEAT_TVO_ADDON))
		mga_tvo_test(&mdev->tvodev);

	mga_disable_outputs(mdev);
}

static int mga_g100_suspend(struct mga_dev *mdev)
{
	u32 option;
	int ret;

	if (mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to power saving mode\n");

	/* slumber */
	mga_disable_outputs(mdev);

	mga_wait_dma_idle(mdev);
	mga_wait_dwgeng_idle(mdev);

	/* Preserve pllsel */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= MGA_OPTION_PLLSEL;
	option |= MGA_OPTION_SYSCLKSL_SYSPLL | MGA_OPTION_GCLKDIV | MGA_OPTION_MCLKDIV;

	ret = sysclk_program(mdev, 6667, option);
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		goto err;
	}

	mdev->suspended = true;

	dev_dbg(mdev->dev, "In power saving mode\n");

	return 0;

 err:
	mga_enable_outputs(mdev);

	return ret;
}

static int mga_g100_resume(struct mga_dev *mdev)
{
	if (!mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to normal mode\n");

	mga_g100_powerup(mdev, false);

	mga_enable_outputs(mdev);

	mdev->suspended = false;

	dev_dbg(mdev->dev, "In normal mode\n");

	return 0;
}

static const struct mga_chip_funcs mga_g100_funcs = {
	.rfhcnt = mga_g100_rfhcnt,
	.monitor_sense_dac1 = mga_1064sg_monitor_sense,
	.suspend = mga_g100_suspend,
	.resume = mga_g100_resume,
	.init = mga_g100_init,
	.set_mode = mga_1064sg_set_mode,
	.test = mga_g100_test,
	.softreset = mga_1064sg_softreset,
};

void mga_g100_prepare(struct mga_dev *mdev)
{
	mdev->funcs = &mga_g100_funcs;
}
