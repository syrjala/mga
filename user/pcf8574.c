/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include "pcf8574.h"

void pcf8574_gpio_set(struct pcf8574_dev *pdev, u8 mask, u8 state)
{
	int ret;
	u8 new_state = (pdev->state & ~mask) | (state & mask);

	BUG_ON(state & ~mask);

	ret = i2c_smbus_write_byte(pdev->adap, pdev->addr, new_state);
	if (ret < 0) {
		dev_err(&pdev->adap->dev, "Failed to write PCF8574\n");
		return;
	}

	pdev->state = new_state;
}

u8 pcf8574_gpio_get(struct pcf8574_dev *pdev)
{
	int ret;

	ret = i2c_smbus_read_byte(pdev->adap, pdev->addr);
	if (ret < 0) {
		dev_err(&pdev->adap->dev, "Failed to read PCF8574\n");
		return 0xff;
	}

	return ret;
}

int pcf8574_init(struct pcf8574_dev *pdev,
		 struct i2c_adapter *adap, u8 addr)
{
	int ret;

	ret = i2c_smbus_read_byte(adap, addr);
	if (ret < 0) {
		dev_dbg(&adap->dev, "Failed to detect PCF8574 @ %02x\n", addr);
		return ret;
	}

	dev_dbg(&adap->dev, "Detected PCF8574 @ %02x\n", addr);

	pdev->adap = adap;
	pdev->addr = addr;
	pdev->state = ret;

	return 0;
}

void pcf8574_reset(struct pcf8574_dev *pdev)
{
	/* Configure all pins as inputs. */
	pcf8574_gpio_set(pdev, 0xff, 0xff);
}
