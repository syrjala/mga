/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_TVOUT_H
#define MGA_TVOUT_H

struct mga_dev;

int mga_tvout_addon_probe(struct mga_dev *mdev);
int mga_tvout_builtin_probe(struct mga_dev *mdev);

#endif
