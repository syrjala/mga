/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_DAC_COMMON_H
#define MGA_DAC_COMMON_H

#include "kernel_emul.h"

struct mga_dev;

struct mga_dac_pll_info {
	const char *name;
	unsigned int fref;
	unsigned int fo_max;
	unsigned int fvco_min, fvco_max;
	u8 m_min, m_max;
	u8 n_min, n_max;
	unsigned int s_limits[4];
	u8 pllm, plln, pllp;
	u8 pllstat, pllstat_lock;
};

struct mga_dac_pll_settings {
	unsigned int fvco;
	unsigned int fo;
	u8 n, m, p;
};

void mga_dac_pll_info_print(struct mga_dev *mdev,
			    const struct mga_dac_pll_info *info);

struct mga_dac_config {
	u32 pixel_format;
	u16 gfx_dst_ckey; /* used in as dst ckey for split mode video */
	u8 ovl_src_ckey; /* used as src ckey for pseudocolor ovl */
	u8 hzoom;
	bool enable_video;
	bool enable_overlay;
	bool video_alpha;
	bool video_palette;
	bool video_polarity;
};

#endif
