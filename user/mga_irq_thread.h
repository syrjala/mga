/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_IRQ_THREAD_H
#define MGA_IRQ_THREAD_H

#include "mga_dump.h"

struct mga_dev;

#define local_irq_disable() do {} while (0)
#define local_irq_enable() do {} while (0)
#define local_irq_is_disabled() (0)

void mga_write32_ien(struct mga_dev *mdev, u32 reg, u32 val);
void mga_write32_iclear(struct mga_dev *mdev, u32 reg, u32 val);
u32 mga_read32_ien(struct mga_dev *mdev, u32 reg);
int mga_irq_thread_init(struct mga_dev *mdev);
void mga_irq_thread_fini(struct mga_dev *mdev);

#endif
