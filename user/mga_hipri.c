/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include "kernel_emul.h"
#include "mga_dump.h"
#include "mga_regs.h"

/*
 * Note that pre-G200 specifications don't have any
 * hiprilvl/maxhipri equations. Not sure if we can use
 * the G200 formula on those chips too.
 */
int mga_g200_hipri(struct mga_dev *mdev,
		   int mclk, int pixclk,
		   struct mga_crtc1_regs *regs)
{
	static const u8 mp_tab[2][3] = {
		{ 52, 41, 32 }, /* w/o CODEC or VIN */
		{ 59, 41, 32 }, /* w/  CODEC or VIN */
	};
	bool codec_or_vin;
	int strmfctl, mp, scale;
	int hiprilvl, maxhipri;

	/*
	 * From MGA-G200 specification:
	 *
	 * ( (7 - hiprilvl) (VC) (8) + ( (1)VC - 1) ) (Tpixclk) >= (MP) (Tmclk) + (11) (VC) (Tpixclk)
	 *
	 * MAXHIPRI = MIN(HIPRILVL, (SCALE + 1)(Tmclk / Tpixclk)) (round to the nearest whole number)
	 *
	 * where:
	 * SCALE = value programmed into the SCALE register.
	 * VC = 8 /DF (which is the number of pixels per slice)
	 * 8 in BPP8
	 * 4 in BPP15 / BPP16
	 * 8/3 in BPP24 packed pixel
	 * 2 in BPP32PAL / BPP32DIR
	 *
	 * Tmclk = The period of MCLK in ns
	 * Tpixclk = The period of the pixel clock in ns
	 * MP = Memory Controller Pipe Depth
	 * strmfctl = The Streamer Pipe Blocking Field
	 *
	 *               MP with    MP without
	 *               CODEC or   VIN CODEC or VIN
	 * if            operating  operating
	 * strmfctl = 2  32         32
	 * strmfctl = 1  41         41
	 * strmfctl = 0  59         52
	 *
	 * Note:
	 * VC = 8 / (SCALE + 1)
	 */

	/* Is CODEC or VIN operating? */
	codec_or_vin = mga_read32(mdev, MGA_CODECCTL) & MGA_CODECCTL_CODECEN ||
		mga_read32(mdev, MGA_VINCTL) & MGA_VINCTL_VINEN;

	strmfctl = (mga_read32(mdev, MGA_MEMRDBK) & MGA_MEMRDBK_STRMFCTL) >> 22;
	/* shouldn't happen assuming it was programmed correctly */
	if (strmfctl > 2) {
		dev_dbg(mdev->dev, "STRMFCTL invalid %d, using 0 instead\n", strmfctl);
		strmfctl = 0;
	}

	mp = mp_tab[codec_or_vin][strmfctl];

	scale = regs->crtcext[0x03] & MGA_CRTCEXT3_SCALE;

	hiprilvl = (368 * mclk - mp * pixclk * (scale + 1) - mclk * (scale + 1)) / (64 * mclk);

	if (hiprilvl < 0 || hiprilvl > 7) {
		dev_dbg(mdev->dev, "crtc1 hiprilvl out of range %d, clamping to %d\n",
			hiprilvl, clamp(hiprilvl, 0, 7));
		hiprilvl = clamp(hiprilvl, 0, 7);
	}

	maxhipri = min(hiprilvl, DIV_ROUND_CLOSEST((scale + 1) * pixclk, mclk));

	regs->crtcext[0x06] = (maxhipri << 4) | hiprilvl;

	return 0;
}

static int calc_maxhipri(int hiprilvl)
{
	if (hiprilvl > 3)
		return  2;
	else if (hiprilvl > 0)
		return 1;
	else
		return 0;
}

int mga_g400_hipri(struct mga_dev *mdev,
		   int mclk, int c1pixclk, int c2pixclk,
		   int c1bpp, int c2bpp, bool bt656,
		   struct mga_crtc1_regs *c1regs,
		   struct mga_crtc2_regs *c2regs)
{
	int tmclk, tc1pix, tc2pix;
	int c1dep, c2dep;
	int d1 = 0, d2 = 0, t, r1 = 0, r2 = 0;
	int c1hiprilvl, c1maxhipri, c2hiprilvl, c2maxhipri;

	/* Per MGA-G400 specification. */
	/* CRTC2 in YUV or grab back mode, just assume that means BT.656 for now */
	if (c2pixclk && bt656) {
		c1hiprilvl = 0;
		c1maxhipri = 0;
		c2hiprilvl = 2;
		c2maxhipri = 1;

		goto done;
	}

	/*
	 * From MGA-G400 specification:
	 *
	 * | (-8)(C1DEP)Tpix (-9)Tmclk         |   | HIPRILVL   |   | (64)Tmclk + (1 - 46(C1DEP))Tpix |
	 * |                                   | X |            | = |                                 |
	 * | (-9)Tmclk       (-8)(C2DEP)Tc2pix |   | C2HIPRILVL |   | (64)Tmclk + (1 - 46(C2DEP))Tpix |
	 *
	 * Tmclk = The number of nanoseconds in an MCLK cycle. (i.e. if MCLK = 143 MHz, then use 7 in the above equation)
	 * Tpix = The number of nanoseconds in a PIXCLK cycle.
	 * Tc2pix = The number of nanoseconds in a C2PIXCLK cycle.
	 * C1DEP = The number of CRTC1 pixels that fit into 128 bits. (depends on the color depth! i.e. at 32BPP use 4, at 8BPP use 16)
	 * C2DEP = The number of CRTC2 pixels that fit into 128 bits. (depends on the color depth! i.e. at 32BPP use 4, at 16BPP use 8)
	 *
	 * The Tpix in the lower row of the right hand side matrix is
	 * assumed to be a typo. It should probably be Tc2pix.
	 *
	 * To make the code readable we assign symbols to the matrix cells like so:
	 *
	 * | d1   t |   | hiprilvl   |   | r1 |
	 * |        | X |            | = |    |
	 * |  t  d2 |   | c2hiprilvl |   | r2 |
	 *
	 * The first and third matrix also need to be multiplied by 3
	 * to accomodate 12 and 24 bpp (to make C1DEP and C2DEP integers).
	 */

	tmclk = DIV_ROUND_UP(1000000, mclk);
	t = 3 * -9 * tmclk;
	if (c1pixclk) {
		tc1pix = DIV_ROUND_UP(1000000, c1pixclk);
		c1dep = 3 * 128 / c1bpp;
		d1 = -8 * c1dep * tc1pix;
		r1 = 3 * 64 * tmclk + (3 - 46 * c1dep) * tc1pix;
	}
	if (c2pixclk) {
		tc2pix = DIV_ROUND_UP(1000000, c2pixclk);
		c2dep = 3 * 128 / c2bpp;
		d2 = -8 * c2dep * tc2pix;
		r2 = 3 * 64 * tmclk + (3 - 46 * c2dep) * tc2pix;
	}

	/*
	 * FIXME: should the mode be rejected if the values come out
	 * outside the valid range? Right now the values will be clamped.
	 */
	if (c1pixclk && c2pixclk) {
		c1hiprilvl = (d2 * r1 - t * r2) / (d1 * d2 - t * t);
		c2hiprilvl = (r2 - t * c1hiprilvl) / d2;
	} else if (c1pixclk) {
		c1hiprilvl = r1 / d1;
		c2hiprilvl = 0;
	} else if (c2pixclk) {
		c1hiprilvl = 0;
		c2hiprilvl = r2 / d2;
	} else {
		c1hiprilvl = 0;
		c2hiprilvl = 0;
	}

	if (c1hiprilvl < 0 || c1hiprilvl > 7) {
		dev_dbg(mdev->dev, "crtc1 hiprilvl out of range %d, clamping to %d\n",
			c1hiprilvl, clamp(c1hiprilvl, 0, 7));
		c1hiprilvl = clamp(c1hiprilvl, 0, 7);
	}
	if (c2hiprilvl < 0 || c2hiprilvl > 7) {
		dev_dbg(mdev->dev, "crtc2 hiprilvl out of range %d, clamping to %d\n",
			c2hiprilvl, clamp(c2hiprilvl, 0, 7));
		c2hiprilvl = clamp(c2hiprilvl, 0, 7);
	}

	c1maxhipri = calc_maxhipri(c1hiprilvl);
	c2maxhipri = calc_maxhipri(c2hiprilvl);

 done:
	c1regs->crtcext[0x06] = (c1maxhipri << 4) | c1hiprilvl;

	c2regs->c2ctl &= ~(MGA_C2CTL_C2MAXHIPRI | MGA_C2CTL_C2HIPRILVL);
	c2regs->c2ctl |= (c2maxhipri << 8) | (c2hiprilvl << 4);

	dev_dbg(mdev->dev, "crtc1 hiprilvl=%d maxhipri=%d\n", c1hiprilvl, c1maxhipri);
	dev_dbg(mdev->dev, "crtc2 hiprilvl=%d maxhipri=%d\n", c2hiprilvl, c2maxhipri);

	return 0;
}
