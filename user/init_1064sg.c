/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <stdlib.h>

#include "mga_dump.h"
#include "mga_dac.h"
#include "mga_regs.h"
#include "modes.h"
#include "mga_i2c.h"

static unsigned int bpp_to_index(unsigned int bpp)
{
	return (bpp + 7) / 8 - 1;
}

unsigned int mga_1064sg_alignment(unsigned int bpp)
{
	static const u8 values[] = { 16, 8, 16, 4 };
	return values[bpp_to_index(bpp)];
}

void mga_1064sg_softreset(struct mga_dev *mdev)
{
	mga_write32(mdev, MGA_RST, MGA_RST_SOFTRESET);
	udelay(10);
	mga_write32(mdev, MGA_RST, 0);
}

static void mga_1064sg_rfhcnt(struct mga_dev *mdev, int mclk)
{
	int rfhcnt = 0;
	u32 option;

	if (mclk <= 0)
		goto done;

	mclk *= 1000;

	/*
	 * ram refresh period >= (rfhcnt<3:1> * 256 + rfhcnt<0> * 64 + 1) * MCLK period
	 */
	rfhcnt = (mclk - 66667) / (64 * 66667);
	rfhcnt = clamp(rfhcnt, 0x1, 0x1F);
	/*
	 * The register doesn't have room for rfhcnt bit [1] so just
	 * collapse bits [1:0] to bit [0] to minimize the reduction
	 * in rfhcnt when bit [1] would be set.
	 */
	rfhcnt = ((rfhcnt & 0x1C) >> 1) | !!(rfhcnt & 0x03);

 done:
	dev_dbg(mdev->dev, "Setting rfhcnt to 0x%01x\n", rfhcnt);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_RFHCNT_OLD;
	option |= rfhcnt << 16;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static unsigned int calc_mclk(unsigned int syspll, u32 option)
{
	unsigned int mclk;

	switch (option & MGA_OPTION_SYSCLKSL) {
	case MGA_OPTION_SYSCLKSL_PCI:
		/* FIXME? */
		return 33333;
	case MGA_OPTION_SYSCLKSL_SYSPLL:
		mclk = syspll;
		break;
	case MGA_OPTION_SYSCLKSL_MCLK:
		/* FIXME? */
		return 0;
	default:
		BUG();
	}

	if (option & MGA_OPTION_MCLKDIV)
		return mclk;
	else
		return div_round(mclk, 2);
}


#if 0
static void mga_1064sg_clock_powerup()
{
	u32 option;
	u8 val;

	/* 1. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 2. */
	option |= MGA_OPTION_SYSCLKSEL_SYSPLL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 3. */
	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 4. */
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXCLKCTRL);
	val = mga_read8(mdev, MGA_X_DATAREG);
	val |= MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 5. */
	val |= MGA_XPIXCLKCTRL_PIXCLKSEL_PIXPLL;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 6. */
	val &= ~MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);
}
#endif

static void sysclk_select(struct mga_dev *mdev, u32 sysclksl)
{
	u32 option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);

	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option &= ~MGA_OPTION_SYSCLKSL;
	option |= sysclksl;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static int sysclk_program(struct mga_dev *mdev,
			  unsigned int syspllfo,
			  u32 option_clk)
{
	struct mga_dac_pll_settings syspll;
	unsigned int mclk;
	u32 option;
	int ret;

	if (syspllfo) {
		ret = mga_dac_syspll_calc(mdev, syspllfo, 0, &syspll);
		if (ret)
			return ret;
	}

	mclk = calc_mclk(0, MGA_OPTION_SYSCLKSL_PCI);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, MGA_OPTION_SYSCLKSL_PCI);
	mga_mclk_change_post(mdev, mclk);

	mga_dac_syspll_power(mdev, false);

	if (syspllfo) {
		mga_dac_syspll_power(mdev, true);
		ret = mga_dac_syspll_program(mdev, &syspll);
		if (ret) {
			mga_dac_syspll_power(mdev, false);
			return ret;
		}
	}

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~(MGA_OPTION_GCLKDIV | MGA_OPTION_MCLKDIV);
	option |= option_clk & (MGA_OPTION_GCLKDIV | MGA_OPTION_MCLKDIV);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	mclk = calc_mclk(syspll.fo, option_clk);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, option_clk);
	mga_mclk_change_post(mdev, mclk);

	if (syspllfo)
		mdev->sysclk_plls |= MGA_PLL_SYSPLL;
	else
		mdev->sysclk_plls &= ~MGA_PLL_SYSPLL;

	return 0;
}

static void mga_1064sg_mem_reset(struct mga_dev *mdev)
{
	const struct mga_pins2 *pins = &mdev->pins.pins2;
#if 0
	u32 pins_option = 0x00004e01;
	u32 pins_mctlwtst = 0x00030101;
#else
	u32 pins_option = pins->option;
	u32 pins_mctlwtst = pins->mctlwtst;
#endif
	u32 option;

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~(MGA_OPTION_FBMSKN | MGA_OPTION_HARDPWMSK);
	option |= pins_option & (MGA_OPTION_FBMSKN | MGA_OPTION_HARDPWMSK);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	mga_write32(mdev, MGA_PLNWT, 0x00000000);
	mga_write32(mdev, MGA_PLNWT, 0xffffffff);

	/* Step 2. */
	mga_write32(mdev, MGA_MCTLWTST, pins_mctlwtst);

	/* Step 3. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_MEMCONFIG_OLD;
	option |= pins_option & MGA_OPTION_MEMCONFIG_OLD;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* Step 4. */
	udelay(200);

	/* Step 5. */
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_MEMRESET);

	/* Step 6. */
	udelay(100); /* min = 100 * MCLK period */

	/* Step 7. */
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_MEMRESET | MGA_MACCESS_JEDECRST);

	/* Step 8. */
	mga_1064sg_rfhcnt(mdev, mdev->mclk);
}

static void mga_1064sg_powerup(struct mga_dev *mdev, bool mem_reset)
{
	const struct mga_pins2 *pins = &mdev->pins.pins2;
#if 0
	u32 pins_option = 0x00004e01;
	unsigned int pins_gclk = 60000;
#else
	u32 pins_option = pins->option;
	unsigned int pins_gclk = pins->clk_2d.gclk[0 >> 2];
#endif
	unsigned int gclk, mclk, syspll = 0;
	unsigned int syspll_fvco_max;
	int ret;

	switch (pins_option & MGA_OPTION_SYSCLKSL) {
	case MGA_OPTION_SYSCLKSL_PCI:
		/* FIXME? */
		gclk = mclk = 33333;
		break;
	case MGA_OPTION_SYSCLKSL_SYSPLL:
		gclk = pins_gclk;
		/* mclk calculated later */
		break;
	case MGA_OPTION_SYSCLKSL_MCLK:
		/* FIXME? */
		gclk = mclk = 0;
		break;
	default:
		BUG();
	}

	if ((pins_option & MGA_OPTION_SYSCLKSL) == MGA_OPTION_SYSCLKSL_SYSPLL) {
		if (pins_option & MGA_OPTION_GCLKDIV)
			syspll = pins_gclk;
		else
			syspll = pins_gclk * 3;

		if (pins_option & MGA_OPTION_MCLKDIV)
			mclk = syspll;
		else
			mclk = div_round(syspll, 2);
	}

	dev_dbg(mdev->dev, "SYSPLL = %u kHz\n", syspll);
	dev_dbg(mdev->dev, "  GCLK = %u kHz\n", gclk);
	dev_dbg(mdev->dev, "  MCLK = %u kHz\n", mclk);

	/* 1064SG BIOS overclocks SYSPLL. Follow suit. */
	syspll_fvco_max = pins->fvco_max;
	if (mdev->chip == MGA_CHIP_1064SG && pins->fvco_max < syspll)
		syspll_fvco_max = syspll;

	mga_dac_init(mdev, pins->fref,
		     pins->fvco_max, syspll_fvco_max);

	ret = sysclk_program(mdev, syspll, pins_option);
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
	}

	if (mem_reset) {
		mga_1064sg_rfhcnt(mdev, 0);//
		udelay(200);//
		mga_1064sg_mem_reset(mdev);
	}

	mdev->maccess = MGA_MACCESS_JEDECRST;
}

#if 0
{
	// iospace=0
	// memspace=0
	// get io+mem aprtures
	// iospace=old
	// memspace=old

	get_pins();

	if (1064SG) {
		pclkmax24 = 158000;
		pclkmax32 = 136000;
	} else {
		pclkmax24 = 192000;
		pclkmax32 = 144000;
	}
}
#endif

static void mga_1064sg_init(struct mga_dev *mdev)
{
	struct drm_display_mode mode = { .vrefresh = 0 };
	unsigned int outputs = 0;
	u32 option;

	mode = mode_640_480_60;
	mode = mode_800_600_60;
	//mode = mode_1024_768_60;
	//mode = mode_1280_1024_60;
	//mode.htotal = 1280;

	outputs |= MGA_OUTPUT_DAC1;

	mga_set_initial_crtc1_mode(mdev,
				   mdev->pixel_format,
				   &mode,
				   outputs);

	mga_misc_disable_vga(mdev);

	dev_dbg(mdev->dev, "Disabling CRTC1\n");
	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);
	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	//mga_dac_init(mdev);

	mga_misc_powerup(mdev);

	mga_dac_powerup(mdev);

	mga_1064sg_powerup(mdev, true);

	mga_crtc1_powerup(mdev);

	mga_i2c_dac_ddc1_init(mdev);

	mdev->outputs |= MGA_OUTPUT_DAC1;

	mga_probe_mem_size(mdev);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	if (mdev->splitmode)
		option |= MGA_OPTION_SPLITMODE;
	else
		option &= ~MGA_OPTION_SPLITMODE;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static void mga_1064sg_test(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");

	mga_enable_outputs(mdev);

	// fixme
	//if (mdev_outputs & MGA_OUTPUT_DAC1)
	//	mdev->funcs->monitor_sense_dac1(mdev);

	if (strstr(test, "reg"))
		mga_reg_test(mdev);

	mga_disable_outputs(mdev);
}

static int mga_1064sg_suspend(struct mga_dev *mdev)
{
	int ret;

	if (mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to power saving mode\n");

	/* slumber */
	mga_disable_outputs(mdev);

	mga_wait_dma_idle(mdev);
	mga_wait_dwgeng_idle(mdev);

	ret = sysclk_program(mdev, 6667, MGA_OPTION_MCLKDIV);
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		goto err;
	}

	mdev->suspended = true;

	dev_dbg(mdev->dev, "In power saving mode\n");

	return 0;

 err:
	mga_enable_outputs(mdev);

	return ret;
}

static int mga_1064sg_resume(struct mga_dev *mdev)
{
	if (!mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to normal mode\n");

	mga_1064sg_powerup(mdev, false);

	mga_enable_outputs(mdev);

	mdev->suspended = false;

	dev_dbg(mdev->dev, "In normal mode\n");

	return 0;
}

void mga_1064sg_monitor_sense(struct mga_dev *mdev)
{
	bool r, g, b;

	mga_crtc1_wait_vblank(mdev);
	mga_dac_monitor_sense_start(mdev);
	mga_crtc1_wait_vblank(mdev);
	mga_dac_monitor_sense_stop(mdev, &r, &g, &b);

	dev_dbg(mdev->dev, "Monitor sense = %s%s%s\n",
		r ? "R" : ".",
		g ? "G" : ".",
		b ? "B" : ".");
}

static const struct mga_chip_funcs mga_1064sg_funcs = {
	.rfhcnt = mga_1064sg_rfhcnt,
	.monitor_sense_dac1 = mga_1064sg_monitor_sense,
	.suspend = mga_1064sg_suspend,
	.resume = mga_1064sg_resume,
	.init = mga_1064sg_init,
	.set_mode = mga_1064sg_set_mode,
	.test = mga_1064sg_test,
	.softreset = mga_1064sg_softreset,
};

void mga_1064sg_prepare(struct mga_dev *mdev)
{
	mdev->funcs = &mga_1064sg_funcs;
}
