/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <stdlib.h>

#include "mga_dump.h"
#include "mga_dac.h"
#include "mga_regs.h"
#include "mga_i2c.h"
#include "modes.h"
#include "mga_flat.h"
#include "mga_tvout.h"

static unsigned int calc_mclk(unsigned int syspll,
			      u32 option, u32 option2)
{
	unsigned int mclk;

	switch (option & MGA_OPTION_SYSCLKSL) {
	case MGA_OPTION_SYSCLKSL_PCI:
		/* FIXME? */
		return 33333;
	case MGA_OPTION_SYSCLKSL_SYSPLL:
		mclk = syspll;
		break;
	case MGA_OPTION_SYSCLKSL_MCLK:
		/* FIXME? */
		return 0;
	default:
		BUG();
	}

	if (option2 & MGA_OPTION2_NOMCLKDIV)
		return mclk;
	else if (option & MGA_OPTION_MCLKDIV)
		return div_round(mclk * 2, 3);
	else
		return div_round(mclk * 1, 2);
}

#if 0
static void mga_g200_clock_powerup(struct mga_dev *mdev)
{
	// fIXME
	u32 option;
	u8 val;

	/* 1. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 2. */
	option |= MGA_OPTION_SYSCLKSL_SYSPLL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 3. */
	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 4. */
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXCLKCTRL);
	val = mga_read8(mdev, MGA_X_DATAREG);
	val |= MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 5. */
	val |= MGA_XPIXCLKCTRL_PIXCLKSEL_PIXPLL;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 6. */
	val &= ~MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);
}
#endif

static void sysclk_select(struct mga_dev *mdev, u32 sysclksl)
{
	u32 option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);

	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option &= ~MGA_OPTION_SYSCLKSL;
	option |= sysclksl & MGA_OPTION_SYSCLKSL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static int sysclk_program(struct mga_dev *mdev,
			  unsigned int syspllfo,
			  u32 option_clk,
			  u32 option2_clk)
{
	struct mga_dac_pll_settings syspll;
	unsigned int mclk;
	u32 option, option2;
	int ret;

	if (syspllfo) {
		ret = mga_dac_syspll_calc(mdev, syspllfo, 0, &syspll);
		if (ret)
			return ret;
	}

	mclk = calc_mclk(0, MGA_OPTION_SYSCLKSL_PCI, 0);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, MGA_OPTION_SYSCLKSL_PCI);
	mga_mclk_change_post(mdev, mclk);

	mga_dac_syspll_power(mdev, false);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_PLLSEL;
	option |= option_clk & MGA_OPTION_PLLSEL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	if (syspllfo) {
		mga_dac_syspll_power(mdev, true);
		ret = mga_dac_syspll_program(mdev, &syspll);
		if (ret) {
			mga_dac_syspll_power(mdev, false);
			return ret;
		}
	}

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~(MGA_OPTION_GCLKDIV | MGA_OPTION_MCLKDIV);
	option |= option_clk & (MGA_OPTION_GCLKDIV | MGA_OPTION_MCLKDIV);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option2 = pci_cfg_read32(&mdev->pdev, MGA_OPTION2);
	option2 &= ~(MGA_OPTION2_NOGCLKDIV | MGA_OPTION2_NOMCLKDIV |
		     MGA_OPTION2_NOWCLKDIV | MGA_OPTION2_WCLKDIV);
	option2 |= option2_clk & (MGA_OPTION2_NOGCLKDIV | MGA_OPTION2_NOMCLKDIV |
				  MGA_OPTION2_NOWCLKDIV | MGA_OPTION2_WCLKDIV);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION2, option2);

	mclk = calc_mclk(syspll.fo, option_clk, option2_clk);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, option_clk);
	mga_mclk_change_post(mdev, mclk);

	if (syspllfo)
		mdev->sysclk_plls |= MGA_PLL_SYSPLL;
	else
		mdev->sysclk_plls &= ~MGA_PLL_SYSPLL;

	return 0;
}

static void mga_g200_mem_reset(struct mga_dev *mdev);

static void mga_g200_powerup(struct mga_dev *mdev, bool mem_reset)
{
	const struct mga_pins3 *pins = &mdev->pins.pins3;
#if 0
#if 0
	/* g200 sd pci, p&d agp, marvel agp */
	u32 pins_option  = 0x00001001;
	u32 pins_option2 = 0x00008000;
	unsigned int pins_gclk = 62000;
#endif
#if 0
	/* g200 mms */
	u32 pins_option  = 0x00005401;
	u32 pins_option2 = 0x00008000;
	unsigned int pins_gclk = 62000;
#endif
#else
	u32 pins_option = pins->clk_2d.option;
	u32 pins_option2 = pins->clk_2d.option2;
	unsigned int pins_gclk = pins->clk_2d.gclk[0 >> 1];
#endif
	unsigned int gclk, mclk, wclk, syspll = 0;
	int ret;

	switch (pins_option & MGA_OPTION_SYSCLKSL) {
	case MGA_OPTION_SYSCLKSL_PCI:
		/* FIXME? */
		gclk = mclk = wclk = 33333;
		break;
	case MGA_OPTION_SYSCLKSL_SYSPLL:
		gclk = pins_gclk;
		/* mclk and wclk calculated later */
		break;
	case MGA_OPTION_SYSCLKSL_MCLK:
		/* FIXME? */
		gclk = mclk = wclk = 0;
		break;
	default:
		BUG();
	}

	if ((pins_option & MGA_OPTION_SYSCLKSL) == MGA_OPTION_SYSCLKSL_SYSPLL) {
		if (pins_option2 & MGA_OPTION2_NOGCLKDIV)
			syspll = div_round(pins_gclk * 1, 1);
		else if (pins_option & MGA_OPTION_GCLKDIV)
			syspll = div_round(pins_gclk * 3, 2);
		else
			syspll = div_round(pins_gclk * 2, 1);

		if (pins_option2 & MGA_OPTION2_NOMCLKDIV)
			mclk = div_round(syspll * 1, 1);
		else if (pins_option & MGA_OPTION_MCLKDIV)
			mclk = div_round(syspll * 2, 3);
		else
			mclk = div_round(syspll * 1, 2);

		if (pins_option2 & MGA_OPTION2_NOWCLKDIV)
			wclk = div_round(syspll * 1, 1);
		else if (pins_option2 & MGA_OPTION2_WCLKDIV)
			wclk = div_round(syspll * 2, 3);
		else
			wclk = div_round(syspll * 1, 2);
	}

	dev_dbg(mdev->dev, "SYSPLL = %u kHz\n", syspll);
	dev_dbg(mdev->dev, "  GCLK = %u kHz\n", gclk);
	dev_dbg(mdev->dev, "  MCLK = %u kHz\n", mclk);
	dev_dbg(mdev->dev, "  WCLK = %u kHz\n", wclk);

	mga_dac_init(mdev, pins->fref,
		     pins->fvco_max, pins->fvco_max);

	ret = sysclk_program(mdev, syspll, pins_option, pins_option2);
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		return;
	}

	if (mem_reset) {
		mga_g100_rfhcnt(mdev, 0);//
		udelay(200);//
		mga_g200_mem_reset(mdev);
	}
}

static void mga_g200_mem_reset(struct mga_dev *mdev)
{
	const struct mga_pins3 *pins = &mdev->pins.pins3;
#if 0
#if 0
	/* g200 sd pci, p&d agp, marvel agp */
	u32 pins_mctlwtst = 0x04244ca1;
	u32 pins_option  = 0x00001001;
	u32 pins_option2 = 0x00008000;
	u32 pins_memrdbk = 0x00000108;
#endif
#if 0
	/* g200 mms */
	u32 pins_mctlwtst = 0x042450a1;
	u32 pins_option  = 0x00005401;
	u32 pins_option2 = 0x00008000;
	u32 pins_memrdbk = 0x00000000;
#endif
#else
	u32 pins_mctlwtst = pins->mctlwtst;
	u32 pins_option = pins->clk_2d.option;
	u32 pins_option2 = pins->clk_2d.option2;
	u32 pins_memrdbk = pins->memrdbk;
#endif
	u32 option, option2, memrdbk;

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~(MGA_OPTION_HARDPWMSK | MGA_OPTION_ENHMEMACC);
	option |= pins_option & (MGA_OPTION_HARDPWMSK | MGA_OPTION_ENHMEMACC);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	mga_write32(mdev, MGA_PLNWT, 0x00000000);
	mga_write32(mdev, MGA_PLNWT, 0xffffffff);

	/* Step 2. */
	mga_write32(mdev, MGA_MCTLWTST, pins_mctlwtst);

	/* Step 3. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_MEMCONFIG_NEW;
	option |= pins_option & MGA_OPTION_MEMCONFIG_NEW;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* Step 4.*/
	option2 = pci_cfg_read32(&mdev->pdev, MGA_OPTION2);
	option2 &= ~MGA_OPTION2_MBUFTYPE;
	option2 |= pins_option2 & MGA_OPTION2_MBUFTYPE;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION2, option2);

	/* Step 5.*/
	memrdbk = mga_read32(mdev, MGA_MEMRDBK);
	memrdbk &= ~MGA_MEMRDBK_MRSOPCOD;
	mga_write32(mdev, MGA_MEMRDBK, memrdbk);

	/* Step 6.*/
	memrdbk = mga_read32(mdev, MGA_MEMRDBK);
	memrdbk &= ~(MGA_MEMRDBK_MCLKBRD0 |
		     MGA_MEMRDBK_MCLKBRD1 |
		     MGA_MEMRDBK_STRMFCTL);
	memrdbk |= pins_memrdbk;
	mga_write32(mdev, MGA_MEMRDBK, memrdbk);

	/* Step 7. */
	udelay(200);

	/* Step 8. */
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_MEMRESET);

	/* Step 9. */
	mga_g100_rfhcnt(mdev, mdev->mclk);
}

static void probe_addons(struct mga_dev *mdev)
{
	const struct mga_pins3 *pins = &mdev->pins.pins3;
	int ret;

	mdev->outputs |= MGA_OUTPUT_DAC1;

	if (pins->features & _MGA_FEAT_TVO_BUILTIN)
		mdev->features |= MGA_FEAT_TVO_BUILTIN;
	if (pins->features & _MGA_FEAT_DVD_BUILTIN)
		mdev->features |= MGA_FEAT_DVD_BUILTIN;
	if (pins->features & _MGA_FEAT_MJPEG_BUILTIN)
		mdev->features |= MGA_FEAT_MJPEG_BUILTIN;
	if (pins->features & _MGA_FEAT_VIN_BUILTIN)
		mdev->features |= MGA_FEAT_VIN_BUILTIN;
	if (pins->features & _MGA_FEAT_TUNER_BUILTIN)
		mdev->features |= MGA_FEAT_TUNER_BUILTIN;
	if (pins->features & _MGA_FEAT_AUDIO_BUILTIN)
		mdev->features |= MGA_FEAT_AUDIO_BUILTIN;
	if (pins->features & _MGA_FEAT_TMDS_BUILTIN)
		mdev->features |= MGA_FEAT_TMDS_BUILTIN;

	dev_dbg(mdev->dev, "Before probe:\n");
	mga_print_features(mdev);

	if (mdev->features & MGA_FEAT_TVO_BUILTIN) {
		/* Mystique, Marvel */
		ret = mga_tvout_builtin_probe(mdev);
		if (ret)
			dev_err(mdev->dev, "Failed to locate builtin TV-out\n");
	} else if (!(mdev->features & MGA_FEAT_TMDS_BUILTIN)) {
		/* No TV-out addon for Mystique, Marvel, MMS, P&D */
		ret = mga_tvout_addon_probe(mdev);
		if (ret)
			dev_dbg(mdev->dev, "Failed to locate TV-out addon\n");
	}

	if (mdev->features & MGA_FEAT_TMDS_BUILTIN) {
		/* MMS, P&D */
		ret = mga_tmds_builtin_probe(mdev);
		if (ret)
			dev_err(mdev->dev, "Failed to locate builtin flat panel\n");
	} else if (!(mdev->features & (MGA_FEAT_TVO_BUILTIN | MGA_FEAT_TVO_ADDON))) {
		/*
		 * No flat panel addon for Mystique, Marvel, MMS, P&D.
		 * Only one addon can be present.
		 */
		ret = mga_tmds_addon_probe(mdev);
		if (ret)
			dev_dbg(mdev->dev, "Failed to locate flat panel addon\n");
	}

	dev_dbg(mdev->dev, "After probe:\n");
	mga_print_features(mdev);
}

static void mga_g200_init(struct mga_dev *mdev)
{
	const struct mga_pins3 *pins = &mdev->pins.pins3;
	struct drm_display_mode mode = { .vrefresh = 0 };
	unsigned int outputs = 0;

	mode = mode_640_480_60;

#if 0
	mode = mode_800_600_60;
	mode.hdisplay = 800;
	mode.hsync_start = 872;
	mode.hsync_width = 88;
	mode.htotal = 1128;
	mode.vdisplay = 600;
	mode.vsync_start = 668;
	mode.vsync_width = 4;
	mode.vtotal = 780;
	mode.htotal = 1200;
	mode.hsync_start = (mode.htotal + mode.hdisplay) / 2;
#endif
#if 0
	if (mode.vtotal < 525) {
		mode.vsync_start += (525 - mode.vtotal) >> 1;
		mode.vsync_start++;
		mode.vtotal = 525;
	}
#endif
#if 1
	if (mode.vtotal < 625) {
		mode.vsync_start += (625 - mode.vtotal) >> 1;
		mode.vsync_start++;
		mode.vtotal = 625;
	}
#endif

	outputs |= MGA_OUTPUT_DAC1;
	//outputs |= MGA_OUTPUT_TMDS1;
	//outputs |= MGA_OUTPUT_TVOUT;

	mga_set_initial_crtc1_mode(mdev,
				   mdev->pixel_format,
				   &mode,
				   outputs);

	mga_misc_disable_vga(mdev);

	dev_dbg(mdev->dev, "Disabling CRTC1\n");
	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);
	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	//mga_dac_init(mdev);

	mga_misc_powerup(mdev);

	mga_dac_powerup(mdev);

	mga_g200_powerup(mdev, true);

	mga_crtc1_powerup(mdev);

	/* Initialize I2C */
	mga_i2c_dac_misc_init(mdev);

	/*
	 * For some reason MMS board advertises builtin DVD
	 * even though such a thing doesn't exist. So let's use
	 * that to detect the MMS board.
	 */
	/*
	 * FIXME MMS and 16MB board both use the special DDC
	 * pin arrangement. Find out if there's a better way
	 * to detect this.
	 */
	if (mdev->features & MGA_FEAT_DVD_BUILTIN ||
	    pins->mem.base_size == 16 << 20)
		mga_i2c_dac_ddc12_init(mdev);
	else
		mga_i2c_dac_ddc1_init(mdev);

	probe_addons(mdev);

	mga_probe_mem_size(mdev);
}

static void mga_g200_test(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");

	mga_enable_outputs(mdev);

	// fixme
	//if (mdev_outputs & MGA_OUTPUT_DAC1)
	//	mdev->funcs->monitor_sense_dac1(mdev);

	if (strstr(test, "reg"))
		mga_reg_test(mdev);
	if (strstr(test, "tvo") &&
	    mdev->features & (MGA_FEAT_TVO_BUILTIN | MGA_FEAT_TVO_ADDON))
		mga_tvo_test(&mdev->tvodev);

	mga_disable_outputs(mdev);
}

static int mga_g200_suspend(struct mga_dev *mdev)
{
	u32 option;
	int ret;

	if (mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to power saving mode\n");

	/* slumber */
	mga_disable_outputs(mdev);

	mga_wait_dma_idle(mdev);
	mga_wait_dwgeng_idle(mdev);

	/* Preserve pllsel */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= MGA_OPTION_PLLSEL;
	option |= MGA_OPTION_SYSCLKSL_SYSPLL | MGA_OPTION_MCLKDIV;

	ret = sysclk_program(mdev, 6667, option, 0);
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		goto err;
	}

	mdev->suspended = true;

	dev_dbg(mdev->dev, "In power saving mode\n");

	return 0;

 err:
	mga_enable_outputs(mdev);

	return ret;
}

static int mga_g200_resume(struct mga_dev *mdev)
{
	if (!mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to normal mode\n");

	mga_g200_powerup(mdev, false);

	mga_enable_outputs(mdev);

	mdev->suspended = false;

	dev_dbg(mdev->dev, "In normal mode\n");

	return 0;
}

static const struct mga_chip_funcs mga_g200_funcs = {
	.rfhcnt = mga_g100_rfhcnt,
	.monitor_sense_dac1 = mga_1064sg_monitor_sense,
	.suspend = mga_g200_suspend,
	.resume = mga_g200_resume,
	.init = mga_g200_init,
	.set_mode = mga_1064sg_set_mode,
	.test = mga_g200_test,
	.softreset = mga_1064sg_softreset,
};

void mga_g200_prepare(struct mga_dev *mdev)
{
	mdev->funcs = &mga_g200_funcs;
}
