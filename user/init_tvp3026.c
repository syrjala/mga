/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <limits.h>

#include "mga_dump.h"
#include "tvp3026_regs.h"

enum {
	/* FIXME ? */
	TVP_PLL_TIMEOUT = 50, /* 5 ms typical */
};

enum {
	TVP_FMT_CI_8      = 0x00,
	TVP_FMT_RGB_664   = 0x01,
	TVP_FMT_RGB_565   = 0x02,
	TVP_FMT_RGB_888   = 0x03,
	TVP_FMT_BGR_888   = 0x04,
	TVP_FMT_RGBX_4444 = 0x05,
	TVP_FMT_XRGB_1555 = 0x06,
	TVP_FMT_XRGB_8888 = 0x07,
	TVP_FMT_BGRX_8888 = 0x08,

	TVP_FMT_PALETTE   = 0x10,
	TVP_FMT_OVERLAY   = 0x20,

	TVP_CURSOR_DISABLE = 0x0,
	TVP_CURSOR_3COLOR  = 0x1,
	TVP_CURSOR_XGA     = 0x2,
	TVP_CURSOR_XWIN    = 0x3,
};

static int tvp_check_format(u8 format)
{
	bool palette = format & TVP_FMT_PALETTE;
	bool overlay = format & TVP_FMT_OVERLAY;

	format &= ~(TVP_FMT_PALETTE | TVP_FMT_OVERLAY);

	switch (format) {
	case TVP_FMT_CI_8:
		if (overlay || !palette)
			return -EINVAL;
		break;
	case TVP_FMT_RGB_664:
	case TVP_FMT_RGB_565:
	case TVP_FMT_RGB_888:
	case TVP_FMT_BGR_888:
		if (overlay)
			return -EINVAL;
		break;
	case TVP_FMT_RGBX_4444:
	case TVP_FMT_XRGB_1555:
	case TVP_FMT_XRGB_8888:
	case TVP_FMT_BGRX_8888:
		if (overlay && palette)
			return -EINVAL;
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

/* default to safe TVP3026-135 limits */
static const struct tvp_pll_info lclkpll_info = {
	.name = "LCLK PLL",
	.xplldata = TVP_XLCLKPLLDATA,

	.fref = 14318,
	.fpll_max = 85000,
	.fvco_min = 110000,
	.fvco_max = 220000,
};
static const struct tvp_pll_info pclkpll_info = {
	.name = "PCLK PLL",
	.xplldata = TVP_XPCLKPLLDATA,

	.fref = 14318,
	.fpll_max = 135000,
	.fvco_min = 110000,
	.fvco_max = 220000,
};
static const struct tvp_pll_info mclkpll_info = {
	.name = "MCLK PLL",
	.xplldata = TVP_XMCLKPLLDATA,

	.fref = 14318,
	.fpll_max = 100000,
	.fvco_min = 110000,
	.fvco_max = 220000,
};

static void tvp_write8(struct tvp_dev *tdev, unsigned int reg, u8 val)
{
	writeb(val, tdev->mmio_virt + reg);
}

static u8 tvp_read8(struct tvp_dev *tdev, unsigned int reg)
{
	return readb(tdev->mmio_virt + reg);
}

static void tvp_x_write8(struct tvp_dev *tdev, unsigned int reg, u8 val)
{
	tvp_write8(tdev, TVP_X_INDEXREG, reg);
	tvp_write8(tdev, TVP_X_DATAREG, val);
}

static u8 tvp_x_read8(struct tvp_dev *tdev, unsigned int reg)
{
	tvp_write8(tdev, TVP_X_INDEXREG, reg);
	return tvp_read8(tdev, TVP_X_DATAREG);
}

int tvp_mclkpll_calc(struct tvp_dev *tdev,
		     unsigned int freq, unsigned int fmax,
		     struct tvp_pll_settings *pll);

static int mclk_init(struct tvp_dev *tdev, unsigned int mclk)
{
	struct tvp_pll_settings pll;
	int ret;

	ret = tvp_mclkpll_calc(tdev, mclk, 0, &pll);
	if (ret)
		return ret;

	tvp_mclkpll_program_safe(tdev, &pll);

	return 0;
}

int tvp_powerup(struct tvp_dev *tdev, unsigned int mclk)
{
	int ret;

	//test
	//mga_crtc1_select_pixpll(tdev->mdev, 0);

	ret = mclk_init(tdev, mclk);
	if (ret)
		return ret;

	/* Disable SCLK, VCLK and dot clock. */
	tvp_x_write8(tdev, TVP_XCLKSEL, TVP_XCLKSEL_VCLK_DIS | TVP_XCLKSEL_DOTCLK_DIS);

	/* Disable LCLK and PCLK PLLs. */
	tvp_x_write8(tdev, TVP_XPLLADD, TVP_XPLLADD_PLLP);

	tvp_x_write8(tdev, TVP_XLCLKPLLDATA, TVP_LCLKPLLP_RESERVED);

	tvp_x_write8(tdev, TVP_XPCLKPLLDATA, TVP_PLLP_RESERVED);

	//mga_crtc1_select_pixpll(tdev->mdev, 3);

	/* DAC power down */
	tvp_x_write8(tdev, TVP_XMISCCTRL, TVP_XMISCCTRL_VGA8DAC | TVP_XMISCCTRL_DAC86DIS | TVP_XMISCCTRL_DACPD);

	/* Sense comparator power down */
	tvp_x_write8(tdev, TVP_XSENSETEST, TVP_XSENSETEST_SENSEPD);

	/* One time init type stuff */
	tvp_x_write8(tdev, TVP_XGENCTRL, 0x00);

	tvp_x_write8(tdev, TVP_XCURCTRL, TVP_XCURCTRL_VBL_4096);

	tvp_x_write8(tdev, TVP_XCOLKEYCTRL, 0x00);

	tvp_x_write8(tdev, TVP_XCOLKEYOVRL, 0xff);
	tvp_x_write8(tdev, TVP_XCOLKEYOVRH, 0xff);

	tvp_x_write8(tdev, TVP_XCOLKEYREDL, 0xff);
	tvp_x_write8(tdev, TVP_XCOLKEYREDH, 0xff);

	tvp_x_write8(tdev, TVP_XCOLKEYGREENL, 0xff);
	tvp_x_write8(tdev, TVP_XCOLKEYGREENH, 0xff);

	tvp_x_write8(tdev, TVP_XCOLKEYBLUEL, 0xff);
	//test hw bug (bits 7:5 must match 7:5 or TVP_XCOLKEYCTRL?
	tvp_x_write8(tdev, TVP_XCOLKEYBLUEH, 0x00);

	tvp_x_write8(tdev, TVP_XGENIOCTRL, 0x00);

	tvp_x_write8(tdev, TVP_XGENIODATA, 0x00);

	tvp_write8(tdev, TVP_PIXRDMSK, 0xFF);

	return 0;
}

void tvp_dotclock_enable(struct tvp_dev *tdev, bool enable)
{
	u8 val;

	val = tvp_x_read8(tdev, TVP_XCLKSEL);

	val &= ~TVP_XCLKSEL_DOTCLK;

	if (enable)
		val |= TVP_XCLKSEL_DOTCLK_PCLKPLL;
	else
		val |= TVP_XCLKSEL_DOTCLK_DIS;

	tvp_write8(tdev, TVP_X_DATAREG, val);
}

void tvp_dac_power(struct tvp_dev *tdev, bool enable)
{
	u8 val;

	val = tvp_x_read8(tdev, TVP_XMISCCTRL);

	if (enable)
		val &= ~TVP_XMISCCTRL_DACPD;
	else
		val |= TVP_XMISCCTRL_DACPD;

	tvp_write8(tdev, TVP_X_DATAREG, val);
}

#if 0
static void tvp_vga_mode(struct tvp_dev *tdev)
{
	// missing
	//XCOLKEYCTRL=0x00;//?

	tvp_x_write8(tdev, TVP_XCLKSEL, TVP_XCLKSEL_VCLK_DIS | TVP_XCLKSEL_DOTCLK_CLK0_CLK0);

	tvp_x_write8(tdev, TVP_XTRUECOLORCTRL, TVP_XTRUECOLORCTRL_PSEUDOCOLOR);

	tvp_x_write8(tdev, TVP_XMULCTRL, TVP_XMULCTRL_VGA);

	// -> P
	//LCLK_PLLEN=0;
	//PCLK_PLLEN=0;

	//PLLSEL(1,1) = 11;
	//PLLSEL(1,1) = 00; or PLLSEL(1,1) = 01;

	tvp_x_write8(tdev, TVP_XMCLKLCLKCTRL,
		     TVP_XMCLKLCLKCTRL_RCLK_PCLKPLL | TVP_XMCLKLCLKCTRL_MCLK_MCLKPLL | TVP_XMCLKLCLKCTRL_MCLK_STROBE);
}
#endif

#if 0
void tvp_pclk()
{
	unsigned int fvco;
	unsigned int in, feed, post;

	fvco = PLL_calcclock(freq, fmax, &in, &feed, &post);
	fvco >>= post;

	in = 64 - lin;
	feed = 64 - lfeed;

	clk[0] = n | 0xC0;
	clk[1] = m;
	clk[2] = p | 0xB0;

	return fvco;
}

{
	1 <= N(5-0) <= 62;
	N |= 0xC0;
	1 <= M(5-0) <= 62;
	M |= LES1 | LES0;
	1 <= P(1,0) <= 3;
	if (24bpp)
		P |= LESEN;
	P |= PLLEN;
	P |= 0x70;
}

non_24(void)
{
	Fl = Fd * B / W;
	Fr = K * Fd * B / W;
	N = 65 - 4 * W / B;
	M = 61;
	Z = (1 << (P+1)) * (Q + 1);
	Z = Fvco * (65 - N) / (4 * Fd * K);

	LES0=0;
	LES1=0;
	LESEN=0;
	PLLEN=1;
}
#endif

#if 0
void tvp_reset(struct tvp_dev *tdev, bool reinit)
{
	//mga_crtc1_select_pixpll(tdev->mdev, 0);

	tvp_write8(tdev, TVP_X_INDEXREG, TVP_XRESET);
	tvp_write8(tdev, TVP_X_DATAREG, 0x00);

	if (!reinit)
		return;

	// program pix pll

	tvp_write8(tdev, TVP_X_INDEXREG, TVP_XCLKSEL);
	tvp_write8(tdev, TVP_X_DATAREG, TVP_XCLKSEL_VCLK_DIS | TVP_XCLKSEL_DOTCLK_PCLKPLL);
}
#endif

int tvp_init(struct tvp_dev *tdev,
	     struct device *dev,
	     void __iomem *mmio_virt,
	     unsigned int fref,
	     unsigned int fvco_min,
	     unsigned int fvco_max,
	     unsigned int rclk_max,
	     unsigned int pclk_max,
	     unsigned int mclk_max)
{
	u8 val;

	tdev->mmio_virt = mmio_virt;
	tdev->dev = dev;

	/* Make sure we're looking at a TVP3026. */
	val = tvp_x_read8(tdev, TVP_XID);
	if (val != TVP_XID_TVP3026)
		return -ENODEV;

	tdev->palette = true;
	tdev->ovl_src_ckey = false;
	tdev->ovl_dst_ckey = false;
	tdev->pedestal = false;
	tdev->interlace = false;

	tdev->rev = tvp_x_read8(tdev, TVP_XREVISION);

	/* copy defaults */
	tdev->lclkpll_info = lclkpll_info;
	tdev->pclkpll_info = pclkpll_info;
	tdev->mclkpll_info = mclkpll_info;

	if (fref) {
		tdev->lclkpll_info.fref = fref;
		tdev->pclkpll_info.fref = fref;
		tdev->mclkpll_info.fref = fref;
	}
	if (fvco_min) {
		tdev->lclkpll_info.fvco_min = fvco_min;
		tdev->pclkpll_info.fvco_min = fvco_min;
		tdev->mclkpll_info.fvco_min = fvco_min;
	}
	if (fvco_max) {
		tdev->lclkpll_info.fvco_max = fvco_max;
		tdev->pclkpll_info.fvco_max = fvco_max;
		tdev->mclkpll_info.fvco_max = fvco_max;
	}
	if (rclk_max)
		tdev->lclkpll_info.fpll_max = rclk_max;
	if (pclk_max)
		tdev->pclkpll_info.fpll_max = pclk_max;
	if (mclk_max)
		tdev->mclkpll_info.fpll_max = mclk_max;

	return 0;
}


#if 0
static void tvp_vga_mode()
{
	loop_PLLEN = 0;
	pixel_PLLEN = 0;
	PLLSEL = 1x;
	PLLSEL = 00; or 01;
}
#endif

#if 0
static void tvp_ext_mode(struct tvp_dev *tdev)
{
	/* Select PCLK PLL as clock source, disable VCLK output. */
	tvp_write8(tdev, TVP_X_INDEXREG, TVP_XCLKSEL);
	tvp_write8(tdev, TVP_X_DATAREG, TVP_XCLKSEL_VCLK_DIS | TVP_XCLKSEL_DOTCLK_PCLKPLL);

	/* Point to P registers. */
	tvp_write8(tdev, TVP_X_INDEXREG, TVP_XPLLADD);
	tvp_write8(tdev, TVP_X_DATAREG, TVP_XPLLADD_PLLP);

	/* Disable LCLK PLL. */
	tvp_write8(tdev, TVP_X_INDEXREG, TVP_XLCLKPLLDATA);
	tvp_write8(tdev, TVP_X_DATAREG, TVP_LCLKPLLP_RESERVED);

	/* Disable PCLK PLL. */
	tvp_write8(tdev, TVP_X_INDEXREG, TVP_XPCLKPLLDATA);
	tvp_write8(tdev, TVP_X_DATAREG, TVP_PLLP_RESERVED);
}
#endif

static unsigned int lclkpll_calc_fvco(unsigned int fdot,
				      u8 k, u8 n, u8 m, u8 p, u8 q)
{
	n &= TVP_PLLN_N;
	m &= TVP_PLLM_M;
	p &= TVP_PLLP_P;

	return div_round(fdot * k * (65 - m) * (q + 1) * (1 << (p + 1)), 65 - n);
}

static unsigned int lclkpll_calc_fpll(unsigned int fdot,
				      u8 k, u8 n, u8 m)
{
	n &= TVP_PLLN_N;
	m &= TVP_PLLM_M;

	return div_round(fdot * k * (65 - m), 65 - n);
}

/* Looks good */
int tvp_lclkpll_calc(struct tvp_dev *tdev,
		     unsigned int fdot,
		     unsigned int bpp,
		     enum tvp_bus_width bus_width,
		     struct tvp_pll_settings *pll)
{
	const struct tvp_pll_info *info = &tdev->lclkpll_info;
	unsigned int n, m, p, q, z;

	dev_dbg(tdev->dev, "%s Fdot = %u kHZ\n", info->name, fdot);
	dev_dbg(tdev->dev, "%s min Fvco = %u kHZ\n", info->name, info->fvco_min);
	dev_dbg(tdev->dev, "%s max Fvco = %u kHZ\n", info->name, info->fvco_max);
	dev_dbg(tdev->dev, "%s max Fpll = %u kHZ\n", info->name, info->fpll_max);

	switch (bus_width) {
	case TVP_BUS_WIDTH_32:
	case TVP_BUS_WIDTH_64:
		break;
	default:
		return -EINVAL;
	}

	switch (bpp) {
	case 8:
	case 16:
	case 24:
	case 32:
		break;
	default:
		return -EINVAL;
	}

	if (bpp == 24) {
		/* 4:3 or 8:3 */
		n = 65 - (32 << bus_width) / 8;
		m = 65 - 3;
	} else {
		/* 1:1, 2:1, 4:1 or 8:1 */
		n = 65 - 4 * (32 << bus_width) / bpp;
		m = 65 - 4;
	}

#if 0
	/*
	 * Target the maximum Fvco to minimize jitter and
	 * to make the division and ilog2() rounding simple.
	 */
	z = info->fvco_max * (65 - n) / (fdot * k * (65 - m));

	if (z <= 16) {
		p = ilog2(z) - 1;
		q = 0;
	} else {
		p = 3;
		q = z / 16 - 1;
	}
#else
	/* Target the minimum Fvco. */
	z = DIV_ROUND_UP(info->fvco_min * (65 - n), fdot * tdev->k * (65 - m));

	if (z < 2) {
		p = 0;
		q = 0;
	} else if (z < 16) {
		p = ilog2(z - 1);
		q = 0;
	} else {
		p = 3;
		q = DIV_ROUND_UP(z - 16, 16);
	}
#endif

	pll->fvco = lclkpll_calc_fvco(fdot, tdev->k, n, m, p, q);
	pll->fpll = lclkpll_calc_fpll(fdot, tdev->k, n, m);

	dev_dbg(tdev->dev, "%s K=%d N=%d M=%d P=%d Q=%d\n",
		info->name, tdev->k, n, m, p, q);
	dev_dbg(tdev->dev, "%s Fvco = %u kHz, Fpll = %u kHz\n",
		info->name, pll->fvco, pll->fpll);

	if (pll->fvco < info->fvco_min ||
	    pll->fvco > info->fvco_max ||
	    pll->fpll > info->fpll_max)
		return -EINVAL;

	BUG_ON(n < 1 || n > 62 || m < 1 || m > 62 || p > 3 || q > 7);

	pll->n = TVP_PLLN_RESERVED | n;
	pll->m = m;
	pll->p = TVP_PLLP_PLLEN | TVP_LCLKPLLP_RESERVED | p;
	pll->q = q;

	if (bpp == 24) {
		if (bus_width == TVP_BUS_WIDTH_64 ||
		    tdev->rev < TVP_XREVISION_REVB)
			pll->m |= TVP_LCLKPLLM_LES1;

		pll->p |= TVP_LCLKPLLP_LESEN;
	}

	return 0;
}

#if 0
void tvp_lclk(unsigned int f_pll)
{
	unsigned int feed, in, post, iv, z;
	unsigned int bpp;

	bpp = final_bppShift;

	if (bits_per_pixel == 24) {
		M = 3; /* set lm to any possible value */
		N = 3 * 32 / bpp;
	} else {
		M = 4;
		N = 4 * 32 / bpp;
	}

	Z = (110000 * N) / (f_pll * M);

	if (z < 2) {
		P = 0;
		Q = 0;
	} else if (z < 4) {
		P = 1;
		Q = 0;
	} else if (z < 8) {
		P = 2;
		Q = 0;
	} else {
		P = 3;
		Q = Z / 16;
	}

	if (bits_per_pixel == 24) {
		clk[3] = ((65 - N) & 0x3F) | 0xC0;
		clk[4] = (65 - M) | 0x80;
		if (rev >= TVP_XREVISION_REVB) {
			if (interleave) {
				reg[XLATCHCTRL] = TVP_XLATCHCTRL_8_3;
			} else {
				clk[4] &= ~0xC0;
				reg[XLATCHCTRL] = TVP_XLATCHCTRL_4_3;
			}
		} else {
			if (interleave) {
				; /* default... */
			} else {
				clk[4] ^= 0xC0;  /* change from 0x80 to 0x40 */
				reg[XLATCHCTRL] = TVP_XLATCHCTRL_4_3;
			}
		}
		clk[5] = post | 0xF8;
		if (24bpp_fix)
			clk[5] ^= 0x40;
	} else {
		clk[3] = ((65 - N) & 0x3F) | 0xC0;
		clk[4] = 65 - M;
		clk[5] = post | 0xF0;
	}
	reg[XMCLKLCLKCTRL] = TVP_XMCLKLCLKCTRL_RCLK_LCLKPLL | TVP_XMCLKLCLKCTRL_MCLK_MCLKPLL |
		TVP_XMCLKLCLKCTRL_MCLK_STROBE | div;
}
#endif

static unsigned int pll_calc_fvco(unsigned int fref, u8 n, u8 m)
{
	n &= TVP_PLLN_N;
	m &= TVP_PLLM_M;

	return div_round(8 * fref * (65 - m), 65 - n);
}

static unsigned int pll_calc_fpll(unsigned int fref, u8 n, u8 m, u8 p)
{
	n &= TVP_PLLN_N;
	m &= TVP_PLLM_M;
	p &= TVP_PLLP_P;

	return div_round(8 * fref * (65 - m), (65 - n) << p);
}

static unsigned int calc_diff(unsigned int a, unsigned int b)
{
	if (a > b)
		return a - b;
	else
		return b - a;
}

static int pll_calc(struct device *dev,
		    const struct tvp_pll_info *info,
		    unsigned int freq,
		    unsigned int fmax,
		    struct tvp_pll_settings *pll)
{
	unsigned int diff, best_diff = UINT_MAX;
	int m, n, p;

	if (fmax == 0 || fmax > info->fpll_max)
		fmax = info->fpll_max;

	dev_dbg(dev, "%s M: %d - %d\n", info->name, 1, 62);
	dev_dbg(dev, "%s N: %d - %d\n", info->name, 40, 62);
	dev_dbg(dev, "%s P: %d - %d\n", info->name, 0, 3);

	for (m = 62; m >= 1; m--) {
	for (n = 62; n >= 40; n--) {
	for (p = 0; p <= 3; p++) {
		unsigned int fvco, fpll;

		fvco = pll_calc_fvco(info->fref, n, m);
		if (fvco < info->fvco_min || fvco > info->fvco_max)
			continue;

		fpll = pll_calc_fpll(info->fref, n, m, p);
		if (fpll > fmax)
			continue;

		diff = calc_diff(fpll, freq);
		if (diff >= best_diff)
			continue;

		dev_dbg(dev, "%s found new best %u kHz (N=%d M=%d P=%d)\n",
			info->name, fpll, n, m, p);

		best_diff = diff;

		pll->fvco = fvco;
		pll->fpll = fpll;
		pll->n = n;
		pll->m = m;
		pll->p = p;
	}
	}
	}

	/* FIXME check that best_diff is within some tolerance? */
	if (best_diff == UINT_MAX)
		return -EINVAL;

	return 0;
}

static int tvp_pll_calc(struct device *dev,
			const struct tvp_pll_info *info,
			unsigned int freq,
			unsigned int fmax,
			struct tvp_pll_settings *pll)
{
	int ret;

	dev_dbg(dev, "%s target Fpll = %u kHZ\n", info->name, freq);
	dev_dbg(dev, "%s min Fvco = %u kHZ\n", info->name, info->fvco_min);
	dev_dbg(dev, "%s max Fvco = %u kHZ\n", info->name, info->fvco_max);
	dev_dbg(dev, "%s max Fpll = %u kHZ\n", info->name, info->fpll_max);

	ret = pll_calc(dev, info, freq, fmax, pll);
	if (ret)
		return ret;

	BUG_ON(pll->n < 40 || pll->n > 62 ||
	       pll->m <  1 || pll->m > 62 || pll->p > 3 ||
	       pll->fvco < info->fvco_min || pll->fvco > info->fvco_max ||
	       pll->fpll > info->fpll_max || pll->fpll > fmax);

	pll->n |= TVP_PLLN_RESERVED;
	pll->p |= TVP_PLLP_RESERVED | TVP_PLLP_PLLEN;

	dev_dbg(dev, "%s N=%02x M=%02x P=%02x\n",
		info->name, pll->n, pll->m, pll->p);
	dev_dbg(dev, "%s Fvco = %u kHz, Fpll = %u kHz\n",
		info->name, pll->fvco, pll->fpll);

	return 0;
}

int tvp_pclkpll_calc(struct tvp_dev *tdev,
		     unsigned int freq, unsigned int fmax,
		     struct tvp_pll_settings *pll)
{
	return tvp_pll_calc(tdev->dev, &tdev->pclkpll_info, freq, fmax, pll);
}

int tvp_mclkpll_calc(struct tvp_dev *tdev,
		     unsigned int freq, unsigned int fmax,
		     struct tvp_pll_settings *pll)
{
	return tvp_pll_calc(tdev->dev, &tdev->mclkpll_info, freq, fmax, pll);
}

static int pll_program(struct tvp_dev *tdev,
		       const struct tvp_pll_info *info,
		       const struct tvp_pll_settings *pll)
{
	unsigned long timeout;
	unsigned int i = 0;
	u8 val;

	dev_dbg(tdev->dev, "Programming %s: N=%02x M=%02x P=%02x\n",
		info->name, pll->n, pll->m, pll->p);

	/* Program the PLL. */
	tvp_x_write8(tdev, TVP_XPLLADD, TVP_XPLLADD_PLLN);
	/* writes to xplldata auto-increment XPLLADD */
	tvp_write8(tdev, TVP_X_INDEXREG, info->xplldata);
	tvp_write8(tdev, TVP_X_DATAREG, pll->n);
	tvp_write8(tdev, TVP_X_DATAREG, pll->m);
	tvp_write8(tdev, TVP_X_DATAREG, pll->p);

	if (!(pll->p & TVP_PLLP_PLLEN))
		return 0;

	/* Wait for the PLL to lock. */
	timeout = jiffies + msecs_to_jiffies(TVP_PLL_TIMEOUT);
	val = tvp_read8(tdev, TVP_X_DATAREG);
	while (!(val & TVP_PLLSTAT_PLLLOCK) && time_before(jiffies, timeout)) {
		i++;
		//msleep(1);
		val = tvp_read8(tdev, TVP_X_DATAREG);
	}

	timeout = jiffies_to_msecs(jiffies - timeout +
				   msecs_to_jiffies(TVP_PLL_TIMEOUT));

	if (!(val & TVP_PLLSTAT_PLLLOCK)) {
		dev_err(tdev->dev,
			"%s NOT locked after %lu ms (%u iterations)\n",
			info->name, timeout, i);
		return -ETIMEDOUT;
	}

	dev_dbg(tdev->dev, "%s locked after %u ms (%u iterations)\n",
		info->name, timeout, i);

	return 0;
}

int tvp_mclkpll_program(struct tvp_dev *tdev,
			const struct tvp_pll_settings *pll)
{
	return pll_program(tdev, &tdev->mclkpll_info, pll);
}

int tvp_pclkpll_program(struct tvp_dev *tdev,
			const struct tvp_pll_settings *pll)
{
	return pll_program(tdev, &tdev->pclkpll_info, pll);
}

int tvp_lclkpll_program(struct tvp_dev *tdev,
			const struct tvp_pll_settings *pll)
{
	u8 val;

	val = tvp_x_read8(tdev, TVP_XMCLKLCLKCTRL);

	val &= ~(TVP_XMCLKLCLKCTRL_Q | TVP_XMCLKLCLKCTRL_RCLK);
	val |= TVP_XMCLKLCLKCTRL_RCLK_LCLKPLL | pll->q;

	tvp_write8(tdev, TVP_X_DATAREG, val);

	dev_dbg(tdev->dev, "Programming %s: Q=%02x\n",
		tdev->lclkpll_info.name, pll->q);

	return pll_program(tdev, &tdev->lclkpll_info, pll);
}

static void pll_disable(struct tvp_dev *tdev,
			const struct tvp_pll_info *info)
{
	u8 val;

	dev_dbg(tdev->dev, "Disabling %s\n", info->name);

	/* Point to P registers. */
	tvp_x_write8(tdev, TVP_XPLLADD, TVP_XPLLADD_PLLP);

	val = tvp_x_read8(tdev, info->xplldata);
	val &= ~TVP_PLLP_PLLEN;
	tvp_write8(tdev, TVP_X_DATAREG, val);
}

void tvp_pclkpll_disable(struct tvp_dev *tdev)
{
	pll_disable(tdev, &tdev->pclkpll_info);
}

void tvp_lclkpll_disable(struct tvp_dev *tdev)
{
	pll_disable(tdev, &tdev->lclkpll_info);
}

void tvp_mclkpll_disable(struct tvp_dev *tdev)
{
	pll_disable(tdev, &tdev->mclkpll_info);
}

static void pll_save(struct tvp_dev *tdev,
		     const struct tvp_pll_info *info,
		     struct tvp_pll_settings *pll)
{
	tvp_x_write8(tdev, TVP_XPLLADD, TVP_XPLLADD_PLLN);
	pll->n = tvp_x_read8(tdev, info->xplldata);

	tvp_x_write8(tdev, TVP_XPLLADD, TVP_XPLLADD_PLLM);
	pll->m = tvp_x_read8(tdev, info->xplldata);

	tvp_x_write8(tdev, TVP_XPLLADD, TVP_XPLLADD_PLLN);
	pll->p = tvp_x_read8(tdev, info->xplldata);

	dev_dbg(tdev->dev, "Saved %s: N=%02x M=%02x P=%02x\n",
		info->name, pll->n, pll->m, pll->p);
}

static void mclkpll_save(struct tvp_dev *tdev,
			 struct tvp_pll_settings *pll)
{
	const struct tvp_pll_info *info = &tdev->mclkpll_info;

	pll_save(tdev, info, pll);

	pll->fvco = pll_calc_fvco(info->fref, pll->n, pll->m);
	pll->fpll = pll_calc_fpll(info->fref, pll->n, pll->m, pll->p);
}

static void pclkpll_save(struct tvp_dev *tdev,
			 struct tvp_pll_settings *pll)
{
	const struct tvp_pll_info *info = &tdev->pclkpll_info;

	pll_save(tdev, info, pll);

	pll->fvco = pll_calc_fvco(info->fref, pll->n, pll->m);
	pll->fpll = pll_calc_fpll(info->fref, pll->n, pll->m, pll->p);
}

static void lclkpll_save(struct tvp_dev *tdev,
			 struct tvp_pll_settings *pll)
{
	const struct tvp_pll_info *info = &tdev->lclkpll_info;
	u8 val;

	pll_save(tdev, info, pll);

	val = tvp_x_read8(tdev, TVP_XMCLKLCLKCTRL);
	pll->q = val & TVP_XMCLKLCLKCTRL_Q;

	dev_dbg(tdev->dev, "Saved %s: Q=%02x\n",
		tdev->lclkpll_info.name, pll->q);

	pll->fvco = lclkpll_calc_fvco(info->fref, tdev->k, pll->n, pll->m, pll->p, pll->q);
	pll->fpll = lclkpll_calc_fpll(info->fref, tdev->k, pll->n, pll->m);
}

/*
 * Programs the MCLK PLL to the desired frequency.
 *
 * Keeps MCLK output stable by using PCLK PLL as the
 * MCLK source during MCLK PLL programming.
 *
 * Since PCLK may be touched, video, hsync and vsync
 * should be disabled when calling this function.
 */
int tvp_mclkpll_program_safe(struct tvp_dev *tdev,
			     const struct tvp_pll_settings *pll)
{
	struct tvp_pll_settings pclk = { .m = 0 };
	u8 xclksel = 0;
	u8 val;

	/* Check which clock is driving MCLK. */
	val = tvp_x_read8(tdev, TVP_XMCLKLCLKCTRL);

	if (val & TVP_XMCLKLCLKCTRL_MCLK_MCLKPLL) {
		/* Save PCLK PLL settings. */
		pclkpll_save(tdev, &pclk);

		/* Save DOTCLK. */
		xclksel = tvp_x_read8(tdev, TVP_XCLKSEL);

		/* Disable DOTCLK. */
		tvp_write8(tdev, TVP_X_DATAREG,
		       (xclksel & ~TVP_XCLKSEL_DOTCLK) |
		       TVP_XCLKSEL_DOTCLK_DIS);

		/* Program PCLK PLL for MCLK frequency. */
		tvp_pclkpll_disable(tdev);
		tvp_pclkpll_program(tdev, pll);

		/* Select PCLK PLL as DOTCLK. */
		tvp_x_write8(tdev, TVP_XCLKSEL, (xclksel & ~TVP_XCLKSEL_DOTCLK) | TVP_XCLKSEL_DOTCLK_PCLKPLL);

		/* Switch MCLK terminal to DOTCLK. */
		val = tvp_x_read8(tdev, TVP_XMCLKLCLKCTRL);
		val &= ~TVP_XMCLKLCLKCTRL_MCLK_MCLKPLL;
		val &= ~TVP_XMCLKLCLKCTRL_MCLK_STROBE;
		tvp_write8(tdev, TVP_X_DATAREG, val);
		val |= TVP_XMCLKLCLKCTRL_MCLK_STROBE;
		tvp_write8(tdev, TVP_X_DATAREG, val);
	}

	/* Program MCLK PLL. */
	tvp_mclkpll_disable(tdev);
	tvp_mclkpll_program(tdev, pll);

	/* Switch MCLK terminal to MCLK PLL. */
	val = tvp_x_read8(tdev, TVP_XMCLKLCLKCTRL);
	val |= TVP_XMCLKLCLKCTRL_MCLK_MCLKPLL;
	val &= ~TVP_XMCLKLCLKCTRL_MCLK_STROBE;
	tvp_write8(tdev, TVP_X_DATAREG, val);
	val |= TVP_XMCLKLCLKCTRL_MCLK_STROBE;
	tvp_write8(tdev, TVP_X_DATAREG, val);

	if (pclk.m) {
		/* Disable DOTCLK. */
		tvp_x_write8(tdev, TVP_XCLKSEL, (xclksel & ~TVP_XCLKSEL_DOTCLK) | TVP_XCLKSEL_DOTCLK_DIS);

		/* Restore PCLK PLL. */
		tvp_pclkpll_disable(tdev);
		tvp_pclkpll_program(tdev, &pclk);

		/* Restore DOTCLK. */
		tvp_x_write8(tdev, TVP_XCLKSEL, xclksel);
	}

	return 0;
}

static unsigned int bpp_to_index(unsigned int bpp)
{
	switch (bpp) {
	default:
		BUG();
	case 8:
		return 0;
	case 15:
		return 1;
	case 16:
		return 2;
	case 24:
		return 3;
	case 32:
		return 4;
	}
}

void tvp_set_palette(struct tvp_dev *tdev)
{
	const struct drm_color *pal = tdev->pal;
	int i;

	/* FIXME is it always OK to access the palette RAM? */

	tvp_write8(tdev, TVP_PALWTADD, 0x00);

	for (i = 0; i < 256; i++) {
		tvp_write8(tdev, TVP_PALDATA, pal[i].r);
		tvp_write8(tdev, TVP_PALDATA, pal[i].g);
		tvp_write8(tdev, TVP_PALDATA, pal[i].b);
	}
}

void tvp_load_palette(struct tvp_dev *tdev,
		      const struct drm_color pal[256])
{
	int i;

	for (i = 0; i < 256; i++)
		tdev->pal[i] = pal[i];

	tvp_set_palette(tdev);
}

void tvp_program_format(struct tvp_dev *tdev,
			unsigned int pixel_format)
{
	u8 val;

	switch (pixel_format) {
	case DRM_FORMAT_C8:
		val = TVP_XTRUECOLORCTRL_PSEUDOCOLOR;
		break;
	case DRM_FORMAT_RGBX4444:
	case DRM_FORMAT_RGBA4444:
		val = TVP_XTRUECOLORCTRL_RGBX_4444;
		break;
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_ARGB1555:
		val = TVP_XTRUECOLORCTRL_XRGB_1555;
		break;
	case DRM_FORMAT_RGB565:
		val = TVP_XTRUECOLORCTRL_RGB_565;
		break;
#if 0
	case DRM_FORMAT_RGB664:
		val = TVP_XTRUECOLORCTRL_RGB_664;
		break;
#endif
	case DRM_FORMAT_RGB888:
		val = TVP_XTRUECOLORCTRL_RGB_888;
		break;
	case DRM_FORMAT_BGR888:
		val = TVP_XTRUECOLORCTRL_BGR_888;
		break;
	case DRM_FORMAT_XRGB8888:
	case DRM_FORMAT_ARGB8888:
		val = TVP_XTRUECOLORCTRL_XRGB_8888;
		break;
	default:
		BUG();
	}

	if (pixel_format != DRM_FORMAT_C8 && tdev->palette)
		val |= TVP_XTRUECOLORCTRL_TRUECOLOR;

	tvp_x_write8(tdev, TVP_XTRUECOLORCTRL, val);

	/* FIXME overlay stuff may need programming too */
}

void tvp_program(struct tvp_dev *tdev,
		 unsigned int pixel_format,
		 enum tvp_bus_width bus_width)
{
	unsigned int bpp = drm_format_depth(pixel_format);
	unsigned int bpp_index = bpp_to_index(bpp);
	unsigned int cpp = drm_format_plane_cpp(pixel_format, 0);
	u8 val;

	tvp_program_format(tdev, pixel_format);

	switch (cpp) {
	case 1:
		val = TVP_XMULCTRL_8BPP;
		break;
	case 2:
		val = TVP_XMULCTRL_16BPP;
		break;
	case 3:
	case 4:
		val = TVP_XMULCTRL_32BPP;
		break;
	default:
		BUG();
	}

	if (bus_width == TVP_BUS_WIDTH_64)
		val |= TVP_XMULCTRL_BUS_64BIT;
	else
		val |= TVP_XMULCTRL_BUS_32BIT;

	tvp_x_write8(tdev, TVP_XMULCTRL, val);

#if 0
	if (cpp == 3) {
		static const u8 latch_vals[][2] = {
			{
				[TVP_BUS_WIDTH_32] = TVP_XLATCHCTRL_4_3_REVA,
				[TVP_BUS_WIDTH_64] = TVP_XLATCHCTRL_8_3_REVA,
			},
			{
				[TVP_BUS_WIDTH_32] = TVP_XLATCHCTRL_4_3_REVB,
				[TVP_BUS_WIDTH_64] = TVP_XLATCHCTRL_8_3_REVB,
			},
		};

		val = latch_vals[tdev->rev >= TVP_XREVISION_REVB][bus_width];
	} else {
		/* FIXME is it OK? */
		//val = TVP_XLATCHCTRL_1_1;
		val = TVP_XLATCHCTRL_4_1;
	}
#else
	if (tdev->rev >= TVP_XREVISION_REVB && cpp == 3) {
		static const u8 latch_revb_24_vals[] = {
			[TVP_BUS_WIDTH_32] = TVP_XLATCHCTRL_4_3_REVB,
			[TVP_BUS_WIDTH_64] = TVP_XLATCHCTRL_8_3_REVB,
		};

		val = latch_revb_24_vals[bus_width];
	} else {
		static const u8 latch_vals[][5] = {
			[TVP_BUS_WIDTH_32] = {
				[1] = TVP_XLATCHCTRL_4_1,
				[2] = TVP_XLATCHCTRL_2_1,
				[3] = TVP_XLATCHCTRL_4_3_REVA,
				[4] = TVP_XLATCHCTRL_1_1,
			},
			[TVP_BUS_WIDTH_64] = {
				[1] = TVP_XLATCHCTRL_8_1,
				[2] = TVP_XLATCHCTRL_4_1,
				[3] = TVP_XLATCHCTRL_8_3_REVA,
				[4] = TVP_XLATCHCTRL_2_1,
			},
		};

		val = latch_vals[bus_width][cpp];
	}
#endif
	tvp_x_write8(tdev, TVP_XLATCHCTRL, val);

	bool ovl_enabled = (bpp == 15 || bpp == 32) &&
		!tdev->palette &&
		(tdev->ovl_src_ckey || tdev->ovl_dst_ckey);
	bool ovl_invert = true;

	val = tvp_x_read8(tdev, TVP_XMISCCTRL);
	if (bpp != 8 && !tdev->palette)
		val |= TVP_XMISCCTRL_PSELPOL;
	else
		val &= ~TVP_XMISCCTRL_PSELPOL;
	tvp_write8(tdev, TVP_X_DATAREG, val);

	val = tvp_x_read8(tdev, TVP_XCOLKEYCTRL);
	val &= ~(TVP_XCOLKEYCTRL_OCOMP | TVP_XCOLKEYCTRL_RCOMP |
		 TVP_XCOLKEYCTRL_GCOMP | TVP_XCOLKEYCTRL_BCOMP |
		 TVP_XCOLKEYCTRL_COLORKEYPOL);
	if (ovl_enabled) {
		if (!ovl_invert)
			val |= TVP_XCOLKEYCTRL_COLORKEYPOL;
		if (tdev->ovl_src_ckey)
			val |= TVP_XCOLKEYCTRL_OCOMP;
		if (tdev->ovl_dst_ckey)
			val |= TVP_XCOLKEYCTRL_RCOMP |
				TVP_XCOLKEYCTRL_GCOMP |
				TVP_XCOLKEYCTRL_BCOMP;
	}
	tvp_write8(tdev, TVP_X_DATAREG, val);
}

void tvp_stop(struct tvp_dev *tdev)
{
	tvp_lclkpll_disable(tdev);
	tvp_pclkpll_disable(tdev);

	//mga_crtc1_select_pixpll(tdev->mdev, 3);

	tvp_dac_power(tdev, false);
	tvp_dotclock_enable(tdev, false);
}

static void tvp_overlay_src_colorkey(struct tvp_dev *tdev,
				     u8 ovrl, u8 ovrh);
static void tvp_overlay_dst_colorkey(struct tvp_dev *tdev,
				     u8 redl, u8 redh,
				     u8 greenl, u8 greenh,
				     u8 bluel, u8 blueh);

static int tvp_check_fb(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_C8:
	case DRM_FORMAT_RGBX4444:
	case DRM_FORMAT_RGBA4444:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_RGB888:
	case DRM_FORMAT_BGR888:
	case DRM_FORMAT_XRGB8888:
	case DRM_FORMAT_ARGB8888:
		return 0;
	default:
		return -EINVAL;
	}
}

int tvp_start(struct tvp_dev *tdev,
	      unsigned int pixel_format,
	      unsigned int pixel_clock,
	      enum tvp_bus_width bus_width,
	      unsigned int rclk_to_lclk_ratio)
{
	struct tvp_pll_settings pclk;
	struct tvp_pll_settings lclk;
	unsigned int bpp = drm_format_depth(pixel_format);
	int ret;

	ret =  tvp_check_fb(pixel_format);
	if (ret)
		return ret;

	switch (bus_width) {
	case TVP_BUS_WIDTH_32:
	case TVP_BUS_WIDTH_64:
		break;
	default:
		return -EINVAL;
	}

	tdev->k = rclk_to_lclk_ratio;

	ret = tvp_pclkpll_calc(tdev, pixel_clock, 0, &pclk);
	if (ret)
		return ret;

	ret = tvp_lclkpll_calc(tdev, pclk.fpll, ALIGN(bpp, 8), bus_width, &lclk);
	if (ret)
		return ret;

	tvp_dac_power(tdev, true);
	tvp_dotclock_enable(tdev, true);

	tvp_lclkpll_disable(tdev);
	tvp_pclkpll_disable(tdev);

	//mga_crtc1_select_pixpll(tdev->mdev, 3);

	tvp_pclkpll_program(tdev, &pclk);
	tvp_lclkpll_program(tdev, &lclk);

	tvp_program(tdev, pixel_format, bus_width);

	// load palette
	if (0 && (pixel_format == DRM_FORMAT_ARGB8888 ||
		  pixel_format == DRM_FORMAT_XRGB8888))
		drm_generate_palette(DRM_FORMAT_C8, tdev->pal);
	else
		drm_generate_palette(pixel_format, tdev->pal);
	tvp_set_palette(tdev);

	u8 ovl1, ovl2;

	switch (bpp) {
	case 15:
		ovl1 = 0x1;
		ovl2 = 0x0;
		break;
	case 32:
		ovl1 = 0xff;
		ovl2 = 0x00;
		break;
	default:
		ovl1 = ovl2 = 0;
		break;
	}
	if (tdev->palette || !tdev->ovl_src_ckey)
		ovl1 = ovl2 = 0;
#if 1
	tvp_overlay_src_colorkey(tdev, ovl1, ovl1);
#else
	tvp_overlay_src_colorkey(tdev, ovl2, ovl2);
#endif

	u8 rgb1,rgb2;

	switch (bpp) {
	case 15:
		rgb1 = 0x0;
		rgb2 = 0xf8;
		break;
	case 32:
		rgb1 = 0x0;
		rgb2 = 0xff;
		break;
	default:
		rgb1 = rgb2 = 0;
		break;
	}
	if (tdev->palette || !tdev->ovl_dst_ckey)
		rgb1 = rgb2 = 0;

#if 0
	tvp_overlay_dst_colorkey(tdev,
				 rgb1, rgb1,
				 rgb1, rgb1,
				 rgb1, rgb1);
#else
	tvp_overlay_dst_colorkey(tdev,
				 rgb2, rgb2,
				 rgb2, rgb2,
				 rgb2, rgb2);
#endif

	return 0;
}

#if 0
static void tvp_calc()
{
	for (;;)
		regs[i] = init[i];
	if (555)
		regs[1] &= 0x01;
	if (interleave)
		regs[2] += 1;
	if (interlace)
		regs[20] |= 0x20;
}
#endif

static unsigned int tvp_crtc_sync(struct tvp_dev *tdev,
				  unsigned int sync)
{
	(void)tdev;
	(void)sync;
	//return HSYNC_POL_HIGH | VSYNC_POL_HIGH;
	return 3;
}

void tvp_set_sync(struct tvp_dev *tdev, unsigned int sync)
{
	u8 val;

	/* This assumes active low HSYNC/VSYNC inputs. */

	val = tvp_x_read8(tdev, TVP_XGENCTRL);

	val &= ~(TVP_XGENCTRL_HSPOL | TVP_XGENCTRL_VSPOL | TVP_XGENCTRL_IOGSYNCEN);

	if (sync & DRM_MODE_FLAG_CSYNC) {
		if (sync & DRM_MODE_FLAG_PCSYNC)
			val |= TVP_XGENCTRL_HSPOL;
	} else {
		if (sync & DRM_MODE_FLAG_PHSYNC)
			val |= TVP_XGENCTRL_HSPOL;
	}

	if (sync & DRM_MODE_FLAG_PVSYNC)
		val |= TVP_XGENCTRL_VSPOL;

	if (sync & DRM_MODE_FLAG_IOGSYNC)
		val |= TVP_XGENCTRL_IOGSYNCEN;

	tvp_write8(tdev, TVP_X_DATAREG, val);
}

void tvp_set_pedestal(struct tvp_dev *tdev, bool pedestal)
{
	u8 val;

	val = tvp_x_read8(tdev, TVP_XGENCTRL);

	if (pedestal)
		val |= TVP_XGENCTRL_PEDON;
	else
		val &= ~TVP_XGENCTRL_PEDON;

	tvp_write8(tdev, TVP_X_DATAREG, val);
}

void tvp_cursor_position(struct tvp_dev *tdev, int x, int y)
{
	x = clamp(x + 0x40, 0, 0xFFF);
	y = clamp(y + 0x40, 0, 0xFFF);

	tvp_write8(tdev, TVP_CURPOSXL, x & 0xff);
	tvp_write8(tdev, TVP_CURPOSXH, x >> 8);
	tvp_write8(tdev, TVP_CURPOSYL, y & 0xff);
	tvp_write8(tdev, TVP_CURPOSYH, y >> 8);
}

static void tvp_overlay_src_colorkey(struct tvp_dev *tdev,
					 u8 ovrl, u8 ovrh)
{
	tvp_x_write8(tdev, TVP_XCOLKEYOVRL, ovrl);
	tvp_x_write8(tdev, TVP_XCOLKEYOVRH, ovrh);
}

static void tvp_overlay_dst_colorkey(struct tvp_dev *tdev,
					 u8 redl, u8 redh,
					 u8 greenl, u8 greenh,
					 u8 bluel, u8 blueh)
{
	tvp_x_write8(tdev, TVP_XCOLKEYREDL, redl);
	tvp_x_write8(tdev, TVP_XCOLKEYREDH, redh);

	tvp_x_write8(tdev, TVP_XCOLKEYGREENL, greenl);
	tvp_x_write8(tdev, TVP_XCOLKEYGREENH, greenh);

	tvp_x_write8(tdev, TVP_XCOLKEYBLUEL, bluel);
	//test hw bug (bits 7:5 must match 7:5 or TVP_XCOLKEYCTRL?
	tvp_x_write8(tdev, TVP_XCOLKEYBLUEH, blueh);
}

static void tvp_overscan_color(struct tvp_dev *tdev, const struct drm_color *col)
{
	tvp_write8(tdev, TVP_COLWTADD, 0x00);

	tvp_write8(tdev, TVP_COLDATA, col->r);
	tvp_write8(tdev, TVP_COLDATA, col->g);
	tvp_write8(tdev, TVP_COLDATA, col->b);
}

static void tvp_cursor_color(struct tvp_dev *tdev, const struct drm_color col[3])
{
	int i;

	tvp_write8(tdev, TVP_COLWTADD, 0x01);

	for (i = 0; i < 3; i++) {
		tvp_write8(tdev, TVP_COLDATA, col[i].r);
		tvp_write8(tdev, TVP_COLDATA, col[i].g);
		tvp_write8(tdev, TVP_COLDATA, col[i].b);
	}
}

static void tvp_cursor_mode(struct tvp_dev *tdev, unsigned int mode, bool interlace)
{
	static const u8 curmode_vals[] = {
	};
	u8 val;

	BUG_ON(mode > TVP_CURSOR_XWIN);

	val = tvp_x_read8(tdev, TVP_XCURCTRL);

	val &= ~(TVP_XCURCTRL_CURMODE | TVP_XCURCTRL_INTERLACE);

	switch (mode) {
	case TVP_CURSOR_DISABLE:
		val |= TVP_XCURCTRL_CURMODE_DISABLE;
		break;
	case TVP_CURSOR_3COLOR:
		val |= TVP_XCURCTRL_CURMODE_3_COLOR;
		break;
	case TVP_CURSOR_XGA:
		val |= TVP_XCURCTRL_CURMODE_XGA;
		break;
	case TVP_CURSOR_XWIN:
		val |= TVP_XCURCTRL_CURMODE_XWIN;
		break;
	default:
		BUG();
	}

	if (interlace)
		val |= TVP_XCURCTRL_INTERLACE;

	tvp_write8(tdev, TVP_X_DATAREG, val);
}

void tvp_cursor_image(struct tvp_dev *tdev, const u8 *data)
{
	int i;
	u8 val;

	val = tvp_x_read8(tdev, TVP_XCURCTRL);
	val &= ~TVP_XCURCTRL_RAMADD;
	tvp_write8(tdev, TVP_X_DATAREG, val);

	tvp_write8(tdev, TVP_CURWTADD, 0x00);
	for (i = 0; i < 1024; i++)
		tvp_write8(tdev, TVP_CURDATA, data[i]);
}

void tvp_monitor_sense_start(struct tvp_dev *tdev)
{
	int i;

	/* FIXME: Make sure a paletted mode is used... */
	/* FIXME: Make sure video is enabled... */
	/*
	 * Program a uniform color for the entire LUT.
	 * Voltage is 0 mV - 700 mV, detection threshold is 350 mV
	 * (1/2 of the maximum). Since the voltage should double
	 * when the monitor termination is missing aim for 1/3 of the
	 * maximum.
	 */
	tvp_write8(tdev, TVP_PALWTADD, 0x00);
	for (i = 0; i < 256 * 3; i++)
		tvp_write8(tdev, TVP_PALDATA, 0xFF / 3);

	/* Power up the sense comparator. */
	tvp_x_write8(tdev, TVP_XSENSETEST, 0);
}

void tvp_monitor_sense_stop(struct tvp_dev *tdev,
			    bool *r, bool *g, bool *b)
{
	u8 val;
	int i;

	/* Get the results. */
	val = tvp_x_read8(tdev, TVP_XSENSETEST);

	*r = !(val & TVP_XSENSETEST_RCOMP);
	*g = !(val & TVP_XSENSETEST_GCOMP);
	*b = !(val & TVP_XSENSETEST_BCOMP);

	/* Power down the sense comparator. */
	tvp_write8(tdev, TVP_X_DATAREG, TVP_XSENSETEST_SENSEPD);

	/* Restore the LUT. */
	tvp_set_palette(tdev);
}

static void tvp_gpio_set(struct tvp_dev *tdev,
			 u8 mask, u8 output)
{
	u8 val;

	BUG_ON(output & ~mask);

	val = tvp_x_read8(tdev, TVP_XGENIOCTRL);

	val = (val & ~mask) | (output & mask);

	tvp_write8(tdev, TVP_X_DATAREG, val);
}

static u8 tvp_gpio_get(struct tvp_dev *tdev)
{
	return tvp_x_read8(tdev, TVP_XGENIODATA);
}

void tvp_i2c_set(struct tvp_dev *tdev, u8 pin, bool state)
{
	tvp_gpio_set(tdev, pin, state ? 0 : pin);
}

bool tvp_i2c_get(struct tvp_dev *tdev, u8 pin)
{
	return !!(tvp_gpio_get(tdev) & pin);
}

#include <stdio.h>
static void tvp_reg_test(struct tvp_dev *tdev)
{
	dev_dbg(tdev->dev, "TVP REG TEST BEGIN:\n");

	while (1) {
		char buf[32];
		unsigned int reg, val;
		char len;

		char *s = fgets(buf, sizeof buf, stdin);
		if (!s)
			continue;
		buf[sizeof buf - 1] = 0;

		if (buf[0] == 'q')
			return;

		int write = 1;
		if (sscanf(buf, "%c/%x=%x", &len, &reg, &val) != 3) {
			if (sscanf(buf, "%c/%x", &len, &reg) != 2) {
				continue;
			}
			write = 0;
		}

		printk("len = %c, reg = %x, val = %x\n",
		       len, reg, val);

		if (len != 'i' && len != 'd')
			continue;

		if (len == 'i') {
			if (reg > 0xff)
				continue;
		} else if (len == 'd') {
			if (reg > 0xf)
				continue;
		} else {
			continue;
		}

		if (len == 'i') {
			if (write) {
				if (val > 0xff)
					continue;
				tvp_x_write8(tdev, reg, val);
			} else {
				val = tvp_x_read8(tdev, reg);
				dev_dbg(tdev->dev, "%02x = %02x\n", reg, val);
			}
		}

		if (len == 'd') {
			if (write) {
				if (val > 0xff)
					continue;
				tvp_write8(tdev, reg, val);
			} else {
				val = tvp_read8(tdev, reg);
				dev_dbg(tdev->dev, "%01x = %02x\n", reg, val);
			}
		}
	}
}
