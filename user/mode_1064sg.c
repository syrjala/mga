/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "mga_regs.h"
#include "mga_kms.h"
#include "mga_dac.h"
#include "mga_flat.h"

struct crtc_settings {
	struct drm_framebuffer *fb;
	struct drm_display_mode mode;
	unsigned int pll;
	unsigned int outputs;
	unsigned int pixclk;
	int hscale;
	int vscale;
	int x;
	int y;
};

static struct crtc_settings mdev_crtc;

static void crtc1_disable(struct mga_dev *mdev)
{
	unsigned int outputs = mdev_crtc.outputs;

	if (!outputs)
		return;

	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);

	if (outputs & (MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1))
		mga_crtc1_dpms(mdev, DRM_MODE_DPMS_OFF);

	if (outputs & MGA_OUTPUT_TVOUT)
		mga_tvo_disable(&mdev->tvodev);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_tmds_power(mdev, false);

	if (outputs & MGA_OUTPUT_DAC1)
		mga_dac_dac_power(mdev, false);

	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	mga_dac_crtc1_power(mdev, false);

	if (outputs & MGA_OUTPUT_TVOUT)
		mga_dac_tvo_source(mdev, MGA_CRTC_NONE);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_dac_panel_source(mdev, MGA_CRTC_NONE);

	mdev_crtc.pixclk = 0;
	mdev_crtc.outputs = 0;
	mdev_crtc.pll = 0;
	memset(&mdev_crtc.mode, 0, sizeof mdev_crtc.mode);
	mdev_crtc.fb = NULL;
}

static void crtc1_enable(struct mga_dev *mdev,
			 const struct mga_mode_config *mc,
			 const struct mga_pll_settings *pll)
{
	const struct mga_plane_config *pc = &mc->crtc1.plane_config;
	const struct mga_crtc_config *cc = &mc->crtc1.crtc_config;
	const struct mga_crtc1_regs *regs = &mc->crtc1.regs;
	const struct drm_framebuffer *fb = pc->fb;
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	bool overlay = cc->overlay;
	unsigned int hzoom = 0x10000 / pc->hscale;
	unsigned int outputs = cc->outputs;
	/* FIXME sync from mode */
	unsigned int sync = DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_NVSYNC;

	mga_crtc1_restore(mdev, regs);
	mga_dac_crtc1_program(mdev, &mc->dac_config);
	mga_dac_crtc1_pixclk_select(mdev, pll->type);

	if (outputs & (MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1))
		mga_crtc1_set_sync(mdev, sync);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_dac_panel_source(mdev, MGA_CRTC_CRTC1);

	if (outputs & MGA_OUTPUT_TVOUT)
		mga_dac_tvo_source(mdev, MGA_CRTC_CRTC1);

	mga_dac_crtc1_power(mdev, true);
	mga_dac_crtc1_set_palette(mdev);
	mga_dac_crtc1_pixclk_enable(mdev, true);

	if (outputs & MGA_OUTPUT_DAC1)
		mga_dac_dac_power(mdev, true);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_tmds_power(mdev, true);

	if (outputs & MGA_OUTPUT_TVOUT)
		mga_tvo_program_tvout(&mdev->tvodev,
				      &mc->tvo_config,
				      mode, &pll->dac);

	if (outputs & (MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1))
		mga_crtc1_dpms(mdev, DRM_MODE_DPMS_ON);

	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, true);
}

int mga_1064sg_set_mode(struct mga_dev *mdev,
			struct mga_mode_config *mc)
{
	struct mga_pll_settings pll = { .type = MGA_PLL_NONE };
	unsigned int pixclk = 0;
	unsigned int plls;
	int ret;
	bool changed = false;
	struct mga_plane_config *pc = &mc->crtc1.plane_config;
	struct mga_crtc_config *cc = &mc->crtc1.crtc_config;

	if (mc->crtc2.crtc_config.mode_valid)
		return -ENODEV;

	/* all or nothing! */
	if (cc->mode_valid) {
		if (!cc->outputs || !pc->fb)
			return -EINVAL;
	} else {
		if (cc->outputs || pc->fb)
			return -EINVAL;
	}

	/* all possible outputs */
	BUG_ON(mdev->outputs & ~(MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1 | MGA_OUTPUT_TVOUT));

	/* check for board features */
	if (cc->outputs & ~mdev->outputs)
		return -ENODEV;

	/* only one MAFC port */
	if (cc->outputs & MGA_OUTPUT_TMDS1 &&
	    cc->outputs & MGA_OUTPUT_TVOUT)
		return -EINVAL;

	if (cc->mode_valid) {
		if (cc->outputs & MGA_OUTPUT_TVOUT)
			cc->vidrst = true;

		dev_dbg(mdev->dev, "x=%u, y=%u\n", pc->src.x1, pc->src.y1);

		ret = mga_crtc1_calc_mode(mdev, pc, cc, &mc->crtc1.regs);
		if (ret)
			return ret;

		pixclk = mga_calc_pixel_clock(&cc->adjusted_mode, 1);
		if (!pixclk)
			return -EINVAL;
	}

	if (mdev->chip >= MGA_CHIP_G200) {
		ret = mga_g200_hipri(mdev, mdev->mclk, pixclk, &mc->crtc1.regs);
		if (ret)
			return ret;
	}

	/* pick our favorite PLLs */

	if (cc->outputs & (MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1))
		pll.type = MGA_PLL_PIXPLL;

	if (cc->outputs & MGA_OUTPUT_TVOUT)
		pll.type = MGA_PLL_TVO;

	/* FIXME CRTC/DAC max clock */
	ret = mga_pll_calc(mdev, pixclk, &pll);
	if (ret)
		return ret;

	if (pll.type != mdev_crtc.pll ||
	    cc->outputs != mdev_crtc.outputs ||
	    pixclk != mdev_crtc.pixclk ||
	    pc->fb != mdev_crtc.fb ||
	    (cc->mode_valid && !drm_mode_equal(&cc->adjusted_mode, &mdev_crtc.mode)) ||
	    pc->hscale != mdev_crtc.hscale ||
	    pc->vscale != mdev_crtc.vscale ||
	    pc->src.x1 != mdev_crtc.x ||
	    pc->src.y1 != mdev_crtc.y)
		changed = true; /* full modeset when pll/outputs/fb/mode/pixclk change */

	if (!changed)
		goto exit;

	if (changed)
		crtc1_disable(mdev);

	if (changed && pll.type) {
		ret = mga_pll_program(mdev, &pll);
		if (ret) {
			mdev_crtc.pll &= ~pll.type;
			goto exit; /* FIXME restore old settings */
		}
	}

	if (changed && cc->mode_valid)
		crtc1_enable(mdev, mc, &pll);

	// disp start
	// zoom

	if (cc->mode_valid)
		mdev_crtc.mode = cc->adjusted_mode;
	else
		memset(&mdev_crtc.mode, 0, sizeof mdev_crtc.mode);
	mdev_crtc.fb = pc->fb;
	mdev_crtc.outputs = cc->outputs;
	mdev_crtc.pixclk = pixclk;
	mdev_crtc.pll = pll.type;
	mdev_crtc.hscale = pc->hscale;
	mdev_crtc.vscale = pc->vscale;
	mdev_crtc.x = pc->src.x1;
	mdev_crtc.y = pc->src.y1;

 exit:
	plls = mdev->sysclk_plls | mdev_crtc.pll;

	/* power down unused PLLs */
	if (mdev->enabled_plls & MGA_PLL_PIXPLL && !(plls & MGA_PLL_PIXPLL))
		mga_dac_pixpll_power(mdev, false);

	return ret;
}
