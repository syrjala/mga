/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <stdlib.h>

#include "mga_dump.h"
#include "mga_g450_dac.h"
#include "tvo_g450.h"
#include "mga_dac_regs.h"
#include "mga_regs.h"
#include "mga_i2c.h"
#include "modes.h"

static unsigned int calc_mclk(unsigned int syspll,
			      unsigned int vidpll,
			      u32 option3)
{
	unsigned int mclk = 0;

	switch (option3 & MGA_OPTION3_MCLKSEL) {
	case MGA_OPTION3_MCLKSEL_PCI:
		/* FIXME? */
		return 33333;
	case MGA_OPTION3_MCLKSEL_SYSPLL:
		mclk = syspll;
		break;
	case MGA_OPTION3_MCLKSEL_MCLK:
		/* FIXME? */
		return 0;
	case MGA_OPTION3_MCLKSEL_VIDPLL:
		mclk = vidpll;
		break;
	}

	switch (option3 & MGA_OPTION3_MCLKDIV) {
	case MGA_OPTION3_MCLKDIV_1_3:
		return div_round(mclk * 1, 3);
	case MGA_OPTION3_MCLKDIV_2_5:
		return div_round(mclk * 2, 5);
	case MGA_OPTION3_MCLKDIV_4_9:
		return div_round(mclk * 4, 9);
	case MGA_OPTION3_MCLKDIV_1_2:
		return div_round(mclk * 1, 2);
	case MGA_OPTION3_MCLKDIV_2_3:
		return div_round(mclk * 2, 3);
	case MGA_OPTION3_MCLKDIV_1_1:
		return mclk;
	default:
		BUG();
	}
}

#if 0
static void mga_g450_clock_powerup(struct mga_dev *mdev)
{
	// fIXME
	u32 option;
	u8 val;

	/* 1. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 2. */
	option |= MGA_OPTION_SYSCLKSL_SYSPLL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 3. */
	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 4. */
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXCLKCTRL);
	val = mga_read8(mdev, MGA_X_DATAREG);
	val |= MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 5. */
	val |= MGA_XPIXCLKCTRL_PIXCLKSEL_PIXPLL;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 6. */
	val &= ~MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);
}
#endif

static void sysclk_select(struct mga_dev *mdev, u32 mask, u32 clksel)
{
	u32 option, option3;

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option3 = pci_cfg_read32(&mdev->pdev, MGA_OPTION3);
	option3 &= ~mask;
	option3 |= clksel & mask;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION3, option3);

	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

enum {
	MGA_OPTION3_CLKSEL_PCI =
		MGA_OPTION3_GCLKSEL_PCI |
		MGA_OPTION3_MCLKSEL_PCI |
		MGA_OPTION3_WCLKSEL_PCI,
	MGA_OPTION3_CLKSEL =
		MGA_OPTION3_GCLKSEL |
		MGA_OPTION3_MCLKSEL |
		MGA_OPTION3_WCLKSEL,
	MGA_OPTION3_CLKDIV_CLKDCYC =
		MGA_OPTION3_GCLKDIV | MGA_OPTION3_GCLKDCYC |
		MGA_OPTION3_MCLKDIV | MGA_OPTION3_MCLKDCYC |
		MGA_OPTION3_WCLKDIV | MGA_OPTION3_WCLKDCYC,
};

static int sysclk_program(struct mga_dev *mdev,
			  unsigned int syspllfo,
			  unsigned int vidpllfo,
			  u32 option_clk,
			  u32 option3_clk)
{
	struct mga_dac_pll_settings syspll[MGA_PLL_SETTINGS_MAX];
	struct mga_dac_pll_settings vidpll[MGA_PLL_SETTINGS_MAX];
	unsigned int syspll_count, vidpll_count;
	unsigned int mclk;
	u32 option, option3;
	int ret;

	if (syspllfo) {
		ret = mga_g450_dac_syspll_calc(mdev, syspllfo, 0,
					       syspll, &syspll_count);
		if (ret)
			return ret;
	}

	if (vidpllfo) {
		ret = mga_g450_dac_vidpll_calc(mdev, vidpllfo, 0,
					       vidpll, &vidpll_count);
		if (ret)
			return ret;
	}

	mclk = calc_mclk(0, 0, MGA_OPTION3_CLKSEL_PCI);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, MGA_OPTION3_CLKSEL, MGA_OPTION3_CLKSEL_PCI);
	mga_mclk_change_post(mdev, mclk);

	mga_g450_dac_vidpll_power(mdev, false);

	mga_dac_syspll_power(mdev, false);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_PLLSEL;
	option |= option_clk & MGA_OPTION_PLLSEL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	if (syspllfo) {
		mga_dac_syspll_power(mdev, true);

		ret = mga_g450_dac_syspll_program(mdev, syspll, syspll_count);
		if (ret) {
			mga_dac_syspll_power(mdev, false);
			return ret;
		}
		/* Read back the information */
		mga_g450_dac_syspll_save(mdev, &syspll[0]);
	}

	if (vidpllfo) {
		mga_g450_dac_vidpll_power(mdev, true);

		ret = mga_g450_dac_vidpll_program(mdev, vidpll, vidpll_count);
		if (ret) {
			mga_g450_dac_vidpll_power(mdev, false);
			return ret;
		}
		/* Read back the results */
		mga_g450_dac_vidpll_save(mdev, &vidpll[0]);
	}

	option3 = pci_cfg_read32(&mdev->pdev, MGA_OPTION3);
	option3 &= ~MGA_OPTION3_CLKDIV_CLKDCYC;
	option3 |= option3_clk & MGA_OPTION3_CLKDIV_CLKDCYC;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION3, option3);

	mclk = calc_mclk(syspll[0].fo, vidpll[0].fo, option3_clk);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, MGA_OPTION3_CLKSEL, option3_clk);
	mga_mclk_change_post(mdev, mclk);

	if (syspllfo)
		mdev->sysclk_plls |= MGA_PLL_SYSPLL;
	else
		mdev->sysclk_plls &= ~MGA_PLL_SYSPLL;

	if (vidpllfo)
		mdev->sysclk_plls |= MGA_PLL_VIDPLL;
	else
		mdev->sysclk_plls &= ~MGA_PLL_VIDPLL;

	return 0;
}

static void mga_g450_mem_reset(struct mga_dev *mdev, unsigned int mclk);

static void mga_g450_powerup(struct mga_dev *mdev, bool mem_reset)
{
	const struct mga_pins5 *pins = &mdev->pins.pins5;
#if 0
#if 1
	// G450 PCI values
	u32 pins_option = 0x404a1560;
	u32 pins_option3 = 0x0090a409;
	unsigned int syspll = 288000;
	unsigned int vidpll = 288000;
#else
	// G550 AGP values
	u32 pins_option = 0x404a1520;
	u32 pins_option3 = 0x0010a401;
	unsigned int syspll = 332000;
	unsigned int vidpll = 332000;
#endif
#else
	u32 pins_option = pins->option;
	u32 pins_option3 = pins->clk_vga.option3;
	unsigned int syspll = pins->clk_vga.syspll;
	unsigned int vidpll = pins->clk_vga.vidpll;
#endif
	unsigned int gclk, mclk, wclk;
	int ret;

	/* HACK test VIDPLL as sysclk */
	if (0) {
		pins_option3 &= ~MGA_OPTION3_CLKSEL;
		pins_option3 |= MGA_OPTION3_GCLKSEL_VIDPLL | MGA_OPTION3_MCLKSEL_VIDPLL | MGA_OPTION3_WCLKSEL_VIDPLL;
		swap(syspll, vidpll);
	}

	if ((pins_option3 & MGA_OPTION3_GCLKSEL) != MGA_OPTION3_GCLKSEL_SYSPLL &&
	    (pins_option3 & MGA_OPTION3_MCLKSEL) != MGA_OPTION3_MCLKSEL_SYSPLL &&
	    (pins_option3 & MGA_OPTION3_WCLKSEL) != MGA_OPTION3_WCLKSEL_SYSPLL)

	/* is any system clock using SYSPLL? */
	if ((pins_option3 & MGA_OPTION3_GCLKSEL) != MGA_OPTION3_GCLKSEL_SYSPLL &&
	    (pins_option3 & MGA_OPTION3_MCLKSEL) != MGA_OPTION3_MCLKSEL_SYSPLL &&
	    (pins_option3 & MGA_OPTION3_WCLKSEL) != MGA_OPTION3_WCLKSEL_SYSPLL)
		syspll = 0;

	/* is any system clock using VIDPLL? */
	if ((pins_option3 & MGA_OPTION3_GCLKSEL) != MGA_OPTION3_GCLKSEL_VIDPLL &&
	    (pins_option3 & MGA_OPTION3_MCLKSEL) != MGA_OPTION3_MCLKSEL_VIDPLL &&
	    (pins_option3 & MGA_OPTION3_WCLKSEL) != MGA_OPTION3_WCLKSEL_VIDPLL)
		vidpll = 0;

	switch (pins_option3 & MGA_OPTION3_GCLKSEL) {
	case MGA_OPTION3_GCLKSEL_PCI:
		/* FIXME? */
		gclk = 33333;
		break;
	case MGA_OPTION3_GCLKSEL_SYSPLL:
		gclk = syspll;
		break;
	case MGA_OPTION3_GCLKSEL_MCLK:
		/* FIXME? */
		gclk = 0;
		break;
	case MGA_OPTION3_GCLKSEL_VIDPLL:
		gclk = vidpll;
		break;
	default:
		BUG();
	}

	switch (pins_option3 & MGA_OPTION3_MCLKSEL) {
	case MGA_OPTION3_MCLKSEL_PCI:
		/* FIXME? */
		mclk = 33333;
		break;
	case MGA_OPTION3_MCLKSEL_SYSPLL:
		mclk = syspll;
		break;
	case MGA_OPTION3_MCLKSEL_MCLK:
		/* FIXME? */
		mclk = 0;
		break;
	case MGA_OPTION3_MCLKSEL_VIDPLL:
		mclk = vidpll;
		break;
	default:
		BUG();
	}

	switch (pins_option3 & MGA_OPTION3_WCLKSEL) {
	case MGA_OPTION3_WCLKSEL_PCI:
		/* FIXME? */
		wclk = 33333;
		break;
	case MGA_OPTION3_WCLKSEL_SYSPLL:
		wclk = syspll;
		break;
	case MGA_OPTION3_WCLKSEL_MCLK:
		/* FIXME? */
		wclk = 0;
		break;
	case MGA_OPTION3_WCLKSEL_VIDPLL:
		wclk = vidpll;
		break;
	default:
		BUG();
	}

	if ((pins_option3 & MGA_OPTION3_GCLKSEL) == MGA_OPTION3_GCLKSEL_SYSPLL ||
	    (pins_option3 & MGA_OPTION3_GCLKSEL) == MGA_OPTION3_GCLKSEL_VIDPLL) {
		switch (pins_option3 & MGA_OPTION3_GCLKDIV) {
		case MGA_OPTION3_GCLKDIV_1_3:
			gclk = div_round(gclk * 1, 3);
			break;
		case MGA_OPTION3_GCLKDIV_2_5:
			gclk = div_round(gclk * 2, 5);
			break;
		case MGA_OPTION3_GCLKDIV_4_9:
			gclk = div_round(gclk * 4, 9);
			break;
		case MGA_OPTION3_GCLKDIV_1_2:
			gclk = div_round(gclk * 1, 2);
			break;
		case MGA_OPTION3_GCLKDIV_2_3:
			gclk = div_round(gclk * 2, 3);
			break;
		case MGA_OPTION3_GCLKDIV_1_1:
			break;
		default:
			BUG();
		}
	}

	if ((pins_option3 & MGA_OPTION3_MCLKSEL) == MGA_OPTION3_MCLKSEL_SYSPLL ||
	    (pins_option3 & MGA_OPTION3_MCLKSEL) == MGA_OPTION3_MCLKSEL_VIDPLL) {
		switch (pins_option3 & MGA_OPTION3_MCLKDIV) {
		case MGA_OPTION3_MCLKDIV_1_3:
			mclk = div_round(mclk * 1, 3);
			break;
		case MGA_OPTION3_MCLKDIV_2_5:
			mclk = div_round(mclk * 2, 5);
			break;
		case MGA_OPTION3_MCLKDIV_4_9:
			mclk = div_round(mclk * 4, 9);
			break;
		case MGA_OPTION3_MCLKDIV_1_2:
			mclk = div_round(mclk * 1, 2);
			break;
		case MGA_OPTION3_MCLKDIV_2_3:
			mclk = div_round(mclk * 2, 3);
			break;
		case MGA_OPTION3_MCLKDIV_1_1:
			break;
		default:
			BUG();
		}
	}

	if ((pins_option3 & MGA_OPTION3_WCLKSEL) == MGA_OPTION3_WCLKSEL_SYSPLL ||
	    (pins_option3 & MGA_OPTION3_WCLKSEL) == MGA_OPTION3_WCLKSEL_VIDPLL) {
		switch (pins_option3 & MGA_OPTION3_WCLKDIV) {
		case MGA_OPTION3_WCLKDIV_1_3:
			wclk = div_round(wclk * 1, 3);
			break;
		case MGA_OPTION3_WCLKDIV_2_5:
			wclk = div_round(wclk * 2, 5);
			break;
		case MGA_OPTION3_WCLKDIV_4_9:
			wclk = div_round(wclk * 4, 9);
			break;
		case MGA_OPTION3_WCLKDIV_1_2:
			wclk = div_round(wclk * 1, 2);
			break;
		case MGA_OPTION3_WCLKDIV_2_3:
			wclk = div_round(wclk * 2, 3);
			break;
		case MGA_OPTION3_WCLKDIV_1_1:
			break;
		default:
			BUG();
		}
	}

	dev_dbg(mdev->dev, "SYSPLL = %u kHz\n", syspll);
	dev_dbg(mdev->dev, "VIDPLL = %u kHz\n", vidpll);
	dev_dbg(mdev->dev, "  GCLK = %u kHz\n", gclk);
	dev_dbg(mdev->dev, "  MCLK = %u kHz\n", mclk);
	dev_dbg(mdev->dev, "  WCLK = %u kHz\n", wclk);

	mga_g450_dac_init(mdev, pins->fref,
			  pins->pixpll.fvco_min, pins->pixpll.fvco_max,
			  pins->syspll.fvco_min, pins->syspll.fvco_max,
			  pins->vidpll.fvco_min, pins->vidpll.fvco_max);

	ret = sysclk_program(mdev, syspll, vidpll, pins_option, pins_option3);
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		return;
	}

	if (mem_reset) {
		mga_g100_rfhcnt(mdev, 0);//
		udelay(200);//
		mga_g450_mem_reset(mdev, mclk);
	} else
		mga_g100_rfhcnt(mdev, mclk);

	mdev->maccess = pins->maccess;
}

static void mga_g450_mem_reset(struct mga_dev *mdev, unsigned int mclk)
{
	const struct mga_pins5 *pins = &mdev->pins.pins5;
#if 0
#if 1
	// G450 PCI values
	u32 pins_option = 0x404a1560;
	u32 pins_option2 = 0x0000ac00;
	u32 pins_memmisc = 0x80000004;
	u32 pins_mctlwtst = 0x0a81462b;
	u32 pins_mctlwtst_core = 0x0a81462b;
	u32 pins_memrdbk = 0x01001522;
	u32 pins_maccess = 0x00004000;
	bool ddr = true;
	bool emrs = true;
	bool dll = true;
#else
	// G550 AGP values
	u32 pins_option = 0x404a1520;
	u32 pins_option2 = 0x0000ac00;
	u32 pins_memmisc = 0x80000a04;
	u32 pins_mctlwtst = 0x0a81462b;
	u32 pins_mctlwtst_core = 0x0a81462b;
	u32 pins_memrdbk = 0x01001220;
	u32 pins_maccess = 0x00004000;
	bool ddr = true;
	bool emrs = true;
	bool dll = true;
#endif
#else
	// G550 AGP values
	u32 pins_option = pins->option;
	u32 pins_option2 = pins->option2;
	u32 pins_memmisc = pins->clk_vga.memmisc;
	u32 pins_mctlwtst = pins->clk_vga.mctlwtst;
	u32 pins_mctlwtst_core = pins->clk_vga.mctlwtst_core;
	u32 pins_memrdbk = pins->clk_vga.memrdbk;
	u32 pins_maccess = pins->maccess;
	bool ddr = pins->mem.ddr;
	bool emrs = pins->mem.emrswen;
	bool dll = pins->mem.dll;
#endif
	u32 option, option2;

	//FIXME
	//FIXME move out of memory init
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~(MGA_OPTION_NOHIREQ |
		    MGA_OPTION_NORETRY |
		    MGA_OPTION_ENHMEMACC |
		    MGA_OPTION_PLLSEL);
	option |= pins_option & (MGA_OPTION_NOHIREQ |
				 MGA_OPTION_NORETRY |
				 MGA_OPTION_ENHMEMACC |
				 MGA_OPTION_PLLSEL);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	// stop rfhcnt

	// FIXME
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~(MGA_OPTION_MEMCONFIG_NEW |
		    MGA_OPTION_MDSFEN |
		    MGA_OPTION_MDSREN |
		    MGA_OPTION_HARDPWMSK |
		    MGA_OPTION_MBLKTYPE);
	option |= pins_option & (MGA_OPTION_MEMCONFIG_NEW |
				 MGA_OPTION_MDSFEN |
				 MGA_OPTION_MDSREN |
				 MGA_OPTION_HARDPWMSK |
				 MGA_OPTION_MBLKTYPE);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	// FIXME
	option2 = pci_cfg_read32(&mdev->pdev, MGA_OPTION2);
	option2 &= ~(MGA_OPTION2_MCLKDRV |
		     MGA_OPTION2_MCMDDRV |
		     MGA_OPTION2_MDATDRV);
	option2 |= pins_option2 & (MGA_OPTION2_MCLKDRV |
				   MGA_OPTION2_MCMDDRV |
				   MGA_OPTION2_MDATDRV);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION2, option2);

	mga_write32(mdev, MGA_MCTLWTST, pins_mctlwtst);

	// FIXME
	pci_cfg_write32(&mdev->pdev, MGA_MEMMISC, pins_memmisc & ~MGA_MEMMISC_MCLKEN);

	mga_write32(mdev, MGA_MEMRDBK, pins_memrdbk);

	mga_write32(mdev, MGA_MACCESS, pins_maccess);

	// FIXME
	pci_cfg_write32(&mdev->pdev, MGA_MEMMISC, pins_memmisc | MGA_MEMMISC_MCLKEN);

	udelay(200);

	if (ddr && (!emrs || !dll))
		mga_write32(mdev, MGA_MEMRDBK, pins_memrdbk & ~0x1000);

	mga_write32(mdev, MGA_MACCESS, pins_maccess | MGA_MACCESS_MEMRESET);

	udelay(200);

	mga_g100_rfhcnt(mdev, mclk);

	mga_write32(mdev, MGA_PLNWT, 0x00000000);
	mga_write32(mdev, MGA_PLNWT, 0xffffffff);

	if (pins_mctlwtst_core != pins_mctlwtst)
		mga_write32(mdev, MGA_MCTLWTST, pins_mctlwtst_core);

	mga_g100_rfhcnt(mdev, mdev->mclk);
}

static void probe_addons(struct mga_dev *mdev)
{
	const struct mga_pins5 *pins = &mdev->pins.pins5;

	if (pins->outputs & _MGA_OUTPUT_DAC1)
		mdev->outputs |= MGA_OUTPUT_DAC1;
	if (pins->outputs & _MGA_OUTPUT_DAC2)
		mdev->outputs |= MGA_OUTPUT_DAC2;
	if (pins->outputs & _MGA_OUTPUT_TMDS1)
		mdev->outputs |= MGA_OUTPUT_TMDS1;
	if (pins->outputs & _MGA_OUTPUT_TMDS2)
		mdev->outputs |= MGA_OUTPUT_TMDS2;
	if (pins->outputs & _MGA_OUTPUT_TVOUT)
		mdev->outputs |= MGA_OUTPUT_TVOUT;

	if (pins->features & _MGA_FEAT_TVO_BUILTIN)
		mdev->features |= MGA_FEAT_TVO_BUILTIN;
	if (pins->features & _MGA_FEAT_DVD_BUILTIN)
		mdev->features |= MGA_FEAT_DVD_BUILTIN;
	if (pins->features & _MGA_FEAT_MJPEG_BUILTIN)
		mdev->features |= MGA_FEAT_MJPEG_BUILTIN;
	if (pins->features & _MGA_FEAT_VIN_BUILTIN)
		mdev->features |= MGA_FEAT_VIN_BUILTIN;
	if (pins->features & _MGA_FEAT_TUNER_BUILTIN)
		mdev->features |= MGA_FEAT_TUNER_BUILTIN;
	if (pins->features & _MGA_FEAT_AUDIO_BUILTIN)
		mdev->features |= MGA_FEAT_AUDIO_BUILTIN;
	if (pins->features & _MGA_FEAT_TMDS_BUILTIN)
		mdev->features |= MGA_FEAT_TMDS_BUILTIN;

	mga_g450_tvo_init(&mdev->tvodev_g450, mdev);

	mga_print_features(mdev);
}

static void mga_g450_init(struct mga_dev *mdev)
{
	struct drm_display_mode c1mode = { .vrefresh = 0 };
	struct drm_display_mode c2mode = { .vrefresh = 0 };
	unsigned int c1outputs = 0;
	unsigned int c2outputs = 0;
	bool use_crtc1 = false;
	bool use_crtc2 = false;


	use_crtc1 = true;
	use_crtc2 = false;

	//c1mode = mode_640_480_60;
	c1mode = mode_1280_1024_60;
	//c1mode = mode_1920_1200_60_r;

	//c2mode = mode_640_480_60;
	//c2mode = mode_720_480_60;
	//c2mode = mode_720_486_60;
	c2mode = mode_720_576_50;
	//c2mode = mode_1280_1024_60;
	//c2mode = mode_1920_1200_60_r;

#if 0
	if (c1mode.vtotal < 625) {
		c1mode.vsync_start += (625 - c1mode.vtotal) >> 1;
		c1mode.vsync_start++;
		c1mode.vtotal = 625;
	}
#endif
#if 0
	if (c2mode.vtotal < 625) {
		c2mode.vsync_start += (625 - c2mode.vtotal) >> 1;
		c2mode.vsync_start++;
		c2mode.vtotal = 625;
	}
#endif
#if 0
	if (c2mode.vtotal < 525) {
		c2mode.vsync_start += (525 - c2mode.vtotal) >> 1;
		c2mode.vsync_start++;
		c2mode.vtotal = 525;
	}
#endif
	//c2mode.vdisplay = 485;
	//c2mode.vsync_start = c2mode.vdisplay + 6;
	//c2mode.vblank_width = 525 - c2mode.vdisplay;

	if (use_crtc1) {
		//c1outputs |= MGA_OUTPUT_DAC1;
		//c1outputs |= MGA_OUTPUT_DAC2;
		c1outputs |= MGA_OUTPUT_TMDS1;
		//c1outputs |= MGA_OUTPUT_TMDS2;
	}

	if (use_crtc2) {
		//c2outputs |= MGA_OUTPUT_DAC1;
		//c2outputs |= MGA_OUTPUT_DAC2;
		//c2outputs |= MGA_OUTPUT_TMDS1;
		//c2outputs |= MGA_OUTPUT_TMDS2;
		c2outputs |= MGA_OUTPUT_TVOUT;
	}

	mga_set_initial_crtc1_mode(mdev,
				   mdev->pixel_format,
				   &c1mode,
				   c1outputs);
	mga_set_initial_crtc2_mode(mdev,
				   mdev->pixel_format,
				   &c1mode,
				   c1outputs);

	mga_misc_disable_vga(mdev);

	dev_dbg(mdev->dev, "Disabling CRTC2\n");
	mga_crtc2_video(mdev, false);
	mga_crtc2_pixclk_enable(mdev, false);
	mga_crtc2_pixclk_select(mdev, MGA_PLL_NONE);

	dev_dbg(mdev->dev, "Disabling CRTC1\n");
	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);
	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	//mga_g450_dac_init(mdev);

	mga_misc_powerup(mdev);

	mga_g450_dac_powerup(mdev);

	mga_g450_powerup(mdev, true);

	mga_crtc1_powerup(mdev);

	mga_i2c_dac_misc_init(mdev);
	mga_i2c_dac_ddc1_init(mdev);
	mga_i2c_dac_ddc2_init(mdev);

	probe_addons(mdev);

	mga_probe_mem_size(mdev);
}

static void mga_g450_test(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");

	mga_enable_outputs(mdev);

	// fixme
#if 0
	if (mdev_outputs[0] & MGA_OUTPUT_DAC1)
		mdev->funcs->monitor_sense_dac1(mdev);
	if (mdev_outputs[0] & MGA_OUTPUT_DAC2)
		mdev->funcs->monitor_sense_dac2(mdev);
#endif

	if (strstr(test, "reg"))
		mga_reg_test(mdev);
	if (strstr(test, "tvo"))
		mga_g450_tvo_test(&mdev->tvodev_g450);

	mga_disable_outputs(mdev);
}

static int mga_g450_suspend(struct mga_dev *mdev)
{
	u32 option;
	int ret;

	if (mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to power saving mode\n");

	mga_disable_outputs(mdev);

	mga_wait_dma_idle(mdev);
	mga_wait_dwgeng_idle(mdev);

	mga_g100_rfhcnt(mdev, 4333);

	/* Preserve pllsel */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= MGA_OPTION_PLLSEL;

	// FIXME test
	ret = sysclk_program(mdev, 6667, 0,
			     option,
			     MGA_OPTION3_GCLKSEL_SYSPLL | MGA_OPTION3_GCLKDIV_1_3 | (0 << 6) |
			     MGA_OPTION3_MCLKSEL_SYSPLL | MGA_OPTION3_MCLKDIV_2_3 | (0xb << 16) |
			     MGA_OPTION3_WCLKSEL_SYSPLL | MGA_OPTION3_WCLKDIV_1_3 | (0 << 26));
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		goto err;
	}

	mdev->suspended = true;

	dev_dbg(mdev->dev, "In power saving mode\n");

	return 0;

 err:
	mga_enable_outputs(mdev);

	return ret;
}

static int mga_g450_resume(struct mga_dev *mdev)
{
	if (!mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to normal mode\n");

	mga_g450_powerup(mdev, false);

	mga_enable_outputs(mdev);

	mdev->suspended = false;

	dev_dbg(mdev->dev, "In normal mode\n");

	return 0;
}

static void mga_g450_monitor_sense_dac1(struct mga_dev *mdev)
{
	bool r, g, b;

	mga_crtc1_wait_vblank(mdev);
	mga_g450_dac1_monitor_sense_start(mdev);
	mga_crtc1_wait_vblank(mdev);
	mga_g450_dac1_monitor_sense_stop(mdev, &r, &g, &b);

	dev_dbg(mdev->dev, "Monitor sense = %s%s%s\n",
		r ? "R" : ".",
		g ? "G" : ".",
		b ? "B" : ".");
}

static void mga_g450_monitor_sense_dac2(struct mga_dev *mdev)
{
	bool r, g, b;

	mga_crtc1_wait_vblank(mdev);
	mga_g450_dac2_monitor_sense_start(mdev);
	mga_crtc1_wait_vblank(mdev);
	mga_g450_dac2_monitor_sense_stop(mdev, &r, &g, &b);

	dev_dbg(mdev->dev, "Monitor sense = %s%s%s\n",
		r ? "R" : ".",
		g ? "G" : ".",
		b ? "B" : ".");
}

static const struct mga_chip_funcs mga_g450_funcs = {
	.rfhcnt = mga_g100_rfhcnt,
	.monitor_sense_dac1 = mga_g450_monitor_sense_dac1,
	.monitor_sense_dac2 = mga_g450_monitor_sense_dac2,
	.suspend = mga_g450_suspend,
	.resume = mga_g450_resume,
	.init = mga_g450_init,
	.set_mode = mga_g450_set_mode,
	.test = mga_g450_test,
	.softreset = mga_1064sg_softreset,
};

void mga_g450_prepare(struct mga_dev *mdev)
{
	mdev->funcs = &mga_g450_funcs;
}
