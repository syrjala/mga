/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "mga_dump.h"

static void dump_rom_data(struct mga_dev *mdev)
{
	unsigned int i;
	unsigned int max;
	unsigned int bytes_left = mdev->rom_size;
	u8 *data = mdev->rom_data;

	while (bytes_left) {
		max = min(16, bytes_left);
		for (i = 0; i < max; i++)
			dev_dbg(mdev->dev, "0x%02x ", data[i]);
		dev_dbg(mdev->dev, "\n");
		bytes_left -= max;
		data += max;
	}
}

u8 rom_read8(struct mga_dev *mdev, unsigned int off)
{
	return mdev->rom_data[off];
}

u16 rom_read16(struct mga_dev *mdev, unsigned int off)
{
	return (mdev->rom_data[off+1] << 8) | mdev->rom_data[off];
}

u32 rom_read32(struct mga_dev *mdev, unsigned int off)
{
	return (mdev->rom_data[off+3] << 24) | (mdev->rom_data[off+2] << 16) | (mdev->rom_data[off+1] << 8) | mdev->rom_data[off];
}

enum {
	MGA_BIOS_SIGNATURE = 0xAA55,

	MGA_PINS_LOCATION = 0x7FFC,
	MGA_PINS_SIGNATURE = 0x412E,
	MGA_PINS1_LENGTH = 0x40,
	MGA_PINS4_LENGTH = 0x80,
	MGA_PINS1_VER_MIN = 0x100,
	MGA_PINS1_VER_MAX = 0x105,
};

void dump_pins(struct mga_dev *mdev)
{
	unsigned int max;
	unsigned int pins_off, pins_len, pins_ver;

	read_rom(mdev);
	if (!mdev->rom_size)
		return;

	dev_dbg(mdev->dev, "\nGoing to look for PInS\n");

	max = min(mdev->rom_size, 0xFF80);

	if (rom_read16(mdev, 0) == MGA_BIOS_SIGNATURE) {
		dev_dbg(mdev->dev, "BIOS signature detected. Using the easy way.\n");
		pins_off = rom_read16(mdev, MGA_PINS_LOCATION);
		goto got_pins_off;
	}

	dev_dbg(mdev->dev, "No BIOS signature. Going to look the hard way.\n");
	/* Look for PInS the hard way. */
	for (pins_off = 0; pins_off <= max; pins_off++) {
		u16 sig = rom_read16(mdev, pins_off);

		if (sig == MGA_PINS_SIGNATURE) {
			/* PInS >= 2.0? */
			u8 len = rom_read8(mdev, pins_off + 2);
			if (len == MGA_PINS1_LENGTH || len == MGA_PINS4_LENGTH)
				break;
		} else if (sig == MGA_PINS1_LENGTH) {
			/* PiNS 1.0? */
			u16 ver = rom_read16(mdev, pins_off + 56);
			if (ver >= MGA_PINS1_VER_MIN && ver <= MGA_PINS1_VER_MAX)
				break;
		}
	}

 got_pins_off:
	if (pins_off > max) {
		dev_err(mdev->dev, "Failed to locate PInS data\n");
		return;
	}

	dev_dbg(mdev->dev, "Checking PInS at offset %04x\n", pins_off);

	if (rom_read16(mdev, pins_off) == MGA_PINS_SIGNATURE) {
		u8 csum = 0;
		unsigned int i;

		dev_dbg(mdev->dev, "PInS signature found.\n");

		pins_len = rom_read8(mdev, pins_off + 2);
		pins_ver = rom_read16(mdev, pins_off + 4);

		for (i = 0; i < pins_len; i++)
			csum += rom_read8(mdev, pins_off + i);

		if (csum) {
			dev_err(mdev->dev, "PInS checksum is invalid.\n");
			return;
		}
	} else {
		dev_dbg(mdev->dev, "No PInS signature. Testing for PInS version 1.0.\n");

		pins_len = rom_read16(mdev, pins_off);
		if (pins_len != MGA_PINS1_LENGTH) {
			dev_err(mdev->dev, "PInS length %04x doesn't match PInS 1.0.\n", pins_len);
			return;
		}

		pins_ver = rom_read16(mdev, pins_off + 56);
		if (pins_ver < 0x100 || pins_ver > 0x105) {
			dev_err(mdev->dev, "PInS version %04x doesn't match PInS 1.0.\n", pins_ver);
			return;
		}
	}
	dev_dbg(mdev->dev, "PInS version 0x%04x.\n", pins_ver);
	dev_dbg(mdev->dev, "PInS length %u bytes\n", pins_len);

	mdev->pins_ptr = pins_off;

	switch (pins_ver >> 8) {
	case 0x01:
		dump_pins1(mdev);
		break;
	case 0x02:
		dump_pins2(mdev);
		break;
	case 0x03:
		dump_pins3(mdev);
		break;
	case 0x04:
		dump_pins4(mdev);
		break;
	case 0x05:
		dump_pins5(mdev);
		break;
	default:
		dev_err(mdev->dev, "Unimplemented PInS format\n");
		return;
	}

	dev_dbg(mdev->dev, "%u bytes processed\n", mdev->pins_ptr - pins_off);
}

void read_rom(struct mga_dev *mdev)
{
	ssize_t r;

	dev_dbg(mdev->dev, "Going to read ROM\n");

	if (mdev->rom_size)
		return;

	r = read(mdev->fd, mdev->rom_data, sizeof mdev->rom_data);
	if (r < 0) {
		dev_err(mdev->dev, "Failed to read ROM: %s\n", strerror(errno));
		return;
	}

	dev_dbg(mdev->dev, "Read ROM %zd bytes\n", r);
	mdev->rom_size = r;
}

void dump_rom(struct mga_dev *mdev)
{
	read_rom(mdev);
	if (!mdev->rom_size)
		return;

	dev_dbg(mdev->dev, "Dump of ROM (%u bytes):\n", mdev->rom_size);
	dump_rom_data(mdev);
}
