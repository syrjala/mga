/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include "mga_dump.h"
#include "mga_kms.h"
#include "mga_dma.h"
#include "mga_regs.h"
#include "mga_bes.h"
#include "tvp3026.h"

static void mga_plane_destroy(struct drm_plane *plane)
{
	struct mga_plane *p = to_mga_plane(plane);
	struct drm_device *dev = plane->dev;

	dev->mode_config.num_plane--;
	list_del(&plane->head);

	drm_mode_object_fini(&plane->base);

	kfree(p);
}

static void mga_crtc_destroy(struct drm_crtc *crtc)
{
	struct mga_crtc *c = to_mga_crtc(crtc);
	struct drm_device *dev = crtc->dev;

	dev->mode_config.num_crtc--;
	list_del(&crtc->head);

	drm_mode_object_fini(&crtc->base);

	kfree(c);
}

static void mga_encoder_destroy(struct drm_encoder *encoder)
{
	struct mga_encoder *e = to_mga_encoder(encoder);
	struct drm_device *dev = encoder->dev;

	dev->mode_config.num_encoder--;
	list_del(&encoder->head);

	drm_mode_object_fini(&encoder->base);

	kfree(e);
}

static void mga_connector_destroy(struct drm_connector *connector)
{
	struct mga_connector *c = to_mga_connector(connector);
	struct drm_device *dev = connector->dev;

	dev->mode_config.num_connector--;
	list_del(&connector->head);

	drm_mode_object_fini(&connector->base);

	kfree(c);
}

static int mga_plane_init(struct drm_device *dev, unsigned int plane,
			  unsigned int possible_crtcs)
{
	struct mga_plane *p;
	int r;

	p = kzalloc(sizeof *p, GFP_KERNEL);
	if (!p)
		return -ENOMEM;

	r = drm_mode_object_init(dev, &p->base.base, DRM_MODE_OBJECT_PLANE);
	if (r) {
		kfree(p);
		return r;
	}

	p->base.dev = dev;
	p->base.destroy = mga_plane_destroy;
	p->base.possible_crtcs = possible_crtcs;
	p->plane = plane;

	list_add_tail(&p->base.head, &dev->mode_config.plane_list);
	dev->mode_config.num_plane++;

	dev_dbg(to_mga_device(dev)->dev, "%s: plane=0x%x, id=%u\n",
		__func__, plane, p->base.base.id);

	return 0;
}

static int mga_crtc_init(struct drm_device *dev, unsigned int crtc)
{
	struct mga_crtc *c;
	int r;

	c = kzalloc(sizeof *c, GFP_KERNEL);
	if (!c)
		return -ENOMEM;

	r = drm_mode_object_init(dev, &c->base.base, DRM_MODE_OBJECT_CRTC);
	if (r) {
		kfree(c);
		return r;
	}

	c->base.dev = dev;
	c->base.destroy = mga_crtc_destroy;
	c->crtc = crtc;

	list_add_tail(&c->base.head, &dev->mode_config.crtc_list);
	dev->mode_config.num_crtc++;

	dev_dbg(to_mga_device(dev)->dev, "%s: crtc=0x%x, id=%u\n",
		__func__, crtc, c->base.base.id);

	return 0;
}

static int mga_encoder_init(struct drm_device *dev, unsigned int output,
			    unsigned int possible_crtcs)
{
	struct mga_encoder *e;
	int r;

	e = kzalloc(sizeof *e, GFP_KERNEL);
	if (!e)
		return -ENOMEM;

	r = drm_mode_object_init(dev, &e->base.base, DRM_MODE_OBJECT_ENCODER);
	if (r) {
		kfree(e);
		return r;
	}

	e->base.dev = dev;
	e->base.destroy = mga_encoder_destroy;
	e->base.possible_crtcs = possible_crtcs;
	e->output = output;

	list_add_tail(&e->base.head, &dev->mode_config.encoder_list);
	dev->mode_config.num_encoder++;

	dev_dbg(to_mga_device(dev)->dev, "%s: output=0x%x, id=%u\n",
		__func__, output, e->base.base.id);

	return 0;
}

static int mga_connector_init(struct drm_device *dev, unsigned int output)
{
	struct mga_connector *c;
	int r;

	c = kzalloc(sizeof *c, GFP_KERNEL);
	if (!c)
		return -ENOMEM;

	r = drm_mode_object_init(dev, &c->base.base, DRM_MODE_OBJECT_CONNECTOR);
	if (r) {
		kfree(c);
		return r;
	}

	c->base.dev = dev;
	c->base.destroy = mga_connector_destroy;
	c->output = output;

	list_add_tail(&c->base.head, &dev->mode_config.connector_list);
	dev->mode_config.num_connector++;

	dev_dbg(to_mga_device(dev)->dev, "%s: output=0x%x, id=%u\n",
		__func__, output, c->base.base.id);

	return 0;
}

static unsigned int mga_g400_clone_outputs(const struct mga_dev *mdev,
					   unsigned int output)
{
	unsigned int outputs = 0;

	BUG_ON(!(output & mdev->outputs));

	/*
	 * Everything except the primary DAC uses the MAFC
	 * port. As there is only one MAFC port the
	 * cloning options are a bit limited.
	 */
	switch (output) {
	case MGA_OUTPUT_DAC1:
		outputs = MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS1 | MGA_OUTPUT_TVOUT;
		break;
	case MGA_OUTPUT_DAC2:
	case MGA_OUTPUT_TMDS1:
	case MGA_OUTPUT_TVOUT:
		outputs = MGA_OUTPUT_DAC1;
		break;
	default:
		BUG();
	}

	/* remove non-existing outputs */
	return outputs & mdev->outputs;
}

static unsigned int mga_g450_clone_outputs(const struct mga_dev *mdev,
					   unsigned int output)
{
	unsigned int outputs = 0;

	BUG_ON(!(output & mdev->outputs));

	/* FIXME dual-DVI mode needs some more thought perhaps */
	/*
	 * TV-out is special on G450/G550 as it needs BT.656
	 * mode, and only the TV encoder understands BT.656.
	 */
	switch (output) {
	case MGA_OUTPUT_DAC1:
	case MGA_OUTPUT_TMDS1:
		outputs = MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS2;
		break;
	case MGA_OUTPUT_DAC2:
	case MGA_OUTPUT_TMDS2:
		outputs = MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1;
		break;
	case MGA_OUTPUT_TVOUT:
		break;
	default:
		BUG();
	}

	/* remove non-existing outputs */
	return outputs & mdev->outputs;
}

static void mga_setup_clones(struct mga_dev *mdev, struct drm_encoder *encoder)
{
	struct drm_device *dev = &mdev->base;
	struct drm_encoder *clone_encoder;
	unsigned int output = to_mga_encoder(encoder)->output;
	unsigned int index = 0;
	unsigned int outputs;

	/*
	 * FIXME does possible_clones mean which encoders
	 * can be driven by the same CRTC simultaneosly?
	 */
	if (mdev->chip >= MGA_CHIP_G450)
		outputs = mga_g450_clone_outputs(mdev, output);
	else
		outputs = mga_g400_clone_outputs(mdev, output);

	list_for_each_entry(clone_encoder, &dev->mode_config.encoder_list, head) {
		if (to_mga_encoder(clone_encoder)->output & outputs)
			encoder->possible_clones |= 1 << index;

		index++;
	}
}

static void mga_init_plane_config(struct mga_plane_config *pc,
				  unsigned int crtc)
{
	pc->crtc = crtc;
	pc->rotation = DRM_ROTATE_0;
}

static int mga_crtc_index(struct drm_device *dev, unsigned int crtc)
{
	struct drm_crtc *dcrtc;
	int index = 0;

	list_for_each_entry(dcrtc, &dev->mode_config.crtc_list, head) {
		if (to_mga_crtc(dcrtc)->crtc == crtc)
			return index;
		index++;
	}

	return -ENOENT;
}

int mga_modeset_init(struct mga_dev *mdev)
{
	struct drm_device *dev = &mdev->base;
	struct drm_encoder *encoder;
	int r;
	unsigned int output = MGA_OUTPUT_DAC1;
	unsigned int outputs = mdev->outputs;
	int crtc_index;

	drm_modeset_init(dev,
			 mga_framebuffer_create,
			 mga_framebuffer_destroy,
			 mga_framebuffer_pre_destroy);

	mutex_lock(&dev->mode_config.mutex);

	r = mga_crtc_init(dev, MGA_CRTC_CRTC1);
	if (r)
		goto out;;

	if (mdev->chip >= MGA_CHIP_G400) {
		r = mga_crtc_init(dev, MGA_CRTC_CRTC2);
		if (r)
			goto out;;
	}

	crtc_index = mga_crtc_index(dev, MGA_CRTC_CRTC1);

	r = mga_plane_init(dev, MGA_PLANE_CRTC1, crtc_index);
	if (r)
		goto out;;

	r = mga_plane_init(dev, MGA_PLANE_CURSOR, crtc_index);
	if (r)
		goto out;;

	if (mdev->chip >= MGA_CHIP_G200) {
		r = mga_plane_init(dev, MGA_PLANE_BES, crtc_index);
		if (r)
			goto out;;
	}

	if (mdev->chip >= MGA_CHIP_G400) {
		crtc_index = mga_crtc_index(dev, MGA_CRTC_CRTC2);

		r = mga_plane_init(dev, MGA_PLANE_CRTC2, crtc_index);
		if (r)
			goto out;;

		r = mga_plane_init(dev, MGA_PLANE_SPIC, crtc_index);
		if (r)
			goto out;;
	}

	while (outputs) {
		unsigned int possible_crtcs;

		if (!(outputs & output)) {
			output <<= 1;
			continue;
		}

		if (mdev->chip >= MGA_CHIP_G450 &&
		    (output == MGA_OUTPUT_TMDS2 || output == MGA_OUTPUT_TVOUT))
			possible_crtcs =
				1 << mga_crtc_index(dev, MGA_CRTC_CRTC2);
		else if (mdev->chip >= MGA_CHIP_G400)
			possible_crtcs =
				1 << mga_crtc_index(dev, MGA_CRTC_CRTC1) |
				1 << mga_crtc_index(dev, MGA_CRTC_CRTC2);
		else
			possible_crtcs =
				1 << mga_crtc_index(dev, MGA_CRTC_CRTC1);

		r = mga_encoder_init(dev, output, possible_crtcs);
		if (r)
			goto out;;

		r = mga_connector_init(dev, output);
		if (r)
			goto out;;

		outputs &= ~output;
		output <<= 1;
	}

	list_for_each_entry(encoder, &dev->mode_config.encoder_list, head)
		mga_setup_clones(mdev, encoder);

	if (mdev->chip >= MGA_CHIP_1064SG) {
		r = mga_mem_alloc(mdev->fb_alloc, &mdev->cursor.bus,
				  &mdev->cursor.virt, 4096, true);
		if (r)
			goto out;
	}

	mga_init_plane_config(&mdev->mode_config.crtc1.plane_config, MGA_CRTC_CRTC1);
	mga_init_plane_config(&mdev->mode_config.cursor.plane_config, MGA_CRTC_CRTC1);

	if (mdev->chip >= MGA_CHIP_G200)
		mga_init_plane_config(&mdev->mode_config.bes.plane_config, MGA_CRTC_CRTC1);

	if (mdev->chip >= MGA_CHIP_G400) {
		mga_init_plane_config(&mdev->mode_config.crtc2.plane_config, MGA_CRTC_CRTC2);
		mga_init_plane_config(&mdev->mode_config.spic.plane_config, MGA_CRTC_CRTC2);
	}

 out:
	mutex_unlock(&dev->mode_config.mutex);

	return r;
}

void mga_modeset_fini(struct mga_dev *mdev)
{
	struct drm_device *dev = &mdev->base;

	if (mdev->chip >= MGA_CHIP_1064SG)
		mga_mem_free(mdev->fb_alloc, mdev->cursor.bus, 4096);

	drm_modeset_fini(dev);
}

static struct mga_plane_config *mga_plane_state_get(struct mga_mode_config *mc, unsigned int plane)
{
	switch (plane) {
	case MGA_PLANE_CRTC1:
		return &mc->crtc1.plane_config;
	case MGA_PLANE_CURSOR:
		return &mc->cursor.plane_config;
	case MGA_PLANE_BES:
		return &mc->bes.plane_config;
	case MGA_PLANE_CRTC2:
		return &mc->crtc2.plane_config;
	case MGA_PLANE_SPIC:
		return &mc->spic.plane_config;
	default:
		BUG();
	}
}

static struct mga_crtc_config *mga_crtc_state_get(struct mga_mode_config *mc, unsigned int crtc)
{
	switch (crtc) {
	case MGA_CRTC_CRTC1:
		return &mc->crtc1.crtc_config;
	case MGA_CRTC_CRTC2:
		return &mc->crtc2.crtc_config;
	default:
		BUG();
	}
}

static void *mga_atomic_begin(struct drm_device *dev, unsigned int flags)
{
	struct mga_dev *mdev = to_mga_device(dev);
	struct mga_mode_config *mc;

	mc = kmemdup(&mdev->mode_config, sizeof *mc, GFP_KERNEL);
	if (!mc)
		return ERR_PTR(-ENOMEM);

	return mc;
}

static const struct drm_color empty_lut[256];

/* FIXME maybe something fancier eventually */
static bool mga_atomic_plane_uses_empty_fb(struct mga_dev *mdev,
					   struct mga_mode_config *mc,
					   unsigned int plane)
{
	const struct mga_plane_config *pc = mga_plane_state_get(mc, plane);

	return pc->fb == NULL || pc->fb == &mdev->empty_fb.base;
}

/* FIXME maybe something fancier eventually */
static bool mga_atomic_plane_modified(struct mga_dev *mdev,
				      struct mga_mode_config *mc,
				      unsigned int plane)
{
	const struct mga_plane_config *pc_old = mga_plane_state_get(&mdev->mode_config, plane);
	const struct mga_plane_config *pc_new = mga_plane_state_get(mc, plane);

	return memcmp(pc_old, pc_new, sizeof *pc_old);
}

static int mga_atomic_plane_set(struct drm_device *dev,
				struct mga_mode_config *mc,
				unsigned int plane,
				const struct drm_property *prop,
				uint64_t prop_value, void *blob_data)
{
	struct mga_dev *mdev = to_mga_device(dev);
	struct mga_plane_config *pc = mga_plane_state_get(mc, plane);
	const struct drm_mode_config *config = &dev->mode_config;

	if (prop == &config->prop_src_x) {
		if (pc->src_x == prop_value)
			return 0;
		pc->src_x = prop_value;
	} else if (prop == &config->prop_src_y) {
		if (pc->src_y == prop_value)
			return 0;
		pc->src_y = prop_value;
	} else if (prop == &config->prop_src_w) {
		if (pc->src_w == prop_value)
			return 0;
		pc->src_w = prop_value;
	} else if (prop == &config->prop_src_h) {
		if (pc->src_h == prop_value)
			return 0;
		pc->src_h = prop_value;
	} else if (prop == &config->prop_crtc_x) {
		if (pc->crtc_x == (int32_t)prop_value)
			return 0;
		pc->crtc_x = prop_value;
	} else if (prop == &config->prop_crtc_y) {
		if (pc->crtc_y == (int32_t)prop_value)
			return 0;
		pc->crtc_y = prop_value;
	} else if (prop == &config->prop_crtc_w) {
		if (pc->crtc_w == prop_value)
			return 0;
		pc->crtc_w = prop_value;
	} else if (prop == &config->prop_crtc_h) {
		if (pc->crtc_h == prop_value)
			return 0;
		pc->crtc_h = prop_value;
	} else if (prop == &config->prop_fb_id) {
		struct drm_framebuffer *fb = NULL;

		if (prop_value) {
			fb = drm_framebuffer_find(dev, prop_value);
			if (!fb)
				return -ENOENT;

			if (!(to_mga_framebuffer(fb)->possible_planes & plane))
				return -EINVAL;
		}

		if (pc->fb == fb)
			return 0;

		pc->fb = fb;
	} else if (prop == &config->prop_crtc_id) {
		struct drm_crtc *crtc;

		if (!prop_value)
			return -EINVAL;

		crtc = drm_crtc_find(dev, prop_value);
		if (!crtc)
			return -ENOENT;

		/* All planes are fixed to one CRTC */
		if (pc->crtc != to_mga_crtc(crtc)->crtc)
			return -EINVAL;
	} else if (prop == &config->prop_rotate) {
		unsigned int rotation;

		if (plane != MGA_PLANE_BES)
			return -EINVAL;

		switch (prop_value) {
		case DRM_ROTATE_0:
		case DRM_ROTATE_0 | DRM_REFLECT_X:
			rotation = prop_value;
			break;
		default:
			return -EINVAL;
		}

		if (pc->rotation == rotation)
			return 0;

		pc->rotation = rotation;
	} else if (prop == &config->prop_lut) {
		struct drm_color *lut;
		size_t size;

		switch (plane) {
		case MGA_PLANE_CRTC1:
			lut = mc->crtc1.lut;
			size = sizeof mc->crtc1.lut;
			break;
		case MGA_PLANE_CURSOR:
			lut = mc->cursor.lut;
			size = sizeof mc->cursor.lut;
			if (mdev->chip < MGA_CHIP_G200)
				size = 3 * sizeof mc->cursor.lut[0];
			break;
		case MGA_PLANE_SPIC:
			lut = mc->spic.lut;
			size = sizeof mc->spic.lut;
			break;
		default:
			return -EINVAL;
		}

		if (prop_value != size)
			return -EINVAL;

		if (!memcmp(lut, blob_data, size))
			return 0;

		memcpy(lut, blob_data, size);

		if (plane == MGA_PLANE_CRTC1)
			mc->dirty |= MGA_DIRTY_CRTC1_LUT;
	} else if (prop == &config->prop_overlay) {
		if (plane != MGA_PLANE_CRTC1)
			return -EINVAL;

		switch (prop_value) {
		case 0:
		case 1:
			break;
		default:
			return -EINVAL;
		}

		if (pc->overlay == prop_value)
			return 0;

		pc->overlay = prop_value;
	} else if (prop == &config->prop_overlay_color_key) {
		if (plane != MGA_PLANE_CRTC1)
			return -EINVAL;

		if (prop_value > 0xff)
			return -EINVAL;

		if (pc->overlay_ckey == prop_value)
			return 0;

		pc->overlay_ckey = prop_value;
	} else
		return -EINVAL;

	/*
	 * FIXME:
	 * - BES deinterlace mode
	 * - cursor color mode
	 * - cursor palette
	 * - CRTC1 palette
	 * - etc.
	 */

	switch (plane) {
	case MGA_PLANE_CRTC1:
		mc->dirty |= MGA_DIRTY_CRTC1;
		break;
	case MGA_PLANE_CURSOR:
		mc->dirty |= MGA_DIRTY_CURSOR;
		break;
	case MGA_PLANE_BES:
		mc->dirty |= MGA_DIRTY_BES;
		break;
	case MGA_PLANE_CRTC2:
		mc->dirty |= MGA_DIRTY_CRTC2;
		break;
	case MGA_PLANE_SPIC:
		mc->dirty |= MGA_DIRTY_SPIC;
		break;
	default:
		BUG();
	}

	return 0;
}

static int mga_connectors_to_outputs(struct drm_device *dev, struct mga_crtc_config *cc,
				     const uint32_t *connector_id, int num_connector_id,
				     unsigned int *outputs)
{
	const struct drm_mode_config *config = &dev->mode_config;
	int i;

	*outputs = 0;

	for (i = 0; i < num_connector_id; i++) {
		struct drm_connector *connector;
		unsigned int output;

		if (!connector_id[i])
			return -EINVAL;

		connector = drm_connector_find(dev, connector_id[i]);
		if (!connector)
			return -ENOENT;

		output = to_mga_connector(connector)->output;

		/* reject duplicates */
		if (*outputs & output)
			return -EINVAL;

		*outputs |= output;
	}

	return 0;
}

static int mga_check_umode(const struct drm_umode *umode)
{
	const struct drm_display_mode *mode = &umode->mode;

	if (mode->flags & ~(DRM_MODE_FLAG_PHSYNC | DRM_MODE_FLAG_NHSYNC |
			    DRM_MODE_FLAG_PVSYNC | DRM_MODE_FLAG_NVSYNC |
			    DRM_MODE_FLAG_PCSYNC | DRM_MODE_FLAG_NCSYNC |
			    DRM_MODE_FLAG_CSYNC | DRM_MODE_FLAG_IOGSYNC |
			    DRM_MODE_FLAG_INTERLACE | DRM_MODE_FLAG_DBLSCAN))
		return -EINVAL;

	if (mode->flags & DRM_MODE_FLAG_PHSYNC &&
	    mode->flags & DRM_MODE_FLAG_NHSYNC)
		return -EINVAL;

	if (mode->flags & DRM_MODE_FLAG_PVSYNC &&
	    mode->flags & DRM_MODE_FLAG_NVSYNC)
		return -EINVAL;

	if (mode->flags & DRM_MODE_FLAG_PCSYNC &&
	    mode->flags & DRM_MODE_FLAG_NCSYNC)
		return -EINVAL;

	if (mode->hdisplay == 0)
		return -EINVAL;

	if (mode->vdisplay == 0)
		return -EINVAL;

	if (mode->flags & DRM_MODE_FLAG_INTERLACE) {
		unsigned int vtotal = mode->vtotal;

		if (mode->flags & DRM_MODE_FLAG_DBLSCAN)
			vtotal = (vtotal << 1) | 1;

		if (!(vtotal & 1))
			return -ENOSYS;
	}

	if (1) {
		bool dblclk = false;//mode->flags & DRM_MODE_FLAG_DBLCLK;
		bool dblscan = mode->flags & DRM_MODE_FLAG_DBLSCAN;
		bool interlace = mode->flags & DRM_MODE_FLAG_INTERLACE;
		unsigned int htotal = mode->htotal << dblclk;
		unsigned int vtotal = (mode->vtotal << dblscan) >> interlace;

		if (htotal > 8192)
			return -EINVAL;
		if (vtotal > 8192)
			return -EINVAL;
	}

	if (!mode->vrefresh && !mode->clock)
		return -EINVAL;

	if (mode->hdisplay & 7)
		return -ENOSYS;

	if (mode->hskew || mode->vscan)
		return -ENOSYS;

	if (mode->flags & ~(DRM_MODE_FLAG_INTERLACE | DRM_MODE_FLAG_DBLSCAN))
		return -ENOSYS;

	return 0;
}

static int mga_atomic_crtc_set(struct drm_device *dev,
			       struct mga_mode_config *mc,
			       unsigned int crtc,
			       struct drm_property *prop,
			       uint64_t prop_value, void *blob_data)
{
	struct mga_crtc_config *cc = mga_crtc_state_get(mc, crtc);
	const struct drm_mode_config *config = &dev->mode_config;

	if (prop == &config->prop_mode) {
		struct drm_display_mode *mode = NULL;

		if (prop_value) {
			const struct drm_umode *umode = blob_data;
			int r;

			if (prop_value != sizeof *umode)
				return -EINVAL;

			r = mga_check_umode(umode);
			if (r)
				return r;

			mode = drm_mode_create(dev);
			if (!mode)
				return -ENOMEM;

			r = drm_convert_umode(mode, umode);
			if (r) {
				drm_mode_destroy(dev, mode);
				return r;
			}

			if (cc->mode_valid &&
			    drm_mode_equal(&cc->user_mode, mode)) {
				drm_mode_destroy(dev, mode);
				return 0;
			}
		} else if (!cc->mode_valid)
			return 0;

		cc->mode_valid = mode != NULL;

		if (mode) {
			drm_mode_copy(&cc->user_mode, mode);
			drm_mode_destroy(dev, mode);
		} else
			memset(&cc->user_mode, 0, sizeof cc->user_mode);
	} else if (prop == &config->prop_connector_ids) {
		int r;
		unsigned int outputs;

		if (prop_value % sizeof(uint32_t))
			return -EINVAL;

		r = mga_connectors_to_outputs(dev, cc, blob_data,
					      prop_value/sizeof(uint32_t), &outputs);
		if (r)
			return r;

		if (cc->outputs == outputs)
			return 0;

		cc->outputs = outputs;
	} else
		return -EINVAL;

	switch (crtc) {
	case MGA_CRTC_CRTC1:
		mc->dirty |= MGA_DIRTY_CRTC1;
		break;
	case MGA_CRTC_CRTC2:
		mc->dirty |= MGA_DIRTY_CRTC2;
		break;
	default:
		BUG();
	}

	return 0;
}

static int mga_atomic_connector_set(struct drm_device *dev,
				    struct mga_mode_config *mc,
				    unsigned int output,
				    struct drm_property *prop,
				    uint64_t prop_value, void *blob_data)
{
	struct mga_dev *mdev = to_mga_device(dev);
	const struct drm_mode_config *config = &dev->mode_config;
	struct mga_tvo_config *tvo_config = &mc->tvo_config;
	const struct mga_crtc_config *cc;

	if (output != MGA_OUTPUT_TVOUT)
		return -EINVAL;

	if (!(mdev->outputs & MGA_OUTPUT_TVOUT))
		return -EINVAL;

	if (prop == &config->prop_tv_std) {
		int r;

		switch (prop_value) {
		case DRM_TV_STD_PAL:
		case DRM_TV_STD_NTSC:
			break;
		default:
			return -EINVAL;
		}

		if (tvo_config->tv_standard == prop_value)
			return 0;

		tvo_config->tv_standard = prop_value;
	} else if (prop == &config->prop_cable_type) {
		int r;

		switch (prop_value) {
		case MGA_TVO_CABLE_COMPOSITE_SVIDEO:
		case MGA_TVO_CABLE_SCART_RGB:
			/*
			 * FIXME need to check if TVout addon,
			 * G200 mystique, G200 marvel, G400 marvel,
			 * G450 marvel can do SCART output...
			 */
			break;
		case MGA_TVO_CABLE_SCART_TYPE2:
			/* G450/G550 can do this */
			if (mdev->chip >= MGA_CHIP_G450)
				return -EINVAL;
			break;
		default:
			return -EINVAL;
		}

		if (tvo_config->cable_type == prop_value)
			return 0;

		tvo_config->cable_type = prop_value;
	} else if (prop == &config->prop_deflicker) {
		int r;

		if (mdev->chip >= MGA_CHIP_G450)
			return -EINVAL;

		switch (prop_value) {
		case 0:
		case 1:
		case 2:
			break;
		default:
			return -EINVAL;
		}

		if (tvo_config->deflicker == prop_value)
			return 0;

		tvo_config->deflicker = prop_value;
	} else if (prop == &config->prop_text_filter) {
		int r;

		if (mdev->chip >= MGA_CHIP_G450)
			return -EINVAL;

		switch (prop_value) {
		case 0:
		case 1:
			break;
		default:
			return -EINVAL;
		}

		if (tvo_config->text_filter == prop_value)
			return 0;

		tvo_config->text_filter = prop_value;
	} else if (prop == &config->prop_color_bars) {
		int r;

		if (mdev->chip >= MGA_CHIP_G450)
			return -EINVAL;

		switch (prop_value) {
		case 0:
		case 1:
			break;
		default:
			return -EINVAL;
		}

		if (tvo_config->color_bars == prop_value)
			return 0;

		tvo_config->color_bars = prop_value;
	} else if (prop == &config->prop_dot_crawl_freeze) {
		int r;

		switch (prop_value) {
		case 0:
		case 1:
			break;
		default:
			return -EINVAL;
		}

		if (tvo_config->dot_crawl_freeze == prop_value)
			return 0;

		tvo_config->dot_crawl_freeze = prop_value;
	} else if (prop == &config->prop_gamma) {
		int r;

		if (prop_value < 4 || prop_value > 40)
			return -EINVAL;

		if (tvo_config->gamma == prop_value)
			return 0;

		tvo_config->gamma = prop_value;
	} else
		return -EINVAL;

	cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	if (cc->outputs & MGA_OUTPUT_TVOUT)
		mc->dirty |= MGA_DIRTY_CRTC1;

	if (mdev->chip >= MGA_CHIP_G400) {
		cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);
		if (cc->outputs & MGA_OUTPUT_TVOUT)
			mc->dirty |= MGA_DIRTY_CRTC2;
	}

	return 0;
}

static int mga_atomic_set(struct drm_device *dev, void *state,
			  struct drm_mode_object *obj,
			  struct drm_property *prop,
			  uint64_t prop_value, void *blob_data)
{
	struct mga_mode_config *mc = state;
	int r = -EINVAL;

	switch (obj->type) {
	case DRM_MODE_OBJECT_PLANE:
		r = mga_atomic_plane_set(dev, mc,
					 to_mga_plane(obj_to_plane(obj))->plane,
					 prop, prop_value, blob_data);
		break;
	case DRM_MODE_OBJECT_CRTC:
		r = mga_atomic_crtc_set(dev, mc,
					to_mga_crtc(obj_to_crtc(obj))->crtc,
					prop, prop_value, blob_data);
		break;
	case DRM_MODE_OBJECT_CONNECTOR:
		r = mga_atomic_connector_set(dev, mc,
					     to_mga_connector(obj_to_connector(obj))->output,
					     prop, prop_value, blob_data);
		break;
	default:
		break;
	}

	kfree(blob_data);

	return r;
}

static struct drm_framebuffer *mga_empty_framebuffer(struct mga_dev *mdev,
						     unsigned int pixel_format,
						     unsigned int width,
						     unsigned int height)
{
	struct mga_framebuffer *mfb = &mdev->empty_fb;
	struct drm_framebuffer *fb = &mfb->base;
	unsigned int cpp = drm_format_plane_cpp(pixel_format, 0);

	fb->pixel_format = pixel_format;
	fb->width = width;
	fb->height = height;
	fb->pitches[0] = ALIGN(width * cpp, crtc1_pitch_alignment(mdev));

	/*
	 * FIXME make sure we have anough memory to satify this.
	 * or perhaps we don't need to because the access might
	 * wrap anyway. Check it anyway.
	 */
	mfb->size = mga_framebuffer_calc_size(fb);
	mfb->possible_planes = MGA_PLANE_CRTC1;

	/*
	 * Leave fb address/offset at 0. It doesn't matter what
	 * data is stored there since and all black palette will
	 * be used to avoid showing anything scanned out.
	 */

	/*
	 * FIXME so we need to worry about 2064W/2164W bank crossing
	 * limitaion?
	 */

	return fb;
}

static unsigned int mga_max_hzoom(const struct mga_dev *mdev,
				  unsigned int pixel_format)
{
	if (mdev->chip <= MGA_CHIP_2164W && pixel_format != DRM_FORMAT_RGB888)
		return 32;

	return 4;
}

static unsigned int mga_calc_max_hzoom(const struct mga_dev *mdev,
				       unsigned int pixel_format,
				       unsigned int hdisplay)
{
	unsigned int hzoom;

	for (hzoom = mga_max_hzoom(mdev, pixel_format);
	     hzoom; hzoom >>= 1) {
		if (hdisplay % hzoom == 0)
			return hzoom;
	}

	BUG();
}

static unsigned int mga_calc_max_vzoom(unsigned int vdisplay)
{
	unsigned int vzoom;

	for (vzoom = 32; vzoom; vzoom--) {
		if (vdisplay % vzoom == 0)
			return vzoom;
	}

	BUG();
}

static int mga_crtc1_check_overlay(const struct drm_framebuffer *fb,
				   const struct mga_crtc_config *cc)
{
	if (!fb)
		return 0;

	switch (fb->pixel_format) {
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_XRGB8888:
		break;
	default:
		if (cc->overlay)
			return -EINVAL;
		break;
	}

	return 0;
}

static int mga_tvout_check_bt656(const struct mga_dev *mdev,
				 const struct mga_mode_config *mc,
				 const struct mga_crtc_config *cc)
{
	const struct drm_display_mode *mode = &cc->adjusted_mode;

	BUG_ON(!(cc->outputs & MGA_OUTPUT_TVOUT));
	BUG_ON(!cc->bt656);

	/* Only MGA-TVO understands BT.656 */
	if (cc->outputs != MGA_OUTPUT_TVOUT)
		return -EINVAL;

	/* no BT.656 mode on G100/G200 */
	if (mdev->chip < MGA_CHIP_G400)
		return -EINVAL;

	/* Only "broadcast" modes are supported. */
	switch (mc->tvo_config.tv_standard) {
	case DRM_TV_STD_PAL:
		if (mode->hdisplay     != 720 ||
		    mode->hblank_start != 720 ||
		    mode->hblank_width != 144 ||
		    mode->htotal       != 864 ||
		    mode->vdisplay     != 576 ||
		    mode->vblank_start != 576 ||
		    mode->vblank_width !=  49 ||
		    mode->vtotal       != 625)
			return -EINVAL;
		break;
	case DRM_TV_STD_NTSC:
		if (mode->hdisplay     != 720 ||
		    mode->hblank_start != 720 ||
		    mode->hblank_width != 138 ||
		    mode->htotal       != 858 ||
		    mode->vdisplay     != 480 ||
		    mode->vblank_start != 480 ||
		    mode->vblank_width !=  45 ||
		    mode->vtotal       != 525)
			return -EINVAL;
		break;
	default:
		BUG();
		break;
	}

	return 0;
}

static int mga_tvout_mode_fixup(const struct mga_dev *mdev,
				struct mga_mode_config *mc,
				struct mga_crtc_config *cc)
{
	struct drm_display_mode *mode = &cc->adjusted_mode;
	unsigned int vtotal = mc->tvo_config.tv_standard == DRM_TV_STD_PAL ? 625 : 525;

	BUG_ON(!(cc->outputs & MGA_OUTPUT_TVOUT));

	if (cc->bt656)
		return mga_tvout_check_bt656(mdev, mc, cc);

	/* only BT.656 mode support on G450/G550 */
	if (mdev->chip >= MGA_CHIP_G450)
		return -EINVAL;

	/* MGA-TVO doesn't do vertical upscaling */
	if (mode->vtotal < vtotal) {
		mode->vsync_start += (vtotal - mode->vtotal) >> 1;
		mode->vtotal = vtotal;
	}

	return 0;
}

static bool mga_outputs_need_tvo(const struct mga_dev *mdev,
				 unsigned int outputs)
{
	if (mdev->chip >= MGA_CHIP_G450)
		return outputs & MGA_OUTPUT_TVOUT;

	/*
	 * TVO pulls on VIDRST so AV9110 PLL can't
	 * provide the clock, so we must resort to
	 * the TVO PLL. Only AV9110 and TVO can
	 * provide the clock on the VIDRST line,
	 * and the flat panel addon is wired so
	 * that it needs the clock on VIDRST,
	 * so we can never use other PLLs with the
	 * flat panel addon.
	 */
	if (mdev->chip == MGA_CHIP_G400 &&
	    outputs & MGA_OUTPUT_TMDS1 &&
	    mdev->features & MGA_FEAT_TVO_BUILTIN)
		return true;

	return outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT);
}

static int mga_g400_check_tvo(const struct mga_dev *mdev,
			      struct mga_mode_config *mc)
{
	struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CRTC1);

	if (!mga_outputs_need_tvo(mdev, cc->outputs))
		return 0;

	if (cc->bt656)
		mc->tvo_config.mode = MGA_TVO_MODE_BT656;
	else if (cc->outputs & MGA_OUTPUT_TVOUT)
		mc->tvo_config.mode = MGA_TVO_MODE_TVOUT;
	else
		mc->tvo_config.mode = MGA_TVO_MODE_CRT;

	if (mdev->chip >= MGA_CHIP_G450)
		return mga_g450_tvo_check_config(&mdev->tvodev_g450,
						 &mc->tvo_config);
	else
		return mga_tvo_check_config(&mdev->tvodev,
					    &mc->tvo_config);
}

static void mga_check_dac(const struct mga_dev *mdev,
			 struct mga_mode_config *mc)
{
	struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CRTC1);

	mc->dac_config.hzoom = 0x10000 / pc->hscale;
	if (cc->overlay &&
	    (pc->fb->pixel_format == DRM_FORMAT_XRGB1555 ||
	     pc->fb->pixel_format == DRM_FORMAT_XRGB8888)) {
		mc->dac_config.enable_overlay = true;
		mc->dac_config.gfx_dst_ckey = pc->overlay_ckey;
	} else {
		mc->dac_config.enable_overlay = false;
		mc->dac_config.gfx_dst_ckey = 0;
	}
	mc->dac_config.hzoom = 0x10000 / pc->hscale;
}

static int mga_g400_crtc1_mode_fixup(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CRTC1);
	int r;

	if (pc->overlay)
		cc->overlay = true;

	if (cc->outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
		cc->vidrst = true;

	r = mga_g400_check_tvo(mdev, mc);
	if (r)
		return r;

	if (cc->outputs & MGA_OUTPUT_TVOUT) {
		r = mga_tvout_mode_fixup(mdev, mc, cc);
		if (r)
			return r;
	}

	r = mga_crtc1_check_overlay(pc->fb, cc);
	if (r)
		return r;

	return 0;
}

static int mga_g400_crtc2_mode_fixup(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CRTC2);
	struct drm_display_mode *mode = &cc->adjusted_mode;
	int r;

	BUG_ON(pc->overlay);

	if (mode->flags & DRM_MODE_FLAG_INTERLACE)
		cc->bt656 = true;
	else if (cc->outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
		cc->vidrst = true;

	r = mga_g400_check_tvo(mdev, mc);
	if (r)
		return r;

	if (cc->outputs & MGA_OUTPUT_TVOUT) {
		r = mga_tvout_mode_fixup(mdev, mc, cc);
		if (r)
			return r;
	}

	return 0;
}

static unsigned int mga_plane_bpp(struct mga_mode_config *mc, unsigned int plane)
{
	const struct mga_plane_config *pc = mga_plane_state_get(mc, plane);

	if (!pc->fb)
		return 0;

	return drm_format_plane_cpp(pc->fb->pixel_format, 0) * 8;
}

static bool mga_crtc2_is_bt656(struct mga_mode_config *mc)
{
	const struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);

	return cc->bt656;
}

static bool mga_crtc_pixclk(struct mga_mode_config *mc, unsigned int crtc)
{
	const struct mga_crtc_config *cc = mga_crtc_state_get(mc, crtc);
	const struct drm_display_mode *mode = &cc->adjusted_mode;

	return mode->clock != 0;
}

static int mga_atomic_check_common(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	int r = 0;

	if (mdev->chip >= MGA_CHIP_G400) {
		unsigned int crtc1_bpp = mga_plane_bpp(mc, MGA_PLANE_CRTC1);
		unsigned int crtc2_bpp = mga_plane_bpp(mc, MGA_PLANE_CRTC2);
		unsigned int crtc1_pixclk = mga_crtc_pixclk(mc, MGA_CRTC_CRTC1);
		unsigned int crtc2_pixclk = mga_crtc_pixclk(mc, MGA_CRTC_CRTC2);

		r = mga_g400_hipri(mdev, mdev->mclk,
				   crtc1_pixclk, crtc2_pixclk,
				   crtc1_bpp, crtc2_bpp, mga_crtc2_is_bt656(mc),
				   &mc->crtc1.regs, &mc->crtc2.regs);
	} else if (mdev->chip >= MGA_CHIP_G200) {
		unsigned int pixclk = mga_crtc_pixclk(mc, MGA_CRTC_CRTC1);

		r = mga_g200_hipri(mdev, mdev->mclk, pixclk, &mc->crtc1.regs);
	}

	return r;
}

static void mga_calc_cursor(struct mga_dev *mdev,
			    const struct drm_region *src,
			    const struct drm_region *dmc,
			    const struct drm_framebuffer *fb);

static int mga_set_mode(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	int r;

	if (mc->dirty & MGA_DIRTY_CRTC1_MODE) {
		struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
		struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CRTC1);

		if (cc->mode_valid) {
			printk("hzoom = %u, vzoom = %u\n",
			       0x10000 / pc->hscale, 0x10000 / pc->vscale);
			printk("x = %u, y = %u\n",
			       pc->src.x1 >> 16, pc->src.y1 >> 16);
		}

		mdev->hzoom = 0x10000 / pc->hscale;
		mdev->vzoom = 0x10000 / pc->vscale;
	}

	if (mc->dirty & MGA_DIRTY_CRTC2_MODE) {
		struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);
		struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CRTC2);

		if (cc->mode_valid) {
			printk("hzoom = %u, vzoom = %u\n",
			       0x10000 / pc->hscale, 0x10000 / pc->vscale);
			printk("x = %u, y = %u\n",
			       pc->src.x1 >> 16, pc->src.y1 >> 16);
		}
	}

	if (mc->dirty & (MGA_DIRTY_CRTC1_MODE | MGA_DIRTY_CRTC2_MODE)) {
		r = mdev->funcs->set_mode(mdev, mc);
		if (r)
			return r;
	}

	if (mc->dirty & MGA_DIRTY_CRTC1_FB)
		mga_crtc1_restore_framebuffer(mdev, &mc->crtc1.regs);

	/* FIXME integrate all planes w/ modeset properly */
	if (mc->dirty & MGA_DIRTY_BES)
		mga_bes_restore(mdev, &mc->bes.regs);

	if (mc->dirty & MGA_DIRTY_CURSOR) {
		struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CURSOR);

		if (drm_region_visible(&pc->dst)) {
			const struct drm_color colors[16] = {
				{ .r = 0x00, .g = 0x00, .b = 0x00, },
				{ .r = 0xff, .g = 0xff, .b = 0xff, },
				{ .r = 0xff, .g = 0x00, .b = 0x00, },
				{ .r = 0x00, .g = 0xff, .b = 0x00, },
				{ .r = 0x00, .g = 0x00, .b = 0xff, },
				{ .r = 0x00, .g = 0xff, .b = 0xff, },
				{ .r = 0xff, .g = 0x00, .b = 0xff, },
				{ .r = 0xff, .g = 0xff, .b = 0x00, },

				{ .r = 0x00, .g = 0x00, .b = 0x00, },
				{ .r = 0xff, .g = 0xff, .b = 0xff, },
				{ .r = 0xff, .g = 0x00, .b = 0x00, },
				{ .r = 0x00, .g = 0xff, .b = 0x00, },
				{ .r = 0x00, .g = 0x00, .b = 0xff, },
				{ .r = 0x00, .g = 0xff, .b = 0xff, },
				{ .r = 0xff, .g = 0x00, .b = 0xff, },
				{ .r = 0xff, .g = 0xff, .b = 0x00, },
			};
			mga_dac_crtc1_cursor_mode(mdev, 4);
			mga_dac_crtc1_restore_cursor_color(mdev, colors);
			mga_calc_cursor(mdev, &pc->src, &pc->dst, pc->fb);
		} else {
			mga_dac_crtc1_cursor_mode(mdev, 0);
		}
	}

	if (mc->dirty & MGA_DIRTY_CRTC1_LUT) {
		const struct drm_color *lut = mc->crtc1.lut;

		if (mga_atomic_plane_uses_empty_fb(mdev, mc, MGA_PLANE_CRTC1))
			lut = empty_lut;

		if (mdev->chip <= MGA_CHIP_2164W)
			tvp_load_palette(&mdev->tdev, lut);
		else
			mga_dac_crtc1_load_palette(mdev, lut);
	}

	if (mc->dirty & MGA_DIRTY_CRTC2_FB)
		mga_crtc2_restore_framebuffer(mdev, &mc->crtc2.regs);

	//if (mc->dirty & MGA_DIRTY_SPIC)
	//xxx

	if (mga_atomic_plane_uses_empty_fb(mdev, mc, MGA_PLANE_CRTC1)) {
		/* empty_fb shouldn't leak out */
		mc->crtc1.plane_config.fb = NULL;
	} else if (mc->crtc1.plane_config.fb) {
		struct drm_framebuffer *empty_fb = &mdev->empty_fb.base;
		struct drm_framebuffer *fb = mc->crtc1.plane_config.fb;

		/*
		 * If the user sets fb=NULL next time around, we don't want
		 * a full modeset, so we should retain the same pixel format
		 * and zoom settings.
		 */
		empty_fb->pixel_format = fb->pixel_format;
		empty_fb->width = fb->width;
		empty_fb->height = fb->height;
	}

	return 0;
}

static const char *mga_plane_name(unsigned int plane)
{
	switch (plane) {
	case MGA_PLANE_CRTC1:
		return "CRTC1";
	case MGA_PLANE_CRTC2:
		return "CRTC1";
	case MGA_PLANE_BES:
		return "BES";
	case MGA_PLANE_CURSOR:
		return "CURSOR";
	case MGA_PLANE_SPIC:
		return "SPIC";
	default:
		BUG();
	}
}

static const char *mga_crtc_name(unsigned int crtc)
{
	switch (crtc) {
	case MGA_CRTC_CRTC1:
		return "CRTC1";
	case MGA_CRTC_CRTC2:
		return "CRTC2";
	default:
		BUG();
	}
}

/*
 * Can be used to check CRTC1/CRTC2/SPIC planes
 */
static int mga_atomic_plane_check(struct mga_dev *mdev,
				  unsigned int plane,
				  struct mga_plane_config *pc,
				  const struct mga_crtc_config *cc)
{
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	unsigned int max_hzoom, max_vzoom;
	unsigned int hzoom, vzoom;

	/* no subpixel coords */
	if (pc->src.x1 & 0xffff || pc->src.x2 & 0xffff ||
	    pc->src.y1 & 0xffff || pc->src.y2 & 0xffff)
		return -EINVAL;

	switch (plane) {
	case MGA_PLANE_CRTC1:
		max_hzoom = mga_max_hzoom(mdev, pc->fb->pixel_format);
		max_vzoom = 32;
		break;
	case MGA_PLANE_CRTC2:
		max_hzoom = 1;
		max_vzoom = 1;
		break;
	case MGA_PLANE_SPIC:
		max_hzoom = 1;
		max_vzoom = 2; /* can use half height sub-picture */
		break;
	default:
		BUG();
	}

	pc->hscale = drm_calc_hscale(&pc->src, &pc->dst, 0x10000 / max_hzoom, 0x10000);
	if (pc->hscale < 0)
		return pc->hscale;

	pc->vscale = drm_calc_vscale(&pc->src, &pc->dst, 0x10000 / max_vzoom, 0x10000);
	if (pc->vscale < 0)
		return pc->vscale;

	/* hzoom/vzoom provide limited scaling capabilities */
	hzoom = 0x10000 / pc->hscale;
	vzoom = 0x10000 / pc->vscale;

	if (hzoom < 1 || hzoom > max_hzoom)
		return -EINVAL;

	if (hzoom & (hzoom - 1))
		return -EINVAL;

	if (vzoom < 1 || vzoom > max_vzoom)
		return -EINVAL;

	/* scaling must be exact */
	if (pc->hscale * hzoom != 0x10000 || pc->vscale * vzoom != 0x10000)
		return -EINVAL;

	/* CRTC off -> no mode -> no clip region -> don't care about the rest */
	if (!cc->mode_valid) {
		drm_region_empty(&pc->src);
		drm_region_empty(&pc->dst);
		return 0;
	}

	drm_region_clip_scaled(&pc->dst, &pc->src,
			       &cc->clip, pc->hscale, pc->vscale);

	/* no subpixel coords */
	if (pc->src.x1 & 0xffff || pc->src.y1 & 0xffff ||
	    pc->src.x2 & 0xffff || pc->src.y2 & 0xffff)
		return -EINVAL;

	/* plane must be fullscreen */
	if (drm_region_width(&pc->dst) != (int)(mode->hdisplay << 16) ||
	    drm_region_height(&pc->dst) != (int)(mode->vdisplay << 16))
		return -EINVAL;

	printk("%s OK\n", mga_plane_name(plane));
	drm_region_print(&pc->src, "src", false);
	drm_region_print(&pc->dst, "dst", false);
	drm_region_print(&cc->clip, "clip", false);

	return 0;
}

static int mga_atomic_check_crtc1(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CRTC1);
	struct drm_display_mode *mode = &cc->adjusted_mode;
	unsigned int hzoom, vzoom;
	unsigned int max_hzoom;
	int r;

	if (cc->mode_valid) {
		/*
		 * CRTC1 plane can be "disabled" by
		 * using a fake fb and all black LUT.
		 */

		/* silly user */
		if (!cc->outputs)
			return -EINVAL;
	} else {
		/* silly user */
		if (cc->outputs)
			return -EINVAL;
	}

	if (!pc->fb) {
		struct drm_framebuffer *empty_fb = &mdev->empty_fb.base;
		unsigned int pixel_format = empty_fb->pixel_format;
		unsigned int width, height;

		if (!cc->mode_valid) {
			/* plane and crtc both disabled, nothing more to do here */
			drm_region_empty(&pc->src);
			drm_region_empty(&pc->dst);
			return 0;
		}

		hzoom = mdev->hzoom;
		vzoom = mdev->vzoom;

		/*
		 * Set up a fake framebuffer, which in conjunction with an
		 * all black palette will show black content for the plane.
		 *
		 * Use maximum hzoom/vzoom to monimize bandwidth requirements.
		 */

		if (!pixel_format)
			pixel_format = DRM_FORMAT_C8;

		if (hzoom > mga_max_hzoom(mdev, pixel_format))
			hzoom = 0;
		if (vzoom > 32)
			vzoom = 0;

		if (hzoom == 0 || mode->hdisplay % hzoom) {
			width = mode->hdisplay;
			hzoom = mga_calc_max_hzoom(mdev, pixel_format, width);
			width /= hzoom;
		} else
			width = mode->hdisplay / hzoom;

		if (vzoom == 0 || mode->vdisplay % vzoom) {
			height = mode->hdisplay;
			vzoom = mga_calc_max_vzoom(height);
			height /= vzoom;
		} else
			height = mode->vdisplay / vzoom;

		pc->fb = mga_empty_framebuffer(mdev, pixel_format, width, height);

		drm_region_init(&pc->src, 0, 0, width << 16, height << 16);
		drm_region_init(&pc->dst, 0, 0, width * hzoom, height * vzoom);
	} else {
		r = drm_framebuffer_check_viewport(pc->fb,
						   pc->src_x, pc->src_y,
						   pc->src_w, pc->src_h);
		if (r)
			return r;

		drm_region_init(&pc->src,
				pc->src_x, pc->src_y,
				pc->src_w, pc->src_h);

		drm_region_init(&pc->dst,
				pc->crtc_x, pc->crtc_y,
				pc->crtc_w, pc->crtc_h);
	}

	drm_region_init(&cc->clip, 0, 0,
			mode->hdisplay, mode->vdisplay);

	r = mga_atomic_plane_check(mdev, MGA_PLANE_CRTC1, pc, cc);
	if (r)
		return r;

	r = mga_g400_crtc1_mode_fixup(mdev, mc);
	if (r)
		return r;

	r = mga_crtc1_calc_mode(mdev, pc, cc, &mc->crtc1.regs);
	if (r)
		return r;

	hzoom = 0x10000 / pc->hscale;

	mode->clock = mga_calc_pixel_clock(mode, hzoom);
	if (!mode->clock)
		return -EINVAL;

	return 0;
}

static int mga_atomic_check_cursor(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CURSOR);
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	int r;

	/* fb=NULL -> disabled plane, ignore all other params */
	if (!pc->fb) {
		drm_region_empty(&pc->src);
		drm_region_empty(&pc->dst);
		return 0;
	}

	/* no subpixel coords */
	if (pc->src_x & 0xffff || pc->src_y & 0xffff ||
	    pc->src_w & 0xffff || pc->src_h & 0xffff)
		return -EINVAL;

	if (pc->fb) {
		r = drm_framebuffer_check_viewport(pc->fb,
						   pc->src_x, pc->src_y,
						   pc->src_w, pc->src_h);
		if (r)
			return r;
	}

	drm_region_init(&pc->src,
			pc->src_x, pc->src_y,
			pc->src_w, pc->src_h);

	drm_region_init(&pc->dst,
			pc->crtc_x, pc->crtc_y,
			pc->crtc_w, pc->crtc_h);

	/* fixed size */
	if (drm_region_width(&pc->src) != 64 << 16 ||
	    drm_region_height(&pc->src) != 64 << 16)
		return -EINVAL;

	/* no scaling */
	if (drm_region_width(&pc->src) != (int)(pc->crtc_w << 16) ||
	    drm_region_height(&pc->src) != (int)(pc->crtc_h << 16))
		return -EINVAL;

	/* CRTC disabled, no mode to check */
	if (!cc->mode_valid) {
		drm_region_empty(&pc->src);
		drm_region_empty(&pc->dst);
		return 0;
	}

	if (pc->dst.x2 <= 0 || pc->dst.x1 >= (int)mode->hdisplay ||
	    pc->dst.y2 <= 0 || pc->dst.y1 >= (int)mode->vdisplay) {
		drm_region_empty(&pc->src);
		drm_region_empty(&pc->dst);
	}

	printk("CURSOR OK\n");
	drm_region_print(&pc->src, "src", false);
	drm_region_print(&pc->dst, "dst", false);
	drm_region_print(&cc->clip, "clip", false);

	return 0;
}

static int mga_atomic_check_bes(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_BES);
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	int r;
	int max_width = mdev->chip >= MGA_CHIP_G450 ? 2048 : 1024;
	int max_height = 1024;
	bool hzoom = false;
	unsigned int cpp;

	/* fb=NULL -> disabled plane, ignore all other params */
	if (!pc->fb) {
		drm_region_empty(&pc->src);
		drm_region_empty(&pc->dst);
		return 0;
	}

	r = drm_framebuffer_check_viewport(pc->fb,
					   pc->src_x, pc->src_y,
					   pc->src_w, pc->src_h);
	if (r)
		return r;

	cpp = drm_format_plane_cpp(pc->fb->pixel_format, 0);

	if (pc->fb->flags & DRM_FB_INTERLACED) {
		/* FIXME deinterlace mode prop:
		 * enum:
		 * DRM_DEINTERLACE_MODE:
		 * - BOP_TOP_FIELD
		 * - BOP_BOTTOM_FIELD
		 */
		pc->deinterlace = false;
	}

	/*
	 * Must check pitch again, since the original check
	 * was done with the assumption that deinterlacing
	 * is not used.
	 */
	if ((pc->fb->pitches[0] << pc->deinterlace) / cpp > MGA_BESPITCH_BESPITCH)
		return -EINVAL;

	drm_region_init(&pc->src,
			pc->src_x, pc->src_y,
			pc->src_w, pc->src_h);

	drm_region_rotate(&pc->src,
			  pc->fb->width << 16,
			  pc->fb->height << 16,
			  pc->rotation);

	drm_region_print(&pc->src, "src", true);

	if (pc->deinterlace) {
		pc->src.y1 >>= 1;
		pc->src.y2 >>= 1;
	}

	drm_region_init(&pc->dst,
			pc->crtc_x, pc->crtc_y,
			pc->crtc_w, pc->crtc_h);

	hzoom = mga_bes_calc_hzoom(mdev, mode);

	pc->hscale = drm_calc_hscale(&pc->src, &pc->dst,
				     1 << 2, ((32 >> hzoom) << 16) - 1);
	if (pc->hscale < 0)
		return pc->hscale;

	pc->vscale = drm_calc_vscale(&pc->src, &pc->dst,
				     1 << 2, (32 << 16) - 1);
	if (pc->vscale < 0)
		return pc->vscale;

	/* hardware will ignore these bits */
	pc->hscale &= ~0x3;
	pc->vscale &= ~0x3;

	if (mdev->chip == MGA_CHIP_G400) {
		switch (pc->fb->pixel_format) {
		case DRM_FORMAT_ARGB8888:
		case DRM_FORMAT_XRGB8888:
			if (pc->hscale != 0x10000)
				return -EINVAL;
			max_width = 512;
			break;
		default:
			break;
		}
	}

	/* FIXME limit pre-clipped size? */

	/* CRTC disabled, nothing more to check */
	if (!cc->mode_valid) {
		drm_region_empty(&pc->src);
		drm_region_empty(&pc->dst);
		return 0;
	}

	drm_region_print(&pc->src, "src", true);

	drm_region_clip_scaled(&pc->dst, &pc->src,
			       &cc->clip, pc->hscale, pc->vscale);

	drm_region_print(&pc->src, "src", true);

	/* make src == dst * scale, exactly */
	drm_region_adjust_size(&pc->src,
			       drm_region_width(&pc->dst) * pc->hscale - drm_region_width(&pc->src),
			       drm_region_height(&pc->dst) * pc->vscale - drm_region_height(&pc->src));

	if (drm_region_width(&pc->src) > (max_width << 16) ||
	    drm_region_height(&pc->src) > (max_height << 16))
		return -EINVAL;

	printk("BES OK\n");
	drm_region_print(&pc->src, "src", true);
	drm_region_print(&pc->dst, "dst", false);
	drm_region_print(&cc->clip, "clip", false);

	mga_bes_calc(mdev, pc, cc, &mc->bes.regs);

	return 0;
}

static int mga_atomic_check_crtc2(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_CRTC2);
	struct drm_display_mode *mode = &cc->adjusted_mode;
	int r;

	if (cc->mode_valid) {
		/* don't think we can disable plane only */
		if (!pc->fb)
			return -EINVAL;

		/* silly user */
		if (!cc->outputs)
			return -EINVAL;
	} else {
		/* silly user */
		if (cc->outputs)
			return -EINVAL;
	}

	drm_region_init(&cc->clip, 0, 0,
			mode->hdisplay, mode->vdisplay);

	if (pc->fb) {
		r = drm_framebuffer_check_viewport(pc->fb,
						   pc->src_x, pc->src_y,
						   pc->src_w, pc->src_h);
		if (r)
			return r;
	}

	drm_region_init(&pc->src,
			pc->src_x, pc->src_y,
			pc->src_w, pc->src_h);

	drm_region_init(&pc->dst,
			pc->crtc_x, pc->crtc_y,
			pc->crtc_w, pc->crtc_h);

	r = mga_atomic_plane_check(mdev, MGA_PLANE_CRTC2, pc, cc);
	if (r)
		return r;

	r = mga_g400_crtc2_mode_fixup(mdev, mc);
	if (r)
		return r;

	r = mga_crtc2_calc_mode(mdev, pc, cc, &mc->crtc2.regs);
	if (r)
		return r;

	mode->clock = mga_calc_pixel_clock(mode, 1);
	if (!mode->clock)
		return -EINVAL;

	return 0;
}

/* make sure CRTC2 format is YUV420 when SPIC is enabled */
static int mga_atomic_check_crtc2_spic(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_plane_config *crtc2 = mga_plane_state_get(mc, MGA_PLANE_CRTC2);
	const struct mga_plane_config *spic = mga_plane_state_get(mc, MGA_PLANE_SPIC);

	/* sub-picture disabled, CRTC2 can do whatever it wants */
	if (!spic->fb)
		return 0;

	/* can't enable sub-picture when CRTC2 is not using 3 plane 4:2:0 format */
	if (crtc2->fb &&
	    crtc2->fb->pixel_format != DRM_FORMAT_YUV420 &&
	    crtc2->fb->pixel_format != DRM_FORMAT_YVU420)
		return -EINVAL;

	return 0;
}

static int mga_atomic_check_spic(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_crtc_config *cc = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);
	struct mga_plane_config *pc = mga_plane_state_get(mc, MGA_PLANE_SPIC);
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	int r;

	if (!pc->fb) {
		drm_region_empty(&pc->src);
		drm_region_empty(&pc->dst);
		return 0;
	}

	r = drm_framebuffer_check_viewport(pc->fb,
					   pc->src_x, pc->src_y,
					   pc->src_w, pc->src_h);
	if (r)
		return r;

	drm_region_init(&pc->src,
			pc->src_x, pc->src_y,
			pc->src_w, pc->src_h);

	drm_region_init(&pc->dst,
			pc->crtc_x, pc->crtc_y,
			pc->crtc_w, pc->crtc_h);

	r = mga_atomic_plane_check(mdev, MGA_PLANE_SPIC, pc, cc);
	if (r)
		return r;

	return 0;
}

static int mga_2064w_check_outputs(const struct mga_dev *mdev,
				   unsigned int outputs)
{
	/* all possible outputs */
	BUG_ON(mdev->outputs & ~MGA_OUTPUT_DAC1);

	return 0;
}

static int mga_1064sg_check_outputs(const struct mga_dev *mdev,
				    unsigned int outputs)
{
	/* all possible outputs */
	BUG_ON(mdev->outputs & ~(MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1 |
				 MGA_OUTPUT_TVOUT));

	/* only one MAFC port */
	if (hweight32(outputs & (MGA_OUTPUT_TMDS1 | MGA_OUTPUT_TVOUT)) > 1)
		return -EINVAL;

	return 0;
}

static int mga_g400_check_outputs(const struct mga_dev *mdev,
				  unsigned int crtc1_outputs,
				  unsigned int crtc2_outputs)
{
	/* all possible outputs */
	BUG_ON(mdev->outputs & ~(MGA_OUTPUT_DAC1 | MGA_OUTPUT_DAC2 |
				 MGA_OUTPUT_TMDS1 | MGA_OUTPUT_TVOUT));

	/* only one MAFC port */
	if (hweight32(crtc1_outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS1 | MGA_OUTPUT_TVOUT)) +
	    hweight32(crtc2_outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS1 | MGA_OUTPUT_TVOUT)) > 1)
		return -EINVAL;

	return 0;
}

static int mga_g450_check_outputs(const struct mga_dev *mdev,
				  unsigned int crtc1_outputs,
				  unsigned int crtc2_outputs)
{
	/* all possible outputs */
	BUG_ON(mdev->outputs & ~(MGA_OUTPUT_DAC1 | MGA_OUTPUT_DAC2 |
				 MGA_OUTPUT_TMDS1 | MGA_OUTPUT_TMDS2 |
				 MGA_OUTPUT_TVOUT));

	/* CRTC1 can drive anything except TMDS2 and TV-out */
	if (crtc1_outputs & ~(MGA_OUTPUT_DAC1 | MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS1))
		return -EINVAL;

	/* CRTC2 can drive anything */
	if (crtc2_outputs & ~(MGA_OUTPUT_DAC1 | MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS1 |
			      MGA_OUTPUT_TMDS2 | MGA_OUTPUT_TVOUT))
		return -EINVAL;

	/* only one output for connector #1 */
	if (hweight32(crtc1_outputs & (MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1)) +
	    hweight32(crtc2_outputs & (MGA_OUTPUT_DAC1 | MGA_OUTPUT_TMDS1)) > 1)
		return -EINVAL;

	/* only one output for connector #2 */
	if (hweight32(crtc1_outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS2)) +
	    hweight32(crtc2_outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TMDS2 | MGA_OUTPUT_TVOUT)) > 1)
		return -EINVAL;

	return 0;
}

static int mga_atomic_check_outputs(const struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_crtc_config *crtc1 = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	const struct mga_crtc_config *crtc2 = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);
	int r;

	BUG_ON(mdev->chip < MGA_CHIP_G400 && crtc2->outputs);
	BUG_ON((crtc1->outputs | crtc2->outputs) & ~mdev->outputs);

	/* the same output can't be driven by multiple CRTCs */
	if (crtc1->outputs & crtc2->outputs)
		return -EINVAL;

	if (mdev->chip >= MGA_CHIP_G450)
		r = mga_g450_check_outputs(mdev, crtc1->outputs, crtc2->outputs);
	else if (mdev->chip >= MGA_CHIP_G400)
		r = mga_g400_check_outputs(mdev, crtc1->outputs, crtc2->outputs);
	else if (mdev->chip >= MGA_CHIP_1064SG)
		r = mga_1064sg_check_outputs(mdev, crtc1->outputs);
	else
		r = mga_2064w_check_outputs(mdev, crtc1->outputs);

	return r;
}

static unsigned int mga_calc_hzoom(const struct mga_plane_config *pc)
{
	unsigned int dw = drm_region_width(&pc->dst);
	unsigned int sw = drm_region_width(&pc->src);

	if (!sw)
		return 0;

	return dw / sw;
}

static unsigned int mga_calc_vzoom(const struct mga_plane_config *pc)
{
	unsigned int dh = drm_region_height(&pc->dst);
	unsigned int sh = drm_region_height(&pc->src);

	if (!sh)
		return 0;

	return dh / sh;
}

static unsigned int mga_calc_cpp(const struct drm_framebuffer *fb)
{
	if (!fb)
		return 0;

	return drm_format_plane_cpp(fb->pixel_format, 0);
}

static void mga_crtc1_pre_calc_dirty(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_mode_config *mc_old = &mdev->mode_config;
	const struct mga_mode_config *mc_new = mc;
	const struct mga_crtc_config *cc_old = mga_crtc_state_get(&mdev->mode_config, MGA_CRTC_CRTC1);
	const struct mga_crtc_config *cc_new = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	const struct mga_plane_config *pc_old = mga_plane_state_get(&mdev->mode_config, MGA_PLANE_CRTC1);
	const struct mga_plane_config *pc_new = mga_plane_state_get(mc, MGA_PLANE_CRTC1);

	mc->dirty &= ~MGA_DIRTY_CRTC1;

	if (cc_new->outputs & MGA_OUTPUT_TVOUT &&
	    mc_old->tvo_config.tv_standard != mc_new->tvo_config.tv_standard)
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (cc_old->outputs != cc_new->outputs)
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (cc_old->mode_valid != cc_new->mode_valid ||
	    !drm_mode_equal(&cc_old->user_mode, &cc_new->user_mode))
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (pc_old->fb != pc_new->fb)
		mc->dirty |= MGA_DIRTY_CRTC1_FB;

	if (pc_old->fb && pc_new->fb &&
	    mga_calc_cpp(pc_old->fb) != mga_calc_cpp(pc_new->fb))
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (pc_old->src_x != pc_new->src_x ||
	    pc_old->src_y != pc_new->src_y ||
	    pc_old->src_w != pc_new->src_w ||
	    pc_old->src_h != pc_new->src_h)
		mc->dirty |= MGA_DIRTY_CRTC1_FB;

	if (pc_old->crtc_x != pc_new->crtc_x ||
	    pc_old->crtc_y != pc_new->crtc_y ||
	    pc_old->crtc_w != pc_new->crtc_w ||
	    pc_old->crtc_h != pc_new->crtc_h)
		mc->dirty |= MGA_DIRTY_CRTC1_FB;

	if (pc_old->overlay != pc_new->overlay)
		mc->dirty |= MGA_DIRTY_CRTC1_FB;

	if (mc->dirty & MGA_DIRTY_CRTC1_LUT &&
	    memcmp(mdev->mode_config.crtc1.lut, mc->crtc1.lut, sizeof mc->crtc1.lut))
		mc->dirty |= MGA_DIRTY_CRTC1_LUT;
	else
		mc->dirty &= ~MGA_DIRTY_CRTC1_LUT;
}

static void mga_crtc1_post_calc_dirty(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_mode_config *mc_old = &mdev->mode_config;
	const struct mga_mode_config *mc_new = mc;
	const struct mga_crtc_config *cc_old = mga_crtc_state_get(&mdev->mode_config, MGA_CRTC_CRTC1);
	const struct mga_crtc_config *cc_new = mga_crtc_state_get(mc, MGA_CRTC_CRTC1);
	const struct mga_plane_config *pc_old = mga_plane_state_get(&mdev->mode_config, MGA_PLANE_CRTC1);
	const struct mga_plane_config *pc_new = mga_plane_state_get(mc, MGA_PLANE_CRTC1);

	BUG_ON(cc_old->bt656 || cc_new->bt656);

	mc->dirty &= ~MGA_DIRTY_CRTC1;

	if (cc_new->outputs & MGA_OUTPUT_TVOUT &&
	    mc_old->tvo_config.tv_standard != mc_new->tvo_config.tv_standard)
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (cc_old->outputs != cc_new->outputs)
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (cc_old->mode_valid != cc_new->mode_valid ||
	    !drm_mode_equal(&cc_old->adjusted_mode, &cc_new->adjusted_mode))
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (cc_old->vidrst != cc_new->vidrst ||
	    cc_old->overlay != cc_new->overlay)
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (pc_old->hscale != pc_new->hscale) {
		if (mdev->chip <= MGA_CHIP_2164W)
			mc->dirty |= MGA_DIRTY_CRTC1_MODE;
		else
			mc->dirty |= MGA_DIRTY_CRTC1_FB;
	}

	if (pc_old->vscale != pc_new->vscale)
		mc->dirty |= MGA_DIRTY_CRTC1_FB;

	if (pc_old->fb && pc_new->fb &&
	    mga_calc_cpp(pc_old->fb) != mga_calc_cpp(pc_new->fb))
		mc->dirty |= MGA_DIRTY_CRTC1_MODE;

	if (pc_old->src.x1 != pc_new->src.x1 ||
	    pc_old->src.y1 != pc_new->src.y1)
		mc->dirty |= MGA_DIRTY_CRTC1_FB;

	if (pc_old->fb != pc_new->fb)
		mc->dirty |= MGA_DIRTY_CRTC1_FB;

	if (pc_old->overlay != pc_new->overlay)
		mc->dirty |= MGA_DIRTY_CRTC1_FB;

	if (mc->dirty & MGA_DIRTY_CRTC1_LUT &&
	    memcmp(mdev->mode_config.crtc1.lut, mc->crtc1.lut, sizeof mc->crtc1.lut))
		mc->dirty |= MGA_DIRTY_CRTC1_LUT;
	else
		mc->dirty &= ~MGA_DIRTY_CRTC1_LUT;
}

static void mga_crtc2_pre_calc_dirty(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_mode_config *mc_old = &mdev->mode_config;
	const struct mga_mode_config *mc_new = mc;
	const struct mga_crtc_config *cc_old = mga_crtc_state_get(&mdev->mode_config, MGA_CRTC_CRTC2);
	const struct mga_crtc_config *cc_new = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);
	const struct mga_plane_config *pc_old = mga_plane_state_get(&mdev->mode_config, MGA_PLANE_CRTC2);
	const struct mga_plane_config *pc_new = mga_plane_state_get(mc, MGA_PLANE_CRTC2);

	mc->dirty &= ~MGA_DIRTY_CRTC2;

	if (cc_new->outputs & MGA_OUTPUT_TVOUT &&
	    mc_old->tvo_config.tv_standard != mc_new->tvo_config.tv_standard)
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (cc_old->outputs != cc_new->outputs)
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (cc_old->mode_valid != cc_new->mode_valid ||
	    !drm_mode_equal(&cc_old->user_mode, &cc_new->user_mode))
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (pc_old->fb != pc_new->fb)
		mc->dirty |= MGA_DIRTY_CRTC2_FB;

	if (pc_old->fb && pc_new->fb &&
	    mga_calc_cpp(pc_old->fb) != mga_calc_cpp(pc_new->fb))
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (pc_old->src_x != pc_new->src_x ||
	    pc_old->src_y != pc_new->src_y ||
	    pc_old->src_w != pc_new->src_w ||
	    pc_old->src_h != pc_new->src_h)
		mc->dirty |= MGA_DIRTY_CRTC2_FB;

	if (pc_old->crtc_x != pc_new->crtc_x ||
	    pc_old->crtc_y != pc_new->crtc_y ||
	    pc_old->crtc_w != pc_new->crtc_w ||
	    pc_old->crtc_h != pc_new->crtc_h)
		mc->dirty |= MGA_DIRTY_CRTC2_FB;
}

static void mga_crtc2_post_calc_dirty(struct mga_dev *mdev, struct mga_mode_config *mc)
{
	const struct mga_mode_config *mc_old = &mdev->mode_config;
	const struct mga_mode_config *mc_new = mc;
	const struct mga_crtc_config *cc_old = mga_crtc_state_get(&mdev->mode_config, MGA_CRTC_CRTC2);
	const struct mga_crtc_config *cc_new = mga_crtc_state_get(mc, MGA_CRTC_CRTC2);
	const struct mga_plane_config *pc_old = mga_plane_state_get(&mdev->mode_config, MGA_PLANE_CRTC2);
	const struct mga_plane_config *pc_new = mga_plane_state_get(mc, MGA_PLANE_CRTC2);

	BUG_ON(cc_old->overlay || cc_new->overlay);
	BUG_ON(pc_old->hscale != 0x10000 || pc_new->hscale != 0x10000);
	BUG_ON(pc_old->vscale != 0x10000 || pc_new->vscale != 0x10000);

	mc->dirty &= ~MGA_DIRTY_CRTC2;

	if (cc_new->outputs & MGA_OUTPUT_TVOUT &&
	    mc_old->tvo_config.tv_standard != mc_new->tvo_config.tv_standard)
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (cc_old->outputs != cc_new->outputs)
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (cc_old->mode_valid != cc_new->mode_valid ||
	    !drm_mode_equal(&cc_old->adjusted_mode, &cc_new->adjusted_mode))
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (cc_old->vidrst != cc_new->vidrst ||
	    cc_old->bt656 != cc_new->bt656 )
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (pc_old->fb != pc_new->fb)
		mc->dirty |= MGA_DIRTY_CRTC2_FB;

	if (pc_old->fb && pc_new->fb &&
	    mga_calc_cpp(pc_old->fb) != mga_calc_cpp(pc_new->fb))
		mc->dirty |= MGA_DIRTY_CRTC2_MODE;

	if (pc_old->src.x1 != pc_new->src.x1 ||
	    pc_old->src.y1 != pc_new->src.y1)
		mc->dirty |= MGA_DIRTY_CRTC2_FB;
}

static void mga_plane_pre_calc_dirty(struct mga_dev *mdev, struct mga_mode_config *mc,
				     unsigned int plane, unsigned int dirty)
{
	const struct mga_plane_config *pc_old = mga_plane_state_get(&mdev->mode_config, plane);
	const struct mga_plane_config *pc_new = mga_plane_state_get(mc, plane);

	mc->dirty &= ~dirty;

	if (pc_old->fb != pc_new->fb ||
	    pc_old->src_x != pc_new->src_x ||
	    pc_old->src_y != pc_new->src_y ||
	    pc_old->src_w != pc_new->src_w ||
	    pc_old->src_h != pc_new->src_h ||
	    pc_old->crtc_x != pc_new->crtc_x ||
	    pc_old->crtc_y != pc_new->crtc_y ||
	    pc_old->crtc_w != pc_new->crtc_w ||
	    pc_old->crtc_h != pc_new->crtc_h ||
	    pc_old->rotation != pc_new->rotation ||
	    pc_old->overlay != pc_new->overlay ||
	    pc_old->overlay_ckey != pc_new->overlay_ckey)
		mc->dirty |= dirty;
}

static void mga_plane_post_calc_dirty(struct mga_dev *mdev,
				      struct mga_mode_config *mc,
				      unsigned int plane,
				      unsigned int dirty)
{
	const struct mga_plane_config *pc_old = mga_plane_state_get(&mdev->mode_config, plane);
	const struct mga_plane_config *pc_new = mga_plane_state_get(mc, plane);

	mc->dirty &= ~dirty;

	if (pc_old->fb != pc_new->fb ||
	    !drm_region_equal(&pc_old->src, &pc_new->src) ||
	    !drm_region_equal(&pc_old->dst, &pc_new->dst) ||
	    pc_old->rotation != pc_new->rotation ||
	    pc_old->overlay != pc_new->overlay ||
	    pc_old->overlay_ckey != pc_new->overlay_ckey)
		mc->dirty |= dirty;
}

static unsigned int mga_mode_vrefresh(const struct drm_display_mode *mode)
{
	bool dblscan, interlace;
	unsigned int vrefresh;

	/* prefer 'clock' since it allows for more accuracy */
	if (!mode->clock)
		return mode->vrefresh << 4;

	dblscan = !!(mode->flags & DRM_MODE_FLAG_DBLSCAN);
	interlace = !!(mode->flags & DRM_MODE_FLAG_INTERLACE);

	return div_round64((u64) mode->clock << (4 + interlace - dblscan),
			   mode->htotal * mode->vtotal);
}

static int mga_update_adjusted_mode(struct drm_device *dev,
				    struct mga_mode_config *mc,
				    unsigned int crtc,
				    unsigned int dirty)
{
	struct mga_crtc_config *cc = mga_crtc_state_get(mc, crtc);

	if (!(mc->dirty & dirty))
		return 0;

	if (!cc->mode_valid) {
		memset(&cc->adjusted_mode, 0, sizeof cc->adjusted_mode);
		return 0;
	}

	drm_mode_copy(&cc->adjusted_mode, &cc->user_mode);
	cc->adjusted_mode.vrefresh = mga_mode_vrefresh(&cc->adjusted_mode);

	return 0;
}

static int mga_atomic_check(struct drm_device *dev, void *state)
{
	struct mga_dev *mdev = to_mga_device(dev);
	struct mga_mode_config *mc = state;
	int r = 0;

	if (mc->dirty == 0)
		return 0;

	if (mc->dirty & (MGA_DIRTY_CRTC1 | MGA_DIRTY_CRTC1_LUT))
		mga_crtc1_pre_calc_dirty(mdev, mc);

	if (mc->dirty & MGA_DIRTY_CRTC2)
		mga_crtc2_pre_calc_dirty(mdev, mc);

	if (mc->dirty & MGA_DIRTY_CURSOR)
		mga_plane_pre_calc_dirty(mdev, mc,
					 MGA_PLANE_CURSOR, MGA_DIRTY_CURSOR);

	if (mc->dirty & MGA_DIRTY_BES)
		mga_plane_pre_calc_dirty(mdev, mc,
					 MGA_PLANE_BES, MGA_DIRTY_BES);

	if (mc->dirty & MGA_DIRTY_SPIC)
		mga_plane_pre_calc_dirty(mdev, mc,
					 MGA_PLANE_SPIC, MGA_DIRTY_SPIC);

	if (mc->dirty & (MGA_DIRTY_CRTC1_MODE | MGA_DIRTY_CRTC2_MODE)) {
		r = mga_atomic_check_outputs(mdev, mc);
		if (r)
			return r;
	}

	r = mga_update_adjusted_mode(dev, mc, MGA_CRTC_CRTC1, MGA_DIRTY_CRTC1_MODE);
	if (r)
		return r;

	r = mga_update_adjusted_mode(dev, mc, MGA_CRTC_CRTC2, MGA_DIRTY_CRTC2_MODE);
	if (r)
		return r;

	if (mc->dirty & MGA_DIRTY_CRTC1) {
		r = mga_atomic_check_crtc1(mdev, mc);
		if (r)
			return r;

		mga_crtc1_post_calc_dirty(mdev, mc);

		if (mc->dirty & MGA_DIRTY_CRTC1_MODE) {
			mc->dirty |= MGA_DIRTY_CURSOR;

			if (mdev->chip >= MGA_CHIP_G200)
				mc->dirty |= MGA_DIRTY_BES;
		}

		/* reload lut in case switching between empty_fb and something else */
		if (mga_atomic_plane_uses_empty_fb(mdev, mc, MGA_PLANE_CRTC1) !=
		    mga_atomic_plane_uses_empty_fb(mdev, &mdev->mode_config, MGA_PLANE_CRTC1))
			mc->dirty |= MGA_DIRTY_CRTC1_LUT;
	}

	if (mc->dirty & MGA_DIRTY_CRTC2) {
		r = mga_atomic_check_crtc2(mdev, mc);
		if (r)
			return r;

		mga_crtc2_post_calc_dirty(mdev, mc);

		if (mc->dirty & MGA_DIRTY_CRTC2)
			mc->dirty |= MGA_DIRTY_SPIC;
	}

	if (mc->dirty & MGA_DIRTY_CURSOR) {
		r = mga_atomic_check_cursor(mdev, mc);
		if (r)
			return r;

		mga_plane_post_calc_dirty(mdev, mc,
					  MGA_PLANE_CURSOR, MGA_DIRTY_CURSOR);
	}

	if (mc->dirty & MGA_DIRTY_BES) {
		r = mga_atomic_check_bes(mdev, mc);
		if (r)
			return r;

		mga_plane_post_calc_dirty(mdev, mc,
					  MGA_PLANE_BES, MGA_DIRTY_BES);
	}

	if (mc->dirty & MGA_DIRTY_SPIC) {
		r = mga_atomic_check_spic(mdev, mc);
		if (r)
			return r;

		mga_plane_post_calc_dirty(mdev, mc,
					  MGA_PLANE_SPIC, MGA_DIRTY_SPIC);
	}

	if (mc->dirty & (MGA_DIRTY_CRTC2 | MGA_DIRTY_SPIC)) {
		r = mga_atomic_check_crtc2_spic(mdev, mc);
		if (r)
			return r;
	}

	/* FIXME calc PLLs */

	if (mc->dirty & (MGA_DIRTY_CRTC1_MODE | MGA_DIRTY_CRTC2_MODE)) {
		r = mga_atomic_check_common(mdev, mc);
		if (r)
			return r;
	}

	/* FIXME somethine else? */

	return r;
}

static int mga_atomic_commit(struct drm_device *dev, void *state)
{
	struct mga_dev *mdev = to_mga_device(dev);
	struct mga_mode_config *mc = state;
	int r;

	if (mc->dirty) {
		struct mga_dev *mdev = to_mga_device(dev);

		r = mga_set_mode(mdev, mc);
		if (r)
			return r;

		mc->dirty = 0;
	}

	mdev->mode_config = *mc;

	return 0;
}

static void mga_atomic_end(struct drm_device *dev, unsigned int flags, void *state)
{
	struct mga_mode_config *mc = state;

	if (mc->dirty && !(flags & DRM_ATOMIC_TEST_ONLY)) {
		struct mga_dev *mdev = to_mga_device(dev);
		int r;

		r = mga_set_mode(mdev, &mdev->mode_config);
		if (r)
			dev_warn(mdev->dev, "Failed to restore mode after error (%d:%s)\n", r, strerror(r));
	}

	kfree(mc);
}

static bool prop_value_change_ok(const struct drm_property *prop, uint64_t value)
{
	return true;
}

static int drm_mode_atomic_ioctl(struct drm_device *dev, void *data)
{
	struct drm_mode_atomic *arg = data;
	uint32_t __user *objs_ptr = (uint32_t __user *)(unsigned long)arg->objs_ptr;
	uint32_t __user *count_props_ptr = (uint32_t __user *)(unsigned long)arg->count_props_ptr;
	uint32_t __user *props_ptr = (uint32_t __user *)(unsigned long)arg->props_ptr;
	uint64_t __user *prop_values_ptr = (uint64_t __user *)(unsigned long)arg->prop_values_ptr;
	uint64_t __user *blob_values_ptr = (uint64_t __user *)(unsigned long)arg->blob_values_ptr;
	unsigned int copied_objs = 0;
	unsigned int copied_props = 0;
	unsigned int copied_blobs = 0;
	void *state;
	int r = 0;
	unsigned int i, j;

	if (arg->flags & ~DRM_ATOMIC_TEST_ONLY)
		return -EINVAL;

	state = mga_atomic_begin(dev, arg->flags);
	if (IS_ERR(state)) {
		r = PTR_ERR(state);
		return r;
	}

	for (i = 0; i < arg->count_objs; i++) {
		struct drm_mode_object *obj;
		uint32_t count_props;
		uint32_t obj_id;

		r = get_user(obj_id, objs_ptr + copied_objs);
		if (r)
			goto out;

		obj = drm_mode_object_find(dev, obj_id, DRM_MODE_OBJECT_ANY);
		if (!obj) {
			r = -ENOENT;
			goto out;
		}

		r = get_user(count_props, count_props_ptr + copied_objs);
		if (r)
			goto out;

		copied_objs++;

		for (j = 0; j < count_props; j++) {
			struct drm_mode_object *prop_obj;
			struct drm_property *prop;
			void *blob_data = NULL;
			uint32_t prop_id;
			uint64_t prop_value;

			r = get_user(prop_id, props_ptr + copied_props);
			if (r)
				goto out;

			prop_obj = drm_mode_object_find(dev, prop_id, DRM_MODE_OBJECT_PROPERTY);
			if (!prop_obj) {
				r = -ENOENT;
				goto out;
			}
			prop = obj_to_property(prop_obj);

			r = get_user(prop_value, prop_values_ptr + copied_props);
			if (r)
				goto out;

			if (!prop_value_change_ok(prop, prop_value)) {
				r = -EINVAL;
				goto out;
			}

			if (prop->flags & DRM_MODE_PROP_BLOB && prop_value) {
				uint64_t blob_ptr;

				r = get_user(blob_ptr, blob_values_ptr + copied_blobs);
				if (r)
					goto out;

				blob_data = kmalloc(prop_value, GFP_KERNEL);
				if (!blob_data)	{
					r = -ENOMEM;
					goto out;
				}

				r = copy_from_user(blob_data, (void __user *)(unsigned long)blob_ptr, prop_value);
				if (r) {
					kfree(blob_data);
					goto out;
				}
			}

			if (prop->flags & DRM_MODE_PROP_BLOB)
				copied_blobs++;

			r = mga_atomic_set(dev, state, obj, prop, prop_value, blob_data);
			if (r)
				goto out;

			copied_props++;
		}
	}

	r = mga_atomic_check(dev, state);
	if (r)
		goto out;

	if (arg->flags & DRM_ATOMIC_TEST_ONLY)
		goto out;

	r = mga_atomic_commit(dev, state);

 out:
	mga_atomic_end(dev, arg->flags, state);

	return r;
}

static int drm_mode_getresources_ioctl(struct drm_device *dev, void *data)
{
	struct drm_mode_config *config = &dev->mode_config;
	struct drm_mode_resources *arg = data;
	uint32_t __user *fb_id = (uint32_t __user *)(unsigned long)arg->fb_id;
	uint32_t __user *plane_id = (uint32_t __user *)(unsigned long)arg->plane_id;
	uint32_t __user *crtc_id = (uint32_t __user *)(unsigned long)arg->crtc_id;
	uint32_t __user *encoder_id = (uint32_t __user *)(unsigned long)arg->encoder_id;
	uint32_t __user *connector_id = (uint32_t __user *)(unsigned long)arg->connector_id;
	unsigned int copied_objs = 0;
	unsigned int copied_props = 0;
	unsigned int copied_blobs = 0;
	struct drm_mode_object *obj;
	unsigned int copied;
	int r = 0;

	if (arg->count_fb >= config->num_fb) {
		struct drm_framebuffer *fb;
		copied = 0;

		list_for_each_entry(fb, &config->fb_list, head) {
			r = put_user(fb->base.id, fb_id + copied);
			if (r)
				goto out;
			copied++;
		}
	}
	arg->count_fb = config->num_fb;

	if (arg->count_plane >= config->num_plane) {
		struct drm_plane *plane;
		copied = 0;

		list_for_each_entry(plane, &config->plane_list, head) {
			r = put_user(plane->base.id, plane_id + copied);
			if (r)
				goto out;
			copied++;
		}
	}
	arg->count_plane = config->num_plane;

	if (arg->count_crtc >= config->num_crtc) {
		struct drm_crtc *crtc;
		copied = 0;

		list_for_each_entry(crtc, &config->crtc_list, head) {
			r = put_user(crtc->base.id, crtc_id + copied);
			if (r)
				goto out;
			copied++;
		}
	}
	arg->count_crtc = config->num_crtc;

	if (arg->count_encoder >= config->num_encoder) {
		struct drm_encoder *encoder;
		copied = 0;

		list_for_each_entry(encoder, &config->encoder_list, head) {
			r = put_user(encoder->base.id, encoder_id + copied);
			if (r)
				goto out;
			copied++;
		}
	}
	arg->count_encoder = config->num_encoder;

	if (arg->count_connector >= config->num_connector) {
		struct drm_connector *connector;
		copied = 0;

		list_for_each_entry(connector, &config->connector_list, head) {
			r = put_user(connector->base.id, connector_id + copied);
			if (r)
				goto out;
			copied++;
		}
	}
	arg->count_connector = config->num_connector;

 out:
	return r;
}

static int mga_dma_general_purpose_ioctl(struct drm_device *dev, struct mga_dma_ioc *d)
{
	struct mga_dev *mdev = to_mga_device(dev);

	return mga_submit_dma(mdev, d->data, d->len, MGA_DMA_GENERAL_PURPOSE_WRITE, &d->handle);
}

static int drm_mode_mk_fb_ioctl(struct drm_device *dev, struct drm_mk_fb *d)
{
	struct drm_framebuffer *fb;

	fb = drm_framebuffer_create(dev,
				    d->width, d->height,
				    d->pixel_format, d->flags,
				    d->offsets, d->pitches);
	if (IS_ERR(fb))
		return PTR_ERR(fb);


	d->fb_id = fb->base.id;
	return 0;
}

static int drm_mode_rm_fb_ioctl(struct drm_device *dev, struct drm_rm_fb *d)
{
	struct drm_framebuffer *fb;

	if (!d->fb_id)
		return -EINVAL;

	fb = drm_framebuffer_find(dev, d->fb_id);

	if (!fb)
		return -ENOENT;

	drm_framebuffer_destroy(dev, fb);

	return 0;
}

static int drm_load_lut_ioctl(struct drm_device *dev, struct drm_lut *d)
{
	return mga_load_lut(to_mga_device(dev), d->crtc, d->lut);
}

static int drm_set_mode_ioctl(struct drm_device *dev, struct drm_setmode *d)
{
	struct drm_framebuffer *fb = NULL;
	struct drm_display_mode *mode = NULL;

	if (d->fb_id)
		fb = drm_framebuffer_find(dev, d->fb_id);

	if (memchr_inv(&d->mode, 0, sizeof d->mode))
		mode = &d->mode;

	return mga_set_mode_crtc(to_mga_device(dev), d->crtc, fb, 0, 0, mode, d->outputs);
}

static int drm_reg_rw_ioctl(struct drm_device *dev, struct drm_reg_rw *d)
{
	return mga_reg_rw(to_mga_device(dev), d->dev, d->dir, d->size, d->reg, &d->val);
}

static int drm_mem_test_ioct(struct drm_device *dev, struct drm_mem_test *d)
{
	return mga_mem_test(to_mga_device(dev), &d->read_bytes, &d->write_bytes,
			    &d->read_usec, &d->write_usec, &d->clock);
}

static int mga_ioctl(struct drm_device *dev, unsigned int cmd, void *data)
{
	struct mga_dev *mdev = to_mga_device(dev);

	switch (cmd) {
	case DRM_IOC_DMA_GENERAL_PURPOSE:
		return mga_dma_general_purpose_ioctl(dev, data);
	case DRM_IOC_INIT:
		//mdev->pixel_format = DRM_FORMAT_XRGB8888;
		mdev->pixel_format = DRM_FORMAT_RGB565;
		//mdev->pixel_format = DRM_FORMAT_C8;
		//mdev->pixel_format = DRM_FORMAT_RGB888;
		mdev->splitmode = true;//
		return mga_init(mdev, false);
	case DRM_IOC_FINI:
		mga_fini(mdev);
		return 0;
	case DRM_IOC_MK_FB:
		return drm_mode_mk_fb_ioctl(dev, data);
	case DRM_IOC_RM_FB:
		return drm_mode_rm_fb_ioctl(dev, data);
	case DRM_IOC_LOAD_LUT:
		return drm_load_lut_ioctl(dev, data);
	case DRM_IOC_HARDRESET:
		mdev->pixel_format = DRM_FORMAT_XRGB8888;
		return mga_hardreset(mdev, false);
	case DRM_IOC_SET_MODE:
		return drm_set_mode_ioctl(dev, data);
	case DRM_IOC_SUSPEND:
		return mga_suspend(mdev);
	case DRM_IOC_RESUME:
		return mga_resume(mdev);
	case DRM_IOC_FILL:
		mga_fill(mdev);
		return 0;
	case DRM_IOC_REG_RW:
		return drm_reg_rw_ioctl(dev, data);
	case DRM_IOC_MEM_TEST:
		return drm_mem_test_ioct(dev, data);
	default:
		return -EINVAL;
	}
}

int drm_ioctl(struct drm_device *dev, unsigned int cmd, void *data)
{
	switch (cmd) {
	case DRM_IOC_GETRESOURCES:
		return drm_mode_getresources_ioctl(dev, data);
	case DRM_IOC_ATOMIC:
		return drm_mode_atomic_ioctl(dev, data);
	default:
		return mga_ioctl(dev, cmd, data);
	}
}

unsigned int mga_calc_pixel_clock(const struct drm_display_mode *mode,
				  unsigned int hzoom)
{
	bool dblscan, interlace;

	if (!mode)
		return 0;

	dblscan = mode->flags & DRM_MODE_FLAG_DBLSCAN;
	interlace = mode->flags & DRM_MODE_FLAG_INTERLACE;

	return div_round64((u64) mode->htotal * mode->vtotal * mode->vrefresh,
			   (1000000 * hzoom) << (4 + interlace - dblscan));
}

static void mga_calc_cursor(struct mga_dev *mdev,
			    const struct drm_region *src,
			    const struct drm_region *dst,
			    const struct drm_framebuffer *fb)
{
	int sx = src->x1 >> 16;
	int sy = src->y1 >> 16;
	int dx = dst->x1 + 64;
	int dy = dst->y1 + 64;
	const void *virt = mga_framebuffer_calc_virt(to_mga_framebuffer(fb), sx, sy, 0, 0);

	BUG_ON(src->x1 & 0xffff || src->x2 & 0xffff || src->y1 & 0xffff || src->y2 & 0xffff);
	BUG_ON(drm_region_width(src) != 64 << 16 || drm_region_height(src) != 64 << 16);
	BUG_ON(drm_region_width(dst) != 64 || drm_region_height(dst) != 64);

	if (mdev->chip >= MGA_CHIP_1064SG) {
		mga_dac_crtc1_cursor_position(mdev, dx, dy);
		mga_dac_crtc1_cursor_image(mdev, virt);
	} else {
		tvp_cursor_position(&mdev->tdev, dx, dy);
		tvp_cursor_image(&mdev->tdev, virt);
	}
}

#if 0
static void mga_load_cursor_2(struct mga_framebuffer *mfb)
{
	const u32 *data = mfb->virt;
	u64 *dst;
	int y;

	for (y = 0; y < 64; y++) {
		u64 slice[2] = { [0] = 0, };
		int x;

		for (x = 0; x < 64; x += 16) {
			int i;

			for (i = 0; i < 16; i++) {
				int n = 63 - (x + i);
				int k = i << 1;

				p0 = !!(*data & (1 << k));
				p1 = !!(*data & (2 << k));

				slice[0] |= p0 << n;
				slice[1] |= p1 << n;
			}

			data++;
		}

		*dst++ = slice0;
		*dst++ = slice1;
	}
}
#endif

/* load four cursor pixels (24 bits) */
static void mga_cursor_load_four(u64 *slice, u32 x, unsigned int n)
{
	u16 p;
	u8 c, t;
	p = (((x & 0x00000f) >>  0) |
	     ((x & 0x0003c0) >>  2) |
	     ((x & 0x00f000) >>  4) |
	     ((x & 0x3c0000) >>  6));
	c = (((x & 0x000010) >>  4) |
	     ((x & 0x000400) >>  9) |
	     ((x & 0x010000) >> 14) |
	     ((x & 0x400000) >> 19));
	t = (((x & 0x000020) >>  5) |
	     ((x & 0x000800) >> 10) |
	     ((x & 0x020000) >> 15) |
	     ((x & 0x800000) >> 20));
	slice[n >> 4] |= p << (n & 0xf);
	slice[5] |= c << n;
	slice[6] |= t << n;
}

static void mga_cursor_load_6(struct mga_framebuffer *mfb)
{
	const u32 *src = mfb->virt;
	u64 *dst;
	int y;

	for (y = 0; y < 64; y++) {
		u64 slice[6] = { [0] = 0, };
		unsigned int n = 0;
		int i;

		/*
		 * Each iteration of the loop loads 16 pixels,
		 * so 4 times the loop completes one line (64 pixels).
		 */
		for (i = 0; i < 4; i++) {
			u32 x;

			x = src[0] & 0x00ffffff;
			mga_cursor_load_four(slice, x, n);
			n += 4;

			x = ((src[0] & 0xff000000) >> 24) |
			    ((src[1] & 0x0000ffff) <<  8);
			mga_cursor_load_four(slice, x, n);
			n += 4;

			x = ((src[1] & 0xffff0000) >> 16) |
			    ((src[2] & 0x000000ff) << 16);
			mga_cursor_load_four(slice, x, n);
			n += 4;

			x = (src[2] & 0xffffff00) >> 8;
			mga_cursor_load_four(slice, x, n);
			n += 4;

			src += 3;
		}

		for (i = 0; i < 6; i++)
			*dst++ = slice[i];
	}
}
