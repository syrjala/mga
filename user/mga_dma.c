/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include "mga_dump.h"
#include "mga_regs.h"
#include "mga_dma.h"
#include "list.h"

/*
 * DMA ring buffer structure.
 * Used for both the primary and secondary/setup DMA buffers.
 */
struct mga_dma_buf {
	/* the physical page */
	struct page *page;
	/* the bus address */
	u32 bus;
	/* the kernel virtual address */
	void *virt;
	/* list head */
	struct list_head list;
	/* true if the buffer has been emitted to the hardware */
	bool emitted;
	/* buffer's current handle */
	u32 handle;
	/* buffer's current head (read) pointer */
	u32 head;
	/* buffer's current tail (write) pointer */
	u32 tail;
	/* buffer's size */
	u32 size;

	/* value of prim tail when this buf is done executing. */
	u32 prim_tail;
	/* value of prim tail when this buf is done executing. */
	u32 prim_wrap;
};

enum {
	MGA_MAX_SEC_BUFFERS = 4,
};

struct mga_dma {
	/* Primary DMA buffer */
	struct mga_dma_buf prim;
	struct mga_dma_buf sec[MGA_MAX_SEC_BUFFERS];
	int num_sec;
	/* Protects the primary DMA buffer */
	spinlock_t prim_lock;
	/* used when waiting for space in primary DMA buffer */
	wait_queue_head_t prim_wait;
	/*
	 * buffer's current tail (write) pointer from hardware POV
	 * needed because hardware doesn't wrap on its own.
	 */
	u32 hw_prim_tail;
	/* */
	u32 hw_prim_wrap;
	/*
	 * status "page"
	 */
	struct {
		bool enable;
		u32 bus;
		u32 *virt;
	} primptr;

	/* Protects command submission */
	struct mutex mutex;
	/* Accept DMA buffers? */
	bool enabled;
	u32 next_handle;
	u32 wrap_handle;
	u32 hw_wrap_head;

	u32 last_emitted_handle;
	u32 last_completed_handle;

	/* DMA buffers that are not currently used */
	struct list_head idle_list;
	/* DMA buffers waiting to be processed by HW */
	struct list_head emitted_list;
	/* Protects idle_list, emitted_list */
	spinlock_t list_lock;
	/* used when waiting for idle buffers */
	wait_queue_head_t idle_wait;
};

/*
 * Initialize the DMA buffer structure.
 */
static int mga_dma_buf_init(struct mga_dev *mdev, struct mga_dma_buf *buf, size_t size)
{
	int r;

	r = mga_mem_alloc(mdev->dma_alloc, &buf->bus, &buf->virt, size, false);
	if (r)
		return r;

	buf->emitted = false;
	buf->handle = 0;
	buf->head = 0;
	buf->tail = 0;
	buf->size = size;

	buf->prim_tail = 0;

	INIT_LIST_HEAD(&buf->list);

	return 0;
}

static void mga_dma_buf_fini(struct mga_dev *mdev, struct mga_dma_buf *buf)
{
	if (!buf->virt)
		return;

	mga_mem_free(mdev->dma_alloc, buf->bus, buf->size);
}

static void mga_primptr_init(struct mga_dev *mdev)
{
	int r;
	u32 bus;
	void *virt;

	/* FIXME verify whether this is true */
	if (mdev->chip < MGA_CHIP_G200) {
		dev_dbg(mdev->dev, "PRIMPTR not supported\n");
		return;
	}

	if (!mdev->dma->primptr.enable) {
		dev_dbg(mdev->dev, "PRIMPTR disabled by user\n");
		mga_write32(mdev, MGA_PRIMPTR, 0);
		return;
	}

	r = mga_mem_alloc(mdev->dma_alloc, &bus, &virt, 16, false);
	if (r) {
		/*
		 * Failing to allocate primptr is not fatal,
		 * we can continue by polling registers instead.
		 */
		dev_warn(mdev->dev, "Failed to allocate PRIMPTR\n");
		mga_write32(mdev, MGA_PRIMPTR, 0);
		return;
	}

	memset(virt, 0, 16);

	mdev->dma->primptr.bus = bus;
	mdev->dma->primptr.virt = virt;

	dev_dbg(mdev->dev, "PRIMPTR at 0x%08x (16 bytes)\n", bus);
	/*
	 * FIXME
	 * find out whether DWSYNC slows things down,
	 * and if so emit it only for specific bufs
	 * (end of frame, or explicit fence).
	 */
	mga_write32(mdev, MGA_PRIMPTR, mdev->dma->primptr.bus |
		   MGA_PRIMPTR_PRIMPTREN1 | MGA_PRIMPTR_PRIMPTREN0);
}

static void mga_primptr_fini(struct mga_dev *mdev)
{
	if (!mdev->dma->primptr.virt)
		return;

	mga_write32(mdev, MGA_PRIMPTR, 0);
	mga_mem_free(mdev->dma_alloc, mdev->dma->primptr.bus, 32);
	mdev->dma->primptr.bus = 0;
	mdev->dma->primptr.virt = NULL;
}

static int mga_dma_start(struct mga_dev *mdev);
static int mga_dma_stop(struct mga_dev *mdev);

int mga_dma_init(struct mga_dev *mdev)
{
	int i;
	int r;
	struct mga_mem_alloc *dma_alloc;
	size_t dma_buf_size;
	unsigned int dma_buf_num;

	if (!mdev->dma_res.len)
		return -ENOSPC;

	dma_buf_size = 128 << 10;
	dma_buf_num = mdev->dma_res.len / dma_buf_size;

	while (dma_buf_num < 4) {
		dma_buf_size >>= 1;
		dma_buf_num = mdev->dma_res.len / dma_buf_size;
	}

	if (dma_buf_num > MGA_MAX_SEC_BUFFERS) {
		dma_buf_num = MGA_MAX_SEC_BUFFERS;
		dma_buf_size = mdev->dma_res.len / dma_buf_num;
	}

	// hack
	dma_buf_size = 256;
	dma_buf_num = 5;

	dma_alloc = mga_mem_alloc_init(mdev->dma_res.off, mdev->dma_res.len, mdev->dma_virt, 64);
	if (IS_ERR(dma_alloc))
		return PTR_ERR(dma_alloc);
	mdev->dma_alloc = dma_alloc;

	mdev->dma = kzalloc(sizeof *mdev->dma, GFP_KERNEL);
	if (!mdev->dma) {
		r = -ENOMEM;
		goto fini;
	}

	INIT_LIST_HEAD(&mdev->dma->idle_list);
	INIT_LIST_HEAD(&mdev->dma->emitted_list);
	spin_lock_init(&mdev->dma->list_lock);
	init_waitqueue_head(&mdev->dma->idle_wait);
	init_waitqueue_head(&mdev->dma->prim_wait);

	mdev->dma->primptr.enable = true;
	mga_primptr_init(mdev);

	r = mga_dma_buf_init(mdev, &mdev->dma->prim, dma_buf_size);
	if (r)
		goto primptr_fini;

	/* FIXME maybe allocate on demand instead */
	for (i = 0; i < MGA_MAX_SEC_BUFFERS; i++) {
		r = mga_dma_buf_init(mdev, &mdev->dma->sec[i], dma_buf_size);
		if (r)
			break;
		list_add_tail(&mdev->dma->sec[i].list, &mdev->dma->idle_list);
		mdev->dma->num_sec++;
	}

	if (mdev->dma->num_sec == 0)
		goto prim_fini;

	r = mga_dma_start(mdev);
	if (r)
		goto sec_fini;

	return 0;

 sec_fini:
	for (i = 0; i < mdev->dma->num_sec; i++) {
		list_del(&mdev->dma->sec[i].list);
		mga_dma_buf_fini(mdev, &mdev->dma->sec[i]);
	}
 prim_fini:
	mga_dma_buf_fini(mdev, &mdev->dma->prim);
 primptr_fini:
	mga_primptr_fini(mdev);

	kfree(mdev->dma);
	mdev->dma = NULL;
 fini:
	mga_mem_alloc_fini(mdev->dma_alloc);
	mdev->dma_alloc = NULL;
	return r;
}

void mga_dma_fini(struct mga_dev *mdev)
{
	int i;

	if (!mdev->dma)
		return;

	mga_dma_stop(mdev);

	for (i = 0; i < mdev->dma->num_sec; i++) {
		list_del(&mdev->dma->sec[i].list);
		mga_dma_buf_fini(mdev, &mdev->dma->sec[i]);
	}

	mga_dma_buf_fini(mdev, &mdev->dma->prim);

	mga_primptr_fini(mdev);

	kfree(mdev->dma);
	mdev->dma = NULL;

	mga_mem_alloc_fini(mdev->dma_alloc);
	mdev->dma_alloc = NULL;
}

/*
 * Return the next DMA buffer handle.
 * Each buffer gets a handle when emitted. The handle is then
 * used to determine when the buffer is completed by the hardware.
 */
static u32 mga_dma_next_handle(struct mga_dev *mdev)
{
	mdev->dma->next_handle = (mdev->dma->next_handle + 1) & 0x3fffffff;

	return mdev->dma->next_handle;
}

/*
 * Return the number of used bytes in the DMA buffer
 */
static size_t mga_dma_buf_used(const struct mga_dma_buf *buf)
{
	if (buf->tail >= buf->head)
		return buf->tail - buf->head;
	else
		return buf->size + buf->tail - buf->head;
}

/*
 * Return the number of free bytes in the DMA buffer
 */
static size_t mga_dma_buf_free(const struct mga_dma_buf *buf)
{
	size_t free = buf->size - mga_dma_buf_used(buf);

	return free > 20 ? (free - 20) : 0;
}

/*
 * Read the current primary DMA address pointer
 */
static void mga_prim_update(struct mga_dev *mdev)
{
	struct mga_dma_buf *prim = &mdev->dma->prim;
	u32 primaddress;

	if (mdev->dma->primptr.virt)
	{
		primaddress = mdev->dma->primptr.virt[0];
#if 0
		dev_dbg(mdev->dev, "PRIMPTR : PRIMADDRESS = 0x%08x DWGSYNC = 0x%08x\n",
			mdev->dma->primptr.virt[0],
			mdev->dma->primptr.virt[1]);
		dev_dbg(mdev->dev, "REGISTER: PRIMADDRESS = 0x%08x DWGSYNC = 0x%08x\n",
			mga_read32(mdev, MGA_PRIMADDRESS),
			mga_read32(mdev, MGA_DWGSYNC));
#endif
	}
	else
		primaddress = mga_read32(mdev, MGA_PRIMADDRESS);

	prim->head = (primaddress & ~MGA_PRIMADDRESS_PRIMOD) - prim->bus;
}

/*
 * Initialize the primary DMA end pointer to the end of the buffer
 * One packet (20 bytes) of DMAPAD writes is left after the end pointer.
 */
static void mga_prim_continue(struct mga_dev *mdev)
{
	const struct mga_dma_buf *prim = &mdev->dma->prim;

	mga_write32(mdev, MGA_PRIMEND, prim->bus + mdev->dma->hw_prim_tail - 20);
}

static bool handle_after_eq(u32 a, u32 b)
{
	return !((a - b) & 0x20000000);
}

static bool mga_dma_handle_is_complete(struct mga_dev *mdev, u32 handle)
{
	bool ret;

	spin_lock_irq(&mdev->dma->prim_lock);
	ret = handle_after_eq(mdev->dma->last_completed_handle, handle);
	spin_unlock_irq(&mdev->dma->prim_lock);

	return ret;
}

static void mga_dma_refresh(struct mga_dev *mdev);

static int mga_dma_wait(struct mga_dev *mdev, u32 handle)
{
	int r;

	if (mga_dma_handle_is_complete(mdev, handle))
		return 0;

	mga_irq_ref(mdev, MGA_IEN_SOFTRAPIEN);

	mga_dma_refresh(mdev);

	/* FIXME make interruptible and add timeout */
	r = wait_event_interruptible(&mdev->dma->idle_wait, mga_dma_handle_is_complete(mdev, handle));

	mga_irq_unref(mdev, MGA_IEN_SOFTRAPIEN);

	return r;
}

static u32 mga_read_dwgsync(struct mga_dev *mdev)
{
	if (mdev->dma->primptr.virt)
		return mdev->dma->primptr.virt[1];
	else
		return mga_read32(mdev, MGA_DWGSYNC);
}

/* FIXME add timeout */
static int mga_dwgeng_wait(struct mga_dev *mdev, u32 handle)
{
	int r;

	r = mga_dma_wait(mdev, handle);
	if (r)
		return r;

	/* no DWGSYNC? */
	if (mdev->chip < MGA_CHIP_G200)
		return 0;

	/* poll DWGSYNC */
	for (;;) {
		u32 dwgsync = mga_read_dwgsync(mdev) >> 2;

		if (handle_after_eq(dwgsync, handle))
			break;

		if (signal_pending(current))
			return -ERESTARTSYS;
	}

	return 0;
}

/*
 * Initialize the primary DMA address pointer to the beginning of the buffer
 */
static void mga_prim_init(struct mga_dev *mdev)
{
	const struct mga_dma_buf *prim = &mdev->dma->prim;

	/* a value that will never equal primaddress */
	mdev->dma->hw_wrap_head = 0xffffffff;
	mga_write32(mdev, MGA_PRIMADDRESS,
		   prim->bus | MGA_PRIMADDRESS_PRIMOD_GENERAL_PURPOSE);
}

static bool mga_prim_available(struct mga_dev *mdev, size_t size)
{
	bool ret;

	spin_lock_irq(&mdev->dma->prim_lock);
	ret = mga_dma_buf_free(&mdev->dma->prim) >= size;
	spin_unlock_irq(&mdev->dma->prim_lock);

	return ret;
}

/*
 * Wait until the specifed amount of space becomes
 * available on the primary DMA buffer.
 */
/* FIXME add timeout */
static int mga_prim_wait_space(struct mga_dev *mdev, size_t size)
{
	int r;

	/* first try w/o reading the current head pointer from hardware */
	if (mga_prim_available(mdev, size))
		return 0;

	/* need to wait if we still don't have enough space */

	mga_irq_ref(mdev, MGA_IEN_SOFTRAPIEN);

	/* refresh the head pointer */
	spin_lock_irq(&mdev->dma->prim_lock);
	mga_prim_update(mdev);
	spin_unlock_irq(&mdev->dma->prim_lock);

	r = wait_event_interruptible(&mdev->dma->prim_wait, mga_prim_available(mdev, size));

	mga_irq_unref(mdev, MGA_IEN_SOFTRAPIEN);

	return r;
}

static bool mga_idle_buf_available(struct mga_dev *mdev)
{
	bool ret;

	spin_lock_irq(&mdev->dma->list_lock);
	ret = !list_empty(&mdev->dma->idle_list);
	spin_unlock_irq(&mdev->dma->list_lock);

	return ret;
}

static bool mga_dma_idle(struct mga_dev *mdev)
{
	bool ret;

	spin_lock_irq(&mdev->dma->list_lock);
	ret = list_empty(&mdev->dma->emitted_list);
	spin_unlock_irq(&mdev->dma->list_lock);

	return ret;
}

static struct mga_dma_buf *get_next_idle_buf(struct mga_dev *mdev)
{
	struct mga_dma_buf *buf = NULL;

	spin_lock_irq(&mdev->dma->list_lock);

	if (!list_empty(&mdev->dma->idle_list)) {
		buf = list_first_entry(&mdev->dma->idle_list, struct mga_dma_buf, list);
		list_del(&buf->list);
	}

	spin_unlock_irq(&mdev->dma->list_lock);

	return buf;
}

/*
 * Get a DMA buffer from the idle list
 */
/* FIXME timeout */
static struct mga_dma_buf *mga_get_idle_dma_buf(struct mga_dev *mdev)
{
	struct mga_dma_buf *buf;
	int r;

	//dev_dbg(mdev->dev, "%s start\n", __func__);

	buf = get_next_idle_buf(mdev);
	if (buf)
		return buf;

	mga_irq_ref(mdev, MGA_IEN_SOFTRAPIEN);

	mga_dma_refresh(mdev);

	r = wait_event_interruptible(&mdev->dma->idle_wait, (buf = get_next_idle_buf(mdev)) != NULL);
	if (r == -ERESTARTSYS)
		buf = ERR_PTR(-ERESTARTSYS);

	mga_irq_unref(mdev, MGA_IEN_SOFTRAPIEN);

	//dev_dbg(mdev->dev, "%s end\n", __func__);

	return buf;
}

/*
 * Put the DMA buffer onto the idle list.
 */
static void mga_put_idle_dma_buf(struct mga_dev *mdev,
				 struct mga_dma_buf *buf)
{
	buf->emitted = false;
	buf->handle = 0;
	buf->head = 0;
	buf->tail = 0;

	spin_lock_irq(&mdev->dma->list_lock);
	list_add_tail(&buf->list, &mdev->dma->idle_list);
	wake_up_all(&mdev->dma->idle_wait);
	spin_unlock_irq(&mdev->dma->list_lock);
}

/*
 * Add a single General purpose pseudo-DMA packet to the DMA buffer.
 */
static void mga_dma_add(struct mga_dma_buf *buf,
			u32 reg0, u32 reg1,
			u32 reg2, u32 reg3,
			u32 val0, u32 val1,
			u32 val2, u32 val3)
{
	u32 *ptr = buf->virt + buf->tail;

	*ptr++ = (mga_reg_to_index(reg3) << 24) |
		 (mga_reg_to_index(reg2) << 16) |
		 (mga_reg_to_index(reg1) <<  8) |
		 (mga_reg_to_index(reg0) <<  0);
	*ptr++ = val0;
	*ptr++ = val1;
	*ptr++ = val2;
	*ptr++ = val3;

	buf->tail += 20;

	pr_debug("%s:\n"
		 " 0x%08x\n"
		 " 0x%08x\n"
		 " 0x%08x\n"
		 " 0x%08x\n"
		 " 0x%08x\n", __func__,
		 *(ptr - 5),
		 *(ptr - 4),
		 *(ptr - 3),
		 *(ptr - 2),
		 *(ptr - 1));
}

/*
 * Grab an idle DMA buffer and copy the data to the buffer.
 */
static struct mga_dma_buf* mga_copy_dma(struct mga_dev *mdev,
					const void *data, size_t len)
{
	struct mga_dma_buf *buf;

	buf = mga_get_idle_dma_buf(mdev);
	if (IS_ERR(buf))
		return buf;

	if (len > buf->size) {
		mga_put_idle_dma_buf(mdev, buf);
		return ERR_PTR(-ENOSPC);
	}

	memcpy(buf->virt, data, len);
	buf->head = 0;
	buf->tail = len;

	return buf;
}

static int mga_check_reg(const struct mga_dev *mdev, u32 reg, u32 val)
{
	if (reg >= 0x1d00 && reg < 0x1dc0)
		reg -= MGA_DWGENG_GO;
	if (!(reg >= 0x1c00 && reg < 0x1e00) &&
	    !(reg >= 0x2c00 && reg < 0x2e00))
		return -EINVAL;

	return 0;
}

static int mga_copy_packet(const struct mga_dev *mdev, u32 *d, const u32 *s)
{
	u32 reg0, reg1, reg2, reg3;
	int r;

	reg0 = mga_index_to_reg((s[0] >>  0) & 0xff);
	reg1 = mga_index_to_reg((s[0] >>  8) & 0xff);
	reg2 = mga_index_to_reg((s[0] >> 16) & 0xff);
	reg3 = mga_index_to_reg((s[0] >> 24) & 0xff);

	r = mga_check_reg(mdev, reg0, s[1]);
	if (r)
		return r;
	r = mga_check_reg(mdev, reg1, s[2]);
	if (r)
		return r;
	r = mga_check_reg(mdev, reg2, s[3]);
	if (r)
		return r;
	r = mga_check_reg(mdev, reg3, s[4]);
	if (r)
		return r;

	memcpy(d, s, 20);

	return 0;
}

/*
 * Grab an idle DMA buffer and copy+verify the data to the buffer.
 */
static struct mga_dma_buf* mga_copy_dma_verify(struct mga_dev *mdev,
					       const void *data, size_t len)
{
	struct mga_dma_buf *buf;
	const u32 *s = data;
	u32 *d;

	if (len % 20)
		return ERR_PTR(-EINVAL);

	buf = mga_get_idle_dma_buf(mdev);
	if (IS_ERR(buf))
		return buf;

	if (len > buf->size) {
		mga_put_idle_dma_buf(mdev, buf);
		return ERR_PTR(-ENOSPC);
	}

	len /= 20;

	d = buf->virt;
	buf->head = 0;
	buf->tail = 0;

	while (len--) {
		int r = mga_copy_packet(mdev, d, s);
		if (r) {
			mga_put_idle_dma_buf(mdev, buf);
			return ERR_PTR(r);
		}
		s += 5;
		d += 5;
		buf->tail += 20;
	}

	return buf;
}

static size_t mga_prim_space_required(struct mga_dma_buf *buf)
{
	/*
	 * If there's a wrap in the buffer, need to
	 * emit two secondary DMAs, otherwise one
	 * secondary DMA. A SOFTRAP is emitted after
	 * the secondary DMA(s). And we also emit a
	 * packet full of DMAPADs afterwards.
	 */
	return (buf->tail >= buf->head ? 40 : 60) + 20;
}

static bool mga_prim_need_wrap(const struct mga_dev *mdev, size_t size)
{
	const struct mga_dma_buf *prim = &mdev->dma->prim;

	/*
	 * Need to reserve 40 bytes to handle wrap.
	 * One 20 byte packet for SOFTRAP, and another
	 * one full of DMAPAD.
	 */
	return prim->size - prim->tail < size + 40;
}

/*
 * Actually emit the DMA buffer for hardware.
 */
static int mga_emit_dma(struct mga_dev *mdev,
			struct mga_dma_buf *buf,
			enum mga_dma_mode mode,
			u32 *ret_handle)
{
	int r;
	u32 address_reg;
	u32 end_reg;
	u32 dmamod;

	BUG_ON(buf->head == buf->tail);

	switch (mode) {
	case MGA_DMA_GENERAL_PURPOSE_WRITE:
		address_reg = MGA_SECADDRESS;
		end_reg = MGA_SECEND;
		dmamod = MGA_SECADDRESS_SECMOD_GENERAL_PURPOSE;
		break;
	case MGA_DMA_BLIT_WRITE:
		address_reg = MGA_SECADDRESS;
		end_reg = MGA_SECEND;
		dmamod = MGA_SECADDRESS_SECMOD_BLIT;
		break;
	case MGA_DMA_VECTOR_WRITE:
		address_reg = MGA_SECADDRESS;
		end_reg = MGA_SECEND;
		dmamod = MGA_SECADDRESS_SECMOD_VECTOR;
		break;
	case MGA_DMA_VERTEX_WRITE:
		address_reg = MGA_SECADDRESS;
		end_reg = MGA_SECEND;
		dmamod = MGA_SECADDRESS_SECMOD_VERTEX;
		break;
	case MGA_DMA_VERTEX_FIXED_LENGTH_SETUP_LIST:
		address_reg = MGA_SETUPADDRESS;
		end_reg = MGA_SETUPEND;
		dmamod = MGA_SETUPADDRESS_SETUPMOD_VERTEX;
		break;
	default:
		BUG();
	}

	mutex_lock(&mdev->dma->mutex);

	if (!mdev->dma->enabled) {
		mutex_unlock(&mdev->dma->mutex);
		return -ENOSYS; //FIXME
	}

	if (mga_prim_need_wrap(mdev, mga_prim_space_required(buf))) {
		r = mga_prim_wait_space(mdev, 40);
		if (r) {
			mutex_unlock(&mdev->dma->mutex);
			return r;
		}

		mdev->dma->wrap_handle = mga_dma_next_handle(mdev);

		/* Add SOFTRAP to handle primary buffer wrapping */
		mga_dma_add(&mdev->dma->prim,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    MGA_DWGSYNC,
			    MGA_SOFTRAP,
			    0,
			    0,
			    mdev->dma->wrap_handle << 2,
			    mdev->dma->wrap_handle << 2);

		/* HW may read part of the packet past PRIMEND */
		/* FIXME is this really true? */
		mga_dma_add(&mdev->dma->prim,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    0,
			    0,
			    0,
			    0);

		spin_lock_irq(&mdev->dma->prim_lock);

		mdev->dma->last_emitted_handle = mdev->dma->wrap_handle;
		dev_dbg(mdev->dev, "emitting %u\n", mdev->dma->last_emitted_handle);

		mdev->dma->hw_prim_tail = mdev->dma->prim.tail;
		mdev->dma->hw_wrap_head = mdev->dma->hw_prim_tail - 20;
		mdev->dma->prim.tail = 0;
		mdev->dma->prim.prim_wrap++;

		dev_dbg(mdev->dev, "PRIM sw wrap %u\n", mdev->dma->prim.prim_wrap);

		mga_irq_ref(mdev, MGA_IEN_SOFTRAPIEN);

		mga_prim_continue(mdev);

		spin_unlock_irq(&mdev->dma->prim_lock);
	}

	r = mga_prim_wait_space(mdev, mga_prim_space_required(buf));
	if (r) {
		mutex_unlock(&mdev->dma->mutex);
		return r;
	}

	buf->handle = mga_dma_next_handle(mdev);
	buf->emitted = true;

	if (ret_handle)
		*ret_handle = buf->handle;

	spin_lock_irq(&mdev->dma->list_lock);
	list_add_tail(&buf->list, &mdev->dma->emitted_list);
	spin_unlock_irq(&mdev->dma->list_lock);

	if (buf->tail >= buf->head) {
		mga_dma_add(&mdev->dma->prim,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    address_reg,
			    end_reg,
			    0,
			    0,
			    buf->bus + buf->head,
			    (buf->bus + buf->tail) | dmamod);
		/* FIXME figure out what's needed between DWGSYNC and SOFTRAP */
		mga_dma_add(&mdev->dma->prim,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    MGA_DWGSYNC,
			    MGA_SOFTRAP,
			    0,
			    0,
			    buf->handle << 2,
			    buf->handle << 2);
	} else {
		mga_dma_add(&mdev->dma->prim,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    address_reg,
			    end_reg,
			    0,
			    0,
			    buf->bus + buf->head,
			    (buf->bus + buf->size) | dmamod);
		mga_dma_add(&mdev->dma->prim,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    address_reg,
			    end_reg,
			    0,
			    0,
			    buf->bus + 0,
			    (buf->bus + buf->tail) | dmamod);
		/* FIXME figure out what's needed between DWGSYNC and SOFTRAP */
		mga_dma_add(&mdev->dma->prim,
			    MGA_DMAPAD,
			    MGA_DMAPAD,
			    MGA_DWGSYNC,
			    MGA_SOFTRAP,
			    0,
			    0,
			    buf->handle << 2,
			    buf->handle << 2);
	}

	/* HW may read part of the packet past PRIMEND */
	/* FIXME is this really true? */
	mga_dma_add(&mdev->dma->prim,
		    MGA_DMAPAD,
		    MGA_DMAPAD,
		    MGA_DMAPAD,
		    MGA_DMAPAD,
		    0,
		    0,
		    0,
		    0);

	buf->prim_tail = mdev->dma->prim.tail - 20;
	buf->prim_wrap = mdev->dma->prim.prim_wrap;

	spin_lock_irq(&mdev->dma->prim_lock);

	mdev->dma->last_emitted_handle = buf->handle;

#if 0
	dev_dbg(mdev->dev, "emitting %u (wrap=%u tail=%u)\n",
		mdev->dma->last_emitted_handle, buf->prim_wrap, buf->prim_tail);
#endif

	if (mdev->dma->prim.prim_wrap == mdev->dma->hw_prim_wrap) {
		mdev->dma->hw_prim_tail = mdev->dma->prim.tail;
		mga_prim_continue(mdev);
	}

	spin_unlock_irq(&mdev->dma->prim_lock);

	mutex_unlock(&mdev->dma->mutex);

	return 0;
}

/*
 * Submit a new DMA buffer using a specific mode.
 */
int mga_submit_dma(struct mga_dev *mdev,
		   const void *data, size_t len,
		   enum mga_dma_mode mode,
		   u32 *ret_handle)
{
	struct mga_dma_buf *buf;
	int r;

	switch (mode) {
	case MGA_DMA_GENERAL_PURPOSE_WRITE:
	case MGA_DMA_BLIT_WRITE:
	case MGA_DMA_VECTOR_WRITE:
		break;
	case MGA_DMA_VERTEX_WRITE:
	case MGA_DMA_VERTEX_FIXED_LENGTH_SETUP_LIST:
		if (mdev->chip < MGA_CHIP_G200)
			return -ENODEV;
		break;
	default:
		return -EINVAL;
	}

	switch (mode) {
	case MGA_DMA_GENERAL_PURPOSE_WRITE:
		buf = mga_copy_dma_verify(mdev, data, len);
		break;
	default:
		/* FIXME need to verify SETUP list too */
		buf = mga_copy_dma(mdev, data, len);
		break;
	}
	if (IS_ERR(buf))
		return PTR_ERR(buf);

	r = mga_emit_dma(mdev, buf, mode, ret_handle);
	if (r) {
		mga_put_idle_dma_buf(mdev, buf);
		return r;
	}

	return 0;
}

static int mga_dma_start(struct mga_dev *mdev)
{
	int r = 0;

	if (!mdev->dma)
		return -ENOENT;

	mutex_lock(&mdev->dma->mutex);

	if (mdev->dma->enabled)
		goto out;

	mdev->dma->enabled = true;

	mga_prim_init(mdev);

 out:
	mutex_unlock(&mdev->dma->mutex);

	return r;
}

static int mga_dma_stop(struct mga_dev *mdev)
{
	int r = 0;

	if (!mdev->dma)
		return -ENOENT;

	mutex_lock(&mdev->dma->mutex);

	if (!mdev->dma->enabled)
		goto out;

	r = mga_dma_wait(mdev, mdev->dma->last_emitted_handle);
	if (r)
		goto out;

	mdev->dma->enabled = false;

 out:
	mutex_unlock(&mdev->dma->mutex);

	return r;
}

/*
 * Wrap the primary DMA buffer head (read) pointer.
 */
static void mga_prim_wrap_complete(struct mga_dev *mdev)
{
	struct mga_dma_buf *prim = &mdev->dma->prim;

	mga_irq_unref(mdev, MGA_IEN_SOFTRAPIEN);

	prim->head = 0;
	mdev->dma->hw_prim_tail = prim->tail;
	mdev->dma->hw_prim_wrap++;

	//dev_dbg(mdev->dev, "PRIM hw wrap %u\n", mdev->dma->hw_prim_wrap);

	mga_prim_init(mdev);
	mga_prim_continue(mdev);
}

/*
 * Signal that buf is completed.
 * buf must be on the emitted list.
 * Caller must hold dma.list_lock
 */
static void mga_complete_dma(struct mga_dev *mdev,
			     struct mga_dma_buf *buf)
{
	BUG_ON(!buf->emitted);

	mdev->dma->last_completed_handle = buf->handle;
	dev_dbg(mdev->dev, "completing %u\n", mdev->dma->last_completed_handle);

	buf->emitted = false;
	buf->handle = 0;
	buf->head = 0;
	buf->tail = 0;

	list_move_tail(&buf->list, &mdev->dma->idle_list);
	wake_up_all(&mdev->dma->idle_wait);
}

static bool mga_dma_buf_is_complete(const struct mga_dev *mdev,
				    const struct mga_dma_buf *buf)
{
	u32 diff = mdev->dma->hw_prim_wrap - buf->prim_wrap;

#if 0
	dev_dbg(mdev->dev, "checking buf (wrap=%u tail=%u)\n",
		buf->prim_wrap, buf->prim_tail);
	dev_dbg(mdev->dev, "hw_prim_wrap=%u prim head=%u\n",
		mdev->dma->hw_prim_wrap, mdev->dma->prim.head);
#endif

	if (diff == 0)
		return mdev->dma->prim.head >= buf->prim_tail;
	else
		return !(diff & 0x80000000);
}

/*
 * Go over emitted list and signal all completed buffers as such.
 */
static void mga_check_and_complete_dma(struct mga_dev *mdev)
{
	struct mga_dma_buf *buf, *next;
	unsigned long flags;

	spin_lock_irqsave(&mdev->dma->list_lock, flags);

	list_for_each_entry_safe(buf, next, &mdev->dma->emitted_list, list) {
		if (!mga_dma_buf_is_complete(mdev, buf))
			break;
		mga_complete_dma(mdev, buf);
	}

	spin_unlock_irqrestore(&mdev->dma->list_lock, flags);
}

static void mga_dma_refresh(struct mga_dev *mdev)
{
	spin_lock_irq(&mdev->dma->prim_lock);

	mga_prim_update(mdev);

	mga_check_and_complete_dma(mdev);

	spin_unlock_irq(&mdev->dma->prim_lock);
}

/*
 * Handle a softrap interrupt.
 */
void mga_dma_handle_softrap(struct mga_dev *mdev)
{
	spin_lock(&mdev->dma->prim_lock);

	mga_prim_update(mdev);

	if (mdev->dma->prim.head == mdev->dma->hw_wrap_head)
		mga_prim_wrap_complete(mdev);
	else
		mga_prim_continue(mdev);

	mga_check_and_complete_dma(mdev);

	wake_up_all(&mdev->dma->prim_wait);

	spin_unlock(&mdev->dma->prim_lock);
}
