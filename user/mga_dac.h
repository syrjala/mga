/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_DAC_H
#define MGA_DAC_H

#include "drm_kms.h"

struct device;
struct mga_dev;
struct mga_dac_pll_info;
struct mga_dac_pll_settings;

int mga_dac_pll_calc(struct device *dev,
		     const struct mga_dac_pll_info *info,
		     unsigned int freq,
		     unsigned int fmax,
		     struct mga_dac_pll_settings *pll);

/* PIXPLLA */
int mga_dac_pixplla_calc(struct mga_dev *mdev,
			 unsigned int freq,
			 unsigned int fmax,
			 struct mga_dac_pll_settings *pll);
void mga_dac_pixplla_save(struct mga_dev *mdev,
			  struct mga_dac_pll_settings *pll);
int mga_dac_pixplla_program(struct mga_dev *mdev,
			    const struct mga_dac_pll_settings *pll);
int mga_dac_pixplla_wait(struct mga_dev *mdev);

/* PIXPLLB */
int mga_dac_pixpllb_calc(struct mga_dev *mdev,
			 unsigned int freq,
			 unsigned int fmax,
			 struct mga_dac_pll_settings *pll);
void mga_dac_pixpllb_save(struct mga_dev *mdev,
			  struct mga_dac_pll_settings *pll);
int mga_dac_pixpllb_program(struct mga_dev *mdev,
			    const struct mga_dac_pll_settings *pll);
int mga_dac_pixpllb_wait(struct mga_dev *mdev);

/* PIXPLLC */
int mga_dac_pixpllc_calc(struct mga_dev *mdev,
			 unsigned int freq,
			 unsigned int fmax,
			 struct mga_dac_pll_settings *pll);
void mga_dac_pixpllc_save(struct mga_dev *mdev,
			  struct mga_dac_pll_settings *pll);
int mga_dac_pixpllc_program(struct mga_dev *mdev,
			    const struct mga_dac_pll_settings *pll);
int mga_dac_pixpllc_wait(struct mga_dev *mdev);

/* SYSPLL */
int mga_dac_syspll_calc(struct mga_dev *mdev,
			unsigned int freq,
			unsigned int fmax,
			struct mga_dac_pll_settings *pll);
void mga_dac_syspll_save(struct mga_dev *mdev,
			 struct mga_dac_pll_settings *pll);
int mga_dac_syspll_program(struct mga_dev *mdev,
			   const struct mga_dac_pll_settings *pll);
int mga_dac_syspll_wait(struct mga_dev *mdev);

void mga_dac_pixpll_power(struct mga_dev *mdev, bool enable);
void mga_dac_syspll_power(struct mga_dev *mdev, bool enable);

/* DAC power */
int mga_dac_start(struct mga_dev *mdev, unsigned int clock);

void mga_dac_init(struct mga_dev *mdev,
		  unsigned int fref,
		  unsigned int pixpll_fvco_max,
		  unsigned int syspll_fvco_max);

void mga_dac_powerup(struct mga_dev *mdev);

int mga_dac_power_down(struct mga_dev *mdev);
int mga_dac_power_up(struct mga_dev *mdev, unsigned int clock);

/* FIXME does this affect CRTC1 or DAC1? */
void mga_dac_dac_set_sync(struct mga_dev *mdev, unsigned int sync);

void mga_dac_pedestal(struct mga_dev *mdev, bool pedestal);

/* DAC1 output */
void mga_dac_dac_power(struct mga_dev *mdev, bool enable);

/* Panel output */
void mga_dac_panel_power(struct mga_dev *mdev, bool enable);
void mga_dac_panel_dpms(struct mga_dev *mdev, unsigned int mode);
void mga_dac_panel_set_sync(struct mga_dev *mdev, unsigned int sync);

/* VDOUT modes */
void mga_dac_bt656_source(struct mga_dev *mdev, unsigned int crtc);
void mga_dac_tvo_source(struct mga_dev *mdev, unsigned int crtc);
void mga_dac_panel_source(struct mga_dev *mdev, unsigned int crtc);
void mga_dac_panel_slave(struct mga_dev *mdev, bool enable);

/* CRTC1 */
void mga_dac_crtc1_power(struct mga_dev *mdev, bool enable);
void mga_dac_crtc1_pixclk_enable(struct mga_dev *mdev, bool enable);
void mga_dac_crtc1_pixclk_select(struct mga_dev *mdev, unsigned int pll);
void mga_dac_monitor_sense_start(struct mga_dev *mdev);
void mga_dac_monitor_sense_stop(struct mga_dev *mdev,
				bool *r, bool *g, bool *b);
struct mga_dac_config;
void mga_dac_crtc1_program_format(struct mga_dev *mdev,
				  const struct mga_dac_config *config);
void mga_dac_crtc1_program(struct mga_dev *mdev,
			   const struct mga_dac_config *config);
void mga_dac_crtc1_cursor_mode(struct mga_dev *mdev,
			       unsigned int mode);
void mga_dac_crtc1_set_palette(struct mga_dev *mdev);
void mga_dac_crtc1_load_palette(struct mga_dev *mdev,
				const struct drm_color pal[256]);

/* GPIO/I2C */
void mga_dac_i2c_set(struct mga_dev *mdev, u8 pin, bool state);
bool mga_dac_i2c_get(struct mga_dev *mdev, u8 pin);

void mga_g450_dac1_monitor_sense_start(struct mga_dev *mdev);
void mga_g450_dac1_monitor_sense_stop(struct mga_dev *mdev,
				      bool *r, bool *g, bool *b);
void mga_g450_dac2_monitor_sense_start(struct mga_dev *mdev);
void mga_g450_dac2_monitor_sense_stop(struct mga_dev *mdev,
				      bool *r, bool *g, bool *b);

void mga_dac_crtc1_cursor_position(struct mga_dev *mdev,
				   unsigned int x, unsigned int y);
void mga_dac_crtc1_cursor_image(struct mga_dev *mdev,
				const u8 *data);

void mga_dac_crtc1_restore_cursor_color(struct mga_dev *mdev,
					const struct drm_color col[16]);

#endif
