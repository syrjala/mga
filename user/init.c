/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include <sys/time.h>

#include "mga_dump.h"
#include "mga_regs.h"
#include "mga_i2c.h"
#include "mga_dac.h"
#include "mga_dac_regs.h"
#include "mga_g450_dac.h"
#include "mga_flat.h"
#include "mga_dualhead.h"
#include "kernel_emul.h"
#include "mga_dma.h"
#include "mga_kms.h"

/*
 * FIXME CHECK 1164SG
 *
 * Bus FIFO size
 * 2064W, 1064SG, 1164SG = 32
 * 2164W, G100, G200 = 64
 * G200SE, G200EV, G200WB = ?
 * G400, G450, G550 = 16
 */
void mga_determine_fifo_size(struct mga_dev *mdev)
{
	u32 val;

	dev_dbg(mdev->dev, "Going to wait for BFIFO empty\n");

	/* FIXME timeout and fallback value */

	printk(KERN_CONT  "[");
	do {
		val = mga_read32(mdev, MGA_FIFOSTATUS);
		printk(KERN_CONT ".");
	} while (!(val & MGA_FIFOSTATUS_BEMPTY));
	printk(KERN_CONT "]\n");

	mdev->fifo_size = val & MGA_FIFOSTATUS_FIFOCOUNT;

	dev_dbg(mdev->dev, "BFIFO size %u\n", mdev->fifo_size);
}

static int mga_wait_fifo(struct mga_dev *mdev, unsigned int count)
{
	BUG_ON(count > mdev->fifo_size);

	dev_dbg(mdev->dev, "Going to wait for %u BFIFO entries\n", count);

	/* FIXME timeout and lockup handling */

	printk(KERN_CONT "[");
	while ((mga_read32(mdev, MGA_FIFOSTATUS) & MGA_FIFOSTATUS_FIFOCOUNT) < count)
		;//printk(KERN_CONT ".");
	printk(KERN_CONT "]\n");

	return 0;
}

#define REG_TO_INDEX(reg) (((reg & 0x2000) >> 6) | ((reg & 0x01fc) >> 2))

#define USE_DMA 1
static u32 *dma_prim_ptr;
static u32 *dma_sec_ptr;
static u32 *dma_end_ptr;

static unsigned int mga_dma(struct mga_dev *mdev,
			    unsigned int off,
			    u16 reg0, u32 data0,
			    u16 reg1, u32 data1,
			    u16 reg2, u32 data2,
			    u16 reg3, u32 data3)
{
	u32 index = ((REG_TO_INDEX(reg0) <<  0) |
		     (REG_TO_INDEX(reg1) <<  8) |
		     (REG_TO_INDEX(reg2) << 16) |
		     (REG_TO_INDEX(reg3) << 24));

	if (USE_DMA &&
	    mdev->chip > MGA_CHIP_2064W && dma_prim_ptr) {
		dma_prim_ptr[off/4 + 0] = index;
		dma_prim_ptr[off/4 + 1] = data0;
		dma_prim_ptr[off/4 + 2] = data1;
		dma_prim_ptr[off/4 + 3] = data2;
		dma_prim_ptr[off/4 + 4] = data3;
	} else {
		mga_write32(mdev, off + 0, index);
		mga_write32(mdev, off + 4, data0);
		mga_write32(mdev, off + 8, data1);
		mga_write32(mdev, off + 12, data2);
		mga_write32(mdev, off + 16, data3);
	}

	return off + 20;
}

enum {
	MGA_OPMODE_PSEUDODMA = 0x00000001,
};

#define mga_write32(mdev, reg, val)\
do { \
	dev_dbg(mdev->dev, #reg " = 0x%08x\n", val);	\
	mga_write32(mdev, reg, val); \
} while (0)

static u32 get_color(u32 pixel_format, u32 color)
{
	switch (pixel_format) {
	case DRM_FORMAT_C8:
		color = (((color & 0xe00000) >> 16) |
			 ((color & 0x00e000) >> 11) |
			 ((color & 0x0000c0) >>  6));
		return (color << 24) | (color << 16) | (color << 8) | color;
	case DRM_FORMAT_ARGB1555:
		color = (((color & 0x80000000) >> 16) |
			 ((color & 0x00f80000) >>  9) |
			 ((color & 0x0000f800) >>  6) |
			 ((color & 0x000000f8) >>  3));
		return (color << 16) | color;
	case DRM_FORMAT_XRGB1555:
		color = (0x8000 |
			 ((color & 0xf80000) >>  9) |
			 ((color & 0x00f800) >>  6) |
			 ((color & 0x0000f8) >>  3));
		return (color << 16) | color;
	case DRM_FORMAT_RGB565:
		color = (((color & 0xf80000) >>  8) |
			 ((color & 0x00fc00) >>  5) |
			 ((color & 0x0000f8) >>  3));
		return (color << 16) | color;
	case DRM_FORMAT_RGB888:
		color = (((color & 0xf80000) >>  8) |
			 ((color & 0x00fc00) >>  5) |
			 ((color & 0x0000f8) >>  3));
		return (color << 24) | color;
	case DRM_FORMAT_ARGB8888:
		return color;
	case DRM_FORMAT_XRGB8888:
		return 0xff000000 | color;
	default:
		return 0;
	}
}

static void rect(struct mga_dev *mdev,
		 const struct drm_framebuffer *fb,
		 s16 x, s16 y,
		 s16 w, s16 h,
		 u32 pitch,
		 u32 color)
{
	mga_wait_fifo(mdev, 5);

	color = get_color(fb->pixel_format, color);

	mga_write32(mdev, MGA_FXBNDRY, ((x + w) << 16) | (x & 0xffff));
	mga_write32(mdev, MGA_YDST, (y * pitch >> 5) & 0x3fffff);
	mga_write32(mdev, MGA_LEN, h & 0xffff);
	mga_write32(mdev, MGA_FCOL + MGA_DWGENG_GO, color);
}

static unsigned int rect_dma(struct mga_dev *mdev,
			     unsigned int off,
			     const struct drm_framebuffer *fb,
			     s16 x, s16 y,
			     s16 w, s16 h,
			     u32 pitch,
			     u32 color)
{
	mga_wait_fifo(mdev, 5);

	color = get_color(fb->pixel_format, color);

	return mga_dma(mdev, off,
		       MGA_FXBNDRY, ((x + w) << 16) | (x & 0xffff),
		       MGA_YDST, (y * pitch >> 5) & 0x3fffff,
		       MGA_LEN, h & 0xffff,
		       MGA_FCOL + MGA_DWGENG_GO, color);
}

int mga_get_crtc1_fb(struct mga_dev *mdev,
		     struct drm_framebuffer **fb)
{
	*fb = mdev->mode_config.crtc1.plane_config.fb;
	return 0;
}

int mga_get_crtc1_mode(struct mga_dev *mdev,
		       const struct drm_display_mode **mode)
{
	if (!mdev->mode_config.crtc1.crtc_config.mode_valid)
		return -ENODEV;
	*mode = &mdev->mode_config.crtc1.crtc_config.adjusted_mode;
	return 0;
}

int mga_get_crtc2_fb(struct mga_dev *mdev,
		     struct drm_framebuffer **fb)
{
	if (mdev->chip < MGA_CHIP_G400)
		return -ENODEV;
	*fb = mdev->mode_config.crtc2.plane_config.fb;
	return 0;
}

int mga_get_crtc2_mode(struct mga_dev *mdev,
		       const struct drm_display_mode **mode)
{
	if (mdev->chip < MGA_CHIP_G400)
		return -ENODEV;
	if (!mdev->mode_config.crtc2.crtc_config.mode_valid)
		return -ENODEV;
	*mode = &mdev->mode_config.crtc2.crtc_config.adjusted_mode;
	return 0;
}

static int mga_get_bes_fb(struct mga_dev *mdev,
			  const struct drm_framebuffer **fb)
{
	if (mdev->chip < MGA_CHIP_G200)
		return -ENODEV;
	*fb = mdev->mode_config.bes.plane_config.fb;
	return 0;
}

static int mga_get_cursor_fb(struct mga_dev *mdev,
			     const struct drm_framebuffer **fb)
{
	*fb = mdev->mode_config.cursor.plane_config.fb;
	return 0;
}

static u32 get_pwidth(u32 pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_C8:
		return MGA_MACCESS_PWIDTH_PW8;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
		return MGA_MACCESS_PWIDTH_PW16 | MGA_MACCESS_DIT555;
	case DRM_FORMAT_RGB565:
		return MGA_MACCESS_PWIDTH_PW16;
	case DRM_FORMAT_RGB888:
		return MGA_MACCESS_PWIDTH_PW24;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		return MGA_MACCESS_PWIDTH_PW32;
	default:
		return MGA_MACCESS_PWIDTH_PW8;
	}
}

void mga_solid_fill(struct mga_dev *mdev)
{
	struct drm_framebuffer *fb;
	unsigned int x, y = 0;
	unsigned int w;
	unsigned int h;
	u32 red = 0xff0000;
	u32 green = 0x00ff00;
	u32 blue = 0x0000ff;
	u32 black = 0x000000;
	u32 cyan = 0x00ffff;
	bool tvo_hack = false;
#ifdef TVO_WINDOWS
	//tvo_hack = true;
#endif
	u32 pwidth;
	u32 ydstorg = 0;
	u32 dstorg = 0;
	u32 pitch;
	unsigned int cpp;

	if (mga_get_crtc1_fb(mdev, &fb))
		return;

	if (!fb)
		return;

	switch (fb->pixel_format) {
	case DRM_FORMAT_C8:
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_RGB888:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		break;
	default:
		return;
	}

	pwidth = get_pwidth(fb->pixel_format);

	cpp = drm_format_plane_cpp(fb->pixel_format, 0);

	w = fb->width;
	h = fb->height;

	if (mdev->chip <= MGA_CHIP_G100)
		ydstorg = (to_mga_framebuffer(fb)->bus + fb->offsets[0]) / cpp;
	else
		dstorg = to_mga_framebuffer(fb)->bus + fb->offsets[0];
	pitch = fb->pitches[0] / cpp;

	dev_dbg(mdev->dev, "WIDTH = %u, HEIGHT = %u PITCH = %u\n", w, h, pitch);

#if 1
	mga_write32(mdev, MGA_OPMODE,
	       MGA_OPMODE_DMAMOD_GENERAL_PURPOSE |
	       MGA_OPMODE_DMADATASIZ_8 |
	       MGA_OPMODE_DIRDATASIZ_8);

	mga_wait_fifo(mdev, 7);

	mga_write32(mdev, MGA_PITCH, MGA_PITCH_YLIN | (pitch & 0xfff));
	if (mdev->chip <= MGA_CHIP_G100)
		mga_write32(mdev, MGA_YDSTORG, ydstorg);
	else
		mga_write32(mdev, MGA_DSTORG, dstorg);
	mga_write32(mdev, MGA_MACCESS, pwidth | mdev->maccess);

	// clip
	mga_write32(mdev, MGA_CXBNDRY, (0xFFF << 16) | 0);
	mga_write32(mdev, MGA_YTOP, 0);
	mga_write32(mdev, MGA_YBOT, 0xFFFFE0);

	//mga_write32(mdev, MGA_PLNWT, 0xFFFFFFFF);
	//mga_write32(mdev, MGA_ZORG, 0);

	// rect fill
	if (mdev->chip <= MGA_CHIP_2064W)
		mga_write32(mdev, MGA_DWGCTL,
			   MGA_DWGCTL_BOP_COPY |
			   MGA_DWGCTL_SHFTZERO |
			   MGA_DWGCTL_SGNZERO |
			   MGA_DWGCTL_ARZERO |
			   MGA_DWGCTL_SOLID |
			   MGA_DWGCTL_ATYPE_RPL |
			   MGA_DWGCTL_OPCOD_TRAP);
	else
		mga_write32(mdev, MGA_DWGCTL,
			   MGA_DWGCTL_TRANSC |
			   MGA_DWGCTL_BOP_COPY |
			   MGA_DWGCTL_SHFTZERO |
			   MGA_DWGCTL_SGNZERO |
			   MGA_DWGCTL_ARZERO |
			   MGA_DWGCTL_SOLID |
			   MGA_DWGCTL_ATYPE_RPL |
			   MGA_DWGCTL_OPCOD_TRAP);

	rect(mdev, fb, 0, 0, w, h, pitch, 0x00ff0000);
	rect(mdev, fb, 0, 0, w, 1, pitch, 0x20202020);
	rect(mdev, fb, 0, 0, 1, h, pitch, 0x20202020);
	rect(mdev, fb, w-1, 0, 1, h, pitch, 0x20202020);
	rect(mdev, fb, 0, h-1, w, 1, pitch, 0x20202020);

	rect(mdev, fb, 32, 0, 1, h-1, pitch, 0xff0000ff);
	rect(mdev, fb, 0, 45, w-1, 1, pitch, 0xff0000ff);

	mga_wait_dwgeng_idle(mdev);

#else
	mga_write32(mdev, MGA_OPMODE,
		   //MGA_OPMODE_PSEUDODMA |
		   MGA_OPMODE_DMAMOD_GENERAL_PURPOSE |
		   MGA_OPMODE_DMADATASIZ_8 |
		   MGA_OPMODE_DIRDATASIZ_8);

	unsigned int off = 0;

	off = mga_dma(mdev, off,
		MGA_PITCH,   MGA_PITCH_YLIN | pitch,
		MGA_YDSTORG, ydstorg,
		MGA_MACCESS, pwidth | mdev->maccess,
		MGA_CXBNDRY, (0xFFF << 16) | 0);
	off = mga_dma(mdev, off,
		MGA_YTOP,    0,
		MGA_YBOT,    0x7FFFFF,
		MGA_DWGCTL, (MGA_DWGCTL_TRANSC | MGA_DWGCTL_BOP_COPY |
			     MGA_DWGCTL_SHFTZERO | MGA_DWGCTL_SGNZERO |
			     MGA_DWGCTL_ARZERO | MGA_DWGCTL_SOLID |
			     MGA_DWGCTL_ATYPE_RPL | MGA_DWGCTL_OPCOD_TRAP),
		MGA_DMAPAD,  0);

	off = rect_dma(mdev, off, fb, 0, 0, w, h, pitch, 0xff0000ff);
	off = rect_dma(mdev, off, fb, 0, 0, w, 1, pitch, 0x20202020);
	off = rect_dma(mdev, off, fb, 0, 0, 1, h, pitch, 0x20202020);
	off = rect_dma(mdev, off, fb, w-1, 0, 1, h, pitch, 0x20202020);
	off = rect_dma(mdev, off, fb, 0, h-1, w, 1, pitch, 0x20202020);

	mga_wait_dwgeng_idle(mdev);

#endif
}
#undef mga_write32

void mga_wait_dwgeng_idle(struct mga_dev *mdev)
{
	unsigned int count = 1000;

	//dev_dbg(mdev->dev, "Going to wait for idle drawing engine\n");

	/* FIXME timeout and lockup handling */

	//printk(KERN_CONT "DWG [");
	while (mga_read32(mdev, MGA_STATUS) & MGA_STATUS_DWGENSTS) {
		if (--count < 1)
			break;
		usleep(1000);
		//printk(KERN_CONT ".");
	}
	//printk(KERN_CONT " %u ", count);
	//printk(KERN_CONT "]\n");
}

void mga_wait_dma_idle(struct mga_dev *mdev)
{
	//dev_dbg(mdev->dev, "Going to wait for idle DMA engine\n");

	if (mdev->chip == MGA_CHIP_2064W)
		return;

	/* FIXME timeout and lockup handling */

	//printk(KERN_CONT "DMA [");
	while (!(mga_read32(mdev, MGA_STATUS) & MGA_STATUS_ENDPRDMASTS))
		usleep(1000);
		//printk(KERN_CONT ".");
	//printk(KERN_CONT "]\n");
}

void mga_wait(struct mga_dev *mdev)
{
	mga_read32(mdev, MGA_STATUS);
	mga_read32(mdev, MGA_STATUS);
}

void mga_mclk_change_pre(struct mga_dev *mdev, unsigned int mclk)
{
	/*
	 * MCLK period shrinks or stays the same so the current
	 * rfhcnt value is safe for both old and new MCLK.
	 */
	if (mclk >= mdev->mclk)
		return;

	/*
	 * MCLK period grows so rfhcnt may need to be
	 * reduced to be safe for both old and new MCLK.
	 */
	mdev->funcs->rfhcnt(mdev, mclk);
	mdev->mclk = mclk;
}

void mga_mclk_change_post(struct mga_dev *mdev, unsigned int mclk)
{
	if (mclk == mdev->mclk)
		return;

	/*
	 * MCLK period shrunk, now it's safe to increase rfhcnt.
	 */
	mdev->funcs->rfhcnt(mdev, mclk);
	mdev->mclk = mclk;
}


#if 0
static void framebuffer_init(struct mga_dev *mdev)
{
	(void)mdev;
	mga_write32(mdev, MGA_OPMODE,
	       MGA_DMAMOD_GENERAL_PURPOSE |
	       MGA_DMADATASIZ_8 |
	       MGA_DIRDATASIZ_8);
	mga_write32(mdev, MGA_PLNWT, 0xFFFFFFFF);
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_JEDECRST);
	mga_write32(mdev, MGA_YDSTORG, 0);
	mga_write32(mdev, MGA_ZORG, 0);
}
#endif

typedef void (*pixel_func_t)(u8 *mem, unsigned int x, u32 value);

static unsigned int pixel_cnt;

static void pixeli(u8 *mem, unsigned int x, u32 value)
{
	writeb(value & 0xff, mem + x);
	pixel_cnt++;
}

static void pixel_yuyv(u8 *mem, unsigned int x, u32 value)
{
	u32 v = ((value & 0x0000ff) << 24) |
		((value & 0xff0000) >>  0) |
		((value & 0x00ff00) >>  0) |
		((value & 0xff0000) >> 16);
	BUG_ON(x & 1);
	writel(v, mem + (x << 1));
	pixel_cnt++;
}

static void pixel_uyvy(u8 *mem, unsigned int x, u32 value)
{
	u32 v = ((value & 0xff0000) <<  8) |
		((value & 0x0000ff) << 16) |
		((value & 0xff0000) >>  8) |
		((value & 0x00ff00) >>  8);
	BUG_ON(x & 1);
	writel(v, mem + (x << 1));
	pixel_cnt++;
}

static void pixel_y(u8 *mem, unsigned int x, u32 value)
{
	u8 v = (value & 0xff0000) >> 16;
	writeb(v, mem + x);
	pixel_cnt++;
}

static void pixel_cb(u8 *mem, unsigned int x, u32 value)
{
	u8 v = (value & 0x00ff00) >> 8;
	BUG_ON(x & 1);
	writeb(v, mem + (x >> 1));
	pixel_cnt++;
}

static void pixel_cr(u8 *mem, unsigned int x, u32 value)
{
	u8 v = (value & 0x0000ff) >> 0;
	BUG_ON(x & 1);
	writeb(v, mem + (x >> 1));
	pixel_cnt++;
}

static void pixel_nv12(u8 *mem, unsigned int x, u32 value)
{
	u16 v = ((value & 0xff00) >> 8) | ((value & 0x00ff) << 8);
	BUG_ON(x & 1);
	writew(v, mem + x);
	pixel_cnt++;
}

static void pixel_nv21(u8 *mem, unsigned int x, u32 value)
{
	u16 v = value & 0xffff;
	BUG_ON(x & 1);
	writew(v, mem + x);
	pixel_cnt++;
}

static void pixel_332(u8 *mem, unsigned int x, u32 value)
{
	u8 v = (((value & 0xe00000) >> 16) |
		((value & 0x00e000) >> 11) |
		((value & 0x0000c0) >>  6));
	writeb(v, mem + x);
	pixel_cnt++;
}

static void pixel_1555(u8 *mem, unsigned int x, u32 value)
{
	u16 v = (((value & 0xf80000) >> 9) |
		 ((value & 0x00f800) >> 6) |
		 ((value & 0x0000f8) >> 3));
	writew(v, mem + (x << 1));
	pixel_cnt++;
}

static void pixel_565(u8 *mem, unsigned int x, u32 value)
{
	u16 v = (((value & 0xf80000) >> 8) |
		 ((value & 0x00fc00) >> 5) |
		 ((value & 0x0000f8) >> 3));
	writew(v, mem + (x << 1));
	pixel_cnt++;
}

static void pixel_888(u8 *mem, unsigned int x, u32 value)
{
	writeb(value >>  0, mem + (x * 3) + 0);
	writeb(value >>  8, mem + (x * 3) + 1);
	writeb(value >> 16, mem + (x * 3) + 2);
	pixel_cnt++;
}

static void pixel_8888(u8 *mem, unsigned int x, u32 value)
{
	writel(value, mem + (x << 2));
	pixel_cnt++;
}

static void pixel_ovl15(u8 *mem, unsigned int x, u32 value)
{
	(void)value;
	writew(readw(mem + (x << 1)) | 0x8000, mem + (x << 1));
	pixel_cnt++;
}

static void pixel_ovl32(u8 *mem, unsigned int x, u32 value)
{
	(void)value;
	writel(readl(mem + (x << 2)) | 0xff000000, mem + (x << 2));
	pixel_cnt++;
}

#define LINE3(c0, c1, c2)			\
do {						\
	if (y & vsub) {				\
		y++;				\
		break;				\
	}					\
	x = 0;					\
	if (!(x & hsub))			\
		pixel(mem, x++, c0);		\
	for (; x < w - 1; x++) {		\
		if (!(x & hsub))		\
			pixel(mem, x, c1);	\
	}					\
	if (!(x & hsub))			\
		pixel(mem, x++, c2);		\
	mem += pitch;				\
	y++;					\
} while (0)

#define LINE1(c0)				\
do {						\
	if (y & vsub) {				\
		y++;				\
		break;				\
	}					\
	for (x = 0; x < w; x++) {		\
		if (!(x & hsub))		\
			pixel(mem, x, c0);	\
	}					\
	mem += pitch;				\
	y++;					\
} while (0)

#define LINE2(c0, c2)					\
do {							\
	if (y & vsub) {					\
		y++;					\
		break;					\
	}						\
	x = 0;						\
	if (!(x & hsub))				\
		pixel(mem, x++, c0);			\
	for (; x < w - 1; x++) {			\
		u32 p = x & 0xff;			\
		p = (p << 16) | (p << 8) | p;		\
		p = color_csc(fb->pixel_format, p);	\
		if (!(x & hsub))			\
			pixel(mem, x, p);		\
	}						\
	if (!(x & hsub))				\
		pixel(mem, x++, c2);			\
	mem += pitch;					\
	y++;						\
} while (0)

#define LINE2_2(c0, c2)					\
do {							\
	if (y & vsub) {					\
		y++;					\
		break;					\
	}						\
	x = 0;						\
	if (!(x & hsub))				\
		pixel(mem, x++, c0);			\
	if (!(x & hsub))				\
		pixel(mem, x++, c0);			\
	for (; x < w - 2; x++) {			\
		u32 p = ((x + phase) ^ 0xff) & 0xff;	\
		p = (p << 16) | (p << 8) | p;		\
		p = color_csc(fb->pixel_format, p);	\
		if (!(x & hsub))			\
			pixel(mem, x, p);		\
	}						\
	if (!(x & hsub))				\
		pixel(mem, x++, c2);			\
	if (!(x & hsub))				\
		pixel(mem, x++, c2);			\
	mem += pitch;					\
	y++;						\
} while (0)

#define LINE2_3(c0, c1)					\
do {							\
	if (y & vsub) {					\
		y++;					\
		break;					\
	}						\
	x = 0;						\
	if (!(x & hsub))				\
		pixel(mem, x++, c0);			\
	for (; x < w - 1; x++) {			\
		u32 p = (phase + x) & 0xff;		\
		p = (p << 16) | (p << 8) | p;		\
		p = color_csc(fb->pixel_format, p);	\
		if (!(x & hsub))			\
			pixel(mem, x, p);		\
	}						\
	if (!(x & hsub))				\
		pixel(mem, x++, c1);			\
	mem += pitch;					\
	y++;						\
	phase++;					\
} while (0)

#define LINE_2(c0, c1)					\
do {							\
	if (y & vsub) {					\
		y++;					\
		break;					\
	}						\
	x = 0;						\
	if (!(x & hsub))				\
		pixel(mem, x++, c0);			\
	for (; x < w - 1; x++) {			\
		u32 p = color_csc(fb->pixel_format, 0);	\
		if (!(x & hsub))			\
			pixel(mem, x, p);		\
	}						\
	if (!(x & hsub))				\
		pixel(mem, x++, c1);			\
	mem += pitch;					\
	y++;						\
} while (0)

static pixel_func_t get_pixel_func(unsigned int pixel_format, int plane)
{
	switch (pixel_format) {
	case DRM_FORMAT_C8:
		return pixel_332;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
		return pixel_1555;
	case DRM_FORMAT_RGB565:
		return pixel_565;
	case DRM_FORMAT_RGB888:
		return pixel_888;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		return pixel_8888;
	case DRM_FORMAT_YUYV:
		return pixel_yuyv;
	case DRM_FORMAT_UYVY:
		return pixel_uyvy;
	case DRM_FORMAT_YUV420:
		switch (plane) {
		case 0:
			return pixel_y;
		case 1:
			return pixel_cb;
		case 2:
			return pixel_cr;
		default:
			BUG();
		}
		break;
	case DRM_FORMAT_YVU420:
		switch (plane) {
		case 0:
			return pixel_y;
		case 1:
			return pixel_cr;
		case 2:
			return pixel_cb;
		default:
			BUG();
		}
		break;
	case DRM_FORMAT_NV12:
		switch (plane) {
		case 0:
			return pixel_y;
		case 1:
			return pixel_nv12;
		default:
			BUG();
		}
		break;
	case DRM_FORMAT_NV21:
		switch (plane) {
		case 0:
			return pixel_y;
		case 1:
			return pixel_nv21;
		default:
			BUG();
		}
		break;
	default:
		BUG();
	}
}

static u32 color_csc(unsigned int pixel_format, u32 color)
{
	int a, r, g, b;
	int y, cb, cr;

	switch (pixel_format) {
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		break;
	default:
		return color;
	}

	a = (color & 0xff000000) >> 24;
	r = (color & 0x00ff0000) >> 16;
	g = (color & 0x0000ff00) >>  8;
	b = (color & 0x000000ff) >>  0;

	y  = (   66 * r + 129 * g +  25 * b  +  16*256 + 128) >> 8;
	cb = ( - 38 * r -  74 * g + 112 * b  + 128*256 + 128) >> 8;
	cr = (  112 * r -  94 * g -  18 * b  + 128*256 + 128) >> 8;

	return (a << 24) | (y << 16) | (cb <<  8) | (cr <<  0);
}

static unsigned int get_hsub(unsigned int pixel_format, int plane)
{
	if (plane > 0 ||
	    pixel_format == DRM_FORMAT_YUYV ||
	    pixel_format == DRM_FORMAT_UYVY)
		return drm_format_horz_subsampling(pixel_format) - 1;

	return 0;
}

static unsigned int get_vsub(unsigned int pixel_format, int plane)
{
	if (plane > 0)
		return drm_format_vert_subsampling(pixel_format) - 1;

	return 0;
}

static void clear_plane(const struct drm_framebuffer *fb, u32 color, int plane)
{
	unsigned int hsub = get_hsub(fb->pixel_format, plane);
	unsigned int vsub = get_vsub(fb->pixel_format, plane);
	unsigned int width = fb->width;
	unsigned int height = fb->height;
	u8 *mem = mga_framebuffer_calc_virt(to_mga_framebuffer(fb), 0, 0, plane, 0);
	pixel_func_t pixel = get_pixel_func(fb->pixel_format, plane);
	unsigned int x, y;

	color = color_csc(fb->pixel_format, color);

	for (y = 0; y < height; y++) {
		if (y & hsub)
			continue;
		for (x = 0; x < width; x++) {
			if (x & hsub)
				continue;
			pixel(mem, x, color);
		}

		mem += fb->pitches[plane];
	}
}

static void clear_fb(const struct drm_framebuffer *fb, uint32_t color)
{
	int i;

	if (!fb)
		return;

	for (i = 0; i < drm_format_num_planes(fb->pixel_format); i++)
		clear_plane(fb, color, i);
}

static void fill_crtc1(struct mga_dev *mdev)
{
	struct drm_framebuffer *fb;
	const struct drm_display_mode *mode;
	u8 *mem;
	u8 *memovl;
	unsigned int x, y = 0;
	unsigned int w;
	unsigned int h;
	unsigned int pitch;
	u32 black = 0x000000;
	u32 white = 0xffffff;
	u32 red = 0xff0000;
	u32 green = 0x00ff00;
	u32 blue = 0x0000ff;
	u32 cyan = 0x00ffff;
	u32 magenta = 0xff00ff;
	u32 yellow = 0xffff00;
	unsigned int cpp;
	void (*pixel)(u8 *mem, unsigned int x, u32 value);
	bool tvo_hack = false;
#ifdef TVO_WINDOWS
	//tvo_hack = true;
#endif
	unsigned int hsub = 0, vsub = 0;

	if (mga_get_crtc1_fb(mdev, &fb))
		return;

	if (!fb)
		return;

	black = color_csc(fb->pixel_format, black);
	white = color_csc(fb->pixel_format, white);

	red = color_csc(fb->pixel_format, red);
	green = color_csc(fb->pixel_format, green);
	blue = color_csc(fb->pixel_format, blue);

	cyan = color_csc(fb->pixel_format, cyan);
	magenta = color_csc(fb->pixel_format, magenta);
	yellow = color_csc(fb->pixel_format, yellow);

	mem = mga_framebuffer_calc_virt(to_mga_framebuffer(fb), 0, 0, 0, 0);
	memovl = mga_framebuffer_calc_virt(to_mga_framebuffer(fb), 0, 0, 0, 0);

	cpp = drm_format_plane_cpp(fb->pixel_format, 0);

	w = fb->width;
	h = fb->height;

	if (tvo_hack)
		w -= 8;

	pitch = fb->pitches[0];

	dev_dbg(mdev->dev, "WIDTH = %u, HEIGHT = %u, PITCH = %u, FORMAT=%u\n", w, h, pitch, fb->pixel_format);

	/* Clear to white */
	clear_fb(fb, 0xffffffff);

	if (tvo_hack) {
		unsigned int y;
		for (y = 0; y < h; y++)
			memset(mem + y * pitch + w * cpp,
			       0x00, 8 * cpp);
	}

	pixel = get_pixel_func(fb->pixel_format, 0);

	//int phase = w & 0xff;
	//int phase = 12;
	int phase = 0;
	int plane = 0;

	LINE1(red);
	LINE1(red);

	for (; y < h/5; )
		LINE2_3(cyan, green);

	//LINE1(black);

	for (; y < 2*h/5; )
		LINE2_3(cyan, green);

	//LINE1(black);

	for (; y < 3*h/5; )
		LINE2_3(cyan, green);

	//LINE1(black);

	for (; y < 4*h/5; )
		LINE2_3(cyan, green);

	//LINE1(black);

	for (; y < h - 2; )
		LINE2_3(cyan, green);

	LINE1(blue);
	LINE1(blue);

	return;

	switch (fb->pixel_format) {
	case DRM_FORMAT_XRGB1555:
		pixel = pixel_ovl15;
		break;
	case DRM_FORMAT_XRGB8888:
		pixel = pixel_ovl32;
		break;
	default:
#if 1
		mem = mdev->mem_virt + (2 << 20) - 1;
		for (x = 0; x < 4; x++)
			*mem-- = 0xaa;
		mem = mdev->mem_virt + (2 << 20);
		for (x = 0; x < 4; x++)
			*mem++ = 0x00;
#endif
		return;
	}

	mem = memovl + ALIGN(pitch / 3, cpp);
	w = w / 3;
	mem += h / 3 * pitch;
	for (y = 0; y < h/3; )
		LINE1(black);
}

static void fill_crtc2(struct mga_dev *mdev)
{
	struct drm_framebuffer *fb;
	u8 *mem;
	unsigned int x, y;
	unsigned int w, h;
	unsigned int pitch;
	u32 black = 0x000000;
	u32 white = 0xffffff;
	u32 red = 0xff0000;
	u32 green = 0x00ff00;
	u32 blue = 0x0000ff;
	u32 cyan = 0x00ffff;
	u32 magenta = 0xff00ff;
	u32 yellow = 0xffff00;
	pixel_func_t pixel;
	int plane;

	if (mga_get_crtc2_fb(mdev, &fb))
		return;

	if (!fb)
		return;

	/* Clear to black */
	clear_fb(fb, 0x00000000);

	black = color_csc(fb->pixel_format, black);
	white = color_csc(fb->pixel_format, white);

	red = color_csc(fb->pixel_format, red);
	green = color_csc(fb->pixel_format, green);
	blue = color_csc(fb->pixel_format, blue);

	cyan = color_csc(fb->pixel_format, cyan);
	magenta = color_csc(fb->pixel_format, magenta);
	yellow = color_csc(fb->pixel_format, yellow);

	for (plane = 0; plane < drm_format_num_planes(fb->pixel_format); plane++) {
		//int phase = w % 256;
		int phase = 0;

		unsigned int hsub = get_hsub(fb->pixel_format, plane);
		unsigned int vsub = get_vsub(fb->pixel_format, plane);

		w = fb->width;
		h = fb->height;
		pitch = fb->pitches[plane];
		y = 0;
		pixel = get_pixel_func(fb->pixel_format, plane);
		mem = mga_framebuffer_calc_virt(to_mga_framebuffer(fb), 0, 0, plane, 0);

		dev_dbg(mdev->dev, "w = %u h = %u pitch = %u mem = %p plane = %d\n",
			w, h, pitch, mem, plane);

		LINE1(blue);

		for (; y < h/5; )
			LINE2_2(red, blue);

		//LINE1(black);

		for (; y < 2*h/5; )
			LINE2_2(red, blue);

		//LINE1(black);

		for (; y < 3*h/5; )
			LINE2_2(red, blue);

		//LINE1(black);

		for (; y < 4*h/5; )
			LINE2_2(red, blue);

		//LINE1(black);

		for (; y < h - 1; )
			LINE2_2(red, blue);

		LINE1(red);
	}
}

static void fill_bes(struct mga_dev *mdev)
{
	const struct drm_framebuffer *fb;
	u8 *mem;
	unsigned int x, y = 0;
	unsigned int w, h;
	unsigned int pitch;
	u32 black = 0x000000;
	u32 white = 0xffffff;
	u32 red = 0xff0000;
	u32 green = 0x00ff00;
	u32 blue = 0x0000ff;
	u32 cyan = 0x00ffff;
	u32 magenta = 0xff00ff;
	u32 yellow = 0xffff00;
	pixel_func_t pixel;
	int plane;

	if (mga_get_bes_fb(mdev, &fb))
		return;

	if (!fb)
		return;

	/* Clear to yellow */
	clear_fb(fb, 0xffffff00);

	black = color_csc(fb->pixel_format, black);
	white = color_csc(fb->pixel_format, white);

	red = color_csc(fb->pixel_format, red);
	green = color_csc(fb->pixel_format, green);
	blue = color_csc(fb->pixel_format, blue);

	cyan = color_csc(fb->pixel_format, cyan);
	magenta = color_csc(fb->pixel_format, magenta);
	yellow = color_csc(fb->pixel_format, yellow);

	for (plane = 0; plane < drm_format_num_planes(fb->pixel_format); plane++) {
		//int phase = w % 256;
		int phase = 0;

		unsigned int hsub = get_hsub(fb->pixel_format, plane);
		unsigned int vsub = get_vsub(fb->pixel_format, plane);

		w = fb->width;
		h = fb->height;
		pitch = fb->pitches[plane];
		y = 0;
		pixel = get_pixel_func(fb->pixel_format, plane);
		mem = mga_framebuffer_calc_virt(to_mga_framebuffer(fb), 0, 0, plane, 0);

		dev_dbg(mdev->dev, "w = %u h = %u pitch = %u mem = %p plane = %d\n",
			w, h, pitch, mem, plane);

		pixel_cnt = 0;

		LINE1(yellow);
		LINE1(yellow);

		for (; y < h/4; )
			LINE2_2(red, blue);

		LINE1(black);
		LINE1(black);

		for (; y < 2*h/4; )
			LINE2_2(red, blue);

		LINE1(black);
		LINE1(black);

		for (; y < 3*h/4; )
			LINE2_2(red, blue);

		LINE1(black);
		LINE1(black);

		for (; y < h - 2; )
			LINE2_2(red, blue);

		LINE1(magenta);
		LINE1(magenta);

		dev_dbg(mdev->dev, "wrote %u pixels\n", pixel_cnt);
	}
}

static void fill_cursor(struct mga_dev *mdev)
{
	const struct drm_framebuffer *fb;
	u8 *mem;
	unsigned int x, y = 0;
	unsigned int w, h;
	unsigned int pitch;
	u32 black = 0;
	u32 white = 1;
	u32 red = 2;
	u32 green = 3;
	u32 blue = 4;
	u32 cyan = 5;
	u32 magenta = 6;
	u32 yellow = 7;

	if (mga_get_cursor_fb(mdev, &fb))
		return;

	if (!fb)
		return;

	mem = mga_framebuffer_calc_virt(to_mga_framebuffer(fb), 0, 0, 0, 0);

	for (y = 0; y < 64; y++) {
		u64 slice[6] = { [0] = 0, };
		int i;

		for (i = 0; i < 4; i++) {
			for (x = 0; x < 16; x++)
				slice[i] |= ((u64) ((x+y) & 15)) << (x << 2);
		}

		dev_dbg(mdev->dev, "cursor line %d:\n", y);
		for (i = 0; i < 6; i++)
			dev_dbg(mdev->dev, " slice[%d] = 0x%016llx\n", i, slice[i]);

		memcpy(mem, slice, sizeof slice);

		mem += sizeof slice;
	}
}

enum {
	DDC_ADDR = 0x50,
	EDID_LENGTH = 0x80,
};

static void get_edid(struct mga_dev *mdev, u8 addr,
		     struct mga_i2c_channel *channel, const char *name)
{
	int ret;
	u8 buf[EDID_LENGTH] = { 0 };
	ret = i2c_smbus_read_buf(&channel->adap, addr, 0, buf, sizeof buf);
	if (ret < 0) {
		dev_err(mdev->dev, "Failed to read EDID for %s\n", name);
	} else {
		int i;
		printk(KERN_CONT "EDID for %s:", name);
		for (i = 0 ; i < (int) sizeof buf; i++) {
			if (i % 8 == 0)
				printk(KERN_CONT "\n");
			printk(KERN_CONT "%02x ", buf[i]);
		}
		printk(KERN_CONT "\n");
	}
}

static void try_ddc1_i2c(struct mga_dev *mdev)
{
	int ret = i2c_probe(&mdev->ddc1.adap, DDC_ADDR);
	if (ret == 0)
		dev_dbg(mdev->dev, "DDC1: Detected I2C device @ 0x%02x\n",
			DDC_ADDR);
	else
		dev_dbg(mdev->dev, "DDC2: No I2C device @ 0x%02x\n",
			DDC_ADDR);
}

static void try_ddc2_i2c(struct mga_dev *mdev)
{
	int ret = i2c_probe(&mdev->ddc2.adap, DDC_ADDR);
	if (ret == 0)
		dev_dbg(mdev->dev, "DDC2: Detected I2C device @ 0x%02x\n",
			DDC_ADDR);
	else
		dev_dbg(mdev->dev, "DDC2: No I2C device @ 0x%02x\n",
			DDC_ADDR);
}

static void try_ddcd_i2c(struct mga_dev *mdev)
{
	int ret = i2c_probe(&mdev->ddcd.adap, DDC_ADDR);
	if (ret == 0)
		dev_dbg(mdev->dev, "DDCD: Detected I2C device @ 0x%02x\n",
			DDC_ADDR);
	else
		dev_dbg(mdev->dev, "DDCD: No I2C device @ 0x%02x\n",
			DDC_ADDR);
}

static void try_misc_i2c(struct mga_dev *mdev)
{
	// 1a for G400 dual head addon tvo
	// 1b for G400 dual head board tvo
	// 20 for G400 dual head addon pcf8574
	// 22 for G200 flat panel addon pcf8574
	// 26 for G400 flat panel addon pcf8574
	u8 addr[] = {
		0x1A, 0x1B,
		0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,
		0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
	};
	int i;

	for (i = 0; i < (int) sizeof addr; i++) {
		int ret = i2c_probe(&mdev->misc.adap, addr[i]);
		if (ret == 0)
			dev_dbg(mdev->dev, "Detected I2C device @ 0x%02x\n",
				addr[i]);
	}
	for (i = 0; i < (int) 0x3F; i++) {
		int ret = i2c_probe(&mdev->misc.adap, addr[i]);
		if (ret == 0)
			dev_dbg(mdev->dev, "Detected I2C device @ 0x%02x\n",
				addr[i]);
	}
}

static void print_diff(struct device *dev,
		       const struct timeval *start,
		       const struct timeval *end)
{
	struct timeval tv = *end;
	tv.tv_sec -= start->tv_sec;
	tv.tv_usec -= start->tv_usec;
	if (tv.tv_usec < 0) {
		tv.tv_usec += 1000000;
		tv.tv_sec -= 1;
	}
	dev_dbg(dev, "duration = %lu.%lu\n", tv.tv_sec, tv.tv_usec);
}

static void mga_pm_test(struct mga_dev *mdev)
{
	sleep(5);

	mdev->funcs->suspend(mdev);

	sleep(5);

	mdev->funcs->resume(mdev);

	if (mdev->funcs->monitor_sense_dac1)
		mdev->funcs->monitor_sense_dac1(mdev);
	if (mdev->funcs->monitor_sense_dac2)
		mdev->funcs->monitor_sense_dac2(mdev);
}

#include <stdio.h>
void mga_reg_test(struct mga_dev *mdev)
{
#if 0
	int i = 1000000;
	u32 max_f0 = 0;
	u32 max_f1 = 0;
	u32 f0tof1 = 0;
	u32 f1tof0 = 0;
	u32 lastl = 0;
	u32 lastf = 0xffffff;
#endif
#if 0
	u32 x = mga_read32(mdev, MGA_C2MISC);
	//mga_write32(mdev, MGA_C2MISC, x | 0x77);
	//mga_write32(mdev, MGA_C2MISC, x | 0x10);
	//mga_write32(mdev, MGA_C2MISC, x | 0x01);
	/* NTSC: F=1 on line 266, F=0 on line 4 */
	//mga_write32(mdev, MGA_C2MISC, x | 0x12);
	/* PAL: F=1 on line 313, F=0 on line 1 */
	//mga_write32(mdev, MGA_C2MISC, x | 0x11);
#endif
#if 0
	while (i--) {
		u32 c = mga_read32(mdev, MGA_C2VCOUNT);
		u32 l = (c & MGA_C2VCOUNT_C2VCOUNT) + 1;
		u32 f = !!(c & MGA_C2VCOUNT_C2FIELD);
		if (f) {
			if (l > max_f1)
				max_f1 = l;
		} else {
			if (l > max_f0)
				max_f0 = l;
		}
		if (l == lastl + 1 && f != lastf) {
			if (f) {
				f0tof1 = l;
			} else {
				f1tof0 = l;
			}
		}
		if (l == 1)
			dev_dbg(mdev->dev, "%u: %u\n", f, l);
		else if (l == lastl + 1) {
			dev_dbg(mdev->dev, "%u: %u\n", lastf, lastl);
			dev_dbg(mdev->dev, "%u: %u\n", f, l);
		}
		lastf = f;
		lastl = l;
	}
	dev_dbg(mdev->dev, "max_f0 = %u\n", max_f0);
	dev_dbg(mdev->dev, "max_f1 = %u\n", max_f1);
	dev_dbg(mdev->dev, "f0tof1 = %u\n", f0tof1);
	dev_dbg(mdev->dev, "f1tof0 = %u\n", f1tof0);
#endif

	dev_dbg(mdev->dev, "REG TEST BEGIN:\n");

	while (1) {
		char buf[32];
		unsigned int reg, val;
		char len;

		char *s = fgets(buf, sizeof buf, stdin);
		if (!s)
			continue;
		buf[sizeof buf - 1] = 0;

		if (buf[0] == 'q')
			return;

		if (buf[0] == 'p' && buf[1] == 'm') {
			mga_pm_test(mdev);
			continue;
		}

		int write = 1;
		if (sscanf(buf, "%c/%x=%x", &len, &reg, &val) != 3) {
			if (sscanf(buf, "%c/%x", &len, &reg) != 2) {
				continue;
			}
			write = 0;
		}

		pr_debug("len = %c, reg = %x, val = %x\n",
			 len, reg, val);

		if (len != 'b' && len != 'w' && len != 'l' && len != 'd' && len != 'c' && len != 'x')
			continue;

		if (len == 'b' || len == 'w' || len == 'l') {
			if (reg < 0x1c00 || reg > 0x3f00)
				continue;
		} else if (len == 'd' || len == 'c' || len == 'x') {
			if (reg > 0xff)
				continue;
		} else {
			continue;
		}

		if (len == 'd' && mdev->chip < MGA_CHIP_1064SG)
			continue;

		if (len == 'l') {
			if (write) {
				if (val > 0xffffffff)
					continue;
				mga_write32(mdev, reg, val);
			} else {
				val = mga_read32(mdev, reg);
				dev_dbg(mdev->dev, "%04x = %08x\n", reg, val);
			}
		}
		if (len == 'w') {
			if (write) {
				if (val > 0xffff)
					continue;
				mga_write16(mdev, reg, val);
			} else {
				val = mga_read16(mdev, reg);
				dev_dbg(mdev->dev, "%04x = %04x\n", reg, val);
			}
		}
		if (len == 'b') {
			if (write) {
				if (val > 0xff)
					continue;
				mga_write8(mdev, reg, val);
			} else {
				val = mga_read8(mdev, reg);
				dev_dbg(mdev->dev, "%04x = %02x\n", reg, val);
			}
		}
		if (len == 'd') {
			if (write) {
				if (val > 0xff)
					continue;
				mga_write8(mdev, MGA_X_INDEXREG, reg);
				mga_write8(mdev, MGA_X_DATAREG, val);
			} else {
				mga_write8(mdev, MGA_X_INDEXREG, reg);
				val = mga_read8(mdev, MGA_X_DATAREG);
				dev_dbg(mdev->dev, "DAC %02x = %02x\n", reg, val);
			}
		}
		if (len == 'c') {
			if (write) {
				if (val > 0xff)
					continue;
				mga_write8(mdev, MGA_CRTCX, reg);
				mga_write8(mdev, MGA_CRTCD, val);
			} else {
				mga_write8(mdev, MGA_CRTCX, reg);
				val = mga_read8(mdev, MGA_CRTCD);
				dev_dbg(mdev->dev, "CRTC%02x = %02x\n", reg, val);
			}
		}
		if (len == 'x') {
			if (write) {
				if (val > 0xff)
					continue;
				mga_write8(mdev, MGA_CRTCEXTX, reg);
				mga_write8(mdev, MGA_CRTCEXTD, val);
			} else {
				mga_write8(mdev, MGA_CRTCEXTX, reg);
				val = mga_read8(mdev, MGA_CRTCEXTD);
				dev_dbg(mdev->dev, "CRTCEXT%02x = %02x\n", reg, val);
			}
		}
	}
}

int mga_reg_rw(struct mga_dev *mdev, u32 dev, u32 dir, u32 size, u32 reg, u32 *val)
{
	dev_dbg(mdev->dev, "%s: dev=%x dir=%x size=%x reg=%x val=%x\n",
		__func__, dev, dir, size, reg, *val);

	switch (size) {
	case DRM_REG_RW_BYTE:
	case DRM_REG_RW_WORD:
	case DRM_REG_RW_LONG:
		break;
	default:
		return -EINVAL;
	}

	switch (dir) {
	case DRM_REG_RW_READ:
		break;
	case DRM_REG_RW_WRITE_READ:
		switch (size) {
		case DRM_REG_RW_BYTE:
			if (*val > 0xff)
				return -EINVAL;
			break;
		case DRM_REG_RW_WORD:
			if (*val > 0xffff)
				return -EINVAL;
			break;
		case DRM_REG_RW_LONG:
			if (*val > 0xffffffff)
				return -EINVAL;
			break;
		}
		break;
	default:
		return -EINVAL;
	}

	switch (dev) {
	case DRM_REG_RW_DAC:
	case DRM_REG_RW_CRTC:
	case DRM_REG_RW_CRTCEXT:
	case DRM_REG_RW_GCTL:
	case DRM_REG_RW_SEQ:
	case DRM_REG_RW_ATTR:
		if (size != DRM_REG_RW_BYTE)
			return -EINVAL;
		break;
	case DRM_REG_RW_TVO:
		if (size == DRM_REG_RW_LONG)
			return -EINVAL;
		break;
	case DRM_REG_RW_PCI:
		if (size != DRM_REG_RW_LONG)
			return -EINVAL;
		break;
	case DRM_REG_RW_REG:
		break;
	default:
		return -EINVAL;
	}

	switch (dev) {
	case DRM_REG_RW_DAC:
		mga_write8(mdev, MGA_X_INDEXREG, reg);
		if (dir == DRM_REG_RW_WRITE_READ)
			mga_write8(mdev, MGA_X_DATAREG, *val);
		*val = mga_read8(mdev, MGA_X_DATAREG);
		break;
	case DRM_REG_RW_CRTC:
		mga_write8(mdev, MGA_CRTCX, reg);
		if (dir == DRM_REG_RW_WRITE_READ)
			mga_write8(mdev, MGA_CRTCD, *val);
		*val = mga_read8(mdev, MGA_CRTCD);
		break;
	case DRM_REG_RW_CRTCEXT:
		mga_write8(mdev, MGA_CRTCEXTX, reg);
		if (dir == DRM_REG_RW_WRITE_READ)
			mga_write8(mdev, MGA_CRTCEXTD, *val);
		*val = mga_read8(mdev, MGA_CRTCEXTD);
		break;
	case DRM_REG_RW_GCTL:
		mga_write8(mdev, MGA_GCTLX, reg);
		if (dir == DRM_REG_RW_WRITE_READ)
			mga_write8(mdev, MGA_GCTLD, *val);
		*val = mga_read8(mdev, MGA_GCTLD);
		break;
	case DRM_REG_RW_SEQ:
		mga_write8(mdev, MGA_SEQX, reg);
		if (dir == DRM_REG_RW_WRITE_READ)
			mga_write8(mdev, MGA_SEQD, *val);
		*val = mga_read8(mdev, MGA_SEQD);
		break;
	case DRM_REG_RW_ATTR:
		if (dir == DRM_REG_RW_WRITE_READ) {
			mga_read8(mdev, MGA_INSTS1_R);
			mga_write8(mdev, MGA_ATTRX, reg);
			mga_write8(mdev, MGA_ATTRD_W, *val);
		}
		mga_read8(mdev, MGA_INSTS1_R);
		mga_write8(mdev, MGA_ATTRX, reg);
		*val = mga_read8(mdev, MGA_ATTRD_R);
		break;
	case DRM_REG_RW_REG:
		switch (size) {
		case DRM_REG_RW_BYTE:
			if (dir == DRM_REG_RW_WRITE_READ)
				mga_write8(mdev, reg, *val);
			*val = mga_read8(mdev, reg);
			break;
		case DRM_REG_RW_WORD:
			if (dir == DRM_REG_RW_WRITE_READ)
				mga_write16(mdev, reg, *val);
			*val = mga_read16(mdev, reg);
			break;
		case DRM_REG_RW_LONG:
			if (dir == DRM_REG_RW_WRITE_READ)
				mga_write32(mdev, reg, *val);
			*val = mga_read32(mdev, reg);
			break;
		}
		break;
	case DRM_REG_RW_PCI:
		if (dir == DRM_REG_RW_WRITE_READ)
			pci_cfg_write32(&mdev->pdev, reg, *val);
		*val = pci_cfg_read32(&mdev->pdev, reg);
		break;
	case DRM_REG_RW_TVO:
		if (!(mdev->features & MGA_FEAT_TVO_BUILTIN) &&
		    !(mdev->features & MGA_FEAT_TVO_ADDON))
			return -EINVAL;
		switch (size) {
		case DRM_REG_RW_BYTE:
			if (dir == DRM_REG_RW_WRITE_READ)
				mga_tvo_write8(&mdev->tvodev, reg, *val);
			*val = mga_tvo_read8(&mdev->tvodev, reg);
			break;
		case DRM_REG_RW_WORD:
			if (dir == DRM_REG_RW_WRITE_READ)
				mga_tvo_write16(&mdev->tvodev, reg, *val);
			*val = mga_tvo_read16(&mdev->tvodev, reg);
			break;
		}
	}

	return 0;
}

int mga_hardreset(struct mga_dev *mdev, bool force)
{
	u32 cfg[16];
	u32 pm_csr;
	int i;

	if ((pci_cfg_read32(&mdev->pdev, 0x34) & 0xff) != 0xdc ||
	    (pci_cfg_read32(&mdev->pdev, 0xdc) & 0xff) == 0x00) {
		dev_dbg(mdev->dev, "No PM support, skipping PCI PM\n");
		return -ENODEV; // FIXME
	}

	if (pci_cfg_read32(&mdev->pdev, 0x04) & 0x1 && !force) {
		dev_dbg(mdev->dev, "VGA device, skipping PCI PM\n");
		return -EINVAL; // FIXME
	}

	dev_dbg(mdev->dev, "not VGA device, doing PCI PM\n");

	/* save std cfg space */
	for (i = 0; i < 16; i++)
		cfg[i] = pci_cfg_read32(&mdev->pdev, i << 2);

	pm_csr = pci_cfg_read32(&mdev->pdev, MGA_PM_CSR);
	pm_csr |= 0x3;
	pci_cfg_write32(&mdev->pdev, MGA_PM_CSR, pm_csr);

	sleep(1);

	pm_csr = pci_cfg_read32(&mdev->pdev, MGA_PM_CSR);
	pm_csr &= ~0x3;
	pci_cfg_write32(&mdev->pdev, MGA_PM_CSR, pm_csr);

	sleep(1);

	/* restore std cfg space */
	for (i = 15; i >= 0; i--)
		pci_cfg_write32(&mdev->pdev, i << 2, cfg[i]);

	return 0;
}


void mga_fill(struct mga_dev *mdev)
{
	if (1) {
		fill_crtc1(mdev);
		fill_crtc2(mdev);
		fill_bes(mdev);
		fill_cursor(mdev);
	} else {
		mga_solid_fill(mdev);
	}
}

int mga_init(struct mga_dev *mdev, bool force)
{
	int r;

	if (pci_cfg_read32(&mdev->pdev, 0x04) & 0x1 && !force) {
		dev_dbg(mdev->dev, "VGA device, skipping init\n");
		return -EINVAL; // FIXME
	}

	dev_dbg(mdev->dev, "not VGA device, doing init\n");

	switch (mdev->chip) {
	case MGA_CHIP_1064SG:
	case MGA_CHIP_1164SG:
	case MGA_CHIP_G100:
		break;
	default:
		/* not supported */
		mdev->splitmode = false;
		break;
	}

	switch (mdev->pixel_format) {
	case DRM_FORMAT_C8:
		// graphics = c8
		// video = argb1555 (not lut)
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
		// only one stream goes through lut (g or v)
		// graphics = argb1555
		// video = argb1555
		break;
	default:
		mdev->splitmode = false;
		break;
	}

	r = request_irq(mdev->irq.irq, mga_irq_handler, 0, "mga", mdev);
	if (r)
		return r;

	/* see if this prevents hang when DMA used w/o clean shutdown */
	mdev->funcs->softreset(mdev);

	mdev->funcs->init(mdev);

	mga_determine_fifo_size(mdev);

	/* 2064W doesn't do bus-mastering */
	/* FIXME implemennt a pseudo-DMA offload thread instead */
	if (mdev->chip != MGA_CHIP_2064W)
		mga_dma_init(mdev);

	mdev->fb_alloc = mga_mem_alloc_init(0, mdev->mem_size, mdev->mem_virt, 256);
	if (!mdev->fb_alloc)
		dev_warn(mdev->dev, "Failed to create FB  allocator\n");

	mga_modeset_init(mdev);

	//mga_fill(mdev);

	return 0;
}

void mga_fini(struct mga_dev *mdev)
{
	mga_modeset_fini(mdev);
	mga_mem_alloc_fini(mdev->fb_alloc);
	mga_dma_fini(mdev);
	free_irq(mdev->irq.irq, mdev);
}

int mga_suspend(struct mga_dev *mdev)
{
	return mdev->funcs->suspend(mdev);
}

int mga_resume(struct mga_dev *mdev)
{
	return mdev->funcs->resume(mdev);
}

static void mode_adjust_outputs(struct mga_dev *mdev,
				struct drm_display_mode *mode,
				unsigned int outputs,
				unsigned int tv_std)
{
	unsigned int vtotal;

	if (!(outputs & mdev->outputs & MGA_OUTPUT_TVOUT))
		return;

	vtotal = tv_std == DRM_TV_STD_PAL ? 625 : 525;

	if (mode->vtotal < vtotal) {
		mode->vsync_start += (vtotal - mode->vtotal) >> 1;
		mode->vtotal = vtotal;
	}
}

static int mga_framebuffer_init_old(const struct mga_dev *mdev,
				    struct drm_framebuffer *fb,
				    unsigned int width,
				    unsigned int height,
				    unsigned int pixel_format,
				    unsigned int offset,
				    unsigned int pitch_align);

static struct mga_framebuffer crtc1_fb;
static struct mga_framebuffer crtc2_fb;

void mga_set_initial_crtc1_mode(struct mga_dev *mdev,
				unsigned int pixel_format,
				const struct drm_display_mode *mode,
				unsigned int outputs)
{
	struct mga_mode_config *mc = &mdev->mode_config;
	struct mga_crtc_config *cc = &mc->crtc1.crtc_config;
	struct mga_plane_config *pc = &mc->crtc1.plane_config;

	if (!outputs || !pixel_format || !mode || !mode->vrefresh)
		return;

	cc->mode_valid = true;
	drm_mode_copy(&cc->user_mode, mode);
	drm_mode_copy(&cc->adjusted_mode, mode);
	cc->outputs = outputs;

	crtc1_fb.base.base.id = 1;
	crtc1_fb.bus = 0;
	INIT_LIST_HEAD(&crtc1_fb.head);

	mga_framebuffer_init_old(mdev, &crtc1_fb.base,
				 mode->hdisplay,
				 mode->vdisplay,
				 pixel_format, 0,
				 pixel_format == DRM_FORMAT_RGB888 ? 192 : 64);

	crtc1_fb.virt = mdev->mem_virt + crtc1_fb.bus;
	crtc1_fb.size = mga_framebuffer_calc_size(&crtc1_fb.base);

	pc->fb = &crtc1_fb.base;
	pc->src_w = pc->fb->width << 16;
	pc->src_h = pc->fb->height << 16;
	pc->crtc_w = pc->fb->width;
	pc->crtc_h = pc->fb->height;
}

void mga_set_initial_crtc2_mode(struct mga_dev *mdev,
				unsigned int pixel_format,
				const struct drm_display_mode *mode,
				unsigned int outputs)
{
	struct mga_mode_config *mc = &mdev->mode_config;
	struct mga_crtc_config *cc = &mc->crtc2.crtc_config;
	struct mga_plane_config *pc = &mc->crtc2.plane_config;

	if (mdev->chip < MGA_CHIP_G400)
		return;

	if (!outputs || !pixel_format || !mode || !mode->vrefresh)
		return;

	cc->mode_valid = true;
	drm_mode_copy(&cc->user_mode, mode);
	drm_mode_copy(&cc->adjusted_mode, mode);
	cc->outputs = outputs;

	crtc2_fb.base.base.id = 2;
	crtc2_fb.bus = 8 << 20;
	INIT_LIST_HEAD(&crtc2_fb.head);

	mga_framebuffer_init_old(mdev, &crtc2_fb.base,
				 mode->hdisplay,
				 mode->vdisplay,
				 pixel_format, 0,
				 64);

	crtc2_fb.virt = mdev->mem_virt + crtc2_fb.bus;
	crtc2_fb.size = mga_framebuffer_calc_size(&crtc2_fb.base);

	pc->fb = &crtc2_fb.base;
	pc->src_w = pc->fb->width << 16;
	pc->src_h = pc->fb->height << 16;
	pc->crtc_w = pc->fb->width;
	pc->crtc_h = pc->fb->height;
}

void mga_enable_outputs(struct mga_dev *mdev)
{
	struct mga_mode_config mc;
	struct mga_crtc_config *cc;
	struct mga_plane_config *pc;
	int ret;

	mc = mdev->mode_config;

	cc = &mc.crtc1.crtc_config;
	pc = &mc.crtc1.plane_config;

	if (cc->mode_valid)
		dev_dbg(mdev->dev,
			"CRTC1: mode=%p, pixel_format=%u, outputs=%x\n",
			&cc->adjusted_mode,
			pc->fb->pixel_format,
			cc->outputs);
	else
		dev_dbg(mdev->dev, "CRTC1: no mode\n");

	if (mdev->chip >= MGA_CHIP_G400) {
		cc = &mc.crtc2.crtc_config;
		pc = &mc.crtc2.plane_config;

		if (cc->mode_valid)
			dev_dbg(mdev->dev,
				"CRTC2: mode=%p, pixel_format=%u, outputs=%x\n",
				&cc->adjusted_mode,
				pc->fb->pixel_format,
				cc->outputs);
		else
			dev_dbg(mdev->dev, "CRTC2: no mode\n");
	}

	ret = mdev->funcs->set_mode(mdev, &mc);
	if (ret)
		return;

	mdev->mode_config = mc;
}


void mga_disable_outputs(struct mga_dev *mdev)
{
	struct mga_mode_config mc;

	mc = mdev->mode_config;

	mc.crtc1.crtc_config.mode_valid = false;
	mc.crtc1.crtc_config.outputs = 0;
	mc.crtc2.crtc_config.mode_valid = false;
	mc.crtc2.crtc_config.outputs = 0;

	mdev->funcs->set_mode(mdev, &mc);

	/*
	 * don't clobber current state since it's
	 * what we use to re-enable outputs later
	 */
}

int mga_load_lut(struct mga_dev *mdev,
		 unsigned int crtc, struct drm_color pal[256])
{
	switch (crtc) {
	case MGA_CRTC_CRTC1:
		if (mdev->chip <= MGA_CHIP_2164W)
			tvp_load_palette(&mdev->tdev, pal);
		else
			mga_dac_crtc1_load_palette(mdev, pal);
		return 0;
	case MGA_CRTC_CRTC2:
		return -ENOSYS;
	default:
		return -EINVAL;
	}
}


int mga_set_mode_crtc(struct mga_dev *mdev,
		      unsigned int crtc,
		      struct drm_framebuffer *fb,
		      unsigned int x, unsigned int y,
		      struct drm_display_mode *mode,
		      unsigned int outputs)
{
	struct mga_mode_config mc;
	struct mga_crtc_config *cc;
	struct mga_plane_config *pc;
	int ret;

	dev_dbg(mdev->dev, "crtc=%u, fb=%p, x=%u, y=%u, mode=%p, outputs=%x\n",
		crtc, fb, x, y, mode, outputs);

	if (mode) {
		if (!fb || !outputs)
			return -EINVAL;
	} else {
		if (fb || outputs)
			return -EINVAL;
	}

	if (mode) {
		ret = drm_framebuffer_check_viewport(fb, 0, 0,
						     mode->hdisplay << 16,
						     mode->vdisplay << 16);
		if (ret)
			return ret;
	}

	mc = mdev->mode_config;

	switch (crtc) {
	case MGA_CRTC_CRTC1:
		cc = &mc.crtc1.crtc_config;
		pc = &mc.crtc1.plane_config;

		cc->outputs = outputs;

		pc->fb = fb;
		pc->src_x = pc->src_y = 0;
		pc->src_w = pc->src_h = 0;
		pc->crtc_x = pc->crtc_y = 0;
		pc->crtc_w = pc->crtc_h = 0;
		if (mode) {
			pc->src_w = mode->hdisplay << 16;
			pc->src_h = mode->vdisplay << 16;
			pc->crtc_w = mode->hdisplay;
			pc->crtc_h = mode->vdisplay;
		}

		memset(&pc->src, 0, sizeof pc->src);
		memset(&pc->dst, 0, sizeof pc->dst);
		if (mode) {
			pc->src.x2 = mode->hdisplay << 16;
			pc->src.y2 = mode->vdisplay << 16;
			pc->dst.x2 = mode->hdisplay;
			pc->dst.y2 = mode->vdisplay;
		}
		pc->hscale = pc->vscale = 0x10000;

		if (mode) {
			drm_mode_copy(&cc->user_mode, mode);
			drm_mode_copy(&cc->adjusted_mode, mode);
			cc->mode_valid = true;
			mode_adjust_outputs(mdev, &cc->adjusted_mode, cc->outputs,
					    mc.tvo_config.tv_standard);
		} else {
			memset(&cc->user_mode, 0, sizeof cc->user_mode);
			memset(&cc->adjusted_mode, 0, sizeof cc->adjusted_mode);
			cc->mode_valid = false;
		}
		break;
	case MGA_CRTC_CRTC2:
		if (mdev->chip < MGA_CHIP_G400)
			return -ENODEV;

		cc = &mc.crtc2.crtc_config;
		pc = &mc.crtc2.plane_config;

		cc->outputs = outputs;

		pc->fb = fb;
		pc->src_x = pc->src_y = 0;
		pc->src_w = pc->src_h = 0;
		pc->crtc_x = pc->crtc_y = 0;
		pc->crtc_w = pc->crtc_h = 0;
		if (mode) {
			pc->src_w = mode->hdisplay << 16;
			pc->src_h = mode->vdisplay << 16;
			pc->crtc_w = mode->hdisplay;
			pc->crtc_h = mode->vdisplay;
		}

		memset(&pc->src, 0, sizeof pc->src);
		memset(&pc->dst, 0, sizeof pc->dst);
		if (mode) {
			pc->src.x2 = mode->hdisplay << 16;
			pc->src.y2 = mode->vdisplay << 16;
			pc->dst.x2 = mode->hdisplay;
			pc->dst.y2 = mode->vdisplay;
		}
		pc->hscale = pc->vscale = 0x10000;

		if (mode) {
			drm_mode_copy(&cc->user_mode, mode);
			drm_mode_copy(&cc->adjusted_mode, mode);
			cc->mode_valid = true;
			mode_adjust_outputs(mdev, &cc->adjusted_mode, cc->outputs,
					    mc.tvo_config.tv_standard);
		} else {
			memset(&cc->user_mode, 0, sizeof cc->user_mode);
			memset(&cc->adjusted_mode, 0, sizeof cc->adjusted_mode);
			cc->mode_valid = false;
		}
		break;
	default:
		return -EINVAL;
	}

	ret = mdev->funcs->set_mode(mdev, &mc);
	if (ret) {
		int ret2;

		/* restore previous mode */
		ret2 = mdev->funcs->set_mode(mdev, &mdev->mode_config);
		if (ret2)
			dev_warn(mdev->dev, "Failed to restore mode after error (%d:%s)\n",
				 ret2, strerror(ret2));

		return ret;
	}

	mdev->mode_config = mc;

	switch (crtc) {
	case MGA_CRTC_CRTC1:
		cc = &mc.crtc1.crtc_config;
		/* return the adjusted mode */
		if (mode)
			drm_mode_copy(mode, &cc->adjusted_mode);
		break;
	case MGA_CRTC_CRTC2:
		cc = &mc.crtc2.crtc_config;
		/* return the adjusted mode */
		if (mode)
			drm_mode_copy(mode, &cc->adjusted_mode);
		break;
	default:
		BUG();
	}

	return 0;
}

void mga_framebuffer_pre_destroy(struct drm_device *dev, struct drm_framebuffer *fb)
{
	struct mga_dev *mdev = to_mga_device(dev);
	struct mga_mode_config mc;
	struct mga_crtc_config *cc;
	struct mga_plane_config *pc;
	int ret;

	mc = mdev->mode_config;

	cc = &mc.crtc1.crtc_config;
	pc = &mc.crtc1.plane_config;

	if (pc->fb && drm_framebuffer_equal(fb, pc->fb)) {
		memset(&cc->user_mode, 0, sizeof cc->user_mode);
		memset(&cc->adjusted_mode, 0, sizeof cc->adjusted_mode);
		cc->mode_valid = false;
		cc->outputs = 0;
		pc->fb = NULL;
	}

	cc = &mc.crtc2.crtc_config;
	pc = &mc.crtc2.plane_config;

	if (mdev->chip >= MGA_CHIP_G400 &&
	    pc->fb && drm_framebuffer_equal(fb, pc->fb)) {
		memset(&cc->user_mode, 0, sizeof cc->user_mode);
		memset(&cc->adjusted_mode, 0, sizeof cc->adjusted_mode);
		cc->mode_valid = false;
		cc->outputs = 0;
		pc->fb = NULL;
	}

	ret = mdev->funcs->set_mode(mdev, &mc);
	if (ret)
		dev_warn(mdev->dev, "Failed to set mode prior to fb destuction (%d:%s)\n",
			 ret, strerror(ret));

	mdev->mode_config = mc;
}

static void mga_normal_tests(struct mga_dev *mdev)
{
	mga_fill(mdev);

	mdev->funcs->test(mdev);
}

static void mga_ddc_tests(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");
	struct timeval start;
	struct timeval end;

	if (strstr(test, "ddc1")) {
		gettimeofday(&start, NULL);
		get_edid(mdev, DDC_ADDR, &mdev->ddc1, "Connector #1");
		gettimeofday(&end, NULL);
		print_diff(mdev->dev, &start, &end);
	}

	if (strstr(test, "ddc2") &&
	    mdev->chip >= MGA_CHIP_G450 &&
	    mga_g400_detect_tvout_adapter(mdev)) {
		gettimeofday(&start, NULL);
		get_edid(mdev, DDC_ADDR, &mdev->ddc2, "Connector #2");
		gettimeofday(&end, NULL);
		print_diff(mdev->dev, &start, &end);
	}

	if (strstr(test, "ddcd") &&
	    mdev->features & MGA_FEAT_TMDS_ADDON) {
		gettimeofday(&start, NULL);
		get_edid(mdev, DDC_ADDR, &mdev->ddcd, "Digital connector");
		gettimeofday(&end, NULL);
		print_diff(mdev->dev, &start, &end);
	}
}

static void mga_misc_tests(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");

	switch (mdev->chip) {
	case MGA_CHIP_2064W:
		break;
	case MGA_CHIP_2164W:
		break;
	case MGA_CHIP_1064SG:
	case MGA_CHIP_1164SG:
		break;
	case MGA_CHIP_G100:
		if (strstr(test, "misc_i2c"))
			try_misc_i2c(mdev);
		break;
	case MGA_CHIP_G200:
	case MGA_CHIP_G200SE:
	case MGA_CHIP_G200EV:
	case MGA_CHIP_G200WB:
		if (strstr(test, "misc_i2c"))
			try_misc_i2c(mdev);
		break;
	case MGA_CHIP_G400:
		if (strstr(test, "misc_i2c"))
			try_misc_i2c(mdev);
		break;
	case MGA_CHIP_G450:
	case MGA_CHIP_G550:
		if (strstr(test, "misc_i2c"))
			try_misc_i2c(mdev);
		break;
	default:
		break;
	}
}

void mga_tests(struct mga_dev *mdev)
{
	mga_normal_tests(mdev);
	mga_ddc_tests(mdev);
	mga_misc_tests(mdev);
}

void mga_prepare(struct mga_dev *mdev)
{
	switch (mdev->chip) {
	case MGA_CHIP_2064W:
		mga_2064w_prepare(mdev);
		break;
	case MGA_CHIP_2164W:
		mga_2164w_prepare(mdev);
		break;
	case MGA_CHIP_1064SG:
	case MGA_CHIP_1164SG:
		mga_1064sg_prepare(mdev);
		break;
	case MGA_CHIP_G100:
		mga_g100_prepare(mdev);
		break;
	case MGA_CHIP_G200:
	case MGA_CHIP_G200SE:
	case MGA_CHIP_G200EV:
	case MGA_CHIP_G200WB:
		mga_g200_prepare(mdev);
		break;
	case MGA_CHIP_G400:
		mga_g400_prepare(mdev);
		break;
	case MGA_CHIP_G450:
	case MGA_CHIP_G550:
		mga_g450_prepare(mdev);
		break;
	}
}

int mga_pll_calc(struct mga_dev *mdev,
		 unsigned int freq,
		 struct mga_pll_settings *pll)
{
	switch (pll->type) {
	case MGA_PLL_NONE:
		return 0;
	case MGA_PLL_CRISTAL:
		/* FIXME */
		pll->fcristal = 27000;
		return 0;
	case MGA_PLL_PIXPLL:
		switch (mdev->chip) {
		case MGA_CHIP_2064W:
		case MGA_CHIP_2164W:
			/* FIXME TVP PIXCLK+LCLK */
			return -ENOSYS;
		case MGA_CHIP_G450:
		case MGA_CHIP_G550:
			/* FIXME */
			return -ENOSYS;;
		default:
			return mga_dac_pixpllc_calc(mdev, freq, 0, &pll->dac);
		}
		break;
	case MGA_PLL_SYSPLL:
		switch (mdev->chip) {
		case MGA_CHIP_2064W:
		case MGA_CHIP_2164W:
			/* FIXME TVP MCLK */
			return 0;
		case MGA_CHIP_G450:
		case MGA_CHIP_G550:
			/* FIXME */
			return -ENOSYS;;
		default:
			return mga_dac_syspll_calc(mdev, freq, 0, &pll->dac);
		}
		break;
	case MGA_PLL_VIDPLL:
		switch (mdev->chip) {
		case MGA_CHIP_G450:
		case MGA_CHIP_G550:
			/* FIXME */
			return -ENOSYS;;
		default:
			return -ENODEV;
		}
		break;
	case MGA_PLL_TVO:
		return mga_tvo_pll_calc(&mdev->tvodev, freq, 0, &pll->dac);
	case MGA_PLL_AV9110:
		return mga_av9110_pll_calc(mdev, freq, pll);
	default:
		return -EINVAL;
	}
}

int mga_pll_program(struct mga_dev *mdev,
		    const struct mga_pll_settings *pll)
{
	switch (pll->type) {
		int ret;

	case MGA_PLL_PIXPLL:
		switch (mdev->chip) {
		case MGA_CHIP_2064W:
		case MGA_CHIP_2164W:
			/* FIXME TVP PCLK+LCLK */
			return -ENOSYS;
		case MGA_CHIP_G450:
		case MGA_CHIP_G550:
			/* FIXME */
			return -ENOSYS;
		default:
			mga_dac_pixpll_power(mdev, true);
			ret = mga_dac_pixpllc_program(mdev, &pll->dac);
			if (ret)
				mga_dac_pixpll_power(mdev, false);
			return ret;
		}
		break;
	case MGA_PLL_SYSPLL:
		switch (mdev->chip) {
		case MGA_CHIP_2064W:
		case MGA_CHIP_2164W:
			/* FIXME TVP MCLK */
			return -ENOSYS;
		case MGA_CHIP_G450:
		case MGA_CHIP_G550:
			/* FIXME */
			return -ENOSYS;
		default:
			mga_dac_syspll_power(mdev, true);
			ret = mga_dac_syspll_program(mdev, &pll->dac);
			if (ret)
				mga_dac_syspll_power(mdev, false);
			return ret;
		}
		break;
	case MGA_PLL_VIDPLL:
		switch (mdev->chip) {
		case MGA_CHIP_G450:
		case MGA_CHIP_G550:
			/* FIXME */
			return -ENOSYS;
		default:
			return -ENODEV;
		}
		break;
	case MGA_PLL_CRISTAL:
		return 0;
	case MGA_PLL_TVO:
		/* FIXME refactor */
		return 0;
	case MGA_PLL_AV9110:
		mga_av9110_pll_power(mdev, true);
		ret = mga_av9110_pll_program(mdev, pll);
		if (ret)
			mga_av9110_pll_power(mdev, false);
		return ret;
	default:
		return -ENODEV;
	}
}

static int mga_framebuffer_init_old(const struct mga_dev *mdev,
				    struct drm_framebuffer *fb,
				    unsigned int width,
				    unsigned int height,
				    unsigned int pixel_format,
				    unsigned int offset,
				    unsigned int pitch_align)
{
	unsigned int cpp;

	switch (pixel_format) {
	case DRM_FORMAT_C8:
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_RGB888:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		/*
		 * CRTC1 supports all these,
		 * G400+ BES and CRTC2 support all except RGB888
		 */
		break;
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		/* G400+ BES and CRTC2 supports these */
		if (mdev->chip < MGA_CHIP_G400)
			return -EINVAL;
		break;
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
		/* G200+ BES supports these */
		if (mdev->chip < MGA_CHIP_G200)
			return -EINVAL;
		break;
	default:
		return -EINVAL;
	}

	cpp = drm_format_plane_cpp(pixel_format, 0);

	memset(fb, 0, sizeof *fb);

	fb->pixel_format = pixel_format;

	fb->width = width;
	fb->height = height;

	if (mdev->chip <= MGA_CHIP_2164W && mdev->interleave)
		pitch_align <<= 1;

	fb->offsets[0] = offset;
	fb->pitches[0] = roundup(fb->width * cpp, pitch_align);

	if (mdev->chip <= MGA_CHIP_2164W)
		mga_crtc1_align_address(mdev, to_mga_framebuffer(fb));

	switch (pixel_format) {
	case DRM_FORMAT_YUV420:
		fb->pitches[1] = fb->pitches[2] = fb->pitches[0] / 2;

		fb->offsets[1] = fb->offsets[0] +
			fb->pitches[0] * fb->height;
		fb->offsets[2] = fb->offsets[1] +
			fb->pitches[1] * fb->height / 2;
		break;
	case DRM_FORMAT_YVU420:
		fb->pitches[2] = fb->pitches[1] = fb->pitches[0] / 2;

		fb->offsets[2] = fb->offsets[0] +
			fb->pitches[0] * fb->height;
		fb->offsets[1] = fb->offsets[2] +
			fb->pitches[2] * fb->height / 2;
		break;
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
		fb->pitches[1] = fb->pitches[0];

		fb->offsets[1] = fb->offsets[0] +
			fb->pitches[0] * fb->height;
		break;
	default:
		break;
	}

	return 0;
}

#ifdef USE_SSE
#include <xmmintrin.h>
#include <emmintrin.h>

static void memcpy_func(void *dst, const void *src, size_t len)
{
	__m128i *dp = dst;
	const __m128i *sp = src;

	len /= 128;

	while (len--) {
		const __m128i x0 = _mm_load_si128(sp++);
		const __m128i x1 = _mm_load_si128(sp++);
		const __m128i x2 = _mm_load_si128(sp++);
		const __m128i x3 = _mm_load_si128(sp++);
		const __m128i x4 = _mm_load_si128(sp++);
		const __m128i x5 = _mm_load_si128(sp++);
		const __m128i x6 = _mm_load_si128(sp++);
		const __m128i x7 = _mm_load_si128(sp++);
		_mm_stream_si128(dp++, x0);
		_mm_stream_si128(dp++, x1);
		_mm_stream_si128(dp++, x2);
		_mm_stream_si128(dp++, x3);
		_mm_stream_si128(dp++, x4);
		_mm_stream_si128(dp++, x5);
		_mm_stream_si128(dp++, x6);
		_mm_stream_si128(dp++, x7);
	}
}
#else
#define memcpy_func(d, s, l) memcpy(d,s,l)
#endif

static void memfill(u8 *data, u8 value,
		    size_t len, size_t patlen)
{
	size_t i, j;

	for (i = 0; i < len; ) {
		for (j = 0; j < patlen && i < len; j++, i++)
			*data++ = value;
		value++;
	}
}

static bool memcheck(const u8 *data, u8 value,
		     size_t len, size_t patlen)
{
	size_t i, j;

	for (i = 0; i < len; ) {
		for (j = 0; j < patlen && i < len; j++, i++) {
			if (*data != value) {
				pr_err("Memcheck failed at byte %zu (0x%02x)\n", i, *data);
				return false;
			}
			data++;
		}
		value++;
	}
	return true;
}

static void mem_test_read_fb(struct mga_dev *mdev,
			     void *data,
			     u32 *read_bytes,
			     u32 *read_usec)
{
	struct timespec pre, post, t;

	memfill(data, 0, mdev->mem_size, 31);
	memfill(mdev->mem_virt, 32, mdev->mem_size, 27);

	clock_gettime(CLOCK_MONOTONIC, &pre);
	memcpy_func(data, mdev->mem_virt, mdev->mem_size);
	clock_gettime(CLOCK_MONOTONIC, &post);
	timespecsub(&post, &pre, &t);

	*read_bytes = mdev->mem_size;
	*read_usec = t.tv_sec * 1000000 + t.tv_nsec / 1000;

	if (!memcheck(data, 32, mdev->mem_size, 27))
		dev_err(mdev->dev, "data check failed after IDUMP\n");
	else
		dev_dbg(mdev->dev, "data check OK after IDUMP\n");
	if (!memcheck(mdev->mem_virt, 32, mdev->mem_size, 27))
		dev_err(mdev->dev, "mem check failed after IDUMP\n");
	else
		dev_dbg(mdev->dev, "mem check OK after IDUMP\n");
}

static void mem_test_write_fb(struct mga_dev *mdev,
			      void *data,
			      u32 *write_bytes,
			      u32 *write_usec)
{
	struct timespec pre, post, t;

	memfill(data, 13, mdev->mem_size, 23);
	memfill(mdev->mem_virt, 101, mdev->mem_size, 41);

	clock_gettime(CLOCK_MONOTONIC, &pre);
	memcpy_func(mdev->mem_virt, data, mdev->mem_size);
	clock_gettime(CLOCK_MONOTONIC, &post);
	timespecsub(&post, &pre, &t);

	*write_bytes = mdev->mem_size;
	*write_usec = t.tv_sec * 1000000 + t.tv_nsec / 1000;

	if (!memcheck(data, 13, mdev->mem_size, 23))
		dev_err(mdev->dev, "data check failed after ILOAD\n");
	else
		dev_dbg(mdev->dev, "data check OK after ILOAD\n");
	if (!memcheck(mdev->mem_virt, 13, mdev->mem_size, 23))
		dev_err(mdev->dev, "mem check failed after ILOAD\n");
	else
		dev_dbg(mdev->dev, "mem check OK after ILOAD\n");
}

enum {
	ILOAD_H = 512,
};

static void prepare_iload(struct mga_dev *mdev)
{
	mga_write32(mdev, MGA_OPMODE,
		   MGA_OPMODE_DMAMOD_BLIT |
		   MGA_OPMODE_DMADATASIZ_8 |
		   MGA_OPMODE_DIRDATASIZ_8);

	mga_wait_fifo(mdev, 7);

	mga_write32(mdev, MGA_PITCH, 1024);
	mga_write32(mdev, MGA_YDSTORG, 0);
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_PWIDTH_PW32 | mdev->maccess);

	mga_write32(mdev, MGA_CXBNDRY, (0xFFF << 16) | 0);
	mga_write32(mdev, MGA_YTOP, 0);
	mga_write32(mdev, MGA_YBOT, 0x7FFFFF);

	mga_write32(mdev, MGA_FCOL, 0x40404040);
	mga_write32(mdev, MGA_BCOL, 0x04040404);

	mga_write32(mdev, MGA_DWGCTL,
		   MGA_DWGCTL_BLTMOD_BFCOL |
		   MGA_DWGCTL_BOP_COPY |
		   MGA_DWGCTL_SHFTZERO |
		   MGA_DWGCTL_SGNZERO |
		   MGA_DWGCTL_ATYPE_RPL |
		   MGA_DWGCTL_OPCOD_ILOAD);
}

static void prepare_idump(struct mga_dev *mdev)
{
	mga_write32(mdev, MGA_OPMODE,
		   MGA_OPMODE_DMAMOD_BLIT |
		   MGA_OPMODE_DMADATASIZ_8 |
		   MGA_OPMODE_DIRDATASIZ_8);

	mga_wait_fifo(mdev, 7);

	mga_write32(mdev, MGA_PITCH, 1024);
	mga_write32(mdev, MGA_YDSTORG, 0);
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_PWIDTH_PW32 | mdev->maccess);
	mga_write32(mdev, MGA_CXBNDRY, (0xFFF << 16) | 0);
	mga_write32(mdev, MGA_YTOP, 0);
	mga_write32(mdev, MGA_YBOT, 0x7FFFFF);

	mga_write32(mdev, MGA_FCOL, 0x30303030);
	mga_write32(mdev, MGA_BCOL, 0x05050505);

	mga_write32(mdev, MGA_DWGCTL,
		   MGA_DWGCTL_BLTMOD_BU32RGB |
		   MGA_DWGCTL_BOP_COPY |
		   MGA_DWGCTL_SHFTZERO |
		   MGA_DWGCTL_SGNZERO |
		   MGA_DWGCTL_ATYPE_RPL |
		   MGA_DWGCTL_OPCOD_IDUMP);
}

static void end_iload_idump(struct mga_dev *mdev)
{
	mga_wait_fifo(mdev, 1);

	mga_write32(mdev, MGA_DWGCTL,
		   MGA_DWGCTL_BLTMOD_BFCOL |
		   MGA_DWGCTL_BOP_COPY |
		   MGA_DWGCTL_SHFTZERO |
		   MGA_DWGCTL_SGNZERO |
		   MGA_DWGCTL_ARZERO |
		   MGA_DWGCTL_ATYPE_RPL |
		   MGA_DWGCTL_OPCOD_TRAP);

	mga_write32(mdev, MGA_OPMODE,
	       MGA_OPMODE_DMAMOD_GENERAL_PURPOSE |
	       MGA_OPMODE_DMADATASIZ_8 |
	       MGA_OPMODE_DIRDATASIZ_8);
}

static void memcpy_idump(struct mga_dev *mdev,
			 void *dst, size_t len)
{
	size_t block = 4 * 1024 * ILOAD_H;
	unsigned int y = 0;

	len /= block;

	//printk("IDUMP [");

	while (len--) {
		//printk(".");
		mga_write32(mdev, MGA_AR0, y + 1024 - 1);
		mga_write32(mdev, MGA_AR3, y);
		mga_write32(mdev, MGA_AR5, 1024);
		mga_write32(mdev, MGA_FXBNDRY, ((1024 - 1) << 16) | 0);
		mga_write32(mdev, MGA_YDSTLEN + MGA_DWGENG_GO,
			   (0 << 16) | ILOAD_H);
		memcpy_func(dst, mdev->iload_virt, block);
		dst += block;
		y += ILOAD_H * 1024;
		//mga_wait_dwgeng_idle(mdev);
	}

	//printk("]\n");
}

static void memcpy_idump_dmawin(struct mga_dev *mdev,
				void *dst, size_t len)
{
	//	size_t block = 4 * 1024 * 128;
	size_t block = 4 * 1024 * ILOAD_H;
	unsigned int y = 0;

	//printk("IDUMP [");

	while (len) {
		size_t left = block;

		//printk(".");
		mga_write32(mdev, MGA_AR0, y + 1024 - 1);
		mga_write32(mdev, MGA_AR3, y);
		mga_write32(mdev, MGA_AR5, 1024);
		mga_write32(mdev, MGA_FXBNDRY, ((1024 - 1) << 16) | 0);
		mga_write32(mdev, MGA_YDSTLEN + MGA_DWGENG_GO,
			   (0 << 16) | ILOAD_H);

		/* memcpy in 7k blocks... */
		while (left) {
			size_t l = min(left, 7 << 10);
			memcpy_func(dst, mdev->mmio_virt, l);
			dst += l;
			left -= l;
		}

		y += ILOAD_H * 1024;
		len -= block;
		//mga_wait_dwgeng_idle(mdev);
	}

	//printk("]\n");
}

static void memcpy_iload(struct mga_dev *mdev,
			 const void *src, size_t len)
{
	size_t block = 4 * 1024 * ILOAD_H;
	unsigned int y = 0;

	len /= block;

	//printk("ILOAD [");

	while (len--) {
		//printk(".");
		mga_write32(mdev, MGA_AR0, 1024 - 1);
		mga_write32(mdev, MGA_AR3, 0);
		mga_write32(mdev, MGA_AR5, 0);
		mga_write32(mdev, MGA_FXBNDRY, ((1024 - 1) << 16) | 0);
		mga_write32(mdev, MGA_YDSTLEN + MGA_DWGENG_GO,
			   (y << 16) | ILOAD_H);
		memcpy_func(mdev->iload_virt, src, block);
		src += block;
		y += ILOAD_H;
		//mga_wait_dwgeng_idle(mdev);
	}

	//printk("]\n");
}

static void memcpy_iload_dmawin(struct mga_dev *mdev,
				const void *src, size_t len)
{
	size_t block = 4 * 1024 * ILOAD_H;
	unsigned int y = 0;

	//printk("ILOAD [");

	while (len) {
		size_t left = block;

		//printk(".");
		mga_write32(mdev, MGA_AR0, 1024 - 1);
		mga_write32(mdev, MGA_AR3, 0);
		mga_write32(mdev, MGA_AR5, 0);
		mga_write32(mdev, MGA_FXBNDRY, ((1024 - 1) << 16) | 0);
		mga_write32(mdev, MGA_YDSTLEN + MGA_DWGENG_GO,
			   (y << 16) | ILOAD_H);

		while (left) {
			size_t l = min(left, 7 << 10);
			memcpy_func(mdev->mmio_virt, src, l);
			src += l;
			left -= l;
		}

		y += ILOAD_H;
		len -= block;
		//mga_wait_dwgeng_idle(mdev);
	}

	//printk("]\n");
}

static void dma_iload(struct mga_dev *mdev, size_t len)
{
	unsigned int y = 0;
	unsigned int h;
	unsigned int secaddr = mdev->dma_res.off + mdev->dma_res.len - len;
	unsigned int off = 0;

	if (len > mdev->dma_res.len - 4096)
		return;

	if (len % (1024 * 4))
		return;

	h = len / (1024 * 4);

	off = mga_dma(mdev, off,
		      MGA_PITCH, 1024,
		      MGA_YDSTORG, 0,
		      MGA_MACCESS, MGA_MACCESS_PWIDTH_PW32 | mdev->maccess,
		      MGA_CXBNDRY, (0xFFF << 16) | 0);

	off = mga_dma(mdev, off,
		      MGA_YTOP, 0,
		      MGA_YBOT, 0x7FFFFF,
		      MGA_DWGCTL, (MGA_DWGCTL_BLTMOD_BFCOL |
				   MGA_DWGCTL_BOP_COPY |
				   MGA_DWGCTL_SHFTZERO |
				   MGA_DWGCTL_SGNZERO |
				   MGA_DWGCTL_ATYPE_RPL |
				   MGA_DWGCTL_OPCOD_ILOAD),
		      MGA_DMAPAD, 0);

	off = mga_dma(mdev, off,
		      MGA_DMAPAD, 0,
		      MGA_AR0, 1024 - 1,
		      MGA_AR3, 0,
		      MGA_AR5, 0);
	off = mga_dma(mdev, off,
		      MGA_FXBNDRY, ((1024 - 1) << 16) | 0,
		      MGA_YDSTLEN + MGA_DWGENG_GO, (y << 16) | h,
		      MGA_SECADDRESS,
		      secaddr | MGA_SECADDRESS_SECMOD_BLIT,
		      MGA_SECEND, secaddr + len);

	mga_dma(mdev, off,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0);

	mga_write32(mdev,
		   MGA_PRIMADDRESS,
		   mdev->dma_res.off | MGA_PRIMADDRESS_PRIMOD_GENERAL_PURPOSE);
	mga_write32(mdev,
		   MGA_PRIMEND,
		   mdev->dma_res.off + off);

	mga_wait_dma_idle(mdev);
	mga_wait_dwgeng_idle(mdev);
}

static void mem_test_read_idump(struct mga_dev *mdev,
				void *data,
				u32 *read_bytes,
				u32 *read_usec)
{
	struct timespec pre, post, t;

	memfill(data, 0, mdev->mem_size, 31);
	memfill(mdev->mem_virt, 32, mdev->mem_size, 27);

	prepare_idump(mdev);

	clock_gettime(CLOCK_MONOTONIC, &pre);
	if (mdev->iload_virt)
		memcpy_idump(mdev, data, mdev->mem_size);
	else
		memcpy_idump_dmawin(mdev, data, mdev->mem_size);
	clock_gettime(CLOCK_MONOTONIC, &post);
	timespecsub(&post, &pre, &t);

	end_iload_idump(mdev);

	*read_bytes = mdev->mem_size;
	*read_usec = t.tv_sec * 1000000 + t.tv_nsec / 1000;

	if (!memcheck(data, 32, mdev->mem_size, 27))
		dev_err(mdev->dev, "data check failed after IDUMP\n");
	else
		dev_dbg(mdev->dev, "data check OK after IDUMP\n");
	if (!memcheck(mdev->mem_virt, 32, mdev->mem_size, 27))
		dev_err(mdev->dev, "mem check failed after IDUMP\n");
	else
		dev_dbg(mdev->dev, "mem check OK after IDUMP\n");
}

static void mem_test_write_iload(struct mga_dev *mdev,
				 void *data,
				 u32 *write_bytes,
				 u32 *write_usec)
{
	struct timespec pre, post, t;

	memfill(data, 13, mdev->mem_size, 23);
	memfill(mdev->mem_virt, 101, mdev->mem_size, 41);

	prepare_iload(mdev);

	clock_gettime(CLOCK_MONOTONIC, &pre);
	if (mdev->iload_virt)
		memcpy_iload(mdev, data, mdev->mem_size);
	else
		memcpy_iload_dmawin(mdev, data, mdev->mem_size);
	clock_gettime(CLOCK_MONOTONIC, &post);
	timespecsub(&post, &pre, &t);

	end_iload_idump(mdev);

	*write_bytes = mdev->mem_size;
	*write_usec = t.tv_sec * 1000000 + t.tv_nsec / 1000;

	if (!memcheck(data, 13, mdev->mem_size, 23))
		dev_err(mdev->dev, "data check failed after ILOAD\n");
	else
		dev_dbg(mdev->dev, "data check OK after ILOAD\n");
	if (!memcheck(mdev->mem_virt, 13, mdev->mem_size, 23))
		dev_err(mdev->dev, "mem check failed after ILOAD\n");
	else
		dev_dbg(mdev->dev, "mem check OK after ILOAD\n");
}

static void mem_test_dma_iload(struct mga_dev *mdev,
			       void *data,
			       u32 *write_bytes,
			       u32 *write_usec)
{
	struct timespec pre, post, t;
	size_t len = mdev->dma_res.len - 4096;

	dma_prim_ptr = mdev->dma_virt;
	dma_sec_ptr = mdev->dma_virt + mdev->dma_res.len - len;

	memfill((void*)dma_sec_ptr, 13, len, 23);
	memfill(mdev->mem_virt, 101, len, 41);

	//prepare_dma_iload(mdev);

	clock_gettime(CLOCK_MONOTONIC, &pre);
	dma_iload(mdev, len);
	clock_gettime(CLOCK_MONOTONIC, &post);
	timespecsub(&post, &pre, &t);

	//end_dma_iload_idump(mdev);

	*write_bytes = len;
	*write_usec = t.tv_sec * 1000000 + t.tv_nsec / 1000;

	if (!memcheck((void*)dma_sec_ptr, 13, len, 23))
		dev_err(mdev->dev, "data check failed after ILOAD\n");
	else
		dev_dbg(mdev->dev, "data check OK after ILOAD\n");
	if (!memcheck(mdev->mem_virt, 13, len, 23))
		dev_err(mdev->dev, "mem check failed after ILOAD\n");
	else
		dev_dbg(mdev->dev, "mem check OK after ILOAD\n");

	dma_prim_ptr = NULL;
	dma_sec_ptr = NULL;
}

static int mga_iload_test(struct mga_dev *mdev,
			  unsigned int bpp,
			  int x, int y, int w, int h)
{
	unsigned int total = w * h;
	u32 pwidth = get_pwidth(mdev->pixel_format);
	u32 dwgctl = (MGA_DWGCTL_BOP_COPY |
		      MGA_DWGCTL_SHFTZERO |
		      MGA_DWGCTL_SGNZERO |
		      MGA_DWGCTL_ATYPE_RPL |
		      MGA_DWGCTL_OPCOD_ILOAD);

	if (bpp == 1)
		dwgctl |= MGA_DWGCTL_BLTMOD_BMONOLEF;
	else
		dwgctl |= MGA_DWGCTL_BLTMOD_BFCOL;

	mga_wait_fifo(mdev, 15);

	mga_dma(mdev, 0,
		MGA_MACCESS, pwidth | mdev->maccess,
		MGA_PITCH, 1024,
		MGA_YDSTORG, 0,
		MGA_CXBNDRY, (0xfff << 16) | 0);

	mga_dma(mdev, 0,
		MGA_YTOP, 0,
		MGA_YBOT, 0x7fffff,
		MGA_FCOL, 0x07e007e0,
		MGA_BCOL, 0x001f001f);

	mga_dma(mdev, 0,
		MGA_DWGCTL, dwgctl,
		MGA_AR0, w - 1,
		MGA_AR3, 0,
		MGA_AR5, 0);

	mga_dma(mdev, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_FXBNDRY, ((x + w - 1) << 16) | (x & 0xffff),
		MGA_YDSTLEN + MGA_DWGENG_GO, (y << 16) | (h & 0xffff));

	total = DIV_ROUND_UP(total * bpp, 32);

	mga_write32(mdev, MGA_OPMODE,
		   MGA_OPMODE_DMAMOD_BLIT |
		   MGA_OPMODE_DMADATASIZ_8 |
		   MGA_OPMODE_DIRDATASIZ_8);

	printk("ILOAD [");

	while (total) {
		unsigned int len = min(total, mdev->fifo_size/2);
		volatile u32 *ptr = mdev->iload_virt;

		total -= len;

		mga_wait_fifo(mdev, len);

		while (len--)
			*ptr++ = 0xffff0000;

		printk(".");
	}

	printk("]\n");

	mga_wait_dwgeng_idle(mdev);

	mga_write32(mdev, MGA_OPMODE,
		   MGA_OPMODE_DMAMOD_GENERAL_PURPOSE |
		   MGA_OPMODE_DMADATASIZ_8 |
		   MGA_OPMODE_DIRDATASIZ_8);

	mga_wait_fifo(mdev, 4);

	mga_dma(mdev, 0,
		MGA_DWGCTL, (MGA_DWGCTL_BLTMOD_BFCOL |
			     MGA_DWGCTL_BOP_COPY |
			     MGA_DWGCTL_SHFTZERO |
			     MGA_DWGCTL_SGNZERO |
			     MGA_DWGCTL_ARZERO |
			     MGA_DWGCTL_ATYPE_RPL |
			     MGA_DWGCTL_OPCOD_TRAP),
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0);

	return 0;
}

static void dump_prim(struct mga_dev *mdev, u32 primend)
{
	u32 primaddr = mga_read32(mdev, MGA_PRIMADDRESS) & ~0x3;
	u32 *ptr =
		mdev->dma_virt +
		(primaddr - mdev->dma_res.off);
	u32 len = primend - primaddr;

	if (len & 3) {
		dev_err(mdev->dev, "Invalid prim dma len\n");
		return;
	}

	dev_dbg(mdev->dev, "PRIM:\n");
	while (len) {
		dev_dbg(mdev->dev, " 0x%08x\n", *ptr++);
		len -= 4;
	}
}

static int mga_dma_test(struct mga_dev *mdev,
			unsigned int bpp,
			int x, int y, int w, int h)
{
	unsigned int total = w * h;
	u32 pwidth = get_pwidth(mdev->pixel_format);
	u32 dwgctl = (MGA_DWGCTL_BOP_COPY |
		      MGA_DWGCTL_SHFTZERO |
		      MGA_DWGCTL_SGNZERO |
		      MGA_DWGCTL_ATYPE_RPL |
		      MGA_DWGCTL_OPCOD_ILOAD);
	unsigned int off = 0;
	unsigned int softrap = 0;

	u32 primaddr = mdev->dma_res.off;
	u32 secaddr = mdev->dma_res.off + mdev->dma_res.len / 2;

	dma_prim_ptr = NULL;
	dma_sec_ptr = NULL;
	dma_end_ptr = NULL;

	if (!mdev->dma_res.index)
		return -ENXIO;

	dma_prim_ptr = mdev->dma_virt;
	dma_sec_ptr = mdev->dma_virt + mdev->dma_res.len / 2;
	dma_end_ptr = mdev->dma_virt + mdev->dma_res.len;

	dev_dbg(mdev->dev, "PRIM: %x - %x (%u bytes)\n",
		dma_prim_ptr, dma_sec_ptr,
		(unsigned long)dma_sec_ptr - (unsigned long)dma_prim_ptr);
	dev_dbg(mdev->dev, "SEC: %x - %x (%u bytes)\n",
		dma_sec_ptr, dma_end_ptr,
		(unsigned long)dma_end_ptr - (unsigned long)dma_sec_ptr);

	if (bpp == 1)
		dwgctl |= MGA_DWGCTL_BLTMOD_BMONOLEF;
	else
		dwgctl |= MGA_DWGCTL_BLTMOD_BFCOL;

	off = mga_dma(mdev, off,
		      MGA_MACCESS, pwidth | mdev->maccess,
		      MGA_PITCH, 1024,
		      MGA_YDSTORG, 0,
		      MGA_CXBNDRY, (0xfff << 16) | 0);

	off = mga_dma(mdev, off,
		      MGA_YTOP, 0,
		      MGA_YBOT, 0x7fffff,
		      MGA_FCOL, 0x07e007e0,
		      MGA_BCOL, 0x001f001f);

	off = mga_dma(mdev, off,
		      MGA_DWGCTL, dwgctl,
		      MGA_AR0, w - 1,
		      MGA_AR3, 0,
		      MGA_AR5, 0);

	off = mga_dma(mdev, off,
		      MGA_DMAPAD, 0,
		      MGA_DMAPAD, 0,
		      MGA_FXBNDRY, ((x + w - 1) << 16) | (x & 0xffff),
		      MGA_YDSTLEN + MGA_DWGENG_GO, (y << 16) | (h & 0xffff));

	total = DIV_ROUND_UP(total * bpp, 32);

	dev_dbg(mdev->dev, "%u bytes\n", total * 4);

	if (dma_sec_ptr + total * 4 < dma_end_ptr) {
#if 1
		off = mga_dma(mdev, off,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0,
			      MGA_SECADDRESS,
			      secaddr | MGA_SECADDRESS_SECMOD_BLIT,
			      MGA_SECEND, secaddr + total * 4);
		off = mga_dma(mdev, off,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0,
			      MGA_SOFTRAP, softrap++ << 2);
#else
		off = mga_dma(mdev, off,
			      MGA_SECADDRESS,
			      secaddr | MGA_SECADDRESS_SECMOD_BLIT,
			      MGA_SECEND, secaddr + total * 4,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0);
#endif
		dev_dbg(mdev->dev, "adding sec DMA at %x - %x (%u bytes)\n",
			secaddr, secaddr + total * 4, total * 4);

		printk("ILOAD [");

		while (total--) {
			*dma_sec_ptr++ = 0xffff0000;
			printk(".");
		}

		printk("]\n");
	}

#if 0
	total = 0;
	while (total++ < 500)
		off = mga_dma(mdev, off,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0);
#endif

	mga_dma(mdev, off,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0);

	//wbinvd(mdev);

	mga_write32(mdev, MGA_PRIMADDRESS,
		   primaddr | MGA_PRIMADDRESS_PRIMOD_GENERAL_PURPOSE);

	dump_prim(mdev, primaddr + off);

	dev_dbg(mdev->dev, "starting prim DMA at %x (%u bytes)\n",
		mga_read32(mdev, MGA_PRIMADDRESS), off);
	mga_write32(mdev, MGA_PRIMEND, primaddr + off);
	dev_dbg(mdev->dev, "started prim DMA (addr=%x)\n",
		mga_read32(mdev, MGA_PRIMADDRESS));

	mga_wait_dma_idle(mdev);
	mga_wait_dwgeng_idle(mdev);

	dev_dbg(mdev->dev, "prim DMA finished (addr=%x)\n",
		mga_read32(mdev, MGA_PRIMADDRESS));
	dev_dbg(mdev->dev, "sec DMA (addr=%x)\n",
		mga_read32(mdev, MGA_SECADDRESS));

	dma_prim_ptr = NULL;
	dma_sec_ptr = NULL;
	dma_end_ptr = NULL;

	return 0;
}

static int mga_dma_fill_test(struct mga_dev *mdev,
			     unsigned int bpp,
			     int x, int y, int w, int h)
{
	unsigned int total = w * h;
	u32 pwidth = get_pwidth(mdev->pixel_format);
	u32 dwgctl = (MGA_DWGCTL_BOP_COPY |
		      MGA_DWGCTL_ARZERO |
		      MGA_DWGCTL_SHFTZERO |
		      MGA_DWGCTL_SGNZERO |
		      MGA_DWGCTL_ATYPE_RPL |
		      MGA_DWGCTL_TRANSC |
		      MGA_DWGCTL_SOLID |
		      MGA_DWGCTL_OPCOD_TRAP);
	unsigned int off = 0;

	u32 primaddr = mdev->dma_res.off;
	u32 secaddr = mdev->dma_res.off + mdev->dma_res.len / 2;

	dma_prim_ptr = NULL;
	dma_sec_ptr = NULL;
	dma_end_ptr = NULL;

	if (!mdev->dma_res.index)
		return -ENXIO;

	dma_prim_ptr = mdev->dma_virt;
	dma_sec_ptr = mdev->dma_virt + mdev->dma_res.len / 2;
	dma_end_ptr = mdev->dma_virt + mdev->dma_res.len;

	dev_dbg(mdev->dev, "PRIM: %x - %x (%u bytes)\n",
		dma_prim_ptr, dma_sec_ptr,
		(unsigned long)dma_sec_ptr - (unsigned long)dma_prim_ptr);
	dev_dbg(mdev->dev, "SEC: %x - %x (%u bytes)\n",
		dma_sec_ptr, dma_end_ptr,
		(unsigned long)dma_end_ptr - (unsigned long)dma_sec_ptr);

	off = mga_dma(mdev, off,
		      MGA_MACCESS, pwidth | mdev->maccess,
		      MGA_PITCH, 1024,
		      MGA_YDSTORG, 0,
		      MGA_CXBNDRY, (0xfff << 16) | 0);

	off = mga_dma(mdev, off,
		      MGA_YTOP, 0,
		      MGA_YBOT, 0x7fffff,
		      MGA_FCOL, 0x07e007e0,
		      MGA_BCOL, 0x001f001f);

	off = mga_dma(mdev, off,
		      MGA_DMAPAD, 0,
		      MGA_DWGCTL, dwgctl,
		      MGA_FXBNDRY, ((x + w - 1) << 16) | (x & 0xffff),
		      MGA_YDSTLEN + MGA_DWGENG_GO, (y << 16) | (h & 0xffff));

#if 0
	total = 0;
	while (total++ < 500)
		off = mga_dma(mdev, off,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0,
			      MGA_DMAPAD, 0);
#endif

	mga_dma(mdev, off,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0);

	//wbinvd(mdev);

	mga_write32(mdev, MGA_PRIMADDRESS,
		   primaddr | MGA_PRIMADDRESS_PRIMOD_GENERAL_PURPOSE);

	dump_prim(mdev, primaddr + off);

	dev_dbg(mdev->dev, "starting prim DMA at %x (%u bytes)\n",
		mga_read32(mdev, MGA_PRIMADDRESS), off);
	mga_write32(mdev, MGA_PRIMEND, primaddr + off);
	dev_dbg(mdev->dev, "started prim DMA (addr=%x)\n",
		mga_read32(mdev, MGA_PRIMADDRESS));

	mga_wait_dma_idle(mdev);
	mga_wait_dwgeng_idle(mdev);

	dev_dbg(mdev->dev, "prim DMA finished (addr=%x)\n",
		mga_read32(mdev, MGA_PRIMADDRESS));
	dev_dbg(mdev->dev, "sec DMA (addr=%x)\n",
		mga_read32(mdev, MGA_SECADDRESS));

	dma_prim_ptr = NULL;
	dma_sec_ptr = NULL;
	dma_end_ptr = NULL;

	return 0;
}

static int mga_tex_test(struct mga_dev *mdev,
			unsigned int bpp,
			int x, int y, int w, int h)
{
	u32 pwidth = get_pwidth(mdev->pixel_format);

	static int oo;

	if (!oo){
		int i, j;
		u16 *ptr = mdev->mem_virt;
		ptr += 1024 * 768;
		for (i = 0; i < 512; i++)
			for (j = 0; j < 512; j++)
				//*ptr++ += i * i % 768;
				*ptr++ = ((j & 0x1f) << 11) |
					0x07e0|
					(i & 0x1f);
		oo=1;
	}

	mga_dma(mdev, 0,
		MGA_DR0, 0,
		MGA_DR2, 0,
		MGA_DR3, 0,
		MGA_DR6, 0);
	mga_dma(mdev, 0,
		MGA_DR7, 0,
		MGA_DR10, 0,
		MGA_DR11, (1 << 15) / h,
		MGA_DR14, 0);
	mga_dma(mdev, 0,
		MGA_DR15, 0,
		MGA_DR4, 256 << 15,
		MGA_DR8, 0 << 15,
		MGA_DR12, 256 << 15);

	mga_dma(mdev, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_ALPHACTRL, 0,
		//MGA_TEXTRANS, 0x0000ffff);
		MGA_TEXTRANS, 0x00000000);

	mga_dma(mdev, 0,
		MGA_TEXWIDTH,  ((((512 -  1) & 0x7ff) << 18) |
				(((  4 -  9) &  0x3f) <<  9) |
				(((  9 +  4) &  0x3d) <<  0)),
		MGA_TEXHEIGHT, ((((512 -  1) & 0x7ff) << 18) |
				(((  4 -  9) &  0x3f) <<  9) |
				(((  9 +  4) &  0x3d) <<  0)),
		MGA_TEXCTL, (MGA_TEXCTL_NPCEN |
			     MGA_TEXCTL_CLAMPU |
			     MGA_TEXCTL_CLAMPV |
			     MGA_TEXCTL_STRANS |//
			     MGA_TEXCTL_ITRANS |//
			     //MGA_TEXCTL_TMODULATE |//
			     MGA_TEXCTL_DECALCKEY |//
			     MGA_TEXCTL_TAKEY |
			     MGA_TEXCTL_TPITCH_512 |
			     MGA_TEXCTL_TEXFORMAT_TW16),
		MGA_TEXORG, 1024 * 768 * 2);

	mga_dma(mdev, 0,
		MGA_TMR0, (768 << (20 - 9)) / w/2,
		MGA_TMR3, (768 << (20 - 9)) / h/2,
		MGA_TMR6, 0,//s/wc start
		MGA_TMR7, (256 << (20 - 9)));//t/wc start

	mga_dma(mdev, 0,
		MGA_TMR1, (512 << (20 - 9)) / h/2,
		MGA_TMR2, -(512 << (20 - 9)) / w/2,
		MGA_TMR4, 0,
		MGA_TMR5, 0);

	mga_dma(mdev, 0,
		MGA_TMR8, 1 << 20,
		MGA_DWGCTL, (MGA_DWGCTL_BOP_COPY |
			     MGA_DWGCTL_SHFTZERO |
			     MGA_DWGCTL_SGNZERO |
			     MGA_DWGCTL_ARZERO |
			     MGA_DWGCTL_ATYPE_I |
			     MGA_DWGCTL_OPCOD_TEXTURE_TRAP),
		//		MGA_DWGCTL_OPCOD_TRAP),
		MGA_FCOL, 0xffffffff,
		MGA_DMAPAD, 0);

	mga_dma(mdev, 0,
		MGA_MACCESS, pwidth | mdev->maccess,
		MGA_PITCH, 1024,
		MGA_YDSTORG, 0,
		MGA_CXBNDRY, (0xfff << 16) | 0);

	mga_dma(mdev, 0,
		MGA_YTOP, 0,
		MGA_YBOT, 0x7fffff,
		MGA_FXBNDRY, ((x + w) << 16) | (x & 0xffff),
		MGA_YDSTLEN + MGA_DWGENG_GO, (y << 16) | (h & 0xffff));

	mga_wait_dwgeng_idle(mdev);

	mga_wait_fifo(mdev, 4);

	mga_dma(mdev, 0,
		MGA_DWGCTL, (MGA_DWGCTL_BLTMOD_BFCOL |
			     MGA_DWGCTL_BOP_COPY |
			     MGA_DWGCTL_SHFTZERO |
			     MGA_DWGCTL_SGNZERO |
			     MGA_DWGCTL_ARZERO |
			     MGA_DWGCTL_ATYPE_RPL |
			     MGA_DWGCTL_OPCOD_TRAP),
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0);

	return 0;
}

static int mga_trap_test(struct mga_dev *mdev,
			 unsigned int bpp,
			 int x0, int w0,
			 int x1, int w1,
			 int y, int h)
{
	u32 pwidth = get_pwidth(mdev->pixel_format);

	static int oo;

	int dxl = x1 - x0;
	int dxr = (x1 + w1) - (x0 + w0);
	u32 sgn = 0;

	if (dxl < 0) {
		sgn |= MGA_SGN_SDXL;
		dxl = -dxl;
	}
	if (dxr < 0) {
		sgn |= MGA_SGN_SDXR;
		dxr = -dxr;
	}

	if (!oo) {
		int i, j;
		u16 *ptr = mdev->mem_virt;
		ptr += 1024 * 768;
		for (i = 0; i < 512; i++)
			for (j = 0; j < 512; j++)
				//*ptr++ += i * i % 768;
				*ptr++ = ((j & 0x1f) << 11) |
					0x07e0|
					(i & 0x1f);
		oo=1;
	}

	mga_dma(mdev, 0,
		MGA_DR0, 0,
		MGA_DR2, 0,
		MGA_DR3, 0,
		MGA_DR6, 0);
	mga_dma(mdev, 0,
		MGA_DR7, 0,
		MGA_DR10, 0,
		MGA_DR11, (255 << 15) / h,
		MGA_DR14, 0);
	mga_dma(mdev, 0,
		MGA_DR15, 0,
		MGA_DR4, 255 << 15,
		MGA_DR8, 0 << 15,
		MGA_DR12, 255 << 15);

	mga_dma(mdev, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_ALPHACTRL, 0,
		MGA_TEXTRANS, 0x0000ffff);
	//MGA_TEXTRANS, 0x00000000);

	mga_dma(mdev, 0,
		MGA_TEXWIDTH,  ((((512 -  1) & 0x7ff) << 18) |
				(((  4 -  9) &  0x3f) <<  9) |
				(((  9 +  4) &  0x3d) <<  0)),
		MGA_TEXHEIGHT, ((((512 -  1) & 0x7ff) << 18) |
				(((  4 -  9) &  0x3f) <<  9) |
				(((  9 +  4) &  0x3d) <<  0)),
		MGA_TEXCTL, (MGA_TEXCTL_NPCEN |
			     //MGA_TEXCTL_CLAMPU |
			     //MGA_TEXCTL_CLAMPV |
			     //MGA_TEXCTL_STRANS |//
			     //MGA_TEXCTL_ITRANS |//
			     MGA_TEXCTL_TMODULATE |//
			     //MGA_TEXCTL_DECALCKEY |//
			     MGA_TEXCTL_TAKEY |
			     MGA_TEXCTL_TPITCH_512 |
			     MGA_TEXCTL_TEXFORMAT_TW16),
		MGA_TEXORG, 1024 * 768 * 2);

	mga_dma(mdev, 0,
		MGA_TMR0, (512 << (20 - 9)) / w0,
		MGA_TMR3, (512 << (20 - 9)) / h,
		MGA_TMR6, 0,//s/wc start
		MGA_TMR7, 0);//t/wc start

	mga_dma(mdev, 0,
		MGA_TMR1, ((x0-x1) << (20 - 9)) / w0,
		MGA_TMR2, 0,//-(512 << (20 - 9)) / w0/2,
		MGA_TMR4, 0,
		MGA_TMR5, ((w1-w0) << (20 - 9)) / w0);

	mga_dma(mdev, 0,
		MGA_TMR8, 1 << 20,
		MGA_DWGCTL, (MGA_DWGCTL_BOP_COPY |
			     MGA_DWGCTL_SHFTZERO |
			     MGA_DWGCTL_ATYPE_I |
			     MGA_DWGCTL_OPCOD_TEXTURE_TRAP),
		//MGA_DWGCTL_OPCOD_TRAP),
		MGA_FCOL, 0xffffffff,
		MGA_SGN, sgn);

	mga_dma(mdev, 0,
		MGA_AR0, h,
		MGA_AR1, -dxl,
		MGA_AR2, -dxl,
		MGA_AR4, -dxr);

	mga_dma(mdev, 0,
		MGA_AR5, -dxr,
		MGA_AR6, h,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0);

	mga_dma(mdev, 0,
		MGA_MACCESS, pwidth | mdev->maccess,
		MGA_PITCH, 1024,
		MGA_YDSTORG, 0,
		MGA_CXBNDRY, (0xfff << 16) | 0);

	mga_dma(mdev, 0,
		MGA_YTOP, 0,
		MGA_YBOT, 0x7fffff,
		MGA_FXBNDRY, ((x0 + w0) << 16) | (x0 & 0xffff),
		MGA_YDSTLEN + MGA_DWGENG_GO, (y << 16) | (h & 0xffff));

	mga_wait_dwgeng_idle(mdev);

	mga_wait_fifo(mdev, 4);

	mga_dma(mdev, 0,
		MGA_DWGCTL, (MGA_DWGCTL_BLTMOD_BFCOL |
			     MGA_DWGCTL_BOP_COPY |
			     MGA_DWGCTL_SHFTZERO |
			     MGA_DWGCTL_SGNZERO |
			     MGA_DWGCTL_ARZERO |
			     MGA_DWGCTL_ATYPE_RPL |
			     MGA_DWGCTL_OPCOD_TRAP),
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0,
		MGA_DMAPAD, 0);

	return 0;
}

int mga_mem_test(struct mga_dev *mdev,
		 u32 *read_bytes,
		 u32 *write_bytes,
		 u32 *read_usec,
		 u32 *write_usec,
		 u32 *clock)
{
	void *data;

#if 1
	//return mga_dma_fill_test(mdev,drm_format_bpp(mdev->pixel_format),
	//32, 32, 32, 32);
	return mga_dma_test(mdev,drm_format_bpp(mdev->pixel_format),
			    32, 32, 32, 32) ||
		mga_dma_test(mdev, 1, 64, 33, 32, 32);

	return mga_trap_test(mdev,drm_format_bpp(mdev->pixel_format),
			     256, 256,
			     128, 512,
			     256, 512);

	return mga_tex_test(mdev,drm_format_bpp(mdev->pixel_format),
			    512, 512, 256, 256);

	return mga_iload_test(mdev, drm_format_bpp(mdev->pixel_format),
			      32, 32, 32, 32) ||
		mga_iload_test(mdev, 1, 64, 33, 32, 32);
#endif

	if (posix_memalign(&data, 128, mdev->mem_size))
		return -ENOMEM;

	/* do we have IDUMP? */
	if (1 && mdev->chip < MGA_CHIP_G100) {
		mem_test_read_idump(mdev,
				    data,
				    read_bytes,
				    read_usec);
	} else {
		mem_test_read_fb(mdev,
				 data,
				 read_bytes,
				 read_usec);
	}

	if (USE_DMA && mdev->chip > MGA_CHIP_2064W && mdev->dma_virt) {
		mem_test_dma_iload(mdev,
				   data,
				   write_bytes,
				   write_usec);
	} else if (1) {
		mem_test_write_iload(mdev,
				     data,
				     write_bytes,
				     write_usec);
	} else {
		mem_test_write_fb(mdev,
				  data,
				  write_bytes,
				  write_usec);
	}

	free(data);

	*clock = mdev->mclk;

	return 0;
}
