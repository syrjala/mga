/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "mga_dump.h"

u8 pci_cfg_read8(struct pci_dev *pdev, u32 addr)
{
	struct mga_pci_cfg pcfg;

	pcfg.offset = addr;
	pcfg.value = 0xFFFFFFFF;

	ioctl(pdev->fd, MGA_IOC_PCI_CFG_READB, &pcfg, sizeof pcfg);

	return pcfg.value & 0xFF;
}

void pci_cfg_write8(struct pci_dev *pdev, u32 addr, u8 value)
{
	struct mga_pci_cfg pcfg;

	pcfg.offset = addr;
	pcfg.value = value;

	ioctl(pdev->fd, MGA_IOC_PCI_CFG_WRITEB, &pcfg, sizeof pcfg);
}

u32 pci_cfg_read32(struct pci_dev *pdev, u32 addr)
{
	struct mga_pci_cfg pcfg;

	pcfg.offset = addr;
	pcfg.value = 0xFFFFFFFF;

	ioctl(pdev->fd, MGA_IOC_PCI_CFG_READL, &pcfg, sizeof pcfg);

	return pcfg.value;
}

void pci_cfg_write32(struct pci_dev *pdev, u32 addr, u32 value)
{
	struct mga_pci_cfg pcfg;

	pcfg.offset = addr;
	pcfg.value = value;

	ioctl(pdev->fd, MGA_IOC_PCI_CFG_WRITEL, &pcfg, sizeof pcfg);
}

int pci_cfg_open(struct pci_dev *pdev, int fd)
{
	pdev->fd = fd;

	return 0;
}

void pci_cfg_close(struct pci_dev *pdev)
{
	pdev->fd = -1;
}

void pci_cfg_save(struct pci_dev *pdev)
{
	int i;

	for (i = 0; i < 16; i++)
		pdev->saved[i] = pci_cfg_read32(pdev, i << 2);
}

void pci_cfg_restore(struct pci_dev *pdev)
{
	int i;

	for (i = 10; i < 16; i++)
		pci_cfg_write32(pdev, i << 2, pdev->saved[i]);
	for (i = 4; i < 10; i++)
		pci_cfg_write32(pdev, i << 2, pdev->saved[i]);
	for (i = 0; i < 4; i++)
		pci_cfg_write32(pdev, i << 2, pdev->saved[i]);
}
