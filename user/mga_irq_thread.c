/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <signal.h>

#include "mga_dump.h"

#include "mga_regs.h"

enum {
	IRQTHREAD_SLEEP_MS = 2,
};

void mga_write32_ien(struct mga_dev *mdev, u32 reg, u32 val)
{
	int r;

	BUG_ON(reg != MGA_IEN);

	pthread_mutex_lock(&mdev->irqthread.mutex);
	dev_dbg(mdev->dev, "IEN=%08x->%08x\n", mdev->irqthread.ien, val);
	r = ioctl(mdev->fd, MGA_IOC_IEN, &val);
	if (r)
		dev_warn(mdev->dev, "MGA_IOC_IEN failed: %d:%s\n", errno, strerror(errno));
	else
		mdev->irqthread.ien = val;
	dev_dbg(mdev->dev, "real ien = 0x%08x\n", readl(mdev->mmio_virt + MGA_IEN));
	pthread_mutex_unlock(&mdev->irqthread.mutex);
}

void mga_write32_iclear(struct mga_dev *mdev, u32 reg, u32 val)
{
	int r;

	BUG_ON(reg != MGA_ICLEAR);

	pthread_mutex_lock(&mdev->irqthread.mutex);
	dev_dbg(mdev->dev, "ICLEAR=%08x\n", val);
	r = ioctl(mdev->fd, MGA_IOC_ICLEAR, &val);
	if (r)
		dev_warn(mdev->dev, "MGA_IOC_ICLEAR failed: %d:%s\n", errno, strerror(errno));
	dev_dbg(mdev->dev, "real ien =  0x%08x\n", readl(mdev->mmio_virt + MGA_IEN));
	pthread_mutex_unlock(&mdev->irqthread.mutex);
}

u32 mga_read32_ien(struct mga_dev *mdev, u32 reg)
{
	u32 val;

	BUG_ON(reg != MGA_IEN);

	pthread_mutex_lock(&mdev->irqthread.mutex);
	val = mdev->irqthread.ien;
	pthread_mutex_unlock(&mdev->irqthread.mutex);

	return val;
}

static bool irq_thread_active;

static void sigusr1(int signal)
{
	(void)signal;
	irq_thread_active = false;
}

static void *mga_irq_thread(void *data)
{
	struct mga_dev *mdev = data;
	struct sigaction act = {
		.sa_handler = sigusr1,
	};

	sigaction(SIGUSR1, &act, NULL);

	while (irq_thread_active) {
		bool call_isr = false;
		irq_handler_t isr = NULL;
		void *isr_dev = NULL;
		u32 status;
		int irq = 0;
		int ret;

		ret = ioctl(mdev->fd, MGA_IOC_WAIT_IRQ);
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			break;

		dev_dbg(mdev->dev, "IRQ\n");

		pthread_mutex_lock(&mdev->irqthread.mutex);
		isr = mdev->irqthread.isr;
		isr_dev = mdev->irqthread.isr_dev;
		mdev->irqthread.irq_pending = true;
		pthread_mutex_unlock(&mdev->irqthread.mutex);

		if (isr)
			isr(irq, isr_dev);

		pthread_mutex_lock(&mdev->irqthread.mutex);
		mdev->irqthread.irq_pending = false;
		pthread_cond_broadcast(&mdev->irqthread.irq_pending_cond);
		pthread_mutex_unlock(&mdev->irqthread.mutex);
	}

	pthread_mutex_lock(&mdev->irqthread.mutex);
	mdev->irqthread.irq_pending = false;
	pthread_cond_broadcast(&mdev->irqthread.irq_pending_cond);
	pthread_mutex_unlock(&mdev->irqthread.mutex);

	dev_dbg(mdev->dev, "IRQ thread done\n");

	return NULL;
}

int mga_irq_thread_init(struct mga_dev *mdev)
{
	mdev->irqthread.ien = 0;
	mga_write32_ien(mdev, MGA_IEN, 0);

	pthread_mutex_init(&mdev->irqthread.mutex, NULL);
	pthread_cond_init(&mdev->irqthread.cond, NULL);
	pthread_cond_init(&mdev->irqthread.irq_pending_cond, NULL);

	irq_thread_active = true;
	return pthread_create(&mdev->irqthread.thread, NULL, mga_irq_thread, mdev);
}

void mga_irq_thread_fini(struct mga_dev *mdev)
{
	BUG_ON(mdev->irqthread.isr);
	BUG_ON(mdev->irqthread.ien);

	mga_write32_ien(mdev, MGA_IEN, 0);
	mdev->irqthread.ien = 0;

	irq_thread_active = false;
	pthread_kill(mdev->irqthread.thread, SIGUSR1);
	pthread_join(mdev->irqthread.thread, NULL);
	pthread_mutex_destroy(&mdev->irqthread.mutex);
	pthread_cond_destroy(&mdev->irqthread.cond);
	pthread_cond_destroy(&mdev->irqthread.irq_pending_cond);
}

int request_irq(unsigned int irq, irq_handler_t handler, unsigned long flags,
		const char *name, void *dev)
{
	struct mga_dev *mdev = dev;
	int ret = 0;

	dev_dbg(mdev->dev, "%s\n", __func__);

	BUG_ON(!dev);

	if (flags)
		return -EINVAL;
	if (!handler)
		return -EINVAL;
	if (!irq)
		return -EINVAL;

	if (irq != mdev->irq.irq)
		return -ENODEV;

	pthread_mutex_lock(&mdev->irqthread.mutex);

	while (mdev->irqthread.irq_pending)
		pthread_cond_wait(&mdev->irqthread.irq_pending_cond, &mdev->irqthread.mutex);

	if (mdev->irqthread.isr)
		ret = -EBUSY;
	else {
		mdev->irqthread.isr = handler;
		mdev->irqthread.isr_dev = dev;
	}

	pthread_mutex_unlock(&mdev->irqthread.mutex);

	return ret;
}

void free_irq(unsigned int irq, void *dev)
{
	struct mga_dev *mdev = dev;

	BUG_ON(!dev);
	BUG_ON(!irq);
	BUG_ON(irq != mdev->irq.irq);

	pthread_mutex_lock(&mdev->irqthread.mutex);

	while (mdev->irqthread.irq_pending)
		pthread_cond_wait(&mdev->irqthread.irq_pending_cond, &mdev->irqthread.mutex);

	BUG_ON(!mdev->irqthread.isr);

	mdev->irqthread.isr = NULL;
	mdev->irqthread.isr_dev = NULL;

	pthread_mutex_unlock(&mdev->irqthread.mutex);
}
