/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_TVO_COMMON_H
#define MGA_TVO_COMMON_H

enum mga_tvo_mode {
	MGA_TVO_MODE_CRT,
	MGA_TVO_MODE_TVOUT,
	MGA_TVO_MODE_BT656,
};

enum mga_tvo_cable_type {
	MGA_TVO_CABLE_COMPOSITE_SVIDEO,
	MGA_TVO_CABLE_SCART_COMPOSITE,
	MGA_TVO_CABLE_SCART_RGB,
	MGA_TVO_CABLE_SCART_TYPE2,
};

struct mga_tvo_config {
	enum mga_tvo_mode mode;
	enum mga_tvo_cable_type cable_type;
	enum drm_tv_standard tv_standard;
	u8 gamma;
	u8 deflicker;
	u8 vidrst_delay;
	bool text_filter;
	bool dot_crawl_freeze;
	bool color_bars;
};

#endif
