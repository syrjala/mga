/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <string.h>

#include "kernel_emul.h"
#include "mga_regs.h"
#include "mga_dac_regs.h"
#include "mga_pins.h"

#define min(a,b) ((a) < (b) ? (a) : (b))

const char *mga_chip_name(unsigned int chip);

static void parse_pins5_g450(const u8 *pins, struct mga_pins5 *p);
static void parse_pins4_g400(const u8 *pins, struct mga_pins4 *p);
static void parse_pins3_g200(const u8 *pins, struct mga_pins3 *p);
static void parse_pins3_g100(const u8 *pins, struct mga_pins3 *p);
static void parse_pins2_1064sg(const u8 *pins, struct mga_pins2 *p);
static void parse_pins2_2164w(const u8 *pins, struct mga_pins2 *p);
static void parse_pins1_2064w(const u8 *pins, struct mga_pins1 *p);

static void print_pins5_g450(const struct mga_pins5 *p);
static void print_pins4_g400(const struct mga_pins4 *p);
static void print_pins3_g200(const struct mga_pins3 *p);
static void print_pins3_g100(const struct mga_pins3 *p);
static void print_pins2_1064sg(const struct mga_pins2 *p);
static void print_pins2_2164w(const struct mga_pins2 *p);
static void print_pins1_2064w(const struct mga_pins1 *p);

static const char *print_features(unsigned int features)
{
	static char feat[4096];

	memset(feat, 0, sizeof feat);

	if (!features)
		return "None";

	snprintf(feat, sizeof feat, "%s%s%s%s%s%s%s",
		 (features & _MGA_FEAT_TVO_BUILTIN) ?
		 "TVO (builtin) " : "",
		 (features & _MGA_FEAT_DVD_BUILTIN) ?
		 "DVD (builtin) " : "",
		 (features & _MGA_FEAT_MJPEG_BUILTIN) ?
		 "MJPEG (builtin) " : "",
		 (features & _MGA_FEAT_VIN_BUILTIN) ?
		 "VIN (builtin) " : "",
		 (features & _MGA_FEAT_TUNER_BUILTIN) ?
		 "Tuner (builtin) " : "",
		 (features & _MGA_FEAT_AUDIO_BUILTIN) ?
		 "Audio (builtin) " : "",
		 (features & _MGA_FEAT_TMDS_BUILTIN) ?
		 "TMDS (builtin) " : "");

	feat[sizeof feat - 1] = 0;

	return feat;
}

static u8 rom_read8(const u8 *rom_data, unsigned int off)
{
	return rom_data[off];
}

static u16 rom_read16(const u8 *rom_data, unsigned int off)
{
	return (rom_data[off+1] << 8) | rom_data[off];
}

#if 0
static u32 rom_read32(const u8 *rom_data, unsigned int off)
{
	return (rom_data[off+3] << 24) | (rom_data[off+2] << 16) | (rom_data[off+1] << 8) | rom_data[off];
}
#endif

enum {
	MGA_BIOS_SIG = 0xAA55,
	MGA_PINS_OFF = 0x7FFC,
	MGA_PINS_SIG = 0x412E,
	MGA_PINS1_LEN = 0x40,
	MGA_PINS4_LEN = 0x80,
	MGA_PINS1_VER_MIN = 0x100,
	MGA_PINS1_VER_MAX = 0x105,
};

static int check_pins(const u8* rom_data,
		      unsigned int off,
		      unsigned int max,
		      u16 *ret_len,
		      u16 *ret_ver)
{
	u16 len, ver, sig = rom_read16(rom_data, off);

	if (sig == MGA_PINS_SIG) {
		/* PInS >= 2.0? */

		len = rom_read8(rom_data, off + 2);
		ver = rom_read16(rom_data, off + 4);

		switch (ver >> 8) {
		case 0x02:
		case 0x03:
			if (len != MGA_PINS1_LEN)
				return -ENOENT;
			break;
		case 0x04:
		case 0x05:
			if (len != MGA_PINS4_LEN)
				return -ENOENT;

			if (off + len > max)
				return -ENOENT;
			break;
		default:
			return -ENOENT;
		}
	} else if (sig == MGA_PINS1_LEN) {
		/* PInS 1.0? */

		len = sig;

		ver = rom_read16(rom_data, off + 56);
		if (ver < MGA_PINS1_VER_MIN || ver > MGA_PINS1_VER_MAX)
			return -ENOENT;
	} else
		return -ENOENT;

	*ret_len = len;
	*ret_ver = ver;

	return 0;
}

int mga_parse_pins(unsigned int chip, const u8 *rom_data,
		   size_t rom_size, union mga_pins *pins)
{
	unsigned int max, off = 0;
	u16 len = 0, ver = 0;

	if (rom_size < MGA_PINS1_LEN)
		return -ENOENT;

	pr_debug("\nGoing to look for PInS\n");

	max = min(rom_size, 0x10000) - MGA_PINS1_LEN + 1;

	if (rom_size > MGA_PINS_OFF + 1 && rom_read16(rom_data, 0) == MGA_BIOS_SIG) {
		off = rom_read16(rom_data, MGA_PINS_OFF);

		pr_debug("BIOS signature detected. Checking PInS at 0x%04x.\n", off);
		if (check_pins(rom_data, off, max, &len, &ver)) {
			off = 0;
			pr_debug("PInS check failed. Looking for PInS the hard way\n");
		}
	} else
		pr_debug("No BIOS signature. Looking for PInS the hard way!\n");

	if (!off) {
		for (off = 0; off < max; off++) {
			if (!check_pins(rom_data, off, max, &len, &ver))
				break;
		}

		if (off == max) {
			pr_err("Failed to locate PInS\n");
			return -ENOENT;
		}

		pr_debug("PInS located at 0x%04x\n", off);
	}

	pr_debug("PInS version 0x%04hx\n", ver);
	pr_debug("PInS length %hu bytes\n", len);

	/* No checksum for PInS 1.0 */
	if ((ver >> 8) != 0x01) {
		u8 csum = 0;
		int i;

		for (i = 0; i < len; i++)
			csum += rom_read8(rom_data, off + i);
		if (csum) {
			pr_err("PInS checksum is invalid.\n");
			return -ENOENT;
		}
	}

	switch (ver >> 8) {
	case 0x01:
		switch (chip) {
		case MGA_CHIP_2064W:
			parse_pins1_2064w(rom_data + off, &pins->pins1);
			print_pins1_2064w(&pins->pins1);
			break;
		default:
			pr_err("Unexpected PInS version 0x%04hx for %s\n",
			       ver, mga_chip_name(chip));
			return -ENOENT;
		}
		break;
	case 0x02:
		switch (chip) {
		case MGA_CHIP_2164W:
			parse_pins2_2164w(rom_data + off, &pins->pins2);
			print_pins2_2164w(&pins->pins2);
			break;
		case MGA_CHIP_1064SG:
		case MGA_CHIP_1164SG:
			parse_pins2_1064sg(rom_data + off, &pins->pins2);
			print_pins2_1064sg(&pins->pins2);
			break;
		default:
			pr_err("Unexpected PInS version 0x%04hx for %s\n",
			       ver, mga_chip_name(chip));
			return -ENOENT;
		}
		break;
	case 0x03:
		switch (chip) {
		case MGA_CHIP_G100:
			parse_pins3_g100(rom_data + off, &pins->pins3);
			print_pins3_g100(&pins->pins3);
			break;
		case MGA_CHIP_G200:
		case MGA_CHIP_G200SE:
		case MGA_CHIP_G200EV:
		case MGA_CHIP_G200WB:
			parse_pins3_g200(rom_data + off, &pins->pins3);
			print_pins3_g200(&pins->pins3);
			break;
		default:
			pr_err("Unexpected PInS version 0x%04hx for %s\n",
			       ver, mga_chip_name(chip));
			return -ENOENT;
		}
		break;
	case 0x04:
		switch (chip) {
		case MGA_CHIP_G400:
			parse_pins4_g400(rom_data + off, &pins->pins4);
			print_pins4_g400(&pins->pins4);
			break;
		default:
			pr_err("Unexpected PInS version 0x%04hx for %s\n",
			       ver, mga_chip_name(chip));
			return -ENOENT;
		}
		break;
	case 0x05:
		switch (chip) {
		case MGA_CHIP_G450:
		case MGA_CHIP_G550:
			parse_pins5_g450(rom_data + off, &pins->pins5);
			print_pins5_g450(&pins->pins5);
			break;
		default:
			pr_err("Unexpected PInS version 0x%04hx for %s\n",
			       ver, mga_chip_name(chip));
			return -ENOENT;
		}
		break;
	default:
		pr_err("Unknown PInS version 0x%04hx\n", ver);
		return -ENOENT;
	}

	return 0;
}

static u16 get_le16(const u8 *pins, unsigned int offset)
{
	return  (pins[offset+1] << 8) |
		(pins[offset+0] << 0);
}

static u32 get_le32(const u8 *pins, unsigned int offset)
{
	return  (pins[offset+3] << 24) |
		(pins[offset+2] << 16) |
		(pins[offset+1] <<  8) |
		(pins[offset+0] <<  0);
}

static unsigned int pins1_clk_speed(u16 val)
{
	if (val == 0xffff)
		return 0;
	return val * 10;
}

static unsigned int pins2_clk_speed(u8 val, u8 offset)
{
	if (val == 0xff)
		return 0;
	return (val + offset) * 1000;
}

static unsigned int pins4_clk_speed(u8 val)
{
	if (val == 0xff)
		return 0;
	return val * 4000;
}

static unsigned int pins5_clk_speed(u8 val, u8 mul)
{
	if (val == 0xff)
		return 0;
	return val * mul * 1000;
}

static void pins1_clk(u16 data, unsigned int *ret, unsigned int fallback_clock)
{
	unsigned int clock = pins1_clk_speed(data);
	*ret = clock ? clock : fallback_clock;
}

static void pins2_clk(u8 data, u8 offset, unsigned int *ret, unsigned int fallback_clock)
{
	unsigned int clock = pins2_clk_speed(data, offset);
	*ret = clock ? clock : fallback_clock;
}

static void pins4_clk(u8 data, unsigned int *ret, unsigned int fallback_clock)
{
	unsigned int clock = pins4_clk_speed(data);
	*ret = clock ? clock : fallback_clock;
}

static void pins5_clk(u8 data, u8 mul, unsigned int *ret, unsigned int fallback_clock)
{
	unsigned int clock = pins5_clk_speed(data, mul);
	*ret = clock ? clock : fallback_clock;
}

struct reg_fields {
	u16 shift;
	u16 len;
	const char *name;
};

static void print_reg(const char *name, const u32 value,
		      const struct reg_fields regs[], int nregs)
{
	int i;
	u32 mask = 0;

	pr_debug("%s\n", name);

	for (i = 0; i < nregs; i++) {
		mask |= ((1 << regs[i].len) - 1) << regs[i].shift;
		pr_debug(" %s = %x\n", regs[i].name,
		       (value >> regs[i].shift) & ((1 << regs[i].len) - 1));
	}
	if (value & ~mask)
		pr_debug("*** some reserved bits are set (0x%08x)\n", value & ~mask);
}

static void print_g450_option(const char *name, u32 value)
{
	static const struct reg_fields option_fields[] = {
		{  2, 1, "sysclkdis" },
		{  5, 1, "syspllpdn" },
		{  6, 1, "   pllsel" },
		{  8, 1, "  vgaioen" },
		{  9, 1, " mblktype" },
		{ 10, 3, "memconfig" },
		{ 13, 3, "   mdsfen" },
		{ 14, 1, "hardpwmsk" },
		{ 15, 5, "   rfhcnt" },
		{ 21, 1, "   mdsren" },
		{ 22, 1, "enhmemacc" },
		{ 28, 1, "  nohireq" },
		{ 29, 1, "  noretry" },
		{ 30, 1, "   biosen" },
		{ 31, 1, "  powerpc" },
	};
	print_reg(name, value, option_fields, ARRAY_SIZE(option_fields));
}

static void print_g450_option2(const char *name, u32 value)
{
	static const struct reg_fields option2_fields[] = {
		{  8, 1, " eepromwt" },
		{ 10, 2, "  mclkdrv" },
		{ 12, 2, "  mcmddrv" },
		{ 14, 2, "  mdatdrv" },
		{ 24, 2, "codclksel" },
		{ 26, 1, "codprediv" },
		{ 27, 1, "codpstdiv" },
	};
	print_reg(name, value, option2_fields, ARRAY_SIZE(option2_fields));
}

static void print_g450_option3(const char *name, u32 value)
{
	static const struct reg_fields option3_fields[] = {
		{  0, 2, " gclksel" },
		{  3, 3, " gclkdiv" },
		{  6, 4, "gclkdcyc" },
		{ 10, 2, " mclksel" },
		{ 13, 3, " mclkdiv" },
		{ 16, 4, "mclkdcyc" },
		{ 20, 2, " wclksel" },
		{ 23, 3, " wclkdiv" },
		{ 26, 4, "wclkdcyc" },
	};
	print_reg(name, value, option3_fields, ARRAY_SIZE(option3_fields));
}

static void print_g450_memmisc(const char *name, u32 value)
{
	static const struct reg_fields memmisc_fields[] = {
		{  0, 2, "olelatncy" },
		{  2, 2, " rdlatncy" },
		{ 29, 1, " mdqsiinv" },
		{ 30, 1, "  mdqiinv" },
		{ 31, 1, "   mclken" },
	};
	print_reg(name, value, memmisc_fields, ARRAY_SIZE(memmisc_fields));
}

static void print_g450_memrdbk(const char *name, u32 value)
{
	static const struct reg_fields memrdbk_fields[] = {
		{  0,  4, "mclkbrd0" },
		{  5,  4, "mclkbrd1" },
		{ 12, 12, " emrsreg" },
		{ 25,  5, "mrsopcod" },
	};
	print_reg(name, value, memrdbk_fields, ARRAY_SIZE(memrdbk_fields));
}

static void print_g450_mctlwtst(const char *name, u32 value)
{
	static const struct reg_fields mctlwtst_fields[] = {
		{  0,  3, "casltncy" },
		{  3, 29, " unknown" },
	};
	print_reg(name, value, mctlwtst_fields, ARRAY_SIZE(mctlwtst_fields));
}

static void print_g450_clocks(const char *name,
			      unsigned int syspll,
			      unsigned int vidpll,
			      u32 option3)
{
	static const unsigned int clkspeed[] = {
		33333,
		0,
		0,
		0,
	};
	static const struct {
		unsigned int num;
		unsigned int den;
	} divs[8] = {
		{ 1, 3 },
		{ 2, 5 },
		{ 4, 9 },
		{ 1, 2 },
		{ 2, 3 },
		{ 1, 1 },
	};
	static const char *clksel[] = {
		"PCI",
		"SYSPLL",
		"MCLK pin",
		"VIDPLL",
	};
	static const char *clkdcyc[] = {
		"unaffected",
		"2.75 to 3.25",
		"3.25 to 3.75",
		"3.75 to 4.25",
		"4.25 to 4.75",
		"4.75 to 5.25",
		"5.25 to 5.75",
		"5.75 to 6.25",
		"6.25 to 6.75",
		"6.75 to 7.25",
		"7.25 to 7.75",
		"7.25 to 8.25",
		"8.25 to 8.75",
		"8.75 to 9.25",
		"9.25 to 9.75",
		"9.75 to 10.25",
	};
	static const char *clkdiv[] = {
		"1/3",
		"2/5",
		"4/9",
		"1/2",
		"2/3",
		"1/1",
	};
	unsigned int gclk, mclk, wclk;

	u8 gclksel = (option3 & MGA_OPTION3_GCLKSEL) >> 0;
	u8 mclksel = (option3 & MGA_OPTION3_MCLKSEL) >> 10;
	u8 wclksel = (option3 & MGA_OPTION3_WCLKSEL) >> 20;
	u8 gclkdiv = (option3 & MGA_OPTION3_GCLKDIV) >> 3;
	u8 mclkdiv = (option3 & MGA_OPTION3_MCLKDIV) >> 13;
	u8 wclkdiv = (option3 & MGA_OPTION3_WCLKDIV) >> 23;
	u8 gclkdcyc = (option3 & MGA_OPTION3_GCLKDCYC) >> 6;
	u8 mclkdcyc = (option3 & MGA_OPTION3_MCLKDCYC) >> 16;
	u8 wclkdcyc = (option3 & MGA_OPTION3_WCLKDCYC) >> 26;

	if (gclksel == 1 || gclksel == 3) {
		gclk = (gclksel == 1) ? syspll : vidpll;
		gclk = div_round(gclk * divs[gclkdiv].num, divs[gclkdiv].den);
	} else
		gclk = clkspeed[gclksel];

	if (mclksel == 1 || mclksel == 3) {
		mclk = (mclksel == 1) ? syspll : vidpll;
		mclk = div_round(mclk * divs[mclkdiv].num, divs[mclkdiv].den);
	} else
		mclk = clkspeed[mclksel];

	if (wclksel == 1 || wclksel == 3) {
		wclk = (wclksel == 1) ? syspll : vidpll;
		wclk = div_round(wclk * divs[wclkdiv].num, divs[wclkdiv].den);
	} else
		wclk = clkspeed[wclksel];

	pr_debug("%s:\n"
	       " SYSPLL Fo = %u kHz\n"
	       " VIDPLL Fo = %u kHz\n"
	       "      GCLK = %u kHz (sel=%s, div=%s dcyc=%s)\n"
	       "      MCLK = %u kHz (sel=%s, div=%s dcyc=%s)\n"
	       "      WCLK = %u kHz (sel=%s, div=%s dcyc=%s)\n",
	       name, syspll, vidpll,
	       gclk, clksel[gclksel], clkdiv[gclkdiv], clkdcyc[gclkdcyc],
	       mclk, clksel[mclksel], clkdiv[mclkdiv], clkdcyc[mclkdcyc],
	       wclk, clksel[wclksel], clkdiv[wclkdiv], clkdcyc[wclkdcyc]);
}

static void parse_pins5_g450(const u8 *pins, struct mga_pins5 *p)
{
	u8 vco_mul;

	memset(p, 0, sizeof *p);

	switch (pins[4]) {
	case 0x00:
		vco_mul = 6;
		break;
	case 0x01:
		vco_mul = 8;
		break;
	default:
		vco_mul = 10;
		break;
	}

	pins5_clk(pins[36], vco_mul, &p->syspll.fvco_max, 230000);
	pins5_clk(pins[37], vco_mul, &p->vidpll.fvco_max, p->syspll.fvco_max);
	pins5_clk(pins[38], vco_mul, &p->pixpll.fvco_max, p->vidpll.fvco_max);

	pins4_clk(pins[39], &p->crtc1.pixclk_8_max,  p->pixpll.fvco_max);
	pins4_clk(pins[40], &p->crtc1.pixclk_16_max, p->crtc1.pixclk_8_max);
	pins4_clk(pins[41], &p->crtc1.pixclk_24_max, p->crtc1.pixclk_16_max);
	pins4_clk(pins[42], &p->crtc1.pixclk_32_max, p->crtc1.pixclk_24_max);

	pins4_clk(pins[43], &p->crtc2.pixclk_16_max, p->vidpll.fvco_max);
	pins4_clk(pins[44], &p->crtc2.pixclk_32_max, p->crtc2.pixclk_16_max);

	pins4_clk(pins[47], &p->tmds.pixclk_max, p->crtc1.pixclk_8_max);

	p->option = get_le32(pins, 48);
	p->option2 = get_le32(pins, 52);

	pins4_clk(pins[56], &p->clk_vga.syspll, 230000);
	pins4_clk(pins[57], &p->clk_vga.vidpll, 230000);
	p->clk_vga.option3 = get_le32(pins, 58);
	p->clk_vga.mctlwtst = get_le32(pins, 62);
	p->clk_vga.memmisc = get_le32(pins, 66);
	p->clk_vga.memrdbk = get_le32(pins, 70);

	pins4_clk(pins[74], &p->clk_sh.syspll, 230000);
	pins4_clk(pins[75], &p->clk_sh.vidpll, 230000);
	p->clk_sh.option3 = get_le32(pins, 76);
	p->clk_sh.mctlwtst = get_le32(pins, 80);
	p->clk_sh.memmisc = get_le32(pins, 84);
	p->clk_sh.memrdbk = get_le32(pins, 88);

	pins4_clk(pins[92], &p->clk_dh.syspll, 230000);
	pins4_clk(pins[93], &p->clk_dh.vidpll, 230000);
	p->clk_dh.option3 = get_le32(pins, 94);
	p->clk_dh.mctlwtst = get_le32(pins, 98);
	p->clk_dh.memmisc = get_le32(pins, 102);
	p->clk_dh.memrdbk = get_le32(pins, 106);

	if (pins[110] & 0x01)
		p->fref = 14318;
	else
		p->fref = 27000;

	if (!(pins[110] & 0x02))
		p->features |= _MGA_FEAT_AUDIO_BUILTIN;
	if (!(pins[110] & 0x04))
		p->features |= _MGA_FEAT_TUNER_BUILTIN;
	if (!(pins[110] & 0x08))
		p->features |= _MGA_FEAT_VIN_BUILTIN;

	p->mem.base_size = ((pins[114] & 0x07) + 1) << 23;

	if ((pins[114] & 0x18) == 0x08)
		p->option |= MGA_OPTION_HARDPWMSK;
	else
		p->mem.sdram = true;

	if (pins[115] & 0x01)
		p->maccess |= 0x00004000;

	if (pins[115] & 0x04) {
		p->clk_vga.mctlwtst_core = p->clk_vga.mctlwtst;
		p->clk_sh.mctlwtst_core = p->clk_sh.mctlwtst;
		p->clk_dh.mctlwtst_core = p->clk_dh.mctlwtst;
	} else {
		static const u8 wtst_xlat[] = { 0, 1, 5, 6, 7, 5, 2, 3 };
		p->clk_vga.mctlwtst_core = (p->clk_vga.mctlwtst & ~MGA_MCTLWTST_CASLTNCY) |
			wtst_xlat[(p->clk_vga.mctlwtst & MGA_MCTLWTST_CASLTNCY)];
		p->clk_sh.mctlwtst_core = (p->clk_sh.mctlwtst & ~MGA_MCTLWTST_CASLTNCY) |
			wtst_xlat[(p->clk_sh.mctlwtst & MGA_MCTLWTST_CASLTNCY)];
		p->clk_dh.mctlwtst_core = (p->clk_dh.mctlwtst & ~MGA_MCTLWTST_CASLTNCY) |
			wtst_xlat[(p->clk_dh.mctlwtst & MGA_MCTLWTST_CASLTNCY)];
	}

	if ((pins[114] & 0x60) == 0x20)
		p->mem.ddr = true;
	if (pins[115] & 0x01)
		p->mem.emrswen = true;
	if (pins[115] & 0x02)
		p->mem.dll = true;

	if (pins[117] & 0x01)
		p->outputs |= _MGA_OUTPUT_DAC1;
	if (pins[117] & 0x02)
		p->outputs |= _MGA_OUTPUT_TMDS1;
	if (pins[117] & 0x04)
		p->outputs |= _MGA_OUTPUT_TVOUT;
	if (pins[117] & 0x10)
		p->outputs |= _MGA_OUTPUT_DAC2;
	if (pins[117] & 0x20)
		p->outputs |= _MGA_OUTPUT_TMDS2;
	if (pins[117] & 0x40)
		p->outputs |= _MGA_OUTPUT_TVOUT;

	pins4_clk(pins[118], &p->dac1.pixclk_max, p->crtc1.pixclk_8_max);
	pins4_clk(pins[119], &p->dac2.pixclk_max, p->dac1.pixclk_max);

	pins5_clk(pins[121], vco_mul, &p->syspll.fvco_min, 50000);
	pins5_clk(pins[122], vco_mul, &p->vidpll.fvco_min, 50000);
	pins5_clk(pins[123], vco_mul, &p->pixpll.fvco_min, 50000);

	pins4_clk(pins[124], &p->crtc1.pixclk_32_max_dh, p->crtc1.pixclk_32_max);
	pins4_clk(pins[125], &p->crtc2.pixclk_32_max_dh, p->crtc2.pixclk_32_max);
}

static void print_pins5_g450(const struct mga_pins5 *p)
{
	const char *mem_type = "unknown";
	const char *mem_rate = "unknown";

	if (p->mem.sdram)
		mem_type = "SDRAM";
	else
		mem_type = "SGRAM";

	if (p->mem.ddr)
		mem_rate = "DDR";
	else
		mem_rate = "SDR";

	pr_debug("\nPInS 5.0 (G450/G550)\n"
	       "           mem type    = %s\n"
	       "           mem rate    = %s\n"
	       "        mem emrswen    = %s\n"
	       "            mem dll    = %s\n"
	       "           base mem    = %u MB\n"
	       "               Fref    = %u kHz\n"
	       "    SYSPLL Fvco max    = %u kHz\n"
	       "    VIDPLL Fvco max    = %u kHz\n"
	       "    PIXPLL Fvco max    = %u kHz\n"
	       "    SYSPLL Fvco min    = %u kHz\n"
	       "    VIDPLL Fvco min    = %u kHz\n"
	       "    PIXPLL Fvco min    = %u kHz\n"
	       "CRTC1 pixclk  8 max    = %u kHz\n"
	       "CRTC1 pixclk 16 max    = %u kHz\n"
	       "CRTC1 pixclk 24 max    = %u kHz\n"
	       "CRTC1 pixclk 32 max    = %u kHz\n"
	       "CRTC1 pixclk 32 max dh = %u kHz\n"
	       "CRTC2 pixclk 16 max    = %u kHz\n"
	       "CRTC2 pixclk 32 max    = %u kHz\n"
	       "CRTC2 pixclk 32 max dh = %u kHz\n"
	       "     DAC1 pixclk max   = %u kHz\n"
	       "     DAC2 pixclk max   = %u kHz\n"
	       "     TMDS pixclk max   = %u kHz\n"
	       "             MACCESS   = %08x\n"
	       "              OPTION   = %08x\n"
	       "             OPTION2   = %08x\n"
	       "   clk vga SYSPLL Fo   = %u kHz\n"
	       "   clk vga VIDPLL Fo   = %u kHz\n"
	       "     clk vga OPTION3   = %08x\n"
	       "    clk vga MCTLWTST   = %08x\n"
	       "  clk vga MCTLWTST C   = %08x\n"
	       "     clk vga MEMMISC   = %08x\n"
	       "     clk vga MEMRDBK   = %08x\n"
	       "    clk sh SYSPLL Fo   = %u kHz\n"
	       "    clk sh VIDPLL Fo   = %u kHz\n"
	       "      clk sh OPTION3   = %08x\n"
	       "     clk sh MCTLWTST   = %08x\n"
	       "   clk sh MCTLWTST C   = %08x\n"
	       "      clk sh MEMMISC   = %08x\n"
	       "      clk sh MEMRDBK   = %08x\n"
	       "    clk dh SYSPLL Fo   = %u kHz\n"
	       "    clk dh VIDPLL Fo   = %u kHz\n"
	       "      clk dh OPTION3   = %08x\n"
	       "     clk dh MCTLWTST   = %08x\n"
	       "   clk dh MCTLWTST C   = %08x\n"
	       "      clk dh MEMMISC   = %08x\n"
	       "      clk dh MEMRDBK   = %08x\n"
	       "            Features   = %s\n",
	       mem_type,
	       mem_rate,
	       p->mem.emrswen  ? "yes" : "no",
	       p->mem.dll      ? "yes" : "no",
	       p->mem.base_size >> 20,
	       p->fref,
	       p->syspll.fvco_max,
	       p->vidpll.fvco_max,
	       p->pixpll.fvco_max,
	       p->syspll.fvco_min,
	       p->vidpll.fvco_min,
	       p->pixpll.fvco_min,
	       p->crtc1.pixclk_8_max,
	       p->crtc1.pixclk_16_max,
	       p->crtc1.pixclk_24_max,
	       p->crtc1.pixclk_32_max,
	       p->crtc1.pixclk_32_max_dh,
	       p->crtc2.pixclk_16_max,
	       p->crtc2.pixclk_32_max,
	       p->crtc2.pixclk_32_max_dh,
	       p->dac1.pixclk_max,
	       p->dac2.pixclk_max,
	       p->tmds.pixclk_max,
	       p->maccess,
	       p->option,
	       p->option2,
	       p->clk_vga.syspll,
	       p->clk_vga.vidpll,
	       p->clk_vga.option3,
	       p->clk_vga.mctlwtst,
	       p->clk_vga.mctlwtst_core,
	       p->clk_vga.memmisc,
	       p->clk_vga.memrdbk,
	       p->clk_sh.syspll,
	       p->clk_sh.vidpll,
	       p->clk_sh.option3,
	       p->clk_sh.mctlwtst,
	       p->clk_sh.mctlwtst_core,
	       p->clk_sh.memmisc,
	       p->clk_sh.memrdbk,
	       p->clk_dh.syspll,
	       p->clk_dh.vidpll,
	       p->clk_dh.option3,
	       p->clk_dh.mctlwtst,
	       p->clk_dh.mctlwtst_core,
	       p->clk_dh.memmisc,
	       p->clk_dh.memrdbk,
	       print_features(p->features));

	print_g450_option("OPTION", p->option);
	print_g450_option2("OPTION", p->option2);

	print_g450_option3("VGA/OPTION3", p->clk_vga.option3);
	print_g450_memmisc("VGA/MEMMISC", p->clk_vga.memmisc);
	print_g450_memrdbk("VGA/MEMRDBK", p->clk_vga.memrdbk);
	print_g450_mctlwtst("VGA/MCTLWTST", p->clk_vga.mctlwtst);
	print_g450_mctlwtst("VGA/MCTLWTST CORE", p->clk_vga.mctlwtst_core);

	print_g450_option3("SH/OPTION3", p->clk_sh.option3);
	print_g450_memmisc("SH/MEMMISC", p->clk_sh.memmisc);
	print_g450_memrdbk("SH/MEMRDBK", p->clk_sh.memrdbk);
	print_g450_mctlwtst("SH/MCTLWTST", p->clk_sh.mctlwtst);
	print_g450_mctlwtst("SH/MCTLWTST CORE", p->clk_sh.mctlwtst_core);

	print_g450_option3("DH/OPTION3", p->clk_dh.option3);
	print_g450_memmisc("DH/MEMMISC", p->clk_dh.memmisc);
	print_g450_memrdbk("DH/MEMRDBK", p->clk_dh.memrdbk);
	print_g450_mctlwtst("DH/MCTLWTST", p->clk_dh.mctlwtst);
	print_g450_mctlwtst("DH/MCTLWTST CORE", p->clk_dh.mctlwtst_core);

	print_g450_clocks("VGA clocks", p->clk_vga.syspll,
			  p->clk_vga.vidpll, p->clk_vga.option3);
	print_g450_clocks("SH clocks", p->clk_sh.syspll,
			  p->clk_sh.vidpll, p->clk_sh.option3);
	print_g450_clocks("DH clocks", p->clk_dh.syspll,
			  p->clk_dh.vidpll, p->clk_dh.option3);
}

static void print_g400_option(const char *name, u32 value)
{
	static const struct reg_fields option_fields[] = {
		{  2, 1, "sysclkdis" },
		{  5, 1, "syspllpdn" },
		{  6, 1, "   pllsel" },
		{  8, 1, "  vgaioen" },
		{ 10, 3, "memconfig" },
		{ 14, 1, "hardpwmsk" },
		{ 15, 5, "   rfhcnt" },
		{ 22, 1, "enhmemacc" },
		{ 28, 1, "  nohireq" },
		{ 29, 1, "  noretry" },
		{ 30, 1, "   biosen" },
		{ 31, 1, "  powerpc" },
	};
	print_reg(name, value, option_fields, ARRAY_SIZE(option_fields));
}

static void print_g400_option3(const char *name, u32 value)
{
	static const struct reg_fields option3_fields[] = {
		{  0, 2, " gclksel" },
		{  3, 3, " gclkdiv" },
		{  6, 4, "gclkdcyc" },
		{ 10, 2, " mclksel" },
		{ 13, 3, " mclkdiv" },
		{ 16, 4, "mclkdcyc" },
		{ 20, 2, " wclksel" },
		{ 23, 3, " wclkdiv" },
		{ 26, 4, "wclkdcyc" },
	};
	print_reg(name, value, option3_fields, ARRAY_SIZE(option3_fields));
}

static void print_g400_memrdbk(const char *name, u32 value)
{
	static const struct reg_fields memrdbk_fields[] = {
		{  0, 4, "mclkbrd0" },
		{  5, 4, "mclkbrd1" },
		{ 22, 2, "strmfctl" },
		{ 25, 5, "mrsopcod" },
	};
	print_reg(name, value, memrdbk_fields, ARRAY_SIZE(memrdbk_fields));
}

static void print_g400_mctlwtst(const char *name, u32 value)
{
	static const struct reg_fields mctlwtst_fields[] = {
		{  0, 3, "casltncy" },
		{  4, 2, "rrddelay" },
		{  7, 2, "rcddelay" },
		{ 10, 3, "  rasmin" },
		{ 14, 2, " rpdelay" },
		{ 18, 2, " wrdelay" },
		{ 21, 1, " rddelay" },
		{ 23, 2, "smrdelay" },
		{ 26, 2, "bwcdelay" },
		{ 29, 3, "bpldelay" },
	};
	print_reg(name, value, mctlwtst_fields, ARRAY_SIZE(mctlwtst_fields));
}

static void print_g400_clocks(const char *name, unsigned int gclk, u32 option3)
{
	static const unsigned int clkspeed[] = {
		33333,
		0,
		0,
		66667,
	};
	static const struct {
		unsigned int num;
		unsigned int den;
	} divs[8] = {
		{ 1, 3 },
		{ 2, 5 },
		{ 4, 9 },
		{ 1, 2 },
		{ 2, 3 },
		{ 1, 1 },
	};
	static const char *clksel[] = {
		"PCI",
		"SYSPLL",
		"MCLK pin",
		"AGPDLL",
	};
	static const char *clkdcyc[] = {
		"unaffected",
		"2.75 to 3.25",
		"3.25 to 3.75",
		"3.75 to 4.25",
		"4.25 to 4.75",
		"4.75 to 5.25",
		"5.25 to 5.75",
		"5.75 to 6.25",
		"6.25 to 6.75",
		"6.75 to 7.25",
		"7.25 to 7.75",
		"7.25 to 8.25",
		"8.25 to 8.75",
		"8.75 to 9.25",
		"9.25 to 9.75",
		"9.75 to 10.25",
	};
	static const char *clkdiv[] = {
		"1/3",
		"2/5",
		"4/9",
		"1/2",
		"2/3",
		"1/1",
	};
	unsigned int syspll, mclk, wclk;

	u8 gclksel = (option3 & MGA_OPTION3_GCLKSEL) >> 0;
	u8 mclksel = (option3 & MGA_OPTION3_MCLKSEL) >> 10;
	u8 wclksel = (option3 & MGA_OPTION3_WCLKSEL) >> 20;
	u8 gclkdiv = (option3 & MGA_OPTION3_GCLKDIV) >> 3;
	u8 mclkdiv = (option3 & MGA_OPTION3_MCLKDIV) >> 13;
	u8 wclkdiv = (option3 & MGA_OPTION3_WCLKDIV) >> 23;
	u8 gclkdcyc = (option3 & MGA_OPTION3_GCLKDCYC) >> 6;
	u8 mclkdcyc = (option3 & MGA_OPTION3_MCLKDCYC) >> 16;
	u8 wclkdcyc = (option3 & MGA_OPTION3_WCLKDCYC) >> 26;

	syspll = div_round(gclk * divs[gclkdiv].den, divs[gclkdiv].num);
	mclk = div_round(syspll * divs[mclkdiv].num, divs[mclkdiv].den);
	wclk = div_round(syspll * divs[wclkdiv].num, divs[wclkdiv].den);

	if (gclksel != 1)
		gclk = clkspeed[gclksel];
	if (mclksel != 1)
		mclk = clkspeed[mclksel];
	if (wclksel != 1)
		wclk = clkspeed[wclksel];

	pr_debug("%s:\n"
	       " SYSPLL = %u kHz\n"
	       "   GCLK = %u kHz (sel=%s, div=%s dcyc=%s)\n"
	       "   MCLK = %u kHz (sel=%s, div=%s dcyc=%s)\n"
	       "   WCLK = %u kHz (sel=%s, div=%s dcyc=%s)\n",
	       name, syspll,
	       gclk, clksel[gclksel], clkdiv[gclkdiv], clkdcyc[gclkdcyc],
	       mclk, clksel[mclksel], clkdiv[mclkdiv], clkdcyc[mclkdcyc],
	       wclk, clksel[wclksel], clkdiv[wclkdiv], clkdcyc[wclkdcyc]);
}

static void parse_pins4_g400(const u8 *pins, struct mga_pins4 *p)
{
	memset(p, 0, sizeof *p);

	pins4_clk(pins[39], &p->pixpll.fvco_max, 230000);
	pins4_clk(pins[38], &p->syspll.fvco_max, p->pixpll.fvco_max);

	pins4_clk(pins[40], &p->crtc1.pixclk_8_max,  p->pixpll.fvco_max);
	pins4_clk(pins[41], &p->crtc1.pixclk_16_max, p->crtc1.pixclk_8_max);
	pins4_clk(pins[42], &p->crtc1.pixclk_24_max, p->crtc1.pixclk_16_max);
	pins4_clk(pins[43], &p->crtc1.pixclk_32_max, p->crtc1.pixclk_24_max);

	pins4_clk(pins[44], &p->crtc2.pixclk_16_max, p->pixpll.fvco_max);
	pins4_clk(pins[45], &p->crtc2.pixclk_32_max, p->crtc2.pixclk_16_max);

	p->option = (((pins[53] & 0x80) << 15) |
		     ((pins[53] & 0x40) << 22) |
		     ((pins[53] & 0x38) <<  7));

	pins2_clk(pins[55], 0, &p->clk_vga.gclk, 80000);
	p->clk_vga.option3  = get_le32(pins, 57);
	p->clk_vga.mctlwtst = get_le32(pins, 61);

	pins2_clk(pins[65], 0, &p->clk_2d.gclk, 80000);
	p->clk_2d.option3   = get_le32(pins, 67);
	p->clk_2d.mctlwtst  = get_le32(pins, 71);

	pins2_clk(pins[75], 0, &p->clk_3d.gclk, 80000);
	p->clk_3d.option3   = get_le32(pins, 77);
	p->clk_3d.mctlwtst  = get_le32(pins, 81);

	p->memrdbk     = (((pins[87] & 0xf0) << 21) |
			  ((pins[87] & 0x03) << 22) |
			  ((pins[86] & 0xf0) <<  1) |
			  ((pins[86] & 0x0f) <<  0));

	p->memrdbk_mod = (((pins[89] & 0xf0) << 21) |
			  ((pins[89] & 0x03) << 22) |
			  ((pins[88] & 0xf0) <<  1) |
			  ((pins[88] & 0x0f) <<  0));

	if (!(pins[91] & 0x01))
		p->features |= _MGA_FEAT_TVO_BUILTIN;
	if (!(pins[91] & 0x02))
		p->features |= _MGA_FEAT_DVD_BUILTIN;
	if (!(pins[91] & 0x04))
		p->features |= _MGA_FEAT_MJPEG_BUILTIN;
	if (!(pins[91] & 0x08))
		p->features |= _MGA_FEAT_VIN_BUILTIN;
	if (!(pins[91] & 0x10))
		p->features |= _MGA_FEAT_TUNER_BUILTIN;
	if (!(pins[91] & 0x20))
		p->features |= _MGA_FEAT_AUDIO_BUILTIN;
	if (!(pins[91] & 0x40))
		p->features |= _MGA_FEAT_TMDS_BUILTIN;

	if (pins[92] & 0x01)
		p->fref = 14318;
	else
		p->fref = 27000;

	p->mem.base_size = 0x400000 << ((pins[92] & 0x0c) >> 2);

	if (pins[92] & 0x10)
		p->mem.sdram = true;
	else
		p->option |= MGA_OPTION_HARDPWMSK;
}

static void print_pins4_g400(const struct mga_pins4 *p)
{
	pr_debug("\nPInS 4.0 (G400)\n"
	       "          base mem = %u MB\n"
	       "              Fref = %u kHz\n"
	       "   SYSPLL Fvco max = %u kHz\n"
	       "   PIXPLL Fvco max = %u kHz\n"
	       "CRTC1 pixclk  8_max = %u kHz\n"
	       "CRTC1 pixclk 16_max = %u kHz\n"
	       "CRTC1 pixclk 24_max = %u kHz\n"
	       "CRTC1 pixclk 32_max = %u kHz\n"
	       "CRTC2 pixclk 16_max = %u kHz\n"
	       "CRTC2 pixclk 32_max = %u kHz\n"
	       "            OPTION = %08x\n"
	       "           MEMRDBK = %08x\n"
	       "       MEMRDBK mod = %08x\n"
	       "  clk VGA     GCLK = %u kHz\n"
	       "  clk VGA  OPTION3 = %08x\n"
	       "  clk VGA MCTLWTST = %08x\n"
	       "  clk 2D      GCLK = %u kHz\n"
	       "  clk 2D   OPTION3 = %08x\n"
	       "  clk 2D  MCTLWTST = %08x\n"
	       "  clk 3D      GCLK = %u kHz\n"
	       "  clk 3D   OPTION3 = %08x\n"
	       "  clk 3D  MCTLWTST = %08x\n"
	       "          Features = %s\n",
	       p->mem.base_size >> 20,
	       p->fref,
	       p->syspll.fvco_max,
	       p->pixpll.fvco_max,
	       p->crtc1.pixclk_8_max,
	       p->crtc1.pixclk_16_max,
	       p->crtc1.pixclk_24_max,
	       p->crtc1.pixclk_32_max,
	       p->crtc2.pixclk_16_max,
	       p->crtc2.pixclk_32_max,
	       p->option,
	       p->memrdbk,
	       p->memrdbk_mod,
	       p->clk_vga.gclk,
	       p->clk_vga.option3,
	       p->clk_vga.mctlwtst,
	       p->clk_2d.gclk,
	       p->clk_2d.option3,
	       p->clk_2d.mctlwtst,
	       p->clk_3d.gclk,
	       p->clk_3d.option3,
	       p->clk_3d.mctlwtst,
	       print_features(p->features));

	print_g400_option("OPTION", p->option);
	print_g400_memrdbk("MEMRDBK", p->memrdbk);
	print_g400_memrdbk("MEMRDBK module", p->memrdbk_mod);
	print_g400_option3("VGA/OPTION3", p->clk_vga.option3);
	print_g400_mctlwtst("VGA/MCTLWTST", p->clk_vga.mctlwtst);
	print_g400_option3("2D/OPTION3", p->clk_2d.option3);
	print_g400_mctlwtst("2D/MCTLWTST", p->clk_2d.mctlwtst);
	print_g400_option3("3D/OPTION3", p->clk_3d.option3);
	print_g400_mctlwtst("3D/MCTLWTST", p->clk_3d.mctlwtst);

	print_g400_clocks("VGA clocks", p->clk_vga.gclk, p->clk_vga.option3);
	print_g400_clocks("2D clocks", p->clk_2d.gclk, p->clk_2d.option3);
	print_g400_clocks("3D clocks", p->clk_3d.gclk, p->clk_3d.option3);
}

static void print_g200_option(const char *name, u32 value)
{
	static const struct reg_fields option_fields[] = {
		{  0, 2, " sysclksl" },
		{  2, 1, "sysclkdis" },
		{  3, 1, "  gclkdiv" },
		{  4, 1, "  mclkdiv" },
		{  5, 1, "syspllpdn" },
		{  6, 1, "   pllsel" },
		{  8, 1, "  vgaioen" },
		{ 12, 3, "memconfig" },
		{ 14, 1, "hardpwmsk" },
		{ 15, 6, "   rfhcnt" },
		{ 22, 1, "enhmemacc" },
		{ 29, 1, "  noretry" },
		{ 30, 1, "   biosen" },
		{ 31, 1, "  powerpc" },
	};
	print_reg(name, value, option_fields, ARRAY_SIZE(option_fields));
}

static void print_g200_option2(const char *name, u32 value)
{
	static const struct reg_fields option2_fields[] = {
		{  8, 1, " eepromwt" },
		{ 12, 2, " mbuftype" },
		{ 14, 1, "nogclkdiv" },
		{ 15, 1, "nomclkdiv" },
		{ 16, 1, "nowclkdiv" },
		{ 17, 1, "  wclkdiv" },
		{ 19, 3, "  modclkp" },
	};
	print_reg(name, value, option2_fields, ARRAY_SIZE(option2_fields));
}

static void print_g200_mctlwtst(const char *name, u32 value)
{
	static const struct reg_fields mctlwtst_fields[] = {
		{  0, 3, "casltncy" },
		{  4, 2, "rrddelay" },
		{  7, 2, "rcddelay" },
		{ 10, 3, "  rasmin" },
		{ 14, 2, " rpdelay" },
		{ 18, 2, " wrdelay" },
		{ 21, 1, " rddelay" },
		{ 23, 2, "smrdelay" },
		{ 26, 2, "bwcdelay" },
		{ 29, 3, "bpldelay" },
	};
	print_reg(name, value, mctlwtst_fields, ARRAY_SIZE(mctlwtst_fields));
}

static void print_g200_memrdbk(const char *name, u32 value)
{
	static const struct reg_fields memrdbk_fields[] = {
		{  0, 4, "mclkbrd0" },
		{  5, 4, "mclkbrd1" },
		{ 22, 2, "strmfctl" },
		{ 25, 4, "mrsopcod" },
	};
	print_reg(name, value, memrdbk_fields, ARRAY_SIZE(memrdbk_fields));
}

static void print_g200_clocks(const char *name, unsigned int gclk, u32 option, u32 option2)
{
	const char *gclkdiv;
	const char *mclkdiv;
	const char *wclkdiv;
	static const char *clksel[] = {
		"PCI",
		"SYSPLL",
		"MCLK pin",
		"Reserved",
	};

	unsigned int syspll, mclk, wclk;

	u8 sysclksl = option & MGA_OPTION_SYSCLKSL;

	if (option2 & MGA_OPTION2_NOGCLKDIV) {
		gclkdiv = "1/1";
		syspll = div_round(gclk * 1, 1);
	} else if (option & MGA_OPTION_GCLKDIV) {
		gclkdiv = "2/3";
		syspll = div_round(gclk * 3, 2);
	} else {
		gclkdiv = "1/2";
		syspll = div_round(gclk * 2, 1);
	}

	if (option2 & MGA_OPTION2_NOMCLKDIV) {
		mclkdiv = "1/1";
		mclk = div_round(syspll * 1, 1);
	} else if (option & MGA_OPTION_MCLKDIV) {
		mclkdiv = "2/3";
		mclk = div_round(syspll * 2, 3);
	} else {
		mclkdiv = "1/2";
		mclk = div_round(syspll * 1, 2);
	}

	if (option2 & MGA_OPTION2_NOWCLKDIV) {
		wclkdiv = "1/1";
		wclk = div_round(syspll * 1, 1);
	} else if (option2 & MGA_OPTION2_WCLKDIV) {
		wclkdiv = "2/3";
		wclk = div_round(syspll * 2, 3);
	} else {
		wclkdiv = "1/2";
		wclk = div_round(syspll * 1, 2);
	}

	pr_debug("%s:\n"
	       " SYSPLL = %u kHz\n"
	       "   GCLK = %u kHz (sel=%s div=%s)\n"
	       "   MCLK = %u kHz (sel=%s div=%s)\n"
	       "   WCLK = %u kHz (sel=%s div=%s)\n",
	       name, syspll,
	       gclk, clksel[sysclksl], gclkdiv,
	       mclk, clksel[sysclksl], mclkdiv,
	       wclk, clksel[sysclksl], wclkdiv);
}

static void parse_pins3_g200(const u8 *pins, struct mga_pins3 *p)
{
	u32 option = 0, option2 = 0;

	memset(p, 0, sizeof *p);

	option = MGA_OPTION_SYSCLKSL_SYSPLL;

	pins2_clk(pins[36], 100, &p->fvco_max, 230000);

	pins2_clk(pins[37], 100, &p->crtc1.pixclk_8_max,  p->fvco_max);
	pins2_clk(pins[38], 100, &p->crtc1.pixclk_16_max, p->crtc1.pixclk_8_max);
	pins2_clk(pins[39], 100, &p->crtc1.pixclk_24_max, p->crtc1.pixclk_16_max);
	pins2_clk(pins[40], 100, &p->crtc1.pixclk_32_max, p->crtc1.pixclk_24_max);

	pins2_clk(pins[43], 0, &p->clk_vga.gclk[0 >> 1], 60000);
	pins2_clk(pins[44], 0, &p->clk_2d.gclk[0 >> 1], 60000);
	pins2_clk(pins[45], 0, &p->clk_2d.gclk[2 >> 1], p->clk_2d.gclk[0 >> 1]);
	pins2_clk(pins[46], 0, &p->clk_2d.gclk[4 >> 1], p->clk_2d.gclk[2 >> 1]);
	pins2_clk(pins[47], 0, &p->clk_2d.gclk[8 >> 1], p->clk_2d.gclk[4 >> 1]);

	p->mctlwtst = get_le32(pins, 48);

	/* gclkdiv, mclkdiv */
	p->clk_2d.option |= (((pins[52] & 0x03) << 3));

	if (pins[52] & 0x08)
		option |= MGA_OPTION_PLLSEL;

	if (pins[52] & 0x10)
		p->mem.sdram = true;
	else
		option |= MGA_OPTION_HARDPWMSK;

	if (pins[52] & 0x20)
		p->fref = 14318;
	else
		p->fref = 27000;

	/* memconfig */
	option |= (pins[54] & 0x07) << 10;

	p->mem.base_size = 0x200000 << ((pins[55] & 0xc0) >> 6);

	p->memrdbk  = (((pins[57] & 0xf0) << 21) |
		       ((pins[57] & 0x03) << 22) |
		       ((pins[56] & 0xf0) <<  1) |
		       ((pins[56] & 0x0f) <<  0));

	/* mbuftype */
	option2 |= (pins[58] & 0x03) << 12;

	/* nogclkdiv, nomclkdiv, nowclkdiv, wclkdiv */
	p->clk_2d.option2 |= (pins[58] & 0x3c) << 12;

	if (!(pins[59] & 0x01))
		p->features |= _MGA_FEAT_TVO_BUILTIN;
	if (!(pins[59] & 0x02))
		p->features |= _MGA_FEAT_DVD_BUILTIN;
	if (!(pins[59] & 0x04))
		p->features |= _MGA_FEAT_MJPEG_BUILTIN;
	if (!(pins[59] & 0x08))
		p->features |= _MGA_FEAT_VIN_BUILTIN;
	if (!(pins[59] & 0x10))
		p->features |= _MGA_FEAT_TUNER_BUILTIN;
	if (!(pins[59] & 0x20))
		p->features |= _MGA_FEAT_AUDIO_BUILTIN;
	if (!(pins[59] & 0x40))
		p->features |= _MGA_FEAT_TMDS_BUILTIN;

	pins2_clk(pins[60], 0, &p->clk_3d.gclk[0 >> 1], p->clk_2d.gclk[0 >> 1]);

	/* nogclkdiv, nomclkdiv, nowclkdiv, wclkdiv */
	p->clk_3d.option2 |= (pins[61] & 0x3c) << 12;

	/* gclkdiv, mclkdiv */
	p->clk_3d.option |= (((pins[61] & 0x80) >> 4) |
			     ((pins[61] & 0x40) >> 2));

	p->clk_3d.gclk[4 >> 1] = p->clk_3d.gclk[0 >> 1];
	p->clk_3d.gclk[4 >> 1] -= ((0x0f - (pins[62] & 0x0f)) << 1) * 1000;
	p->clk_3d.gclk[8 >> 1] = p->clk_3d.gclk[0 >> 1];
	p->clk_3d.gclk[8 >> 1] -= ((0xf0 - (pins[62] & 0xf0)) >> 3) * 1000;

	p->clk_2d.option |= option;
	p->clk_2d.option2 |= option2;

	p->clk_3d.option |= option;
	p->clk_3d.option2 |= option2;

	p->clk_vga.option = p->clk_2d.option;
	p->clk_vga.option2 = p->clk_2d.option2;

	/* 3D 2MiB clock not included, duplicate the 0MiB entry */
	p->clk_3d.gclk[2 >> 1] = p->clk_3d.gclk[0 >> 1];
}

static void print_pins3_g200(const struct mga_pins3 *p)
{
	pr_debug("\nPInS 3.0 (G200)\n"
	       "          base mem = %u MB\n"
	       "              Fref = %u kHz\n"
	       "          Fvco max = %u kHz\n"
	       "CRTC1 pixclk 8_max  = %u kHz\n"
	       "CRTC1 pixclk 16_max = %u kHz\n"
	       "CRTC1 pixclk 24_max = %u kHz\n"
	       "CRTC1 pixclk 32_max = %u kHz\n"
	       " clk VGA GCLK      = %u kHz\n"
	       " clk 2D  GCLK base = %u kHz\n"
	       " clk 2D  GCLK  2MB = %u kHz\n"
	       " clk 2D  GCLK  4MB = %u kHz\n"
	       " clk 2D  GCLK  8MB = %u kHz\n"
	       " clk 3D  GCLK base = %u kHz\n"
	       " clk 3D  GCLK  4MB = %u kHz\n"
	       " clk 3D  GCLK  8MB = %u kHz\n"
	       "          MCTLWTST = %08x\n"
	       "           MEMRDBK = %08x\n"
	       "   clk VGA  OPTION = %08x\n"
	       "   clk VGA OPTION2 = %08x\n"
	       "    clk 2D  OPTION = %08x\n"
	       "    clk 2D OPTION2 = %08x\n"
	       "    clk 3D  OPTION = %08x\n"
	       "    clk 3D OPTION2 = %08x\n"
	       "          Features = %s\n",
	       p->mem.base_size >> 20,
	       p->fref,
	       p->fvco_max,
	       p->crtc1.pixclk_8_max,
	       p->crtc1.pixclk_16_max,
	       p->crtc1.pixclk_24_max,
	       p->crtc1.pixclk_32_max,
	       p->clk_vga.gclk[0 >> 1],
	       p->clk_2d.gclk[0 >> 1],
	       p->clk_2d.gclk[2 >> 1],
	       p->clk_2d.gclk[4 >> 1],
	       p->clk_2d.gclk[8 >> 1],
	       p->clk_3d.gclk[0 >> 1],
	       p->clk_3d.gclk[4 >> 1],
	       p->clk_3d.gclk[8 >> 1],
	       p->mctlwtst,
	       p->memrdbk,
	       p->clk_vga.option,
	       p->clk_vga.option2,
	       p->clk_2d.option,
	       p->clk_2d.option2,
	       p->clk_3d.option,
	       p->clk_3d.option2,
	       print_features(p->features));

	print_g200_mctlwtst("MCTLWTST", p->mctlwtst);
	print_g200_memrdbk("MEMRDBK", p->memrdbk);
	print_g200_option("VGA OPTION", p->clk_vga.option);
	print_g200_option2("VGA OPTION2", p->clk_vga.option2);
	print_g200_option("2D OPTION", p->clk_2d.option);
	print_g200_option2("2D OPTION2", p->clk_2d.option2);
	print_g200_option("3D OPTION", p->clk_3d.option);
	print_g200_option2("3D OPTION2", p->clk_3d.option2);

	print_g200_clocks("VGA clocks", p->clk_vga.gclk[0 >> 1],
			  p->clk_vga.option, p->clk_vga.option2);

	print_g200_clocks("2D/Base clocks", p->clk_2d.gclk[0 >> 1],
			  p->clk_2d.option, p->clk_2d.option2);
	print_g200_clocks("2D/2MB clocks", p->clk_2d.gclk[2 >> 1],
			  p->clk_2d.option, p->clk_2d.option2);
	print_g200_clocks("2D/4MB clocks", p->clk_2d.gclk[4 >> 1],
			  p->clk_2d.option, p->clk_2d.option2);
	print_g200_clocks("2D/8MB clocks", p->clk_2d.gclk[8 >> 1],
			  p->clk_2d.option, p->clk_2d.option2);

	print_g200_clocks("3D/Base clocks", p->clk_3d.gclk[0 >> 1],
			  p->clk_3d.option, p->clk_3d.option2);
	print_g200_clocks("3D/4MB clocks", p->clk_3d.gclk[4 >> 1],
			  p->clk_3d.option, p->clk_3d.option2);
	print_g200_clocks("3D/8MB clocks", p->clk_3d.gclk[8 >> 1],
			  p->clk_3d.option, p->clk_3d.option2);
}

static void print_g100_option(const char *name, u32 value)
{
	static const struct reg_fields option_fields[] = {
		{  0, 2, " sysclksl" },
		{  2, 1, "sysclkdis" },
		{  3, 1, "  gclkdiv" },
		{  4, 1, "  mclkdiv" },
		{  5, 1, "syspllpdn" },
		{  6, 1, "   pllsel" },
		{  7, 1, " fmclkdiv" },
		{  8, 1, "  vgaioen" },
		{ 12, 1, "memconfig" },
		{ 13, 1, "splitmode" },
		{ 15, 6, "   rfhcnt" },
		{ 22, 1, "mrmoption" },
		{ 29, 1, "  noretry" },
		{ 30, 1, "   biosen" },
		{ 31, 1, "  powerpc" },
	};
	print_reg(name, value, option_fields, ARRAY_SIZE(option_fields));
}

static void print_g100_option2(const char *name, u32 value)
{
	static const struct reg_fields option2_fields[] = {
		{  0, 4, " memrclkd" },
		{  8, 1, " eepromwt" },
		{ 12, 2, " mbuftype" },
	};
	print_reg(name, value, option2_fields, ARRAY_SIZE(option2_fields));
}

static void print_g100_mctlwtst(const char *name, u32 value)
{
	static const struct reg_fields mctlwtst_fields[] = {
		{  0, 2, "casltncy" },
		{  4, 2, "rrddelay" },
		{  8, 2, " rcdelay" },
		{ 10, 2, "bwcdelay" },
		{ 13, 3, "bpldelay" },
		{ 16, 3, "  rasmin" },
		{ 21, 1, " rddelay" },
		{ 24, 2, " rpdelay" },
	};
	print_reg(name, value, mctlwtst_fields, ARRAY_SIZE(mctlwtst_fields));
}

static void print_g100_clocks(const char *name, unsigned int gclk, u32 option)
{
	const char *gclkdiv;
	const char *mclkdiv;
	const char *fmclkdiv;
	static const char *clksel[] = {
		"PCI",
		"SYSPLL",
		"MCLK pin",
		"Reserved",
	};

	unsigned int syspll, mclk, fmclk;

	u8 sysclksl = option & MGA_OPTION_SYSCLKSL;

	if (option & MGA_OPTION_GCLKDIV) {
		gclkdiv = "1/3";
		syspll = gclk * 3;
	} else {
		gclkdiv = "1/2";
		syspll = gclk * 2;
	}

	if (option & MGA_OPTION_MCLKDIV) {
		mclkdiv = "1/1";
		mclk = div_round(syspll, 1);
	} else {
		mclkdiv = "1/2";
		mclk = div_round(syspll, 2);
	}

	if (option & MGA_OPTION_FMCLKDIV) {
		fmclkdiv = "1/2";
		fmclk = div_round(syspll, 2);
	} else {
		fmclkdiv = "1/1";
		fmclk = div_round(syspll, 1);
	}

	pr_debug("%s:\n"
	       " SYSPLL = %u kHz\n"
	       "   GCLK = %u kHz (sel=%s div=%s)\n"
	       "   MCLK = %u kHz (sel=%s div=%s)\n"
	       "  FMCLK = %u kHz (sel=%s div=%s)\n",
	       name, syspll,
	       gclk, clksel[sysclksl], gclkdiv,
	       mclk, clksel[sysclksl], mclkdiv,
	       fmclk, clksel[sysclksl], fmclkdiv);
}

static void parse_pins3_g100(const u8 *pins, struct mga_pins3 *p)
{
	u32 option = 0, option2 = 0;

	memset(p, 0, sizeof *p);

	option = MGA_OPTION_SYSCLKSL_SYSPLL;

	pins2_clk(pins[36], 100, &p->fvco_max, 230000);
	pins2_clk(pins[37], 100, &p->crtc1.pixclk_8_max,  p->fvco_max);
	pins2_clk(pins[38], 100, &p->crtc1.pixclk_16_max, p->crtc1.pixclk_8_max);
	pins2_clk(pins[39], 100, &p->crtc1.pixclk_24_max, p->crtc1.pixclk_16_max);
	pins2_clk(pins[40], 100, &p->crtc1.pixclk_32_max, p->crtc1.pixclk_24_max);

	pins2_clk(pins[43], 0, &p->clk_vga.gclk[0 >> 1], 60000);
	pins2_clk(pins[44], 0, &p->clk_2d.gclk[0 >> 1], 60000);
	pins2_clk(pins[45], 0, &p->clk_2d.gclk[2 >> 1], p->clk_2d.gclk[0 >> 1]);
	pins2_clk(pins[46], 0, &p->clk_2d.gclk[4 >> 1], p->clk_2d.gclk[2 >> 1]);
	pins2_clk(pins[47], 0, &p->clk_2d.gclk[8 >> 1], p->clk_2d.gclk[4 >> 1]);

	p->mctlwtst = get_le32(pins, 48);

	option |= (((pins[52] & 0x4) << 5) |
		   ((pins[52] & 0x3) << 3));

	if (pins[52] & 0x08)
		option |= MGA_OPTION_PLLSEL;
	if (pins[52] & 0x10)
		p->mem.sdram = true;
	if (pins[52] & 0x20)
		p->fref = 14318;
	else
		p->fref = 27000;

	/* mbuftype */
	option2 |= (pins[54] & 0x03) << 12;
	/* memconfig */
	option |= (pins[54] & 0x04) << 10;
	/* memrclkd */
	option2 |= (pins[54] & 0xf0) >> 4;

	p->clk_vga.crtcext6 = pins[55] & 0x07;
	p->clk_2d.crtcext6 = (pins[55] & 0x38) >> 3;
	p->mem.base_size = 0x200000 << ((pins[55] & 0xc0) >> 6);

	if (!(pins[59] & 0x01))
		p->features |= _MGA_FEAT_TVO_BUILTIN;
	if (!(pins[59] & 0x02))
		p->features |= _MGA_FEAT_DVD_BUILTIN;
	if (!(pins[59] & 0x04))
		p->features |= _MGA_FEAT_MJPEG_BUILTIN;
	if (!(pins[59] & 0x08))
		p->features |= _MGA_FEAT_VIN_BUILTIN;
	if (!(pins[59] & 0x10))
		p->features |= _MGA_FEAT_TUNER_BUILTIN;
	if (!(pins[59] & 0x20))
		p->features |= _MGA_FEAT_AUDIO_BUILTIN;
	if (!(pins[59] & 0x40))
		p->features |= _MGA_FEAT_TMDS_BUILTIN;

	p->clk_vga.option = option;
	p->clk_vga.option2 = option2;
	p->clk_2d.option = option;
	p->clk_2d.option2 = option2;

	/* value from BIOS initialized card */
	p->vrefctrl = MGA_XVREFCTRL_SYSPLLBGPDN |
		MGA_XVREFCTRL_SYSPLLBGEN;
}

static void print_pins3_g100(const struct mga_pins3 *p)
{
	pr_debug("\nPInS 3.0 (G100)\n"
	       "          base mem = %u MB\n"
	       "              Fref = %u kHz\n"
	       "          Fvco max = %u kHz\n"
	       "CRTC1 pixclk 8_max  = %u kHz\n"
	       "CRTC1 pixclk 16_max = %u kHz\n"
	       "CRTC1 pixclk 24_max = %u kHz\n"
	       "CRTC1 pixclk 32_max = %u kHz\n"
	       " clk VGA GCLK      = %u kHz\n"
	       " clk 2D  GCLK base = %u kHz\n"
	       " clk 2D  GCLK  2MB = %u kHz\n"
	       " clk 2D  GCLK  4MB = %u kHz\n"
	       " clk 2D  GCLK  8MB = %u kHz\n"
	       " clk VGA  CRTCEXT6 = %02x\n"
	       " clk 2D   CRTCEXT6 = %02x\n"
	       " clk VGA    OPTION = %08x\n"
	       " clk VGA   OPTION2 = %08x\n"
	       " clk 2D     OPTION = %08x\n"
	       " clk 2D    OPTION2 = %08x\n"
	       "          MCTLWTST = %08x\n"
	       "          Features = %s\n",
	       p->mem.base_size >> 20,
	       p->fref,
	       p->fvco_max,
	       p->crtc1.pixclk_8_max,
	       p->crtc1.pixclk_16_max,
	       p->crtc1.pixclk_24_max,
	       p->crtc1.pixclk_32_max,
	       p->clk_vga.gclk[0 >> 1],
	       p->clk_2d.gclk[0 >> 1],
	       p->clk_2d.gclk[2 >> 1],
	       p->clk_2d.gclk[4 >> 1],
	       p->clk_2d.gclk[8 >> 1],
	       p->clk_vga.crtcext6,
	       p->clk_2d.crtcext6,
	       p->clk_vga.option,
	       p->clk_vga.option2,
	       p->clk_2d.option,
	       p->clk_2d.option2,
	       p->mctlwtst,
	       print_features(p->features));

	print_g100_mctlwtst("MCTLWTST", p->mctlwtst);
	print_g100_option("VGA OPTION", p->clk_vga.option);
	print_g100_option2("VGA OPTION2", p->clk_vga.option2);
	print_g100_option("2D OPTION", p->clk_2d.option);
	print_g100_option2("2D OPTION2", p->clk_2d.option2);

	print_g100_clocks("VGA clocks", p->clk_vga.gclk[0 >> 1], p->clk_vga.option);
	print_g100_clocks("Base clocks", p->clk_2d.gclk[0 >> 1], p->clk_2d.option);
	print_g100_clocks("2MB clocks", p->clk_2d.gclk[2 >> 1], p->clk_2d.option);
	print_g100_clocks("4MB clocks", p->clk_2d.gclk[4 >> 1], p->clk_2d.option);
	print_g100_clocks("8MB clocks", p->clk_2d.gclk[8 >> 1], p->clk_2d.option);
}

static void print_1064sg_option(const char *name, u32 value)
{
	static const struct reg_fields option_fields[] = {
		{  0, 2, " sysclksl" },
		{  2, 1, "sysclkdis" },
		{  3, 1, "  gclkdiv" },
		{  4, 1, "  mclkdiv" },
		{  5, 1, "syspllpdn" },
		{  8, 1, "  vgaioen" },
		{  9, 3, "   fbmskn" },
		{ 12, 1, "memconfig" },
		{ 13, 1, "splitmode" },
		{ 14, 1, "hardpwmsk" },
		{ 15, 1, "   unimem" },
		{ 16, 4, "   rfhcnt" },
		{ 20, 1, " eepromwt" },
		{ 24, 5, "productid" },
		{ 29, 1, "  noretry" },
		{ 30, 1, "   biosen" },
		{ 31, 1, "  powerpc" },
	};
	print_reg(name, value, option_fields, ARRAY_SIZE(option_fields));
}

static void print_1064sg_mctlwtst(const char *name, u32 value)
{
	static const struct reg_fields mctlwtst_fields[] = {
		{  0, 1, "casltncy" },
		{  8, 1, " rcdelay" },
		{ 16, 2, "  rasmin" },
	};
	print_reg(name, value, mctlwtst_fields, ARRAY_SIZE(mctlwtst_fields));
}

static void print_1064sg_clocks(const char *name, unsigned int gclk, u32 option)
{
	const char *gclkdiv;
	const char *mclkdiv;
	static const char *clksel[] = {
		"PCI",
		"SYSPLL",
		"MCLK pin",
		"Reserved",
	};

	unsigned int syspll, mclk;

	u8 sysclksl = option & MGA_OPTION_SYSCLKSL;

	if (option & MGA_OPTION_GCLKDIV) {
		gclkdiv = "1/1";
		syspll = gclk;
	} else {
		gclkdiv = "1/3";
		syspll = gclk * 3;
	}

	if (option & MGA_OPTION_MCLKDIV) {
		mclkdiv = "1/1";
		mclk = div_round(syspll, 1);
	} else {
		mclkdiv = "1/2";
		mclk = div_round(syspll, 2);
	}

	pr_debug("%s:\n"
	       " SYSPLL = %u kHz\n"
	       "   GCLK = %u kHz (sel=%s div=%s)\n"
	       "   MCLK = %u kHz (sel=%s div=%s)\n",
	       name, syspll,
	       gclk, clksel[sysclksl], gclkdiv,
	       mclk, clksel[sysclksl], mclkdiv);
}

static void parse_pins2_1064sg(const u8 *pins, struct mga_pins2 *p)
{
	memset(p, 0, sizeof *p);

	p->fref = 14318;
	p->option = MGA_OPTION_HARDPWMSK | MGA_OPTION_FBMSKN | MGA_OPTION_SYSCLKSL_SYSPLL;

	pins2_clk(pins[41], 100, &p->fvco_max, 170000);
	pins2_clk(pins[42], 100, &p->crtc1.pixclk_max, p->fvco_max);
	pins2_clk(pins[43], 0, &p->clk_2d.gclk[0 >> 2], 40000);

	pins2_clk(pins[45], 0, &p->clk_2d.gclk[4 >> 2], p->clk_2d.gclk[0 >> 2]);
	pins2_clk(pins[46], 0, &p->clk_2d.gclk[8 >> 2], p->clk_2d.gclk[4 >> 2]);
	pins2_clk(pins[47], 0, &p->clk_2d.gclk_mod, p->clk_2d.gclk[0 >> 2]);

	p->mctlwtst = (((pins[51] & 0x0c) << 14) |
		       ((pins[51] & 0x02) <<  7) |
		       ((pins[51] & 0x01) <<  0));

	pins2_clk(pins[53], 0, &p->clk_2d.gclk[12 >> 2], p->clk_2d.gclk[8 >> 2]);
	pins2_clk(pins[54], 0, &p->clk_2d.gclk[16 >> 2], p->clk_2d.gclk[12 >> 2]);
}

static void print_pins2_1064sg(const struct mga_pins2 *p)
{
	pr_debug("\nPInS 2.0 (1064SG)\n"
	       "            Fref = %u kHz\n"
	       "        Fvco max = %u kHz\n"
	       "CRTC1 pixclk max = %u kHz\n"
	       "       GCLK base = %u kHz\n"
	       "       GCLK  4MB = %u kHz\n"
	       "       GCLK  8MB = %u kHz\n"
	       "       GCLK 12MB = %u kHz\n"
	       "       GCLK 16MB = %u kHz\n"
	       "       GCLK  mod = %u kHz\n"
	       "        MCTLWTST = %08x\n"
	       "          OPTION = %08x\n",
	       p->fref,
	       p->fvco_max,
	       p->crtc1.pixclk_max,
	       p->clk_2d.gclk[0 >> 2],
	       p->clk_2d.gclk[4 >> 2],
	       p->clk_2d.gclk[8 >> 2],
	       p->clk_2d.gclk[12 >> 2],
	       p->clk_2d.gclk[16 >> 2],
	       p->clk_2d.gclk_mod,
	       p->mctlwtst,
	       p->option);

	print_1064sg_option("OPTION", p->option);
	print_1064sg_mctlwtst("MCTLWTST", p->mctlwtst);

	print_1064sg_clocks("Base clocks", p->clk_2d.gclk[0 >> 2], p->option);
	print_1064sg_clocks("4MB clocks", p->clk_2d.gclk[4 >> 2], p->option);
	print_1064sg_clocks("8MB clocks", p->clk_2d.gclk[8 >> 2], p->option);
	print_1064sg_clocks("12MB clocks", p->clk_2d.gclk[12 >> 2], p->option);
	print_1064sg_clocks("16MB clocks", p->clk_2d.gclk[16 >> 2], p->option);
	print_1064sg_clocks("Module clocks", p->clk_2d.gclk_mod, p->option);
}

static void print_2164w_option(const char *name, u32 value)
{
	static const struct reg_fields option_fields[] = {
		{  8, 1, "  vgaioen" },
		{ 12, 2, "memconfig" },
		{ 16, 4, "   rfhcnt" },
		{ 20, 1, " eepromwt" },
		{ 21, 1, " nogscale" },
		{ 24, 5, "productid" },
		{ 29, 1, "  noretry" },
		{ 30, 1, "   biosen" },
		{ 31, 1, "  powerpc" },
	};
	print_reg(name, value, option_fields, ARRAY_SIZE(option_fields));
}

static void print_2164w_clocks(const char *name, unsigned int gclk, u32 option)
{
	const char *gclkdiv;
	unsigned int pll;

	if (option & MGA_OPTION_NOGSCALE) {
		gclkdiv = "1/1";
		pll = gclk;
	} else {
		gclkdiv = "1/4";
		pll = gclk * 4;
	}

	pr_debug("%s:\n"
	       " PLL = %u kHz\n"
	       "GCLK = %u kHz (div=%s)\n",
	       name, pll, gclk, gclkdiv);
}

static void parse_pins2_2164w(const u8 *pins, struct mga_pins2 *p)
{
	memset(p, 0, sizeof *p);

	p->fref = 14318;
	p->option = MGA_OPTION_NOGSCALE;

	pins2_clk(pins[41], 100, &p->fvco_max, 220000);
	pins2_clk(pins[42], 100, &p->crtc1.pixclk_max, p->fvco_max);
	pins2_clk(pins[43], 0, &p->clk_2d.gclk[0 >> 2], 60000);
	pins2_clk(pins[44], 0, &p->ldclk_max, 85000);
	pins2_clk(pins[45], 0, &p->clk_2d.gclk[4 >> 2], p->clk_2d.gclk[0 >> 2]);
	pins2_clk(pins[46], 0, &p->clk_2d.gclk[8 >> 2], p->clk_2d.gclk[4 >> 2]);
	pins2_clk(pins[47], 0, &p->clk_2d.gclk_mod, p->clk_2d.gclk[0 >> 2]);

	pins2_clk(pins[53], 0, &p->clk_2d.gclk[12 >> 2], p->clk_2d.gclk[8 >> 2]);
	pins2_clk(pins[54], 0, &p->clk_2d.gclk[16 >> 2], p->clk_2d.gclk[12 >> 2]);
}

static void print_pins2_2164w(const struct mga_pins2 *p)
{
	pr_debug("\nPInS 2.0 (2164W)\n"
	       "            Fref = %u kHz\n"
	       "        Fvco max = %u kHz\n"
	       "CRTC1 pixclk max = %u kHz\n"
	       "       LDCLK max = %u kHz\n"
	       "       GCLK base = %u kHz\n"
	       "       GCLK  4MB = %u kHz\n"
	       "       GCLK  8MB = %u kHz\n"
	       "       GCLK 12MB = %u kHz\n"
	       "       GCLK 16MB = %u kHz\n"
	       "       GCLK mod  = %u kHz\n"
	       "          OPTION = %08x\n",
	       p->fref,
	       p->fvco_max,
	       p->crtc1.pixclk_max,
	       p->ldclk_max,
	       p->clk_2d.gclk[0 >> 2],
	       p->clk_2d.gclk[4 >> 2],
	       p->clk_2d.gclk[8 >> 2],
	       p->clk_2d.gclk[12 >> 2],
	       p->clk_2d.gclk[16 >> 2],
	       p->clk_2d.gclk_mod,
	       p->option);

	print_2164w_option("OPTION", p->option);

	print_2164w_clocks("Base clocks", p->clk_2d.gclk[0 >> 2], p->option);
	print_2164w_clocks("4MB clocks", p->clk_2d.gclk[4 >> 2], p->option);
	print_2164w_clocks("8MB clocks", p->clk_2d.gclk[8 >> 2], p->option);
	print_2164w_clocks("12MB clocks", p->clk_2d.gclk[12 >> 2], p->option);
	print_2164w_clocks("16MB clocks", p->clk_2d.gclk[16 >> 2], p->option);
	print_2164w_clocks("Module clocks", p->clk_2d.gclk_mod, p->option);
}

static void print_2064w_option(const char *name, u32 value)
{
	static const struct reg_fields option_fields[] = {
		{  8, 1, "   vgaioen" },
		{ 12, 1, "interleave" },
		{ 16, 4, "    rfhcnt" },
		{ 20, 1, "  eepromwt" },
		{ 21, 1, "  nogscale" },
		{ 24, 5, " productid" },
		{ 29, 1, "   noretry" },
		{ 30, 1, "    biosen" },
		{ 31, 1, "   powerpc" },
	};
	print_reg(name, value, option_fields, ARRAY_SIZE(option_fields));
}

static void print_2064w_clocks(const char *name, unsigned int gclk, u32 option)
{
	const char *gclkdiv;
	unsigned int pll;

	if (option & MGA_OPTION_NOGSCALE) {
		gclkdiv = "1/1";
		pll = gclk;
	} else {
		gclkdiv = "1/4";
		pll = gclk * 4;
	}

	pr_debug("%s:\n"
	       " PLL = %u kHz\n"
	       "GCLK = %u kHz (div=%s)\n",
	       name, pll, gclk, gclkdiv);
}

static void parse_pins1_2064w(const u8 *pins, struct mga_pins1 *p)
{
	static const unsigned int vco_max[] = {
		175000,
		220000,
		240000,
	};
	u8 vco_idx;

	memset(p, 0, sizeof *p);

	p->fref = 14318;
	p->option = MGA_OPTION_NOGSCALE;

	vco_idx = pins[22];
	if (vco_idx >= ARRAY_SIZE(vco_max))
		vco_idx = 0;
	p->fvco_max = vco_max[vco_idx];

	pins1_clk(get_le16(pins, 24), &p->crtc1.pixclk_max, p->fvco_max);
	pins1_clk(get_le16(pins, 26), &p->ldclk_max, 85000);
	pins1_clk(get_le16(pins, 28), &p->clk_2d.gclk[0 >> 2], 50000);
	pins1_clk(get_le16(pins, 30), &p->clk_2d.gclk[4 >> 2], p->clk_2d.gclk[0 >> 2]);
	pins1_clk(get_le16(pins, 32), &p->clk_2d.gclk[8 >> 2], p->clk_2d.gclk[4 >> 2]);
	pins1_clk(get_le16(pins, 34), &p->clk_2d.gclk_mod, p->clk_2d.gclk[0 >> 2]);

	pins1_clk(get_le16(pins, 54), &p->clk_vga.gclk[0 >> 2], p->clk_2d.gclk[0 >> 2]);
}

static void print_pins1_2064w(const struct mga_pins1 *p)
{
	pr_debug("\nPInS 1.0\n"
	       "            Fref = %u kHz\n"
	       "        Fvco max = %u kHz\n"
	       "CRTC1 pixclk 8max = %u kHz\n"
	       "       LDCLK max = %u kHz\n"
	       "       GCLK base = %u kHz\n"
	       "       GCLK  4MB = %u kHz\n"
	       "       GCLK  8MB = %u kHz\n"
	       "       GCLK  mod = %u kHz\n"
	       "       GCLK  VGA = %u kHz\n"
	       "          OPTION = %08x\n",
	       p->fref,
	       p->fvco_max,
	       p->crtc1.pixclk_max,
	       p->ldclk_max,
	       p->clk_2d.gclk[0 >> 2],
	       p->clk_2d.gclk[4 >> 2],
	       p->clk_2d.gclk[8 >> 2],
	       p->clk_2d.gclk_mod,
	       p->clk_vga.gclk[0 >> 2],
	       p->option);

	print_2064w_option("OPTION", p->option);

	print_2064w_clocks("Base clocks", p->clk_2d.gclk[0 >> 2], p->option);
	print_2064w_clocks("4MB clocks", p->clk_2d.gclk[4 >> 2], p->option);
	print_2064w_clocks("8MB clocks", p->clk_2d.gclk[8 >> 2], p->option);
	print_2064w_clocks("Module clocks", p->clk_2d.gclk_mod, p->option);
	print_2064w_clocks("VGA clocks", p->clk_vga.gclk[0 >> 2], p->option);
}
