/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef KERNEL_EMUL_H
#define KERNEL_EMUL_H

#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <pthread.h>

#include "mga_kernel_api.h"

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

enum irqreturn {
	IRQ_NONE = 0,
	IRQ_HANDLED = 1,
};
typedef enum irqreturn irqreturn_t;
#define IRQ_RETVAL(x)   ((x) != IRQ_NONE)

typedef irqreturn_t (*irq_handler_t)(int, void *);

#define PAGE_SIZE 4096
#define HZ 1000

#define ERESTARTSYS 512
#define signal_pending(task) false

#define ERR_PTR(err) ((void *)(long)(err))
#define PTR_ERR(ptr) ((int)(long)(ptr))
#define IS_ERR(ptr) \
({ \
	pr_debug("IS_ERR(%lx) = %d\n", (unsigned long)(ptr),(unsigned int)(unsigned long)(ptr) >= 0xc0000000); \
	((unsigned int)(unsigned long)(ptr) >= 0xc0000000);		\
})

#define swap(a,b)				\
	do {					\
		__typeof(a) _a = (a);		\
		__typeof(a) _b = (b);		\
		(a) = _b;			\
		(b) = _a;			\
	} while (0)


#define container_of(ptr, type, member) ({                   \
	const typeof(((type *)NULL)->member) *__ptr = (ptr); \
	(type*)((char*)__ptr - offsetof(type, member));      \
})

#define __ALIGN_MASK(x,mask)   (((x)+(mask))&~(mask))
#define ALIGN(x,a) __ALIGN_MASK(x,(typeof(x))(a)-1)
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a)[0])
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))
#define clamp(x,min,max) ((x) < (min) ? (min) : (x) > (max) ? (max) : (x))

typedef struct __wait_queue_head wait_queue_head_t;

struct __wait_queue_head {
	pthread_mutex_t mutex;
	pthread_cond_t cond;
};

static inline u64 div_u64(u64 dividend, u32 divisor)
{
	return dividend / divisor;
}

#define BUG() assert(0)
#define BUG_ON(x) assert(!(x))

#define __iomem
#define __user

unsigned long get_jiffies(void);

#define jiffies get_jiffies()

unsigned long usecs_to_jiffies(const unsigned int u);

unsigned long msecs_to_jiffies(const unsigned int m);
unsigned int jiffies_to_msecs(const unsigned long j);

#define time_after(a,b) ((long)(b) - (long)(a) < 0)
#define time_before(a,b) time_after(b,a)
#define time_after_eq(a,b) ((long)(a) - (long)(b) >= 0)
#define time_before_eq(a,b) time_after_eq(b,a)

void msleep(unsigned int msecs);
void udelay(unsigned int usecs);
void ndelay(unsigned int nsecs);

int fls(int x);
int ilog2(u32 n);

#define DIV_ROUND_UP(n, d) (((n) + (d) - 1) / (d))
#define DIV_ROUND_CLOSEST(n, d) (((n) + (d)/2)/(d))

#define roundup(x,a) (DIV_ROUND_UP(x,a) * (a))

unsigned int div_round(unsigned int n, unsigned int d);
static inline u64 div_round_u64(u64 n, u32 d)
{
	return (2 * n + d) / (2 * d);
}

#define KERN_EMERG ""
#define KERN_ALERT ""
#define KERN_CRIT ""
#define KERN_ERR ""
#define KERN_WARNING ""
#define KERN_NOTICE ""
#define KERN_INFO ""
#define KERN_DEBUG ""

#define KERN_CONT ""

void printk(const char *fmt, ...);

#ifndef pr_fmt
#define pr_fmt(fmt) fmt
#endif

#define pr_emerg(fmt, ...) \
	printk(KERN_EMERG pr_fmt(fmt), ##__VA_ARGS__)
#define pr_alert(fmt, ...) \
	printk(KERN_ALERT pr_fmt(fmt), ##__VA_ARGS__)
#define pr_crit(fmt, ...) \
	printk(KERN_CRIT pr_fmt(fmt), ##__VA_ARGS__)
#define pr_err(fmt, ...) \
	printk(KERN_ERR pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warning(fmt, ...) \
	printk(KERN_WARNING pr_fmt(fmt), ##__VA_ARGS__)
#define pr_notice(fmt, ...) \
	printk(KERN_NOTICE pr_fmt(fmt), ##__VA_ARGS__)
#define pr_info(fmt, ...) \
	printk(KERN_INFO pr_fmt(fmt), ##__VA_ARGS__)
#define pr_cont(fmt, ...) \
	printk(KERN_CONT fmt, ##__VA_ARGS__)
#define pr_debug(fmt, ...) \
	printk(KERN_DEBUG fmt, ##__VA_ARGS__)

#include "device.h"

typedef unsigned long gfp_t;
enum {
	GFP_KERNEL = 0x1,
};
void *kmalloc(size_t size, gfp_t flags);
void *kzalloc(size_t size, gfp_t flags);
void *kcalloc(size_t n, size_t size, gfp_t flags);
void *kmemdup(void *ptr, size_t size, gfp_t flags);
void kfree(void *ptr);

u8   readb(void __iomem *addr);
u16  readw(void __iomem *addr);
u32  readl(void __iomem *addr);
void writeb(u8 val, void __iomem *addr);
void writew(u16 val, void __iomem *addr);
void writel(u32 val, void __iomem *addr);

struct pci_bus {
	u32 domain;
	u32 number;
};

#define pci_domain_nr(bus) ((bus)->domain)

struct pci_dev {
	int fd;
	u32 devfn;
	u16 vendor;
	u16 device;
	u16 sub_vendor;
	u16 sub_device;
	struct device dev;
	struct pci_bus pci_bus;
	struct pci_bus *bus;
	u32 class;
	u8 revision;
	u32 saved[16];
};

int  pci_cfg_open(struct pci_dev *pdev, int fd);
void pci_cfg_close(struct pci_dev *pdev);
u32  pci_cfg_read32(struct pci_dev *pdev, u32 addr);
void pci_cfg_write32(struct pci_dev *pdev, u32 addr, u32 value);
u8  pci_cfg_read8(struct pci_dev *pdev, u32 addr);
void pci_cfg_write8(struct pci_dev *pdev, u32 addr, u8 value);
void pci_cfg_save(struct pci_dev *pdev);
void pci_cfg_restore(struct pci_dev *pdev);

void timespecadd(const struct timespec *a,
		 const struct timespec *b,
		 struct timespec *result);
void timespecsub(const struct timespec *a,
		 const struct timespec *b,
		 struct timespec *result);

int request_irq(unsigned int irq, irq_handler_t handler, unsigned long flags,
		const char *name, void *dev);
void free_irq(unsigned int irq, void *dev);

struct mutex {
	pthread_mutex_t mutex;
};

void mutex_lock(struct mutex *mutex);
void mutex_unlock(struct mutex *mutex);
void mutex_init(struct mutex *mutex);

struct completion {
	bool complete;
	bool inited;
	wait_queue_head_t wait;
};

void init_completion(struct completion *completion);
void wait_for_completion(struct completion *completion);
int wait_for_completion_timeout(struct completion *completion, unsigned int timeout);
void complete(struct completion *completion);

void *memchr_inv(const void *start, int c, size_t n);

unsigned int hweight32(unsigned int x);

/* call this first, then you can use __user ptr for ioctl data */
void _ioctl_user_data(void __user *data, size_t start, size_t end);

int get_user_4(u32 *x, const u32 __user *ptr);
int get_user_8(u64 *x, const u64 __user *ptr);
int put_user_4(u32 x, u32 __user *ptr);
int put_user_8(u64 x, u64 __user *ptr);
int copy_from_user(void *dst, const void __user *src, size_t len);
int copy_to_user(void __user *dst, const void *src, size_t len);

#define get_user(x, ptr)				\
({							\
	int __ret = -EFAULT;				\
	switch (sizeof(*(ptr))) {			\
		u32 __x4;				\
		u64 __x8;				\
	case 4:						\
		__ret = get_user_4(&__x4, (const void __user *)(ptr));	\
		(x) = __x4;				\
		break;					\
	case 8:						\
		__ret = get_user_8(&__x8, (const void __user *)(ptr));	\
		(x) = __x8;				\
		break;					\
	default:					\
		break;					\
	}						\
	__ret;						\
})

#define put_user(x, ptr)				\
({							\
	int __ret = -EFAULT;				\
	switch (sizeof(*(ptr))) {			\
		u32 __x4;				\
		u64 __x8;				\
	case 4:						\
		__ret = put_user_4((x), (void __user *)(ptr));	\
		break;					\
	case 8:						\
		__ret = put_user_8((x), (void __user *)(ptr));	\
		break;					\
	default:					\
		break;					\
	}						\
	__ret;						\
})

#endif
