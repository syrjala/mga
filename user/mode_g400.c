/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "mga_regs.h"
#include "mga_kms.h"
#include "mga_dac.h"
#include "mga_flat.h"
#include "mga_dualhead.h"

struct mga_crtc_state1 {
	struct drm_framebuffer *fb;
	struct drm_display_mode mode;
	unsigned int pll;
	unsigned int outputs;
	unsigned int pixclk;
	int hscale;
	int vscale;
	int x;
	int y;
};

struct mga_mode_state1 {
	struct mga_crtc_state1 crtc1;
	struct mga_crtc_state1 crtc2;
};

static struct mga_mode_state1 mdev_crtcs;

static bool crtc_needs_tvo(const struct mga_dev *mdev,
			   unsigned int outputs)
{
	return outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT) ||
		(outputs & MGA_OUTPUT_TMDS1 &&
		 mdev->features & MGA_FEAT_TVO_BUILTIN);
}

static void dac_enable(struct mga_dev *mdev,
		       unsigned int crtc)
{
	mga_crtc2_dac_source(mdev, crtc);

	mga_dac_dac_power(mdev, true);
}

static void dac_disable(struct mga_dev *mdev)
{
	mga_dac_dac_power(mdev, false);

	mga_crtc2_dac_source(mdev, MGA_CRTC_NONE);
}

static void panel_enable(struct mga_dev *mdev,
			 unsigned int crtc)
{
	mga_dac_panel_source(mdev, crtc);

	/*
	 * Must always use the slave panellink mode because
	 * the flat panel addon's input clock comes via the
	 * VIDRST pin.
	 */
	mga_dac_panel_slave(mdev, true);

	mga_tmds_power(mdev, true);
}

static void panel_disable(struct mga_dev *mdev)
{
	mga_tmds_power(mdev, false);

	mga_dac_panel_slave(mdev, false);

	mga_dac_panel_source(mdev, MGA_CRTC_NONE);
}

static void tvo_enable(struct mga_dev *mdev, unsigned int crtc)
{
	if (mdev->features & MGA_FEAT_DUALHEAD_ADDON)
		mga_dualhead_addon_vidrst(mdev, true);

	mga_dac_tvo_source(mdev, crtc);
}

static void tvo_enable_bt656(struct mga_dev *mdev, unsigned int crtc)
{
}

static void tvo_disable(struct mga_dev *mdev)
{
	mga_tvo_disable(&mdev->tvodev);

	mga_dac_tvo_source(mdev, MGA_CRTC_NONE);

	if (mdev->features & MGA_FEAT_DUALHEAD_ADDON)
		mga_dualhead_addon_vidrst(mdev, false);
}

static void crtc1_disable(struct mga_dev *mdev)
{
	unsigned int outputs = mdev_crtcs.crtc1.outputs;

	if (!outputs)
		return;

	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);

	if (outputs & MGA_OUTPUT_DAC1)
		mga_crtc1_dpms(mdev, DRM_MODE_DPMS_OFF);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_OFF);

	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	mga_dac_crtc1_power(mdev, false);

	if (outputs & MGA_OUTPUT_DAC1)
		dac_disable(mdev);

	if (outputs & MGA_OUTPUT_TMDS1)
		panel_disable(mdev);

	if (crtc_needs_tvo(mdev, outputs))
		tvo_disable(mdev);

	mdev_crtcs.crtc1.pixclk = 0;
	mdev_crtcs.crtc1.outputs = 0;
	mdev_crtcs.crtc1.pll = 0;
	memset(&mdev_crtcs.crtc1.mode, 0, sizeof mdev_crtcs.crtc1.mode);
	mdev_crtcs.crtc1.fb = NULL;
}

static void crtc2_disable(struct mga_dev *mdev)
{
	unsigned int outputs = mdev_crtcs.crtc2.outputs;

	if (!outputs)
		return;

	if (crtc_needs_tvo(mdev, outputs))
		tvo_disable(mdev);

	mga_crtc2_wait_vblank(mdev);
	mga_crtc2_video(mdev, false);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_OFF);

	mga_crtc2_pixclk_enable(mdev, false);
	mga_crtc2_pixclk_select(mdev, MGA_PLL_NONE);

	if (outputs & MGA_OUTPUT_DAC1)
		dac_disable(mdev);

	if (outputs & MGA_OUTPUT_TMDS1)
		panel_disable(mdev);

	mdev_crtcs.crtc2.pixclk = 0;
	mdev_crtcs.crtc2.outputs = 0;
	mdev_crtcs.crtc2.pll = 0;
	memset(&mdev_crtcs.crtc2.mode, 0, sizeof mdev_crtcs.crtc2.mode);
	mdev_crtcs.crtc2.fb = NULL;
}

static void crtc1_enable(struct mga_dev *mdev,
			 const struct mga_mode_config *mc,
			 const struct mga_pll_settings *pll)
{
	const struct mga_plane_config *pc = &mc->crtc1.plane_config;
	const struct mga_crtc_config *cc = &mc->crtc1.crtc_config;
	const struct mga_crtc1_regs *regs = &mc->crtc1.regs;
	const struct drm_framebuffer *fb = pc->fb;
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	bool overlay = cc->overlay;
	unsigned int outputs = cc->outputs;
	unsigned int hzoom = 0x10000 / pc->hscale;
	/* FIXME sync from mode */
	unsigned int sync = DRM_MODE_FLAG_PHSYNC | DRM_MODE_FLAG_PVSYNC;

	mga_crtc1_restore(mdev, regs);
	mga_dac_crtc1_program(mdev, &mc->dac_config);
	mga_dac_crtc1_pixclk_select(mdev, pll->type);

	// FIXME no effect on g400?
	mga_dac_dac_set_sync(mdev, sync);//

	if (outputs & MGA_OUTPUT_DAC1) {
		mga_crtc1_set_sync(mdev, sync);
		mga_misc_set_sync(mdev, sync);
		dac_enable(mdev, MGA_CRTC_CRTC1);
	}

	if (outputs & MGA_OUTPUT_TMDS1) {
		mga_dac_panel_set_sync(mdev, sync);
		panel_enable(mdev, MGA_CRTC_CRTC1);

		if (mdev->features & MGA_FEAT_TVO_BUILTIN)
			mga_tvo_program_panel(&mdev->tvodev,
					      &mc->tvo_config,
					      mode, &pll->dac);
	}

	if (outputs & MGA_OUTPUT_DAC2) {
		if (mdev->features & MGA_FEAT_DUALHEAD_ADDON)
			mga_dualhead_addon_set_sync(mdev, sync);
		else
			mga_dualhead_builtin_set_sync(mdev, sync);
	}

	if (outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
		tvo_enable(mdev, MGA_CRTC_CRTC1);

	mga_dac_crtc1_power(mdev, true);
	mga_dac_crtc1_set_palette(mdev);
	mga_dac_crtc1_pixclk_enable(mdev, true);

	if (outputs & MGA_OUTPUT_DAC1)
		mga_crtc1_dpms(mdev, DRM_MODE_DPMS_ON);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_ON);

	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, true);

	if (outputs & MGA_OUTPUT_DAC2) {
		mga_tvo_program_crt(&mdev->tvodev,
				    &mc->tvo_config,
				    mode, &pll->dac);
	}

	if (outputs & MGA_OUTPUT_TVOUT) {
		mga_tvo_program_tvout(&mdev->tvodev,
				      &mc->tvo_config,
				      mode, &pll->dac);
	}
}

static void crtc2_enable(struct mga_dev *mdev,
			 const struct mga_mode_config *mc,
			 const struct mga_pll_settings *pll)
{
	const struct mga_plane_config *pc = &mc->crtc2.plane_config;
	const struct mga_crtc_config *cc = &mc->crtc2.crtc_config;
	const struct mga_crtc2_regs *regs = &mc->crtc2.regs;
	const struct drm_framebuffer *fb = pc->fb;
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	bool vidrst = cc->vidrst;
	bool bt656 = cc->bt656;
	unsigned int outputs = cc->outputs;
	/* FIXME sync from mode */
	unsigned int sync = DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_NVSYNC;

	mga_crtc2_restore(mdev, regs);
	mga_crtc2_pixclk_select(mdev, pll->type);

	if (outputs & MGA_OUTPUT_DAC1) {
		mga_crtc2_set_sync(mdev, sync);
		dac_enable(mdev, MGA_CRTC_CRTC2);
	}

	if (outputs & MGA_OUTPUT_TMDS1) {
		mga_dac_panel_set_sync(mdev, sync);
		panel_enable(mdev, MGA_CRTC_CRTC2);

		if (mdev->features & MGA_FEAT_TVO_BUILTIN)
			mga_tvo_program_panel(&mdev->tvodev,
					      &mc->tvo_config,
					      mode, &pll->dac);
	}

	if (outputs & MGA_OUTPUT_DAC2) {
		if (mdev->features & MGA_FEAT_DUALHEAD_ADDON)
			mga_dualhead_addon_set_sync(mdev, sync);
		else
			mga_dualhead_builtin_set_sync(mdev, sync);
	}

	if (outputs & MGA_OUTPUT_TVOUT)
		tvo_disable(mdev);

	if (cc->bt656)
		mga_dac_bt656_source(mdev, MGA_CRTC_CRTC2);

	if (outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT) && !cc->bt656)
		tvo_enable(mdev, MGA_CRTC_CRTC2);

	mga_crtc2_video(mdev, true);
	mga_crtc2_pixclk_enable(mdev, true);

	if (outputs & MGA_OUTPUT_TMDS1)
		mga_dac_panel_dpms(mdev, DRM_MODE_DPMS_ON);

	if (outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
		mga_tvo_program(&mdev->tvodev,
				&mc->tvo_config,
				mode, &pll->dac);

	if (cc->bt656) {
		mga_crtc2_wait_active_video(mdev);
		mga_crtc2_wait_active_video(mdev);
		mga_crtc2_restore_interlace(mdev, regs);
		mga_tvo_enable_bt656(&mdev->tvodev);
		mga_crtc2_wait_active_video(mdev);
		mga_crtc2_wait_active_video(mdev);
		mga_tvo_reset_mafc(&mdev->tvodev);
	}
}

static unsigned int mga_g400_select_pll(struct mga_dev *mdev,
					unsigned int outputs)
{
	unsigned int pll = MGA_PLL_NONE;

	if (outputs & MGA_OUTPUT_DAC1)
		pll = MGA_PLL_PIXPLL;

	if (outputs & MGA_OUTPUT_TMDS1) {
		/*
		 * TVO seems to always pull on VIDRST line,
		 * thus preventing AV9110 clocks from working.
		 */
		if (mdev->features & MGA_FEAT_TVO_BUILTIN)
			pll = MGA_PLL_TVO;
		else
			pll = MGA_PLL_AV9110;
	}

	/* use PIXPLL when there's only one CRTC active */
	if (outputs & MGA_OUTPUT_DAC1 && outputs & MGA_OUTPUT_TMDS1)
		pll = MGA_PLL_PIXPLL;

	/* always use TVO PLL when using TVO for output */
	if (outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
		pll = MGA_PLL_TVO;

	return pll;
}

static int mga_g400_pll_calc(struct mga_dev *mdev,
			     unsigned int outputs,
			     unsigned int pixclk,
			     struct mga_pll_settings *pll)
{
	/* FIXME CRTC/DAC max clock */
	switch (pll->type) {
	case MGA_PLL_NONE:
		return 0;
	case MGA_PLL_CRISTAL:
		/* FIXME */
		pll->fcristal = 27000;
		return 0;
	case MGA_PLL_PIXPLL:
		return mga_dac_pixpllc_calc(mdev, pixclk, 0, &pll->dac);
	case MGA_PLL_SYSPLL:
		return mga_dac_syspll_calc(mdev, pixclk, 0, &pll->dac);
	case MGA_PLL_TVO:
		return mga_tvo_pll_calc(&mdev->tvodev, pixclk, 0, &pll->dac);
	case MGA_PLL_AV9110:
		return mga_av9110_pll_calc(mdev, pixclk, pll);
	default:
		BUG_ON(outputs);
		return 0;
	}
}

static int mga_g400_pll_program(struct mga_dev *mdev,
				const struct mga_pll_settings *pll)
{
	switch (pll->type) {
	case MGA_PLL_NONE:
	case MGA_PLL_CRISTAL:
		return 0;
	case MGA_PLL_PIXPLL:
		mga_dac_pixpll_power(mdev, true);
		return mga_dac_pixpllc_program(mdev, &pll->dac);
	case MGA_PLL_SYSPLL:
		mga_dac_syspll_power(mdev, true);
		return mga_dac_syspll_program(mdev, &pll->dac);
	case MGA_PLL_TVO:
		/* FIXME */
		return 0;
	case MGA_PLL_AV9110:
		return mga_av9110_pll_program(mdev, pll);
	default:
		BUG();
	}
}

int mga_g400_set_mode(struct mga_dev *mdev,
		      struct mga_mode_config *mc)
{
	struct mga_pll_settings pll[2] = {
		[0] = { .type = MGA_PLL_NONE, },
		[1] = { .type = MGA_PLL_NONE, },
	};
	unsigned int pixclk[2] = { 0, 0 };
	unsigned int plls;
	int ret;
	unsigned int cpp[2] = { 0, 0 };
	struct mga_crtc1_regs *c1regs = &mc->crtc1.regs;
	struct mga_crtc2_regs *c2regs = &mc->crtc2.regs;
	bool changed[2] = {};
	struct mga_plane_config *pc[2] = {
		[0] = &mc->crtc1.plane_config,
		[1] = &mc->crtc2.plane_config,
	};
	struct mga_crtc_config *cc[2] = {
		[0] = &mc->crtc1.crtc_config,
		[1] = &mc->crtc2.crtc_config,
	};

	dev_dbg(mdev->dev, "%s\n"
		"mode[0] = %p\n"
		"mode[1] = %p\n"
		"cpp[0] = %u\n"
		"cpp[1] = %u\n"
		"outputs[0] = %x\n"
		"outputs[1] = %x\n",
		__func__,
		cc[0]->adjusted_mode,
		cc[1]->adjusted_mode,
		cpp[0],
		cpp[1],
		cc[0]->outputs,
		cc[1]->outputs);

	/* all or nothing */
	if (cc[0]->mode_valid) {
		if (!cc[0]->outputs || !pc[0]->fb)
			return -EINVAL;
		cpp[0] = drm_format_plane_cpp(pc[0]->fb->pixel_format, 0);
	} else {
		if (cc[0]->outputs || pc[0]->fb)
			return -EINVAL;
	}
	if (cc[1]->mode_valid) {
		if (!cc[1]->outputs || !pc[1]->fb)
			return -EINVAL;
		cpp[1] = drm_format_plane_cpp(pc[1]->fb->pixel_format, 0);
	} else {
		if (cc[1]->outputs || pc[1]->fb)
			return -EINVAL;
	}

	/* check board outputs */
	if ((cc[0]->outputs | cc[1]->outputs) & ~mdev->outputs)
		return -ENODEV;

	/* the same output can't be sourced from multiple CRTCs */
	if (cc[0]->outputs & cc[1]->outputs)
		return -EINVAL;

	/* only one MAFC port */
	if (!!(cc[0]->outputs & MGA_OUTPUT_DAC2) +
	    !!(cc[0]->outputs & MGA_OUTPUT_TMDS1) +
	    !!(cc[0]->outputs & MGA_OUTPUT_TVOUT) +
	    !!(cc[1]->outputs & MGA_OUTPUT_DAC2) +
	    !!(cc[1]->outputs & MGA_OUTPUT_TMDS1) +
	    !!(cc[1]->outputs & MGA_OUTPUT_TVOUT) > 1)
		return -EINVAL;

	if (cc[0]->mode_valid) {
		if (cc[0]->outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
			cc[0]->vidrst |= true;

#ifdef TVO_WINDOWS
		if (cc[0]->outputs & MGA_OUTPUT_DAC2) {
			cc[0]->adjusted_mode.htotal -= 8;
			cc[0]->adjusted_mode.hdisplay += 8;
			cc[0]->adjusted_mode.hsync_start += 8;
		}
#endif
#ifdef TVO_BEOS
		if (cc[0]->outputs & MGA_OUTPUT_DAC2)
			cc[0]->adjusted_mode.vdisplay += 1;
#endif
		ret = mga_crtc1_calc_mode(mdev, pc[0], cc[0], c1regs);
		if (ret)
			return ret;

#ifdef TVO_WINDOWS
		if (cc[0]->outputs & MGA_OUTPUT_DAC2)
			cc[0]->adjusted_mode.htotal += 8;
#endif
		pixclk[0] = mga_calc_pixel_clock(&cc[0]->adjusted_mode, 1);
#ifdef TVO_WINDOWS
		if (cc[0]->outputs & MGA_OUTPUT_DAC2)
			cc[0]->adjusted_mode.htotal -= 8;
#endif

		if (!pixclk[0])
			return -EINVAL;
	}

	if (cc[1]->mode_valid) {
		if (cc[1]->adjusted_mode.flags & DRM_MODE_FLAG_INTERLACE) {
			cc[1]->bt656 = true;
		} else if (cc[1]->outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
			cc[1]->vidrst = true;

#ifdef TVO_WINDOWS
		if (cc[1]->outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT)) {
			cc[1]->adjusted_mode.htotal -= 8;
			cc[1]->adjusted_mode.hdisplay += 8;
			cc[1]->adjusted_mode.hsync_start += 8;
		}
#endif
#ifdef TVO_BEOS
		if (cc[1]->outputs & MGA_OUTPUT_DAC2)
			cc[1]->adjusted_mode.vdisplay += 1;
#endif
		ret = mga_crtc2_calc_mode(mdev, pc[1], cc[1], c2regs);
		if (ret)
			return ret;

#ifdef TVO_WINDOWS
		if (cc[1]->outputs & (MGA_OUTPUT_DAC2 | MGA_OUTPUT_TVOUT))
			cc[1]->adjusted_mode->htotal += 8;
#endif
		pixclk[1] = mga_calc_pixel_clock(&cc[1]->adjusted_mode, 1);
#ifdef TVO_WINDOWS
		if (cc[1]->outputs & MGA_OUTPUT_DAC2)
			cc[1]->adjusted_mode->htotal += 8;
#endif
		if (!pixclk[1])
			return -EINVAL;
	}

	ret = mga_g400_hipri(mdev, mdev->mclk, pixclk[0], pixclk[1],
			     cpp[0] * 8, cpp[1] * 8, cc[1]->bt656, c1regs, c2regs);
	if (ret)
		return ret;

	/* pick our favorite PLLs */

	pll[0].type = mga_g400_select_pll(mdev, cc[0]->outputs);
	pll[1].type = mga_g400_select_pll(mdev, cc[1]->outputs);

	/* FIXME CRTC/DAC max clock */
	ret = mga_g400_pll_calc(mdev, cc[0]->outputs, pixclk[0], &pll[0]);
	ret = mga_g400_pll_calc(mdev, cc[1]->outputs, pixclk[1], &pll[1]);

	if (pll[0].type != mdev_crtcs.crtc1.pll ||
	    pixclk[0] != mdev_crtcs.crtc1.pixclk ||
	    cc[0]->outputs != mdev_crtcs.crtc1.outputs ||
	    pc[0]->fb != mdev_crtcs.crtc1.fb ||
	    (cc[0]->mode_valid && !drm_mode_equal(&cc[0]->adjusted_mode, &mdev_crtcs.crtc1.mode)) ||
	    pc[0]->hscale != mdev_crtcs.crtc1.hscale ||
	    pc[0]->vscale != mdev_crtcs.crtc1.vscale ||
	    pc[0]->src.x1 != mdev_crtcs.crtc1.x ||
	    pc[0]->src.y1 != mdev_crtcs.crtc1.y)
		changed[0] = true;
	if (pll[1].type != mdev_crtcs.crtc2.pll ||
	    pixclk[1] != mdev_crtcs.crtc2.pixclk ||
	    cc[1]->outputs != mdev_crtcs.crtc2.outputs ||
	    pc[1]->fb != mdev_crtcs.crtc2.fb ||
	    (cc[1]->mode_valid && !drm_mode_equal(&cc[1]->adjusted_mode, &mdev_crtcs.crtc2.mode)) ||
	    pc[1]->src.x1 != mdev_crtcs.crtc2.x ||
	    pc[1]->src.y1 != mdev_crtcs.crtc2.y)
		changed[1] = true;

	if (!changed[0] && !changed[1])
		goto exit;

	if (changed[0])
		crtc1_disable(mdev);

	if (changed[1])
		crtc2_disable(mdev);

	dev_dbg(mdev->dev, "%s %x %x\n", __func__, cc[0]->outputs, cc[1]->outputs);

	if (cc[0]->mode_valid && pll[0].type) {
		ret = mga_g400_pll_program(mdev, &pll[0]);
		if (ret) {
			// FIXME restore previous mode?
			goto exit;
		}
	}

	if (cc[1]->mode_valid && pll[1].type) {
		ret = mga_g400_pll_program(mdev, &pll[1]);
		if (ret) {
			// FIXME restore previous mode?
			goto exit;
		}
	}

	if (changed[0] && cc[0]->mode_valid && crtc_needs_tvo(mdev, cc[0]->outputs))
		mc->tvo_config.vidrst_delay = mga_crtc1_vidrst_delay(mdev, pc[0]->fb->pixel_format);

	if (changed[1] && cc[1]->mode_valid && crtc_needs_tvo(mdev, cc[1]->outputs))
		mc->tvo_config.vidrst_delay = mga_crtc2_vidrst_delay(mdev, cc[1]->bt656);

	if (changed[0] && cc[0]->mode_valid)
		crtc1_enable(mdev, mc, &pll[0]);

	if (changed[1] && cc[1]->mode_valid)
		crtc2_enable(mdev, mc, &pll[1]);

	// disp start
	// zoom

	mdev_crtcs.crtc1.fb = pc[0]->fb;
	if (cc[0]->mode_valid)
		mdev_crtcs.crtc1.mode = cc[0]->adjusted_mode;
	else
		memset(&mdev_crtcs.crtc1.mode, 0, sizeof mdev_crtcs.crtc1.mode);
	mdev_crtcs.crtc1.pll = pll[0].type;
	mdev_crtcs.crtc1.outputs = cc[0]->outputs;
	mdev_crtcs.crtc1.pixclk = pixclk[0];
	mdev_crtcs.crtc1.hscale = pc[0]->vscale;
	mdev_crtcs.crtc1.vscale = pc[0]->vscale;
	mdev_crtcs.crtc1.x = pc[0]->src.x1;
	mdev_crtcs.crtc1.y = pc[0]->src.y1;

	mdev_crtcs.crtc2.fb = pc[1]->fb;
	if (cc[1]->mode_valid)
		mdev_crtcs.crtc2.mode = cc[1]->adjusted_mode;
	else
		memset(&mdev_crtcs.crtc2.mode, 0, sizeof mdev_crtcs.crtc2.mode);
	mdev_crtcs.crtc2.pll = pll[1].type;
	mdev_crtcs.crtc2.outputs = cc[1]->outputs;
	mdev_crtcs.crtc2.pixclk = pixclk[1];
	mdev_crtcs.crtc2.hscale = pc[1]->hscale;
	mdev_crtcs.crtc2.vscale = pc[1]->vscale;
	mdev_crtcs.crtc2.x = pc[1]->src.x1;
	mdev_crtcs.crtc2.y = pc[1]->src.y1;

 exit:
	// FIXME hipri

	/* power down unused PLLs */
	plls = mdev->sysclk_plls | mdev_crtcs.crtc1.pll | mdev_crtcs.crtc2.pll;

	if (mdev->enabled_plls & MGA_PLL_PIXPLL && !(plls & MGA_PLL_PIXPLL))
		mga_dac_pixpll_power(mdev, false);
	if (mdev->enabled_plls & MGA_PLL_SYSPLL && !(plls & MGA_PLL_SYSPLL))
		mga_dac_syspll_power(mdev, false);
	if (mdev->enabled_plls & MGA_PLL_AV9110 && !(plls & MGA_PLL_AV9110))
		mga_av9110_pll_power(mdev, false);

	return ret;
}
