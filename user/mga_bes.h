/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_BES_H
#define MGA_BES_H

struct mga_dev;
struct drm_display_mode;
struct drm_framebuffer;
struct drm_region;

bool mga_bes_calc_hzoom(const struct mga_dev *mdev,
			const struct drm_display_mode *mode);

void mga_bes_calc(struct mga_dev *mdev,
		  const struct mga_plane_config *pc,
		  const struct mga_crtc_config *cc,
		  struct mga_bes_regs *regs);

void mga_bes_restore(struct mga_dev *mdev,
		     const struct mga_bes_regs *regs);

#endif
