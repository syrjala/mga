/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "mga_i2c.h"
#include "i2c.h"

int mga_i2c_init(struct mga_dev *mdev,
		 void (*setsda)(void *data, int state),
		 void (*setscl)(void *data, int state),
		 int (*getsda)(void *data),
		 int (*getscl)(void *data),
		 struct mga_i2c_channel *chan)
{
	int ret;

	chan->adap.owner = THIS_MODULE;
	chan->adap.id = 0;
	chan->adap.algo_data = &chan->algo;
	chan->adap.dev.parent = mdev->dev;
	chan->algo.setsda = setsda;
	chan->algo.setscl = setscl;
	chan->algo.getsda = getsda;
	chan->algo.getscl = getscl;
	chan->algo.udelay = 10; /* 100 kHz */
	chan->algo.timeout = usecs_to_jiffies(2200); /* DDC says 2.2ms max */
	chan->algo.data = mdev;

	i2c_set_adapdata(&chan->adap, chan);

	ret = i2c_bit_add_bus(&chan->adap);
	if (ret)
		return ret;

	setsda(mdev, 1);
	setscl(mdev, 1);
	udelay(10);

	return 0;
}

enum {
	/* DDC */
	TVP_DDC_SCL = 0x10,
	TVP_DDC_SDA = 0x04,
};

static void tvp_ddc_setscl(void *data, int state)
{
	struct mga_dev *mdev = data;

	tvp_i2c_set(&mdev->tdev, TVP_DDC_SCL, state);
}

static void tvp_ddc_setsda(void *data, int state)
{
	struct mga_dev *mdev = data;

	tvp_i2c_set(&mdev->tdev, TVP_DDC_SDA, state);
}

static int tvp_ddc_getscl(void *data)
{
	struct mga_dev *mdev = data;

	return tvp_i2c_get(&mdev->tdev, TVP_DDC_SCL);
}

static int tvp_ddc_getsda(void *data)
{
	struct mga_dev *mdev = data;

	return tvp_i2c_get(&mdev->tdev, TVP_DDC_SDA);
}

int mga_i2c_tvp_ddc_init(struct mga_dev *mdev)
{
	return mga_i2c_init(mdev, tvp_ddc_setsda, tvp_ddc_setscl,
			    tvp_ddc_getsda, tvp_ddc_getscl, &mdev->ddc1);
}

enum {
	/* DDC for connector 1 */
	MGA_DDC1_SCL = 0x08, /* pin DDC<3> */
	MGA_DDC1_SDA = 0x02, /* pin DDC<1> */

	/* DDC for connector 2 */
	MGA_DDC2_SCL = 0x04, /* pin DDC<2> */
	MGA_DDC2_SDA = 0x01, /* pin DDC<0> */

	/* External chips */
	MGA_MISC_SCL = 0x20, /* pin MISC<1> */
	MGA_MISC_SDA = 0x10, /* pin MISC<0> */
};

static void mga_ddc1_setscl(void *data, int state)
{
	struct mga_dev *mdev = data;

	mga_dac_i2c_set(mdev, MGA_DDC1_SCL, state);
}

static void mga_ddc1_setsda(void *data, int state)
{
	struct mga_dev *mdev = data;

	mga_dac_i2c_set(mdev, MGA_DDC1_SDA, state);
}

static int mga_ddc1_getscl(void *data)
{
	struct mga_dev *mdev = data;

	return mga_dac_i2c_get(mdev, MGA_DDC1_SCL);
}

static int mga_ddc1_getsda(void *data)
{
	struct mga_dev *mdev = data;

	return mga_dac_i2c_get(mdev, MGA_DDC1_SDA);
}

static void mga_ddc2_setscl(void *data, int state)
{
	struct mga_dev *mdev = data;

	mga_dac_i2c_set(mdev, MGA_DDC2_SCL, state);
}

static void mga_ddc2_setsda(void *data, int state)
{
	struct mga_dev *mdev = data;

	mga_dac_i2c_set(mdev, MGA_DDC2_SDA, state);
}

static int mga_ddc2_getscl(void *data)
{
	struct mga_dev *mdev = data;

	return mga_dac_i2c_get(mdev, MGA_DDC2_SCL);
}

static int mga_ddc2_getsda(void *data)
{
	struct mga_dev *mdev = data;

	return mga_dac_i2c_get(mdev, MGA_DDC2_SDA);
}

static void mga_misc_setscl(void *data, int state)
{
	struct mga_dev *mdev = data;

	mga_dac_i2c_set(mdev, MGA_MISC_SCL, state);
}

static void mga_misc_setsda(void *data, int state)
{
	struct mga_dev *mdev = data;

	mga_dac_i2c_set(mdev, MGA_MISC_SDA, state);
}

static int mga_misc_getscl(void *data)
{
	struct mga_dev *mdev = data;

	return mga_dac_i2c_get(mdev, MGA_MISC_SCL);
}

static int mga_misc_getsda(void *data)
{
	struct mga_dev *mdev = data;

	return mga_dac_i2c_get(mdev, MGA_MISC_SDA);
}

int mga_i2c_dac_ddc1_init(struct mga_dev *mdev)
{
	return mga_i2c_init(mdev, mga_ddc1_setsda, mga_ddc1_setscl,
			    mga_ddc1_getsda, mga_ddc1_getscl, &mdev->ddc1);
}

int mga_i2c_dac_misc_init(struct mga_dev *mdev)
{
	return mga_i2c_init(mdev, mga_misc_setsda, mga_misc_setscl,
			    mga_misc_getsda, mga_misc_getscl, &mdev->misc);
}

int mga_i2c_dac_ddc2_init(struct mga_dev *mdev)
{
	return mga_i2c_init(mdev, mga_ddc2_setsda, mga_ddc2_setscl,
			    mga_ddc2_getsda, mga_ddc2_getscl, &mdev->ddc2);
}

int mga_i2c_dac_ddc12_init(struct mga_dev *mdev)
{
	/*
	 * G200 MMS: input and output use different sets of pins
	 */
	return mga_i2c_init(mdev, mga_ddc1_setsda, mga_ddc1_setscl,
			    mga_ddc2_getsda, mga_ddc2_getscl, &mdev->ddc1);
}

int mga_g400_detect_tvout_adapter(struct mga_dev *mdev)
{
	bool detected;

	if (mdev->chip < MGA_CHIP_G400 ||
	    !(mdev->outputs & MGA_OUTPUT_DAC2))
		return -ENODEV;

	/* SDA and SCL should be high at this point. */
	if (!mga_ddc2_getscl(mdev) || !mga_ddc2_getsda(mdev))
		return -ENODEV;

	/*
	 * The TV-out adapter has SCL and SDA shorted.
	 * Drive one low and the other should follow.
	 */

	/* This will generate an I2C START if SCL remains high. */
	mga_ddc2_setsda(mdev, 0);
	udelay(10);

	detected = !mga_ddc2_getscl(mdev);

	/* Generate an I2C STOP if SCL remained high. */
	mga_ddc2_setsda(mdev, 1);
	udelay(10);

	return detected ? 0 : -ENODEV;
}
