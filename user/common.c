/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>
#include <fcntl.h>
#include <assert.h>
#include <stdarg.h>
#include <errno.h>

#include "server.h"

int open_socket(const char *path, struct sockaddr_un *addr)
{
	memset(addr, 0, sizeof *addr);

	addr->sun_family = AF_UNIX;

	strncpy(addr->sun_path, path, sizeof addr->sun_path - 1);

	return socket(AF_UNIX, SOCK_STREAM, 0);
}

ssize_t wrap_write(int fd, const void *data, size_t len)
{
	size_t left = len;
	ssize_t r;

	while (left) {
		r = write(fd, data, left);
		if (r < 0 && errno == EINTR)
			continue;
		if (r < 0)
			return r;
		left -= r;
		data += r;
	}

	assert(left == 0);

	return len;
}

ssize_t wrap_read(int fd, void *data, size_t len)
{
	size_t left = len;
	ssize_t r;

	while (left) {
		r = read(fd, data, left);
		if (r < 0 && errno == EINTR)
			continue;
		if (r < 0)
			return r;
		left -= r;
		data += r;
	}

	assert(left == 0);

	return len;
}
