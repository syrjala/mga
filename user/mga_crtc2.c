/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>

#include "mga_dump.h"
#include "mga_regs.h"
#include "mga_kms.h"

void mga_crtc2_video(struct mga_dev *mdev, bool enable)
{
	u32 val = mga_read32(mdev, MGA_C2CTL);

	if (enable)
		val |= MGA_C2CTL_C2EN;
	else
		val &= ~MGA_C2CTL_C2EN;

	mga_write32(mdev, MGA_C2CTL, val);
}

void mga_crtc2_dpms(struct mga_dev *mdev, u8 mode)
{
	(void)mdev;
	(void)mode;

	/* FIXME modify timing registers perhaps? */
}

void mga_crtc2_pixclk_enable(struct mga_dev *mdev, bool enable)
{
	u32 val = mga_read32(mdev, MGA_C2CTL);

	if (enable)
		val &= ~MGA_C2CTL_C2PIXCLKDIS;
	else
		val |= MGA_C2CTL_C2PIXCLKDIS;

	mga_write32(mdev, MGA_C2CTL, val);

	if (!enable) {
		val = mga_read32(mdev, MGA_C2CTL);

		val &= ~MGA_C2CTL_C2INTERLACE;

		mga_write32(mdev, MGA_C2CTL, val);
	}

	mga_wait(mdev);
}

void mga_crtc2_pixclk_select(struct mga_dev *mdev,
			     unsigned int pll)
{
	static const u32 g400_c2pixclksel_vals[] = {
		[MGA_PLL_NONE]    = MGA_C2CTL_C2PIXCLKSEL_PCI,
		[MGA_PLL_PIXPLL]  = MGA_C2CTL_C2PIXCLKSEL_PIXPLL,
		[MGA_PLL_SYSPLL]  = MGA_C2CTL_C2PIXCLKSEL_SYSPLL,
		[MGA_PLL_TVO]     = MGA_C2CTL_C2PIXCLKSEL_VDOCLK,
		[MGA_PLL_AV9110]  = MGA_C2CTL_C2PIXCLKSEL_VDOCLK,
	};
	static const u32 g450_c2pixclksel_vals[] = {
		[MGA_PLL_NONE]    = MGA_C2CTL_C2PIXCLKSEL_PCI,
		[MGA_PLL_PIXPLL]  = MGA_C2CTL_C2PIXCLKSEL_PIXPLL,
		[MGA_PLL_SYSPLL]  = MGA_C2CTL_C2PIXCLKSEL_SYSPLL_G450,
		[MGA_PLL_VIDPLL]  = MGA_C2CTL_C2PIXCLKSEL_VIDPLL_G450,
		[MGA_PLL_CRISTAL] = MGA_C2CTL_C2PIXCLKSEL_CRISTAL_G450,
	};
	u32 val;

	val = mga_read32(mdev, MGA_C2CTL);

	val &= ~MGA_C2CTL_C2PIXCLKSEL;
	if (mdev->chip >= MGA_CHIP_G450)
		val |= g450_c2pixclksel_vals[pll];
	else
		val |= g400_c2pixclksel_vals[pll];

	mga_write32(mdev, MGA_C2CTL, val);
}

/*
 * G450/G550: CRTC2 -> DAC1/DAC2/TMDS
 * G400: CRTC2 -> DAC1
 */
void mga_crtc2_set_sync(struct mga_dev *mdev, unsigned int sync)
{
	u32 val = mga_read32(mdev, MGA_C2MISC);

	if (sync & DRM_MODE_FLAG_NHSYNC)
		val |= MGA_C2MISC_C2HSYNCPOL;
	else
		val &= ~MGA_C2MISC_C2HSYNCPOL;

	if (sync & DRM_MODE_FLAG_NVSYNC)
		val |= MGA_C2MISC_C2VSYNCPOL;
	else
		val &= ~MGA_C2MISC_C2VSYNCPOL;

	mga_write32(mdev, MGA_C2MISC, val);
}

void mga_crtc2_dac_source(struct mga_dev *mdev, unsigned int crtc)
{
	u32 val = mga_read32(mdev, MGA_C2CTL);

	if (crtc == MGA_CRTC_CRTC2)
		val |= MGA_C2CTL_CRTCDACSEL;
	else
		val &= ~MGA_C2CTL_CRTCDACSEL;

	mga_write32(mdev, MGA_C2CTL, val);
}

static unsigned int calc_offset(unsigned int pitch,
				unsigned int cpp)
{
	return ALIGN(pitch * cpp, 0x40);
}

static unsigned int calc_startadd(unsigned int address)
{
	return ALIGN(address, 0x40);
}

static int crtc2_mode_to_timings(const struct mga_dev *mdev,
				 const struct drm_display_mode *mode,
				 bool bt656,
				 struct mga_crtc2_regs *regs)
{
	int hdispend, hsyncstr, hsyncend, htotal;
	int vdispend, vsyncstr, vsyncend, vtotal;
	int hpreload, vpreload, linecomp;

	/* horizontal timings */

	/* round hdisplay down to avoid exceeding framebuffer width */
	hdispend = mode->hdisplay & ~7;
	hsyncstr = ALIGN(mode->hsync_start, 8);
	hsyncend = hsyncstr + ALIGN(mode->hsync_width, 8);
	/* do not align htotal in BT.656 mode to allow exact NTSC timings. */
	htotal = bt656 ? mode->htotal : ALIGN(mode->htotal, 8);

	hdispend -= 8;
	hsyncstr -= 8;
	hsyncend -= 8;
	htotal   -= 8;

	// FIXME
	if (hdispend < 0)
		return -EINVAL;
	if (hsyncstr - hdispend < 8) {
		dev_dbg(mdev->dev, "crtc2: adjusting hsyncstr %d -> %d\n", hsyncstr, hdispend + 8);
		hsyncstr = hdispend + 8;
	}
	if (hsyncend - hsyncstr < 8) {
		dev_dbg(mdev->dev, "crtc2: adjusting hsyncend %d -> %d\n", hsyncend, hsyncstr + 8);
		hsyncend = hsyncstr + 8;
	}
	if (htotal - hsyncend < 8) {
		dev_dbg(mdev->dev, "crtc2: adjusting htotal %d -> %d\n", htotal, 8);
		htotal = hsyncend + 8;
	}

	dev_dbg(mdev->dev,
		"crtc2:\n"
		" htotal   = %d\n"
		" hdispend = %d\n"
		" hsyncstr = %d\n"
		" hsyncend = %d\n",
		htotal, hdispend, hsyncstr, hsyncend);

	if (htotal > 0xff8 || hdispend > 0xff8 || hsyncstr > 0xff8 || hsyncend > 0xff8)
		return -EINVAL;

	/* vertical timings */

	vdispend = mode->vdisplay;
	vsyncstr = mode->vsync_start;
	vsyncend = mode->vsync_start + mode->vsync_width;
	vtotal   = mode->vtotal;

	if (mode->flags & DRM_MODE_FLAG_INTERLACE) {
		vdispend >>= 1;
		vsyncstr >>= 1;
		vsyncend >>= 1;
		vtotal   >>= 1;
	}

	vdispend--;
	vsyncstr--;
	vsyncend--;
	vtotal--;

	// FIXME
	if (vdispend < 0)
		return -EINVAL;
	if (vsyncstr - vdispend < 1) {
		dev_dbg(mdev->dev, "crtc2: adjusting vsyncstr %d -> %d\n", vsyncstr, vdispend + 1);
		vsyncstr = vdispend + 1;
	}
	if (vsyncend - vsyncstr < 1) {
		dev_dbg(mdev->dev, "crtc2: adjusting vsyncend %d -> %d\n", vsyncend, vsyncstr + 1);
		vsyncend = vsyncstr + 1;
	}
	if (vtotal - vsyncend < 1) {
		dev_dbg(mdev->dev, "crtc2: adjusting vtotal %d -> %d\n", vtotal, vsyncend + 1);
		vtotal = vsyncend + 1;
	}

	dev_dbg(mdev->dev,
		"crtc2:\n"
		" vtotal   = %u\n"
		" vdispend = %u\n"
		" vsyncstr = %u\n"
		" vsyncend = %u\n",
		vtotal, vdispend, vsyncstr, vsyncend);

	if (vdispend > 0xfff || vsyncstr > 0xfff || vsyncend > 0xfff || vtotal > 0xfff)
		return -EINVAL;

	/* FIXME why +8 and +1? */
	hpreload = hsyncstr + 8;
	vpreload = vsyncstr + 1;
	/* FIXME check the exact location of interrupt */
	linecomp = vdispend + 1;

	if (hpreload > 0xfff || vpreload > 0xfff || linecomp > 0xfff)
		return -EINVAL;

	regs->c2hparam = (hdispend << 16) | htotal;
	regs->c2hsync = (hsyncend << 16) | hsyncstr;
	regs->c2vparam = (vdispend << 16) | vtotal;
	regs->c2vsync = (vsyncend << 16) | vsyncstr;
	regs->c2preload = (vpreload << 16) | hpreload;
	regs->c2misc = linecomp << 16;

	if (bt656) {
		unsigned int c2fieldline0, c2fieldline1;

		/*
		 * EAV/SAV F bit toggles
		 * c2fieldline1 lines after the top field active video.
		 * c2fieldline0 lines after the bottom field active video,
		 */
		if (htotal & 7) {
			regs->c2datactl |= MGA_C2DATACTL_C2NTSCEN;

			/*
			 * NTSC
			 * F=1 is the top field
			 * F=0 is the bottom field
			 * 240 active lines per field
			 */
			/* F=0 starts on line 4, F=1 first active line is 286 */
			c2fieldline1 = 525 + 4 - 286 - 240;
			/* F=1 starts on line 266, F=0 first active line is 24 */
			c2fieldline0 = 266 - 24 - 240;
		} else {
			/*
			 * PAL
			 * F=0 is the top field
			 * F=1 is the bottom field
			 * 288 active lines per field
			 */
			/* F=1 starts on line 313, F=0 first active line is 23 */
			c2fieldline1 = 313 - 23 - 288;
			/* F=0 starts on line 1, F=1 first active line is 336 */
			c2fieldline0 = 625 + 1 - 336 - 288;
		}

		regs->c2misc |= ((c2fieldline1 - 1) << 4) | ((c2fieldline0 - 1) << 0);

		if (mode->flags & DRM_MODE_FLAG_INTERLACE)
			regs->c2ctl |= MGA_C2CTL_C2INTERLACE;

		if (mdev->chip >= MGA_CHIP_G450)
			regs->c2ctl |= 0x1000;
	}

	return 0;
}

static void crtc2_timings_to_mode(struct drm_display_mode *mode,
				  const struct mga_crtc2_regs *regs)
{
	int hdispend, hsyncstr, hsyncend, htotal;
	int vdispend, vsyncstr, vsyncend, vtotal;

	mode->flags = 0;

	htotal   = (regs->c2hparam & 0xffff) + 8;
	hdispend = (regs->c2hparam >> 16) + 8;
	hsyncstr = (regs->c2hsync & 0xffff) + 8;
	hsyncend = (regs->c2hsync >> 16) + 8;

	vtotal   = (regs->c2vparam & 0xffff) + 1;
	vdispend = (regs->c2vparam >> 16) + 1;
	vsyncstr = (regs->c2vsync & 0xffff) + 1;
	vsyncend = (regs->c2vsync >> 16) + 1;

	if (regs->c2ctl & MGA_C2CTL_C2INTERLACE) {
		vtotal = (vtotal << 1) + 1;
		vdispend <<= 1;
		vsyncstr <<= 1;
		vsyncstr <<= 1;

		mode->flags |= DRM_MODE_FLAG_INTERLACE;
	}

	mode->htotal = htotal;
	mode->hdisplay = hdispend;
	mode->hblank_start = hdispend;
	mode->hblank_width = htotal - hdispend;
	mode->hsync_start = hsyncstr;
	mode->hsync_width = hsyncend - hsyncstr;

	mode->vtotal = vtotal;
	mode->vdisplay = vdispend;
	mode->vblank_start = vdispend;
	mode->vblank_width = vtotal - vdispend;
	mode->vsync_start = vsyncstr;
	mode->vsync_width = vsyncend - vsyncstr;
}

static int crtc2_check_framebuffer(const struct mga_dev *mdev,
				   const struct mga_framebuffer *mfb,
				   const struct drm_display_mode *mode,
				   bool bt656,
				   unsigned int x, unsigned int y)
{
	switch (mfb->base.pixel_format) {
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		break;
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		if (!bt656) {
			dev_dbg(mdev->dev, "crtc2: YCbCr only supported in ITU-R BT.656 mode\n");
			return -EINVAL;
		}
		break;
	default:
		dev_dbg(mdev->dev, "crtc2: unsupported pixel format 0x%08x\n", mfb->base.pixel_format);
		return -EINVAL;
	}

	if (mga_framebuffer_calc_bus(mfb, x, y, 0, 0) & 0x3f) {
		dev_dbg(mdev->dev, "crtc2: unsupported offset\n");
		return -EINVAL;
	}

	if (mfb->base.pitches[0] & 0x3f) {
		dev_dbg(mdev->dev, "crtc2: unsupported pitch\n");
		return -EINVAL;
	}

	switch (mfb->base.pixel_format) {
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
		if (x & 1) {
			dev_dbg(mdev->dev, "crtc2: unsupported x offset\n");
			return -EINVAL;
		}
		break;
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		if (x & 1 || y & 1) {
			dev_dbg(mdev->dev, "crtc2: unsupported x/y offsets\n");
			return -EINVAL;
		}
		x >>= 1;
		y >>= 1;

		if (mga_framebuffer_calc_bus(mfb, x, y, 1, 0) & 0x3f ||
		    mga_framebuffer_calc_bus(mfb, x, y, 2, 0) & 0x3f) {
			dev_dbg(mdev->dev, "crtc2: unsupported offset\n");
			return -EINVAL;
		}

		if (mfb->base.pitches[1] & 0x3f ||
		    mfb->base.pitches[2] & 0x3f ||
		    mfb->base.pitches[1] != mfb->base.pitches[0] / 2 ||
		    mfb->base.pitches[2] != mfb->base.pitches[1]) {
			dev_dbg(mdev->dev, "crtc2: unsupported pitch\n");
			return -EINVAL;
		}
		break;
	default:
		break;
	}

	return 0;
}

static bool mga_mode_is_broadcast(const struct drm_display_mode *mode)
{
	if (mode->flags != DRM_MODE_FLAG_INTERLACE)
		return false;

	if (mode->clock != 13500)
		return false;

	/* FIXME checks h/v syncs too? */

	/* PAL */
	if (mode->hdisplay     == 720 &&
	    mode->hblank_start == 720 &&
	    mode->hblank_width == 144 &&
	    mode->htotal       == 864 &&
	    mode->vdisplay     == 576 &&
	    mode->vblank_start == 576 &&
	    mode->vblank_width == 49 &&
	    mode->vtotal       == 625)
		return true;

	/* NTSC */
	if (mode->hdisplay     == 720 &&
	    mode->hblank_start == 720 &&
	    mode->hblank_width == 138 &&
	    mode->htotal       == 858 &&
	    mode->vdisplay     == 480 &&
	    mode->vblank_start == 480 &&
	    mode->vblank_width == 45 &&
	    mode->vtotal       == 525)
		return true;

	return false;
}

static int crtc2_check_mode(const struct mga_dev *mdev,
			    const struct drm_display_mode *mode,
			    bool vidrst, bool bt656)
{
	if (mode->flags & ~DRM_MODE_FLAG_INTERLACE) {
		dev_dbg(mdev->dev, "crtc2: unsupported mode flags\n");
		return -EINVAL;
	}

	BUG_ON(mode->private_flags);

	if (bt656) {
		BUG_ON(vidrst);

		if (!mga_mode_is_broadcast(mode)) {
			dev_dbg(mdev->dev, "crtc2: only \"broadcast\" modes supported in ITU-R BT.656 mode\n");
			return -EINVAL;
		}

		/* FIXME check somewhere that the TV standard matches the mode when doing BT.656 */
	} else {
		if (mode->flags & DRM_MODE_FLAG_INTERLACE) {
			dev_dbg(mdev->dev, "crtc2: interlaced scan supported only in ITU-R BT.656 mode\n");
			return -EINVAL;
		}
	}

	return 0;
}

static int mga_crtc2_calc_timings(const struct mga_dev *mdev,
				  const struct drm_display_mode *mode,
				  bool vidrst, bool bt656,
				  struct mga_crtc2_regs *regs)
{
	int ret;

	ret = crtc2_check_mode(mdev, mode, vidrst, bt656);
	if (ret)
		return ret;

	ret = crtc2_mode_to_timings(mdev, mode, bt656, regs);
	if (ret)
		return ret;

	dev_dbg(mdev->dev, "Original CRTC2 mode:\n");
	drm_mode_print(mdev->dev, mode);

	return 0;
}

static bool mga_crtc2_current_field(struct mga_dev *mdev)
{
	bool c2field;
	unsigned int c2vcount;
	unsigned int c2fieldline0, c2fieldline1;
	unsigned int c2fieldlinemin, c2fieldlinemax;
	unsigned int c2misc = mga_read32(mdev, MGA_C2MISC);
	unsigned int vdisplay = mga_read32(mdev, MGA_C2VPARAM) >> 16;

	c2fieldline0 = (c2misc & MGA_C2MISC_C2FIELDLINE0) >> 0;
	c2fieldline1 = (c2misc & MGA_C2MISC_C2FIELDLINE1) >> 4;

	/*
	 * The field bit toggles
	 * c2fieldline1 lines after the top field active video.
	 * c2fieldline0 lines after the bottom field active video,
	 */

	c2fieldline0 += vdisplay + 1;
	c2fieldline1 += vdisplay + 1;

	/*
	 * If c2fieldline0 and c2fieldline1 differ and the counter
	 * is between them we can't tell which field it actually is
	 * because the c2field bit will be the same for both fields.
	 */
	c2fieldlinemin = min(c2fieldline0, c2fieldline1);
	c2fieldlinemax = max(c2fieldline0, c2fieldline1);

	do {
		c2vcount = mga_read32(mdev, MGA_C2VCOUNT);
		c2field = !!(c2vcount & MGA_C2VCOUNT_C2FIELD);
		c2vcount &= MGA_C2VCOUNT_C2VCOUNT;
	} while (c2vcount > c2fieldlinemin && c2vcount <= c2fieldlinemax);

	if ((c2field == 0 && c2vcount >= vdisplay && c2vcount < c2fieldline0) ||
	    (c2field == 1 && c2vcount >= vdisplay && c2vcount < c2fieldline1))
		c2field = !c2field;

	return c2field;
}

/*
 * Calculate all register values which depend on the framebuffer.
 */
static int mga_crtc2_calc_framebuffer(const struct mga_dev *mdev,
				      const struct mga_framebuffer *mfb,
				      const struct drm_display_mode *mode,
				      bool bt656,
				      unsigned int x, unsigned int y,
				      struct mga_crtc2_regs *regs)
{
	int ret;

	ret = crtc2_check_framebuffer(mdev, mfb, mode, bt656, x, y);
	if (ret)
		return ret;

	regs->c2ctl &= ~(MGA_C2CTL_C2DEPTH |
			 MGA_C2CTL_C2VCBCRSINGLE);

	regs->c2datactl &= ~(MGA_C2DATACTL_C2DITHEN |
			     MGA_C2DATACTL_C2YFILTEN |
			     MGA_C2DATACTL_C2CBCRFILTEN |
			     MGA_C2DATACTL_C2UYVYFMT);

	switch (mfb->base.pixel_format) {
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
		regs->c2ctl |= MGA_C2CTL_C2DEPTH_15BPP;
		break;
	case DRM_FORMAT_RGB565:
		regs->c2ctl |= MGA_C2CTL_C2DEPTH_16BPP;
		break;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		regs->c2ctl |= MGA_C2CTL_C2DEPTH_32BPP;
		break;
	case DRM_FORMAT_YUYV:
		regs->c2ctl |= MGA_C2CTL_C2DEPTH_YCBCR422;
		break;
	case DRM_FORMAT_UYVY:
		regs->c2ctl |= MGA_C2CTL_C2DEPTH_YCBCR422;
		regs->c2datactl |= MGA_C2DATACTL_C2UYVYFMT;
		break;
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		regs->c2ctl |= MGA_C2CTL_C2DEPTH_YCBCR420;
		break;
	}

	regs->c2startadd0 = mga_framebuffer_calc_bus(mfb, x, y, 0, 0);
	regs->c2offset = mfb->base.pitches[0];

	switch (mfb->base.pixel_format) {
	case DRM_FORMAT_YUV420:
		regs->c2pl2startadd0 = mga_framebuffer_calc_bus(mfb, x, y, 1, 0);
		regs->c2pl3startadd0 = mga_framebuffer_calc_bus(mfb, x, y, 2, 0);
		break;
	case DRM_FORMAT_YVU420:
		regs->c2pl2startadd0 = mga_framebuffer_calc_bus(mfb, x, y, 2, 0);
		regs->c2pl3startadd0 = mga_framebuffer_calc_bus(mfb, x, y, 1, 0);
		break;
	default:
		regs->c2pl2startadd0 = 0;
		regs->c2pl3startadd0 = 0;
		break;
	}

	regs->c2startadd1 = regs->c2startadd0;
	regs->c2pl2startadd1 = regs->c2pl2startadd0;
	regs->c2pl3startadd1 = regs->c2pl3startadd0;

	if (mode->flags & DRM_MODE_FLAG_INTERLACE) {
		regs->c2startadd0 += mfb->base.pitches[0];
		regs->c2offset <<= 1;

		switch (mfb->base.pixel_format) {
		case DRM_FORMAT_YUV420:
		case DRM_FORMAT_YVU420:
			regs->c2pl2startadd0 += mfb->base.pitches[1];
			regs->c2pl3startadd0 += mfb->base.pitches[2];
			break;
		default:
			break;
		}
	}

	if (bt656) {
		switch (mfb->base.pixel_format) {
		case DRM_FORMAT_ARGB1555:
		case DRM_FORMAT_XRGB1555:
		case DRM_FORMAT_RGB565:
		case DRM_FORMAT_ARGB8888:
		case DRM_FORMAT_XRGB8888:
			/* Use dithering + filtering in RGB->YCbCr converter */
			regs->c2datactl |= MGA_C2DATACTL_C2DITHEN |
					   MGA_C2DATACTL_C2YFILTEN |
					   MGA_C2DATACTL_C2CBCRFILTEN;
			break;
		case DRM_FORMAT_YUV420:
		case DRM_FORMAT_YVU420:
			/* Treat chroma as progressive? */
			if (!(mfb->base.flags & DRM_FB_INTERLACED))
				regs->c2datactl |= MGA_C2CTL_C2VCBCRSINGLE;
			break;
		default:
			break;
		}
	}

	return 0;
}

void mga_crtc2_restore_framebuffer(struct mga_dev *mdev,
				   const struct mga_crtc2_regs *regs)
{
	mga_write32(mdev, MGA_C2CTL, regs->c2ctl);
	mga_write32(mdev, MGA_C2STARTADD0, regs->c2startadd0);
	mga_write32(mdev, MGA_C2STARTADD1, regs->c2startadd1);
	mga_write32(mdev, MGA_C2PL2STARTADD0, regs->c2pl2startadd0);
	mga_write32(mdev, MGA_C2PL2STARTADD1, regs->c2pl2startadd1);
	mga_write32(mdev, MGA_C2PL3STARTADD0, regs->c2pl3startadd0);
	mga_write32(mdev, MGA_C2PL3STARTADD1, regs->c2pl3startadd1);
	mga_write32(mdev, MGA_C2OFFSET, regs->c2offset);
	mga_write32(mdev, MGA_C2DATACTL, regs->c2datactl);
	mga_write32(mdev, MGA_C2SPICSTARTADD0, regs->c2spicstartadd0);
	mga_write32(mdev, MGA_C2SPICSTARTADD1, regs->c2spicstartadd1);
}

int mga_crtc2_calc_mode(struct mga_dev *mdev,
			const struct mga_plane_config *pc,
			struct mga_crtc_config *cc,
			struct mga_crtc2_regs *regs)
{
	const struct drm_framebuffer *fb = pc->fb;
	struct drm_display_mode *mode = &cc->adjusted_mode;
	bool vidrst = cc->vidrst;
	bool bt656 = cc->bt656;
	unsigned int x = pc->src.x1 >> 16;
	unsigned int y = pc->src.y1 >> 16;
	const struct mga_framebuffer *mfb = to_mga_framebuffer(fb);
	unsigned int htotal;
	unsigned int hdispend;
	unsigned int hsyncstr;
	unsigned int hsyncend;
	unsigned int hpreload;

	unsigned int vtotal;
	unsigned int vdispend;
	unsigned int vsyncstr;
	unsigned int vsyncend;
	unsigned int vpreload;

	unsigned int address;
	unsigned int pitch;

	unsigned int hiprilvl;
	unsigned int maxhipri;

	int ret;
	int i;

	regs->c2ctl = MGA_C2CTL_C2PIXCLKDIS |
		MGA_C2CTL_C2PIXCLKSEL_PCI |
		MGA_C2CTL_C2VIDRSTMOD_RISING;
	regs->c2datactl = 0;

	ret = mga_crtc2_calc_framebuffer(mdev, mfb, mode, bt656, x, y, regs);
	if (ret)
		return ret;

	ret = mga_crtc2_calc_timings(mdev, mode, vidrst, bt656, regs);
	if (ret)
		return ret;

	if (vidrst)
		regs->c2ctl |= MGA_C2CTL_C2HPLOADEN | MGA_C2CTL_C2VPLOADEN;

	/* Copy the adjusted mode back */
	crtc2_timings_to_mode(mode, regs);

	dev_dbg(mdev->dev, "Adjusted CRTC2 mode:\n");
	drm_mode_print(mdev->dev, mode);

	return 0;
}

unsigned int mga_crtc2_vidrst_delay(const struct mga_dev *mdev, bool bt656)
{
	/*
	 * FIXME vidrst is not used in BT.656 mode, so why is
	 * the vidrst delay for it in the specification?
	 */
	return bt656 ? 52 : 34;
}

void mga_crtc2_enable_vidrst(struct mga_dev *mdev, bool enable)
{
	u32 val = mga_read32(mdev, MGA_C2CTL);

	if (enable)
		val |= MGA_C2CTL_C2HPLOADEN | MGA_C2CTL_C2VPLOADEN;
	else
		val &= ~(MGA_C2CTL_C2HPLOADEN | MGA_C2CTL_C2VPLOADEN);

	mga_write32(mdev, MGA_C2CTL, val);
}

enum {
	MGA_FRAME_TIMEOUT = 100,
};

void mga_crtc2_wait_vblank(struct mga_dev *mdev)
{
	unsigned long timeout = jiffies + msecs_to_jiffies(MGA_FRAME_TIMEOUT);

	mga_write32(mdev, MGA_ICLEAR, MGA_ICLEAR_C2VLINEICLR);

	dev_dbg(mdev->dev, "CRTC2 VBLANK [");
	/* FIXME why doesn't it seem to work? */
	while (!(mga_read32(mdev, MGA_STATUS) & MGA_STATUS_C2VLINEPEN) &&
	       time_before(jiffies, timeout))
		printk(KERN_CONT ".");
	printk(KERN_CONT "]\n");

	if (time_after_eq(jiffies, timeout))
		dev_err(mdev->dev, "CRTC2 VBLANK timeout\n");
}

void mga_crtc2_wait_active_video(struct mga_dev *mdev)
{
	unsigned long timeout = jiffies + msecs_to_jiffies(MGA_FRAME_TIMEOUT);

	dev_dbg(mdev->dev, "CRTC2 ACT [");
	while ((mga_read32(mdev, MGA_C2VCOUNT) & MGA_C2VCOUNT_C2VCOUNT) &&
	       time_before(jiffies, timeout))
		printk(KERN_CONT ".");
	printk(KERN_CONT "]\n");

	if (time_after_eq(jiffies, timeout))
		dev_err(mdev->dev, "CRTC2 ACT timeout\n");
}

void mga_crtc2_restore(struct mga_dev *mdev,
		       const struct mga_crtc2_regs *regs)
{
	/* Must not enable interlace this early */
	u32 c2ctl = regs->c2ctl & ~MGA_C2CTL_C2INTERLACE;
	int i;

	mga_write32(mdev, MGA_C2CTL, c2ctl);
	mga_write32(mdev, MGA_C2HPARAM, regs->c2hparam);
	mga_write32(mdev, MGA_C2HSYNC, regs->c2hsync);
	mga_write32(mdev, MGA_C2VPARAM, regs->c2vparam);
	mga_write32(mdev, MGA_C2VSYNC, regs->c2vsync);
	mga_write32(mdev, MGA_C2PRELOAD, regs->c2preload);
	mga_write32(mdev, MGA_C2STARTADD0, regs->c2startadd0);
	mga_write32(mdev, MGA_C2STARTADD1, regs->c2startadd1);
	mga_write32(mdev, MGA_C2PL2STARTADD0, regs->c2pl2startadd0);
	mga_write32(mdev, MGA_C2PL2STARTADD1, regs->c2pl2startadd1);
	mga_write32(mdev, MGA_C2PL3STARTADD0, regs->c2pl3startadd0);
	mga_write32(mdev, MGA_C2PL3STARTADD1, regs->c2pl3startadd1);
	mga_write32(mdev, MGA_C2OFFSET, regs->c2offset);
	mga_write32(mdev, MGA_C2MISC, regs->c2misc);
	mga_write32(mdev, MGA_C2DATACTL, regs->c2datactl);
	for (i = 0; i < 16; i++)
		mga_write32(mdev, MGA_C2SUBPICLUT + i, regs->c2subpiclut[i]);
	mga_write32(mdev, MGA_C2SPICSTARTADD0, regs->c2spicstartadd0);
	mga_write32(mdev, MGA_C2SPICSTARTADD1, regs->c2spicstartadd1);
}

void mga_crtc2_restore_interlace(struct mga_dev *mdev,
				 const struct mga_crtc2_regs *regs)
{
	if (!(regs->c2ctl & MGA_C2CTL_C2INTERLACE))
		return;

	mga_crtc2_wait_vblank(mdev);
	mga_crtc2_wait_active_video(mdev);
	mga_crtc2_wait_vblank(mdev);
	mga_crtc2_wait_active_video(mdev);

	mga_write32(mdev, MGA_C2CTL, regs->c2ctl);
}

static void mga_crtc2_save(struct mga_dev *mdev,
			   struct mga_crtc2_regs *regs)
{
	int i;

	regs->c2ctl = mga_read32(mdev, MGA_C2CTL);
	regs->c2hparam = mga_read32(mdev, MGA_C2HPARAM);
	regs->c2hsync = mga_read32(mdev, MGA_C2HSYNC);
	regs->c2vparam = mga_read32(mdev, MGA_C2VPARAM);
	regs->c2vsync = mga_read32(mdev, MGA_C2VSYNC);
	regs->c2preload = mga_read32(mdev, MGA_C2PRELOAD);
	regs->c2startadd0 = mga_read32(mdev, MGA_C2STARTADD0);
	regs->c2startadd1 = mga_read32(mdev, MGA_C2STARTADD1);
	regs->c2pl2startadd0 = mga_read32(mdev, MGA_C2PL2STARTADD0);
	regs->c2pl2startadd1 = mga_read32(mdev, MGA_C2PL2STARTADD1);
	regs->c2pl3startadd0 = mga_read32(mdev, MGA_C2PL3STARTADD0);
	regs->c2pl3startadd1 = mga_read32(mdev, MGA_C2PL3STARTADD1);
	regs->c2offset = mga_read32(mdev, MGA_C2OFFSET);
	regs->c2misc = mga_read32(mdev, MGA_C2MISC);
	regs->c2datactl = mga_read32(mdev, MGA_C2DATACTL);
	for (i = 0; i < 16; i++)
		regs->c2subpiclut[i] = mga_read32(mdev, MGA_C2SUBPICLUT + i);
	regs->c2spicstartadd0 = mga_read32(mdev, MGA_C2SPICSTARTADD0);
	regs->c2spicstartadd1 = mga_read32(mdev, MGA_C2SPICSTARTADD1);
}
