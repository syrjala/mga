/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "kernel_emul.h"
#include "mga_dump.h"
#include "tvo.h"

static const struct mga_dac_pll_info tvo_pll_info = {
	.name = "TVO PLL",

	.fref     =  27000,//FIXME pins maybe?
	.fo_max   = 220000,//FIXME pins maybe?
	.fvco_min =  50000,
	.fvco_max = 220000,//FIXME pins maybe?

	.m_min = 1,
	.m_max = 31,
	.n_min = 1,
	.n_max = 127,
	.s_limits = { 100000, 140000, 180000, },
};

static unsigned int pll_calc_fvco(unsigned int fref, int n, int m)
{
	return div_round(fref * (n + 1), m + 1);
}

static unsigned int pll_calc_fo(unsigned int fref, int n, int m, int p)
{
	return div_round(fref * (n + 1), (m + 1) * (p + 1));
}

static int calc_htotal(struct device *dev,
		       unsigned int htotal, unsigned int vtotal,
		       unsigned int len,
		       unsigned int *best_htotal,
		       unsigned int *best_htotal_last)
{
	unsigned int htotal_max;
	bool found = false;

	htotal = htotal + 2;
	htotal_max = min(htotal + 7, 2048);

	/* bigger ones produce incorrect image */
	if (htotal > 2048)
		return -EINVAL;
	/* artifacts near the bottom edge of the screen with bigger values */
	if (vtotal > 1040)
		return -EINVAL;

	for (; htotal <= htotal_max; htotal++) {
		unsigned int scrlen;
		unsigned int htotal_last;

		scrlen = htotal * (vtotal - 1);

		//dev_dbg(dev, "htotal = %u, scrlen = %u, len = %u\n", htotal, scrlen, len);

		/* 2 <= htotal_last <= htotal */
		if (len < scrlen + 2 || len > scrlen + htotal)
			continue;

		htotal_last = len - scrlen;
		if (htotal_last <= *best_htotal_last)
			continue;

		dev_dbg(dev, "TVO found best htotal = %u, htotal_last = %u\n",
			htotal, htotal_last);

		*best_htotal_last = htotal_last;
		*best_htotal = htotal;

		found = true;
		//break;//
	}

	if (!found)
		return -EINVAL;

	return 0;
}

static int pll_calc(struct mga_tvo_dev *tdev,
		    const struct mga_tvo_config *config,
		    unsigned int htotal, unsigned int vtotal,
		    const struct mga_dac_pll_info *info,
		    struct mga_dac_pll_settings *pll,
		    unsigned int *ret_htotal,
		    unsigned int *ret_htotal_last)
{
	/* 27000000/50 vs. 27000000/59.94005994 */
	unsigned int fref = config->tv_standard == DRM_TV_STD_PAL ? 540000 : 450450;
	bool found = false;
	int m, n, p;
	u8 s;

	for (p = 4; p > 0; p >>= 1) {
	for (m = info->m_max; m >= info->m_min; m--) {
	for (n = info->n_max; n >= info->n_min; n--) {
		unsigned int fvco, fo;
		unsigned int len, div;
		u8 pll_p = p - 1;

		//dev_dbg(tdev->dev, "M = %d N= %d P = %d\n", m, n, p);

		fvco = pll_calc_fvco(27000, n, m);
		//dev_dbg(tdev->dev, "Fvco = %u\n", fvco);
		if (fvco < info->fvco_min || fvco > info->fvco_max)
			continue;

		fo = pll_calc_fo(27000, n, m, pll_p);
		//dev_dbg(tdev->dev, "Fo = %u\n", fo);
		if (fo > info->fo_max)
			continue;

		len = fref * (n + 1);
		div = (m + 1) * (pll_p + 1);

		//dev_dbg(tdev->dev, "len = %u, div = %u\n", len, div);
		if (len % div)
			continue;
		len = len / div;

		if (calc_htotal(tdev->dev, htotal, vtotal, len,
				ret_htotal, ret_htotal_last))
			continue;

		dev_dbg(tdev->dev, "%s: found new best %u kHz (N=%d M=%d P=%d)\n",
			info->name, fo, n, m, pll_p);

		pll->fvco = fvco;
		pll->fo = fo;
		pll->n = n;
		pll->m = m;
		pll->p = pll_p;

		found = true;
	}
	}
	}

	if (!found)
		return -EINVAL;

	/* Loop filter bandwith. */
	for (s = 0; s < 3; s++) {
		if (pll->fvco < info->s_limits[s])
			break;
	}
	pll->p |= s << 3;

	dev_dbg(tdev->dev, "%s parameters N=%d M=%d P=%d\n",
		info->name, pll->n, pll->m, pll->p);

	return 0;

}

int calc_tvout(struct mga_tvo_dev *tdev,
	       const struct mga_tvo_config *config,
	       const struct drm_display_mode *mode,
	       struct mga_tvo_crtc_regs *regs,
	       struct mga_dac_pll_settings *pll)
{
	int ret;
	unsigned int tvo_htotal = 0, tvo_htotal_last = 0;

	dev_dbg(tdev->dev, "htotal = %u, vtotal = %u\n",
		mode->htotal, mode->vtotal);

	ret = pll_calc(tdev, config,
		       mode->htotal, mode->vtotal,
		       &tvo_pll_info, pll,
		       &tvo_htotal, &tvo_htotal_last);
	if (ret)
		return ret;

	dev_dbg(tdev->dev, "TVO htotal = %u, htotal_last = %u\n",
		tvo_htotal, tvo_htotal_last);

	unsigned int hbp = tvo_htotal - mode->hsync_start - mode->hsync_width;
	unsigned int vbp = mode->vtotal - mode->vsync_start - mode->vsync_width;

	unsigned int hscale;
	int hdisp;
	unsigned int ibmin = hbp + mode->hdisplay + 4;
	unsigned int ib;
	unsigned int i;

	//hdec = (864 << 7) / mode->htotal;
	hscale = (736 << 7) / mode->htotal;
	if (hscale < 0x41) {
		dev_dbg(tdev->dev, "TVO hscale limited to 0.5/0x40 fro %f/0x%02x\n",
			(double) hscale / 0x80, hscale);
		hscale = 0x41;
	} else if (hscale > 0x81) {
		dev_dbg(tdev->dev, "TVO hscale limited to 1.0/0x80 fro %f/0x%02x\n",
			(double) hscale / 0x80, hscale);
		hscale = 0x81;
	}

	dev_dbg(tdev->dev, "TVO hscale = %f (%u / %u)\n",
		(double) hscale / 0x80, hscale, 0x80);

	dev_dbg(tdev->dev, "TVO hdisp = %u - (%u + %u) * %f\n",
		768, hbp, mode->hdisplay, (double) hscale / 0x80);

	hscale--;

	/* feels good */
	//hdisp = (767 << 7) - (hbp + mode->hdisplay - 6) * hscale;
	hdisp = (767 << 7) - (hbp + mode->hdisplay - 8) * hscale;

	/*
	 * Register 0x9a causes slight shifts in the horizontal image position.
	 * It seems to operate in four "phases", and when the shift becomes
	 * large enough the right edge of the image may be cut off by register
	 * 0xc2. This adjustment tries to keep the right edge visible.
	 */
	//if ((hbp & 3) < 2)
	//hdisp--;

	hdisp = clamp(hdisp >> 8, 0, 0xff);

	regs->hscale = hscale;//90
	regs->hdisp = hdisp;//c2

	unsigned int vscale;
	int vdisp;
	int lines = config->tv_standard == DRM_TV_STD_PAL ? 625 : 525;

	unsigned int in = (mode->vtotal - 1) * tvo_htotal + tvo_htotal_last;
	unsigned int out = lines * tvo_htotal;
	dev_dbg(tdev->dev, "TVO in = %u, out = %u\n", in, out);
	vscale = ((u64) out << 15) / in;
	if (vscale > 0x8000) {
		dev_dbg(tdev->dev, "TVO vscale limited to 1.0/0x80000 fro %f/0x%02x\n",
			(double) vscale / 0x80000, vscale);
		vscale = 0x8000;
	}

	dev_dbg(tdev->dev, "TVO vscale = %f (%x)\n", (double) vscale / 0x8000, vscale);

	//vdisp = ((mode->vsync_width + vbp + mode->vdisplay) * vscale) >> 16;
	vdisp = ((mode->vsync_width + vbp + mode->vdisplay) * lines + mode->vtotal) /
		(mode->vtotal << 1);
	vdisp = clamp(vdisp - 146, 0, 0xff);

	vscale--;

	regs->vscale = vscale;//91
	regs->vdisp = vdisp;//be

#if 0
	i = 1;
	do {
		ib = ((((((0x7800 << 7) * i) - (0x100 << 7)) / hdec) + 0x05E7) >> 8);
		i++;
	} while (ib < ibmin);
	if (ib >= tvo_htotal)
		ib = ibmin;
#endif
#if 0
	i = 1;
	do {
		ib = ((0x3c0000 * i - 0x8000) / hdec + 0x5e7) >> 8;
		i++;
	} while (ib < ibmin);
	if (ib >= tvo_htotal)
		ib = ibmin;
#endif

	ib = ibmin;
	if (ib * hscale > (736 << 7))
		ib = (736 << 7) / hscale;
	//if (ib * hscale > (736 << 7))
	//ib -= (ib * hscale - (736 << 7)) / hscale;
	//if (ib * hscale < (736 << 7))
	//ib += ((736 << 7) - ib * hscale) / hscale;
	if (ib > tvo_htotal - 1)
		ib = tvo_htotal - 1;

	dev_dbg(tdev->dev, "TVO ib=%u ibmin=%u (max = %u) (ht=%u)\n", ib, ibmin,
		(736<<7) / hscale, tvo_htotal - 1);

	//unsigned int hvidrst = tvo_htotal_last - 2 - mode->hsync_width - config->vidrst_delay;
	//unsigned int hvidrst = tvo_htotal - mode->hsync_width - config->vidrst_delay - 8;
	unsigned int hvidrst = tvo_htotal - mode->hsync_width - config->vidrst_delay;
	//unsigned int hvidrst = tvo_htotal - 2 - mode->hsync_width - config-.vidrst_delay;
	if (tvo_htotal_last < mode->htotal)
		hvidrst += tvo_htotal_last;
	if (hvidrst > mode->htotal)
		hvidrst -= mode->htotal;
	if (hvidrst > tvo_htotal)
		hvidrst = 0;

	regs->htotal_last = tvo_htotal_last - 2;//96

	// RULE hsyncend = left edge crop
	regs->hsyncend = hbp;//9a
	// RULE hblkend = horz offset of some sort
	regs->hblkend = 4;//9c ?
	regs->hblkstr = ib;//9e
	regs->htotal = tvo_htotal - 2;//a0

	regs->vsyncend = mode->vtotal - mode->vsync_start - 1;//a2
	regs->vblkend = 1;//a4 ?
	if (tdev->version < MGA_TVO_C)
		regs->vblkend = 4;//a4 ?
	regs->vblkstr = 0;//a6 ?
	regs->vtotal = mode->vtotal - 1;//a8

	regs->hvidrst = hvidrst - 2;//aa
	regs->vvidrst = mode->vtotal - 2;//ac

	// RULE no effect
	regs->reg_98 = 0;//98 ?
	// RULE no effect
	regs->reg_ae = 0;//1;//ae ?

	return 0;
}
