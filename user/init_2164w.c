/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <stdlib.h>

#include "mga_dump.h"
#include "mga_i2c.h"
#include "mga_regs.h"
#include "mga_kms.h"
#include "modes.h"

static void mga_2164w_softreset(struct mga_dev *mdev)
{
	mga_write32(mdev, MGA_RST, MGA_RST_SOFTRESET);

	/* Sequence specifies 1 us but RST register description specifies min. 10 us. */
	udelay(10);

	mga_write32(mdev, MGA_RST, 0);

	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_MEMRESET);

	udelay(1);

	mga_wait_dwgeng_idle(mdev);
}

static void mga_2164w_rfhcnt(struct mga_dev *mdev, int gclk)
{
	int rfhcnt = 0;
	u32 option;

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);

	if (gclk > 0) {
		int gscaling_factor = (option & MGA_OPTION_NOGSCALE) ? 1 : 4;

		gclk *= 1000;

		/*
		 * 33.2 us >= (rfhcnt <3:1> * 512 + rfhcnt <0> * 64 + 1) * gclk_ period * gscaling_factor
		 * The gscaling_factor is tied to the value of nogscale, as shown below:
		 * nogscale  gscaling_factor
		 * 0         4
		 * 1         1
		 */
		rfhcnt = (gclk - 30120 * gscaling_factor) / (64 * 30120 * gscaling_factor);
		rfhcnt = clamp(rfhcnt, 0x1, 0x3F);
		/*
		 * The register doesn't have room for rfhcnt bits [2:1] so just
		 * collapse bits [2:0] into bit [0] to minimize the reduction
		 * in rfhcnt when bits [2:1] would be set.
		 */
		rfhcnt = ((rfhcnt & 0x38) >> 2) | !!(rfhcnt & 0x07);
	}

	dev_dbg(mdev->dev, "Setting rfhcnt to 0x%01x\n", rfhcnt);

	option &= ~MGA_OPTION_RFHCNT_OLD;
	option |= rfhcnt << 16;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

static void framebuffer_init(struct mga_dev *mdev)
{
	(void)mdev;
#if 1
	mga_write32(mdev, MGA_OPMODE,
	       MGA_OPMODE_DMAMOD_GENERAL_PURPOSE |
	       MGA_OPMODE_DMADATASIZ_8 |
	       MGA_OPMODE_DIRDATASIZ_8);
	mga_write32(mdev, MGA_PLNWT, 0xFFFFFFFF);
	mga_write32(mdev, MGA_MACCESS, 0);
	mga_write32(mdev, MGA_YDSTORG, 0);
	mga_write32(mdev, MGA_ZORG, 0);
#endif
}

static void mga_2164w_init(struct mga_dev *mdev)
{
	struct drm_display_mode mode = { .vrefresh = 0 };
	unsigned int outputs = 0;

	mode = mode_640_480_60;
	mode = mode_800_600_60;
	mode = mode_1024_768_60;
	//mode = mode_1280_1024_60;

	outputs |= MGA_OUTPUT_DAC1;

	mga_set_initial_crtc1_mode(mdev,
				   mdev->pixel_format,
				   &mode,
				   outputs);


	static const u32 memconfig_vals[] = {
		MGA_OPTION_MEMCONFIG_00,
		MGA_OPTION_MEMCONFIG_01,
	};
	const struct mga_pins2 *pins = &mdev->pins.pins2;
	unsigned int gclk = pins->clk_2d.gclk[0 >> 2];
	u32 option;
	struct mga_crtc1_regs regs = { .crtc[0] = 0 };
	const struct drm_display_mode *pmode;
	struct drm_framebuffer *pfb;

	mdev->interleave = false;
	mdev->nogscale = !!(pins->option & MGA_OPTION_NOGSCALE);

	mga_misc_disable_vga(mdev);

	dev_dbg(mdev->dev, "Disabling CRTC1\n");
	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);
	mga_crtc1_dpms(mdev, DRM_MODE_DPMS_OFF);

	dev_dbg(mdev->dev, "Setting interleave to %d\n", mdev->interleave);
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_MEMCONFIG;
	option |= memconfig_vals[mdev->interleave];
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	dev_dbg(mdev->dev, "Initializing TVP3026\n");
	tvp_init(&mdev->tdev, mdev->dev, mdev->mmio_virt + 0x3C00,
		 pins->fref, 0, max(220000, pins->fvco_max),
		 pins->ldclk_max, pins->fvco_max, 0);

	mga_misc_powerup(mdev);

	dev_dbg(mdev->dev, "Powering up TVP3026\n");
	tvp_powerup(&mdev->tdev, gclk);

	dev_dbg(mdev->dev, "Programming nogscale\n");
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_NOGSCALE;
	option |= pins->option & MGA_OPTION_NOGSCALE;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	dev_dbg(mdev->dev, "Powering up CRTC1\n");
	mga_crtc1_powerup(mdev);

	mga_get_crtc1_mode(mdev, &pmode);
	mga_get_crtc1_fb(mdev, &pfb);
	mode = *pmode;
	struct mga_plane_config pc = {
		.fb = pfb,
		.hscale = 0x10000,
		.vscale = 0x10000,
	};
	struct mga_crtc_config cc = {
		.adjusted_mode = mode,
	};
	mga_crtc1_calc_mode(mdev, &pc, &cc, &regs);
	mga_crtc1_restore(mdev, &regs);
	tvp_start(&mdev->tdev, pfb->pixel_format,
		  mga_calc_pixel_clock(&mode, 1),
		  mdev->interleave ? TVP_BUS_WIDTH_64 : TVP_BUS_WIDTH_32, 1);
	framebuffer_init(mdev);

	dev_dbg(mdev->dev, "Going to enable hsync/vsync\n");
	mga_crtc1_dpms(mdev, DRM_MODE_DPMS_ON);

	dev_dbg(mdev->dev, "Performing soft reset\n");
	mga_write32(mdev, MGA_RST, MGA_RST_SOFTRESET);
	udelay(200);
	mga_write32(mdev, MGA_RST, 0);

	dev_dbg(mdev->dev, "Going to calculate rhfcnt\n");
	mga_2164w_rfhcnt(mdev, gclk);
	udelay(200);

	dev_dbg(mdev->dev, "Waiting for VBL\n");
	mga_crtc1_wait_vblank(mdev);

	dev_dbg(mdev->dev, "Enabling video\n");
	mga_crtc1_video(mdev, true);

	dev_dbg(mdev->dev, "Waiting for VBL\n");
	mga_crtc1_wait_vblank(mdev);

	dev_dbg(mdev->dev, "Performing mem reset\n");
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_MEMRESET);
	udelay(1);

	dev_dbg(mdev->dev, "Waiting for drawing engine idle\n");
	mga_wait_dwgeng_idle(mdev);

	/* WRAM init done */

	mga_i2c_tvp_ddc_init(mdev);

	mdev->outputs |= MGA_OUTPUT_DAC1;

	mga_disable_outputs(mdev);

	mga_probe_mem_size(mdev);

	/*
	 * Enable interleave if necessary.
	 *
	 * 4 MB boards could be used in non-interleave mode. It would relax
	 * some alignment constraints but it would also set the bank boundary to
	 * 2 MB so placement of the scanout buffer would be harder and it reduces
	 * the RAMDAC serial port bandwith so it reduces the maximum resolution
	 * or refresh rate. So use interleave mode whenever possible.
	 */
	if (!mdev->interleave && mdev->mem_size >= 0x400000) {
		mdev->interleave = true;

		dev_dbg(mdev->dev, "Setting interleave to %d\n", mdev->interleave);
		option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
		option &= ~MGA_OPTION_MEMCONFIG;
		option |= memconfig_vals[mdev->interleave];
		pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
	}

	// FIXME gclk is the value we asked from the PLL, not the actual freq we got
	mdev->mclk = gclk;

	/* increase memory clock when possible */
	gclk = pins->clk_2d.gclk[mdev->mem_size >> 22];
	if (gclk != mdev->mclk) {
		struct tvp_pll_settings pll;

		if (tvp_mclkpll_calc(&mdev->tdev, gclk, 0, &pll) == 0 &&
		    tvp_mclkpll_program_safe(&mdev->tdev, &pll) == 0)
			mdev->mclk = pll.fpll;
	}
}

static void mga_2164w_test(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");

	mga_enable_outputs(mdev);

	// fixme
	//	if (mdev_outputs & MGA_OUTPUT_DAC1)
	//	mdev->funcs->monitor_sense_dac1(mdev);

	if (strstr(test, "reg"))
		mga_reg_test(mdev);

	mga_disable_outputs(mdev);
}

static int mga_2164w_suspend(struct mga_dev *mdev)
{
	/* FIXME implement */
	return -ENOSYS;
}

static int mga_2164w_resume(struct mga_dev *mdev)
{
	/* FIXME implement */
	return -ENOSYS;
}

static const struct mga_chip_funcs mga_2164w_funcs = {
	.rfhcnt = mga_2164w_rfhcnt,
	.monitor_sense_dac1 = mga_2064w_monitor_sense,
	.suspend = mga_2164w_suspend,
	.resume = mga_2164w_resume,
	.init = mga_2164w_init,
	.set_mode = mga_2064w_set_mode,
	.test = mga_2164w_test,
	.softreset = mga_2164w_softreset,
};

void mga_2164w_prepare(struct mga_dev *mdev)
{
	mdev->funcs = &mga_2164w_funcs;
}
