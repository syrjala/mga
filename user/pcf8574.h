/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef PCF8574_H
#define PCF8574_H

#include "kernel_emul.h"
#include "i2c.h"

struct pcf8574_dev
{
	struct i2c_adapter *adap; /*< parent I2C bus */
	u8 addr; /*< I2C address for PCF8574 */
	u8 state; /*< Output state of pins */
};

void pcf8574_gpio_set(struct pcf8574_dev *pdev, u8 mask, u8 state);
u8 pcf8574_gpio_get(struct pcf8574_dev *pdev);

int pcf8574_init(struct pcf8574_dev *pdev, struct i2c_adapter *adap, u8 addr);
void pcf8574_reset(struct pcf8574_dev *pdev);

#endif
