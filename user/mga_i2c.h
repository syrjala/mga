/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_I2C_H
#define MGA_I2C_H

#include "mga_dump.h"

int mga_i2c_init(struct mga_dev *mdev,
		 void (*setsda)(void *data, int state),
		 void (*setscl)(void *data, int state),
		 int (*getsda)(void *data),
		 int (*getscl)(void *data),
		 struct mga_i2c_channel *chan);

int mga_i2c_tvp_ddc_init(struct mga_dev *mdev);

int mga_i2c_dac_misc_init(struct mga_dev *mdev);
int mga_i2c_dac_ddc1_init(struct mga_dev *mdev);
int mga_i2c_dac_ddc2_init(struct mga_dev *mdev);
int mga_i2c_dac_ddc12_init(struct mga_dev *mdev);

#endif
