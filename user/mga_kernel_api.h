/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_KERNEL_API_H
#define MGA_KERNEL_API_H

#include <stdint.h>
#include <sys/ioctl.h>

enum {
	MGA_OFFSET_MMIO         = 0,
	MGA_OFFSET_MEM          = 32 << 20,
	MGA_OFFSET_ILOAD        = 64 << 20,
	MGA_OFFSET_DMA          = 128 << 20,
};

struct mga_resource {
	uint32_t index;
	uint32_t len;
	uint64_t off;
};

struct mga_chip {
	uint32_t chip;
	uint32_t pci_domain_nr;
	uint32_t pci_bus_nr;
	uint32_t pci_devfn;
	uint16_t pci_vendor_id;
	uint16_t pci_device_id;
	uint16_t pci_sub_vendor_id;
	uint16_t pci_sub_device_id;
	uint32_t pci_class;
};

struct mga_pci_cfg {
	uint32_t offset;
	uint32_t value;
};

enum {
	MGA_IOC_PCI_CFG_READL   = _IOWR('M', 1, struct mga_pci_cfg),
	MGA_IOC_PCI_CFG_WRITEL  = _IOW ('M', 2, struct mga_pci_cfg),
	MGA_IOC_SUSPEND         = _IO  ('M', 3),
	MGA_IOC_RESUME          = _IO  ('M', 4),
	MGA_IOC_CHIP            = _IOR ('M', 5, struct mga_chip),
	MGA_IOC_MMIO_RESOURCE   = _IOR ('M', 6, struct mga_resource),
	MGA_IOC_MEM_RESOURCE    = _IOR ('M', 7, struct mga_resource),
	MGA_IOC_ILOAD_RESOURCE  = _IOR ('M', 8, struct mga_resource),
	MGA_IOC_PCI_CFG_READB   = _IOWR('M', 9, struct mga_pci_cfg),
	MGA_IOC_PCI_CFG_WRITEB  = _IOW ('M', 10, struct mga_pci_cfg),
	MGA_IOC_DMA_RESOURCE    = _IOR ('M', 11, struct mga_resource),
	MGA_IOC_CACHE_FLUSH     = _IO  ('M', 12),
	MGA_IOC_IRQ_RESOURCE    = _IOR ('M', 13, uint32_t),
	MGA_IOC_IEN             = _IOW ('M', 14, uint32_t),
	MGA_IOC_ICLEAR          = _IOW ('M', 15, uint32_t),
	MGA_IOC_WAIT_IRQ        = _IO  ('M', 16),
};

#define PCI_DEVFN(slot, func) ((((slot) & 0x1f) << 3) | ((func) & 0x07))
#define PCI_SLOT(devfn)       (((devfn) >> 3) & 0x1f)
#define PCI_FUNC(devfn)       ((devfn) & 0x07)

enum {
	MGA_CHIP_2064W,
	MGA_CHIP_2164W,
	MGA_CHIP_1064SG,
	MGA_CHIP_1164SG,
	MGA_CHIP_G100,
	MGA_CHIP_G200,
	MGA_CHIP_G200SE,
	MGA_CHIP_G200EV,
	MGA_CHIP_G200WB,
	MGA_CHIP_G400,
	MGA_CHIP_G450,
	MGA_CHIP_G550,
};

#endif
