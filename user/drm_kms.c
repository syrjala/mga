/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <limits.h>
#include "drm_kms.h"

/* 1&2 reserved for the initial fake fbs */
static unsigned int next_id = 3;

static unsigned int alloc_id(void)
{
	if (next_id == 0)
		return 0;
	/* FIXME fixed range for props for now... */
	if (next_id >= 0x1000 && next_id < 0x2000)
		next_id = 0x2000;
	return next_id++;
}

static void free_id(unsigned int fb_id)
{
	/* nothing yet */
}

void drm_framebuffer_init(struct drm_framebuffer *fb,
			  unsigned int width,
			  unsigned int height,
			  unsigned int pixel_format,
			  unsigned int flags,
			  unsigned int offsets[4],
			  unsigned int pitches[4])
{
	fb->pixel_format = pixel_format;
	fb->flags = flags;
	fb->width = width;
	fb->height = height;

	switch (drm_format_num_planes(fb->pixel_format)) {
	case 3:
		fb->offsets[2] = offsets[2];
		fb->pitches[2] = pitches[2];
		/* fall through */
	case 2:
		fb->offsets[1] = offsets[1];
		fb->pitches[1] = pitches[1];
		/* fall through */
	case 1:
		fb->offsets[0] = offsets[0];
		fb->pitches[0] = pitches[0];
		break;
	default:
		BUG();
	}
}

struct drm_framebuffer *drm_framebuffer_create(struct drm_device *dev,
					       unsigned int width,
					       unsigned int height,
					       unsigned int pixel_format,
					       unsigned int flags,
					       unsigned int offsets[4],
					       unsigned int pitches[4])
{
	struct drm_framebuffer *fb;

	if (flags & ~DRM_FB_INTERLACED)
		return ERR_PTR(-EINVAL);

	if (!width || !height || !pixel_format || !pitches || !offsets)
		return ERR_PTR(-EINVAL);

	/* safe for signed 16.16 fixed point math */
	if (width > 0x7fff || height > 0x7fff)
		return ERR_PTR(-ERANGE);

	if (width % drm_format_horz_chroma_subsampling(pixel_format))
		return ERR_PTR(-EINVAL);

	if (height % drm_format_vert_chroma_subsampling(pixel_format))
		return ERR_PTR(-EINVAL);

	switch (drm_format_num_planes(pixel_format)) {
	case 3:
		if (pitches[2] < width / drm_format_horz_chroma_subsampling(pixel_format))
			return ERR_PTR(-EINVAL);
		/* fall through */
	case 2:
		if (pitches[1] < width / drm_format_horz_chroma_subsampling(pixel_format))
			return ERR_PTR(-EINVAL);
		/* fall through */
	case 1:
		if (pitches[0] < width * drm_format_plane_cpp(pixel_format, 0))
			return ERR_PTR(-EINVAL);
		break;
	default:
		return ERR_PTR(-EINVAL);
	}

	fb = dev->fb.create(dev, width, height,
			    pixel_format, flags,
			    offsets, pitches);
	if (IS_ERR(fb))
		return fb;

	return fb;
}

void drm_framebuffer_destroy(struct drm_device *dev,
			     struct drm_framebuffer *fb)
{
	if (!fb)
		return;

	dev->fb.pre_destroy(dev, fb);
	dev->fb.destroy(dev, fb);
}

struct drm_framebuffer *drm_framebuffer_find(struct drm_device *dev, unsigned int id)
{
	struct drm_mode_object *obj;

	mutex_lock(&dev->mode_config.mutex);
	obj = drm_mode_object_find(dev, id, DRM_MODE_OBJECT_FB);
	mutex_unlock(&dev->mode_config.mutex);

	if (!obj)
		return NULL;

	return obj_to_framebuffer(obj);
}

struct drm_crtc *drm_crtc_find(struct drm_device *dev, unsigned int id)
{
	struct drm_mode_object *obj;

	mutex_lock(&dev->mode_config.mutex);
	obj = drm_mode_object_find(dev, id, DRM_MODE_OBJECT_CRTC);
	mutex_unlock(&dev->mode_config.mutex);

	if (!obj)
		return NULL;

	return obj_to_crtc(obj);
}

struct drm_plane *drm_plane_find(struct drm_device *dev, unsigned int id)
{
	struct drm_mode_object *obj;

	mutex_lock(&dev->mode_config.mutex);
	obj = drm_mode_object_find(dev, id, DRM_MODE_OBJECT_PLANE);
	mutex_unlock(&dev->mode_config.mutex);

	if (!obj)
		return NULL;

	return obj_to_plane(obj);
}

struct drm_encoder *drm_encoder_find(struct drm_device *dev, unsigned int id)
{
	struct drm_mode_object *obj;

	mutex_lock(&dev->mode_config.mutex);
	obj = drm_mode_object_find(dev, id, DRM_MODE_OBJECT_ENCODER);
	mutex_unlock(&dev->mode_config.mutex);

	if (!obj)
		return NULL;

	return obj_to_encoder(obj);
}

struct drm_connector *drm_connector_find(struct drm_device *dev, unsigned int id)
{
	struct drm_mode_object *obj;

	mutex_lock(&dev->mode_config.mutex);
	obj = drm_mode_object_find(dev, id, DRM_MODE_OBJECT_CONNECTOR);
	mutex_unlock(&dev->mode_config.mutex);

	if (!obj)
		return NULL;

	return obj_to_connector(obj);
}

static void drm_mode_property_init(struct drm_device *dev,
			   struct drm_property *prop,
			   uint32_t prop_id, uint32_t flags)
{
	prop->base.id = prop_id;
	prop->base.type = DRM_MODE_OBJECT_PROPERTY;
	prop->flags = flags;

	pr_debug("adding prop %p to obj_list\n", &prop->base.head);
	list_add_tail(&prop->base.head, &dev->mode_config.obj_list);
	pr_debug("after add prev=%p next=%p\n",
		 prop->base.head.prev, prop->base.head.next);
}

static void drm_mode_property_range_init(struct drm_device *dev,
					 struct drm_property *prop,
					 uint32_t prop_id,
					 uint64_t min, uint64_t max)
{
	drm_mode_property_init(dev, prop, prop_id, 0);
}

void drm_modeset_init(struct drm_device *dev,
		      drm_fb_create_t create,
		      drm_fb_destroy_t destroy,
		      drm_fb_destroy_t pre_destroy)
{
	mutex_init(&dev->mode_config.mutex);

	INIT_LIST_HEAD(&dev->mode_config.plane_list);
	INIT_LIST_HEAD(&dev->mode_config.crtc_list);
	INIT_LIST_HEAD(&dev->mode_config.encoder_list);
	INIT_LIST_HEAD(&dev->mode_config.connector_list);
	INIT_LIST_HEAD(&dev->mode_config.fb_list);

	pr_debug("init obj_list %p\n", &dev->mode_config.obj_list);
	INIT_LIST_HEAD(&dev->mode_config.obj_list);
	pr_debug("after init prev=%p next=%p\n",
		 dev->mode_config.obj_list.prev, dev->mode_config.obj_list.next);

	dev->fb.create = create;
	dev->fb.destroy = destroy;
	dev->fb.pre_destroy = pre_destroy;

	drm_mode_property_range_init(dev, &dev->mode_config.prop_crtc_id,
				     DRM_MODE_PROP_CRTC_ID, 0, UINT_MAX);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_fb_id,
				     DRM_MODE_PROP_FB_ID, 0, UINT_MAX);

	drm_mode_property_range_init(dev, &dev->mode_config.prop_src_x,
				     DRM_MODE_PROP_SRC_X, 0, UINT_MAX);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_src_y,
				     DRM_MODE_PROP_SRC_Y, 0, UINT_MAX);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_src_w,
				     DRM_MODE_PROP_SRC_W, 0, UINT_MAX);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_src_h,
				     DRM_MODE_PROP_SRC_H, 0, UINT_MAX);

	drm_mode_property_range_init(dev, &dev->mode_config.prop_crtc_x,
				     DRM_MODE_PROP_CRTC_X, INT_MIN, INT_MAX);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_crtc_y,
				     DRM_MODE_PROP_CRTC_Y, INT_MIN, INT_MAX);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_crtc_w,
				     DRM_MODE_PROP_CRTC_W, 0, UINT_MAX);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_crtc_h,
				     DRM_MODE_PROP_CRTC_H, 0, UINT_MAX);

	drm_mode_property_init(dev, &dev->mode_config.prop_mode,
			       DRM_MODE_PROP_MODE, DRM_MODE_PROP_BLOB);
	drm_mode_property_init(dev, &dev->mode_config.prop_connector_ids,
			       DRM_MODE_PROP_CONNECTOR_IDS, DRM_MODE_PROP_BLOB);

	drm_mode_property_range_init(dev, &dev->mode_config.prop_rotate,
				     DRM_MODE_PROP_ROTATE,
				     DRM_ROTATE_0, DRM_ROTATE_270 | DRM_REFLECT_X | DRM_REFLECT_Y);

	drm_mode_property_init(dev, &dev->mode_config.prop_lut,
			       DRM_MODE_PROP_LUT, DRM_MODE_PROP_BLOB);

	drm_mode_property_range_init(dev, &dev->mode_config.prop_overlay,
				     DRM_MODE_PROP_OVERLAY, 0, 1);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_overlay_color_key,
				     DRM_MODE_PROP_OVERLAY_COLOR_KEY, 0, 0xff);

	drm_mode_property_range_init(dev, &dev->mode_config.prop_tv_std,
				     DRM_MODE_PROP_TV_STD, 0, 1);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_cable_type,
				     DRM_MODE_PROP_CABLE_TYPE, 0, 2);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_text_filter,
				     DRM_MODE_PROP_TEXT_FILTER, 0, 1);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_deflicker,
				     DRM_MODE_PROP_DEFLICKER, 0, 2);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_color_bars,
				     DRM_MODE_PROP_COLOR_BARS, 0, 1);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_dot_crawl_freeze,
				     DRM_MODE_PROP_DOT_CRAWL_FREEZE, 0, 1);
	drm_mode_property_range_init(dev, &dev->mode_config.prop_gamma,
				     DRM_MODE_PROP_GAMMA, 4, 40);
}

void drm_modeset_fini(struct drm_device *dev)
{
	struct drm_framebuffer *fb, *fb_next;
	struct drm_plane *plane, *plane_next;
	struct drm_crtc *crtc, *crtc_next;
	struct drm_encoder *encoder, *encoder_next;
	struct drm_connector *connector, *connector_next;

	mutex_lock(&dev->mode_config.mutex);

	list_for_each_entry_safe(fb, fb_next, &dev->mode_config.fb_list, head)
		drm_framebuffer_destroy(dev, fb);

	list_for_each_entry_safe(plane, plane_next, &dev->mode_config.plane_list, head)
		plane->destroy(plane);
	list_for_each_entry_safe(crtc, crtc_next, &dev->mode_config.crtc_list, head)
		crtc->destroy(crtc);
	list_for_each_entry_safe(encoder, encoder_next, &dev->mode_config.encoder_list, head)
		encoder->destroy(encoder);
	list_for_each_entry_safe(connector, connector_next, &dev->mode_config.connector_list, head)
		connector->destroy(connector);

	mutex_unlock(&dev->mode_config.mutex);

	drm_mode_object_fini(&dev->mode_config.prop_crtc_id.base);
	drm_mode_object_fini(&dev->mode_config.prop_fb_id.base);
	drm_mode_object_fini(&dev->mode_config.prop_src_x.base);
	drm_mode_object_fini(&dev->mode_config.prop_src_y.base);
	drm_mode_object_fini(&dev->mode_config.prop_src_w.base);
	drm_mode_object_fini(&dev->mode_config.prop_src_h.base);
	drm_mode_object_fini(&dev->mode_config.prop_crtc_x.base);
	drm_mode_object_fini(&dev->mode_config.prop_crtc_y.base);
	drm_mode_object_fini(&dev->mode_config.prop_crtc_w.base);
	drm_mode_object_fini(&dev->mode_config.prop_crtc_h.base);
	drm_mode_object_fini(&dev->mode_config.prop_mode.base);
	drm_mode_object_fini(&dev->mode_config.prop_connector_ids.base);
}

int drm_format_plane_cpp(unsigned int pixel_format, int plane)
{
	switch (pixel_format) {
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
		return 2;
	case DRM_FORMAT_RGB888:
		return 3;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		return 4;
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
		return plane ? 2 : 1;
	case DRM_FORMAT_C8:
	case DRM_FORMAT_AC44:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		return 1;
	default:
		return 0;
	}
}

int drm_format_bits_per_pixel(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
		return 16;
	case DRM_FORMAT_RGB888:
		return 24;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		return 32;
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_C8:
	case DRM_FORMAT_AC44:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		return 8;
	case DRM_FORMAT_C6:
		return 6;
	case DRM_FORMAT_C4:
		return 4;
	case DRM_FORMAT_C2:
		return 2;
	case DRM_FORMAT_C1:
		return 1;
	default:
		return 0;
	}
}

int drm_format_num_planes(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		return 3;
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
		return 2;
	default:
		return 1;
	}
}

int drm_format_horz_chroma_subsampling(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
		return 2;
	default:
		return 1;
	}
}

int drm_format_vert_chroma_subsampling(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
		return 2;
	default:
		return 1;
	}
}

int drm_format_depth(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_C8:
		return 8;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
		return 15;
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
		return 16;
	case DRM_FORMAT_RGB888:
		return 24;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		return 32;
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		return 12;
	default:
		return 0;
	}
}

int drm_format_bpp(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_C8:
		return 8;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
		return 16;
	case DRM_FORMAT_RGB888:
		return 24;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		return 32;
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		return 12;
	default:
		return 0;
	}
}

bool drm_framebuffer_equal(const struct drm_framebuffer *fb1,
			   const struct drm_framebuffer *fb2)
{
	/* no need to look at anything else */
	return fb1->base.id == fb2->base.id;
}

bool drm_mode_equal(const struct drm_display_mode *mode1,
		    const struct drm_display_mode *mode2)
{
	/* ignore id, it's not a meaningful indicator of anything */
	return  mode1->vrefresh == mode2->vrefresh &&
		mode1->clock == mode2->clock &&
		mode1->flags == mode2->flags &&
		mode1->vtotal == mode2->vtotal &&
		mode1->vdisplay == mode2->vdisplay &&
		mode1->vblank_start == mode2->vblank_start &&
		mode1->vblank_width == mode2->vblank_width &&
		mode1->vsync_start == mode2->vsync_start &&
		mode1->vsync_width == mode2->vsync_width &&
		mode1->htotal == mode2->htotal &&
		mode1->hdisplay == mode2->hdisplay &&
		mode1->hblank_start == mode2->hblank_start &&
		mode1->hblank_width == mode2->hblank_width &&
		mode1->hsync_start == mode2->hsync_start &&
		mode1->hsync_width == mode2->hsync_width;
}

void drm_generate_palette(unsigned int pixel_format,
			  struct drm_color pal[256])
{
	int i, rsz, gsz, bsz;

	switch (pixel_format) {
	case DRM_FORMAT_C8:
		rsz = 7;
		gsz = 7;
		bsz = 3;
		break;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
		rsz = 31;
		gsz = 31;
		bsz = 31;
		break;
	case DRM_FORMAT_RGB565:
		rsz = 31;
		gsz = 63;
		bsz = 31;
		break;
	case DRM_FORMAT_RGB888:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		rsz = 255;
		gsz = 255;
		bsz = 255;
		break;
	default:
		memset(pal, 0, sizeof pal[0] * 256);
		return;
	}

	if (pixel_format == DRM_FORMAT_C8) {
#if 1
		int r, g, b;

		i = 0;
		for (r = 0; r <= rsz; r++)
		for (g = 0; g <= gsz; g++)
		for (b = 0; b <= bsz; b++) {
			pal[i].r = r * 255 / rsz;
			pal[i].g = g * 255 / gsz;
			pal[i].b = b * 255 / bsz;
			i++;
		}
#else
		for (i = 0; i <= 0xff; i++) {
			pal[i].r = pal[i].g = pal[i].b = i;
		}
#endif
	} else {
		/* Load the gfx palette */
		for (i = 0; i <= rsz; i++)
			pal[i].r = i * 255 / rsz;
		for (i = 0; i <= gsz; i++)
			pal[i].g = i * 255 / gsz;
		for (i = 0; i <= bsz; i++)
			pal[i].b = i * 255 / bsz;

		if (pixel_format == DRM_FORMAT_XRGB1555) {
#if 0
			/* Load the overlay palette */
			for (i = 0; i <= rsz; i++)
				pal[i | 0x80].r = i * 255 / 2 / rsz;
			for (i = 0; i <= gsz; i++)
				pal[i | 0x80].g = i * 255 / 2 / gsz;
			for (i = 0; i <= bsz; i++)
				pal[i | 0x80].b = i * 255 / 2 / bsz;
#else
			/* Load the overlay palette */
			for (i = 0; i <= rsz; i++)
				pal[i | 0x80].r = 0xff;
			for (i = 0; i <= gsz; i++)
				pal[i | 0x80].g = 0x00;
			for (i = 0; i <= bsz; i++)
				pal[i | 0x80].b = 0x00;
#endif
		} else if (0) {
			/* G16V16 */
			/* Load the video palette */
			for (i = 0; i <= rsz; i++)
				pal[i | 0x20].r = i * 255 / rsz;
			for (i = 0; i <= gsz; i++)
				pal[i | 0x20].g = i * 255 / gsz;
			for (i = 0; i <= bsz; i++)
				pal[i | 0x20].b = i * 255 / bsz;
		}
	}

	for (i = 0; i < 256; i++) {
		pal[i].r >>= 1;
		pal[i].g >>= 1;
		pal[i].b >>= 1;
	}

#if 0
	pal[0].r = 0xff;
	pal[0].g = 0;
	pal[0].b = 0;
#endif
#if 0
	pal[0].r = 0;
	pal[0].g = 0xff;
	pal[0].b = 0;
	pal[1].r = 0xff;
	pal[1].g = 0;
	pal[1].b = 0;
	pal[255].r = 0;
	pal[255].g = 0;
	pal[255].b = 0xff;
#endif
}

int drm_mode_object_init(struct drm_device *dev,
			 struct drm_mode_object *obj,
			 unsigned int type)
{
	obj->id = alloc_id();
	if (!obj->id)
		return -ENOSPC;

	obj->type = type;

	pr_debug("adding obj %p to obj_list\n", &obj->head);
	list_add_tail(&obj->head, &dev->mode_config.obj_list);
	pr_debug("after add prev=%p next=%p\n",
		 obj->head.prev, obj->head.next);

	return 0;
}

void drm_mode_object_fini(struct drm_mode_object *obj)
{
	if (obj->type != DRM_MODE_OBJECT_PROPERTY)
		free_id(obj->id);
	list_del(&obj->head);
}

struct drm_mode_object *drm_mode_object_find(struct drm_device *dev,
					     unsigned int id,
					     unsigned int type)
{
	struct drm_mode_object *obj;

	list_for_each_entry(obj, &dev->mode_config.obj_list, head)
		if (obj->id == id && (type == DRM_MODE_OBJECT_ANY || obj->type == type))
			return obj;

	return NULL;
}

struct drm_display_mode *drm_mode_create(struct drm_device *dev)
{
	struct drm_display_mode *mode;
	int r;

	mode = kzalloc(sizeof *mode, GFP_KERNEL);
	if (!mode)
		return NULL;

	mutex_lock(&dev->mode_config.mutex);
	r = drm_mode_object_init(dev, &mode->base, DRM_MODE_OBJECT_MODE);
	mutex_unlock(&dev->mode_config.mutex);

	if (r) {
		kfree(mode);
		return NULL;
	}

	return mode;
}

void drm_mode_destroy(struct drm_device *dev, struct drm_display_mode *mode)
{
	if (!mode)
		return;

	mutex_lock(&dev->mode_config.mutex);
	drm_mode_object_fini(&mode->base);
	mutex_unlock(&dev->mode_config.mutex);

	kfree(mode);
}

/* mode_config.mutex must be locked */
void drm_mode_copy(struct drm_display_mode *dst,
		   const struct drm_display_mode *src)
{
	unsigned int id = dst->base.id;
	struct list_head head = dst->base.head;
	*dst = *src;
	dst->base.head = head;
	dst->base.id = id;
}

struct drm_display_mode *drm_mode_duplicate(struct drm_device *dev,
					    const struct drm_display_mode *mode)
{
	struct drm_display_mode *new;

	if (!mode)
		return NULL;

	new = drm_mode_create(dev);
	if (!new)
		return NULL;

	drm_mode_copy(new, mode);

	return new;
}

int drm_convert_umode(struct drm_display_mode *mode, const struct drm_umode *umode)
{
	drm_mode_copy(mode, &umode->mode);

	return 0;
}

int drm_format_horz_subsampling(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		return 2;
	default:
		return 1;
	}
}

int drm_format_vert_subsampling(unsigned int pixel_format)
{
	switch (pixel_format) {
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		return 2;
	default:
		return 1;
	}
}

unsigned int drm_format_plane_width(unsigned int pixel_format, unsigned int width, int plane)
{
	if (plane == 0)
		return width;

	return width / drm_format_horz_subsampling(pixel_format);
}

unsigned int drm_format_plane_height(unsigned int pixel_format, unsigned int height, int plane)
{
	if (plane == 0)
		return height;

	return height / drm_format_vert_subsampling(pixel_format);
}

void drm_mode_print(struct device *dev, const struct drm_display_mode *mode)
{
	dev_dbg(dev,
		" vrefresh     = %f Hz\n"
		" dblscan      = %d\n"
		" interlace    = %d\n"
		" htotal       = %lu\n"
		" hdisplay     = %lu\n"
		" hblank_start = %lu\n"
		" hblank_width = %lu\n"
		" hsync_start  = %lu\n"
		" hsync_width  = %lu\n"
		" vtotal       = %lu\n"
		" vdisplay     = %lu\n"
		" vblank_start = %lu\n"
		" vblank_width = %lu\n"
		" vsync_start  = %lu\n"
		" vsync_width  = %lu\n",
		mode->vrefresh / 1000.0,
		!!(mode->flags & DRM_MODE_FLAG_DBLSCAN),
		!!(mode->flags & DRM_MODE_FLAG_INTERLACE),
		mode->htotal,
		mode->hdisplay,
		mode->hblank_start,
		mode->hblank_width,
		mode->hsync_start,
		mode->hsync_width,
		mode->vtotal,
		mode->vdisplay,
		mode->vblank_start,
		mode->vblank_width,
		mode->vsync_start,
		mode->vsync_width);
}

bool drm_region_equal(const struct drm_region *a,
		      const struct drm_region *b)
{
	return memcmp(a, b, sizeof *a) == 0;
}

void drm_region_print(const struct drm_region *r,
		      const char *name, bool fixed_point)
{
	int w = drm_region_width(r);
	int h = drm_region_height(r);

	if (fixed_point) {
		const int mul = 1000000 * (0x10000 >> 10);

		pr_debug("region \"%s\": %d.%06ux%d.%06u%+d.%06u%+d.%06u\n", name,
			 w >> 16, ((w & 0xffff) * mul) >> 10,
			 h >> 16, ((h & 0xffff) * mul) >> 10,
			 r->x1 >> 16, ((r->x1 & 0xffff) * mul) >> 10,
			 r->y1 >> 16, ((r->y1 & 0xffff) * mul) >> 10);
	} else
		pr_debug("region \"%s\": %dx%d%+d%+d\n", name,
			 w, h, r->x1, r->y1);
}

int drm_framebuffer_check_viewport(const struct drm_framebuffer *fb,
				   uint32_t x, uint32_t y,
				   uint32_t w, uint32_t h)
{
	uint32_t fb_width = fb->width << 16;
	uint32_t fb_height = fb->height << 16;

	BUG_ON(fb->width > 0x7fff || fb->height > 0x7fff);

	if (w > fb_width || x > fb_width - w ||
	    h > fb_height || y > fb_height - h)
		return -ENOSPC;

	return 0;
}
