/* -------------------------------------------------------------------------
 * i2c-algo-bit.c i2c driver algorithms for bit-shift adapters
 * -------------------------------------------------------------------------
 *   Copyright (C) 1995-2000 Simon G. Vogl

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 * ------------------------------------------------------------------------- */

/* With some changes from Frodo Looijaard <frodol@dds.nl>, Kyösti Mälkki
   <kmalkki@cc.hut.fi> and Jean Delvare <jdelvare@suse.de> */

/* Mostly lifted from Linux kernel drivers/i2c/algos/i2c-algo-bit.c */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define pr_fmt(fmt) "i2c: " fmt

#include "mga_dump.h"

#define DEBUG(x...) do { } while (0)
//#define DEBUG pr_debug

#define setsda(adap, val) (adap)->setsda((adap)->data, (val))
#define setscl(adap, val) (adap)->setscl((adap)->data, (val))
#define getsda(adap) (adap)->getsda((adap)->data)
#define getscl(adap) (adap)->getscl((adap)->data)

static int sclhi(struct i2c_algo_bit_data *adap)
{
	DEBUG("%s start\n", __func__);

	setscl(adap, 1);
	while (!getscl(adap))
		;
	udelay(adap->udelay);
	return 0;

	DEBUG("%s end\n", __func__);
}

static void scllo(struct i2c_algo_bit_data *adap)
{
	DEBUG("%s start\n", __func__);

	setscl(adap, 0);
	udelay(DIV_ROUND_UP(adap->udelay, 2));

	DEBUG("%s end\n", __func__);
}

static void sdalo(struct i2c_algo_bit_data *adap)
{
	DEBUG("%s start\n", __func__);

	setsda(adap, 0);
	udelay(DIV_ROUND_UP(adap->udelay, 2));

	DEBUG("%s end\n", __func__);
}

static void sdahi(struct i2c_algo_bit_data *adap)
{
	DEBUG("%s start\n", __func__);

	setsda(adap, 1);
	udelay(DIV_ROUND_UP(adap->udelay, 2));

	DEBUG("%s end\n", __func__);
}

static void i2c_start(struct i2c_algo_bit_data *adap)
{
	DEBUG("%s start\n", __func__);

	//assert(scl && sda);
	setsda(adap, 0);
	udelay(adap->udelay);
	scllo(adap);

	DEBUG("%s end\n", __func__);
}

static void i2c_repstart(struct i2c_algo_bit_data *adap)
{
	DEBUG("%s start\n", __func__);

	//assert(!scl);
	sdahi(adap);
	sclhi(adap);
	setsda(adap, 0);
	udelay(adap->udelay);
	scllo(adap);

	DEBUG("%s end\n", __func__);
}

static void i2c_stop(struct i2c_algo_bit_data *adap)
{
	DEBUG("%s start\n", __func__);

	//assert(!scl);
	sdalo(adap);
	sclhi(adap);
	setsda(adap, 1);
	udelay(adap->udelay);

	DEBUG("%s end\n", __func__);
}

static int i2c_outb(struct i2c_adapter *i2c_adap, u8 c)
{
	int i;
	int sb;
	int ack;
	struct i2c_algo_bit_data *adap = i2c_adap->algo_data;

	DEBUG("%s start\n", __func__);

	for (i = 7; i >= 0; i--) {
		sb = (c >> i) & 1;
		setsda(adap, sb);
		udelay(DIV_ROUND_UP(adap->udelay, 2));
		if (sclhi(adap) < 0)
			return -ETIMEDOUT;
		scllo(adap);
	}
	sdahi(adap);
	if (sclhi(adap) < 0)
		return -ETIMEDOUT;

	ack = !getsda(adap);
	scllo(adap);

	DEBUG("%s end\n", __func__);

	return ack;
}

static int i2c_inb(struct i2c_adapter *i2c_adap)
{
	int i;
	u8 indata = 0;
	struct i2c_algo_bit_data *adap = i2c_adap->algo_data;

	DEBUG("%s start\n", __func__);

	sdahi(adap);

	for (i = 0; i < 8; i++) {
		if (sclhi(adap) < 0)
			return -ETIMEDOUT;

		indata <<= 1;
		if (getsda(adap))
			indata |= 1;

		setscl(adap, 0);

		udelay(DIV_ROUND_UP(adap->udelay, i == 7 ? 2 : 1));
	}

	DEBUG("%s end\n", __func__);

	return indata;
}

static int try_address(struct i2c_adapter *i2c_adap, u8 addr, int retries)
{
	struct i2c_algo_bit_data *adap = i2c_adap->algo_data;
	int i, ret = 0;

	DEBUG("%s start\n", __func__);

	for (i = 0; i <= retries; i++) {
		ret = i2c_outb(i2c_adap, addr);
		if (ret == 1 || i == retries)
			break;
		i2c_stop(adap);
		udelay(adap->udelay);
		//yield();
		i2c_start(adap);
	}

	DEBUG("%s end\n", __func__);

	return ret;
}

static int sendbytes(struct i2c_adapter *i2c_adap, struct i2c_msg *msg)
{
	const u8 *temp = msg->buf;
	int count = msg->len;
	int wrcount = 0;
	int ret;

	DEBUG("%s start\n", __func__);

	while (count > 0) {
		ret = i2c_outb(i2c_adap, *temp);

		if (ret > 0) {
			count--;
			temp++;
			wrcount++;
		} else if (ret == 0) {
			return -EIO;
		} else {
			return ret;
		}
	}

	DEBUG("%s end\n", __func__);

	return wrcount;
}

static int acknak(struct i2c_adapter *i2c_adap, bool is_ack)
{
	struct i2c_algo_bit_data *adap = i2c_adap->algo_data;

	DEBUG("%s start\n", __func__);

	if (is_ack)
		setsda(adap, 0);
	udelay(DIV_ROUND_UP(adap->udelay, 2));
	if (sclhi(adap) < 0)
		return -ETIMEDOUT;
	scllo(adap);

	DEBUG("%s end\n", __func__);

	return 0;
}

static int readbytes(struct i2c_adapter *i2c_adap, struct i2c_msg *msg)
{
	int ret;
	int rdcount = 0;
	u8 *temp = msg->buf;
	int count = msg->len;

	DEBUG("%s start\n", __func__);

	while (count > 0) {
		ret = i2c_inb(i2c_adap);
		if (ret < 0)
			break;

		*temp++ = ret;
		rdcount++;
		count--;

		ret = acknak(i2c_adap, count);
		if (ret < 0)
			return ret;
	}

	DEBUG("%s end\n", __func__);

	return rdcount;
}

static int bit_doAddress(struct i2c_adapter *i2c_adap, const struct i2c_msg *msg)
{
	unsigned int flags = msg->flags;
	u8 addr;
	int ret, retries;

	DEBUG("%s start\n", __func__);

	retries = i2c_adap->retries;

	addr = msg->addr << 1;

	if (flags & I2C_M_RD)
		addr |= 1;

	if (flags & I2C_M_REV_DIR_ADDR)
		addr ^= 1;

	ret = try_address(i2c_adap, addr, retries);
	if (ret != 1)
		return -ENXIO;

	DEBUG("%s end\n", __func__);

	return 0;
}

static int bit_xfer(struct i2c_adapter *i2c_adap, struct i2c_msg msgs[], int num)
{
	struct i2c_msg *pmsg;
	struct i2c_algo_bit_data *adap = i2c_adap->algo_data;
	int i;
	int ret;

	DEBUG("%s start\n", __func__);

	i2c_start(adap);

	for (i = 0; i < num; i++) {
		pmsg = &msgs[i];

		if (!(pmsg->flags & I2C_M_NOSTART)) {
			if (i)
				i2c_repstart(adap);
			ret = bit_doAddress(i2c_adap, pmsg);
			if (ret)
				goto bailout;
		}

		if (pmsg->flags & I2C_M_RD)
			ret = readbytes(i2c_adap, pmsg);
		else
			ret = sendbytes(i2c_adap, pmsg);

		if (ret < pmsg->len) {
			if (ret >= 0)
				ret = -EREMOTEIO;
			goto bailout;
		}
	}
	ret = i;

 bailout:
	i2c_stop(adap);

	DEBUG("%s end\n", __func__);

	return ret;
}

void i2c_set_adapdata(struct i2c_adapter *adap, void *data)
{
	dev_set_drvdata(&adap->dev, data);
}

void *i2c_get_adapdata(struct i2c_adapter *adap)
{
	return dev_get_drvdata(&adap->dev);
}


static int i2c_bit_prepare_bus(struct i2c_adapter *adap)
{
	/* register new adapter to i2c module... */
	adap->timeout = 100;	/* default values, should       */
	adap->retries = 3;	/* be replaced by defines       */

	return 0;
}

static struct i2c_adapter *i2c_adapters[8];
static unsigned int num_i2c_adapters;

static int i2c_add_adapter(struct i2c_adapter *adap)
{
	if (num_i2c_adapters >= ARRAY_SIZE(i2c_adapters))
		return -ENOMEM;
	adap->id = num_i2c_adapters;
	i2c_adapters[num_i2c_adapters] = adap;
	++num_i2c_adapters;
	return 0;
}

int i2c_bit_add_bus(struct i2c_adapter *adap)
{
	int err;

	err = i2c_bit_prepare_bus(adap);
	if (err)
		return err;

	return i2c_add_adapter(adap);
}

int i2c_transfer(struct i2c_adapter * adap, struct i2c_msg *msgs, int num)
{
	return bit_xfer(adap, msgs, num);
}


int i2c_smbus_write_byte(struct i2c_adapter *adap, u8 addr, u8 data)
{
	u8 buf[1];
	int ret;

	buf[0] = data;

	struct i2c_msg msg[] = {
		{ addr, 0, 1, &buf[0] },
	};

	ret = i2c_transfer(adap, msg, 1);
	if (ret < 0)
		return ret;

	return 0;
}

int i2c_smbus_read_byte(struct i2c_adapter *adap, u8 addr)
{
	u8 buf[1];
	int ret;

	buf[0] = 0;

	struct i2c_msg msg[] = {
		{ addr, I2C_M_RD, 1, &buf[0] },
	};

	ret = i2c_transfer(adap, msg, 1);
	if (ret < 0)
		return ret;

	return buf[0];
}

int i2c_smbus_write_byte_data(struct i2c_adapter *adap,
			      u8 addr, u8 command, u8 data)
{
	u8 buf[2];
	int ret;

	buf[0] = command;
	buf[1] = data;

	//pr_debug("I2C: writing %x = %x\n", command, data);

	struct i2c_msg msg[] = {
		{ addr, 0, 2, &buf[0] },
	};

	ret = i2c_transfer(adap, msg, 1);
	if (ret < 0)
		return ret;

	return 0;
}

int i2c_smbus_write_word_data(struct i2c_adapter *adap,
			      u8 addr, u8 command, u16 data)
{
	u8 buf[3];
	int ret;

	buf[0] = command;
	buf[1] = data;
	buf[2] = data >> 8;

	//pr_debug("I2C: writing %x = %x\n", command, data);

	struct i2c_msg msg[] = {
		{ addr, 0, 3, &buf[0] },
	};

	ret = i2c_transfer(adap, msg, 1);
	if (ret < 0)
		return ret;

	return 0;
}

static int i2c_smbus_dwrite_word_data(struct i2c_adapter *adap,
				      u8 addr, u8 command, u32 data)
{
	u8 buf[5];
	int ret;

	buf[0] = command;
	buf[1] = data;
	buf[2] = data >> 8;
	buf[3] = data >> 16;
	buf[4] = data >> 24;

	//pr_debug("I2C: writing %x = %x\n", command, data);

	struct i2c_msg msg[] = {
		{ addr, 0, 5, &buf[0] },
	};

	ret = i2c_transfer(adap, msg, 1);
	if (ret < 0)
		return ret;

	return 0;
}

static int i2c_smbus_read_byte_data(struct i2c_adapter *adap,
				    u8 addr, u8 command)
{
	u8 buf[2];
	int ret;

	buf[0] = command;
	buf[1] = 0;

	struct i2c_msg msg[] = {
		{ addr, 0,        1, &buf[0] },
		{ addr, I2C_M_RD, 1, &buf[1] },
	};

	ret = i2c_transfer(adap, msg, 2);
	if (ret < 0)
		return ret;

	return buf[1];
}

static int i2c_smbus_read_word_data(struct i2c_adapter *adap,
				    u8 addr, u8 command)
{
	u8 buf[3];
	int ret;

	buf[0] = command;
	buf[1] = 0;
	buf[2] = 0;

	struct i2c_msg msg[] = {
		{ addr, 0,        1, &buf[0] },
		{ addr, I2C_M_RD, 2, &buf[1] },
	};

	ret = i2c_transfer(adap, msg, 2);
	if (ret < 0)
		return ret;

	return (buf[2] << 8) | buf[1];
}

int i2c_smbus_read_buf(struct i2c_adapter *adap,
		       u8 addr, u8 command,
		       u8 buf[], size_t len)
{
	int ret;

	buf[0] = command;

	struct i2c_msg msg[] = {
		{ addr, 0,          1, &command },
		{ addr, I2C_M_RD, len, buf },
	};

	ret = i2c_transfer(adap, msg, 2);
	if (ret < 0)
		return ret;

	return len;
}

int i2c_probe(struct i2c_adapter *i2c_adap, u8 addr)
{
	int ret;
	u8 buf[1];
	struct i2c_msg msg = { addr, I2C_M_RD, 1, &buf[0] };
	struct i2c_algo_bit_data *adap = i2c_adap->algo_data;

	i2c_start(adap);

	ret = bit_doAddress(i2c_adap, &msg);

	i2c_stop(adap);

	return ret;
}
