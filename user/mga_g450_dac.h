/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_G450_DAC_H
#define MGA_G450_DAC_H

#include "mga_dump.h"
#include "mga_dac_common.h"

enum {
	MGA_PLL_SETTINGS_MAX = 64,
};

enum mga_rset {
	MGA_RSET_0_7_V,
	MGA_RSET_1_0_V,
};

/* PIXPLLA */
int mga_g450_dac_pixplla_calc(struct mga_dev *mdev,
			      unsigned int freq, unsigned int fmax,
			      struct mga_dac_pll_settings *pll,
			      unsigned int *pll_count);
void mga_g450_dac_pixplla_save(struct mga_dev *mdev,
			       struct mga_dac_pll_settings *pll);
int mga_g450_dac_pixplla_program(struct mga_dev *mdev,
				 const struct mga_dac_pll_settings *pll,
				 unsigned int pll_count);
int mga_g450_dac_pixplla_wait(struct mga_dev *mdev);

/* PIXPLLB */
int mga_g450_dac_pixpllb_calc(struct mga_dev *mdev,
			      unsigned int freq, unsigned int fmax,
			      struct mga_dac_pll_settings *pll,
			      unsigned int *pll_count);
void mga_g450_dac_pixpllb_save(struct mga_dev *mdev,
			       struct mga_dac_pll_settings *pll);
int mga_g450_dac_pixpllb_program(struct mga_dev *mdev,
				 const struct mga_dac_pll_settings *pll,
				 unsigned int pll_count);
int mga_g450_dac_pixpllb_wait(struct mga_dev *mdev);

/* PIXPLLC */
int mga_g450_dac_pixpllc_calc(struct mga_dev *mdev,
			      unsigned int freq, unsigned int fmax,
			      struct mga_dac_pll_settings *pll,
			      unsigned int *pll_count);
void mga_g450_dac_pixpllc_save(struct mga_dev *mdev,
			       struct mga_dac_pll_settings *pll);
int mga_g450_dac_pixpllc_program(struct mga_dev *mdev,
				 const struct mga_dac_pll_settings *pll,
				 unsigned int pll_count);
int mga_g450_dac_pixpllc_wait(struct mga_dev *mdev);

int mga_g450_dac_syspll_calc(struct mga_dev *mdev,
			     unsigned int freq, unsigned int fmax,
			     struct mga_dac_pll_settings *pll,
			     unsigned int *pll_count);
void mga_g450_dac_syspll_save(struct mga_dev *mdev,
			      struct mga_dac_pll_settings *pll);
int mga_g450_dac_syspll_program(struct mga_dev *mdev,
				const struct mga_dac_pll_settings *pll,
				unsigned int pll_count);
int mga_g450_dac_syspll_wait(struct mga_dev *mdev);

int mga_g450_dac_vidpll_calc(struct mga_dev *mdev,
			     unsigned int freq, unsigned int fmax,
			     struct mga_dac_pll_settings *pll,
			     unsigned int *pll_count);
void mga_g450_dac_vidpll_save(struct mga_dev *mdev,
			      struct mga_dac_pll_settings *pll);
int mga_g450_dac_vidpll_program(struct mga_dev *mdev,
				const struct mga_dac_pll_settings *pll,
				unsigned int pll_count);
int mga_g450_dac_vidpll_wait(struct mga_dev *mdev);

void mga_g450_dac_vidpll_power(struct mga_dev *mdev, bool on);
void mga_g450_dac_crtc2_power(struct mga_dev *mdev, bool on);
void mga_g450_dac_dac2_power(struct mga_dev *mdev, bool on);
void mga_g450_dac_panel_power(struct mga_dev *mdev, bool on);

int mga_g450_dac_panel_mode(struct mga_dev *mdev, unsigned int pixclk);

void mga_g450_dac_dac1_source(struct mga_dev *mdev, unsigned int crtc);
void mga_g450_dac_dac2_source(struct mga_dev *mdev, unsigned int crtc, bool tve);
void mga_g450_dac_panel_source(struct mga_dev *mdev, unsigned int crtc, bool bt656);

void mga_g450_dac_dac1_dpms(struct mga_dev *mdev, unsigned int mode);
void mga_g450_dac_dac2_dpms(struct mga_dev *mdev, unsigned int mode);

void mga_g450_dac_dac1_set_sync(struct mga_dev *mdev, unsigned int sync);
void mga_g450_dac_dac2_set_sync(struct mga_dev *mdev, unsigned int sync);

void mga_g450_dac_init(struct mga_dev *mdev,
		       unsigned int fref,
		       unsigned int pixpll_fvco_min,
		       unsigned int pixpll_fvco_max,
		       unsigned int syspll_fvco_min,
		       unsigned int syspll_fvco_max,
		       unsigned int vidpll_fvco_min,
		       unsigned int vidpll_fvco_max);

void mga_g450_dac_powerup(struct mga_dev *mdev);

int mga_g450_dac_power_down(struct mga_dev *mdev);
int mga_g450_dac_power_up(struct mga_dev *mdev, unsigned int clock, bool crtc2);

int mga_g450_dac_start(struct mga_dev *mdev, unsigned int clock, bool crtc2);

void mga_g450_dac_rset(struct mga_dev *mdev, enum mga_rset rset);

void  mga_g450_dac_dvi_clock(struct mga_dev *mdev, unsigned int crtc,
			     unsigned int pll, bool phase);

void  mga_g450_dac_dvi_pipe(struct mga_dev *mdev, unsigned int pll, u8 val);

#endif
