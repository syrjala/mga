/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include "kernel_emul.h"
#include "drm_kms.h"
#include "mga_dump.h"
#include "mga_bes.h"
#include "mga_regs.h"
#include "mga_kms.h"

static void mga_bes_calc_vertical(bool field,
				  struct drm_region *v,
				  const struct drm_region *clip,
				  u32 *besvwght,
				  u32 *besvsrclst,
				  u32 *besctl)
{
	/* FIXME check how besv?wght works */
	*besvwght = v->y1 & 0xfffc;
	if (v->y1 < 0)
		*besvwght |= field ? MGA_BESV2WGHT_BES2WGHTS : MGA_BESV1WGHT_BES1WGHTS;

	/*
	 * Must clip because the +-0.25 deinterlace adjustment may have
	 * shifted the coordinates slightly outside the field dimensions.
	 */
	drm_region_clip(v, clip);

	/* we only need the integer coordinates from now on */
	v->y1 = v->y1 >> 16;
	v->y2 = ALIGN(v->y2, 0x10000) >> 16;

	*besvsrclst = v->y2 - v->y1 - 1;

	if (v->y1 & 1)
		*besctl |= field ? MGA_BESCTL_BESV2SRCSTP : MGA_BESCTL_BESV1SRCSTP;
}

/* FIXME figure out what alignment issues hzoom brings */
bool mga_bes_calc_hzoom(const struct mga_dev *mdev,
			const struct drm_display_mode *mode)
{
	unsigned int pixclk = mga_calc_pixel_clock(mode, 1);//FIXME hzoom

	if (mdev->chip >= MGA_CHIP_G450)
		return pixclk >= 234000;
	else
		return pixclk >= 135000;
}

static void mga_bes_calc_coords(struct mga_dev *mdev,
				const struct mga_plane_config *pc,
				const struct mga_crtc_config *cc,
				struct mga_bes_regs *regs)
{
	const struct drm_region *src = &pc->src;
	const struct drm_region *dst = &pc->dst;
	const struct drm_framebuffer *fb = pc->fb;
	const struct drm_display_mode *mode = &cc->adjusted_mode;
	const struct drm_region clip = {
		.x2 = fb->width << 16,
		.y2 = fb->height << (16 - pc->deinterlace),
	};
	struct drm_region v1 = *src, v2 = *src;
	bool hzoom = mga_bes_calc_hzoom(mdev, mode);
	struct mga_framebuffer *mfb = to_mga_framebuffer(fb);
	bool hmirror = pc->rotation & DRM_REFLECT_X;
	int x = hmirror ? fb->width : 0;
	int x1, x2, width;
	int align;

	regs->besctl |= MGA_BESCTL_BESEN;

	if (mdev->chip >= MGA_CHIP_G400)
		align = 0x10;
	else
		align = 0x8;

	switch (fb->pixel_format) {
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		align <<= 1;
		break;
	default:
		align /= drm_format_plane_cpp(fb->pixel_format, 0);
		break;
	}

	x1 = (v1.x1 >> 16) & ~(align - 1);
	x2 = ALIGN((v1.x2 + 0xffff) >> 16, align);
	v1.x1 -= x1 << 16;
	v1.x2 -= x1 << 16;
	width = x2 - x1;

	x = hmirror ? ((int)fb->width - x1) : x1;

	dev_dbg(mdev->dev, "BES paragraph aligned coords %u - %u\n", x1, x2);
	drm_region_print(&v1, "BES neutered src", true);

	if (pc->deinterlace) {
		/* shift top field by +0.25 */
		drm_region_translate(&v1, 0,  0x4000);
		/* shift bottom field by -0.25 */
		drm_region_translate(&v2, 0, -0x4000);
	}

	if (hmirror)
		regs->besctl |= MGA_BESCTL_BESHMIR;

	/* FIXME? */
	regs->besctl |= MGA_BESCTL_BESDITH;

	if (hzoom)
		regs->besglobctl |= MGA_BESGLOBCTL_BESHZOOM | MGA_BESGLOBCTL_BESHZOOMF;

	/* destinatipn coordinates */
	regs->beshcoord = (dst->x1 << 16) | (dst->x2 - 1);
	regs->besvcoord = (dst->y1 << 16) | (dst->y2 - 1);

	/* horizontal source coordinates */
	regs->beshsrcst  = v1.x1;
	regs->beshsrcend = v1.x2 - (1 << 16);
	regs->beshsrclst = (width - 1) << 16;

	/* field 1 vertical source coordinates */
	mga_bes_calc_vertical(0, &v1, &clip,
			      &regs->besv1wght, &regs->besv1srclst, &regs->besctl);

	/* field 2 vertical source coordinates */
	mga_bes_calc_vertical(1, &v2, &clip,
			      &regs->besv2wght, &regs->besv2srclst, &regs->besctl);

	regs->beshiscal = pc->hscale << hzoom;
	regs->besviscal = pc->vscale;

	/* FIXME BESHFIXC */
	/* FIXME BESDITH */
	/* FIXME BESHMIR */
	/* FIXME BESBWEN */
	/* FIXME BESBLANK */

	regs->bespitch = (fb->pitches[0] << pc->deinterlace) / drm_format_plane_cpp(fb->pixel_format, 0);

	if (mdev->chip == MGA_CHIP_G400 &&
	    (fb->pixel_format == DRM_FORMAT_ARGB8888 ||
	     fb->pixel_format == DRM_FORMAT_XRGB8888)) {
		BUG_ON(pc->hscale << hzoom != 0x10000);
		BUG_ON(drm_region_width(src) > 512 << 16);

		regs->beshiscal  <<= 1;
		regs->beshsrcst  <<= 1;
		regs->beshsrcend <<= 1;
		regs->beshsrclst <<= 1;
		regs->bespitch   <<= 1;
	}

	/* pixel format settings */
	/* FIXME test if RGB filtering works on G450/G550 */
	switch (fb->pixel_format) {
	case DRM_FORMAT_YVU420:
		regs->besglobctl |= MGA_BESGLOBCTL_BESCORDER;
		/* fall through */
	case DRM_FORMAT_YUV420:
		regs->besglobctl |= MGA_BESGLOBCTL_BES3PLANE;
		regs->besctl  |= MGA_BESCTL_BESHFEN |
				 MGA_BESCTL_BESVFEN |
				 MGA_BESCTL_BESCUPS |
				 MGA_BESCTL_BES420PL;
		break;
	case DRM_FORMAT_NV21:
		regs->besglobctl |= MGA_BESGLOBCTL_BESCORDER;
		/* fall through */
	case DRM_FORMAT_NV12:
		regs->besctl |= MGA_BESCTL_BESHFEN |
				MGA_BESCTL_BESVFEN |
				MGA_BESCTL_BESCUPS |
				MGA_BESCTL_BES420PL;
		break;
	case DRM_FORMAT_UYVY:
		regs->besglobctl |= MGA_BESGLOBCTL_BESUYVYFMT;
		/* fall through */
	case DRM_FORMAT_YUYV:
		regs->besctl |= MGA_BESCTL_BESHFEN |
				MGA_BESCTL_BESVFEN |
				MGA_BESCTL_BESCUPS;
		break;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
		regs->besglobctl |= MGA_BESGLOBCTL_BESRGBMODE_RGB15;
		break;
	case DRM_FORMAT_RGB565:
		regs->besglobctl |= MGA_BESGLOBCTL_BESRGBMODE_RGB16;
		break;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		regs->besglobctl |= MGA_BESGLOBCTL_BESRGBMODE_RGB32;
		break;
	default:
		BUG();
	}

	/* can't do vertical filterin with wide source */
	if (width > 1024) {
		BUG_ON(mdev->chip < MGA_CHIP_G450);
		regs->besctl &= ~MGA_BESCTL_BESVFEN;
	}

	/*
	 * FIXME other bandwidth limitations that would
	 * be affected by filtering, cups, hzoomf, etc.
	 */

	/*
	 * By this time:
	 * v1/v2 have been clipped to field dimensions,
	 * and they only have integer y-coordinates
	 */
	regs->besa1org = mga_framebuffer_calc_bus(mfb, x, v1.y1, 0, 0);
	regs->besa2org = mga_framebuffer_calc_bus(mfb, x, v2.y1, 0, 1);

	BUG_ON(regs->besa1org & (mdev->chip >= MGA_CHIP_G400 ? 0xf : 0x7));
	BUG_ON(regs->besa2org & (mdev->chip >= MGA_CHIP_G400 ? 0xf : 0x7));

	regs->besa1org -= hmirror;
	regs->besa2org -= hmirror;

	/* for 4:2:0 subsampling */
	x >>= 1;
	v1.y1 >>= 1;
	v2.y1 >>= 1;

	regs->besa1corg = 0;
	regs->besa2corg = 0;
	regs->besa1c3org = 0;
	regs->besa2c3org = 0;

	switch (fb->pixel_format) {
	case DRM_FORMAT_YVU420:
	case DRM_FORMAT_YUV420:
		regs->besa1corg = mga_framebuffer_calc_bus(mfb, x, v1.y1, 1, 0);
		regs->besa2corg = mga_framebuffer_calc_bus(mfb, x, v2.y1, 1, 1);
		regs->besa1c3org = mga_framebuffer_calc_bus(mfb, x, v1.y1, 2, 0);
		regs->besa2c3org = mga_framebuffer_calc_bus(mfb, x, v2.y1, 2, 1);

		BUG_ON(regs->besa1corg & (mdev->chip >= MGA_CHIP_G400 ? 0xf : 0x7));
		BUG_ON(regs->besa2corg & (mdev->chip >= MGA_CHIP_G400 ? 0xf : 0x7));
		BUG_ON(regs->besa1c3org & (mdev->chip >= MGA_CHIP_G400 ? 0xf : 0x7));
		BUG_ON(regs->besa2c3org & (mdev->chip >= MGA_CHIP_G400 ? 0xf : 0x7));

		regs->besa1corg -= hmirror;
		regs->besa2corg -= hmirror;
		regs->besa1c3org -= hmirror;
		regs->besa2c3org -= hmirror;
		break;
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_NV12:
		regs->besa1corg = mga_framebuffer_calc_bus(mfb, x, v1.y1, 1, 0);
		regs->besa2corg = mga_framebuffer_calc_bus(mfb, x, v2.y1, 1, 1);

		BUG_ON(regs->besa1corg & (mdev->chip >= MGA_CHIP_G400 ? 0xf : 0x7));
		BUG_ON(regs->besa2corg & (mdev->chip >= MGA_CHIP_G400 ? 0xf : 0x7));

		regs->besa1corg -= hmirror;
		regs->besa2corg -= hmirror;
		break;
	default:
		break;
	}
}

void mga_bes_calc(struct mga_dev *mdev,
		  const struct mga_plane_config *pc,
		  const struct mga_crtc_config *cc,
		  struct mga_bes_regs *regs)
{
	const struct drm_display_mode *mode = &cc->adjusted_mode;

	regs->besctl = 0;
	regs->besglobctl = (mode ? mode->vdisplay : 0) << 16;

	if (!drm_region_visible(&pc->dst))
		return;

	mga_bes_calc_coords(mdev, pc, cc, regs);
}

void mga_bes_restore(struct mga_dev *mdev,
		     const struct mga_bes_regs *regs)
{
	mga_write32(mdev, MGA_BESGLOBCTL, 0xfff << 16);

	mga_write32(mdev, MGA_BESA1ORG, regs->besa1org);
	mga_write32(mdev, MGA_BESA2ORG, regs->besa2org);

	mga_write32(mdev, MGA_BESA1CORG, regs->besa1corg);
	mga_write32(mdev, MGA_BESA2CORG, regs->besa2corg);

	mga_write32(mdev, MGA_BESCTL, regs->besctl);

	mga_write32(mdev, MGA_BESPITCH, regs->bespitch);
	mga_write32(mdev, MGA_BESHCOORD, regs->beshcoord);
	mga_write32(mdev, MGA_BESVCOORD, regs->besvcoord);
	mga_write32(mdev, MGA_BESHISCAL, regs->beshiscal);
	mga_write32(mdev, MGA_BESVISCAL, regs->besviscal);
	mga_write32(mdev, MGA_BESHSRCST, regs->beshsrcst);
	mga_write32(mdev, MGA_BESHSRCEND, regs->beshsrcend);

	if (mdev->chip >= MGA_CHIP_G400)
		mga_write32(mdev, MGA_BESLUMACTL, regs->beslumactl);

	mga_write32(mdev, MGA_BESV1WGHT, regs->besv1wght);
	mga_write32(mdev, MGA_BESV2WGHT, regs->besv2wght);
	mga_write32(mdev, MGA_BESHSRCLST, regs->beshsrclst);
	mga_write32(mdev, MGA_BESV1SRCLST, regs->besv1srclst);
	mga_write32(mdev, MGA_BESV2SRCLST, regs->besv2srclst);

	if (mdev->chip >= MGA_CHIP_G400) {
		mga_write32(mdev, MGA_BESA1C3ORG, regs->besa1c3org);
		mga_write32(mdev, MGA_BESA2C3ORG, regs->besa2c3org);
	}

	mga_write32(mdev, MGA_BESGLOBCTL, regs->besglobctl);
}
