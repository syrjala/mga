/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "mga_kernel_api.h"
#include "mga_pins.h"

static int read_rom(const char *fname, u8 *rom_data, size_t *rom_size)
{
	ssize_t r;
	int fd;

	pr_debug("Going to read ROM\n");

	fd = open(fname, O_RDONLY);
	if (fd < 0)
		return -1;

	r = read(fd, rom_data, 512*1024);
	if (r < 0) {
		pr_err("Failed to read ROM: %s\n", strerror(errno));
		close(fd);
		return -1;
	}

	pr_debug("Read ROM %zd bytes\n", r);

	*rom_size = r;

	return 0;
}

int main(int argc, char *argv[])
{
	u8 rom_data[512*1024];
	size_t rom_size;
	union mga_pins pins;

	if (argc != 2)
		return 1;

	if (read_rom(argv[1], rom_data, &rom_size))
		return 2;

	mga_parse_pins(MGA_CHIP_2064W, rom_data, rom_size, &pins);
	mga_parse_pins(MGA_CHIP_2164W, rom_data, rom_size, &pins);
	mga_parse_pins(MGA_CHIP_1064SG, rom_data, rom_size, &pins);
	mga_parse_pins(MGA_CHIP_G100, rom_data, rom_size, &pins);
	mga_parse_pins(MGA_CHIP_G200, rom_data, rom_size, &pins);
	mga_parse_pins(MGA_CHIP_G400, rom_data, rom_size, &pins);
	mga_parse_pins(MGA_CHIP_G450, rom_data, rom_size, &pins);

	return 0;
}
