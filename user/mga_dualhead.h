/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_DUALHEAD_H
#define MGA_DUALHEAD_H

struct mga_dev;

void mga_dualhead_addon_set_sync(struct mga_dev *mdev, unsigned int sync);

void mga_dualhead_addon_vidrst(struct mga_dev *mdev, bool enable);

int mga_dualhead_addon_probe(struct mga_dev *mdev);

void mga_dualhead_builtin_set_sync(struct mga_dev *mdev, unsigned int sync);

int mga_dualhead_builtin_probe(struct mga_dev *mdev);

#endif
