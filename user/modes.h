/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MODES_H
#define MODES_H

#include "mga_dump.h"

static const struct drm_display_mode mode_htest = {
	.vrefresh = 170000,

	.flags = 0,

	.htotal = 256,
	.hdisplay = 64,
	.hblank_start = 64,
	.hblank_width = 192,
	.hsync_start = 128,
	.hsync_width = 64,

	.vtotal = 2048,
	.vdisplay = 1792,
	.vblank_start = 1792,
	.vblank_width = 256,
	.vsync_start = 1912,
	.vsync_width = 16,
};

static const struct drm_display_mode mode_vtest = {
	.vrefresh = 170000,

	.flags = 0,

	.htotal = 2048,
	.hdisplay = 512,
	.hblank_start = 512,
	.hblank_width = 1536,
	.hsync_start = 1152,
	.hsync_width = 256,

	.vtotal = 257,
	.vdisplay = 254,
	.vblank_start = 254,
	.vblank_width = 3,
	.vsync_start = 255,
	.vsync_width = 1,
};

static const struct drm_display_mode mode_320_240_60_dbl = {
	.vrefresh = 60000,

	.flags = DRM_MODE_FLAG_DBLSCAN,

	.htotal = 400,
	.hdisplay = 320,
	.hblank_start = 320,
	.hblank_width = 80,
	.hsync_start = 336,
	.hsync_width = 48,

	.vtotal = 262,
	.vdisplay = 240,
	.vblank_start = 240,
	.vblank_width = 22,
	.vsync_start = 245,
	.vsync_width = 1,
};

static const struct drm_display_mode mode_640_480_60_oddball = {
	.vrefresh = 60000,

	.flags = 0,

	.hdisplay = 640,

	.vdisplay = 480,
};

static const struct drm_display_mode mode_640_480_60 = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 800,
	.hdisplay = 640,
	.hblank_start = 640,
	.hblank_width = 160,
	.hsync_start = 664,
	.hsync_width = 96,

	.vtotal = 525,
	.vdisplay = 480,
	.vblank_start = 480,
	.vblank_width = 45,
	.vsync_start = 491,
	.vsync_width = 2,
};

static const struct drm_display_mode mode_800_600_60 = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 1024,
	.hdisplay = 800,
	.hblank_start = 800,
	.hblank_width = 224,
	.hsync_start = 832,
	.hsync_width = 80,

	.vtotal = 622,
	.vdisplay = 600,
	.vblank_start = 600,
	.vblank_width = 22,
	.vsync_start = 601,
	.vsync_width = 3,
};

static const struct drm_display_mode mode_1024_768_60 = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 1328,
	.hdisplay = 1024,
	.hblank_start = 1024,
	.hblank_width = 320,
	.hsync_start = 1072,
	.hsync_width = 104,

	.vtotal = 798,
	.vdisplay = 768,
	.vblank_start = 768,
	.vblank_width = 30,
	.vsync_start = 771,
	.vsync_width = 4,
};

static const struct drm_display_mode mode_1024_768_87_laced = {
	.vrefresh = 87000,

	.flags = DRM_MODE_FLAG_INTERLACE,

	.htotal = 1264,
	.hdisplay = 1024,
	.hblank_start = 1024,
	.hblank_width = 240,
	.hsync_start = 1048,
	.hsync_width = 160,

	.vtotal = 817,
	.vdisplay = 768,
	.vblank_start = 768,
	.vblank_width = 49,
	.vsync_start = 776,
	.vsync_width = 8,
};

static const struct drm_display_mode mode_1280_1024_60 = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 1712,
	.hdisplay = 1280,
	.hblank_start = 1280,
	.hblank_width = 432,
	.hsync_start = 1368,
	.hsync_width = 98,

	.vtotal = 1063,
	.vdisplay = 1024,
	.vblank_start = 1024,
	.vblank_width = 39,
	.vsync_start = 1027,
	.vsync_width = 7,
};

static const struct drm_display_mode mode_1312_1048_60 = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 1744,
	.hdisplay = 1312,
	.hblank_start = 1312,
	.hblank_width = 432,
	.hsync_start = 1392,
	.hsync_width = 136,

	.vtotal = 1087,
	.vdisplay = 1048,
	.vblank_start = 1048,
	.vblank_width = 39,
	.vsync_start = 1051,
	.vsync_width = 10,
};

static const struct drm_display_mode mode_1400_1050_60 = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 1864,
	.hdisplay = 1400,
	.hblank_start = 1400,
	.hblank_width = 464,
	.hsync_start = 1488,
	.hsync_width = 144,

	.vtotal = 1089,
	.vdisplay = 1050,
	.vblank_start = 1050,
	.vblank_width = 39,
	.vsync_start = 1053,
	.vsync_width = 4,
};

static const struct drm_display_mode mode_1400_1050_60_r = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 1560,
	.hdisplay = 1400,
	.hblank_start = 1400,
	.hblank_width = 160,
	.hsync_start = 1448,
	.hsync_width = 32,

	.vtotal = 1080,
	.vdisplay = 1050,
	.vblank_start = 1050,
	.vblank_width = 30,
	.vsync_start = 1053,
	.vsync_width = 4,
};

static const struct drm_display_mode mode_1600_1200_60 = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 2160,
	.hdisplay = 1600,
	.hblank_start = 1600,
	.hblank_width = 560,
	.hsync_start = 1664,
	.hsync_width = 192,

	.vtotal = 1250,
	.vdisplay = 1200,
	.vblank_start = 1200,
	.vblank_width = 50,
	.vsync_start = 1201,
	.vsync_width = 3,
};

static const struct drm_display_mode mode_1600_1200_60_r = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 1760,
	.hdisplay = 1600,
	.hblank_start = 1600,
	.hblank_width = 160,
	.hsync_start = 1648,
	.hsync_width = 32,

	.vtotal = 1235,
	.vdisplay = 1200,
	.vblank_start = 1200,
	.vblank_width = 35,
	.vsync_start = 1203,
	.vsync_width = 4,
};

static const struct drm_display_mode mode_1920_1200_60 = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 2592,
	.hdisplay = 1920,
	.hblank_start = 1920,
	.hblank_width = 672,
	.hsync_start = 2048,
	.hsync_width = 208,

	.vtotal = 1242,
	.vdisplay = 1200,
	.vblank_start = 1200,
	.vblank_width = 42,
	.vsync_start = 1201,
	.vsync_width = 3,
};

static const struct drm_display_mode mode_1680_1050_60_r = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 1840,
	.hdisplay = 1680,
	.hblank_start = 1680,
	.hblank_width = 160,
	.hsync_start = 1728,
	.hsync_width = 32,

	.vtotal = 1080,
	.vdisplay = 1050,
	.vblank_start = 1050,
	.vblank_width = 30,
	.vsync_start = 1053,
	.vsync_width = 6,
};

static const struct drm_display_mode mode_1920_1200_60_r = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 2080,
	.hdisplay = 1920,
	.hblank_start = 1920,
	.hblank_width = 160,
	.hsync_start = 1968,
	.hsync_width = 32,

	.vtotal = 1235,
	.vdisplay = 1200,
	.vblank_start = 1200,
	.vblank_width = 35,
	.vsync_start = 1209,
	.vsync_width = 6,
};

static const struct drm_display_mode mode_720_480_60 = {
	.vrefresh = 60000,

	.flags = DRM_MODE_FLAG_INTERLACE,

	.htotal = 858,
	.hdisplay = 720,
	.hblank_start = 720,
	.hblank_width = 138,
	.hsync_start = 736,
	.hsync_width = 64,

	.vtotal = 525,
	.vdisplay = 480,
	.vblank_start = 480,
	.vblank_width = 45,
	.vsync_start = 486,
	.vsync_width = 6,
};

static const struct drm_display_mode mode_720_486_60 = {
	.vrefresh = 60000,

	.flags = DRM_MODE_FLAG_INTERLACE,

	.htotal = 858,
	.hdisplay = 720,
	.hblank_start = 720,
	.hblank_width = 138,
	.hsync_start = 736,
	.hsync_width = 64,

	.vtotal = 525,
	.vdisplay = 486,
	.vblank_start = 486,
	.vblank_width = 39,
	.vsync_start = 492,
	.vsync_width = 6,
};

static const struct drm_display_mode mode_720_576_50 = {
	.vrefresh = 50000,

	.flags = DRM_MODE_FLAG_INTERLACE,

	.htotal = 864,
	.hdisplay = 720,
	.hblank_start = 720,
	.hblank_width = 144,
	.hsync_start = 732,
	.hsync_width = 64,

	.vtotal = 625,
	.vdisplay = 576,
	.vblank_start = 576,
	.vblank_width = 49,
	.vsync_start = 581,
	.vsync_width = 5,
};

static const struct drm_display_mode mode_720_576_xx = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal = 792,
	.hdisplay = 720,
	.hblank_start = 720,
	.hblank_width = 72,
	.hsync_start = 724,
	.hsync_width = 46,

	.vtotal = 650,
	.vdisplay = 576,
	.vblank_start = 576,
	.vblank_width = 74,
	.vsync_start = 596,
	.vsync_width = 4,
};

static const struct drm_display_mode mode_640_480_tvo = {
	.vrefresh = 50000,

	.flags = 0,

	.htotal       = 792,
	.hdisplay     = 640,
	.hblank_start = 640,
	.hblank_width = 152,
	.hsync_start  = 672,
	.hsync_width  = 48,
	.vtotal       = 625,
	.vdisplay     = 480,
	.vblank_start = 480,
	.vblank_width = 145,
	.vsync_start  = 534,
	.vsync_width  = 4,
};

static const struct drm_display_mode mode_800_600_tvo = {
	.vrefresh = 50000,

	.flags = 0,

	.htotal       = 896,
	.hdisplay     = 800,
	.hblank_start = 800,
	.hblank_width = 96,
	.hsync_start  = 816,
	.hsync_width  = 32,
	.vtotal       = 700,
	.vdisplay     = 600,
	.vblank_start = 600,
	.vblank_width = 100,
	.vsync_start  = 629,
	.vsync_width  = 4,
};

static const struct drm_display_mode mode_1024_768_tvo = {
	.vrefresh = 50000,

	.flags = 0,

	.htotal       = 1136,
	.hdisplay     = 1024,
	.hblank_start = 1024,
	.hblank_width = 112,
	.hsync_start  = 1048,
	.hsync_width  = 24,

	.vtotal       = 896,
	.vdisplay     = 768,
	.vblank_start = 768,
	.vblank_width = 128,
	.vsync_start  = 808,
	.vsync_width  = 4,
};

static const struct drm_display_mode mode_1280_720_60_cea = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal       = 1650,
	.hdisplay     = 1280,
	.hblank_start = 1280,
	.hblank_width = 370,
	.hsync_start  = 1390,
	.hsync_width  = 40,

	.vtotal       = 750,
	.vdisplay     = 720,
	.vblank_start = 720,
	.vblank_width = 30,
	.vsync_start  = 725,
	.vsync_width  = 5,
};

static const struct drm_display_mode mode_1280_720_50_cea = {
	.vrefresh = 50000,

	.flags = 0,

	.htotal       = 1980,
	.hdisplay     = 1280,
	.hblank_start = 1280,
	.hblank_width = 700,
	.hsync_start  = 1720,
	.hsync_width  = 40,

	.vtotal       = 750,
	.vdisplay     = 720,
	.vblank_start = 720,
	.vblank_width = 30,
	.vsync_start  = 725,
	.vsync_width  = 5,
};

static const struct drm_display_mode mode_1920_1080_60_cea = {
	.vrefresh = 60000,

	.flags = 0,

	.htotal       = 2200,
	.hdisplay     = 1920,
	.hblank_start = 1920,
	.hblank_width = 280,
	.hsync_start  = 2008,
	.hsync_width  = 44,

	.vtotal       = 1125,
	.vdisplay     = 1080,
	.vblank_start = 1080,
	.vblank_width = 45,
	.vsync_start  = 1084,
	.vsync_width  = 5,
};

static const struct drm_display_mode mode_1920_1080i_60_cea = {
	.vrefresh = 60000,

	.flags = DRM_MODE_FLAG_INTERLACE,

	.htotal       = 2200,
	.hdisplay     = 1920,
	.hblank_start = 1920,
	.hblank_width = 280,
	.hsync_start  = 2008,
	.hsync_width  = 44,

	.vtotal       = 1125,
	.vdisplay     = 1080,
	.vblank_start = 1080,
	.vblank_width = 45,
	.vsync_start  = 1084,
	.vsync_width  = 5,
};

static const struct drm_display_mode mode_1920_1080_50_cea = {
	.vrefresh = 50000,

	.flags = 0,

	.htotal       = 2640,
	.hdisplay     = 1920,
	.hblank_start = 1920,
	.hblank_width = 720,
	.hsync_start  = 2448,
	.hsync_width  = 44,

	.vtotal       = 1125,
	.vdisplay     = 1080,
	.vblank_start = 1080,
	.vblank_width = 45,
	.vsync_start  = 1084,
	.vsync_width  = 5,
};

static const struct drm_display_mode mode_1920_1080i_50_cea = {
	.vrefresh = 50000,

	.flags = DRM_MODE_FLAG_INTERLACE,

	.htotal       = 2640,
	.hdisplay     = 1920,
	.hblank_start = 1920,
	.hblank_width = 720,
	.hsync_start  = 2448,
	.hsync_width  = 44,

	.vtotal       = 1125,
	.vdisplay     = 1080,
	.vblank_start = 1080,
	.vblank_width = 45,
	.vsync_start  = 1084,
	.vsync_width  = 5,
};

static const struct drm_display_mode mode_1920_1080_30_cea = {
	.vrefresh = 29970,

	.flags = 0,

	.htotal       = 2200,
	.hdisplay     = 1920,
	.hblank_start = 1920,
	.hblank_width = 280,
	.hsync_start  = 2008,
	.hsync_width  = 44,

	.vtotal       = 1125,
	.vdisplay     = 1080,
	.vblank_start = 1080,
	.vblank_width = 45,
	.vsync_start  = 1084,
	.vsync_width  = 5,
};

static const struct drm_display_mode mode_1920_1080_25_cea = {
	.vrefresh = 25000,

	.flags = 0,

	.htotal       = 2640,
	.hdisplay     = 1920,
	.hblank_start = 1920,
	.hblank_width = 720,
	.hsync_start  = 2448,
	.hsync_width  = 44,

	.vtotal       = 1125,
	.vdisplay     = 1080,
	.vblank_start = 1080,
	.vblank_width = 45,
	.vsync_start  = 1084,
	.vsync_width  = 5,
};

static const struct drm_display_mode mode_1920_1080_24_cea = {
	.vrefresh = 24000,

	.flags = 0,

	.htotal       = 2750,
	.hdisplay     = 1920,
	.hblank_start = 1920,
	.hblank_width = 830,
	.hsync_start  = 2558,
	.hsync_width  = 44,

	.vtotal       = 1125,
	.vdisplay     = 1080,
	.vblank_start = 1080,
	.vblank_width = 45,
	.vsync_start  = 1084,
	.vsync_width  = 5,
};

#endif
