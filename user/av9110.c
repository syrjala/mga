/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <limits.h>

#include "av9110.h"

int av9110_init(struct av9110_dev *adev,
		struct device *dev,
		unsigned int fref,
		void (*setcen)(void *data, int state),
		void (*setsclk)(void *data, int state),
		void (*setdata)(void *data, int state),
		void *data)
{
	/* 2 MHz < fREF < 32 MHz */
	if (fref < 2000 || fref > 32000)
		return -EINVAL;

	adev->dev = dev;
	adev->fref = fref;
	adev->setcen = setcen;
	adev->setsclk = setsclk;
	adev->setdata = setdata;
	adev->data = data;

	setcen(data, 1);
	setsclk(data, 1);
	setdata(data, 1);

	return 0;
}

void av9110_power_down(struct av9110_dev *adev)
{
	struct av9110_pll_settings pll;

	/* Go for minimum frequencies. */
	av9110_pll_calc(adev, 0, 1, &pll);

	/* tristate outputs */
	pll.val &= ~(1 << 19);
	pll.val &= ~(1 << 20);

	/* FIXME cache the register value? */

	av9110_pll_program(adev, &pll);
}

static unsigned int calc_diff(unsigned int a, unsigned int b)
{
	if (a > b)
		return a - b;
	else
		return b - a;
}

int av9110_pll_calc(struct av9110_dev *adev,
		    unsigned int clk_freq,
		    unsigned int clkx_freq,
		    struct av9110_pll_settings *pll)
{
	unsigned int fref = adev->fref;
	unsigned int n, m, v, r, x;
	unsigned int diff, best_fclk = 0, best_diff = UINT_MAX;
	unsigned int best_n = 0, best_m = 0, best_v = 0, best_r = 0, best_x = 0;
	unsigned int x_min, x_max;
	unsigned int freq;
	bool oe_clk, oe_clkx, ref_clk = false;

	/*
	 * fCLK and fCLK/X are outputs.
	 *
	 * fVCO = N * V * fREF
	 * fCLK = fVCO / R
	 * fCLK/X = fCLK / X
	 *
	 * N = 3 - 127
	 * M = 3 - 127
	 * V = 1, 8
	 * R = 1, 2, 4, 8
	 * X = 1, 2, 4, 8
	 *
	 * fCLK can also output fREF directly.
	 */

	if (clk_freq && clkx_freq && clk_freq != fref) {
		/* X is fixed */
		x_min = x_max = clk_freq / clkx_freq;
		freq = clkx_freq;

		switch (x_min) {
		case 1:
		case 2:
		case 4:
		case 8:
			if (clkx_freq * x_min != clk_freq)
				return -EINVAL;
			break;
		default:
			return -EINVAL;
		}
	} else if (clkx_freq) {
		/* X can change freely */
		x_min = 1;
		x_max = 8;
		freq = clkx_freq;
	} else if (clk_freq) {
		/* X not used */
		x_min = x_max = 1;
		freq = clk_freq;
	} else {
		return -EINVAL;
	}

	for (m = 127; m >= 3; m--) {
		/* 200 kHz < fREF / M < 5 MHz */
		unsigned int frefm = div_round(fref, m);
		if (frefm < 200 || frefm > 5000)
			continue;

		for (n = 127; n >= 3; n--) {
		for (v = 8; v >= 1; v >>= 3) {
			/* 50 MHz < fVCO < 250 MHz */
			unsigned int fvco = div_round(n * v * fref, m);
			if (fvco < 50000 || fvco > 250000)
				continue;

			for (r = 8; r >= 1; r >>= 1) {
			for (x = x_max; x >= x_min; x >>= 1) {
				unsigned int fclk = div_round(n * v * fref, m * r * x);
				diff = calc_diff(fclk, freq);
				/*
				 * If there are equally good matches
				 * we prefer the earlier match since
				 * R >= 2 provides an improved duty cycle.
				 */
				if (diff < best_diff) {
					best_diff = diff;
					best_fclk = fclk;
					best_n = n;
					best_v = v;
					best_m = m;
					best_r = r;
					best_x = x;
				}
			}
			}
		}
		}
	}

	/* Have fCLK output fREF directly? */
	ref_clk = (clk_freq == fref);

	if (ref_clk && !clkx_freq) {
		best_diff = 0;
		best_fclk = fref;
	}

	/* FIXME check that best_diff is within some tolerance? */
	if (best_diff == UINT_MAX)
		return -EINVAL;

	n = best_n;
	v = best_v;
	m = best_m;
	r = best_r;
	x = best_x;
	oe_clk = !!clk_freq;
	oe_clkx = !!clkx_freq;

	dev_dbg(adev->dev,
		"AV9110 frequency = %u\n"
		" N = %u\n"
		" M = %u\n"
		" V = %u\n"
		" X = %u\n"
		" R = %u\n"
		" OE CLK = %u\n"
		" OE CLK/X = %u\n"
		" REF CLK = %u\n",
		best_fclk, n, m, v, x, r, oe_clk, oe_clkx, ref_clk);

	if (clkx_freq)
		pll->fclkx = best_fclk;
	else
		pll->fclkx = 0;

	if (clk_freq)
		pll->fclk = ref_clk ? fref : (x * best_fclk);
	else
		pll->fclk = 0;

	pll->val =
		(n        <<  0) |
		(m        <<  7) |
		((!!v)    << 14) |
		(ilog2(x) << 15) |
		(ilog2(r) << 17) |
		(oe_clk   << 19) |
		(oe_clkx  << 20) |
		(1        << 21) |
		(ref_clk  << 22) |
		(1        << 23);

	return 0;
}

void av9110_pll_program(struct av9110_dev *adev,
			const struct av9110_pll_settings *pll)
{
	int i;
	u32 val = pll->val;

	adev->setcen(adev->data, 1);
	adev->setsclk(adev->data, 0);
	adev->setdata(adev->data, 0);
	ndelay(10);

	adev->setcen(adev->data, 0);
	for (i = 0; i < 24; i++) {
		adev->setdata(adev->data, val & 1);
		ndelay(10);
		adev->setsclk(adev->data, 1);
		ndelay(10);
		adev->setsclk(adev->data, 0);
		val >>= 1;
	}
	/* Apparently for the programming to take, DATA must be pulled low here. */
	adev->setdata(adev->data, 0);
	ndelay(10);
	adev->setcen(adev->data, 1);

	adev->setsclk(adev->data, 1);
	adev->setdata(adev->data, 1);
}

void av9110_pll_wait(struct av9110_dev *adev)
{
	(void)adev;

	/* Wait for PLL to lock */
	udelay(200);
}
