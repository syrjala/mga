/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "mga_dac.h"
#include "mga_dac_common.h"
#include "mga_cve2_data.h"
#include "i2c.h"
#include "drm_kms.h"
#include "tvo.h"

static int i2c_tvo_read_byte_data(struct i2c_adapter *adap,
				  u8 addr, u8 offset)
{
	int ret;
	u8 buf[1] = { [0] = 0 };
	struct i2c_msg msg[2] = {
		[0] = {
			.addr = addr,
			.flags = I2C_M_REV_DIR_ADDR,
			.len = 1,
			.buf = &offset,
		},
		[1] = {
			.addr = addr,
			.flags = I2C_M_RD | I2C_M_NOSTART,
			.len = 1,
			.buf = buf,
		},
	};

	ret = i2c_transfer(adap, msg, 2);
	if (ret < 0)
		return ret;

	return buf[0];
}

static int i2c_tvo_read_word_data(struct i2c_adapter *adap,
				  u8 addr, u8 offset)
{
	int ret;
	u8 buf[2] = { [0] = 0 };
	struct i2c_msg msg[2] = {
		[0] = {
			.addr = addr,
			.flags = I2C_M_REV_DIR_ADDR,
			.len = 1,
			.buf = &offset,
		},
		[1] = {
			.addr = addr,
			.flags = I2C_M_RD | I2C_M_NOSTART,
			.len = 2,
			.buf = buf,
		},
	};

	ret = i2c_transfer(adap, msg, 2);
	if (ret < 0)
		return ret;

	return (buf[1] << 8) | buf[0];
}

static int tvo_write8(struct mga_tvo_dev *tdev, u8 reg, u8 val)
{
	int ret;

	dev_dbg(tdev->dev, "TVO  %02x <- %02x       (%d)\n", reg, val, val);

	ret = i2c_smbus_write_byte_data(tdev->adap, tdev->addr, reg, val);
	if (ret < 0) {
		dev_err(tdev->dev, "Failed to write TVO register %02x\n", reg);
		return ret;
	}

	return 0;
}

static u8 tvo_read8(struct mga_tvo_dev *tdev, u8 reg)
{
	int ret = i2c_tvo_read_byte_data(tdev->adap, tdev->addr, reg);
	if (ret < 0) {
		dev_err(tdev->dev, "Failed to read TVO register %02x\n", reg);
		return 0xff;
	}

	dev_dbg(tdev->dev, "TVO  %02x = %02x       (%d)\n", reg, ret, ret);

	return ret;
}

static int tvo_write16(struct mga_tvo_dev *tdev, u8 reg, u16 val)
{
	int ret;

	dev_dbg(tdev->dev, "TVO  %02x <- %04x     (%d)\n", reg, val, val);

	ret = i2c_smbus_write_word_data(tdev->adap, tdev->addr, reg, val);
	if (ret < 0) {
		dev_err(tdev->dev, "Failed to write TVO register %02x\n", reg);
		return ret;
	}

	return 0;
}

static u16 tvo_read16(struct mga_tvo_dev *tdev, u8 reg)
{
	int ret = i2c_tvo_read_word_data(tdev->adap, tdev->addr, reg);
	if (ret < 0) {
		dev_err(tdev->dev, "Failed to read TVO register %02x\n", reg);
		return 0xffff;
	}

	dev_dbg(tdev->dev, "TVO  %02x = %04x     (%d)\n", reg, ret, ret);

	return ret;
}

u8 mga_tvo_read8(struct mga_tvo_dev *tdev, u8 reg)
{
	return tvo_read8(tdev, reg);
}

u16 mga_tvo_read16(struct mga_tvo_dev *tdev, u8 reg)
{
	return tvo_read16(tdev, reg);
}

int mga_tvo_write8(struct mga_tvo_dev *tdev, u8 reg, u8 val)
{
	return tvo_write8(tdev, reg, val);
}

int mga_tvo_write16(struct mga_tvo_dev *tdev, u8 reg, u16 val)
{
	return tvo_write16(tdev, reg, val);
}

static void cve2_write8(void *data, u8 reg, u8 val)
{
	struct mga_tvo_dev *tdev = data;

	tvo_write8(tdev, reg, val);
}

static void cve2_write16(void *data, u8 reg, u16 val)
{
	struct mga_tvo_dev *tdev = data;

	tvo_write16(tdev, reg, val);
}

static u8 cve2_read8(void *data, u8 reg)
{
	struct mga_tvo_dev *tdev = data;

	return tvo_read8(tdev, reg);
}

static u16 cve2_read16(void *data, u8 reg)
{
	struct mga_tvo_dev *tdev = data;

	return tvo_read16(tdev, reg);
}

static const struct mga_dac_pll_info tvo_pll_info = {
	.name = "TVO PLL",

	.fref     =  27000,//FIXME PInS?
	.fo_max   = 220000,//FIXME PInS?
	.fvco_min =  50000,
	.fvco_max = 220000,//FIXME PInS?

	.m_min = 1,
	.m_max = 31,
	.n_min = 1,
	.n_max = 127,
	.s_limits = { 100000, 140000, 180000, },
};

int mga_tvo_pll_calc(struct mga_tvo_dev *tdev,
		     unsigned int freq,
		     unsigned int fmax,
		     struct mga_dac_pll_settings *pll)
{
	int ret;

	ret = mga_dac_pll_calc(tdev->dev, &tvo_pll_info, freq, fmax, pll);
	if (ret)
		return ret;

	dev_dbg(tdev->dev, "TVO PLL parameters NREG=%d MREG=%d PREG=%d\n",
		pll->n, pll->m, pll->p);

	return 0;
}

static void program_pll(struct mga_tvo_dev *tdev,
			const struct mga_dac_pll_settings *pll)
{
	tvo_write8(tdev, 0x80, pll->m);
	tvo_write8(tdev, 0x81, pll->n);
	tvo_write8(tdev, 0x82, pll->p | 0x80);
}

/*
 * TVO has some problems vertical blanking in CRT output mode. Vertical
 * blanking isn't actually effective except during the vertical sync pulse.
 * There are several ways to combat the problem:
 * 1) increase hdisplay and add some black pixels (used by Windows)
 * 2) increase vdisplay and add a black line (used by BeOS)
 * 3) extend the sync pulse to cover the entire blanking period
 *
 * This driver implements methods 1 and 3.
 */
#ifdef TVO_WINDOWS
/*
 * Calculates the TVO CRTC registers for CRT output mode.
 *
 * Follows the method used by the Windows drivers.
 *
 * MGA CRTC parameters are adjusted like so:
 *  htotal -= 8
 *  hdisplay += 8
 *  hsync_start += 8
 * TVO CRTC htotal is MGA CRTC htotal + 8, otherwise the MGA CRTC parameters
 * are respected. Pixel clock is calculated for the full TVO CRTC htotal.
 *
 * The extra 8 visible pixels are set to 0 to make them black, assuming the
 * LUT[0,0,0] == black (which it apparently always is in Windows). This
 * approach retains full control over the vertical sync pulse but it requires
 * cooperation from the memory allocator (the extra 8 pixels per line) and it
 * assumes that the LUT[0,0,0] is black.
 */
static void calc_crtc_regs_crt_windows(const struct mga_tvo_config *config,
				       const struct drm_display_mode *mode,
				       struct mga_tvo_crtc_regs *regs)
{
	unsigned int tvo_htotal = mode->htotal + 8;
	unsigned int hfp = mode->hsync_start - mode->hdisplay;
	unsigned int hbp = tvo_htotal - (mode->hsync_start + mode->hsync_width);
	unsigned int hsw = mode->hsync_width;
	unsigned int vfp = mode->vsync_start - mode->vdisplay;
	unsigned int vbp = mode->vtotal - (mode->vsync_start + mode->vsync_width);
	unsigned int vsw = mode->vsync_width;

	regs->hscale = 0; //0x90
	regs->vscale = 0; //0x91

	regs->reg_98 = 0; /* no effect? */ //0x98
	regs->reg_ae = 0; /* no effect? */ //0xae

	regs->hsyncend = hsw; //0x9a
	regs->hblkend = hsw + hbp + 4; //0x9c
	regs->hblkstr = hsw + hbp + mode->hdisplay; //0x9e
	regs->htotal = tvo_htotal - 2; //0xa0

	regs->vsyncend = mode->vsync_width - 1; //0xa2
	regs->vblkend = mode->vsync_width + vbp; //0xa4
	regs->vblkstr = mode->vtotal - vfp; //0xa6
	regs->vtotal = mode->vtotal - 1; //0xa8

	regs->htotal_last = tvo_htotal - 2; //0x96

	regs->vdisp = 0x00; /* no effect? */ //0xbe
	regs->hdisp = 0x00; /* no effect? */ //0xc2

	regs->hvidrst = tvo_htotal - config->vidrst_delay - 1; //0xaa
	regs->vvidrst = mode->vtotal - 2; //0xac
}
#endif

#ifdef TVO_BEOS
/*
 * Calculates the TVO CRTC registers for CRT output mode.
 *
 * Follows the method used by the Windows drivers.
 *
 * MGA CRTC parameters are adjusted like so:
 *  vtotal -= 1
 *  vdisplay += 1
 *  vsync_start += 1
 * TVO CRTC vtotal is MGA CRTC vtotal + 1, otherwise the MGA CRTC parameters
 * are respected. Pixel clock is calculated for the full TVO CRTC htotal.
 *
 * The extra 8 visible pixels are set to 0 to make them black, assuming the
 * LUT[0,0,0] == black (which it apparently always is in Windows). This
 * approach retains full control over the vertical sync pulse but it requires
 * cooperation from the memory allocator (the extra 8 pixels per line) and it
 * assumes that the LUT[0,0,0] is black.
 */
static void calc_crtc_regs_crt_beos(const struct mga_tvo_config *config,
				    const struct drm_display_mode *mode,
				    struct mga_tvo_crtc_regs *regs)
{
	unsigned int hfp = mode->hsync_start - mode->hdisplay;
	unsigned int hbp = mode->htotal - mode->hsync_start - mode->hsync_width;
	unsigned int hsw = mode->hsync_width;
	unsigned int vfp = mode->vsync_start - mode->vdisplay;
	unsigned int vbp = mode->vtotal - mode->vsync_start - mode->vsync_width;
	unsigned int vsw = mode->vsync_width;

	regs->hscale = 0; //0x90
	regs->vscale = 0; //0x91

	regs->reg_98 = 0; /* no effect? */ //0x98
	regs->reg_ae = 0; /* no effect? */ //0xae

	regs->hsyncend = hsw; //0x9a
	regs->hblkend = hsw + hbp; //0x9c
	regs->hblkstr = hsw + hbp + mode->hdisplay; //0x9e
	regs->htotal = mode->htotal; //0xa0

	regs->vsyncend = mode->vsync_width - 1; //0xa2
	regs->vblkend = mode->vsync_width + vbp; //0xa4
	regs->vblkstr = mode->vtotal - vfp; //0xa6
	regs->vtotal = mode->vtotal - 1; //0xa8

	regs->htotal_last = mode->htotal; //0x96

	regs->vdisp = 0x00; /* no effect? */ //0xbe
	regs->hdisp = 0x00; /* no effect? */ //0xc2

	regs->hvidrst = mode->htotal - config->vidrst_delay - 1; //0xaa
	regs->vvidrst = mode->vtotal - 2; //0xac
}
#endif

/*
 * Calculates the TVO CRTC registers for CRT output mode.
 *
 * If the vertical registers are calculated like the horizontal
 * registers the vertical blanking period will not be blank. The
 * video output will keep repeating the last 8 bytes of the last
 * active line. Extending the vertical sync pulse over back porch
 * fixes that. Proper control over the sync pulse is lost though.
 */
static void calc_crtc_regs_crt(const struct mga_tvo_config *config,
			       const struct drm_display_mode *mode,
			       struct mga_tvo_crtc_regs *regs)
{
	unsigned int hfp = mode->hsync_start - mode->hdisplay;
	unsigned int hbp = mode->htotal - (mode->hsync_start + mode->hsync_width);
	unsigned int hsw = mode->hsync_width;
	unsigned int vfp = mode->vsync_start - mode->vdisplay;
	unsigned int vbp = mode->vtotal - (mode->vsync_start + mode->vsync_width);
	unsigned int vsw = mode->vsync_width;

	regs->hscale = 0; //0x90
	regs->vscale = 0; //0x91

	regs->reg_98 = 0; /* no effect? */ //0x98
	regs->reg_ae = 0; /* no effect? */ //0xae

	regs->hsyncend = hsw; //0x9a
	regs->hblkend = hsw + hbp; //0x9c
	regs->hblkstr = hsw + hbp + mode->hdisplay; //0x9e
	regs->htotal = mode->htotal; //0xa0

	/*
	 * If the vertical registers are calculated like the horizontal
	 * registers the vertical blanking period will not be blank. The
	 * video output will keep repeating the last 8 bytes of the last
	 * active line. Extending the vertical sync pulse over back porch
	 * fixes that. Proper control over the sync pulse is lost though.
	 */
	regs->vsyncend = mode->vtotal - mode->vsync_start; //0xa2
	/* no effect when sync was extended */
	regs->vblkend = mode->vtotal - mode->vsync_start; //0xa4
	/* no effect when sync was extended */
	regs->vblkstr = mode->vtotal - mode->vsync_start + mode->vdisplay; //0xa6
	/*  due to sync being extended */
	regs->vtotal = mode->vtotal - mode->vsync_start + mode->vdisplay; //0xa8

	regs->htotal_last = mode->htotal; //0x96

	regs->vdisp = 0x00; /* no effect? */ //0xbe
	regs->hdisp = 0x00; /* no effect? */ //0xc2

	regs->hvidrst = mode->htotal - config->vidrst_delay - 8; //0xaa
	/*  due to sync being extended */
	regs->vvidrst = mode->vtotal - mode->vsync_start + mode->vdisplay; //0xac
}

static void calc_crtc_regs_tv(const struct mga_tvo_config *config,
			      const struct drm_display_mode *mode,
			      struct mga_tvo_crtc_regs *regs)
{
	unsigned int htotal = 784;
	unsigned int hdisplay = 720;
	unsigned int hsyncstr = hdisplay - 16 + ((htotal - hdisplay)/2);
	unsigned int hsyncend = hsyncstr + 32;

	unsigned int vtotal = 625;
	unsigned int vdisplay = 576;
	unsigned int vsyncstr = 576;
	unsigned int vsyncend = 625;

	unsigned int htotal_last = htotal;

	// FIXME based on some param
	// FIXME 736?
	regs->hscale = clamp((720 << 7) / mode->htotal, 0x40, 0x80);
	regs->hscale = 0x7b;

	regs->hdisp = ((768 << 7) -
			 (mode->htotal - mode->hsync_width) * regs->hscale) >> 8;

	regs->vdisp = 1;
	regs->vscale = 0x0000;
	regs->vscale = 0x7fff;

	unsigned int out_clks, in_clks;
	out_clks = (mode->vtotal + 1) * (mode->htotal + 2);
	in_clks = vtotal * (htotal + 2) + (htotal_last + 2);

	regs->reg_98 = 0x0000;
	regs->reg_ae = 0x0000;

	regs->reg_98 = 0x0000;
	regs->reg_ae = 0x0036;

	//regs->hsyncend = backporch;
	//regs->hblkend = 4;
	//regs->hblkstr = ib_len;
	//regs->htotal = htotal;

	regs->hsyncend = 0x0032;
	regs->hblkend = 0x0003;
	regs->hblkstr = 0x02c4;
	regs->htotal = 0x035e;

	regs->vsyncend = mode->vsync_width - 1;
	regs->vblkend = 1;
	regs->vblkstr = 0;
	regs->vtotal = mode->vtotal - 1;

	regs->vsyncend = 0x008c;
	regs->vblkend = 0x005b;
	regs->vblkstr = 0x000b;
	regs->vtotal = 0x0270;

	//regs->htotal_last = htotal - 2;
	regs->htotal_last = 0x35e;

	//regs->vdisp = 0x2e;
	//regs->hdisp = 0x00;
	regs->vdisp = 0x88;//0xbe
	regs->hdisp = 0x37;//0xc2

	regs->hvidrst = mode->htotal - config->vidrst_delay - 8;
	regs->vvidrst = mode->vtotal - 2;

	regs->hvidrst = 0x2bc;
	regs->vvidrst = 0x30;

	regs->hvidrst -= 1;
#if 0
	if (bpp == 15 || bpp == 16)
		regs->hvidrst += 2;
	if (bpp == 32)
		regs->hvidrst -= 3;
	if (mode->hdisplay <= 640)
		regs->hvidrst += 8;
#endif
}

static void program_crtc(struct mga_tvo_dev *tdev,
			 const struct mga_tvo_crtc_regs *regs)
{
	tvo_write8(tdev, 0x90, regs->hscale);
	tvo_write16(tdev, 0x91, regs->vscale);

	tvo_write16(tdev, 0x9a, regs->hsyncend);
	tvo_write16(tdev, 0x9c, regs->hblkend);
	tvo_write16(tdev, 0x9e, regs->hblkstr);
	tvo_write16(tdev, 0xa0, regs->htotal);

	tvo_write16(tdev, 0xa2, regs->vsyncend);
	tvo_write16(tdev, 0xa4, regs->vblkend);
	tvo_write16(tdev, 0xa6, regs->vblkstr);
	tvo_write16(tdev, 0xa8, regs->vtotal);

	tvo_write16(tdev, 0x98, regs->reg_98);
	tvo_write16(tdev, 0xae, regs->reg_ae);

	tvo_write16(tdev, 0x96, regs->htotal_last);

	tvo_write16(tdev, 0xaa, regs->hvidrst);
	tvo_write16(tdev, 0xac, regs->vvidrst);

	tvo_write8(tdev, 0xbe, regs->vdisp);
	tvo_write8(tdev, 0xc2, regs->hdisp);
}

static void program_deflicker(struct mga_tvo_dev *tdev)
{
	static const u8 deflicker_vals[] = {
		[0] = 0x00,
		[1] = 0xb1,
		[2] = 0xa2,
	};
	u8 val = deflicker_vals[tdev->config.deflicker];

#if 0
	/* hal doesn't do this, and it doesn't seem right anyway */
	if (tdev->dev == MGA_TVO_VER_B)
		val |= 0x40;
#endif

	tvo_write8(tdev, 0x93, val);
}

static const u8 tvo_gamma_vals[][9] = {
	{ 0x83, 0x39, 0xdf, 0x0f, 0x75, 0xd4, 0xfb, 0x5b, 0x9c, }, /*  4 */
	{ 0x85, 0x3d, 0x80, 0x3f, 0xb4, 0x93, 0xc3, 0x64, 0xb4, }, /*  6 */
	{ 0x83, 0x13, 0x3f, 0x1f, 0x32, 0x42, 0xab, 0x40, 0xb0, }, /*  8 */
	{ 0x00, 0x00, 0x00, 0x1f, 0x10, 0x10, 0x10, 0x64, 0xc8, }, /* 10 */
	{ 0x08, 0x17, 0x2f, 0x49, 0x93, 0xf4, 0xdc, 0x50, 0xc3, }, /* 12 */
	{ 0x16, 0x2b, 0x40, 0x50, 0x93, 0x73, 0x3a, 0x55, 0xa8, }, /* 14 */
	{ 0x22, 0x3c, 0x50, 0xd6, 0x93, 0xd4, 0xbc, 0x55, 0xa7, }, /* 16 */
	{ 0x2d, 0x4d, 0x60, 0xd8, 0x93, 0x63, 0x5b, 0x55, 0x9f, }, /* 18 */
	{ 0x38, 0x4c, 0x70, 0x6b, 0x93, 0xd4, 0x94, 0x40, 0x90, }, /* 20 */
	{ 0x41, 0x5b, 0x80, 0x89, 0x93, 0xc4, 0x11, 0x45, 0x94, }, /* 22 */
	{ 0x48, 0x68, 0x88, 0x8a, 0x93, 0xb4, 0xf5, 0x49, 0x93, }, /* 24 */
	{ 0x57, 0x74, 0x8f, 0x7e, 0x10, 0x53, 0xe5, 0x4d, 0x90, }, /* 26 */
	{ 0x5f, 0x77, 0x98, 0xfe, 0xf4, 0x53, 0xdd, 0x4d, 0x97, }, /* 28 */
	{ 0x64, 0x81, 0x9f, 0x9c, 0xf4, 0x94, 0xc5, 0x4d, 0xa0, }, /* 30 */
	{ 0x69, 0x8d, 0xa7, 0xf7, 0xf4, 0x84, 0xb5, 0x54, 0xa6, }, /* 32 */
	{ 0x69, 0x93, 0xa8, 0xf7, 0xf4, 0xf5, 0xb5, 0x5a, 0xaa, }, /* 34 */
	{ 0x78, 0x99, 0xaf, 0xf8, 0xd4, 0xe5, 0xa5, 0x5a, 0xb4, }, /* 36 */
	{ 0x77, 0x9c, 0xb0, 0xf8, 0xf4, 0xe5, 0x54, 0x4a, 0xa0, }, /* 38 */
	{ 0x77, 0x9e, 0xb7, 0xf8, 0xf4, 0xe5, 0x95, 0x4e, 0xa5, }, /* 40 */
};

static void program_gamma(struct mga_tvo_dev *tdev)
{
	int i;

	for (i = 0; i < 9; i++)
		tvo_write8(tdev, 0x83 + i, tdev->gamma_vals[i]);
}

/* FIXME check everything */
/* SCART pins: CVBS=20, Y=15, C=11 */
//MGA_TVO_CABLE_COMPOSITE_SVIDEO,
/* SCART pins: CVBS=7 */
//MGA_TVO_CABLE_SCART_COMPOSITE,
/* SCART pins: R=22, G=21, B=20, CSYNC=20 */
//MGA_TVO_CABLE_SCART_RGB,
/* SCART pins: Y=20, C=15 */
//MGA_TVO_CABLE_SCART_TYPE2,

static void program_cabletype(struct mga_tvo_dev *tdev)
{
	static const u8 mode_vals[] = {
		[MGA_TVO_MODE_CRT]   = 0x03,
		[MGA_TVO_MODE_TVOUT] = 0x08,
		[MGA_TVO_MODE_BT656] = 0x80,
	};
	static const u8 cable_vals[] = {
		[MGA_TVO_CABLE_COMPOSITE_SVIDEO] = 0x00,
		[MGA_TVO_CABLE_SCART_COMPOSITE]  = 0x01,
		[MGA_TVO_CABLE_SCART_RGB]        = 0x05,
		[MGA_TVO_CABLE_SCART_TYPE2]      = 0x02,
	};
	u8 val = mode_vals[tdev->config.mode];

	if (tdev->config.mode != MGA_TVO_MODE_CRT)
		val |= cable_vals[tdev->config.cable_type];

	tvo_write8(tdev, 0xb0, val);
}

/*
 * To keep the sync_pol_actual counter in sync with the hardware
 * register 0xb9 should only be programmed by strobe_sync_polarity()
 * or program_sync_polarity().
 *
 * Should only be called directly when TV output is used
 * (leaves bit 0x20 high).
 */
static void strobe_sync_polarity(struct mga_tvo_dev *tdev, u8 val)
{
	tvo_write8(tdev, 0xb9, val & ~0x20);
	tvo_write8(tdev, 0xb9, val | 0x20);
	tdev->sync_pol_actual = (tdev->sync_pol_actual + 1) & 3;
}

/*
 * Should only be called when CRT output is used (leaves bit 0x20 low).
 */
static void program_sync_polarity(struct mga_tvo_dev *tdev)
{
	while (tdev->sync_pol_actual != tdev->sync_pol_wanted)
		strobe_sync_polarity(tdev, 0x1a);

	tvo_write8(tdev, 0xb9, 0x1a);
}

static void program_8d(struct mga_tvo_dev *tdev)
{
	u8 val = 0x00;

	switch (tdev->config.mode) {
	case MGA_TVO_MODE_CRT:
		break;
	case MGA_TVO_MODE_BT656:
		val |= 0x03;
		break;
	case MGA_TVO_MODE_TVOUT:
		if (tdev->config.gamma != 10)
			val |= 0x04;
		if (tdev->config.text_filter)
			val |= 0x08;
		if (tdev->config.color_bars)
			val |= 0x10;
		break;
	}

	tvo_write8(tdev, 0x8d, val);
}

void mga_tvo_program_panel(struct mga_tvo_dev *tdev,
			   const struct mga_tvo_config *config,
			   const struct drm_display_mode *mode,
			   const struct mga_dac_pll_settings *pll)
{
	struct mga_tvo_crtc_regs regs;

	BUG_ON(config->mode != MGA_TVO_MODE_CRT);
	BUG_ON(mga_tvo_check_config(tdev, config));

	tdev->config = *config;

	calc_crtc_regs_crt(config, mode, &regs);

	tvo_write16(tdev, 0x8e, 0x1eff);
	tvo_write8(tdev, 0xc6, 0x01);

	program_pll(tdev, pll);

	program_crtc(tdev, &regs);

	tvo_write8(tdev, 0xb3, 0x01);

	tvo_write8(tdev, 0x8c, 0x20);

	tvo_write8(tdev, 0x94, 0xa2);

	tdev->enabled = true;
}

void mga_tvo_program_crt(struct mga_tvo_dev *tdev,
			 const struct mga_tvo_config *config,
			 const struct drm_display_mode *mode,
			 const struct mga_dac_pll_settings *pll)
{
	struct mga_tvo_crtc_regs regs;

	BUG_ON(config->mode != MGA_TVO_MODE_CRT);
	BUG_ON(mga_tvo_check_config(tdev, config));

	tdev->config = *config;

#ifdef TVO_WINDOWS
	calc_crtc_regs_crt_windows(config, mode, &regs);
#elif defined(TVO_BEOS)
	calc_crtc_regs_crt_beos(config, mode, &regs);
#else
	calc_crtc_regs_crt(config, mode, &regs);
#endif

	tvo_write16(tdev, 0x8e, 0x1eff);
	tvo_write8(tdev, 0xc6, 0x01);

	tvo_write8(tdev, 0xb0, 0x03);

	program_sync_polarity(tdev);
	//tvo_write8(tdev, 0xb9, 0x1a);

	tvo_write8(tdev, 0xbf, 0x22);

	program_8d(tdev);
	//tvo_write8(tdev, 0x8d, 0x00);

	tvo_write8(tdev, 0x8c, 0x20);

	tvo_write8(tdev, 0x94, 0xb2);

	program_pll(tdev, pll);

	program_crtc(tdev, &regs);

	tvo_write8(tdev, 0xb3, 0x01);

	tvo_write16(tdev, 0xaa, regs.hvidrst);

	tdev->enabled = true;
}

int mga_tvo_calc_tvout(struct mga_tvo_dev *tdev,
		       const struct mga_tvo_config *config,
		       const struct drm_display_mode *mode,
		       const struct mga_dac_pll_settings *pll)
{
	struct mga_tvo_crtc_regs regs;
	struct mga_dac_pll_settings pll2;
	int ret;

	ret = calc_tvout(tdev, config, mode, &regs, &pll2);
	if (ret)
		dev_err(tdev->dev, "Invalid mode for TV-out\n");

	return ret;
}

void mga_tvo_program_tvout(struct mga_tvo_dev *tdev,
			   const struct mga_tvo_config *config,
			   const struct drm_display_mode *mode,
			   const struct mga_dac_pll_settings *pll)
{
	struct mga_tvo_crtc_regs regs;
	struct mga_dac_pll_settings pll2;

	BUG_ON(config->mode != MGA_TVO_MODE_TVOUT);
	BUG_ON(mga_tvo_check_config(tdev, config));

	if (calc_tvout(tdev, config, mode, &regs, &pll2)) {
		dev_err(tdev->dev, "Invalid mode for TV-out\n");
		return;
	}

	tdev->config = *config;

	cve2_power(&tdev->cdev, false);

	tvo_write16(tdev, 0x8e, 0x1eff);
	tvo_write8(tdev, 0xc6, 0x01);

	cve2_program_regs(&tdev->cdev);

	tvo_write8(tdev, 0xb3, 0x01);

	program_cabletype(tdev);
	//tvo_write8(tdev, 0xb0, 0x08);

	strobe_sync_polarity(tdev, 0x38);
	//tvo_write8(tdev, 0xb9, 0x38);

	tvo_write8(tdev, 0xbf, 0x02);

	tvo_write8(tdev, 0x94, 0xb3);

	program_pll(tdev, &pll2);

	tvo_write8(tdev, 0x8c, 0x20);

	program_8d(tdev);
	//tvo_write8(tdev, 0x8d, 0x00);

	program_crtc(tdev, &regs);

	program_8d(tdev);
	program_deflicker(tdev);
	cve2_program_adjustments(&tdev->cdev);
	program_gamma(tdev);

	program_cabletype(tdev);
	//tvo_write8(tdev, 0xb0, 0x08);

	cve2_power(&tdev->cdev, true);

	tvo_write8(tdev, 0x95, 0x20);

	tvo_write16(tdev, 0xaa, regs.hvidrst);

	tdev->enabled = true;
}

void mga_tvo_program_bt656(struct mga_tvo_dev *tdev,
			   const struct mga_tvo_config *config,
			   const struct drm_display_mode *mode,
			   const struct mga_dac_pll_settings *pll)
{
	(void)mode;

	BUG_ON(config->mode != MGA_TVO_MODE_BT656);
	BUG_ON(mga_tvo_check_config(tdev, config));

	tdev->config = *config;

	cve2_power(&tdev->cdev, false);

	//tvo_write16(tdev, 0x8e, 0x1eff);
	//tvo_write8(tdev, 0xc6, 0x01);

	cve2_program_regs(&tdev->cdev);

	tvo_write8(tdev, 0xb3, 0x01);

	tvo_write8(tdev, 0x82, 0xA0);

	tvo_write8(tdev, 0xd3, 0x01);

	tvo_write8(tdev, 0x8c, 0x10);

	tvo_write8(tdev, 0x94, 0xa2);

	program_8d(tdev);
	//tvo_write8(tdev, 0x8d, 0x03);

	tvo_write8(tdev, 0xb9, 0x38);

	tvo_write8(tdev, 0xbf, 0x02);

	program_8d(tdev);
	program_deflicker(tdev);
	cve2_program_adjustments(&tdev->cdev);
	program_gamma(tdev);

	program_cabletype(tdev);
	//tvo_write8(tdev, 0xb0, 0x80);
}

void mga_tvo_enable_bt656(struct mga_tvo_dev *tdev)
{
	tvo_write8(tdev, 0x82, 0x20);

	cve2_power(&tdev->cdev, true);

	tdev->enabled = true;
}

void mga_tvo_program(struct mga_tvo_dev *tdev,
		     const struct mga_tvo_config *config,
		     const struct drm_display_mode *mode,
		     const struct mga_dac_pll_settings *pll)
{
	switch (config->mode) {
	case MGA_TVO_MODE_CRT:
		mga_tvo_program_crt(tdev, config, mode, pll);
		break;
	case MGA_TVO_MODE_TVOUT:
		mga_tvo_program_tvout(tdev, config, mode, pll);
		break;
	case MGA_TVO_MODE_BT656:
		mga_tvo_program_bt656(tdev, config, mode, pll);
		break;
	default:
		BUG();
	}
}

void mga_tvo_monitor_sense(struct mga_tvo_dev *tdev,
			   bool *r, bool *g, bool *b)
{
	u8 sense;
	u8 tmp8;
	u16 tmp16;

	tvo_write16(tdev, 0x8e, 0x1eff);
	tvo_write8(tdev, 0xc6, 0x01);

	tvo_write8(tdev, 0x35, 0xa0);

	tvo_write16(tdev, 0x10, 0x0028);
	tvo_write16(tdev, 0x0e, 0x0028);
	tvo_write16(tdev, 0x3c, 0x0028);

	tmp16 = tvo_read16(tdev, 0xa0);
	tmp16 -= 0x0002;
	tvo_write16(tdev, 0x9a, tmp16);

	tvo_write8(tdev, 0xb3, 0x01);

	tmp8 = tvo_read8(tdev, 0xbf);
	tmp8 |= 0x10;
	tvo_write8(tdev, 0xbf, tmp8);

	tvo_write8(tdev, 0xb0, 0x01);

	tvo_write8(tdev, 0x0b, 0x00);

	tvo_read8(tdev, 0x06);
	tvo_write8(tdev, 0x06, 0xf9);

	sense = tvo_read8(tdev, 0xb8);

	tvo_write16(tdev, 0x8e, 0x1eff);
	tvo_write8(tdev, 0xc6, 0x01);

	// 0x0 and 0x7 are bad?
	// FIXME figure out how these bits work
	*r = !(sense & 0x01);
	*g = !(sense & 0x02);
	*b = !(sense & 0x04);
}

void mga_tvo_reset_mafc(struct mga_tvo_dev *tdev)
{
	tvo_write8(tdev, 0xd4, 0x01);
	tvo_write8(tdev, 0xd4, 0x00);
}

void mga_tvo_set_sync(struct mga_tvo_dev *tdev, unsigned int sync)
{
	/* Convert into sync polarity state machine state */
	switch (sync & (DRM_MODE_FLAG_PHSYNC | DRM_MODE_FLAG_PVSYNC)) {
	case 0:
		tdev->sync_pol_wanted = 2;
		break;
	case DRM_MODE_FLAG_PHSYNC:
		tdev->sync_pol_wanted = 3;
		break;
	case DRM_MODE_FLAG_PVSYNC:
		tdev->sync_pol_wanted = 1;
		break;
	case DRM_MODE_FLAG_PHSYNC | DRM_MODE_FLAG_PVSYNC:
		tdev->sync_pol_wanted = 0;
		break;
	}

	if (!tdev->enabled || tdev->config.mode != MGA_TVO_MODE_CRT)
		return;

	program_sync_polarity(tdev);
}

#include <stdio.h>
int mga_tvo_test(struct mga_tvo_dev *tdev)
{
	dev_dbg(tdev->dev, "TVO TEST BEGIN:\n");

	while (1) {
		char buf[32];
		unsigned int reg, val;
		char len;

		char *s = fgets(buf, sizeof buf, stdin);
		if (!s)
			continue;
		buf[sizeof buf - 1] = 0;

		if (buf[0] == 'q')
			return 1;
		if (buf[0] == 'd') {
			unsigned int dot_crawl_freeze;
			if (sscanf(buf, "d/%u", &dot_crawl_freeze) != 1)
				continue;
			if (mga_tvo_set_dot_crawl_freeze(tdev, dot_crawl_freeze))
				dev_err(tdev->dev, "invalid dot crawl freeze\n");
			continue;
		}
		if (buf[0] == 'f') {
			unsigned int deflicker;
			if (sscanf(buf, "f/%u", &deflicker) != 1)
				continue;
			if (mga_tvo_set_deflicker(tdev, deflicker))
				dev_err(tdev->dev, "invalid deflicker\n");
			continue;
		}
		if (buf[0] == 'g') {
			unsigned int gamma;
			if (sscanf(buf, "g/%u", &gamma) != 1)
				continue;
			if (mga_tvo_set_gamma(tdev, gamma))
				dev_err(tdev->dev, "invalid gamma\n");
			continue;
		}
		if (buf[0] == 't') {
			unsigned int text_filter;
			if (sscanf(buf, "t/%u", &text_filter) != 1)
				continue;
			if (mga_tvo_set_text_filter(tdev, text_filter))
				dev_err(tdev->dev, "invalid text filter\n");
			continue;
		}
		if (buf[0] == 'c') {
			unsigned int color_bars;
			if (sscanf(buf, "c/%u", &color_bars) != 1)
				continue;
			if (mga_tvo_set_color_bars(tdev, color_bars))
				dev_err(tdev->dev, "invalid color bars\n");
			continue;
		}

		int write = 1;
		if (sscanf(buf, "%c/%x=%d", &len, &reg, &val) != 3) {
			if (sscanf(buf, "%c/%x", &len, &reg) != 2) {
				continue;
			}
			write = 0;
		}

		if (reg < 0x80) {
			cve2_test(&tdev->cdev, len, reg, val, write);
			continue;
		}

		if (reg < 0x80 || reg > 0xD4)
			continue;

		if (len != 'b' && len != 'w')
			continue;

		if (len == 'w') {
			if (write) {
				if (val > 0xffff)
					continue;
				//val = ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
				tvo_write16(tdev, reg, val);
			} else {
				val = tvo_read16(tdev, reg);
				val = ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
				dev_dbg(tdev->dev, "%02x = %04x (%d)\n",
					reg, val, val);
			}
		}
		if (len == 'b') {
			if (write) {
				if (val > 0xff)
					continue;
				tvo_write8(tdev, reg, val);
			} else {
				val = tvo_read8(tdev, reg);
				dev_dbg(tdev->dev, "%02x = %02x   (%d)\n",
					reg, val, val);
			}
		}
	}

	return 0;
}

static void mga_tvo_dump(struct mga_tvo_dev *tdev)
{
	dev_dbg(tdev->dev, "TVO registers:\n");
	tvo_read8(tdev, 0x80);
	tvo_read8(tdev, 0x81);
	tvo_read8(tdev, 0x82);
	tvo_read8(tdev, 0x83);
	tvo_read8(tdev, 0x84);
	tvo_read8(tdev, 0x85);
	tvo_read8(tdev, 0x86);
	tvo_read8(tdev, 0x87);
	tvo_read8(tdev, 0x88);
	tvo_read8(tdev, 0x89);
	tvo_read8(tdev, 0x8a);
	tvo_read8(tdev, 0x8b);
	tvo_read8(tdev, 0x8c);
	tvo_read8(tdev, 0x8d);
	tvo_read8(tdev, 0x8e);
	tvo_read8(tdev, 0x90);
	tvo_read16(tdev, 0x91);
	tvo_read8(tdev, 0x93);
	tvo_read8(tdev, 0x94);
	tvo_read8(tdev, 0x95);
	tvo_read16(tdev, 0x96);
	tvo_read16(tdev, 0x98);
	tvo_read16(tdev, 0x9a);
	tvo_read16(tdev, 0x9c);
	tvo_read16(tdev, 0x9e);
	tvo_read16(tdev, 0xa0);
	tvo_read16(tdev, 0xa2);
	tvo_read16(tdev, 0xa4);
	tvo_read16(tdev, 0xa6);
	tvo_read16(tdev, 0xa8);
	tvo_read16(tdev, 0xaa);
	tvo_read16(tdev, 0xac);
	tvo_read16(tdev, 0xae);
	tvo_read8(tdev, 0xb0);
	tvo_read8(tdev, 0xb1);
	tvo_read8(tdev, 0xb2);
	tvo_read8(tdev, 0xb3);
	tvo_read8(tdev, 0xb4);//32
	tvo_read8(tdev, 0xb5);//32
	tvo_read8(tdev, 0xb6);//32
	tvo_read8(tdev, 0xb7);//32
	tvo_read8(tdev, 0xb8);
	tvo_read8(tdev, 0xb9);
	tvo_read16(tdev, 0xba);
	tvo_read16(tdev, 0xbc);
	tvo_read8(tdev, 0xbe);
	tvo_read8(tdev, 0xbf);
	tvo_read16(tdev, 0xc0);
	tvo_read8(tdev, 0xc2);
	tvo_read16(tdev, 0xc3);
	tvo_read8(tdev, 0xc5);
	tvo_read8(tdev, 0xc6);
	tvo_read8(tdev, 0xc7);
	tvo_read8(tdev, 0xc8);
	tvo_read8(tdev, 0xc9);
	tvo_read8(tdev, 0xca);
	tvo_read8(tdev, 0xcb);
	tvo_read8(tdev, 0xcc);
	tvo_read8(tdev, 0xcd);
	tvo_read8(tdev, 0xce);
	tvo_read8(tdev, 0xcf);
	tvo_read8(tdev, 0xd0);
	tvo_read8(tdev, 0xd1);
	tvo_read8(tdev, 0xd2);
	tvo_read8(tdev, 0xd3);
	tvo_read8(tdev, 0xd4);
}

static void mga_tvo_pll_save(struct mga_tvo_dev *tdev,
		      struct mga_dac_pll_settings *pll)
{
	pll->m = tvo_read8(tdev, 0x80);
	pll->n = tvo_read8(tdev, 0x81);
	pll->p = tvo_read8(tdev, 0x82);
}

static void mga_tvo_save_crtc_regs(struct mga_tvo_dev *tdev,
				   struct mga_tvo_crtc_regs *regs)
{
	regs->hscale = tvo_read8(tdev, 0x90);
	regs->vscale = tvo_read16(tdev, 0x91);

	regs->hsyncend = tvo_read16(tdev, 0x9a);
	regs->hblkend = tvo_read16(tdev, 0x9c);
	regs->hblkstr = tvo_read16(tdev, 0x9e);
	regs->htotal = tvo_read16(tdev, 0xa0);

	regs->vsyncend = tvo_read16(tdev, 0xa2);
	regs->vblkend = tvo_read16(tdev, 0xa4);
	regs->vblkstr = tvo_read16(tdev, 0xa6);
	regs->vtotal = tvo_read16(tdev, 0xa8);

	regs->reg_98 = tvo_read16(tdev, 0x98);
	regs->reg_ae = tvo_read16(tdev, 0xae);

	regs->htotal_last = tvo_read16(tdev, 0x96);

	regs->hvidrst = tvo_read16(tdev, 0xaa);
	regs->vvidrst = tvo_read16(tdev, 0xac);

	regs->vdisp = tvo_read8(tdev, 0xbe);
	regs->hdisp = tvo_read8(tdev, 0xc2);
}

static void mga_tvo_save(struct mga_tvo_dev *tdev)
{
	int i;

	/* 0x83 - 0x8b */
	for (i = 0; i < 9; i++)
		tdev->gamma_vals[i] = tvo_read8(tdev, 0x83 + i);

	tvo_read8(tdev, 0x8c);
	tvo_read8(tdev, 0x8d);
	tvo_read8(tdev, 0x8e);
	tvo_read8(tdev, 0x93);//deflicker
	tvo_read8(tdev, 0x94);
	tvo_read8(tdev, 0x95);
	tvo_read8(tdev, 0xb0);
	tvo_read8(tdev, 0xb1);
	tvo_read8(tdev, 0xb2);
	tvo_read8(tdev, 0xb3);
	tvo_read8(tdev, 0xb4);//32
	tvo_read8(tdev, 0xb5);//32
	tvo_read8(tdev, 0xb6);//32
	tvo_read8(tdev, 0xb7);//32
	tvo_read8(tdev, 0xb8);
	tvo_read8(tdev, 0xb9);
	tvo_read16(tdev, 0xba);
	tvo_read16(tdev, 0xbc);
	tvo_read8(tdev, 0xbf);
	tvo_read16(tdev, 0xc0);
	tvo_read16(tdev, 0xc3);
	tvo_read8(tdev, 0xc5);
	tvo_read8(tdev, 0xc6);
	tvo_read8(tdev, 0xc7);
	tvo_read8(tdev, 0xc8);
	tvo_read8(tdev, 0xc9);
	tvo_read8(tdev, 0xca);
	tvo_read8(tdev, 0xcb);
	tvo_read8(tdev, 0xcc);
	tvo_read8(tdev, 0xcd);
	tvo_read8(tdev, 0xce);
	tvo_read8(tdev, 0xcf);
	tvo_read8(tdev, 0xd0);
	tvo_read8(tdev, 0xd1);
	tvo_read8(tdev, 0xd2);
	tvo_read8(tdev, 0xd3);
	tvo_read8(tdev, 0xd4);//reset_mafc
}

static void mga_tvo_decode_regs(struct mga_tvo_dev *tdev)
{
	u8 reg_8d = 0;
	u8 reg_93 = 0;
	u8 reg_b0 = 0;
	u8 reg_b3 = 0;

	switch (reg_8d & 0x03) {
	case 0x00:
		if (reg_b0 & 0x08)
			tdev->config.mode = MGA_TVO_MODE_TVOUT;
		else
			tdev->config.mode = MGA_TVO_MODE_CRT;
		break;
	case 0x03:
		tdev->config.mode = MGA_TVO_MODE_BT656;
		break;
	default:
		dev_dbg(tdev->dev, "Unknown TVO mode 0x%02x\n", reg_8d);
		break;
	}

	if (tdev->config.mode != MGA_TVO_MODE_CRT) {
		switch (reg_b0 & 0x07) {
		case 0x00:
			tdev->config.cable_type = MGA_TVO_CABLE_COMPOSITE_SVIDEO;
			break;
		case 0x01:
			tdev->config.cable_type = MGA_TVO_CABLE_SCART_COMPOSITE;
			break;
		case 0x05:
			tdev->config.cable_type = MGA_TVO_CABLE_SCART_RGB;
			break;
		case 0x02:
			tdev->config.cable_type = MGA_TVO_CABLE_SCART_TYPE2;
			break;
		default:
			dev_dbg(tdev->dev, "Unknown TVO cable type 0x%02x\n", reg_b0);
			break;
		}
	}

	if (reg_8d & 0x04) {
		int i;

		for (i = 0; i < (int) ARRAY_SIZE(tvo_gamma_vals); i++) {
			if (!memcmp(tvo_gamma_vals[i], tdev->gamma_vals, sizeof tvo_gamma_vals[0])) {
				tdev->config.gamma = (i << 1) + 4;
				break;
			}
		}
	} else
		tdev->config.gamma = 10;

	tdev->config.text_filter = reg_8d & 0x08;
	tdev->config.color_bars = reg_8d & 0x10;

	switch (reg_93) {
	case 0x00:
		tdev->config.deflicker = 0;
		break;
	case 0xb1:
		tdev->config.deflicker = 1;
		break;
	case 0xa2:
		tdev->config.deflicker = 2;
		break;
	default:
		dev_dbg(tdev->dev, "Unknown TVO deflicker 0x%02x\n", reg_93);
		break;
	}

	/* FIXME verify */
	tdev->enabled = reg_b3 & 0x01;
}

void mga_tvo_disable(struct mga_tvo_dev *tdev)
{
	/*
	 * Leave the sync polarity bit high so that tv-out programming
	 * sequence won't cause a low to high transition if monitor
	 * mode was used before.
	 * The bit can be high or low at this point so force a low to
	 * high transition to keep the counter in sync with the hardware.
	 */
	strobe_sync_polarity(tdev, 0x38);

	cve2_power(&tdev->cdev, false);

	tvo_write8(tdev, 0x82, 0xa0);

	tvo_write8(tdev, 0x8c, 0x00);

	tvo_write8(tdev, 0x94, 0xa2);

	tvo_write16(tdev, 0x8e, 0x1eff);
	tvo_write8(tdev, 0xc6, 0x01);

	tdev->enabled = false;
}

int mga_tvo_init(struct mga_tvo_dev *tdev,
		 struct mga_dev *mdev,
		 u8 addr)
{
	const struct cve2_adjustments *desktop_adj;
	const struct cve2_adjustments *video_adj;
	const struct cve2_bwlevel *bwlevel;
	const struct cve2_regs *regs;
	int ret;

	switch (mdev->chip) {
	case MGA_CHIP_G100:
	case MGA_CHIP_G200:
#if 0
		desktop_adj = g100_cve2_desktop_adj;
		video_adj = g100_cve2_video_adj;
		bwlevel = g100_cve2_bwlevel;
		regs = g100_cve2_regs;
		break;
#endif
	case MGA_CHIP_G400:
		desktop_adj = g400_cve2_desktop_adj;
		video_adj = g400_cve2_video_adj;
		bwlevel = g400_cve2_bwlevel;
		regs = g400_cve2_regs;
		break;
	default:
		BUG();
	}

	ret = i2c_tvo_read_byte_data(&mdev->misc.adap, addr, 0xb2);
	if (ret < 0) {
		dev_dbg(mdev->dev, "Failed to detect MGA-TVO @ @%02x\n", addr);
		return ret;
	}

	dev_dbg(mdev->dev, "Detected MGA-TVO rev. %02x @ %02x\n", ret, addr);

	tdev->version = ret;

	cve2_init(&tdev->cdev, mdev->dev,
		  desktop_adj, video_adj,
		  bwlevel, regs,
		  cve2_write8, cve2_write16,
		  cve2_read8, cve2_read16,
		  tdev);

	tdev->dev = mdev->dev;
	tdev->adap = &mdev->misc.adap;
	tdev->addr = addr;

	/* defaults */
	tdev->enabled = false;
	tdev->config.mode = MGA_TVO_MODE_TVOUT;
	tdev->config.text_filter = false;
	tdev->config.color_bars = false;
	tdev->config.gamma = 10;
	tdev->config.deflicker = 0;
	tdev->config.cable_type = MGA_TVO_CABLE_COMPOSITE_SVIDEO;

	/*
	 * An external reset must be issued afterwards to guarantee
	 * that the actual hardware state will match this counter.
	 */
	tdev->sync_pol_actual = 0;
	tdev->sync_pol_wanted = 0;
	tvo_write8(tdev, 0xb9, 0x38);

	//mga_tvo_dump(tdev);

	return 0;
}

int mga_tvo_set_tv_standard(struct mga_tvo_dev *tdev, unsigned int std)
{
	int ret;

	/* FIXME? */
	if (tdev->enabled)
		return -ENXIO;

	return cve2_set_tv_standard(&tdev->cdev, std);
}

int mga_tvo_set_dot_crawl_freeze(struct mga_tvo_dev *tdev, bool enable)
{
	return cve2_set_dot_crawl_freeze(&tdev->cdev, enable);
}

int mga_tvo_set_color_bars(struct mga_tvo_dev *tdev, bool enable)
{
	if (tdev->config.color_bars == enable)
		return 0;

	tdev->config.color_bars = enable;

	if (!tdev->enabled || tdev->config.mode != MGA_TVO_MODE_TVOUT)
		return 0;

	program_8d(tdev);

	return 0;
}

int mga_tvo_set_text_filter(struct mga_tvo_dev *tdev, bool enable)
{
	if (tdev->config.text_filter == enable)
		return 0;

	tdev->config.text_filter = enable;

	if (!tdev->enabled || tdev->config.mode != MGA_TVO_MODE_TVOUT)
		return 0;

	program_8d(tdev);

	return 0;
}

int mga_tvo_set_deflicker(struct mga_tvo_dev *tdev, u8 deflicker)
{
	if (deflicker > 2)
		return -EINVAL;

	if (tdev->config.deflicker == deflicker)
		return 0;

	tdev->config.deflicker = deflicker;

	if (!tdev->enabled || tdev->config.mode != MGA_TVO_MODE_TVOUT)
		return 0;

	program_deflicker(tdev);

	return 0;
}

int mga_tvo_set_gamma(struct mga_tvo_dev *tdev, u8 gamma)
{
	if (gamma < 4 || gamma > 40)
		return -EINVAL;

	if (tdev->config.gamma == gamma)
		return 0;

	tdev->config.gamma = gamma;
	memcpy(tdev->gamma_vals, tvo_gamma_vals[(gamma - 4) >> 1], sizeof tdev->gamma_vals);

	if (!tdev->enabled || tdev->config.mode != MGA_TVO_MODE_TVOUT)
		return 0;

	program_8d(tdev);
	program_gamma(tdev);

	return 0;
}

int mga_tvo_set_cable_type(struct mga_tvo_dev *tdev,
			   enum mga_tvo_cable_type cable_type)
{
	switch (cable_type) {
	case MGA_TVO_CABLE_COMPOSITE_SVIDEO:
	case MGA_TVO_CABLE_SCART_COMPOSITE:
	case MGA_TVO_CABLE_SCART_RGB:
	case MGA_TVO_CABLE_SCART_TYPE2:
		break;
	default:
		return -EINVAL;
	}

	if (tdev->config.cable_type == cable_type)
		return 0;

	tdev->config.cable_type = cable_type;

	if (!tdev->enabled || tdev->config.mode == MGA_TVO_MODE_CRT)
		return 0;

	program_cabletype(tdev);

	return 0;
}

int mga_tvo_check_config(const struct mga_tvo_dev *tdev,
			 const struct mga_tvo_config *config)
{
	switch (config->mode) {
	case MGA_TVO_MODE_CRT:
	case MGA_TVO_MODE_TVOUT:
	case MGA_TVO_MODE_BT656:
		dev_dbg(tdev->dev, "Unsupported TVO mode %u\n", config->mode);
		return -EINVAL;
	}

	switch (config->cable_type) {
	case MGA_TVO_CABLE_COMPOSITE_SVIDEO:
	case MGA_TVO_CABLE_SCART_RGB:
	case MGA_TVO_CABLE_SCART_TYPE2:
		break;
	default:
		dev_dbg(tdev->dev, "Unsupported cable type %u\n", config->cable_type);
		return -EINVAL;
	}

	switch (config->tv_standard) {
	case DRM_TV_STD_PAL:
	case DRM_TV_STD_NTSC:
		break;
	default:
		dev_dbg(tdev->dev, "Unsupported TV standard %u\n", config->tv_standard);
		return -EINVAL;
	}

	if (config->gamma < 4 || config->gamma > 40) {
		dev_dbg(tdev->dev, "Unsupported gamma %u\n", config->gamma);
		return -EINVAL;
	}

	if (config->deflicker > 2) {
		dev_dbg(tdev->dev, "Deflicker not supported\n");
		return -EINVAL;
	}

	return 0;
}
