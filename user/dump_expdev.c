/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "mga_dump.h"

enum {
	MGA_EXPDEV_START = 0x3E00,
	MGA_EXPDEV_END   = 0x3FFF,
};

void dump_expdev(struct mga_dev *mdev)
{
	unsigned int off;

	if (mdev->chip > MGA_CHIP_G100)
		return;

	dev_dbg(mdev->dev, "Dump of EXPDEV registers:\n");

	off = MGA_EXPDEV_START;
	while (off <= MGA_EXPDEV_END) {
		unsigned int i, max = min(MGA_EXPDEV_END - off + 1, 16);
		if (!(off & 63))
			dev_dbg(mdev->dev, "EXTCS%u\n", (off - MGA_EXPDEV_START) / 64);
		dev_dbg(mdev->dev, "%04x: ", off);
		for (i = 0; i < max; i++) {
			u8 val = mga_read8(mdev, off + i);
			printk(KERN_CONT " %02x", val);
		}
		printk(KERN_CONT "\n");
		off += max;
	}
}
