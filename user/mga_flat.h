/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_FLAT_H
#define MGA_FLAT_H

struct mga_dev;
struct mga_pll_settings;

int mga_tmds_builtin_probe(struct mga_dev *mdev);
int mga_tmds_addon_probe(struct mga_dev *mdev);

void mga_tmds_power(struct mga_dev *mdev, bool power);

int mga_av9110_pll_calc(struct mga_dev *mdev, unsigned int freq,
			struct mga_pll_settings *pll);

int mga_av9110_pll_program(struct mga_dev *mdev,
			   const struct mga_pll_settings *pll);

void mga_av9110_pll_power(struct mga_dev *mdev, bool enable);

#endif
