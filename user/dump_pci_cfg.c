/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "mga_dump.h"

static void dump_reg(struct pci_dev *pdev, const char *name, u32 addr)
{
	dev_dbg(&pdev->dev, "%s = 0x%08x\n", name, pci_cfg_read32(pdev, addr));
}

enum {
	MASK_2064W = 1 << MGA_CHIP_2064W,
	MASK_2164W = 1 << MGA_CHIP_2164W,
	MASK_1064SG = 1 << MGA_CHIP_1064SG,
	MASK_1164SG = 1 << MGA_CHIP_1164SG,

	MASK_G100 = 1 << MGA_CHIP_G100,
	MASK_G200 = ((1 << MGA_CHIP_G200) |
		     (1 << MGA_CHIP_G200SE) |
		     (1 << MGA_CHIP_G200EV) |
		     (1 << MGA_CHIP_G200WB)),
	MASK_G400 = 1 << MGA_CHIP_G400,
	MASK_G450 = 1 << MGA_CHIP_G450,
	MASK_G550 = 1 << MGA_CHIP_G550,

	MASK_G = MASK_G100 | MASK_G200 | MASK_G400 | MASK_G450 | MASK_G550,
	MASK_ALL = MASK_2064W | MASK_1064SG | MASK_2164W | MASK_1164SG | MASK_G,
};

static const struct {
	u32 addr;
	const char *name;
	unsigned int chip_mask;
} registers[] = {
	{ 0x00, "DEVID    ", MASK_ALL },
	{ 0x04, "DEVCTRL  ", MASK_ALL },
	{ 0x08, "CLASS    ", MASK_ALL },
	{ 0x0C, "HEADER   ", MASK_ALL },
	{ 0x10, "MGABASE1 ", MASK_2064W | MASK_1064SG },
	{ 0x14, "MGABASE2 ", MASK_2064W | MASK_1064SG },
	{ 0x18, "MGABASE3 ",              MASK_1064SG },
	{ 0x10, "MGABASE2 ", MASK_2164W | MASK_1164SG | MASK_G },
	{ 0x14, "MGABASE1 ", MASK_2164W | MASK_1164SG | MASK_G },
	{ 0x18, "MGABASE3 ", MASK_2164W | MASK_1164SG | MASK_G },
	{ 0x2C, "SUBSYSID ", MASK_ALL },
	{ 0x30, "ROMBASE  ", MASK_ALL },
	{ 0x3C, "INTCTRL  ", MASK_ALL },
	{ 0x40, "OPTION   ", MASK_ALL },
	{ 0x44, "MGA_INDEX", MASK_ALL },
	{ 0x48, "MGA_DATA ", MASK_ALL },
	{ 0x50, "OPTION2  ", MASK_G },
	{ 0x54, "OPTION3  ", MASK_G400 | MASK_G450 | MASK_G550 },
	{ 0x58, "MEMMISC  ", MASK_G450 | MASK_G550 },
	// FIXME what else (esp. G450/G550)?
};

enum {
	PCI_DEVCTRL = 0x04,
	PCI_DEVCTRL_CAPLIST = 0x100000,

	PCI_CAP_PTR = 0x34,
	PCI_PM_IDENT = 0xDC,
	PCI_PM_CSR = 0xE0,
	PCI_AGP_IDENT = 0xF0,
	PCI_AGP_STS = 0xF4,
	PCI_AGP_CMD = 0xF8,
};

static void dump_caps(struct pci_dev *pdev)
{
	u32 cap_ptr;

	if (!(pci_cfg_read32(pdev, PCI_DEVCTRL) & PCI_DEVCTRL_CAPLIST)) {
		dev_dbg(&pdev->dev, "No capability list\n");
		return;
	}

	cap_ptr = pci_cfg_read32(pdev, PCI_CAP_PTR) & 0xFF;

	dev_dbg(&pdev->dev, "CAP_PTR = %02x\n", cap_ptr);

	while (cap_ptr) {
		switch (cap_ptr) {
		case PCI_AGP_IDENT:
			dump_reg(pdev, "AGP_IDENT", PCI_AGP_IDENT);
			dump_reg(pdev, "AGP_STS  ", PCI_AGP_STS);
			dump_reg(pdev, "AGP_CMD  ", PCI_AGP_CMD);
			cap_ptr = (pci_cfg_read32(pdev, PCI_AGP_IDENT) >> 8) & 0xFF;
			break;
		case PCI_PM_IDENT:
			dump_reg(pdev, "PM_IDENT ", PCI_PM_IDENT);
			dump_reg(pdev, "PM_CSR   ", PCI_PM_CSR);
			cap_ptr = (pci_cfg_read32(pdev, PCI_PM_IDENT) >> 8) & 0xFF;
			break;
		default:
			dev_err(&pdev->dev, "Unknown capability %02x\n", cap_ptr);
			cap_ptr = 0;
			break;
		}
		dev_dbg(&pdev->dev, "CAP_PTR = %02x\n", cap_ptr);
	}
}

void dump_pci_cfg(struct mga_dev *mdev)
{
	unsigned int i;

	if (pci_cfg_open(&mdev->pdev, mdev->fd))
		return;

	dev_dbg(mdev->dev, "Dump of PCI configuration space registers:\n");

	for (i = 0; i < ARRAY_SIZE(registers); i++)
		if (registers[i].chip_mask & (1 << mdev->chip))
			dump_reg(&mdev->pdev,
				 registers[i].name,
				 registers[i].addr);

	dump_caps(&mdev->pdev);
	fflush(stdout);
}
