/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef WAIT_H
#define WAIT_H

#include "kernel_emul.h"
#include <pthread.h>

static inline void init_waitqueue_head(wait_queue_head_t *wq)
{
	pthread_condattr_t attr;

	pthread_condattr_init(&attr);
	pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);

	pthread_mutex_init(&wq->mutex, NULL);
	pthread_cond_init(&wq->cond, &attr);
}

static inline void wake_up_all(wait_queue_head_t *wq)
{
	pthread_mutex_lock(&wq->mutex);
	pthread_cond_broadcast(&wq->cond);
	pthread_mutex_unlock(&wq->mutex);
}

#define wait_event(wq, COND) \
{ \
	for (;;) { \
		if (COND) \
			break; \
		pthread_mutex_lock(&(wq)->mutex); \
		pthread_cond_wait(&(wq)->cond, &(wq)->mutex); \
		pthread_mutex_unlock(&(wq)->mutex); \
	} \
}

#define wait_event_interruptible(wq, COND) \
({ \
	int __ret = -ERESTARTSYS; \
	for (;;) { \
		if (COND) { \
			__ret = 0; \
			break; \
		} \
		pthread_mutex_lock(&(wq)->mutex); \
		pthread_cond_wait(&(wq)->cond, &(wq)->mutex); \
		pthread_mutex_unlock(&(wq)->mutex); \
		if (signal_pending(current)) \
			break; \
	} \
	__ret; \
})

#define wait_event_timeout(wq, COND, timeout) \
({ \
	int __ret; \
	struct timespec __to, __tc, __t; \
	clock_gettime(CLOCK_MONOTONIC, &__tc); \
	__t.tv_sec = (timeout) / HZ; \
	__t.tv_nsec = ((timeout) % HZ) * (1000000000 / HZ); \
	timespecadd(&__tc, &__t, &__to); /* __to = __tc + __t; */ \
	for (;;) { \
		if (COND) { \
			clock_gettime(CLOCK_MONOTONIC, &__tc); \
			timespecsub(&__to, &__tc, &__t); /* _t = __to - __tc */ \
			__ret = __t.tv_sec * HZ + __t.tv_nsec / (1000000000 / HZ); \
			if (__ret < 1) \
				__ret = 1; \
			break; \
		} \
		pthread_mutex_lock(&(wq)->mutex); \
		__ret = pthread_cond_timedwait(&(wq)->cond, &(wq)->mutex, &__to); \
		pthread_mutex_unlock(&(wq)->mutex); \
		if (__ret == ETIMEDOUT) { \
			clock_gettime(CLOCK_MONOTONIC, &__tc);	\
			__ret = 0; \
			break; \
		} \
	} \
	__ret; \
})

#define wait_event_timeout_interruptible(wq, COND, timeout) \
({ \
	int __ret = -ERESTARTSYS; \
	struct timespec __to, __tc, __t; \
	clock_gettime(CLOCK_MONOTONIC, &__tc); \
	__t.tv_sec = (timeout) / HZ; \
	__t.tv_nsec = ((timeout) % HZ) * (1000000000 / HZ); \
	timespecadd(&__tc, &__t, &__to); /* __to = __tc + __t; */ \
	for (;;) { \
		if (COND) { \
			clock_gettime(CLOCK_MONOTONIC, &__tc); \
			timespecsub(&__to, &__tc, &__t); /* _t = __to - __tc */ \
			__ret = __t.tv_sec * HZ + __t.tv_nsec / (1000000000 / HZ); \
			if (__ret < 1) \
				__ret = 1; \
			break; \
		} \
		pthread_mutex_lock(&(wq)->mutex); \
		__ret = pthread_cond_timedwait(&(wq)->cond, &(wq)->mutex, &__to); \
		pthread_mutex_unlock(&(wq)->mutex); \
		if (__ret == ETIMEDOUT) { \
			clock_gettime(CLOCK_MONOTONIC, &__tc);	\
			__ret = 0; \
			break; \
		} \
		if (signal_pending(current)) \
			break; \
	} \
	__ret; \
})

#endif
