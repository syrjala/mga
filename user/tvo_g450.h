/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef MGA_G450_TVO_H
#define MGA_G450_TVO_H

#include "kernel_emul.h"
#include "drm_kms.h"
#include "cve2.h"
#include "mga_tvo_common.h"

struct mga_g450_tvo_dev {
	struct cve2_dev cdev;

	struct device *dev;

	struct mga_dev *mdev;

	bool enabled;
	u8 tv_standard;
	enum mga_tvo_cable_type cable_type;
};

int mga_g450_tvo_init(struct mga_g450_tvo_dev *tdev,
		      struct mga_dev *mdev);

void mga_g450_tvo_program_bt656(struct mga_g450_tvo_dev *tdev,
				const struct mga_tvo_config *config,
				const struct drm_display_mode *mode,
				unsigned int bpp);

void mga_g450_tvo_enable_bt656(struct mga_g450_tvo_dev *tdev);

void mga_g450_tvo_disable(struct mga_g450_tvo_dev *tdev);

int mga_g450_tvo_set_tv_standard(struct mga_g450_tvo_dev *tdev, u8 std);
int mga_g450_tvo_set_dot_crawl_freeze(struct mga_g450_tvo_dev *tdev, bool enable);
int mga_g450_tvo_set_cable_type(struct mga_g450_tvo_dev *tdev,
				enum mga_tvo_cable_type cable_type);

int mga_g450_tvo_test(struct mga_g450_tvo_dev *tdev);

int mga_g450_tvo_check_config(const struct mga_g450_tvo_dev *tdev,
			      const struct mga_tvo_config *config);

#endif
