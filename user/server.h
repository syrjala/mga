/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#ifndef SERVER_H
#define SERVER_H

#include "kernel_emul.h"
#include "drm_kms.h"

#define LE32_TO_CPU(x) (((x)[3] << 24) | \
			((x)[2] << 16) | \
			((x)[1] <<  8) | \
			((x)[0] <<  0))

#define CPU_TO_LE32(x, i) do {	\
	(x)[0] = (i) >>  0;	\
	(x)[1] = (i) >>  8;	\
	(x)[2] = (i) >> 16;	\
	(x)[3] = (i) >> 24;	\
} while (0)

enum {
	MGA_CMD_OPEN = 0xdeadbeef,
	MGA_CMD_CLOSE,
	MGA_CMD_READ,
	MGA_CMD_WRITE,
	MGA_CMD_IOCTL,
};

struct mga_cmd {
	u32 cmd;
	u32 serial;
	u32 len;
	u32 target;
	u8 data[0];
};

struct mga_res {
	u32 cmd;
	u32 serial;
	u32 len;
	u32 target;
	u32 code;
	u32 _errno;
	u8 data[0];
};

struct server_ioctl {
	u32 cmd;
	u32 pad;
	u8 data[0];
};

int open_socket(const char *path, struct sockaddr_un *addr);
ssize_t wrap_read(int fd, void *data, size_t len);
ssize_t wrap_write(int fd, const void *data, size_t len);

#endif
