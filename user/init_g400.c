/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include <stdlib.h>

#include "mga_dump.h"
#include "mga_dac.h"
#include "mga_dac_regs.h"
#include "mga_regs.h"
#include "mga_i2c.h"
#include "modes.h"
#include "mga_flat.h"
#include "mga_tvout.h"
#include "mga_dualhead.h"

static u32 mga_g400_save_cfg_or(struct mga_dev *mdev)
{
	u32 agp_sts;
	u32 cfg_or;

	cfg_or = mga_read32(mdev, MGA_CFG_OR);

	/* these are write-only fields */
	cfg_or &= ~(MGA_CFG_OR_RQ_OR | MGA_CFG_OR_RATE_CAP_OR |
		    MGA_CFG_OR_E2PBYP | MGA_CFG_OR_E2PBYPCLK |
		    MGA_CFG_OR_E2PBYPD | MGA_CFG_OR_E2PBYPCSN |
		    MGA_CFG_OR_E2PQ);

	/* determine rage_cap and rq_or from AGP_STS */
	agp_sts = pci_cfg_read32(&mdev->pdev, MGA_AGP_STS);
	cfg_or |= (agp_sts & MGA_AGP_STS_RATE_CAP) << 21;
	cfg_or |= (agp_sts & MGA_AGP_STS_RQ);

	if (mdev->pdev.revision > 0x4) {
		/* magic undocumented bits? */
		cfg_or |= 0x3;
		cfg_or |= 0x60000000;
	}

	return cfg_or;
}

#if 0
static void mga_g400_save(struct mga_dev *mdev)
{
	pci_cfg_save(&mdev->pdev);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	subsysid = pci_cfg_read32(&mdev->pdev, MGA_SUBSYSID_R);
	option2 = pci_cfg_read32(&mdev->pdev, MGA_OPTION2);
	option3 = pci_cfg_read32(&mdev->pdev, MGA_OPTION3);
	agp_cmd = pci_cfg_read32(&mdev->pdev, MGA_AGP_CMD);
	cfg_or = mga_g400_save_cfg_or(mdev);
	memrdbk = mga_read32(mdev, MGA_MEMRDBK);

	c2ctl;
	c2misc;
	c2datactl;
	// rest are WO?

	besctl;
	besglobctrl;
	// rest are WO?

	// write only regs
	mctlwtst

	// need to cooperate w/ bridge anyway
}

static voi mga_g400_restore(struct mga_dev *mdev)
{
	pci_cfg_write32(&mdev->pdev, MGA_SUBSYSID_W, subsysid);
	pci_cfg_write32(&mdev->pdev, MGA_AGP_CMD, agp_cmd);
	mga_write32(&mdev->pdev, MGA_CFG_OR, cfg_or);
}
#endif

static unsigned int calc_mclk(unsigned int syspll, u32 option3)
{
	unsigned int mclk = 0;

	switch (option3 & MGA_OPTION3_MCLKSEL) {
	case MGA_OPTION3_MCLKSEL_PCI:
		/* FIXME? */
		return 33333;
	case MGA_OPTION3_MCLKSEL_SYSPLL:
		mclk = syspll;
		break;
	case MGA_OPTION3_MCLKSEL_MCLK:
		/* FIXME? */
		return 0;
	case MGA_OPTION3_MCLKSEL_AGPDLL:
		/* FIXME? */
		mclk = 66667;
		break;
	}

	switch (option3 & MGA_OPTION3_MCLKDIV) {
	case MGA_OPTION3_MCLKDIV_1_3:
		return div_round(mclk * 1, 3);
	case MGA_OPTION3_MCLKDIV_2_5:
		return div_round(mclk * 2, 5);
	case MGA_OPTION3_MCLKDIV_4_9:
		return div_round(mclk * 4, 9);
	case MGA_OPTION3_MCLKDIV_1_2:
		return div_round(mclk * 1, 2);
	case MGA_OPTION3_MCLKDIV_2_3:
		return div_round(mclk * 2, 3);
	case MGA_OPTION3_MCLKDIV_1_1:
		return mclk;
	default:
		BUG();
	}
}

#if 0
static void mga_g400_clock_powerup(struct mga_dev *mdev)
{
	// fIXME
	u32 option;
	u8 val;

	/* 1. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 2. */
	option |= MGA_OPTION_SYSCLKSL_SYSPLL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 3. */
	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* 4. */
	mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXCLKCTRL);
	val = mga_read8(mdev, MGA_X_DATAREG);
	val |= MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 5. */
	val |= MGA_XPIXCLKCTRL_PIXCLKSEL_PIXPLL;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* 6. */
	val &= ~MGA_XPIXCLKCTRL_PIXCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);
}

{
	//G400 DH WINDOWS CFG_OR = 0x7708
}
#endif

static void sysclk_select(struct mga_dev *mdev, u32 mask, u32 clksel)
{
	u32 option, option3;

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);

	option |= MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	option3 = pci_cfg_read32(&mdev->pdev, MGA_OPTION3);
	option3 &= ~mask;
	option3 |= clksel & mask;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION3, option3);

	option &= ~MGA_OPTION_SYSCLKDIS;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);
}

enum {
	MGA_OPTION3_CLKSEL_PCI =
		MGA_OPTION3_GCLKSEL_PCI |
		MGA_OPTION3_MCLKSEL_PCI |
		MGA_OPTION3_WCLKSEL_PCI,
	MGA_OPTION3_CLKSEL =
		MGA_OPTION3_GCLKSEL |
		MGA_OPTION3_MCLKSEL |
		MGA_OPTION3_WCLKSEL,
	MGA_OPTION3_CLKDIV_CLKDCYC =
		MGA_OPTION3_GCLKDIV | MGA_OPTION3_GCLKDCYC |
		MGA_OPTION3_MCLKDIV | MGA_OPTION3_MCLKDCYC |
		MGA_OPTION3_WCLKDIV | MGA_OPTION3_WCLKDCYC,
};

static int sysclk_program(struct mga_dev *mdev,
			  unsigned int syspllfo,
			  u32 option_clk,
			  u32 option3_clk)
{
	struct mga_dac_pll_settings syspll;
	unsigned int mclk;
	u32 option, option3;
	int ret;

	if (syspllfo) {
		ret = mga_dac_syspll_calc(mdev, syspllfo, 0, &syspll);
		if (ret)
			return ret;
	}

	mclk = calc_mclk(0, MGA_OPTION3_CLKSEL_PCI);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, MGA_OPTION3_CLKSEL, MGA_OPTION3_CLKSEL_PCI);
	mga_mclk_change_post(mdev, mclk);

	mga_dac_syspll_power(mdev, false);

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_PLLSEL;
	option |= option_clk & MGA_OPTION_PLLSEL;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	if (syspllfo) {
		mga_dac_syspll_power(mdev, true);

		ret = mga_dac_syspll_program(mdev, &syspll);
		if (ret) {
			mga_dac_syspll_power(mdev, false);
			return ret;
		}
	}

	option3 = pci_cfg_read32(&mdev->pdev, MGA_OPTION3);
	option3 &= ~MGA_OPTION3_CLKDIV_CLKDCYC;
	option3 |= option3_clk & MGA_OPTION3_CLKDIV_CLKDCYC;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION3, option3);

	mclk = calc_mclk(syspll.fo, option3_clk);
	mga_mclk_change_pre(mdev, mclk);
	sysclk_select(mdev, MGA_OPTION3_CLKSEL, option3_clk);
	mga_mclk_change_post(mdev, mclk);

	if (syspllfo)
		mdev->sysclk_plls |= MGA_PLL_SYSPLL;
	else
		mdev->sysclk_plls &= ~MGA_PLL_SYSPLL;

	return 0;
}

static void mga_g400_mem_reset(struct mga_dev *mdev);

static void mga_g400_powerup(struct mga_dev *mdev, bool mem_reset)
{
	const struct mga_pins4 *pins = &mdev->pins.pins4;
#if 0
	/* g400 sh, max and marvel */
	u32 pins_option = 0x10404000;
	u32 pins_option3 = 0x0190a419;
	/* g400 sh */
	//int pins_gclk = 80000;
	/* g400 max and marvel */
	int pins_gclk = 82000;
#else
	u32 pins_option = pins->option;
	u32 pins_option3 = pins->clk_vga.option3;
	int pins_gclk = pins->clk_vga.gclk;
#endif
	int gclk, mclk, wclk, syspll = 0;
	int ret;

	if ((pins_option3 & MGA_OPTION3_MCLKSEL) == MGA_OPTION3_MCLKSEL_SYSPLL ||
	    (pins_option3 & MGA_OPTION3_WCLKSEL) == MGA_OPTION3_WCLKSEL_SYSPLL)
		syspll = pins_gclk;

	if ((pins_option3 & MGA_OPTION3_GCLKSEL) == MGA_OPTION3_GCLKSEL_SYSPLL) {
		switch (pins_option3 & MGA_OPTION3_GCLKDIV) {
		case MGA_OPTION3_GCLKDIV_1_3:
			syspll = div_round(pins_gclk * 3, 1);
			break;
		case MGA_OPTION3_GCLKDIV_2_5:
			syspll = div_round(pins_gclk * 5, 2);
			break;
		case MGA_OPTION3_GCLKDIV_4_9:
			syspll = div_round(pins_gclk * 9, 4);
			break;
		case MGA_OPTION3_GCLKDIV_1_2:
			syspll = div_round(pins_gclk * 2, 1);
			break;
		case MGA_OPTION3_GCLKDIV_2_3:
			syspll = div_round(pins_gclk * 3, 2);
			break;
		case MGA_OPTION3_GCLKDIV_1_1:
			syspll = pins_gclk;
			break;
		default:
			BUG();
		}
	}

	switch (pins_option3 & MGA_OPTION3_GCLKSEL) {
	case MGA_OPTION3_GCLKSEL_PCI:
		/* FIXME? */
		gclk = 33333;
		break;
	case MGA_OPTION3_GCLKSEL_SYSPLL:
		gclk = syspll;
		break;
	case MGA_OPTION3_GCLKSEL_MCLK:
		/* FIXME? */
		gclk = 0;
		break;
	case MGA_OPTION3_GCLKSEL_AGPDLL:
		/* FIXME ? */
		gclk = 66667;
		break;
	default:
		BUG();
	}

	switch (pins_option3 & MGA_OPTION3_MCLKSEL) {
	case MGA_OPTION3_MCLKSEL_PCI:
		/* FIXME? */
		mclk = 33333;
		break;
	case MGA_OPTION3_MCLKSEL_SYSPLL:
		mclk = syspll;
		break;
	case MGA_OPTION3_MCLKSEL_MCLK:
		/* FIXME? */
		mclk = 0;
		break;
	case MGA_OPTION3_MCLKSEL_AGPDLL:
		/* FIXME ? */
		mclk = 66667;
		break;
	default:
		BUG();
	}

	switch (pins_option3 & MGA_OPTION3_WCLKSEL) {
	case MGA_OPTION3_WCLKSEL_PCI:
		/* FIXME? */
		wclk = 33333;
		break;
	case MGA_OPTION3_WCLKSEL_SYSPLL:
		wclk = syspll;
		break;
	case MGA_OPTION3_WCLKSEL_MCLK:
		/* FIXME? */
		wclk = 0;
		break;
	case MGA_OPTION3_WCLKSEL_AGPDLL:
		/* FIXME ? */
		wclk = 66667;
		break;
	default:
		BUG();
	}

	if ((pins_option3 & MGA_OPTION3_GCLKSEL) == MGA_OPTION3_GCLKSEL_SYSPLL ||
	    (pins_option3 & MGA_OPTION3_GCLKSEL) == MGA_OPTION3_GCLKSEL_AGPDLL) {
		switch (pins_option3 & MGA_OPTION3_GCLKDIV) {
		case MGA_OPTION3_GCLKDIV_1_3:
			gclk = div_round(gclk * 1, 3);
			break;
		case MGA_OPTION3_GCLKDIV_2_5:
			gclk = div_round(gclk * 2, 5);
			break;
		case MGA_OPTION3_GCLKDIV_4_9:
			gclk = div_round(gclk * 4, 9);
			break;
		case MGA_OPTION3_GCLKDIV_1_2:
			gclk = div_round(gclk * 1, 2);
			break;
		case MGA_OPTION3_GCLKDIV_2_3:
			gclk = div_round(gclk * 2, 3);
			break;
		case MGA_OPTION3_GCLKDIV_1_1:
			break;
		default:
			BUG();
		}
	}

	if ((pins_option3 & MGA_OPTION3_MCLKSEL) == MGA_OPTION3_MCLKSEL_SYSPLL ||
	    (pins_option3 & MGA_OPTION3_MCLKSEL) == MGA_OPTION3_MCLKSEL_AGPDLL) {
		switch (pins_option3 & MGA_OPTION3_MCLKDIV) {
		case MGA_OPTION3_MCLKDIV_1_3:
			mclk = div_round(mclk * 1, 3);
			break;
		case MGA_OPTION3_MCLKDIV_2_5:
			mclk = div_round(mclk * 2, 5);
			break;
		case MGA_OPTION3_MCLKDIV_4_9:
			mclk = div_round(mclk * 4, 9);
			break;
		case MGA_OPTION3_MCLKDIV_1_2:
			mclk = div_round(mclk * 1, 2);
			break;
		case MGA_OPTION3_MCLKDIV_2_3:
			mclk = div_round(mclk * 2, 3);
			break;
		case MGA_OPTION3_MCLKDIV_1_1:
			break;
		default:
			BUG();
		}
	}

	if ((pins_option3 & MGA_OPTION3_WCLKSEL) == MGA_OPTION3_WCLKSEL_SYSPLL ||
	    (pins_option3 & MGA_OPTION3_WCLKSEL) == MGA_OPTION3_WCLKSEL_AGPDLL) {
		switch (pins_option3 & MGA_OPTION3_WCLKDIV) {
		case MGA_OPTION3_WCLKDIV_1_3:
			wclk = div_round(wclk * 1, 3);
			break;
		case MGA_OPTION3_WCLKDIV_2_5:
			wclk = div_round(wclk * 2, 5);
			break;
		case MGA_OPTION3_WCLKDIV_4_9:
			wclk = div_round(wclk * 4, 9);
			break;
		case MGA_OPTION3_WCLKDIV_1_2:
			wclk = div_round(wclk * 1, 2);
			break;
		case MGA_OPTION3_WCLKDIV_2_3:
			wclk = div_round(wclk * 2, 3);
			break;
		case MGA_OPTION3_WCLKDIV_1_1:
			break;
		default:
			BUG();
		}
	}

	dev_dbg(mdev->dev, "SYSPLL = %u kHz\n", syspll);
	dev_dbg(mdev->dev, "  GCLK = %u kHz\n", gclk);
	dev_dbg(mdev->dev, "  MCLK = %u kHz\n", mclk);
	dev_dbg(mdev->dev, "  WCLK = %u kHz\n", wclk);

	mga_dac_init(mdev, pins->fref,
		     pins->pixpll.fvco_max, pins->syspll.fvco_max);

	ret = sysclk_program(mdev, syspll, pins_option, pins_option3);
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		return;
	}

	if (mem_reset) {
		mga_g100_rfhcnt(mdev, 0);//
		udelay(200);//
		mga_g400_mem_reset(mdev);
	}
}

static void mga_g400_mem_reset(struct mga_dev *mdev)
{
	const struct mga_pins4 *pins = &mdev->pins.pins4;
#if 0
	/* g400 sh, max and marvel */
	u32 pins_option = 0x10404000;
	u32 pins_memrdbk = 0x00000108;
	/* g400 sh and marvel */
	u32 pins_mctlwtst = 0x24045491;
	/* g400 max */
	//u32 pins_mctlwtst = 0x20049911;
#else
	u32 pins_option = pins->option;
	u32 pins_memrdbk = pins->memrdbk;
	u32 pins_mctlwtst = pins->clk_vga.mctlwtst;
#endif

	u32 option;
	u32 memrdbk;

	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~(MGA_OPTION_NOHIREQ |
		    MGA_OPTION_ENHMEMACC |
		    MGA_OPTION_HARDPWMSK);
	option |= pins_option & (MGA_OPTION_NOHIREQ |
				 MGA_OPTION_ENHMEMACC |
				 MGA_OPTION_HARDPWMSK);
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	mga_write32(mdev, MGA_PLNWT, 0x00000000);
	mga_write32(mdev, MGA_PLNWT, 0xffffffff);

	/* Step 3. */
	mga_write32(mdev, MGA_MCTLWTST, pins_mctlwtst);

	/* Step 4. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= ~MGA_OPTION_MEMCONFIG;
	option |= pins_option;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* Step 5.*/
	memrdbk = mga_read32(mdev, MGA_MEMRDBK);
	memrdbk &= ~MGA_MEMRDBK_MRSOPCOD;
	mga_write32(mdev, MGA_MEMRDBK, memrdbk);

	/* Step 6.*/
	memrdbk = mga_read32(mdev, MGA_MEMRDBK);
	memrdbk &= ~(MGA_MEMRDBK_MCLKBRD0 |
		     MGA_MEMRDBK_MCLKBRD1 |
		     MGA_MEMRDBK_STRMFCTL);
	memrdbk |= pins_memrdbk;
	mga_write32(mdev, MGA_MEMRDBK, memrdbk);

	/* Step 7. */
	udelay(200);

	/* Step 8. */
	mga_write32(mdev, MGA_MACCESS, MGA_MACCESS_MEMRESET);

	/* Step 9. */
	mga_g100_rfhcnt(mdev, mdev->mclk);
}

static void probe_addons(struct mga_dev *mdev)
{
	const struct mga_pins4 *pins = &mdev->pins.pins4;
	int ret;

	mdev->outputs |= MGA_OUTPUT_DAC1;

	if (pins->features & _MGA_FEAT_TVO_BUILTIN)
		mdev->features |= MGA_FEAT_TVO_BUILTIN;
	if (pins->features & _MGA_FEAT_DVD_BUILTIN)
		mdev->features |= MGA_FEAT_DVD_BUILTIN;
	if (pins->features & _MGA_FEAT_MJPEG_BUILTIN)
		mdev->features |= MGA_FEAT_MJPEG_BUILTIN;
	if (pins->features & _MGA_FEAT_VIN_BUILTIN)
		mdev->features |= MGA_FEAT_VIN_BUILTIN;
	if (pins->features & _MGA_FEAT_TUNER_BUILTIN)
		mdev->features |= MGA_FEAT_TUNER_BUILTIN;
	if (pins->features & _MGA_FEAT_AUDIO_BUILTIN)
		mdev->features |= MGA_FEAT_AUDIO_BUILTIN;
	if (pins->features & _MGA_FEAT_TMDS_BUILTIN)
		mdev->features |= MGA_FEAT_TMDS_BUILTIN;

	dev_dbg(mdev->dev, "Before probe:\n");
	mga_print_features(mdev);

	if (mdev->features & MGA_FEAT_TVO_BUILTIN) {
		if (mdev->features & MGA_FEAT_MJPEG_BUILTIN) {
			ret = mga_tvout_builtin_probe(mdev);
			if (ret)
				dev_warn(mdev->dev, "Failed to locate builtin TV-out\n");
		} else {
			ret = mga_dualhead_builtin_probe(mdev);
			if (ret)
				dev_warn(mdev->dev, "Failed to locate builtin dualhead\n");
		}
	} else {
		ret = mga_dualhead_addon_probe(mdev);
		if (ret)
			dev_dbg(mdev->dev, "Failed to locate dualhead addon\n");
	}

#if 1
	/* FIXME can a flatpanel addon be attached to a Marvel board? */
	if (!(mdev->features & MGA_FEAT_TVO_ADDON)) {
		ret = mga_tmds_addon_probe(mdev);
		if (ret)
			dev_dbg(mdev->dev, "Failed to locate flat panel addon\n");
	}
#endif

	dev_dbg(mdev->dev, "After probe:\n");
	mga_print_features(mdev);
}

static void mga_g400_init(struct mga_dev *mdev)
{
	struct drm_display_mode c1mode = { .vrefresh = 0 };
	struct drm_display_mode c2mode = { .vrefresh = 0 };
	unsigned int c1outputs = 0;
	unsigned int c2outputs = 0;
	bool use_crtc1 = false;
	bool use_crtc2 = false;

	use_crtc1 = true;
	use_crtc2 = true;

	//c1mode = mode_htest;
	//c1mode = mode_vtest;
	//c1mode = mode_640_480_60;
	//c1mode = mode_720_576_50;
	//c1mode = mode_800_600_60;
	c1mode = mode_1024_768_60;

#if 0
	//c1mode.hdisplay = 720;
	//c1mode.vtotal = 625;
	////c1mode.vsync_start = (c1mode.vdisplay + c1mode.vtotal - c1mode.vsync_width) / 2 - 20;
#if 1
	c1mode.hdisplay = 800;
	c1mode.hsync_start = 872;
	c1mode.hsync_width = 88;
	c1mode.htotal = 1128;
	c1mode.vdisplay = 600;
	c1mode.vsync_start = 668;
	c1mode.vsync_width = 4;
	c1mode.vtotal = 780;
#endif
	c1mode.htotal = 1200;
	c1mode.hsync_start = (c1mode.htotal + c1mode.hdisplay) / 2;

	//c1mode.htotal = 1528;
	//c1mode.htotal = 960;
	//c1mode.htotal = 856;
	//c1mode.hdisplay = 640;
	//c1mode.hsync_start = (c1mode.hdisplay + c1mode.htotal) / 2;
	//c1mode.vdisplay = 920;//
	//c1mode.hsync_start = c1mode.hdisplay + 24 + 24 + 32;
	//c1mode.hsync_width = 8;
	//c1mode.htotal = c1mode.hsync_start + c1mode.hsync_width + 40 + 0 + 32;
	//c1mode.vtotal = c1mode.vdisplay + 128;
	//c1mode.hsync_start = (c1mode.hdisplay + c1mode.htotal) / 2 - 24;
	//c1mode.vsync_start = (c1mode.vdisplay + c1mode.vtotal) / 2 - 28;
	//c1mode.hsync_width = 8;
	//c1mode.hdisplay = 512;
	//c1mode = mode_720_576_xx;
	//c1mode = mode_800_600_60;
	//c1mode = mode_1024_768_60;
	//c1mode = mode_1280_1024_60;
	//c1mode = mode_1312_1048_60;
	//c1mode = mode_1400_1050_60;
	//c1mode = mode_1400_1050_60_r;
	//c1mode = mode_1680_1050_60_r;
	//c1mode = mode_1920_1200_60_r;
	//c1mode.flags &= ~DRM_MODE_FLAG_INTERLACE;
#if 0
	if (c1mode.vtotal < 625) {
		c1mode.vsync_start += (625 - c1mode.vtotal) >> 1;
		c1mode.vsync_start++;
		c1mode.vtotal = 625;
	}
#endif
#endif

	//c2mode = mode_1024_768_60;
	//c2mode = mode_720_576_50;
	//c2mode = mode_800_600_60;
	//c2mode = mode_1024_768_60;
	//c2mode = mode_1280_1024_60;
#if 0
	//c2mode.hsync_start = c2mode.hdisplay + 16;
	//c2mode.hsync_width = 8;
	//c2mode.htotal = c2mode.hsync_start + c2mode.hsync_width + 16;
	c2mode.hsync_start = c2mode.hdisplay + 8 + 192;
	c2mode.hsync_width = 8;
	c2mode.htotal = c2mode.hsync_start + c2mode.hsync_width + 8 + 192;
	c2mode.vsync_start = c2mode.vdisplay + 29 + 159;
	c2mode.vsync_width = 3;
	c2mode.vtotal = c2mode.vsync_start + c2mode.vsync_width + 0 + 159;
	c2mode.flags &= ~DRM_MODE_FLAG_INTERLACE;
	if (c2mode.vtotal < 625) {
		c2mode.vsync_start += (625 - c2mode.vtotal) >> 1;
		c2mode.vsync_start++;
		c2mode.vtotal = 625;
	}
#endif

	if (use_crtc1) {
		c1outputs |= MGA_OUTPUT_DAC1;
		//c1outputs |= MGA_OUTPUT_DAC2;
		//c1outputs |= MGA_OUTPUT_TMDS1;
		//c1outputs |= MGA_OUTPUT_TVOUT;

		if (c1outputs & MGA_OUTPUT_TVOUT &&
		    c1mode.vtotal < 625) {
			c1mode.vsync_start += (625 - c1mode.vtotal + 1) >> 1;
			c1mode.vtotal = 625;
		}
	}

	//c2mode = mode_1024_768_60;
	c2mode = mode_800_600_60;

	c2mode.hsync_start = c2mode.hdisplay + 8;
	c2mode.hsync_width = 8;
	c2mode.htotal = c2mode.hsync_start + c2mode.hsync_width + 256;
	c2mode.vsync_start = c2mode.vdisplay + 128;
	c2mode.vsync_width = 1;
	c2mode.vtotal = c2mode.vsync_start + c2mode.vsync_width + 1;

	if (use_crtc2) {
		//c2outputs |= MGA_OUTPUT_DAC1;
		//c2outputs |= MGA_OUTPUT_DAC2;
		//c2outputs |= MGA_OUTPUT_TMDS1;
		c2outputs |= MGA_OUTPUT_TVOUT;

		if (c2outputs & MGA_OUTPUT_TVOUT &&
		    c2mode.vtotal < 625) {
			c2mode.vsync_start += (625 - c2mode.vtotal + 1) >> 1;
			c2mode.vtotal = 625;
		}
	}

	mga_set_initial_crtc1_mode(mdev,
				   mdev->pixel_format,
				   &c1mode,
				   c1outputs);
	mga_set_initial_crtc2_mode(mdev,
				   mdev->pixel_format,
				   &c2mode,
				   c2outputs);

	mga_misc_disable_vga(mdev);

	dev_dbg(mdev->dev, "Disabling CRTC2\n");
	mga_crtc2_wait_vblank(mdev);
	mga_crtc2_video(mdev, false);
	mga_crtc2_pixclk_enable(mdev, false);
	mga_crtc2_pixclk_select(mdev, MGA_PLL_NONE);

	dev_dbg(mdev->dev, "Disabling CRTC1\n");
	mga_crtc1_wait_vblank(mdev);
	mga_crtc1_video(mdev, false);
	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	//mga_dac_init(mdev);

	mga_misc_powerup(mdev);

	mga_dac_powerup(mdev);

	mga_g400_powerup(mdev, true);

	mga_crtc1_powerup(mdev);

	mga_i2c_dac_misc_init(mdev);
	mga_i2c_dac_ddc1_init(mdev);

	probe_addons(mdev);

	mga_probe_mem_size(mdev);
}

static void mga_g400_test(struct mga_dev *mdev)
{
	const char *test = getenv("MGA_TEST");

	mga_enable_outputs(mdev);

	//fixme
#if 0
	if (mdev_outputs[0] & MGA_OUTPUT_DAC1)
		mdev->funcs->monitor_sense_dac1(mdev);
	if (mdev_outputs[0] & MGA_OUTPUT_DAC2 &&
	    mdev->funcs->monitor_sense_dac2)
		mdev->funcs->monitor_sense_dac2(mdev);
#endif

	if (strstr(test, "reg"))
		mga_reg_test(mdev);
	if (strstr(test, "tvo") &&
	    mdev->features & (MGA_FEAT_TVO_BUILTIN | MGA_FEAT_TVO_ADDON))
		mga_tvo_test(&mdev->tvodev);

	mga_disable_outputs(mdev);
}

static int mga_g400_suspend(struct mga_dev *mdev)
{
	u32 option;
	int ret;

	if (mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to power saving mode\n");

	/* slumber */
	mga_disable_outputs(mdev);

	mga_wait_dma_idle(mdev);
	mga_wait_dwgeng_idle(mdev);

	/* Preserve pllsel */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option &= MGA_OPTION_PLLSEL;

	ret = sysclk_program(mdev, 6667,
			     option,
			     MGA_OPTION3_GCLKSEL_SYSPLL | MGA_OPTION3_GCLKDIV_1_3 | (0 << 6) |
			     MGA_OPTION3_MCLKSEL_SYSPLL | MGA_OPTION3_MCLKDIV_2_3 | (0xb << 16) |
			     MGA_OPTION3_WCLKSEL_SYSPLL | MGA_OPTION3_WCLKDIV_1_3 | (0 << 26));
	if (ret) {
		dev_err(mdev->dev, "Failed to program system clock\n");
		goto err;
	}

	mdev->suspended = true;

	dev_dbg(mdev->dev, "In power saving mode\n");

	return 0;

 err:
	mga_enable_outputs(mdev);

	return ret;
}

static int mga_g400_resume(struct mga_dev *mdev)
{
	if (!mdev->suspended)
		return 0;

	dev_dbg(mdev->dev, "Going to normal mode\n");

	mga_g400_powerup(mdev, false);

	mga_enable_outputs(mdev);

	mdev->suspended = false;

	dev_dbg(mdev->dev, "In normal mode\n");

	return 0;
}

static const struct mga_chip_funcs mga_g400_funcs = {
	.rfhcnt = mga_g100_rfhcnt,
	.monitor_sense_dac1 = mga_1064sg_monitor_sense,
	.suspend = mga_g400_suspend,
	.resume = mga_g400_resume,
	.init = mga_g400_init,
	.set_mode = mga_g400_set_mode,
	.test = mga_g400_test,
	.softreset = mga_1064sg_softreset,
};

void mga_g400_prepare(struct mga_dev *mdev)
{
	mdev->funcs = &mga_g400_funcs;
}
