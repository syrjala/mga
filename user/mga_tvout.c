/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "mga_tvout.h"

int mga_tvout_builtin_probe(struct mga_dev *mdev)
{
	int ret;

	switch (mdev->chip) {
	case MGA_CHIP_G200:
	case MGA_CHIP_G400:
		break;
	default:
		return -ENODEV;
	}

	ret = mga_tvo_init(&mdev->tvodev, mdev, 0x1b);
	if (ret) {
		dev_dbg(mdev->dev, "Failed to init TV-out board TVO\n");
		return ret;
	}

	mga_tvo_disable(&mdev->tvodev);

	mdev->features |= MGA_FEAT_TVO_BUILTIN;
	mdev->outputs |= MGA_OUTPUT_TVOUT;

	return 0;
}

int mga_tvout_addon_probe(struct mga_dev *mdev)
{
	int ret;

	switch (mdev->chip) {
	case MGA_CHIP_G100:
	case MGA_CHIP_G200:
		break;
	default:
		return -ENODEV;
	}

	ret = mga_tvo_init(&mdev->tvodev, mdev, 0x1b);
	if (ret) {
		dev_dbg(mdev->dev, "Failed to init TV-out addon TVO\n");
		return ret;
	}

	mga_tvo_disable(&mdev->tvodev);

	mdev->features |= MGA_FEAT_TVO_ADDON;
	mdev->outputs |= MGA_OUTPUT_TVOUT;

	return 0;
}
