/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "mga_dump.h"
#include "tvp3026_regs.h"

static const char *direct_register_names[] = {
	//[TVP_PALWTADD ] = "PALWTADD",
	//[TVP_CURWTADD ] = "CURWTADD",
	//[TVP_PALDATA  ] = "PALDATA",
	  [TVP_PIXRDMSK ] = "PIXRDMSK",
	//[TVP_PALRDADD ] = "PALRDADD",
	//[TVP_CURRDADD ] = "CURRDADD",
	//[TVP_COLWTADD ] = "COLWTADD",
	//[TVP_COLDATA  ] = "COLDATA",
	//[TVP_COLRDADD ] = "COLRDADD",
	  [TVP_CURCTRL  ] = "CURCTRL",
	//[TVP_X_DATAREG] = "X_DATAREG",
	//[TVP_CURDATA  ] = "CURDATA",
	  [TVP_CURPOSXL ] = "CURPOSXL",
	  [TVP_CURPOSXH ] = "CURPOSXH",
	  [TVP_CURPOSYL ] = "CURPOSYL",
	  [TVP_CURPOSYH ] = "CURPOSYH",
};

static const char *indirect_register_names[] = {
	    [TVP_XREVISION      ] = "XREVISION",
	    [TVP_XCURCTRL       ] = "XCURCTRL",
	    [TVP_XLATCHCTRL     ] = "XLATCHCTRL",
	    [TVP_XTRUECOLORCTRL ] = "XTRUECOLORCTRL",
	    [TVP_XMULCTRL       ] = "XMULCTRL",
	    [TVP_XCLKSEL        ] = "XCLKSEL",
	    [TVP_XPALPAGE       ] = "XPALPAGE",
	    [TVP_XGENCTRL       ] = "XGENCTRL",
	    [TVP_XMISCCTRL      ] = "XMISCCTRL",
	    [TVP_XGENIOCTRL     ] = "XGENIOCTRL",
	    [TVP_XGENIODATA     ] = "XGENIODATA",
	//  [TVP_XPLLADD        ] = "XPLLADD",
	//  [TVP_XPCLKPLLDATA   ] = "XPCLKPLLDATA",
	//  [TVP_XMCLKPLLDATA   ] = "XMCLKPLLDATA",
	//  [TVP_XLCLKPLLDATA   ] = "XLCLKPLLDATA",
	    [TVP_XCOLKEYOVRL    ] = "XCOLKEYOVRL",
	    [TVP_XCOLKEYOVRH    ] = "XCOLKEYOVRH",
	    [TVP_XCOLKEYREDL    ] = "XCOLKEYREDL",
	    [TVP_XCOLKEYREDH    ] = "XCOLKEYREDH",
	    [TVP_XCOLKEYGREENL  ] = "XCOLKEYGREENL",
	    [TVP_XCOLKEYGREENH  ] = "XCOLKEYGREENH",
	    [TVP_XCOLKEYBLUEL   ] = "XCOLKEYBLUEL",
	    [TVP_XCOLKEYBLUEH   ] = "XCOLKEYBLUEH",
	    [TVP_XCOLKEYCTRL    ] = "XCOLKEYCTRL",
	    [TVP_XMCLKLCLKCTRL  ] = "XMCLKLCLKCTRL",
	    [TVP_XSENSETEST     ] = "XSENSETEST",
	    [TVP_XTESTDATA      ] = "XTESTDATA",
	    [TVP_XCRCREML       ] = "XCRCREML",
	    [TVP_XCRCREMH       ] = "XCRCREMH",
	//WO[TVP_XCRCBITSEL     ] = "XCRCBITSEL",
	    [TVP_XID            ] = "XID",
	//WO[TVP_XRESET         ] = "XRESET",
};

static void dump_direct(struct mga_dev *mdev)
{
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(direct_register_names); i++) {
		u8 val;

		if (!direct_register_names[i])
			continue;

		val = mga_read8(mdev, 0x3C00 + i);

		dev_dbg(mdev->dev, "%s = %02x\n", direct_register_names[i], val);
	}
}

static void dump_indirect(struct mga_dev *mdev)
{
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(indirect_register_names); i++) {
		u8 val;

		if (!indirect_register_names[i])
			continue;

		mga_write8(mdev, 0x3C00 + TVP_X_INDEXREG, i);
		val = mga_read8(mdev, 0x3C00 + TVP_X_DATAREG);

		dev_dbg(mdev->dev, "%s = %02x\n", indirect_register_names[i], val);
	}
}

static const struct {
	u8 addr;
	const char *name;
} pll_regs[] = {
	{ TVP_XPLLADD_PLLN,    "PLLN"    },
	{ TVP_XPLLADD_PLLM,    "PLLM"    },
	{ TVP_XPLLADD_PLLP,    "PLLP"    },
	{ TVP_XPLLADD_PLLSTAT, "PLLSTAT" },
};

static void dump_pll(struct mga_dev *mdev, const char *name, u8 data_reg)
{
	int i;

	for (i = 0; i < 4; i++) {
		u8 val;

		mga_write8(mdev, 0x3C00 + TVP_X_INDEXREG, TVP_XPLLADD);
		mga_write8(mdev, 0x3C00 + TVP_X_DATAREG, pll_regs[i].addr);

		mga_write8(mdev, 0x3C00 + TVP_X_INDEXREG, data_reg);
		val = mga_read8(mdev, 0x3C00 + TVP_X_DATAREG);

		dev_dbg(mdev->dev, "%s %s = %02x\n", name, pll_regs[i].name, val);
	}
}

static unsigned int pll_calc_fvco(unsigned int fref, int n, int m)
{
	return div_round(8 * fref * (65 - m), 65 - n);
}

static unsigned int pll_calc_fpll(unsigned int fref, int n, int m, int p)
{
	return div_round(8 * fref * (65 - m), (65 - n) << p);
}

static unsigned int decode_pll(struct mga_dev *mdev, const char *name, u8 data_reg)
{
	int i;
	u8 data[4];
	u8 n, m, p;
	unsigned int fref = 14318;
	unsigned int fpll, fvco;

	for (i = 0; i < 4; i++) {
		mga_write8(mdev, 0x3C00 + TVP_X_INDEXREG, TVP_XPLLADD);
		mga_write8(mdev, 0x3C00 + TVP_X_DATAREG, pll_regs[i].addr);

		mga_write8(mdev, 0x3C00 + TVP_X_INDEXREG, data_reg);
		data[i] = mga_read8(mdev, 0x3C00 + TVP_X_DATAREG);
	}

	n = data[0];
	m = data[1];
	p = data[2];

	fvco = pll_calc_fvco(fref, n, m);
	fpll = pll_calc_fpll(fref, n, m, p);

	dev_dbg(mdev->dev, "%s PLL\n", name);
	dev_dbg(mdev->dev, " Fref = %u kHz\n", fref);
	dev_dbg(mdev->dev, " Fvco = %u kHz\n", fvco);
	dev_dbg(mdev->dev, " Fpll = %u kHz\n", fpll);

	return fpll;
}

static unsigned int lclkpll_calc_fvco(unsigned int fdot,
				      u8 n, u8 m, u8 p, u8 q)
{
	return div_round(fdot * (65 - m) * (q + 1) * (1 << (p + 1)), 65 - n);
}

static unsigned int lclkpll_calc_fpll(unsigned int fdot,
				      u8 n, u8 m)
{
	return div_round(fdot * (65 - m), (65 - n));
}

static unsigned int decode_lclkpll(struct mga_dev *mdev, unsigned int fd)
{
	const char *name = "LCLK";
	u8 data_reg = TVP_XLCLKPLLDATA;
	int i;
	u8 data[4];
	u8 n, m, p, q;
	unsigned int fpll, fvco;

	for (i = 0; i < 4; i++) {
		mga_write8(mdev, 0x3C00 + TVP_X_INDEXREG, TVP_XPLLADD);
		mga_write8(mdev, 0x3C00 + TVP_X_DATAREG, pll_regs[i].addr);

		mga_write8(mdev, 0x3C00 + TVP_X_INDEXREG, data_reg);
		data[i] = mga_read8(mdev, 0x3C00 + TVP_X_DATAREG);
	}
	mga_write8(mdev, 0x3C00 + TVP_X_INDEXREG, TVP_XMCLKLCLKCTRL);
	q = mga_read8(mdev, 0x3C00 + TVP_X_DATAREG);

	n = data[0] & 0x3F;
	m = data[1] & 0x3F;
	p = data[2] & 0x03;
	q &= TVP_XMCLKLCLKCTRL_Q;

	fvco = lclkpll_calc_fvco(fd, n, m, p, q);
	fpll = lclkpll_calc_fpll(fd, n, m);

	dev_dbg(mdev->dev, "%s PLL\n", name);
	dev_dbg(mdev->dev, " Fd   = %u kHz\n", fd);
	dev_dbg(mdev->dev, " Fvco = %u kHz\n", fvco);
	dev_dbg(mdev->dev, " Fpll = %u kHz\n", fpll);

	return fpll;
}

void dump_tvp3026(struct mga_dev *mdev)
{
	unsigned int fd;

	if (mdev->chip > MGA_CHIP_2164W)
		return;

	dev_dbg(mdev->dev, "Dump of TVP3026 registers:\n");

	dump_direct(mdev);
	dump_indirect(mdev);
	dump_pll(mdev, "MCLK", TVP_XMCLKPLLDATA);
	dump_pll(mdev, "PCLK", TVP_XPCLKPLLDATA);
	dump_pll(mdev, "LCLK", TVP_XLCLKPLLDATA);

	decode_pll(mdev, "MCLK", TVP_XMCLKPLLDATA);
	fd = decode_pll(mdev, "PCLK", TVP_XPCLKPLLDATA);
	decode_lclkpll(mdev, fd);
}
