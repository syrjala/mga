/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "mga_dump.h"
#include "mga_dac_regs.h"
#include "mga_g450_dac.h"
#include "tvo_g450.h"

#include "mga_cve2_data.h"

static void mga_dac_write8(struct mga_dev *mdev, u8 reg, u8 val)
{
	mga_write8(mdev, MGA_X_INDEXREG, reg);
	mga_write8(mdev, MGA_X_DATAREG, val);
}

static u8 mga_dac_read8(struct mga_dev *mdev, u8 reg)
{
	mga_write8(mdev, MGA_X_INDEXREG, reg);
	return mga_read8(mdev, MGA_X_DATAREG);
}

static void tvo_write8(struct mga_g450_tvo_dev *tdev, u8 reg, u8 val)
{
	struct mga_dev *mdev = tdev->mdev;

	dev_dbg(tdev->dev, "TVO  %02x <- %02x   (%d)\n", reg, val, val);

	mga_dac_write8(mdev, MGA_XTVOINDEX, reg);
	mga_dac_write8(mdev, MGA_XTVODATA, val);
}

static void tvo_write16(struct mga_g450_tvo_dev *tdev, u8 reg, u16 val)
{
	struct mga_dev *mdev = tdev->mdev;

	dev_dbg(tdev->dev, "TVO  %02x <- %04x (%d)\n", reg, val, val);

	mga_dac_write8(mdev, MGA_XTVOINDEX, reg);
	mga_dac_write8(mdev, MGA_XTVODATA, val);

	mga_dac_write8(mdev, MGA_XTVOINDEX, reg + 1);
	mga_dac_write8(mdev, MGA_XTVODATA, val >> 8);
}

static u8 tvo_read8(struct mga_g450_tvo_dev *tdev, u8 reg)
{
	struct mga_dev *mdev = tdev->mdev;
	u8 val;

	mga_dac_write8(mdev, MGA_XTVOINDEX, reg);
	val = mga_dac_read8(mdev, MGA_XTVODATA);

	dev_dbg(tdev->dev, "TVO  %02x = %02x   (%d)\n", reg, val, val);

	return val;
}

static u16 tvo_read16(struct mga_g450_tvo_dev *tdev, u8 reg)
{
	struct mga_dev *mdev = tdev->mdev;
	u16 val = 0;

	mga_dac_write8(mdev, MGA_XTVOINDEX, reg);
	val |= mga_dac_read8(mdev, MGA_XTVODATA);

	mga_dac_write8(mdev, MGA_XTVOINDEX, reg + 1);
	val |= mga_dac_read8(mdev, MGA_XTVODATA) << 8;

	dev_dbg(tdev->dev, "TVO  %02x = %04x (%d)\n", reg, val, val);

	return val;
}

static void cve2_write8(void *data, u8 reg, u8 val)
{
	struct mga_g450_tvo_dev *tdev = data;

	tvo_write8(tdev, reg, val);
}

static void cve2_write16(void *data, u8 reg, u16 val)
{
	struct mga_g450_tvo_dev *tdev = data;

	tvo_write16(tdev, reg, val);
}

static u8 cve2_read8(void *data, u8 reg)
{
	struct mga_g450_tvo_dev *tdev = data;

	return tvo_read8(tdev, reg);
}

static u16 cve2_read16(void *data, u8 reg)
{
	struct mga_g450_tvo_dev *tdev = data;

	return tvo_read16(tdev, reg);
}

static void program_cabletype(struct mga_g450_tvo_dev *tdev)
{
	u8 val = 0x01;

	if (tdev->tv_standard == DRM_TV_STD_NTSC)
		val |= 0x02;

	if (tdev->cable_type == MGA_TVO_CABLE_SCART_RGB)
		val |= 0x40;

	tvo_write8(tdev, 0x80, val);
}

void mga_g450_tvo_program_bt656(struct mga_g450_tvo_dev *tdev,
				const struct mga_tvo_config *config,
				const struct drm_display_mode *mode,
				unsigned int bpp)
{
	(void)mode;
	(void)bpp;

	mga_g450_tvo_set_tv_standard(tdev, config->tv_standard);
	mga_g450_tvo_set_cable_type(tdev, config->cable_type);

	cve2_power(&tdev->cdev, false);

	cve2_program_regs(&tdev->cdev);

	cve2_power(&tdev->cdev, true);

	tvo_write16(tdev, 0x82, tdev->tv_standard == DRM_TV_STD_NTSC ? 0x0014 : 0x0017);

	tvo_write16(tdev, 0x84, 0x0001);

	program_cabletype(tdev);

	program_cabletype(tdev);

	cve2_power(&tdev->cdev, true);

	cve2_program_adjustments(&tdev->cdev);

	tdev->enabled = true;
}

#include <stdio.h>
int mga_g450_tvo_test(struct mga_g450_tvo_dev *tdev)
{
	while (1) {
		char buf[32];
		unsigned int reg, val;
		char len;

		char *s = fgets(buf, sizeof buf, stdin);
		if (!s)
			continue;
		buf[sizeof buf - 1] = 0;

		if (buf[0] == 'q')
			return 1;

		int write = 1;
		if (sscanf(buf, "%c/%x=%d", &len, &reg, &val) != 3) {
			if (sscanf(buf, "%c/%x", &len, &reg) != 2) {
				continue;
			}
			write = 0;
		}

		if (reg < 0x80 || reg > 0x82)
			continue;

		if (len != 'b' && len != 'w')
			continue;

		if (len == 'w') {
			if (write) {
				if (val > 0xffff)
					continue;
				tvo_write16(tdev, reg, val);
			} else {
				val = tvo_read16(tdev, reg);
				dev_dbg(tdev->dev, "%02x = %04x (%d)\n",
					reg, val, val);
			}
		}
		if (len == 'b') {
			if (write) {
				if (val > 0xff)
					continue;
				tvo_write8(tdev, reg, val);
			} else {
				val = tvo_read8(tdev, reg);
				dev_dbg(tdev->dev, "%02x = %02x   (%d)\n",
					reg, val, val);
			}
		}
	}

	return 0;
}

static void dump(struct mga_g450_tvo_dev *tdev)
{
	dev_dbg(tdev->dev, "G450 TVO registers:\n");
	tvo_read8(tdev, 0x80);
	tvo_read16(tdev, 0x82);
	tvo_read16(tdev, 0x84);
}

void mga_g450_tvo_disable(struct mga_g450_tvo_dev *tdev)
{
	cve2_power(&tdev->cdev, false);

	tvo_write8(tdev, 0x80, 0x00);

	tdev->enabled = false;
}

void mga_g450_tvo_enable_bt656(struct mga_g450_tvo_dev *tdev)
{
	program_cabletype(tdev);

	cve2_power(&tdev->cdev, true);

	dump(tdev);
}

int mga_g450_tvo_init(struct mga_g450_tvo_dev *tdev,
		      struct mga_dev *mdev)
{
	switch (mdev->chip) {
	case MGA_CHIP_G450:
	case MGA_CHIP_G550:
		break;
	default:
		BUG();
	}

	cve2_init(&tdev->cdev, mdev->dev,
		  g450_cve2_desktop_adj, g450_cve2_video_adj,
		  g450_cve2_bwlevel, g450_cve2_regs,
		  cve2_write8, cve2_write16,
		  cve2_read8, cve2_read16,
		  tdev);

	tdev->dev = mdev->dev;
	tdev->mdev = mdev;

	/* defaults */
	tdev->enabled = false;
	tdev->cable_type = MGA_TVO_CABLE_COMPOSITE_SVIDEO;
	//tdev->cable_type = MGA_TVO_CABLE_SCART_RGB;

	//dump(tdev);

	mga_g450_tvo_disable(tdev);

	return 0;
}

int mga_g450_tvo_set_tv_standard(struct mga_g450_tvo_dev *tdev,
				 u8 tv_standard)
{
	int ret;

	/* FIXME? */
	if (tdev->enabled)
		return -ENXIO;

	ret = cve2_set_tv_standard(&tdev->cdev, tv_standard);
	if (ret)
		return ret;

	tdev->tv_standard = tv_standard;

	return 0;
}

int mga_g450_tvo_set_dot_crawl_freeze(struct mga_g450_tvo_dev *tdev,
				      bool enable)
{
	return cve2_set_dot_crawl_freeze(&tdev->cdev, enable);
}

int mga_g450_tvo_set_cable_type(struct mga_g450_tvo_dev *tdev,
				enum mga_tvo_cable_type cable_type)
{
	switch (cable_type) {
	case MGA_TVO_CABLE_COMPOSITE_SVIDEO:
	case MGA_TVO_CABLE_SCART_RGB:
		break;
	default:
		return -EINVAL;
	}

	if (tdev->cable_type == cable_type)
		return 0;

	tdev->cable_type = cable_type;

	if (!tdev->enabled)
		return 0;

	program_cabletype(tdev);

	return 0;
}

int mga_g450_tvo_check_config(const struct mga_g450_tvo_dev *tdev,
			      const struct mga_tvo_config *config)
{
	if (config->mode != MGA_TVO_MODE_BT656) {
		dev_dbg(tdev->dev, "Only BT.656 mode supported\n");
		return -EINVAL;
	}

	switch (config->cable_type) {
	case MGA_TVO_CABLE_COMPOSITE_SVIDEO:
	case MGA_TVO_CABLE_SCART_RGB:
		break;
	default:
		dev_dbg(tdev->dev, "Unsupported cable type %u\n", config->cable_type);
		return -EINVAL;
	}

	switch (config->tv_standard) {
	case DRM_TV_STD_PAL:
	case DRM_TV_STD_NTSC:
		break;
	default:
		dev_dbg(tdev->dev, "Unsupported TV standard %u\n", config->tv_standard);
		return -EINVAL;
	}

	if (config->gamma != 10) {
		dev_dbg(tdev->dev, "Gamma not supported\n");
		return -EINVAL;
	}

	if (config->deflicker > 0) {
		dev_dbg(tdev->dev, "Deflicker not supported\n");
		return -EINVAL;
	}

	if (config->text_filter) {
		dev_dbg(tdev->dev, "Text filter not supported\n");
		return -EINVAL;
	}

	if (config->color_bars) {
		dev_dbg(tdev->dev, "Color bars not supported\n");
		return -EINVAL;
	}

	return 0;
}
