/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>

#include "pcf8574.h"
#include "av9110.h"
#include "mga_i2c.h"
#include "mga_flat.h"

/* PCF7574 IO pin mapping. */
enum {
	/* DVI DDC. */
	MGA_DVI_DDC_SCL      = 0x01,
	MGA_DVI_DDC_SDA      = 0x02,

	/* DVI HPD input. */
	MGA_DVI_HPD          = 0x20,

	/* Force SiI1x4 PD#. */
	MGA_SII1X4_PDN_FORCE = 0x10,

	/* DVI HPD controls SiI1x4 PD#. */
	MGA_SII1X4_PDN_HPD   = 0x40,

	/* AV9110 control. Only on G400 flat panel addon. */
	MGA_AV9110_DATA      = 0x04,
	MGA_AV9110_CEN       = 0x08,
	MGA_AV9110_SCLK      = 0x80,
};

static void ddc_setscl(void *data, int state)
{
	struct mga_dev *mdev = data;

	if (state)
		pcf8574_gpio_set(&mdev->edev, MGA_DVI_DDC_SCL, MGA_DVI_DDC_SCL);
	else
		pcf8574_gpio_set(&mdev->edev, MGA_DVI_DDC_SCL, 0);
}

static void ddc_setsda(void *data, int state)
{
	struct mga_dev *mdev = data;

	if (state)
		pcf8574_gpio_set(&mdev->edev, MGA_DVI_DDC_SDA, MGA_DVI_DDC_SDA);
	else
		pcf8574_gpio_set(&mdev->edev, MGA_DVI_DDC_SDA, 0);
}

static int ddc_getscl(void *data)
{
	struct mga_dev *mdev = data;

	return !!(pcf8574_gpio_get(&mdev->edev) & MGA_DVI_DDC_SCL);
}

static int ddc_getsda(void *data)
{
	struct mga_dev *mdev = data;

	return !!(pcf8574_gpio_get(&mdev->edev) & MGA_DVI_DDC_SDA);
}

void mga_tmds_power(struct mga_dev *mdev, bool power)
{
	static const u8 power_vals[] = {
		MGA_SII1X4_PDN_FORCE | MGA_SII1X4_PDN_HPD,
		MGA_SII1X4_PDN_FORCE,
	};

	pcf8574_gpio_set(&mdev->edev,
			 MGA_SII1X4_PDN_HPD | MGA_SII1X4_PDN_FORCE,
			 power_vals[power]);
}

static void clock_setcen(void *data, int state)
{
	struct mga_dev *mdev = data;

	if (state)
		pcf8574_gpio_set(&mdev->edev, MGA_AV9110_CEN, MGA_AV9110_CEN);
	else
		pcf8574_gpio_set(&mdev->edev, MGA_AV9110_CEN, 0);
}

static void clock_setsclk(void *data, int state)
{
	struct mga_dev *mdev = data;

	if (state)
		pcf8574_gpio_set(&mdev->edev, MGA_AV9110_SCLK, MGA_AV9110_SCLK);
	else
		pcf8574_gpio_set(&mdev->edev, MGA_AV9110_SCLK, 0);
}

static void clock_setdata(void *data, int state)
{
	struct mga_dev *mdev = data;

	if (state)
		pcf8574_gpio_set(&mdev->edev, MGA_AV9110_DATA, MGA_AV9110_DATA);
	else
		pcf8574_gpio_set(&mdev->edev, MGA_AV9110_DATA, 0);
}

static int mga_tmds_probe(struct mga_dev *mdev)
{
	u8 addr;
	int ret;

	switch (mdev->chip) {
	case MGA_CHIP_G100:
	case MGA_CHIP_G200:
		addr = 0x22;
		break;
	case MGA_CHIP_G400:
		addr = 0x26;
		break;
	default:
		return -ENODEV;
	}

	ret = pcf8574_init(&mdev->edev, &mdev->misc.adap, addr);
	if (ret < 0) {
		dev_err(mdev->dev, "Failed to init PCF8574\n");
		return ret;
	}

	if (mdev->chip != MGA_CHIP_G400)
		return 0;

	ret = av9110_init(&mdev->adev,
			  mdev->dev,
			  14318,
			  clock_setcen,
			  clock_setsclk,
			  clock_setdata,
			  mdev);
	if (ret) {
		dev_err(mdev->dev, "Failed to init AV9110\n");
		return ret;
	}

	av9110_power_down(&mdev->adev);

	return 0;
}

int mga_tmds_builtin_probe(struct mga_dev *mdev)
{
	int ret;

	if (!(mdev->features & MGA_FEAT_TMDS_BUILTIN))
		return -ENODEV;

	ret = mga_tmds_probe(mdev);
	if (ret)
		return ret;

	mdev->outputs |= MGA_OUTPUT_TMDS1;

	return 0;
}

int mga_tmds_addon_probe(struct mga_dev *mdev)
{
	int ret;

	if (mdev->features & MGA_FEAT_TMDS_BUILTIN)
		return -ENODEV;

	ret = mga_tmds_probe(mdev);
	if (ret)
		return ret;

	ret = mga_i2c_init(mdev, ddc_setsda, ddc_setscl,
			   ddc_getsda, ddc_getscl, &mdev->ddcd);
	if (ret) {
		dev_err(mdev->dev, "Failed to init TMDS DDC\n");
		return ret;
	}

	mdev->features |= MGA_FEAT_TMDS_ADDON;
	mdev->outputs |= MGA_OUTPUT_TMDS1;

	return 0;
}

static int check_av9110(struct mga_dev *mdev)
{
	/* AV9110 only present on the G400 flat panel addon. */
	if (mdev->chip != MGA_CHIP_G400 || !(mdev->features & MGA_FEAT_TMDS_ADDON))
		return -ENODEV;

	/* If MGA-TVO is also present AV9110 can't operate. */
	if (mdev->features & MGA_FEAT_TVO_BUILTIN)
		return -ENXIO;

	return 0;
}

void mga_av9110_pll_power(struct mga_dev *mdev, bool enable)
{
	if (check_av9110(mdev))
		return;

	if (enable)
		return;

	av9110_power_down(&mdev->adev);

	mdev->enabled_plls &= ~MGA_PLL_AV9110;
}

int mga_av9110_pll_calc(struct mga_dev *mdev, unsigned int freq,
			struct mga_pll_settings *pll)
{
	int ret;

	BUG_ON(pll->type != MGA_PLL_AV9110);

	ret = check_av9110(mdev);
	if (ret)
		return ret;

	dev_dbg(mdev->dev, "AV9110 requested frequency %u kHz\n", freq);

	ret = av9110_pll_calc(&mdev->adev, freq, 0, &pll->av9110);
	if (ret < 0) {
		dev_err(mdev->dev, "Failed to calculate AV9110 PLL params\n");
		return ret;
	}

	dev_dbg(mdev->dev, "AV9110 real frequency %u kHz\n", pll->av9110.fclk);

	return 0;
}

int mga_av9110_pll_program(struct mga_dev *mdev,
			   const struct mga_pll_settings *pll)
{
	int ret;

	if (pll->type != MGA_PLL_AV9110)
		return -EINVAL;

	ret = check_av9110(mdev);
	if (ret)
		return ret;

	av9110_pll_program(&mdev->adev, &pll->av9110);

	av9110_pll_wait(&mdev->adev);

	mdev->enabled_plls |= MGA_PLL_AV9110;

	return 0;
}
