/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <errno.h>
#include "drm_kms.h"

int drm_region_width(const struct drm_region *r)
{
	return r->x2 - r->x1;
}

int drm_region_height(const struct drm_region *r)
{
	return r->y2 - r->y1;
}

void drm_region_init(struct drm_region *r,
		     int x, int y, int w, int h)
{
	r->x1 = x;
	r->x2 = x + w;
	r->y1 = y;
	r->y2 = y + h;
}

void drm_region_empty(struct drm_region *r)
{
	drm_region_init(r, 0, 0, 0, 0);
}

bool drm_region_clip(struct drm_region *r,
		     const struct drm_region *clip)
{
	int diff;

	diff = clip->x1 - r->x1;
	if (diff > 0)
		r->x1 += diff;
	diff = clip->y1 - r->y1;
	if (diff > 0)
		r->y1 += diff;
	diff = r->x2 - clip->x2;
	if (diff > 0)
		r->x2 -= diff;
	diff = r->y2 - clip->y2;
	if (diff > 0)
		r->y2 -= diff;

	return drm_region_visible(r);
}

bool drm_region_clip_both(struct drm_region *dst,
			  struct drm_region *src,
			  const struct drm_region *clip)
{
	int diff;

	diff = clip->x1 - dst->x1;
	if (diff > 0) {
		dst->x1 += diff;
		src->x1 += diff;
	}
	diff = clip->y1 - dst->y1;
	if (diff > 0) {
		dst->y1 += diff;
		src->y1 += diff;
	}
	diff = dst->x2 - clip->x2;
	if (diff > 0) {
		dst->x2 -= diff;
		src->x2 -= diff;
	}
	diff = dst->y2 - clip->y2;
	if (diff > 0) {
		dst->y2 -= diff;
		src->y2 -= diff;
	}

	return drm_region_visible(dst);
}

int drm_calc_hscale(const struct drm_region *src,
		    const struct drm_region *dst,
		    int min_hscale, int max_hscale)
{
	int src_w = drm_region_width(src);
	int dst_w = drm_region_width(dst);
	int hscale;

	if (src_w <= 0 || dst_w <= 0)
		return 0;

	hscale = src_w / dst_w;

	if (hscale < min_hscale)
		return -ERANGE;
	if (hscale > max_hscale)
		return -ERANGE;

	return hscale;
}

int drm_calc_vscale(const struct drm_region *src,
		    const struct drm_region *dst,
		    int min_vscale, int max_vscale)
{
	int src_h = drm_region_height(src);
	int dst_h = drm_region_height(dst);
	int vscale;

	if (src_h <= 0 || dst_h <= 0)
		return 0;

	vscale = src_h / dst_h;

	if (vscale < min_vscale)
		return -ERANGE;
	if (vscale > max_vscale)
		return -ERANGE;

	return vscale;
}

bool drm_region_clip_scaled(struct drm_region *dst, struct drm_region *src,
			    const struct drm_region *clip,
			    int hscale, int vscale)
{
	int diff;

	diff = clip->x1 - dst->x1;
	if (diff > 0)
		src->x1 += diff * hscale;
	diff = clip->y1 - dst->y1;
	if (diff > 0)
		src->y1 += diff * vscale;
	diff = dst->x2 - clip->x2;
	if (diff > 0)
		src->x2 -= diff * hscale;
	diff = dst->y2 - clip->y2;
	if (diff > 0)
		src->y2 -= diff * vscale;

	return drm_region_clip(dst, clip);
}

bool drm_region_visible(const struct drm_region *r)
{

	return drm_region_width(r) > 0 && drm_region_height(r) > 0;

}

void drm_region_adjust_size(struct drm_region *r, int x, int y)
{
	r->x1 -= x >> 1;
	r->y1 -= y >> 1;
	r->x2 += (x + 1) >> 1;
	r->y2 += (y + 1) >> 1;
}

void drm_region_translate(struct drm_region *r, int x, int y)
{
	r->x1 += x;
	r->x2 += x;
	r->y1 += y;
	r->y2 += y;
}

void drm_region_rotate(struct drm_region *r,
		       int width, int height,
		       unsigned int rotation)
{
	struct drm_region tmp = *r;

	if (rotation & DRM_REFLECT_X) {
		r->x1 = width - tmp.x2;
		r->x2 = width - tmp.x1;
	}

	if (rotation & DRM_REFLECT_Y) {
		r->y1 = height - tmp.y2;
		r->y2 = height - tmp.y1;
	}

	tmp = *r;

	switch (rotation & 0xf) {
	case DRM_ROTATE_0:
		break;
	case DRM_ROTATE_90:
		r->x1 = tmp.y1;
		r->x2 = tmp.y2;
		r->y1 = width - tmp.x2;
		r->y2 = width - tmp.x1;
		break;
	case DRM_ROTATE_180:
		r->x1 = width - tmp.x2;
		r->x2 = width - tmp.x1;
		r->y1 = height - tmp.y2;
		r->y2 = height - tmp.y1;
		break;
	case DRM_ROTATE_270:
		r->x1 = height - tmp.y2;
		r->x2 = height - tmp.y1;
		r->y1 = tmp.x1;
		r->y2 = tmp.x2;
		break;
	default:
		BUG();
	}
}

void drm_region_rotate_rev(struct drm_region *r,
			   int width, int height,
			   unsigned int rotation)
{
	struct drm_region tmp = *r;

	switch (rotation & 0xf) {
	case DRM_ROTATE_0:
		break;
	case DRM_ROTATE_90:
		r->x1 = width - tmp.y2;
		r->x2 = width - tmp.y1;
		r->y1 = tmp.x1;
		r->y2 = tmp.x2;
		break;
	case DRM_ROTATE_180:
		r->x1 = width - tmp.x2;
		r->x2 = width - tmp.x1;
		r->y1 = height - tmp.y2;
		r->y2 = height - tmp.y1;
		break;
	case DRM_ROTATE_270:
		r->x1 = tmp.y1;
		r->x2 = tmp.y2;
		r->y1 = height - tmp.x2;
		r->y2 = height - tmp.x1;
		break;
	default:
		BUG();
	}

	tmp = *r;

	if (rotation & DRM_REFLECT_X) {
		r->x1 = width - tmp.x2;
		r->x2 = width - tmp.x1;
	}

	if (rotation & DRM_REFLECT_Y) {
		r->y1 = height - tmp.y2;
		r->y2 = height - tmp.y1;
	}
}
