/* SPDX-License-Identifier: MIT */
/*
 * Matrox userspace driver.
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#include <limits.h>
#include <errno.h>

#include "mga_dump.h"
#include "mga_dac.h"
#include "mga_regs.h"
#include "mga_dac_regs.h"

static void mga_dac_write8(struct mga_dev *mdev, u8 reg, u8 val)
{
	mga_write8(mdev, MGA_X_INDEXREG, reg);
	mga_write8(mdev, MGA_X_DATAREG, val);
}

static u8 mga_dac_read8(struct mga_dev *mdev, u8 reg)
{
	mga_write8(mdev, MGA_X_INDEXREG, reg);
	return mga_read8(mdev, MGA_X_DATAREG);
}

enum {
	/* FIXME ? */
	MGA_PLL_TIMEOUT = 10, /* 1 ms typical for G100-G400 */
};

static const struct mga_dac_pll_info pixplla_info = {
	.name = "PIXPLLA",
	.pllm = MGA_XPIXPLLAM,
	.plln = MGA_XPIXPLLAN,
	.pllp = MGA_XPIXPLLAP,
	.pllstat = MGA_XPIXPLLSTAT,
	.pllstat_lock = MGA_XPIXPLLSTAT_PIXLOCK,
};

static const struct mga_dac_pll_info pixpllb_info = {
	.name = "PIXPLLB",
	.pllm = MGA_XPIXPLLBM,
	.plln = MGA_XPIXPLLBN,
	.pllp = MGA_XPIXPLLBP,
	.pllstat = MGA_XPIXPLLSTAT,
	.pllstat_lock = MGA_XPIXPLLSTAT_PIXLOCK,
};

static const struct mga_dac_pll_info pixpllc_info = {
	.name = "PIXPLLC",
	.pllm = MGA_XPIXPLLCM,
	.plln = MGA_XPIXPLLCN,
	.pllp = MGA_XPIXPLLCP,
	.pllstat = MGA_XPIXPLLSTAT,
	.pllstat_lock = MGA_XPIXPLLSTAT_PIXLOCK,
};

static const struct mga_dac_pll_info syspll_info = {
	.name = "SYSPLL",
	.pllm = MGA_XSYSPLLM,
	.plln = MGA_XSYSPLLN,
	.pllp = MGA_XSYSPLLP,
	.pllstat = MGA_XSYSPLLSTAT,
	.pllstat_lock = MGA_XSYSPLLSTAT_SYSLOCK,
};

static unsigned int pll_calc_fvco(unsigned int fref, int n, int m)
{
	return div_round(fref * (n + 1), m + 1);
}

static unsigned int pll_calc_fo(unsigned int fref, int n, int m, int p)
{
	return div_round(fref * (n + 1), (m + 1) * (p + 1));
}

static unsigned int calc_diff(unsigned int a, unsigned int b)
{
	if (a > b)
		return a - b;
	else
		return b - a;
}

static int pll_calc(struct device *dev,
		    const struct mga_dac_pll_info *info,
		    unsigned int freq,
		    unsigned int fmax,
		    struct mga_dac_pll_settings *pll)
{
	unsigned int diff, best_diff = UINT_MAX;
	int m, n, p;

	if (fmax == 0 || fmax > info->fo_max)
		fmax = info->fo_max;

	dev_dbg(dev, "%s M: %d - %d\n", info->name, info->m_min, info->m_max);
	dev_dbg(dev, "%s N: %d - %d\n", info->name, info->n_min, info->n_max);
	dev_dbg(dev, "%s P: %d - %d\n", info->name, 0, 7);

	for (m = info->m_min; m <= info->m_max; m++) {
	for (n = info->n_min; n <= info->n_max; n++) {
	for (p = 1; p <= 8; p <<= 1) {
		u8 pll_p = p - 1;
		unsigned int fvco, fo;

		fvco = pll_calc_fvco(info->fref, n, m);
		if (fvco < info->fvco_min || fvco > info->fvco_max)
			continue;

		fo = pll_calc_fo(info->fref, n, m, pll_p);
		if (fo > fmax)
			continue;

		diff = calc_diff(fo, freq);
		if (diff >= best_diff)
			continue;

		dev_dbg(dev, "%s: found new best %u kHz (NREG=%02x MREG=%02x PREG=%02x)\n",
			info->name, fo, n, m, pll_p);
		best_diff = diff;
		pll->fvco = fvco;
		pll->fo = fo;
		pll->n = n;
		pll->m = m;
		pll->p = pll_p;
	}
	}
	}

	/* FIXME check that best_diff is within some tolerance? */
	if (best_diff == UINT_MAX)
		return -EINVAL;

	return 0;
}

int mga_dac_pll_calc(struct device *dev,
		     const struct mga_dac_pll_info *info,
		     unsigned int freq,
		     unsigned int fmax,
		     struct mga_dac_pll_settings *pll)
{
	int ret;
	u8 s;

	dev_dbg(dev, "%s target Fo = %u kHZ\n", info->name, freq);
	dev_dbg(dev, "%s min Fvco = %u kHZ\n", info->name, info->fvco_min);
	dev_dbg(dev, "%s max Fvco = %u kHZ\n", info->name, info->fvco_max);
	dev_dbg(dev, "%s max Fo = %u kHZ\n", info->name, info->fo_max);

	ret = pll_calc(dev, info, freq, fmax, pll);
	if (ret)
		return ret;

	BUG_ON(pll->n < info->n_min || pll->n > info->n_max ||
	       pll->m < info->m_min || pll->m > info->m_max || pll->p > 7 ||
	       pll->fvco < info->fvco_min || pll->fvco > info->fvco_max ||
	       pll->fo > info->fo_max || (fmax && pll->fo > fmax));

	/* Loop filter bandwith. */
	for (s = 0; s < 3; s++) {
		if (pll->fvco < info->s_limits[s])
			break;
	}
	pll->p |= s << 3;

	dev_dbg(dev, "%s N=%02x M=%02x P=%02x\n",
		info->name, pll->n, pll->m, pll->p);
	dev_dbg(dev, "%s Fvco = %u kHz, Fo = %u kHz\n",
		info->name, pll->fvco, pll->fo);

	return 0;
}

int mga_dac_pixplla_calc(struct mga_dev *mdev,
			 unsigned int freq,
			 unsigned int fmax,
			 struct mga_dac_pll_settings *pll)
{
	return mga_dac_pll_calc(mdev->dev, &mdev->pixplla_info, freq, fmax, pll);
}

int mga_dac_pixpllb_calc(struct mga_dev *mdev,
			 unsigned int freq,
			 unsigned int fmax,
			 struct mga_dac_pll_settings *pll)
{
	return mga_dac_pll_calc(mdev->dev, &mdev->pixpllb_info, freq, fmax, pll);
}

int mga_dac_pixpllc_calc(struct mga_dev *mdev,
			 unsigned int freq,
			 unsigned int fmax,
			 struct mga_dac_pll_settings *pll)
{
	return mga_dac_pll_calc(mdev->dev, &mdev->pixpllc_info, freq, fmax, pll);
}

int mga_dac_syspll_calc(struct mga_dev *mdev,
			unsigned int freq,
			unsigned int fmax,
			struct mga_dac_pll_settings *pll)
{
	return mga_dac_pll_calc(mdev->dev, &mdev->syspll_info, freq, fmax, pll);
}

static int pll_wait(struct mga_dev *mdev,
		    const struct mga_dac_pll_info *info)
{
	unsigned long timeout;
	unsigned int i = 0;
	u8 val;
	u8 lock = info->pllstat_lock;

	mga_write8(mdev, MGA_X_INDEXREG, info->pllstat);

	/* Wait for the PLL to lock. */
	timeout = jiffies + msecs_to_jiffies(MGA_PLL_TIMEOUT);
	val = mga_read8(mdev, MGA_X_DATAREG);
	while (!(val & lock) && time_before(jiffies, timeout)) {
		i++;
		//msleep(1);
		val = mga_read8(mdev, MGA_X_DATAREG);
	}

	timeout = jiffies_to_msecs(jiffies - timeout +
				   msecs_to_jiffies(MGA_PLL_TIMEOUT));

	if (!(val & lock)) {
		dev_err(mdev->dev,
			"%s NOT locked after %lu ms (%u iterations)\n",
			info->name, timeout, i);

		return -ETIMEDOUT;
	}

	dev_dbg(mdev->dev, "%s locked after %lu ms (%u iterations)\n",
		info->name, timeout, i);

	return 0;
}

static void pll_program(struct mga_dev *mdev,
			const struct mga_dac_pll_info *info,
			const struct mga_dac_pll_settings *pll)
{
	dev_dbg(mdev->dev, "Programming %s: N=%02x M=%02x P=%02x\n",
		info->name, pll->n, pll->m, pll->p);

	mga_dac_write8(mdev, info->pllm, pll->m);
	mga_dac_write8(mdev, info->plln, pll->n);
	mga_dac_write8(mdev, info->pllp, pll->p);
}

static void pll_save(struct mga_dev *mdev,
		     const struct mga_dac_pll_info *info,
		     struct mga_dac_pll_settings *pll)
{
	pll->m = mga_dac_read8(mdev, info->pllm);
	pll->n = mga_dac_read8(mdev, info->plln);
	pll->p = mga_dac_read8(mdev, info->pllp);

	pll->fvco = pll_calc_fvco(info->fref, pll->n, pll->m);
	pll->fo = pll_calc_fo(info->fref, pll->n, pll->m, pll->p);

	dev_dbg(mdev->dev, "Saved %s: N=%02x M=%02x P=%02x\n",
		info->name, pll->n, pll->m, pll->p);
}

int mga_dac_pixplla_program(struct mga_dev *mdev,
			    const struct mga_dac_pll_settings *pll)
{
	pll_program(mdev, &mdev->pixplla_info, pll);

	return pll_wait(mdev, &mdev->pixplla_info);
}

void mga_dac_pixplla_save(struct mga_dev *mdev,
			  struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->pixplla_info, pll);
}

int mga_dac_pixpllb_program(struct mga_dev *mdev,
			    const struct mga_dac_pll_settings *pll)
{
	pll_program(mdev, &mdev->pixpllb_info, pll);

	return pll_wait(mdev, &mdev->pixpllb_info);
}

void mga_dac_pixpllb_save(struct mga_dev *mdev,
			  struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->pixpllb_info, pll);
}

int mga_dac_pixpllc_program(struct mga_dev *mdev,
			    const struct mga_dac_pll_settings *pll)
{
	pll_program(mdev, &mdev->pixpllc_info, pll);

	return pll_wait(mdev, &mdev->pixpllc_info);
}

void mga_dac_pixpllc_save(struct mga_dev *mdev,
			  struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->pixpllc_info, pll);
}

int mga_dac_syspll_program(struct mga_dev *mdev,
			   const struct mga_dac_pll_settings *pll)
{
	pll_program(mdev, &mdev->syspll_info, pll);

	return pll_wait(mdev, &mdev->syspll_info);
}

void mga_dac_syspll_save(struct mga_dev *mdev,
			 struct mga_dac_pll_settings *pll)
{
	pll_save(mdev, &mdev->syspll_info, pll);
}

#if 0
static void mga_dac_powerup(struct mga_dev *mdev)
{
	u32 option;
	u8 val;

	/* Step 1. */
	if (0) {
		static const u8 vrefctrl_vals[] = {
			[MGA_CHIP_1064SG] = 0x03,
			[MGA_CHIP_1164SG] = 0x03,
			[MGA_CHIP_G100]   = 0x03,
			[MGA_CHIP_G200]   = 0x03,
			[MGA_CHIP_G200SE] = 0x03,
			[MGA_CHIP_G200EV] = 0x00, /* FIXME? */
			[MGA_CHIP_G200WB] = 0x07,
			[MGA_CHIP_G400]   = 0x00,
			[MGA_CHIP_G450]   = 0x00,
			[MGA_CHIP_G550]   = 0x00,
		};

		mga_write8(mdev, MGA_X_INDEXREG, MGA_XVREFCTRL);
		mga_write8(mdev, MGA_X_DATAREG, vrefctrl_vals[mdev->chip]);

		msleep(100);
	}

	/* Step 2. */
	option = pci_cfg_read32(&mdev->pdev, MGA_OPTION);
	option |= MGA_OPTION_SYSPLLPDN;
	pci_cfg_write32(&mdev->pdev, MGA_OPTION, option);

	/* Step 3. */
	mga_dac_syspll_wait(mdev);

	/* Step 4. */
	val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);
	val |= MGA_XPIXCLKCTRL_PIXPLLPDN;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* Step 5. */
	mga_dac_pixpll_wait(mdev);

	/* Step 6. */
	val = mga_dac_read8(mdev, MGA_XMISCCTRL);
	val |= MGA_XMISCCTRL_RAMCS;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* Step 7. */
	val |= MGA_XMISCCTRL_DACPDN;
	mga_write8(mdev, MGA_X_DATAREG, val);
}
#endif

void mga_dac_dac_power(struct mga_dev *mdev, bool on)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XMISCCTRL);
	if (on)
		val |= MGA_XMISCCTRL_DACPDN;
	else
		val &= ~MGA_XMISCCTRL_DACPDN;
	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_dac_panel_dpms(struct mga_dev *mdev,
			unsigned int mode)
{
	static const u8 pansoff_vals[] = {
		[DRM_MODE_DPMS_ON]      = 0,
		[DRM_MODE_DPMS_STANDBY] = MGA_XPANELMODE_PANHSOFF,
		[DRM_MODE_DPMS_SUSPEND] = MGA_XPANELMODE_PANVSOFF,
		[DRM_MODE_DPMS_OFF]     = MGA_XPANELMODE_PANHSOFF | MGA_XPANELMODE_PANVSOFF,
	};
	u8 val;

	/* G400 panhsoff/panvsoff affects only CRTC1/CRTC2+TMDS */

	val = mga_dac_read8(mdev, MGA_XPANELMODE);

	val &= ~(MGA_XPANELMODE_PANHSOFF | MGA_XPANELMODE_PANVSOFF);
	val |= pansoff_vals[mode];

	mga_write8(mdev, MGA_X_DATAREG, val);
}

/*
 * G400/G450/G550: CRTC1/CRTC2 -> PANEL
 */
void mga_dac_panel_set_sync(struct mga_dev *mdev,
			    unsigned int sync)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPANELMODE);

	if (sync & DRM_MODE_FLAG_NHSYNC)
		val |= MGA_XPANELMODE_PANHSPOL;
	else
		val &= !MGA_XPANELMODE_PANHSPOL;

	if (sync & DRM_MODE_FLAG_NVSYNC)
		val |= MGA_XPANELMODE_PANVSPOL;
	else
		val &= ~MGA_XPANELMODE_PANVSPOL;

	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_dac_tvo_source(struct mga_dev *mdev, unsigned int crtc)
{
	static const u8 vdoutsel_vals[] = {
		[MGA_CRTC_NONE] = 0,
		[MGA_CRTC_CRTC1] = MGA_XMISCCTRL_VDOUTSEL_MAFC12,
		[MGA_CRTC_CRTC2] = MGA_XMISCCTRL_VDOUTSEL_CRTC2RGB,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XMISCCTRL);

	val &= ~(MGA_XMISCCTRL_MFCSEL | MGA_XMISCCTRL_VDOUTSEL);
	val |= MGA_XMISCCTRL_MFCSEL_MAFC | vdoutsel_vals[crtc];

	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_dac_bt656_source(struct mga_dev *mdev, unsigned int crtc)
{
	static const u8 vdoutsel_vals[] = {
		[MGA_CRTC_NONE] = 0,
		[MGA_CRTC_CRTC1] = 0,
		[MGA_CRTC_CRTC2] = MGA_XMISCCTRL_VDOUTSEL_CRTC2656,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XMISCCTRL);

	val &= ~(MGA_XMISCCTRL_MFCSEL | MGA_XMISCCTRL_VDOUTSEL);
	val |= MGA_XMISCCTRL_MFCSEL_MAFC | vdoutsel_vals[crtc];

	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_dac_panel_slave(struct mga_dev *mdev, bool enable)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPANELMODE);

	if (enable)
		val |= MGA_XPANELMODE_PANELSEL;
	else
		val &= ~MGA_XPANELMODE_PANELSEL;

	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_dac_panel_source(struct mga_dev *mdev, unsigned int crtc)
{
	static const u8 vdoutsel_vals[] = {
		[MGA_CRTC_NONE] = 0,
		[MGA_CRTC_CRTC1] = MGA_XMISCCTRL_VDOUTSEL_MAFC12,
		[MGA_CRTC_CRTC2] = MGA_XMISCCTRL_VDOUTSEL_CRTC2RGB,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XMISCCTRL);

	val &= ~(MGA_XMISCCTRL_MFCSEL | MGA_XMISCCTRL_VDOUTSEL);
	val |= MGA_XMISCCTRL_MFCSEL_PANELLINK | vdoutsel_vals[crtc];

	mga_write8(mdev, MGA_X_DATAREG, val);
}

static void mga_dac_pll_info_init(struct mga_dev *mdev,
				  struct mga_dac_pll_info *info,
				  unsigned int fref,
				  unsigned int fvco_max)
{
	info->fref = fref;
	info->fvco_min = 50000;
	info->fvco_max = fvco_max;
	// FIXME ?
	info->fo_max = fvco_max;

	switch (mdev->chip) {
	case MGA_CHIP_1064SG:
	case MGA_CHIP_1164SG:
		info->n_min = 100;
		info->n_max = 127;
		info->m_min = 1;
		info->m_max = 31;
		break;
	case MGA_CHIP_G100:
	case MGA_CHIP_G200:
		info->n_min = 7;
		info->n_max = 127;
		info->m_min = 1;
		info->m_max = 6;
		break;
#if 0
	case MGA_CHIP_G200SE:
		//info->fref = 25000;
		info->fvco_min = 160000;
		info->fvco_max = 320000;
		info->n_min = 16;
		info->n_max = 255;
		info->m_min = 0;
		info->m_max = 31;
		break;
	case MGA_CHIP_G200EV:
		//info->fref = 50000;
		info->fvco_min = 150000;
		info->fvco_max = 550000;
		info->n_min = 16;
		info->n_max = 255;
		info->m_min = 0;
		info->m_max = 15;
		/* supposedly all values 1-16 ok */
		info->p_min = 1;
		info->p_max = 16;
		break;
	case MGA_CHIP_G200WB://FIXME?
#endif
	case MGA_CHIP_G400:
		info->n_min = 1;
		info->n_max = 127;
		info->m_min = 1;
		info->m_max = 31;
		break;
	}

	switch (mdev->chip) {
	case MGA_CHIP_G400:
		info->s_limits[0] = 110000;
		info->s_limits[1] = 170000;
		info->s_limits[2] = 240000;
		break;
	default:
		info->s_limits[0] = 100000;
		info->s_limits[1] = 140000;
		info->s_limits[2] = 180000;
		break;
	}
}

void mga_dac_init(struct mga_dev *mdev,
		  unsigned int fref,
		  unsigned int pixpll_fvco_max,
		  unsigned int syspll_fvco_max)
{
	mdev->pixplla_info = pixplla_info;
	mdev->pixpllb_info = pixpllb_info;
	mdev->pixpllc_info = pixpllc_info;
	mdev->syspll_info = syspll_info;

	mga_dac_pll_info_init(mdev, &mdev->pixplla_info,
			      fref, pixpll_fvco_max);
	mga_dac_pll_info_init(mdev, &mdev->pixpllb_info,
			      fref, pixpll_fvco_max);
	mga_dac_pll_info_init(mdev, &mdev->pixpllc_info,
			      fref, pixpll_fvco_max);
	mga_dac_pll_info_init(mdev, &mdev->syspll_info,
			      fref, syspll_fvco_max);

	mga_dac_pll_info_print(mdev, &mdev->pixplla_info);
	mga_dac_pll_info_print(mdev, &mdev->pixpllb_info);
	mga_dac_pll_info_print(mdev, &mdev->pixpllc_info);
	mga_dac_pll_info_print(mdev, &mdev->syspll_info);
}

static void mga_dac_defaults(struct mga_dev *mdev, struct mga_dac_regs *regs)
{
	memset(regs, 0, sizeof *regs);

	/* Not in PInS? Values from BIOS initialized cards. */
	switch (mdev->chip) {
		/* OK, checked from BIOS inited card */
	case MGA_CHIP_G100:
		regs->indirect[MGA_XVREFCTRL] = mdev->pins.pins3.vrefctrl;
		break;
		/* Re-check these from BIOS inited card */
	case MGA_CHIP_1064SG:
	case MGA_CHIP_1164SG:
	case MGA_CHIP_G200:
	case MGA_CHIP_G200SE:
		regs->indirect[MGA_XVREFCTRL] = MGA_XVREFCTRL_SYSPLLBGPDN |
						MGA_XVREFCTRL_SYSPLLBGEN;
		break;
	case MGA_CHIP_G200WB:
		regs->indirect[MGA_XVREFCTRL] = MGA_XVREFCTRL_SYSPLLBGPDN |
						MGA_XVREFCTRL_SYSPLLBGEN |
						MGA_XVREFCTRL_PIXPLLBGPDN;
		break;
	default:
		/* FIXME G200EV? */
		regs->indirect[MGA_XVREFCTRL] = 0;
		break;
	}

	/* Disable pixclk, PCI clock as pixclk, power down PIXPLL */
	regs->indirect[MGA_XPIXCLKCTRL] = MGA_XPIXCLKCTRL_PIXCLKSL_PCI | MGA_XPIXCLKCTRL_PIXCLKDIS;

	/* Power down DAC and LUT. */
	/* Note: pixclk must be disabled before touching RAMCS. */
	regs->indirect[MGA_XMISCCTRL] = MGA_XMISCCTRL_VGA8DAC | MGA_XMISCCTRL_MFCSEL_DISABLE | MGA_XMISCCTRL_VDOUTSEL_MAFC12;

	/* Not in PInS? Value from BIOS initialized card. */
	if (mdev->chip == MGA_CHIP_G400)
		regs->indirect[MGA_XMAFCDEL] = 0x04;

	if (mdev->chip >= MGA_CHIP_G400)
		regs->indirect[MGA_XPANELMODE] = MGA_XPANELMODE_PANHSOFF | MGA_XPANELMODE_PANVSOFF;

	regs->indirect[MGA_XMULCTRL] = MGA_XMULCTRL_DEPTH_BPP8;

	regs->indirect[MGA_XGENCTRL] = MGA_XGENCTRL_IOGSYNCDIS;

	if (mdev->chip >= MGA_CHIP_G200) {
		regs->indirect[MGA_XCOLMSK0RED] = 0xff;
		regs->indirect[MGA_XCOLMSK0BLUE] = 0xff;
		regs->indirect[MGA_XCOLMSK0GREEN] = 0xff;
	}

	regs->pixrdmsk = 0xFF;
}

void mga_dac_powerup(struct mga_dev *mdev)
{
	u8 val;
	struct mga_dac_regs regs;

	mga_dac_defaults(mdev, &regs);

	mga_dac_write8(mdev, MGA_XVREFCTRL, regs.indirect[MGA_XVREFCTRL]);
	msleep(100);

	/* Disable pixclk, PCI clock as pixclk, power down PIXPLL */
	mga_dac_write8(mdev, MGA_XPIXCLKCTRL, regs.indirect[MGA_XPIXCLKCTRL]);

	/* Power down DAC and LUT. */
	/* Note: pixclk must be disabled before touching RAMCS. */
	mga_dac_write8(mdev, MGA_XMISCCTRL, regs.indirect[MGA_XMISCCTRL]);

	/* Not in PInS? Value from BIOS initialized card. */
	if (mdev->chip == MGA_CHIP_G400)
		mga_dac_write8(mdev, MGA_XMAFCDEL, regs.indirect[MGA_XMAFCDEL]);

	if (mdev->chip >= MGA_CHIP_G400)
		mga_dac_write8(mdev, MGA_XPANELMODE, regs.indirect[MGA_XPANELMODE]);

	mga_dac_write8(mdev, MGA_XMULCTRL, regs.indirect[MGA_XMULCTRL]);

	/* Power down the sense comparator. */
	mga_dac_write8(mdev, MGA_XSENSETEST, regs.indirect[MGA_XSENSETEST]);

	mga_dac_write8(mdev, MGA_XGENCTRL, regs.indirect[MGA_XGENCTRL]);

	mga_dac_write8(mdev, MGA_XCURCTRL, regs.indirect[MGA_XCURCTRL]);

	mga_dac_write8(mdev, MGA_XZOOMCTRL, regs.indirect[MGA_XZOOMCTRL]);

	if (mdev->chip >= MGA_CHIP_G200) {
		mga_dac_write8(mdev, MGA_XCOLMSK, regs.indirect[MGA_XCOLMSK]);

		mga_dac_write8(mdev, MGA_XCOLKEY, regs.indirect[MGA_XCOLKEY]);

		mga_dac_write8(mdev, MGA_XKEYOPMODE, regs.indirect[MGA_XKEYOPMODE]);

		mga_dac_write8(mdev, MGA_XCOLMSK0RED, regs.indirect[MGA_XCOLMSK0RED]);
		mga_dac_write8(mdev, MGA_XCOLMSK0GREEN, regs.indirect[MGA_XCOLMSK0BLUE]);
		mga_dac_write8(mdev, MGA_XCOLMSK0BLUE, regs.indirect[MGA_XCOLMSK0GREEN]);

		mga_dac_write8(mdev, MGA_XCOLKEY0RED, regs.indirect[MGA_XCOLKEY0RED]);
		mga_dac_write8(mdev, MGA_XCOLKEY0GREEN, regs.indirect[MGA_XCOLKEY0BLUE]);
		mga_dac_write8(mdev, MGA_XCOLKEY0BLUE, regs.indirect[MGA_XCOLKEY0GREEN]);
	} else {
		mga_dac_write8(mdev, MGA_XCOLKEYMSKL, regs.indirect[MGA_XCOLKEYMSKL]);
		mga_dac_write8(mdev, MGA_XCOLKEYMSKH, regs.indirect[MGA_XCOLKEYMSKH]);

		mga_dac_write8(mdev, MGA_XCOLKEYL, regs.indirect[MGA_XCOLKEYL]);
		mga_dac_write8(mdev, MGA_XCOLKEYH, regs.indirect[MGA_XCOLKEYH]);
	}

	mga_dac_write8(mdev, MGA_XGENIOCTRL, regs.indirect[MGA_XGENIOCTRL]);
	mga_dac_write8(mdev, MGA_XGENIODATA, regs.indirect[MGA_XGENIODATA]);

	mga_write8(mdev, MGA_PIXRDMSK, regs.pixrdmsk);
}

//	mga_dac_syspll_save(mdev, );
//	mga_dac_pixplla_save(mdev, );
//	mga_dac_pixpllb_save(mdev, );
//	mga_dac_pixpllc_save(mdev, );
//	mga_dac_vidpll_save(mdev, );

static void mga_dac_save_indirect(struct mga_dev *mdev, struct mga_dac_regs *regs)
{
	regs->indirect[MGA_XCURADDL] = mga_dac_read8(mdev, MGA_XCURADDL);
	regs->indirect[MGA_XCURADDH] = mga_dac_read8(mdev, MGA_XCURADDH);
	regs->indirect[MGA_XCURCTRL] = mga_dac_read8(mdev, MGA_XCURCTRL);

	regs->indirect[MGA_XVREFCTRL] = mga_dac_read8(mdev, MGA_XVREFCTRL);
	regs->indirect[MGA_XMULCTRL] = mga_dac_read8(mdev, MGA_XMULCTRL);
	regs->indirect[MGA_XPIXCLKCTRL] = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);
	regs->indirect[MGA_XGENCTRL] = mga_dac_read8(mdev, MGA_XGENCTRL);
	regs->indirect[MGA_XMISCCTRL] = mga_dac_read8(mdev, MGA_XMISCCTRL);

	regs->indirect[MGA_XGENIOCTRL] = mga_dac_read8(mdev, MGA_XGENIOCTRL);
	regs->indirect[MGA_XGENIODATA] = mga_dac_read8(mdev, MGA_XGENIODATA);

	regs->indirect[MGA_XZOOMCTRL] = mga_dac_read8(mdev, MGA_XZOOMCTRL);
	regs->indirect[MGA_XSENSETEST] = mga_dac_read8(mdev, MGA_XSENSETEST);
	regs->indirect[MGA_XCRCREML] = mga_dac_read8(mdev, MGA_XCRCREML);
	regs->indirect[MGA_XCRCREMH] = mga_dac_read8(mdev, MGA_XCRCREMH);
	regs->indirect[MGA_XCRCBITSEL] = mga_dac_read8(mdev, MGA_XCRCBITSEL);

	if (mdev->chip == MGA_CHIP_1064SG || mdev->chip == MGA_CHIP_1164SG || mdev->chip == MGA_CHIP_G100) {
		regs->indirect[MGA_XCOLKEYMSKL] = mga_dac_read8(mdev, MGA_XCOLKEYMSKL);
		regs->indirect[MGA_XCOLKEYMSKH] = mga_dac_read8(mdev, MGA_XCOLKEYMSKH);
		regs->indirect[MGA_XCOLKEYL] = mga_dac_read8(mdev, MGA_XCOLKEYL);
		regs->indirect[MGA_XCOLKEYH] = mga_dac_read8(mdev, MGA_XCOLKEYH);
	}

	if (mdev->chip >= MGA_CHIP_G200) {
		regs->indirect[MGA_XCOLMSK] = mga_dac_read8(mdev, MGA_XCOLMSK);
		regs->indirect[MGA_XCOLKEY] = mga_dac_read8(mdev, MGA_XCOLKEY);
		regs->indirect[MGA_XKEYOPMODE] = mga_dac_read8(mdev, MGA_XKEYOPMODE);
		regs->indirect[MGA_XCOLMSK0RED] = mga_dac_read8(mdev, MGA_XCOLMSK0RED);
		regs->indirect[MGA_XCOLMSK0GREEN] = mga_dac_read8(mdev, MGA_XCOLMSK0GREEN);
		regs->indirect[MGA_XCOLMSK0BLUE] = mga_dac_read8(mdev, MGA_XCOLMSK0BLUE);
		regs->indirect[MGA_XCOLKEY0RED] = mga_dac_read8(mdev, MGA_XCOLKEY0RED);
		regs->indirect[MGA_XCOLKEY0GREEN] = mga_dac_read8(mdev, MGA_XCOLKEY0GREEN);
		regs->indirect[MGA_XCOLKEY0BLUE] = mga_dac_read8(mdev, MGA_XCOLKEY0BLUE);
	}

	if (mdev->chip >= MGA_CHIP_G400) {
		regs->indirect[MGA_XPANELMODE] = mga_dac_read8(mdev, MGA_XPANELMODE);
		regs->indirect[MGA_XMAFCDEL] = mga_dac_read8(mdev, MGA_XMAFCDEL);
	}

	if (mdev->chip >= MGA_CHIP_G450) {
		regs->indirect[MGA_XDVIPIPECTRL] = mga_dac_read8(mdev, MGA_XDVIPIPECTRL);
		regs->indirect[MGA_XDVICLKCTRL] = mga_dac_read8(mdev, MGA_XDVICLKCTRL);
		regs->indirect[MGA_XTVOINDEX] = mga_dac_read8(mdev, MGA_XTVOINDEX);
		regs->indirect[MGA_XTVODATA] = mga_dac_read8(mdev, MGA_XTVODATA);
		regs->indirect[MGA_XDISPCTRL] = mga_dac_read8(mdev, MGA_XDISPCTRL);
		regs->indirect[MGA_XSYNCCTRL] = mga_dac_read8(mdev, MGA_XSYNCCTRL);
		regs->indirect[MGA_XPWRCTRL] = mga_dac_read8(mdev, MGA_XPWRCTRL);
		regs->indirect[MGA_XPANCTRL] = mga_dac_read8(mdev, MGA_XPANCTRL);
	}

#if 0
	if (mdev->chip == MGA_CHIP_G200WB) {
		regs->indirect[MGA_XSPAREREG] = mga_dac_read8(mdev, MGA_XSPAREREG); /* G200WB */
		regs->indirect[MGA_XREMHEADCTRL] = mga_dac_read8(mdev, MGA_XREMHEADCTRL); /* G200WB */
		regs->indirect[MGA_XREMHEADCTRL2] = mga_dac_read8(mdev, MGA_XREMHEADCTRL2); /* G200WB */
		regs->indirect[MGA_XPIXPLLCN_WB] = mga_dac_read8(mdev, MGA_XPIXPLLCN_WB); /* G200WB */
		regs->indirect[MGA_XPIXPLLCM_WB] = mga_dac_read8(mdev, MGA_XPIXPLLCM_WB); /* G200WB */
		regs->indirect[MGA_XPIXPLLCP_WB] = mga_dac_read8(mdev, MGA_XPIXPLLCP_WB); /* G200WB */
		regs->indirect[MGA_XPIXPLLCM_EV] = mga_dac_read8(mdev, MGA_XPIXPLLCM_EV); /* G200EV */
		regs->indirect[MGA_XPIXPLLCN_EV] = mga_dac_read8(mdev, MGA_XPIXPLLCN_EV); /* G200EV */
		regs->indirect[MGA_XPIXPLLCP_EV] = mga_dac_read8(mdev, MGA_XPIXPLLCP_EV); /* G200EV */
	}
#endif
}

static unsigned int bpp_to_index(unsigned int bpp)
{
	switch (bpp) {
	default:
		BUG();
	case 8:
		return 0;
	case 15:
		return 1;
	case 16:
		return 2;
	case 24:
		return 3;
	case 32:
		return 4;
	}
}

static void setup_colorkey(struct mga_dev *mdev,
			   struct mga_dac_config *config)
{
	u8 colkeyl = 0;
	u8 colkeyh = 0;
	u8 colkeymskl = 0;
	u8 colkeymskh = 0;
	u8 val;

	config->gfx_dst_ckey = 0xffff; /* used in as dst ckey for split mode video */
	config->ovl_src_ckey = 0xff; /* used as src ckey for pseudocolor ovl */
	config->enable_video = false;
	config->enable_overlay = false;
	config->video_alpha = false;
	config->video_palette = false;
	config->video_polarity = false;

	mga_write8(mdev, MGA_X_INDEXREG, MGA_XMULCTRL);
	switch (mga_read8(mdev, MGA_X_DATAREG) & MGA_XMULCTRL_DEPTH) {
	case MGA_XMULCTRL_DEPTH_BPP8:
		config->enable_video = false;
		config->video_palette = false;
		config->video_alpha = false;
		config->enable_overlay = false;
		break;
	case MGA_XMULCTRL_DEPTH_BPP15:
		config->enable_video = false;
		config->video_palette = false;
		config->video_alpha = config->enable_overlay;
		break;
	case MGA_XMULCTRL_DEPTH_BPP16:
		config->enable_video = false;
		config->video_palette = false;
		config->video_alpha = false;
		config->enable_overlay = false;
		break;
	case MGA_XMULCTRL_DEPTH_BPP24:
		config->enable_video = false;
		config->video_palette = false;
		config->video_alpha = false;
		config->enable_overlay = false;
		break;
	case MGA_XMULCTRL_DEPTH_BPP32DIR:
		config->enable_video = false;
		config->video_palette = false;
		if (!config->enable_overlay)
			break;
		colkeyl = config->ovl_src_ckey;
		colkeymskl = 0xff;
		break;
	case MGA_XMULCTRL_DEPTH_2G8V16:
		config->video_palette = false;
		if (!config->enable_video) {
			config->video_alpha = false;
			break;
		}
		colkeyl = config->gfx_dst_ckey;
		colkeymskl = 0xff;
		break;
	case MGA_XMULCTRL_DEPTH_G16V16:
		if (!config->enable_video) {
			config->video_alpha = false;
			break;
		}
		colkeyl = config->gfx_dst_ckey;
		colkeyh = config->gfx_dst_ckey >> 8;
		colkeymskl = 0xff;
		colkeymskh = 0xff;
		break;
	case MGA_XMULCTRL_DEPTH_BPP32PAL:
		config->enable_video = false;
		config->enable_overlay = false;
		config->video_alpha = false;
		config->video_palette = false;
		break;
	default:
		BUG();
	};

	//if (!config->enable_video)
	//config->video_polarity = false;

	val = mga_dac_read8(mdev, MGA_XMULCTRL);
	if (config->video_palette)
		val |= MGA_XMULCTRL_VIDEOPAL;
	else
		val &= ~MGA_XMULCTRL_VIDEOPAL;
	mga_write8(mdev, MGA_X_DATAREG, val);

	val = mga_dac_read8(mdev, MGA_XGENCTRL);
	if (config->video_alpha)
		val |= MGA_XGENCTRL_ALPHAEN;
	else
		val &= ~MGA_XGENCTRL_ALPHAEN;
	if (config->video_polarity)
		val |= MGA_XGENCTRL_VS;
	else
		val &= ~MGA_XGENCTRL_VS;
	mga_write8(mdev, MGA_X_DATAREG, val);

	mga_dac_write8(mdev, MGA_XCOLKEYMSKL, colkeymskl);
	mga_dac_write8(mdev, MGA_XCOLKEYMSKH, colkeymskh);

	mga_dac_write8(mdev, MGA_XCOLKEYL, colkeyl);
	mga_dac_write8(mdev, MGA_XCOLKEYH, colkeyh);
}

void mga_dac_crtc1_set_palette(struct mga_dev *mdev)
{
	const struct drm_color *pal = mdev->palette;
	int i;

	if (!(mga_dac_read8(mdev, MGA_XMISCCTRL) & MGA_XMISCCTRL_RAMCS))
		return;

	mga_write8(mdev, MGA_PALWTADD, 0x00);

	for (i = 0; i < 256; i++) {
		mga_write8(mdev, MGA_PALDATA, pal[i].r);
		mga_write8(mdev, MGA_PALDATA, pal[i].g);
		mga_write8(mdev, MGA_PALDATA, pal[i].b);
	}
}

void mga_dac_crtc1_load_palette(struct mga_dev *mdev,
				const struct drm_color pal[256])
{
	int i;

	for (i = 0; i < 256; i++)
		mdev->palette[i] = pal[i];

	mga_dac_crtc1_set_palette(mdev);
}

void mga_dac_crtc1_power(struct mga_dev *mdev, bool enable)
{
	/* pixclk must be disabled before touching ramcs! */
	u8 val;

	val = mga_dac_read8(mdev, MGA_XMISCCTRL);

	if (enable)
		val |= MGA_XMISCCTRL_RAMCS;
	else
		val &= ~MGA_XMISCCTRL_RAMCS;

	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_dac_crtc1_program_format(struct mga_dev *mdev,
				  const struct mga_dac_config *config)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XMULCTRL);
	val &= ~MGA_XMULCTRL_DEPTH;
	switch (config->pixel_format) {
	case DRM_FORMAT_C8:
		val |= MGA_XMULCTRL_DEPTH_BPP8;
		break;
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
		val |= MGA_XMULCTRL_DEPTH_BPP15;
		break;
	case DRM_FORMAT_RGB565:
		val |= MGA_XMULCTRL_DEPTH_BPP16;
		break;
	case DRM_FORMAT_RGB888:
		val |= MGA_XMULCTRL_DEPTH_BPP24;
		break;
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
		if (config->enable_overlay)
			val |= MGA_XMULCTRL_DEPTH_BPP32DIR;
		else
			val |= MGA_XMULCTRL_DEPTH_BPP32PAL;
		break;
	}
	mga_write8(mdev, MGA_X_DATAREG, val);

	val = mga_dac_read8(mdev, MGA_XGENCTRL);
	if (config->enable_overlay &&
	    (config->pixel_format == DRM_FORMAT_ARGB1555 ||
	     config->pixel_format == DRM_FORMAT_XRGB1555))
		val |= MGA_XGENCTRL_ALPHAEN;
	else
		val &= ~MGA_XGENCTRL_ALPHAEN;
	mga_write8(mdev, MGA_X_DATAREG, val);

	/* FIXME overlay stuff may need programming too */
}

void mga_dac_crtc1_program(struct mga_dev *mdev,
			   const struct mga_dac_config *config)
{
	u8 val;

	mga_dac_crtc1_program_format(mdev, config);

	val = mga_dac_read8(mdev, MGA_XZOOMCTRL);
	val &= ~MGA_XZOOMCTRL_HZOOM;
	val |= (config->hzoom - 1) & MGA_XZOOMCTRL_HZOOM;
	mga_write8(mdev, MGA_X_DATAREG, val);

	setup_colorkey(mdev, (struct mga_dac_config *)config);
}

void mga_dac_crtc1_cursor_position(struct mga_dev *mdev,
				   unsigned int x, unsigned int y)
{
	x = min(x, 0xFFF);
	y = min(y, 0xFFF);

	mga_write32(mdev, MGA_CURPOS, (y << 16) | x);
}

#define MGA_READ_CURCOL(mdev, col, n)					\
do {									\
	(col)[n].r = mga_dac_read8((mdev), MGA_XCURCOL##n##RED);	\
	(col)[n].g = mga_dac_read8((mdev), MGA_XCURCOL##n##GREEN);	\
	(col)[n].b = mga_dac_read8((mdev), MGA_XCURCOL##n##BLUE);	\
} while (0)

#define MGA_WRITE_CURCOL(mdev, col, n)					\
do {									\
	mga_dac_write8((mdev), MGA_XCURCOL##n##RED, (col)[n].r);	\
	mga_dac_write8((mdev), MGA_XCURCOL##n##GREEN, (col)[n].g);	\
	mga_dac_write8((mdev), MGA_XCURCOL##n##BLUE, (col)[n].b);	\
} while (0)

void mga_dac_crtc1_restore_cursor_color(struct mga_dev *mdev,
					const struct drm_color col[16])
{
	MGA_WRITE_CURCOL(mdev, col, 0);
	MGA_WRITE_CURCOL(mdev, col, 1);
	MGA_WRITE_CURCOL(mdev, col, 2);

	if (mdev->chip < MGA_CHIP_G200)
		return;

	MGA_WRITE_CURCOL(mdev, col, 3);
	MGA_WRITE_CURCOL(mdev, col, 4);
	MGA_WRITE_CURCOL(mdev, col, 5);
	MGA_WRITE_CURCOL(mdev, col, 6);
	MGA_WRITE_CURCOL(mdev, col, 7);
	MGA_WRITE_CURCOL(mdev, col, 8);
	MGA_WRITE_CURCOL(mdev, col, 9);
	MGA_WRITE_CURCOL(mdev, col, 10);
	MGA_WRITE_CURCOL(mdev, col, 11);
	MGA_WRITE_CURCOL(mdev, col, 12);
	MGA_WRITE_CURCOL(mdev, col, 13);
	MGA_WRITE_CURCOL(mdev, col, 14);
	MGA_WRITE_CURCOL(mdev, col, 15);
}

static void mga_dac_crtc1_save_cursor_color(struct mga_dev *mdev,
					    struct drm_color col[16])
{
	MGA_READ_CURCOL(mdev, col, 0);
	MGA_READ_CURCOL(mdev, col, 1);
	MGA_READ_CURCOL(mdev, col, 2);

	if (mdev->chip < MGA_CHIP_G200)
		return;

	MGA_READ_CURCOL(mdev, col, 3);
	MGA_READ_CURCOL(mdev, col, 4);
	MGA_READ_CURCOL(mdev, col, 5);
	MGA_READ_CURCOL(mdev, col, 6);
	MGA_READ_CURCOL(mdev, col, 7);
	MGA_READ_CURCOL(mdev, col, 8);
	MGA_READ_CURCOL(mdev, col, 9);
	MGA_READ_CURCOL(mdev, col, 10);
	MGA_READ_CURCOL(mdev, col, 11);
	MGA_READ_CURCOL(mdev, col, 12);
	MGA_READ_CURCOL(mdev, col, 13);
	MGA_READ_CURCOL(mdev, col, 14);
	MGA_READ_CURCOL(mdev, col, 15);
}

void mga_dac_crtc1_cursor_mode(struct mga_dev *mdev,
			       unsigned int mode)
{
	static const u8 curmode_vals[] = {
		[0] = MGA_XCURCTRL_CURMODE_DISABLE,
		[1] = MGA_XCURCTRL_CURMODE_3_COLOR,
		[2] = MGA_XCURCTRL_CURMODE_XGA,
		[3] = MGA_XCURCTRL_CURMODE_XWINDOWS,
		[4] = MGA_XCURCTRL_CURMODE_16_COLOR,
	};
	u8 val;

	val = mga_dac_read8(mdev, MGA_XCURCTRL);

	val &= ~MGA_XCURCTRL_CURMODE;
	val |= curmode_vals[mode];

	mga_write8(mdev, MGA_X_DATAREG, val);
}

static void mga_dac_crtc1_cursor_fb(struct mga_dev *mdev,
				    struct mga_framebuffer *mfb)
{
	u32 curadd = mga_framebuffer_calc_bus(mfb, 0, 0, 0, 0);

	BUG_ON(curadd & ~0x3fffc00);

	curadd >>= 10;

	mga_dac_write8(mdev, MGA_XCURADDL, curadd & 0xFF);
	mga_dac_write8(mdev, MGA_XCURADDH, curadd >> 8);
}

void mga_dac_crtc1_cursor_image(struct mga_dev *mdev,
				const u8 *data)
{
	/* FIXME allocate memory using some proper method */
	unsigned int curadd = mdev->cursor.bus >> 10;
	void __iomem *curdata = mdev->cursor.virt;
	int i;

	/*
	 * FIXME curadd only has 24bits according to  docs
	 * does it mean G400+ can't place cursor at top half
	 * of 32MB FB?
	 */
	mga_dac_write8(mdev, MGA_XCURADDL, curadd & 0xFF);
	mga_dac_write8(mdev, MGA_XCURADDH, curadd >> 8);

	dev_dbg(mdev->dev, "curadd = %x\n", curadd);
	dev_dbg(mdev->dev, "curdata = %p\n", curdata);

	if (mdev->chip >= MGA_CHIP_G200) {
		for (i = 0; i < 3072; i++)
			writeb(data[i], curdata + i);
	} else {
		for (i = 0; i < 1024; i++)
			writeb(data[i], curdata + i);
	}
}

/* CRTC1 ??? */
void mga_dac_monitor_sense_start(struct mga_dev *mdev)
{
	int i;

	/* FIXME: Make sure a paletted mode is used... */
	/* FIXME: Make sure video is enabled... */
	/*
	 * Program a uniform color for the entire LUT.
	 * Voltage is 0 mV - 700 mV, detection threshold is 350 mV
	 * (1/2 of the maximum). Since the voltage should double
	 * when the monitor termination is missing aim for 1/3 of the
	 * maximum.
	 */
	mga_write8(mdev, MGA_PALWTADD, 0x00);
	for (i = 0; i < 256 * 3; i++)
		mga_write8(mdev, MGA_PALDATA, 0xFF / 3);

	/* Power up the sense comparator. */
	mga_dac_write8(mdev, MGA_XSENSETEST, MGA_XSENSETEST_SENSEPDN);
}

/* CRTC1 ??? */
void mga_dac_monitor_sense_stop(struct mga_dev *mdev,
				bool *r, bool *g, bool *b)
{
	int i;
	u8 val;

	/* Get the results. */
	val = mga_dac_read8(mdev, MGA_XSENSETEST);
	*r = !(val & MGA_XSENSETEST_RCOMP);
	*g = !(val & MGA_XSENSETEST_GCOMP);
	*b = !(val & MGA_XSENSETEST_BCOMP);

	/* Power down the sense comparator. */
	mga_dac_write8(mdev, MGA_XSENSETEST, 0);

	mga_dac_crtc1_set_palette(mdev);
}

static unsigned int mga_dac_crtc_sync(struct mga_dev *mdev,
				      unsigned int sync)
{
	(void)mdev;
	//return sync & ~SYNC_ON_GREEN;
	return sync;
}

/*
 * FIXME verify details
 * G400/G450/G550: no effect
 * Others: CRTC1 -> DAC1
 */
/* FIXME does this affect CRTC1, DAC1, DAC2 or what? */
void mga_dac_dac_set_sync(struct mga_dev *mdev,
			  unsigned int sync)
{
	u8 val;

	/* G400 iogsyncdis doesn't seem to have any effect */

	val = mga_dac_read8(mdev, MGA_XGENCTRL);

	if (sync & DRM_MODE_FLAG_IOGSYNC)
		val &= ~MGA_XGENCTRL_IOGSYNCDIS;
	else
		val |= MGA_XGENCTRL_IOGSYNCDIS;

	mga_write8(mdev, MGA_X_DATAREG, val);
}

/*
 * FIXME G450/G550
 * G400: CRTC1/CRTC2 -> DAC1
 * Others: CRTC1 -> DAC1
 */
void mga_dac_pedestal(struct mga_dev *mdev,
		      bool pedestal)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XGENCTRL);

	if (pedestal)
		val |= MGA_XGENCTRL_PEDON;
	else
		val &= ~MGA_XGENCTRL_PEDON;

	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_dac_crtc1_pixclk_enable(struct mga_dev *mdev, bool enable)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);

	if (enable)
		val &= ~MGA_XPIXCLKCTRL_PIXCLKDIS;
	else
		val |= MGA_XPIXCLKCTRL_PIXCLKDIS;

	mga_write8(mdev, MGA_X_DATAREG, val);

	mga_wait(mdev);
}

void mga_dac_crtc1_pixclk_select(struct mga_dev *mdev,
				 unsigned int pll)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);

	val &= ~MGA_XPIXCLKCTRL_PIXCLKSL;
	switch (pll) {
	case MGA_PLL_NONE:
		val |= MGA_XPIXCLKCTRL_PIXCLKSL_PCI;
		break;
	case MGA_PLL_PIXPLL:
		val |= MGA_XPIXCLKCTRL_PIXCLKSL_PIXPLL;
		break;
	case MGA_PLL_VIDPLL:
		val |= MGA_XPIXCLKCTRL_PIXCLKSL_VIDPLL;
		break;
	case MGA_PLL_TVO:
	case MGA_PLL_AV9110:
		val |= MGA_XPIXCLKCTRL_PIXCLKSL_VDOCLK;
		break;
	}

	mga_write8(mdev, MGA_X_DATAREG, val);
}

void mga_dac_pixpll_power(struct mga_dev *mdev, bool enable)
{
	u8 val;

	val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);

	if (enable)
		val |= MGA_XPIXCLKCTRL_PIXPLLPDN;
	else
		val &= ~MGA_XPIXCLKCTRL_PIXPLLPDN;

	mga_write8(mdev, MGA_X_DATAREG, val);

	if (enable)
		mdev->enabled_plls |= MGA_PLL_PIXPLL;
	else
		mdev->enabled_plls &= MGA_PLL_PIXPLL;
}

void mga_dac_syspll_power(struct mga_dev *mdev, bool enable)
{
	u32 val;

	val = pci_cfg_read32(&mdev->pdev, MGA_OPTION);

	if (enable)
		val |= MGA_OPTION_SYSPLLPDN;
	else
		val &= ~MGA_OPTION_SYSPLLPDN;

	pci_cfg_write32(&mdev->pdev, MGA_OPTION, val);

	if (enable)
		mdev->enabled_plls |= MGA_PLL_SYSPLL;
	else
		mdev->enabled_plls &= MGA_PLL_SYSPLL;
}

int mga_dac_power_down(struct mga_dev *mdev)
{
	mga_dac_crtc1_cursor_mode(mdev, 0);

	mga_dac_crtc1_pixclk_enable(mdev, false);
	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_NONE);

	mga_dac_dac_power(mdev, false);

	mga_dac_crtc1_power(mdev, false);

	mga_dac_pixpll_power(mdev, false);

	u8 val;
	val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);
	dev_dbg(mdev->dev, "PIXCLK is %s\n",
		(val & MGA_XPIXCLKCTRL_PIXCLKDIS) ? "DISABLED":"ENABLED");

	return 0;
}

int mga_dac_power_up(struct mga_dev *mdev, unsigned int clock)
{
	struct mga_dac_pll_settings pll;
	int ret;

	u8 val;

	val = mga_dac_read8(mdev, MGA_XPIXCLKCTRL);
	dev_dbg(mdev->dev, "pre PIXCLK is %s\n",
		(val & MGA_XPIXCLKCTRL_PIXCLKDIS) ? "DISABLED":"ENABLED");

	ret = mga_dac_pixpllc_calc(mdev, clock, 0, &pll);
	if (ret)
		return ret;

	mga_dac_crtc1_pixclk_enable(mdev, false);

	mga_dac_pixpll_power(mdev, true);

	mga_dac_crtc1_power(mdev, true);

	mga_dac_dac_power(mdev, true);

	ret = mga_dac_pixpllc_program(mdev, &pll);
	if (ret)
		goto error;

	mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_PIXPLL);

	mga_dac_crtc1_pixclk_enable(mdev, true);

	//mga_dac_crtc1_cursor_mode(mdev, 1); // FIXME

	// Restore palette
	if (0 && (mdev->pixel_format == DRM_FORMAT_ARGB8888 ||
		  mdev->pixel_format == DRM_FORMAT_XRGB8888))
		drm_generate_palette(DRM_FORMAT_C8, mdev->palette);
	else
		drm_generate_palette(mdev->pixel_format, mdev->palette);
	mga_dac_crtc1_set_palette(mdev);

	return 0;
 error:
	mga_dac_dac_power(mdev, false);
	mga_dac_crtc1_power(mdev, false);
	mga_dac_pixpll_power(mdev, false);

	return ret;
}

enum {
	/* G200WB signal BMC */
	MGA_MISC_BMC = 0x10,
};

static void mga_dac_gpio_set(struct mga_dev *mdev,
			     u8 mask, u8 output)
{
	u8 val;

	BUG_ON(output & ~mask);

	mga_write8(mdev, MGA_X_INDEXREG, MGA_XGENIOCTRL);
	val = mga_read8(mdev, MGA_X_DATAREG);
	val = (val & ~mask) | (output & mask);
	mga_write8(mdev, MGA_X_DATAREG, val);
}

static u8 mga_dac_gpio_get(struct mga_dev *mdev)
{
	return mga_dac_read8(mdev, MGA_XGENIODATA);
}

void mga_dac_i2c_set(struct mga_dev *mdev, u8 pin, bool state)
{
	mga_dac_gpio_set(mdev, pin, state ? 0 : pin);
}

bool mga_dac_i2c_get(struct mga_dev *mdev, u8 pin)
{
	return !!(mga_dac_gpio_get(mdev) & pin);
}

static void g200wb_pixpll_program(struct mga_dev *mdev,
				  const struct mga_dac_pll_settings *pll)
{
	unsigned int i = 0;
	u8 val;

	do {
		bool locked = false;
		unsigned int count = 0;
		u32 vcount;

		/* Set pixclkdis to 1 */
		mga_dac_crtc1_pixclk_enable(mdev, false);

		mga_write8(mdev, MGA_X_INDEXREG, MGA_XREMHEADCTRL);
		val = mga_read8(mdev, MGA_X_DATAREG);
		val |= MGA_XREMHEADCTRL_REMCLKDIS;
		mga_write8(mdev, MGA_X_DATAREG, val);

		/* Select PLL set C */
		//mga_pll_sel(0x3)

		/* Set pixlock to 0 */
		mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXPLLSTAT);
		val = mga_read8(mdev, MGA_X_DATAREG);
		val &= ~MGA_XPIXPLLSTAT_PIXLOCK;
		mga_write8(mdev, MGA_X_DATAREG, val);

		mga_write8(mdev, MGA_X_INDEXREG, MGA_XPIXCLKCTRL);
		val = mga_read8(mdev, MGA_X_DATAREG);
		val |= MGA_XPIXCLKCTRL_PIXPLLPDN | 0x80;
		mga_write8(mdev, MGA_X_DATAREG, val);

		udelay(500);

		/*
		 * When we are varying the output frequency by more than
		 * 10%, we must reset the PLL. However to be prudent, we
		 * will reset it each time that we are changing it.
		 */
		mga_write8(mdev, MGA_X_INDEXREG, MGA_XVREFCTRL);
		val = mga_read8(mdev, MGA_X_DATAREG);
		val &= ~MGA_XVREFCTRL_PIXPLLBGPDN;
		mga_write8(mdev, MGA_X_DATAREG, val);

		udelay(50);

		/* Program pixel PLL registers */
		mga_dac_write8(mdev, MGA_XPIXPLLCM_WB, pll->m);
		mga_dac_write8(mdev, MGA_XPIXPLLCN_WB, pll->n);
		mga_dac_write8(mdev, MGA_XPIXPLLCP_WB, pll->p);

		udelay(50);

		val = mga_read8(mdev, MGA_MISC_R);
		mga_write8(mdev, MGA_MISC_W, val);

		udelay(50);

		mga_write8(mdev, MGA_MISC_W, val);

		udelay(500);

		/* Turning the PLL on */
		val = mga_dac_read8(mdev, MGA_XVREFCTRL);
		val |= MGA_XVREFCTRL_PIXPLLBGPDN;
		mga_write8(mdev, MGA_X_DATAREG, val);

		udelay(500);

		/* Select the pixel PLL */
		mga_dac_crtc1_pixclk_select(mdev, MGA_PLL_PIXPLL);

		val = mga_dac_read8(mdev, MGA_XREMHEADCTRL);
		val &= ~MGA_XREMHEADCTRL_REMCLKSL;
		val |= MGA_XREMHEADCTRL_REMCLKSL_PLL;
		mga_write8(mdev, MGA_X_DATAREG, val);

		/* Set pixlock to 1 */
		val = mga_dac_read8(mdev, MGA_XPIXPLLSTAT);
		val |= MGA_XPIXPLLSTAT_PIXLOCK;
		mga_write8(mdev, MGA_X_DATAREG, val);

		/* Reset dotclock rate bit. */
		mga_write8(mdev, MGA_SEQX, 1);
		val = mga_read8(mdev, MGA_SEQD);
		val &= ~0x08;
		mga_write8(mdev, MGA_SEQD, val);

		/* Set pixclkdis to 1 */
		mga_dac_crtc1_pixclk_enable(mdev, true);

		/*
		 * Poll vcount. If it increments twice inside 150us,
		 * we assume that the PLL has locked.
		 */
		vcount = mga_read16(mdev, MGA_VCOUNT);
		while (count < 30 && !locked) {
			u32 temp = mga_read32(mdev, MGA_VCOUNT);

			if (temp < vcount)
				vcount = 0;

			if (temp - vcount > 2)
				locked = true;
			else
				udelay(5);
			count++;
		}

		if (locked)
			break;

		mga_write8(mdev, MGA_CRTCEXTX, 0x1e);
		val = mga_read8(mdev, MGA_CRTCEXTD);
		val = min(val + 1, 0xff);
		mga_write8(mdev, MGA_CRTCEXTD, val);

		i++;
	} while (i <= 32);

	/* Set remclkdis to 0 */
	val = mga_dac_read8(mdev, MGA_XREMHEADCTRL);
	val &= ~MGA_XREMHEADCTRL_REMCLKDIS;
	mga_write8(mdev, MGA_X_DATAREG, val);
}

static void g200wb_prepare(struct mga_dev *mdev)
{
	u8 val;

	mga_dac_gpio_set(mdev, MGA_MISC_BMC, MGA_MISC_BMC);

	val = mga_dac_read8(mdev, MGA_XSPAREREG);
	val |= MGA_XSPAREREG_REMFREQMSK;
	mga_write8(mdev, MGA_X_DATAREG, val);

	while (!(val & MGA_XSPAREREG_REMHSYNCSTS)) {
		val = mga_read8(mdev, MGA_X_DATAREG);
	}

	while (!(val & MGA_XSPAREREG_REMVSYNCSTS)) {
		val = mga_read8(mdev, MGA_X_DATAREG);
	}
}

static void g200wb_restore(struct mga_dev *mdev)
{
	u8 val;

	mga_write8(mdev, MGA_CRTCEXTX, 1);
	val = mga_read8(mdev, MGA_CRTCEXTD);
	val |= MGA_CRTCEXT1_VRSTEN | MGA_CRTCEXT1_HRSTEN;
	mga_write8(mdev, MGA_CRTCEXTD, val);

	val = mga_dac_read8(mdev, MGA_XREMHEADCTRL2);
	val |= MGA_XREMHEADCTRL2_RSTLVL2;
	mga_write8(mdev, MGA_X_DATAREG, val);

	udelay(10);

	val = mga_dac_read8(mdev, MGA_XREMHEADCTRL2);
	val &= ~MGA_XREMHEADCTRL2_RSTLVL2;
	mga_write8(mdev, MGA_X_DATAREG, val);

	udelay(10);

	val = mga_dac_read8(mdev, MGA_XSPAREREG);
	val &= ~MGA_XSPAREREG_REMFREQMSK;
	mga_write8(mdev, MGA_X_DATAREG, val);

	mga_dac_gpio_set(mdev, MGA_MISC_BMC, 0);
}

void mga_dac_pll_info_print(struct mga_dev *mdev,
			    const struct mga_dac_pll_info *info)
{
	dev_dbg(mdev->dev,
		"%s\n"
		" Fref = %u kHz\n"
		" Fo max = %u kHz\n"
		" Fvco min = %u kHz\n"
		" Ffvo max = %u kHz\n"
		" M = [%u,%u]\n"
		" N = [%u,%u]\n"
		" S limits = {%u,%u,%u,%u}\n"
		" PLLM    = 0x%02x\n"
		" PLLN    = 0x%02x\n"
		" PLLP    = 0x%02x\n"
		" PLLSTAT = 0x%02x\n"
		" PLLSTAT LOCK = 0x%02x\n",
		info->name,
		info->fref, info->fo_max,
		info->fvco_min, info->fvco_max,
		info->m_min, info->m_max,
		info->n_min, info->n_max,
		info->s_limits[0], info->s_limits[1],
		info->s_limits[2], info->s_limits[3],
		info->pllm, info->plln, info->pllp,
		info->pllstat, info->pllstat_lock);
}
