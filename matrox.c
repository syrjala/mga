/* SPDX-License-Identifier: MIT */
/*
 * Matrox kernel stub
 *
 * Copyright (C) 2008-2019 Ville Syrjälä <syrjala@sci.fi>
 */

#define  DEBUG 1

#include <linux/module.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/wait.h>

enum {
	MGA_CHIP_2064W,
	MGA_CHIP_2164W,
	MGA_CHIP_1064SG,
	MGA_CHIP_1164SG,
	MGA_CHIP_G100,
	MGA_CHIP_G200,
	MGA_CHIP_G200SE,
	MGA_CHIP_G200EV,
	MGA_CHIP_G200WB,
	MGA_CHIP_G400,
	MGA_CHIP_G450,
	MGA_CHIP_G550,
};

enum {
	MGA_BUS_PCI,
	MGA_BUS_AGP,
	MGA_BUS_PCIE,
};

static const char *mga_chip_names[] = {
	[MGA_CHIP_2064W]  = "MGA-2064W",
	[MGA_CHIP_2164W]  = "MGA-2164W",
	[MGA_CHIP_1064SG] = "MGA-1064SG",
	[MGA_CHIP_1164SG] = "MGA-1164SG",
	[MGA_CHIP_G100]   = "MGA-G100",
	[MGA_CHIP_G200]   = "MGA-G200",
	[MGA_CHIP_G200SE] = "MGA-G200SE",
	[MGA_CHIP_G200EV] = "MGA-G200EV",
	[MGA_CHIP_G200WB] = "MGA-G200WB",
	[MGA_CHIP_G400]   = "MGA-G400",
	[MGA_CHIP_G450]   = "MGA-G450",
	[MGA_CHIP_G550]   = "MGA-G550",
};

static const char *mga_bus_names[] = {
	[MGA_BUS_PCI]  = "PCI",
	[MGA_BUS_AGP]  = "AGP",
	[MGA_BUS_PCIE] = "PCIe",
};

enum {
	MGA_STATUS = 0x1E14,
	MGA_ICLEAR = 0x1E18,
	MGA_IEN    = 0x1E1C,
};

struct mga_dev {
	struct pci_dev *pdev;

	void __iomem *mmio_virt;
	void __iomem *mem_virt;
	void __iomem *iload_virt;

	u8 chip;
	u8 bus;

	s8 mmio_bar;
	s8 mem_bar;
	s8 iload_bar;

	u8 *rom;
	size_t rom_size;

	struct {
		struct page *pages;
		unsigned int num_pages;
	} dma;

	struct {
		unsigned int irq;
		spinlock_t lock;
		bool pending;
		u32 ien;
		wait_queue_head_t wait;
	} irq;
};

static u32 mga_read32(struct mga_dev *mdev, u32 reg)
{
	return readl(mdev->mmio_virt + reg);
}

static void mga_write32(struct mga_dev *mdev, u32 reg, u32 val)
{
	dev_dbg(&mdev->pdev->dev, "%s(%x, %x)\n", __func__, reg, val);
	writel(val, mdev->mmio_virt + reg);
	dev_dbg(&mdev->pdev->dev, "%s: %x = %x\n", __func__, reg, mga_read32(mdev, reg));
	writel(val, mdev->mmio_virt + reg);
	dev_dbg(&mdev->pdev->dev, "%s: %x = %x\n", __func__, reg, mga_read32(mdev, reg));
}

static irqreturn_t mga_irq_handler(int irq, void *dev)
{
	struct mga_dev *mdev = dev;
	u32 status = 0;
	int ret = IRQ_NONE;

	spin_lock(&mdev->irq.lock);

	if (!mdev->irq.pending)
		status = mga_read32(mdev, MGA_STATUS);

	if (status & mdev->irq.ien) {
		mga_write32(mdev, MGA_IEN, 0);
		mdev->irq.pending = true;
		wake_up_all(&mdev->irq.wait);
		ret = IRQ_HANDLED;
	}

	spin_unlock(&mdev->irq.lock);

	return ret;
}

static void mga_resume(struct pci_dev *pdev)
{
	struct mga_dev *mdev = pci_get_drvdata(pdev);

	/* 2064W doesn't support bus mastering. */
	if (mdev->chip != MGA_CHIP_2064W)
		pci_set_master(pdev);

	/* FIXME initialize device after D3 */
}

static void mga_suspend(struct pci_dev *pdev)
{
	/* FIXME deinitialize device */
}

// HACK for fops driver
#include "fops.c"

static bool known_pci_bridge(const struct pci_dev *pdev)
{
	return (pdev->vendor == 0x1011 && pdev->device == 0x0024) || /* Intel 21152 */
	       (pdev->vendor == 0x3388 && pdev->device == 0x0022) || /* PLX PCI6150 */
	       (pdev->vendor == 0x3388 && pdev->device == 0x0021) || /* PLX PCI6152 */
	       (pdev->vendor == 0x12D8 && pdev->device == 0x8150);   /* Pericom PI71C8150 */
}

static bool known_pcie_bridge(const struct pci_dev *pdev)
{
	return pdev->vendor == 0x104C && pdev->device == 0x8231; /* TI XIO2000 */
}

static void mga_probe_chip_and_bus(struct mga_dev *mdev, struct pci_dev *pdev)
{
	switch (pdev->device) {
	case 0x0519:
		mdev->chip = MGA_CHIP_2064W;
		break;
	case 0x051a:
	case 0x051e:
		if (pdev->revision >= 0x03)
			mdev->chip = MGA_CHIP_1164SG;
		else
			mdev->chip = MGA_CHIP_1064SG;
		break;
	case 0x051b:
	case 0x051f:
		mdev->chip = MGA_CHIP_2164W;
		break;
	case 0x0520:
	case 0x0521:
		mdev->chip = MGA_CHIP_G200;
		break;
	case 0x0522:
	case 0x0524:
		mdev->chip = MGA_CHIP_G200SE;
		break;
	case 0x0525:
		if (pdev->revision >= 0x80)
			mdev->chip = MGA_CHIP_G450;
		else
			mdev->chip = MGA_CHIP_G400;
		break;
	case 0x0530:
		mdev->chip = MGA_CHIP_G200WB;
		break;
	case 0x0532:
		mdev->chip = MGA_CHIP_G200EV;
		break;
	case 0x1000:
	case 0x1001:
		mdev->chip = MGA_CHIP_G100;
		break;
	case 0x2527:
		mdev->chip = MGA_CHIP_G550;
		break;
	default:
		BUG();
	}

	/*
	 * Identify the bus type.
	 *
	 * Many PCI/PCIe boards are in fact built using AGP chips, so simply
	 * checking the AGP capability is not enough. Also there's no sane
	 * way to tell whether the parent bridge is an AGP bridge. Only the
	 * host bridge has the AGP capability, but we can't really tell whether
	 * there are non-AGP bridges between the host bridge and the chip.
	 *
	 * So just check whether the parent bridge is one of the PCI/PCIe
	 * bridges known to be used by Matrox on these PCI/PCIe boards.
	 *
	 * G200 MMS, G450 PCI/MMS, G550 PCI:
	 *  AGP chip(s) w/ PCI<->PCI bridge
	 * G500 PCIe:
	 *  AGP chip(s) w/ PCI<->PCIe bridge
	 */
	if (pci_find_capability(pdev, PCI_CAP_ID_AGP)) {
		if (known_pcie_bridge(pdev->bus->self))
			mdev->bus = MGA_BUS_PCIE;
		else if (known_pci_bridge(pdev->bus->self))
			mdev->bus = MGA_BUS_PCI;
		else
			mdev->bus = MGA_BUS_AGP;
	} else
		mdev->bus = MGA_BUS_PCI;

	dev_info(&pdev->dev, "detected %s (%s)\n",
		 mga_chip_names[mdev->chip], mga_bus_names[mdev->bus]);

	switch (mdev->chip) {
	case MGA_CHIP_2064W:
		mdev->mmio_bar = 0;
		mdev->mem_bar = 1;
		mdev->iload_bar = -1;
		break;
	case MGA_CHIP_1064SG:
		mdev->mmio_bar = 0;
		mdev->mem_bar = 1;
		mdev->iload_bar = 2;
		break;
	default:
		mdev->mmio_bar = 1;
		mdev->mem_bar = 0;
		mdev->iload_bar = 2;
		break;
	}
}

static int mga_read_rom(struct mga_dev *mdev, struct pci_dev *pdev)
{
	void __iomem *rom;
	size_t rom_size;

	rom = pci_map_rom(pdev, &rom_size);

	/*
	 * G200/G450 MMS: first head is OK but the other heads
	 * report rom_size=0 even though they have EEPROMs attached to
	 * them. The EEPROM contents seem to be bogus though, not even
	 * PInS data can be found by brute force search. Most likely
	 * it's just used to initialize PCI subsys IDs.
	 */
	if (rom && !rom_size) {
		pci_unmap_rom(pdev, rom);
		rom = NULL;
	}

	/* G100 MMS: only the first head has ROM */
	if (!rom)
		return -ENODEV;

	dev_info(&pdev->dev, "Device has ROM (%zu kB)\n", rom_size >> 10);

	mga_pins_parse(mdev, rom, rom_size);

	pci_unmap_rom(pdev, rom);

	return 0;
}

static int mga_verify_mms(struct pci_dev *pdev)
{
	struct pci_dev *dev;
	u16 subsystem_vendor = 0, subsystem_device = 0;

	/*
	 * Make sure each device on the bus is identical.
	 * Hopefully that is good enough to detect MMS cards.
	 */
	list_for_each_entry(dev, &pdev->bus->devices, bus_list) {
		if (dev->vendor != pdev->vendor ||
		    dev->device != pdev->device)
			return -ENODEV;

		/*
		 * G100 MMS: Only the first head has any ROM,
		 * so the other heads have zeroed SUBSYSID.
		 */
		if (dev->subsystem_vendor) {
			if (!subsystem_vendor)
				subsystem_vendor = dev->subsystem_vendor;
			else if (subsystem_vendor != dev->subsystem_vendor)
				return -ENODEV;
		}
		if (dev->subsystem_device) {
			if (!subsystem_device)
				subsystem_device = dev->subsystem_device;
			else if (subsystem_device != dev->subsystem_device)
				return -ENODEV;
		}
	}

	return 0;
}

static int mga_read_rom_bus(struct mga_dev *mdev, struct pci_dev *pdev)
{
	struct pci_dev *dev;

	list_for_each_entry(dev, &pdev->bus->devices, bus_list) {
		if (dev != pdev)
			continue;
		if (!mga_read_rom(mdev, dev))
			return 0;
	}

	return -ENODEV;
}

static void mga_dma_free(struct mga_dev *mdev)
{
	int i;

	if (!mdev->dma.pages)
		return;

	for (i = 0; i < mdev->dma.num_pages; i++)
		ClearPageReserved(&mdev->dma.pages[i]);

	__free_pages(mdev->dma.pages, get_order(mdev->dma.num_pages << PAGE_SHIFT));
}

static void mga_dma_alloc(struct pci_dev *pdev,
			  struct mga_dev *mdev, int size)
{
	int order = get_order(size);
	int i;

	while (order >= 0) {
		mdev->dma.pages = alloc_pages(GFP_USER |
					      GFP_HIGHUSER |
					      __GFP_ZERO |
					      __GFP_COMP,
					      order);

		if (mdev->dma.pages) {
			mdev->dma.num_pages = 1 << order;
			dev_info(&pdev->dev, "allocated %u KiB for DMA starting at 0x%x\n",
				 mdev->dma.num_pages << PAGE_SHIFT >> 10,
				 lower_32_bits(page_to_phys(mdev->dma.pages)));
			break;
		}
		order--;
	}

	for (i = 0; i < mdev->dma.num_pages; i++)
		SetPageReserved(&mdev->dma.pages[i]);
}

static int mga_pci_probe(struct pci_dev *pdev,
			 const struct pci_device_id *id)
{
	struct mga_dev *mdev;
	int err;

	/* Identify the VGA device. */
	{
	u16 cmd;
	pci_read_config_word(pdev, PCI_COMMAND, &cmd);
	if (cmd & PCI_COMMAND_IO)
		dev_info(&pdev->dev, "device has IO decode enabled, assuming it's the VGA device\n");
	}

	err = pci_enable_device_mem(pdev);
	if (err)
		return err;

	mdev = kzalloc(sizeof *mdev, GFP_KERNEL);
	if (!mdev) {
		err = -ENOMEM;
		goto disable_device;
	}

	mdev->pdev = pdev;

	mga_probe_chip_and_bus(mdev, pdev);

	/* Parse PinS. */
	err = mga_read_rom(mdev, pdev);

	if (err && (mdev->chip == MGA_CHIP_G100 ||
		    mdev->chip == MGA_CHIP_G200 ||
		    mdev->chip == MGA_CHIP_G450)) {
		/* Could be a secondary MMS head -> make sure. */
		err = mga_verify_mms(pdev);
		if (err)
			goto free_dev;

		/* On MMS cards only the first head has a proper ROM. */
		dev_info(&pdev->dev, "%s MMS head without ROM\n",
			 mga_chip_names[mdev->chip]);

		err = mga_read_rom_bus(mdev, pdev);
	}

	if (err)
		goto free_dev;

	err = pci_request_region(pdev, mdev->mmio_bar, "mga control");
	if (err)
		goto free_dev;
	mdev->mmio_virt = pci_iomap(pdev, mdev->mmio_bar, 0);
	if (!mdev->mmio_virt) {
		err = -ENXIO;
		goto release_mmio;
	}

	err = pci_request_region(pdev, mdev->mem_bar, "mga frame buffer");
	if (err)
		goto unmap_mmio;
	/* FIXME map only RAM size? Need to probe RAM size first... */
	mdev->mem_virt = pci_iomap(pdev, mdev->mem_bar, 0);
	if (!mdev->mem_virt) {
		err = -ENXIO;
		goto release_mem;
	}

	/* FIXME: Any use for the ILOAD aperture? */
	if (mdev->iload_bar >= 0) {
		err = pci_request_region(pdev, mdev->iload_bar, "mga iload");
		if (err)
			goto unmap_mem;
		mdev->iload_virt = pci_iomap(pdev, mdev->iload_bar, 0);
		if (!mdev->iload_virt) {
			err = -ENXIO;
			goto release_iload;
		}
	}

	if (!pdev->irq) {
		err = -ENXIO;
		goto unmap_iload;
	}
	mdev->irq.irq = pdev->irq;

	mga_dma_alloc(pdev, mdev, 4 << 20); /* start w/ 4MB for DMA */

	pci_set_cacheline_size(pdev);

	spin_lock_init(&mdev->irq.lock);
	init_waitqueue_head(&mdev->irq.wait);

	err = request_irq(mdev->irq.irq, mga_irq_handler, 0, "mga", mdev);
	if (err)
		goto dma_free;

	pci_set_drvdata(pdev, mdev);

	mga_resume(pdev);

	/* FIXME */
	//mga_probe_mem_size();

	/* FIXME init is done -> register to relevant subsystems. */

	err = mga_fops_register(pdev); // HACK for fops driver
	if (err) {
		mga_suspend(pdev);
		goto clear_drvdata;
	}

	return 0;

 clear_drvdata:
	pci_set_drvdata(pdev, NULL);
	free_irq(mdev->irq.irq, mdev);
 dma_free:
	mga_dma_free(mdev);
 unmap_iload:
	if (mdev->iload_bar >= 0)
		pci_iounmap(pdev, mdev->iload_virt);
 release_iload:
	if (mdev->iload_bar >= 0)
		pci_release_region(pdev, mdev->iload_bar);
 unmap_mem:
	pci_iounmap(pdev, mdev->mem_virt);
 release_mem:
	pci_release_region(pdev, mdev->mem_bar);
 unmap_mmio:
	pci_iounmap(pdev, mdev->mmio_virt);
 release_mmio:
	pci_release_region(pdev, mdev->mmio_bar);
 free_dev:
	mga_pins_free(mdev); // HACK for fops driver
	kfree(mdev);
 disable_device:
	pci_disable_device(pdev);

	return err;
}

static void mga_pci_remove(struct pci_dev *pdev)
{
	struct mga_dev *mdev = pci_get_drvdata(pdev);

	mga_pins_free(mdev); // HACK for fops driver
	mga_fops_deregister(pdev); // HACK for fops driver

	/* FIXME stop everything */

	mga_suspend(pdev);

	pci_set_drvdata(pdev, NULL);

	free_irq(mdev->irq.irq, mdev);

	mga_dma_free(mdev);

	/* Release resources. */
	if (mdev->iload_bar >= 0) {
		pci_iounmap(pdev, mdev->iload_virt);
		pci_release_region(pdev, mdev->iload_bar);
	}

	pci_iounmap(pdev, mdev->mem_virt);
	pci_release_region(pdev, mdev->mem_bar);

	pci_iounmap(pdev, mdev->mmio_virt);
	pci_release_region(pdev, mdev->mmio_bar);

	kfree(mdev);

	pci_disable_device(pdev);
}

#ifdef CONFIG_PM
static int mga_pci_suspend(struct pci_dev *pdev, pm_message_t state)
{
	//struct mga_dev *mdev = pci_get_drvdata(pdev);

	/* FIXME stop everything */

	mga_suspend(pdev);

	/* FIXME save config space? */

	/* FIXME what about 2064W w/o PM support? */
	/* FIXME check */
	pci_set_power_state(pdev, PCI_D3cold);

	return 0;
}

static int mga_pci_resume(struct pci_dev *pdev)
{
	//struct mga_dev *mdev = pci_get_drvdata(pdev);

	mga_resume(pdev);

	/* FIXME restart everything */

	return 0;
}
#else
#define mga_pci_suspend NULL
#define mga_pci_resume NULL
#endif

static const struct pci_device_id mga_pci_id_table[] = {
	{ PCI_DEVICE(0x102b, 0x0519) }, /* MGA-2064W PCI */
	{ PCI_DEVICE(0x102b, 0x051a) }, /* MGA-1064SG/1164SG PCI */
	{ PCI_DEVICE(0x102b, 0x051b) }, /* MGA-2164W PCI */
	{ PCI_DEVICE(0x102b, 0x051e) }, /* MGA-1064SG/1164SG AGP */
	{ PCI_DEVICE(0x102b, 0x051f) }, /* MGA-2164W AGP */
	{ PCI_DEVICE(0x102b, 0x0520) }, /* MGA-G200 PCI */
	{ PCI_DEVICE(0x102b, 0x0521) }, /* MGA-G200 AGP */
	{ PCI_DEVICE(0x102b, 0x0522) }, /* MGA-G200 SE A PCI */
	{ PCI_DEVICE(0x102b, 0x0524) }, /* MGA-G200 SE B PCI */
	{ PCI_DEVICE(0x102b, 0x0525) }, /* MGA-G400/G450 AGP */
#if 0
	/* FIXME these need custom PLL code */
	{ PCI_DEVICE(0x102b, 0x0530) }, /* MGA-G200 WB PCI */
	{ PCI_DEVICE(0x102b, 0x0532) }, /* MGA-G200 EV PCI */
#endif
	{ PCI_DEVICE(0x102b, 0x1000) }, /* MGA-G100 PCI */
	{ PCI_DEVICE(0x102b, 0x1001) }, /* MGA-G100 AGP */
	{ PCI_DEVICE(0x102b, 0x2527) }, /* MGA-G550 AGP */
	{ }
};

static struct pci_driver mga_pci_driver = {
	.name = "mga",
	.id_table = mga_pci_id_table,
	.probe = mga_pci_probe,
	.remove = mga_pci_remove,
	.suspend = mga_pci_suspend,
	.resume = mga_pci_resume,
};

static int __init mga_init(void)
{
	return pci_register_driver(&mga_pci_driver);
}

static void __exit mga_exit(void)
{
	pci_unregister_driver(&mga_pci_driver);
}

module_init(mga_init);
module_exit(mga_exit);

MODULE_DESCRIPTION("Matrox graphics driver");
MODULE_AUTHOR("Ville Syrjala <syrjala@sci.fi");
MODULE_LICENSE("Dual MIT/GPL");
MODULE_DEVICE_TABLE(pci, mga_pci_id_table);
